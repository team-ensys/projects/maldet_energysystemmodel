# CO2 Components

File with all Components, allocated to the co2 sector. Following packages are imported:

```
from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import emission_components as em_comp`

Call of the components:

```
componentObject = em_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`emissions = em_comp.Emissions(sector=consumer1.getEmissionsSector())`

## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
def component(self):
        m_emissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions, variable_costs=self.costs_in)
            
        emissions = solph.Sink(label=self.label, inputs={self.sector_in: m_emissions})
        
        return emissions
```

## CO2 Emissions Total

Summation of all components with emissions in a total emission sink by a balance rule. The sink is multiplied by a given CO2 price. 
By implementing the emissions as an own sector, the output is generated by the bus balance ruls

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| CO2 Preis in €/kg | `costs` | 0.03 |
| Sector | `sector` | / |
| Maximum Emissions | `max_emissions` | 1000000000 |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | P2H_ + position |

</details>

<details>
<summary>Code</summary>

```

    class Emissions():
    
    def __init__(self, *args, **kwargs):
        max_emissions = 1000000000
        deltaT=1
        position='Consumer_1'
        
        #CO2 Price in € per kg
        co2_price = 0.03
        
        self.max_emissions= kwargs.get("max_emissions", max_emissions)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Emissions_' + self.position
        self.labelSankey = 'Emissions_' + self.position
        self.costs_in=kwargs.get("costs", co2_price)
        self.color='dimgrey'
        
        
    def component(self):
        m_emissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions, variable_costs=self.costs_in)
            
        emissions = solph.Sink(label=self.label, inputs={self.sector_in: m_emissions})
        
        return emissions


```
</details>
