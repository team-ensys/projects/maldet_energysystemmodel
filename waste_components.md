# Waste Components

File with all Components, allocated to the waste sector. Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import waste_components as waste_comp`

Call of the components:

```
componentObject = waste_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`wasteCombustion = waste_comp.Wastecombustion(sector_in=consumer1.getWastedisposalSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()])`


## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
def component(self):
            m_wasteAd = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_waste*self.share_biowaste*self.eta), variable_costs=self.costs_in)
            q_wasteAdGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            wasteAd = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteAd},
                outputs={self.sector_out : q_wasteAdGas},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_waste*self.share_biowaste*self.eta})
            
            
            return wasteAd
```


## Accruing Waste

Accruing Waste given as timeseries, which can be given in the input data.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Total Steps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | WasteAccruing_ + position |
| Accruing Waste | `waste_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class Accruing():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'WasteAccruing_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.waste_timeseries = kwargs.get("waste_timeseries", demandArray)

        def component(self):
            
            m_waste = solph.Flow(fix=self.waste_timeseries, nominal_value=1)
            
            wasteAccruing = solph.Source(label=self.label, outputs={self.sector: m_waste})
            
            return wasteAccruing 


```
</details>


## Wastestorage

**Input: Waste (from Accruing)**

**Output: Waste Storage (Own Sector for further waste processing)**

Wastestorage Component --> 
Waste storages are usually emptied at certain timeperiods, which is implemented in the model with a new block [StorageIntermediate](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/StorageIntermediate.md). The disposal periods can be set with a parameter `disposal_periods`. 

**If the parameter is set to zero, a simple GenericStorage is used, where the storage can be emptied in all periods.**



<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0 |
| Input Efficiency | `eta_in` | 1 |
| Output Efficiency | `eta_out` | 1 |
| Standby Efficiency | `eta_sb` | 1 |
| Maximum Volume | `volume_max` | 0.24 |
| Start Volume | `volume_start` | 0 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Density Waste | `rho_waste` | 100 |
| Timesteps after which the storage is emptied | `disposal_periods` | 8760 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | WasteStorage_ + position |
| Battery Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class WasteStorage():
    
    def __init__(self, *args, **kwargs):
        rho_municipalwaste = 100
        costsIn = 0
        costsOut=0
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 1
        V_max = 0.24
        V_start = 0
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        disposal_periods=0
        position='Consumer_1'
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start=V_start
        #self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'WasteStorage_' + self.position
        self.balanced = kwargs.get("balanced", True)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        
        
    def component(self):
        #Energy Flows
        m_waste2storeWasteStoreIn = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_in)
        m_waste2storeWasteStoreOut = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_out)
        
        if(self.disposal_periods==0):
             wastestore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
             return wastestore
        else:
        
            wastestore = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps)
        
            return wastestore
```
</details>

## Wastestorage Sink

Sink for the excess waste. Should only be used if no other waste treatment possibilities are possible and no disposal with costs can be implemented.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Disposal Volume| `volume_max` | 0.24 |
| Density Waste | `rho_waste` | 100 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | WastestorageSink_ + position |

</details>

<details>
<summary>Code</summary>

```

class WastestorageSink():
    
    def __init__(self, *args, **kwargs):
        rho_municipalwaste = 100
        V_max=0.24
        deltaT=1
        position='Consumer_1'
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.v_max= kwargs.get("volume_max", V_max)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'WastestorageSink_' + self.position
        self.costs=0
        
        
    def component(self):
        m_storageDisposal = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs)
            
        wasteDisposal = solph.Sink(label=self.label, inputs={self.sector: m_storageDisposal})
        
        return wasteDisposal
 




```
</details>

## Waste to Biogas (Anaerobic Digestion)

Waste to Biogas Component --> Simple Transformer

**Input: Waste (from storage)**

**Output: Gas**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.068 |
| Output costs | `c_out` | 0 |
| Conversion factor | `conversion_factor` | 0.5 |
| Input Power Limit | `P_in` | 100 |
| Output Power Limit | `P_out` | 100 |
| Share of Biowaste | `share_biowaste` | 0.225 |
| Heating Value Waste | `H_waste` | 3.4 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | WasteBiogas_ + position |

</details>

<details>
<summary>Code</summary>

```

    class WasteBiogas():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.5
            share_biowaste=0.225
            eta=0.9
            costsIn=0.068
            costsOut=0
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0
            position='Consumer_1'
            H_waste=3.4
            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'WasteBiogas_' + self.position
            self.H_waste = kwargs.get("H_waste", H_waste)

    def component(self):
            m_wasteAd = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_waste*self.share_biowaste*self.eta), variable_costs=self.costs_in)
            q_wasteAdGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            wasteAd = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteAd},
                outputs={self.sector_out : q_wasteAdGas},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_waste*self.share_biowaste*self.eta})
            
            
            return wasteAd



```
</details>

## Waste Hydrogen AD

Waste to Hydrogen --> Simple Transformer

**Input: Waste (from storage)**

**Output: Hydrogen**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.068 |
| Output costs | `c_out` | 0 |
| Conversion factor | `conversion_factor` | 0.0148 |
| Maximum processable mass | `M_max` | 145 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | WasteADH2_ + position |

</details>

<details>
<summary>Code</summary>

```

class WasteHydrogenAD():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.0148
            share_biowaste=0.225
            eta=0.9
            costsIn=0.068
            costsOut=0
            MLimit = 145
            deltaT=1
            emissions=0
            position='Consumer_1'

            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.M_max = kwargs.get("M_max", MLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'WasteADH2_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Waste\nADH2_' + self.position
            self.color='plum'

    def component(self):
            m_wasteAd = solph.Flow(min=0, max=1, nominal_value=self.M_max, variable_costs=self.costs_in)
            q_wasteAdH2 = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.share_biowaste*self.eta*self.conversion_factor, variable_costs=self.costs_out)
            
            wasteAdH2 = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteAd},
                outputs={self.sector_out : q_wasteAdH2},
                conversion_factors={self.sector_out : self.share_biowaste*self.eta*self.conversion_factor})
            
            
            return wasteAdH2    



```
</details>

## Waste Combustion

**Input: Waste (from storage)**

**Output: Electricity, Heat**

Waste combustion is a special case, as this technology has two outputs. The output sectors, output costs and conversion factors are given to the component as list and are called with their indices. 

`sector_out=[sector_out1, sector_out2]`

`c_out=[c_out1, c_out2]`

`conversion_factor=[conversion_factor1, conversion_factor2]`

**It is important that output sectors and conversion factors are given to the component in the same order!** 

The default values are set in the order `[Electricity, Heat]`.

If emissions are set, a component with multiple outputs is created.


<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | [0.04, 0.04] |
| Conversion factor | `conversion_factor` | [0.35, 0.44]|
| Input Power Limit | `P_in` | 100 |
| Output Power Limit | `P_out` | 100 |
| Heating Value Waste | `H_waste` | 3.4 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Emissions Sector | `sector_emissions` | 0 |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 1.1 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Wastecombustion_ + position |

</details>

<details>
<summary>Code</summary>

```

    class Wastecombustion():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.004, 0.004]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=1.1
            maxEmissions=1000000
            position='Consumer_1'
            H_waste=3.4
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Wastecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Waste\nCombustion_' + self.position
            self.H_waste = kwargs.get("H_waste", H_waste)
            self.color='brown'

    def component(self):
            m_wasteCombustion = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_waste, variable_costs=self.costs_in)
            q_wasteCombustionEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_wasteCombustionTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste, self.sector_out[0] : self.conversion_factor[0]*self.H_waste})
                
            else:
                m_wastecombustionEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl, self.sector_emissions : m_wastecombustionEmissions},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste, self.sector_out[0] : self.conversion_factor[0]*self.H_waste, self.sector_emissions : self.emissions})
            
            
            return wasteCombustion
        

```
</details>

## Waste Fermentation

Waste to Hydrogen Fermentation Component --> Simple Transformer

**Input: Waste (from storage)**

**Output: Hydrogen**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 2.07 |
| Conversion factor | `conversion_factor` | 0.5 |
| Efficiency | `eta` | 0.9 |
| Maximum Conversion Rate | `rate_max` | 0.01 |
| Nominal Conversion Rate | `rate` | 0.0888 |
| Share of Biowaste | `share_biowaste` | 0.225 |
| Heating Value Waste | `H_waste` | 3.4 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | WasteFermentation_ + position |

</details>

<details>
<summary>Code</summary>

```

    class Fermentation():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.5
            share_biowaste=0.225
            eta=0.9
            costsIn=0
            costsOut=2.07
            rate_max=0.01
            rate_fermentation=0.0888
            deltaT=1
            emissions=0
            position='Consumer_1'
            H_waste=3.4
            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.rate = kwargs.get("rate", rate_fermentation)
            self.rate_max = kwargs.get("rate_max", rate_max)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'WasteFermentation_' + self.position
            self.H_waste = kwargs.get("H_waste", H_waste)

    def component(self):
            m_wasteFerm = solph.Flow(min=0, max=1, nominal_value=self.rate_max*self.deltaT/(self.rate*self.share_biowaste*self.eta), variable_costs=self.costs_in)
            v_wasteFermHydro = solph.Flow(min=0, max=1, nominal_value=self.rate_max*self.deltaT, variable_costs=self.costs_out)
            
            wasteFerm = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteFerm},
                outputs={self.sector_out : v_wasteFermHydro},
                conversion_factors={self.sector_out : self.rate*self.share_biowaste*self.eta})
            
            
            
            return wasteFerm



```
</details>

## Waste to Biofuel

Waste to Biofuel for Biofuel Vehicle --> Simple Transformer

**Input: Waste (from storage)**

**Output: Biofuel**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 530 |
| Efficiency | `eta` | 0.9 |
| Maximum Processable Mass | `M_max` | 700 |
| Share of Biowaste | `share_biowaste` | 0.225 |
| Densitiy Waste | `rho_waste` | 800 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | WasteBiofuel_ + position |

</details>

<details>
<summary>Code</summary>

```

    class WasteBiofuel():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class

            share_biowaste=0.225
            eta=0.9
            costsIn=0
            costsOut=530
            M_max=700
            deltaT=1
            emissions=0
            position='Consumer_1'
            rho_biofuel=800
            
            self.eta = kwargs.get("eta", eta)

            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.M_max = kwargs.get("M_max", M_max)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.factor_processing = self.deltaT/24
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'WasteBiofuel_' + self.position
            self.rho_biofuel = kwargs.get("rho_biofuel", rho_biofuel)

    def component(self):
            m_waste2biofuel = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.factor_processing, variable_costs=self.costs_in)
            v_waste2biofuel = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.factor_processing/self.rho_biofuel*self.share_biowaste*self.eta, variable_costs=self.costs_out)
            
            waste2biofuel = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_waste2biofuel},
                outputs={self.sector_out : v_waste2biofuel},
                conversion_factors={self.sector_out : self.factor_processing/self.rho_biofuel*self.share_biowaste*self.eta})
            
            
            
            
            return waste2biofuel



```
</details>

## Biofuel Sink

Sink for the excess biofuel. Should only be used if the biofuel production is investigated without application in a biofuel vehicle.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Processable Mass | `M_max` | 700 |
| Share of Biowaste | `share_biowaste` | 0.225 |
| Efficiency | `eta` | 0.9 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | BiofuelSink_ + position |

</details>

<details>
<summary>Code</summary>



```
class BiofuelSink():
    
    def __init__(self, *args, **kwargs):
        share_biowaste=0.225
        eta=0.9
        M_max=700
        deltaT=1
        emissions=0
        position='Consumer_1'
        rho_biofuel=800
            
        self.eta = kwargs.get("eta", eta)

        self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)

        self.costs = 0
        self.M_max = kwargs.get("M_max", M_max)
        self.sector = kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.factor_processing = self.deltaT/24
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'BiofuelSink_' + self.position
        self.rho_biofuel = kwargs.get("rho_biofuel", rho_biofuel)
        
        
    def component(self):
        v_waste2biofuel = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.factor_processing/self.rho_biofuel*self.share_biowaste*self.eta, variable_costs=self.costs)
            
        wasteDisposal = solph.Sink(label=self.label, inputs={self.sector: v_waste2biofuel})
        
        return wasteDisposal
```

</details>

## Wastedisposal

Sink for the waste disposal with disposal costs. Emissions that occur in the disposal process can be set. 

A minimum of disposed waste can be set by giving the minimum and the input data --> amout of input data that is disposed in total over the year. If no continous waste input into the storage is implemented, a storage start volume must be given. This is to guarantee the implementation of the constraint.

** sum_t(m_dispose(t)) >= min_dispose * sum_t(m_input(t)) **

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Disposal Volume| `volume_max` | 0.24 |
| Minimum Diposed Waste| `min_disposal` | 0 |
| Input Waste Timeseries| `waste_in` | 0 |
| Density Waste | `rho_waste` | 100 |
| Disposal Costs | `costs` | Numpy Array with 0 |
| Sector | `sector` | / |
| Emissions Sector | `sector_emissions` | 0 |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0.382 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Wastedisposal_ + position |

</details>

<details>
<summary>Code</summary>

```

class Wastedisposal():
    
    def __init__(self, *args, **kwargs):

        rho_municipalwaste = 100
        V_max=0.24
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        emissions=0.382
        maxEmissions=1000000
        minDisposal=0
        
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.v_max= kwargs.get("volume_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.minDisposalPercent = kwargs.get("min_disposal", minDisposal)
        self.position = kwargs.get("position", position)
        label = 'Wastedisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Waste\nDisposal_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.inputWaste = kwargs.get("waste_in", 0)
        
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.max_emissions=kwargs.get("max_emissions", maxEmissions)
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        self.color='red'
        
        self.minDisposal = np.sum(np.multiply(self.inputWaste, self.minDisposalPercent))
        
        
    def component(self):


                
        m_wasteDispose = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_in, summed_min=self.minDisposal/(self.v_max*self.rho_municipalwaste))
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            
            wasteDispose = solph.Sink(label=self.label, inputs={self.sector_in: m_wasteDispose})
            
        else:
             m_wastedisposalEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
             wasteDispose = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteDispose},
                    outputs={self.sector_emissions : m_wastedisposalEmissions},
                    conversion_factors={self.sector_emissions : self.emissions})
            
        

        
        return wasteDispose
 




```
</details>

## Waste Market

Sink for a waste market where revenues can be generated via innovative business models with waste selling.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Disposal Volume| `volume_max` | 0.24 |
| Density Waste | `rho_waste` | 100 |
| Disposal Costs | `revenues` | Numpy Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Wastemarket_ + position |

</details>

<details>
<summary>Code</summary>

```

class Wastemarket():
    
    def __init__(self, *args, **kwargs):

        rho_municipalwaste = 100
        V_max=0.24
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.v_max= kwargs.get("volume_max", V_max)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Wastemarket_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsArray = np.zeros(self.timesteps)
        
        self.revenues=kwargs.get("revenues", costsArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):


                
        m_waste2Market = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.revenues)
            
        waste2Market = solph.Sink(label=self.label, inputs={self.sector: m_waste2Market})
        

        
        return waste2Market
 




```
</details>

