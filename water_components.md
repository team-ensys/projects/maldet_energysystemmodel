# Water Components

File with all Components, allocated to the water sector (including sewage and sludge). Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import water_components as water_comp`

Call of the components:

```
componentObject = water_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`waterDemand = water_comp.Waterdemand(sector_in=consumer1.getWaterSector(), sector_out=consumer1.getSewageSector(), demand=consumer1.input_data['waterDemand'])`

## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
def component(self):
            v_sewageWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs_in)
            v_cleanWater = solph.Flow(min=0, max=1, nominal_value=self.limit[0], variable_costs=self.costs_out[0])
            v_sludgeSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[1], variable_costs=self.costs_out[1])
            q_heatSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[2], variable_costs=self.costs_out[2])

            
            sewageTreatmentPlant = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageWaterTreat},
                outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat},
                conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1], self.sector_out[2] : self.conversion_factor[2]})
            
            
            return sewageTreatmentPlant
```

## Pipeline

Input component to cover the remaining required water demand. It is differed between costs and model costs (only for optimization).

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Connection Volume | `V` | 100000 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Costs | `costs` | 1000 |
| Model costs | `costs_model` | costs |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Waterpipeline_ + position |

</details>

<details>
<summary>Code</summary>

```


class Pipeline():
    
    def __init__(self, *args, **kwargs):
        V_connection = 100000
        deltaT=1
        position='Consumer_1'
        costs=1000
        timesteps=8760
        
        self.V= kwargs.get("V", V_connection)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Waterpipeline_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Water\nPipeline_' + self.position
        self.costs_model=kwargs.get("costs_model", costs)
        self.color='slategrey'
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        v_waterpipeline = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_model)
            
        waterpipeline = solph.Source(label=self.label, outputs={self.sector_out: v_waterpipeline})
        
        return waterpipeline
    


```
</details>

## Pipeline Purchase

Purchase from the water pipeline with fixed costs and water costs. The water costs are given to the model as timeseries. Fixed costs are added to the consumer attribute `fixed_costs` to be able to evaluate the total fixed costs for all sectors.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Connection Volume | `V` | 100000 |
| Fix Costs | `costs_fixed` | 28.33 |
| Water Costs | `costs_water` | Numpay Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Waterpipelinepurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class PipelinePurchase():
    
    def __init__(self, *args, **kwargs):
        V_connection = 100000
        C_fix = 28.33
        deltaT=1
        position='Consumer_1'
        timesteps=8760

        
        self.V= kwargs.get("V", V_connection)
        self.costs_fixed = kwargs.get("costs_fixed", C_fix)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Waterpipelinepurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)

        
        costsPipelineArray = np.zeros(self.timesteps)
            
        self.costs_pipeline = kwargs.get("costs_water", costsPipelineArray)
        
        
    def component(self):
        v_waterpipeline = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_pipeline)
            
        waterpipeline = solph.Source(label=self.label, outputs={self.sector: v_waterpipeline})
        
        return waterpipeline     
    

```
</details>

## Demand

Water Demand given as timeseries, which can be given in the input data. The output of the demand is sewage which must be further treated/can be further used. Therefore, the demand is implemented as a transformer.  If the sewage should not be considered, the parameter `sewage` must be set to `False`. In that case, the water demand is implemented as a sink.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector Input | `sector_in` | / |
| Sector Output | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timestep` | 8760 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Waterdemand_ + position |
| Heat Demand | `demand` | Numpy Array with 0 |
| Share of Sewage in Water Demand | `share_sewage` | 0.95 |
| Sewage Consideration | `sewage` | True |

</details>

<details>
<summary>Code</summary>

```

class Waterdemand():
    def __init__(self, *args, **kwargs):
        share_waterSewage=0.95
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        sewage=True

        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Waterdemand_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.sewage = kwargs.get("sewage", sewage)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        if(self.sewage):
        
            v_sewageIn = solph.Flow(fix=self.demand, nominal_value=self.demandMax)
            v_sewageOut = solph.Flow(min=0, max=1, nominal_value=self.demandMax*self.share_sewage)
        
            #Conversion to sewage sector
            #Aggregation process to more consumers
            water2sewage = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageIn},
                    outputs={self.sector_out : v_sewageOut},
                    conversion_factors={self.sector_out : self.share_sewage})
        
            return water2sewage  
        
        else:
            v_waterIn = solph.Flow(fix=self.demand, nominal_value=self.demandMax)

            waterDemand = solph.Sink(label=self.label, inputs={self.sector_in: v_waterIn})
            
            return waterDemand

```
</details>

## Sewage Sink

Sink for the excess sewage. Should only be used if no other sewage treatment possibilities are possible and no disposal with costs can be implemented.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Disposal Volume| `V_max` | 1000000 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | SewageSink_ + position |

</details>

<details>
<summary>Code</summary>

```

class SewageSink():
    
    def __init__(self, *args, **kwargs):
        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        v_max = 1000000

        self.sector= kwargs.get("sector")
        self.v_max = kwargs.get("V_max", v_max)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Sewagesink_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.costs=0

        
        
    def component(self):
        v_sewage = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs)
            
        sewageSink = solph.Sink(label=self.label, inputs={self.sector: v_sewage})
        
        return sewageSink



```
</details>

## Sludge Sink

Sink for the excess sludge. Should only be used if no other sludge treatment possibilities are possible and no disposal with costs can be implemented.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Disposal Volume| `V_max` | 1000000 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | SludgeSink_ + position |

</details>

<details>
<summary>Code</summary>

```

class SludgeSink():
    
    def __init__(self, *args, **kwargs):
        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        v_max = 1000000

        self.sector= kwargs.get("sector")
        self.v_max = kwargs.get("V_max", v_max)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Sludgesink_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.costs=0

        
        
    def component(self):
        v_sewage = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs)
            
        sludgeSink = solph.Sink(label=self.label, inputs={self.sector: v_sewage})
        
        return sludgeSink


```
</details>

## Sewage Treatment

**Input: Sewage **

**Optional additional input: Electricity **

**Output: Clean Water, Sludge, Exhaust Heat**

**Optional Output: Emissions**

In the process of sewage treatment, clean water, sludge and exhaust heat are generated. The sectors, costs and conversion factors for the outputs are given as lists. A component transformer losses with multiple outputs is created.

`sector_out=[sector_out1, sector_out2, sector_out3]`

`c_out=[c_out1, c_out2, c_out3]`

`conversion_factor=[conversion_factor1, conversion_factor2, conversion_factor3]`

**It is important that output sectors and conversion factors are given to the component in the same order!** 

The default values are set in the order `[Water, Sludge, Heat]`.


<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.04 |
| Output costs | `c_out` | [0, 0, 0] |
| Electricity input costs | `c_loss` | 0 |
| Conversion factor | `conversion_factor` | [0.35, 0.44]|
| Electricity Input | `losses` | 0.5 |
| Maximum Volume | `V_max` | 1000000 |
| Temperature Difference in Water Treatment | `tempdif` | 3 |
| Share of Sewage in Water | `share_sewage` | 0.225 |
| Sludge Concentration | `concentration_sluge` | 9.2 |
| Sludge Density | `rho_sluge` | 1800 |
| Efficiency | `eta` | 0.9 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Sector for Input Electricity | `sector_loss` | 0 |
| Emissions Sector | `sector_emisisons` | 0 |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0.3 |
| Maximum emissions | `max_emissions` | 1000000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Sewagetreatment_ + position |

</details>

<details>
<summary>Code</summary>

```

class SewageTreatment():

        def __init__(self, *args, **kwargs):
            
            v_max = 1000000
            
            tempdif=3
            share_waterSewage=0.9
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000
            
            eta=0.95
            waterrecovery = 0.95
            
            Ct_sludge = 9.2
            rho_sludge = 1800
            
            el_loss = 0.5
            
            costsIn=0.04
            costsOut=[0, 0, 0]
            costsLoss = 0
            deltaT=1
            emissions=0.3
            maxEmissions=1000000000
            position='Consumer_1'
            
            self.tempdif = kwargs.get("tempdif", tempdif)
            self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.eta = kwargs.get("eta", eta)
            self.eta_water = kwargs.get("eta_water", waterrecovery)
            self.v_max = kwargs.get("V_max", v_max)
            self.losses = kwargs.get("losses", el_loss)
            
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_loss = kwargs.get("c_loss", costsLoss)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_in = kwargs.get("sector_in")
            self.sector_loss = kwargs.get("sector_loss")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Sewagetreatment_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Sewage\nTreatment_' + self.position
            self.color='black'
            
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            
            if(self.emissions==0 or self.sector_emissions==0):
                self.limit=[self.v_max, self.v_max*self.concentration_sludge/self.rho_sludge, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]
                
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]
                
            else:
                self.limit=[self.v_max, self.v_max*self.concentration_sludge/self.rho_sludge, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, self.max_emissions]
            
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, self.emissions]

        def component(self):
            v_sewageWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs_in)
            q_elecWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.losses, variable_costs=self.costs_loss)
            v_cleanWater = solph.Flow(min=0, max=1, nominal_value=self.limit[0], variable_costs=self.costs_out[0])
            v_sludgeSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[1], variable_costs=self.costs_out[1])
            q_heatSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[2], variable_costs=self.costs_out[2])

            if(self.emissions==0 or self.sector_emissions==0):
            
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            else:
                m_sewageTreatEmissions = solph.Flow(min=0, max=1, nominal_value=self.limit[3])
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat, self.sector_emissions : m_sewageTreatEmissions},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            
            return sewageTreatment
```
</details>

## Sewage to Hydrogen

Sewage to Hydrogen Component --> Simple Transformer

**Input: Sewage**

**Output: Hydrogen**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 14 |
| Conversion factor | `conversion_factor` | 0.5 |
| Maximum Flow | `Q_max` | 7 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | SewageH2_ + position |

</details>

<details>
<summary>Code</summary>

```

    class SewageHydrogen():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.5
            costsIn=0
            costsOut=14
            Qmax = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.Q_max = kwargs.get("Q_max", Qmax)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'SeawageH2_' + self.position
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.yearfactor = self.timesteps/timesteps



    def component(self):
            v_sewage2H2Water = solph.Flow(min=0, max=1, nominal_value=self.Q_max*self.deltaT/self.conversion_factor, variable_costs=self.costs_in)
            v_sewage2H2Hydrogen = solph.Flow(min=0, max=1, nominal_value=self.Q_max*self.deltaT, variable_costs=self.yearfactor*self.costs_out)
            
            sewage2H2 = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewage2H2Water},
                outputs={self.sector_out : v_sewage2H2Hydrogen},
                conversion_factors={self.sector_out : self.conversion_factor})
            
            return sewage2H2
        
 


```
</details>

## Sludge Storage

**Input: Sludge (from Treatment)**

**Output: Sludge to Treatment**

Wastestorage Component --> 
Sludge storages are can be emptied at certain timeperiods, which is implemented in the model with a new block [StorageIntermediate](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/StorageIntermediate.md). The disposal periods can be set with a parameter `disposal_periods`. 

**If the parameter is set to zero, a simple GenericStorage is used, where the storage can be emptied in all periods.**



<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.01 |
| Output costs | `c_out` | 0.01 |
| Input Efficiency | `eta_in` | 1 |
| Output Efficiency | `eta_out` | 1 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Maximum Volume | `volume_max` | 1.5 |
| Start Volume | `volume_start` | 1 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum Flow | `Q_max` | 1.5 |
| Timesteps after which the storage is emptied | `disposal_periods` | 8760 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Sludgestorage_ + position |
| Battery Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class SludgeStorage():
    
    def __init__(self, *args, **kwargs):
        V_max = 1.5
        V_start = 0
        Q_max = 1.5
        costsIn = 0.01
        costsOut=0.01
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        disposal_periods=0
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'Sludgestorage_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        self.balanced = kwargs.get("balanced", True)
        
        
    def component(self):
        #Energy Flows
        v_waterstoreIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_waterstoreOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
        
        if(self.disposal_periods==0):
            sludgeStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
            
            return sludgeStorage
        
        else:
            sludgeStorage = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps)
        
            return sludgeStorage    
 
    
```
</details>

## Water Storage

**Input: Water**

**Output: Water**

Implemented as GenericStorage

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.01 |
| Output costs | `c_out` | 0.01 |
| Input Efficiency | `eta_in` | 1 |
| Output Efficiency | `eta_out` | 1 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Maximum Volume | `volume_max` | 1.5 |
| Start Volume | `volume_start` | 0 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum Flow | `Q_max` | 1.5 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Waterstorage + position |
| Battery Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class WaterStorage():
    
    def __init__(self, *args, **kwargs):
        V_max = 1.5
        V_start = 0
        Q_max = 1.5
        costsIn = 0.01
        costsOut=0.01
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'Waterstorage_' + self.position
        self.balanced = kwargs.get("balanced", True)
        
        
    def component(self):
        #Energy Flows
        v_waterstoreIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_waterstoreOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
            
        waterStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return waterStorage     
    
    
```
</details>

## Sludge Combustion

**Input: Sludge**

**Output: Electricity, Heat**

**Optional Output: Emissions**

Sludge combustion is a special case, as this technology has two outputs. The output sectors, output costs and conversion factors are given to the component as list and are called with their indices. 

`sector_out=[sector_out1, sector_out2]`

`c_out=[c_out1, c_out2]`

`conversion_factor=[conversion_factor1, conversion_factor2]`

**It is important that output sectors and conversion factors are given to the component in the same order!** 

The default values are set in the order `[Electricity, Heat]`.


<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | [0.06, 0.06] |
| Conversion factor | `conversion_factor` | [0.35, 0.44]|
| Input Power Limit | `P_in` | 100 |
| Output Power Limit | `P_out` | 100 |
| Heating Value Sludge | `H_sludge` | 2.78 |
| Sludge Concentration | `concentration_sluge` | 9.2 |
| Sludge Density | `rho_sluge` | 1800 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Emissions Sector | `sector_emissions` | 0 |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 50 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Sludgecombustion_ + position |

</details>

<details>
<summary>Code</summary>

```

class Sludgecombustion():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.06, 0.06]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=50
            maxEmissions=1000000
            position='Consumer_1'
            
            #Sludge Parameters
            Ct_sludge = 9.2
            rho_sludge = 1800
            H_sludge = 10/3.6
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Sludgecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Sludge\nCombustion_' + self.position
            self.color='green'
            

            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.H_sludge= kwargs.get("H_sludge", H_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000

        def component(self):
            v_sewageSludgeComb = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_sludge*self.rho_sludge), variable_costs=self.costs_in*self.rho_sludge)
            q_SludgeCombEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_SludgeCombTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])


            if(self.emissions==0 or self.sector_emissions==0):
            
                sludgeCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageSludgeComb},
                    outputs={self.sector_out[0] : q_SludgeCombEl, self.sector_out[1] : q_SludgeCombTh},
                    conversion_factors={self.sector_out[0] : self.H_sludge*self.rho_sludge*self.conversion_factor[0], self.sector_out[1] : self.H_sludge*self.rho_sludge*self.conversion_factor[1]})
            else:
                m_sludgecombustionemissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                sludgeCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageSludgeComb},
                    outputs={self.sector_out[0] : q_SludgeCombEl, self.sector_out[1] : q_SludgeCombTh, self.sector_emissions : m_sludgecombustionemissions},
                    conversion_factors={self.sector_out[0] : self.H_sludge*self.rho_sludge*self.conversion_factor[0], self.sector_out[1] : self.H_sludge*self.rho_sludge*self.conversion_factor[1], self.sector_emissions : self.emissions})
            
            
            
            return sludgeCombustion
```
</details>

## MFC

Microbial Fuel Cell --> Simple Transformer

**Input: Sludge**

**Output: Electricity**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.043 |
| Output costs | `c_out` | 0 |
| Conversion factor | `conversion_factor` | 0.073 |
| Maximum Volume | `V_max` | 20 |
| Sludge Density | `rho_sluge` | 1800 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | MFC_ + position |

</details>

<details>
<summary>Code</summary>

```

    class MFC():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.073
            costsIn=0.043
            costsOut=0
            V_max = 20
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            rho_sludge = 1800
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.V_max = kwargs.get("V_max", V_max)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'MFC_' + self.position
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)




    def component(self):
            v_sewageSludgeMFC = solph.Flow(min=0, max=1, nominal_value=self.V_max, variable_costs=self.costs_in*self.rho_sludge)
            q_sewageSludgeMFCel = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.conversion_factor/self.deltaT)

            sludgeMFC = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageSludgeMFC},
                outputs={self.sector_out : q_sewageSludgeMFCel},
                conversion_factors={self.sector_out : self.conversion_factor})
            
            return sludgeMFC
        
 


```
</details>


## Sludge Anaerobic Digestion

Sludge Anaerobic Digestion Block --> Simple Transformer

**Input: Sludge**

**Output: Gas**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.068 |
| Output costs | `c_out` | 0 |
| Conversion factor | `conversion_factor` | 0.5 |
| Maximum Input Power | `P_in` | 100 |
| Maximum Output Power | `P_out` | 100 |
| Sludge Density | `rho_sluge` | 1800 |
| Gas Heating Value | `H_gas` | 11.42 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | SludgeAD_ + position |

</details>

<details>
<summary>Code</summary>

```

    class SludgeAD():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.5
            costsIn=0.068
            costsOut=0
            P_in = 100
            P_out=100
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            rho_sludge = 1800
            
            #Heating Value Gas in kWh/m³
            H_gas = 11.42
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.P_in = kwargs.get("P_in", P_in)
            self.P_out = kwargs.get("P_Out", P_out)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'SludgeAD_' + self.position
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.H_gas = kwargs.get("H_gas", H_gas)
             



    def component(self):
            v_sewageSludgeAD = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_gas*self.rho_sludge*self.conversion_factor), variable_costs=self.rho_sludge*self.costs_in)
            q_sewageSludgeADGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)



            
            sludgeAD = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageSludgeAD},
                outputs={self.sector_out : q_sewageSludgeADGas},
                conversion_factors={self.sector_out : self.H_gas*self.rho_sludge*self.conversion_factor})
            
            return sludgeAD        
        
 


```
</details>

## Sludge Anaerobic to Hydrogen

Sludge Anaerobic Digestion Block --> Simple Transformer

**Input: Sludge**

**Output: Hydrogen**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.068 |
| Output costs | `c_out` | 0 |
| Conversion factor | `conversion_factor` | 0.5 |
| Mass Limit | `M_max` | 145 |
| Sludge Density | `rho_sluge` | 1800 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | SludgeADH2_ + position |

</details>

<details>
<summary>Code</summary>

```

class SludgeADH2():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.5
            costsIn=0.068
            costsOut=0
            MLimit=145
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            rho_sludge = 1800
            
            
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.M_max = kwargs.get("M_max", MLimit)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'SludgeADH2_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Sludge\nADH2_' + self.position
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.color='sandybrown'
             



    def component(self):
            v_sewageSludgeAD = solph.Flow(min=0, max=1, nominal_value=self.M_max/self.rho_sludge, variable_costs=self.rho_sludge*self.costs_in)
            q_sewageSludgeADH2 = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.conversion_factor, variable_costs=self.costs_out)



            
            sludgeADH2 = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageSludgeAD},
                outputs={self.sector_out : q_sewageSludgeADH2},
                conversion_factors={self.sector_out : self.rho_sludge*self.conversion_factor})
            
            return sludgeADH2      
        
 


```
</details>

## Sludge Disposal

Sink for the sludge disposal with disposal costs. Optional emissions for disposal.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Disposal Volume| `volume_max` | 1000000 |
| Density Sludge | `rho_sludge` | 1800 |
| Share Sewage | `share_sewage` | 0.95 |
| Concentration Sludge | `concentration_sludge` | 9.2 |
| Disposal Costs | `costs` | Numpy Array with 0 |
| Sector | `sector` | / |
| Emissions Sector | `sector_emissions` | 0 |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 1457 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Sludgedisposal_ + position |

</details>

<details>
<summary>Code</summary>

```

class Sludgedisposal():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000
        share_waterSewage=0.95
        Ct_sludge = 9.2
        rho_sludge = 1800
        emissions=1457
        maxEmissions=1000000
        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
        self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sludgedisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Sludge\nDisposal_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.color='peru'
        
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.max_emissions=kwargs.get("max_emissions", maxEmissions)
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        
        
    def component(self):

          
        v_sludgeWasted = solph.Flow(nominal_value=self.V_max*self.concentration_sludge/self.rho_sludge, variable_costs=self.costs_in)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            sludgeWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_sludgeWasted})
            
        else:
            m_sludgewasteEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
            
            sludgeWasted = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sludgeWasted},
                    outputs={self.sector_emissions : m_sludgewasteEmissions},
                    conversion_factors={self.sector_emissions : self.emissions})
        

        
        return sludgeWasted
    
 




```
</details>

## Sewage Disposal

Sink for the sewage disposal with disposal costs. Optional additional outputs for emissions.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Maximum Disposal Volume| `volume_max` | 1000000 |
| Share Sewage | `share_sewage` | 0.95 |
| Disposal Costs | `costs` | Numpy Array with 0 |
| Sector | `sector` | / |
| Emissions Sector | `sector_emissions` | 0 |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0.3 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Sewagedisposal_ + position |

</details>

<details>
<summary>Code</summary>

```

class Sewagedisposal():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000
        share_waterSewage=0.95
        emissions=0.3
        maxEmissions=1000000
        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sewagedisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Sewage\nDisposal_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.color='brown'
        
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.max_emissions=kwargs.get("max_emissions", maxEmissions)
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        
        
    def component(self):


                
        v_sewageWasted = solph.Flow(nominal_value=self.V_max, variable_costs=self.costs_in)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            sewageWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_sewageWasted})
        
        else:
            m_sewagewasteEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
            
            sewageWasted = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWasted},
                    outputs={self.sector_emissions : m_sewagewasteEmissions},
                    conversion_factors={self.sector_emissions : self.emissions})
        
        return sewageWasted
    
 




```
</details>


## Water Demand Reuse

Water Demand Component if Greywater Output is considered --> Demand output as a variable
Sector in  water sector
Sector Out Sewage
Sector Greywater Greywater
Sector waterdemand is waterdemandsector
Penalty Costs if the use of potable water is financially punished at certain timesteps

**Input: Potable Water**

**Output: Sewage and Greywater**

**Useful Output: Waterdemand Sector**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.068 |
| Output costs | `c_out` | 0 |
| Conversion factor | `conversion_factor` | 0.5 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Greywater Sector | `sector_greywater` | / |
| Waterdemand Sector | `sector_waterdemand` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Share of sewage in water | `share_sewage` | 0.95 |
| Considered share of Greywater in Sewage | `share_greywater` | 0 |
| Effective use of greywater | `eta_greywater` | 1 |
| Penalty Costs for not used greywater | `penalty_costs` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Potablewaterdemand_ + position |

</details>

<details>
<summary>Code</summary>

```

class WaterdemandReuse():
    def __init__(self, *args, **kwargs):
        share_waterSewage=0.95
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        eta_greywater = 1
        share_greywater = 0
        penaltyCosts = 0
 

        self.sector_in= kwargs.get("sector_in")
        self.sector_out1= kwargs.get("sector_out")
        self.sector_greywater= kwargs.get("sector_greywater")
        self.sector_waterdemand= kwargs.get("sector_waterdemand")
        self.sector_out=[self.sector_out1, self.sector_greywater, self.sector_waterdemand]
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Potablewaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Water\nDemand_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        self.share_greywater = kwargs.get("share_greywater", share_greywater)
        self.eta_greywater = kwargs.get("eta_greywater", eta_greywater)
        self.penalty_costs = kwargs.get("penalty_costs", penaltyCosts)
        self.costs_out=[ 0, 0, self.penalty_costs]
        self.color='yellow'

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        sewageOutMax = np.multiply(self.demand, self.share_sewage)
        greywaterOutMax = np.multiply(self.demand, self.share_sewage*self.share_greywater)
        
        v_waterdemandIn = solph.Flow(min=0, max=self.demand/self.demandMax, nominal_value=self.demandMax)
        v_sewageOut = solph.Flow(min=0, max=sewageOutMax/max(sewageOutMax), nominal_value=max(sewageOutMax), variableCosts=self.costs_out[0])
        v_greywater = solph.Flow(min=0, max=greywaterOutMax/max(greywaterOutMax), nominal_value=max(greywaterOutMax), variableCosts=self.costs_out[1])
        v_waterdemandOut = solph.Flow(min=0, ax=self.demand/self.demandMax, nominal_value=self.demandMax, variableCosts=self.costs_out[2])
        
        #Conversion to sewage sector
        #Aggregation process to more consumers
        water2sewage = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterdemandIn},
            outputs={self.sector_out1 : v_sewageOut, self.sector_greywater : v_greywater, self.sector_waterdemand : v_waterdemandOut},
            conversion_factors={self.sector_out1 : self.share_sewage*(1-self.share_greywater),
                                self.sector_greywater : self.share_sewage*self.share_greywater*self.eta_greywater,
                                self.sector_waterdemand : 1})
        
        return water2sewage  
        
        
 


```
</details>

## Total Water Demand Sink

Sink for the total water demand. The given demand of water must be set as a timeseries. Demand can be covered by potable water and greywater. 

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Waterdemand | `demand` | Numpy Array with Zeros |
| Label | `label` | Totalwaterdemand_ + position |

</details>

<details>
<summary>Code</summary>

```

class WaterDemandPotableGreySink():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760


        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Totalwaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Water\nDemand_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.color='yellow'

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        
        v_waterIn = solph.Flow(fix=self.demand, nominal_value=1)

        waterDemand = solph.Sink(label=self.label, inputs={self.sector_in: v_waterIn})
            
        return waterDemand
        
        
 


```
</details>

## Greywater to Waterdemand

Block for Greywater Contribution to total water demand

**Input: Greywater**

**Output: Waterdemand**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Maximum of Greywater to Waterdemand | `demand_share` | 0.5 |
| Minimum of Greywater to Waterdemand | `min_greywater` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Greywaterdemand_ + position |

</details>

<details>
<summary>Code</summary>

```

class Greywaterdemand():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        demand_share = 0.5
        minimumGreywater=0


        self.sector_in= kwargs.get("sector_in")
        self.sector_out=kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Greywaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Greywater\nDemand_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.color='lightgreen'
        self.demand_share=kwargs.get("demand_share", demand_share)
        self.min_greywater=kwargs.get("min_greywater", minimumGreywater)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        maxFlowsWater = np.multiply(self.demand, self.demand_share)
        minimumOfGreywater = np.multiply(self.demand, self.min_greywater)
        
        v_waterIn = solph.Flow(min=0, max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))
        v_waterOut = solph.Flow(min=minimumOfGreywater/max(maxFlowsWater), max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))

        waterDemand = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterIn},
            outputs={self.sector_out : v_waterOut},
            conversion_factors={self.sector_out :1})
            
        return waterDemand
        
 


```
</details>


## Greywater to Waterdemand inclusive Sewage

Block for Greywater Contribution to total water demand, with sewage from greywater use

**Input: Greywater**

**Output: Waterdemand, Sewage**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Maximum of Greywater to Waterdemand | `demand_share` | 0.5 |
| Minimum of Greywater to Waterdemand | `min_greywater` | 0 |
| Share of sewage in greywater| `sewage_share` | 0.5 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Greywaterdemand_ + position |

</details>

<details>
<summary>Code</summary>

```

class Greywaterdemand2Sewage():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        demand_share = 0.5
        minimumGreywater=0
        sewageShar = 0.5


        self.sector_in= kwargs.get("sector_in")
        self.sector_out=kwargs.get("sector_out")
        self.sector_sewage=kwargs.get("sector_sewage")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Greywaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Greywater\nDemand_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.color='lightgreen'
        self.demand_share=kwargs.get("demand_share", demand_share)
        self.min_greywater=kwargs.get("min_greywater", minimumGreywater)
        self.sewage_share = kwargs.get("sewage_share", sewageShar)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        maxFlowsWater = np.multiply(self.demand, self.demand_share)
        minimumOfGreywater = np.multiply(self.demand, self.min_greywater)
        
        v_waterIn = solph.Flow(min=0, max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))
        v_waterOut = solph.Flow(min=minimumOfGreywater/max(maxFlowsWater), max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))
        v_sewageOut = solph.Flow(min=minimumOfGreywater/max(maxFlowsWater), max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))

        waterDemand = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterIn},
            outputs={self.sector_out : v_waterOut, self.sector_sewage : v_sewageOut},
            conversion_factors={self.sector_out :1, self.sector_sewage : self.sewage_share})
            
        return waterDemand
        
 


```
</details>

## Greywater not used

Not used greywater --> to Sewage

**Input: Greywater**

**Output: Sewage**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | GreywaterSewage_ + position |

</details>

<details>
<summary>Code</summary>

```

class Greywatersewage():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760


        self.sector_in= kwargs.get("sector_in")
        self.sector_out=kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'GreywaterSewage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Greywater\nSewage_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.color='crimson'


        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        
        v_waterIn = solph.Flow(min=0, max=1, nominal_value=self.demandMax)
        v_waterOut = solph.Flow(min=0, max=1, nominal_value=self.demandMax)

        waterDemand = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterIn},
            outputs={self.sector_out : v_waterOut},
            conversion_factors={self.sector_out :1})
            
        return waterDemand
        
 


```
</details>

## Greywater Storage

**Input: Greywater**

**Output: Greywater**

Implemented as GenericStorage. It should be considered that greywater can only be stored for a certain time period (usually a day) --> Storage with fixed disposal periods.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.01 |
| Output costs | `c_out` | 0.01 |
| Input Efficiency | `eta_in` | 1 |
| Output Efficiency | `eta_out` | 1 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Maximum Volume | `volume_max` | 1.5 |
| Start Volume | `volume_start` | 0 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum Flow | `Q_max` | 1.5 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Greywaterstorage + position |
| Battery Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class GreywaterStorage():
    
    def __init__(self, *args, **kwargs):
        V_max = 1.5
        V_start = 0
        Q_max = 1.5
        costsIn = 0.01
        costsOut=0.01
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        disposal_periods=24
        timesteps=8760
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Greywaterstorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Greywater\nStorage_' + self.position
        self.balanced = kwargs.get("balanced", True)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        self.timesteps=kwargs.get("timesteps", timesteps)
        self.color='red'
        
        
    def component(self):
        #Energy Flows
        v_waterstoreIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_waterstoreOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
            
        if(self.disposal_periods==0):
        
            waterStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
            return waterStorage     
        else:
        
            waterStorage = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps)
        
            return waterStorage
    
    
```
</details>

## Seawater Source

Sewater Source for Water Desalination.

**Output: Seawater**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Output Sector | `sector` | / |
| Maximum processable volume | `V` | 1000 |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Costs | `costs` | 0 |
| Label | `label` | Seawatersource_ + position |

</details>

<details>
<summary>Code</summary>

```

class Seawatersource():
    
    def __init__(self, *args, **kwargs):
        V_seawater = 1000
        deltaT=1
        position='Consumer_1'
        costs=0
        
        self.V= kwargs.get("V", V_seawater)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Seawatersource' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Seawater\nSource_' + self.position
        self.costs_out=kwargs.get("costs", costs)
        self.color = 'aqua'
        
        
    def component(self):
        v_seawater = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_out)
            
        seawater = solph.Source(label=self.label, outputs={self.sector_out: v_seawater})
        
        return seawater 
        
 


```
</details>

## Desalination

Seawater Desalination for potable water

**Input: Seawater**

**Additional Input: Electricity or Heat**

**Output: Potable Water**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input Sector | `sector_in` | / |
| Additional Input Sector | `sector_loss` | / |
| Output Sector | `sector_out` | / |
| Maximum processable volume | `V` | 1000 |
| Conversion Efficiency | `efficiency` | 1 |
| Energy Use | `energy` | 3 |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Costs Input | `costs_in` | 0 |
| Costs Output | `costs_out` | 0.44 |
| Label | `label` | Seawatersource_ + position |

</details>

<details>
<summary>Code</summary>

```

class Desalination():
    
    def __init__(self, *args, **kwargs):
        V_desalination = 1
        K_desalination=3
        C_desalination_in = 0
        C_desalination_out = 0.44
        eta_desalination = 1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.V = kwargs.get("V", V_desalination)
        self.eta = kwargs.get("efficiency", eta_desalination)
        self.energy = kwargs.get("energy", K_desalination)
        self.costs_in = kwargs.get("c_in", C_desalination_in)
        self.costs_out = kwargs.get("c_out", C_desalination_out)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.sector_loss = kwargs.get("sector_loss")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Desalination_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Desalination_' + self.position
        self.P = self.V*self.energy/self.deltaT
        self.color='olivedrab'
        
    def component(self):
        v_seawater = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs = self.costs_in)
        v_potablewater = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_out)
            
  
        q_energy = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT)
            
            
            
        desalination = tlmo.TransformerLossesMultipleOut(
            label=self.label,
            inputs={self.sector_in : v_seawater, self.sector_loss : q_energy},
            outputs={self.sector_out : v_potablewater},
            conversion_sector = self.sector_in, losses_sector=self.sector_loss,
            conversion_factor=[self.eta], losses_factor=self.energy)

        
        return desalination
        
 


```
</details>


## Rainwater Collection

Rainwater timeseries per m² must be set as input paramters. The total rooftop area is an additional parameter, to calculate the total collected water by multiplication.

**Output: Rainwater**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Output Sector | `sector` | / |
| Rainwater Input | `generation_timeseries` | Numpy array with zero |
| Timestep | `timerange` | 1 |
| Emissions | `emisisons` | 0 |
| Area Roof | `area_roof` | 100 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Costs | `c_out` | 0 |
| Label | `label` | Rainwatercollection_ + position |

</details>

<details>
<summary>Code</summary>

```

class Rainwatercollect():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsOut=0
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            AreaRoof = 100
            

            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_out = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.Area_Roof = kwargs.get("area_roof", AreaRoof)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Rainwatercollection_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Rainwater_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.color='gold'
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("generation_timeseries", generationArray)
            self.conversion_timeseries = np.multiply(self.conversion_timeseries, self.Area_Roof/1000)

        def component(self):
            
            v_rainwater = solph.Flow(fix = self.conversion_timeseries, nominal_value=1)
            
            rainwater = solph.Source(label=self.label, outputs={self.sector_out: v_rainwater})
            
            return rainwater
        
 


```
</details>

## Greywater Storage

**Input: Greywater**

**Output: Greywater**

Implemented as GenericStorage. It should be considered that greywater can only be stored for a certain time period (usually a day) --> Storage with fixed disposal periods.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.01 |
| Output costs | `c_out` | 0.01 |
| Input Efficiency | `eta_in` | 0.9 |
| Output Efficiency | `eta_out` | 1 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Maximum Volume | `volume_max` | 4.1 |
| Start Volume | `volume_start` | 0 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum Flow | `Q_max` | 1.5 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Rainwatertank + position |
| Battery Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class Rainwatertank():
    
    def __init__(self, *args, **kwargs):
        V_max = 4.1
        V_start = 0
        Q_max = 10
        costsIn = 0
        costsOut=0
        Eta_in = 0.9
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Rainwatertank' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Rainwater\Tank_' + self.position
        self.balanced = kwargs.get("balanced", True)
        self.color='chocolate'
        
        
    def component(self):
        #Energy Flows
        v_rainwaterIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_rainwaterOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
            
        rainwaterTank = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_rainwaterIn},
                outputs={self.sector_out: v_rainwaterOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return rainwaterTank  
    
    
    
```
</details>

## Rainwater Disposal

Not used rainwater from rainwater storage tank.

**Input in Source: Rainwater**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Maximum volume | `V_max` | 1000000 |
| Timestep | `timerange` | 1 |
| Emissions | `emisisons` | 0 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Costs | `costs` | 0 |
| Label | `label` | Rainwatercollection_ + position |

</details>

<details>
<summary>Code</summary>

```

class Rainwaterdisposal():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000
        emissions=0

        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Rainwaterdisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Rainwater\nDisposal_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.color='black'
        self.emissions=kwargs.get("emissions", emissions)

        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        
        
    def component(self):

          
        v_rainwaterWaste = solph.Flow(nominal_value=self.V_max, variable_costs=self.costs_in)
        
        
            
        rainwaterWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_rainwaterWaste})
            

        

        
        return rainwaterWasted
        
 


```
</details>

## Rainwater Pump

Pump for rainwater to greywater --> electricity for pumps needed

**Input: Rainwater**

**Additional Input: Electricity**

**Output: Greywater**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector Input | `sector_in` | / |
| Sector Output | `sector_out` | / |
| Sector Loss | `sector_loss` | / |
| Maximum power of pump | `P_max` | 1.3 |
| Maximum flow of pump | `Q_max` | 5 |
| Maximum hight of pump | `h_max` | 4 |
| Efficiency | `eta` | 0.5 |
| Timestep | `timerange` | 1 |
| Total Timesteps | `timesteps` | 8760 |
| Component Location | `position` | Consumer_1 |
| Costs Input | `c_in` | 0.02 |
| Costs Output | `c_out` | 0 |
| Label | `label` | Rainwaterpump_ + position |

</details>

<details>
<summary>Code</summary>

```

class Rainwaterpump():
    
    def __init__(self, *args, **kwargs):
        P = 1.3
        Q = 5
        eta = 0.5
        h = 4
        deltaT=1
        costsIn=0.02
        costsOut=0
        position='Consumer_1'
        rho_H2O = 1000
        g = 9.81
        
        self.P= kwargs.get("P_max", P)
        self.Q= kwargs.get("Q_max", Q)
        self.h= kwargs.get("h_max", h)
        self.eta= kwargs.get("eta", eta)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.sector_loss= kwargs.get("sector_loss")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Rainwaterpump_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Rainwaterpump' + self.position
        self.costs_in=kwargs.get("c_in", costsIn)
        self.costs_out=kwargs.get("c_out", costsOut)
        self.conversionFactor = rho_H2O*g*self.h/self.eta
        self.color='lemonchiffon'
        
    def component(self):
        q_rainwaterpumpEl = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=0)
        v_rainwaterpumpRainwater = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_in)
        v_rainwaterpumpGreywater = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_out)
            
        
        rainwaterpump = tlmo.TransformerLossesMultipleOut(
            label=self.label,
            inputs={self.sector_in : v_rainwaterpumpRainwater, self.sector_loss : q_rainwaterpumpEl},
            outputs={self.sector_out : v_rainwaterpumpGreywater},
            conversion_sector = self.sector_in, losses_sector=self.sector_loss,
            conversion_factor=[1], losses_factor=self.conversionFactor/3600000)
        
        return rainwaterpump        

        
 


```
</details>


