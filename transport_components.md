# Transport Components

File with all Components, that can be used to cover the transport demand. Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import transport_components as trans_comp`

Call of the components:

```
componentObject = trans_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`fuelcellVehicle=trans_comp.FuelcellVehicle(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getFuelcellVehicleSector(), sector_drive=consumer1.getTransportSector(), timesteps=timesteps)`


## Methods
All transport components are implemented as storage, that represents their tank or battery state. The components of the transport sector have additional methods compared to the other sectors. A method `sink()` creates a sink as output for the storages. A sink is defined if the vehicle is only considered as a load, without the aim of covering a specific transport demand. If the latter is done, the method `dive()` is implemented, where the storage output is transformed to a driving distance with the corresponding driving consumption. Additionally the [binary balance](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/transportDecision.md) for the transport sector is recommended to have a proper transport demand coverage.


## Demand

Transport Demand given as timeseries, which can be given in the input data. The distance to drive in each hour is given.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Mobilitydemand_ + position |
| Electricity Demand | `demand_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'Mobilitydemand_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            s_transportDemand = solph.Flow(fix = self.demand_timeseries, nominal_value=1)
            
            transportDemand = solph.Sink(label=self.label, inputs={self.sector: s_transportDemand})
            
            return transportDemand    

```
</details>


## Electric Vehicle
The parts of the electric vehicle are separated into battery (as `GenericStorage`), a potential sink for modelling purposes (as `Sink`) and a component for the tranformation into the driving distance (as `Transformer`.)

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Charging Efficiency | `eta_charge` | 0.95 |
| Driving Efficiency | `eta_drive` | 0.9 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0 |
| Charging Power | `P_charge` | 11 |
| Driving Power | `P_drive` | 110 |
| Driving Consumption | `consumption` | 0.194 |
| Start SOC Battery | `soc_start` | 40 |
| Minmum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum SOC | `soc_max` | 40 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Driving/Transport Sector | `sector_drive` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | ElectricVehicle_ + position |
| Label Sink Component | `labelSink` | ElectricVehicleSink_ + position |
| Label Drive Component | `labelDrive` | ElectricVehicleDrive_ + position |
| Storage Balance | `balanced` | `True` |
| Charging Power Limitation | `charge_limit` | 1 |
| Driving Power Limitation | `drive_limit` | 1 |

</details>

<details>
<summary>Code</summary>

```

class ElectricVehicle():
    
    def __init__(self, *args, **kwargs):
        P_charge = 11
        P_drive = 110
        costsIn = 0
        costsOut=0
        Eta_charge = 0.95
        Eta_drive = 0.9
        Eta_sb = 0.999
        SOC_max = 40
        SOC_start = 40
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        vehicle_consumption=0.194
        chargeLimit = 1
        driveLimit = 1
        timesteps=8760
        
        
        #Default Values for Nissan Leaf Standard Edition
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_charge = kwargs.get("P_charge", P_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'ElectricVehicle_' + self.position
        self.labelDrive = 'ElectricVehicleDrive_' + self.position
        self.labelSink = 'ElectricVehicleSink_' + self.position
        self.balanced = kwargs.get("balanced", True)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.sinkCosts=0
        
    def component(self):
        #Energy Flows
        q_evCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.P_charge*self.deltaT)
        q_evDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT)
            
        electricVehicleStore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_evCharge},
                outputs={self.sector_out: q_evDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return electricVehicleStore
    
    def sink(self):
        q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.sinkCosts)
            
        vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
        return vehicle_sink
    
    def drive(self):
        q_evToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT)
        s_EV = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)


        evDrive = solph.Transformer(
            label=self.labelDrive,
            inputs={self.sector_out : q_evToMotor},
            outputs={self.sector_drive : s_EV},
            conversion_factors={self.sector_drive : 1/self.consumption})
        
        return evDrive

```
</details>


## Biofuel Vehicle
The parts of the biofuel vehicle are separated into tank (as `GenericStorage`), a potential sink for modelling purposes (as `Sink`) and a component for the tranformation into the driving distance (as `Transformer`.)

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Charging Efficiency | `eta_charge` | 1 |
| Driving Efficiency | `eta_drive` | 0.35 |
| Standby Efficiency | `eta_sb` | 1 |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0 |
| Charging Flow | `Q_charge` | 4000 |
| Driving Power | `P_drive` | 110 |
| Driving Consumption | `consumption` | 0.000044 |
| Start Tank Volume | `soc_start` | 0.05 |
| Minmum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum SOC | `soc_max` | 0.05 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Driving/Transport Sector | `sector_drive` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | BiofuelVehicle_ + position |
| Label Sink Component | `labelSink` | BiofuelVehicleSink_ + position |
| Label Drive Component | `labelDrive` | BiofuelVehicleDrive_ + position |
| Storage Balance | `balanced` | `True` |
| Charging Power Limitation | `charge_limit` | 1 |
| Driving Power Limitation | `drive_limit` | 1 |
| Heating Value Diesel | `H_diesel` | 10400 |

</details>

<details>
<summary>Code</summary>

```

class BiofuelVehicle():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        P_drive = 110
        costsIn = 0
        costsOut=0
        Eta_charge = 1
        Eta_drive = 0.35
        Eta_sb = 1
        SOC_max = 0.05
        SOC_start = 0.05
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        vehicle_consumption=0.000044
        chargeLimit = 1
        driveLimit = 1
        #Heating Value Diesel in kWh/m³
        H_diesel = 10400
        timesteps=8760
        self.costsSink=0
        
        #Default Values for VW Golf 2 TDI DSG Style
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.Q_charge = kwargs.get("Q_charge", Q_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("soc_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'BiofuelVehicle_' + self.position
        self.labelDrive = 'BiofuelVehicleDrive_' + self.position
        self.labelSink = 'BiofuelVehicleSink_' + self.position
        self.balanced = kwargs.get("balanced", True)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.H_diesel = kwargs.get("H_diesel", H_diesel)
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        
        
    def component(self):
        #Energy Flows
        v_biovCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.Q_charge*self.deltaT)
        v_biovDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
            
        biofuelVehicle = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_biovCharge},
                outputs={self.sector_out: v_biovDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return biofuelVehicle
    
    def sink(self):
        q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.costsSink)
            
        vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
        return vehicle_sink
    
    def drive(self):
        v_biovToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
        s_Biov = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)

        biovDrive = solph.Transformer(
            label=self.labelDrive,
            inputs={self.sector_out : v_biovToMotor},
            outputs={self.sector_drive : s_Biov},
            conversion_factors={self.sector_drive : 1/self.consumption})
        
        return biovDrive
    

```
</details>

## Fuelcell Vehicle
The parts of the fuelcell vehicle are separated into tank (as `GenericStorage`), a potential sink for modelling purposes (as `Sink`) and a component for the tranformation into the driving distance (as `Transformer`.)

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Charging Efficiency | `eta_charge` | 1 |
| Driving Efficiency | `eta_drive` | 0.6 |
| Standby Efficiency | `eta_sb` | 1 |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0 |
| Charging Flow | `Q_charge` | 4000 |
| Driving Power | `P_drive` | 120 |
| Driving Consumption | `consumption` | 0.095 |
| Start Tank Volume | `soc_start` | 0.158 |
| Minmum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum SOC | `soc_max` | 0.158 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Driving/Transport Sector | `sector_drive` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | FuelcellVehicle_ + position |
| Label Sink Component | `labelSink` | FuelcellVehicleSink_ + position |
| Label Drive Component | `labelDrive` | FuelcellVehicleDrive_ + position |
| Storage Balance | `balanced` | `True` |
| Charging Power Limitation | `charge_limit` | 1 |
| Driving Power Limitation | `drive_limit` | 1 |
| Heating Value Hydrogen | `H_hydrogen` | 3 |

</details>

<details>
<summary>Code</summary>

```

class FuelcellVehicle():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        P_drive = 120
        costsIn = 0
        costsOut=0
        Eta_charge = 1
        Eta_drive = 0.6
        Eta_sb = 1
        SOC_max = 0.158
        SOC_start = 0.158
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        position='Consumer_1'
        vehicle_consumption=0.095
        chargeLimit = 1
        driveLimit = 1
        #Heating Value Diesel in kWh/m³
        H_hydrogen = 3
        
        #Default Values for Hyundai Nexo
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.Q_charge = kwargs.get("Q_charge", Q_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'FuelcellVehicle_' + self.position
        self.labelDrive = 'FuelcellVehicleDrive_' + self.position
        self.labelSink = 'FuelcellVehicleSink_' + self.position
        self.balanced = kwargs.get("balanced", True)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
        self.costsSink=0
        
        
        
    def component(self):
        v_fcCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.Q_charge*self.deltaT)
        v_fcDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_hydrogen)
            
        fcVehicle = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_fcCharge},
                outputs={self.sector_out: v_fcDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return fcVehicle
    
    def sink(self):
       q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.costsSink)
            
       vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
       return vehicle_sink 
   
    def drive(self):
         v_fcToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_hydrogen)
         s_fc = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)
         


         fcDrive = solph.Transformer(
                label=self.labelDrive,
                inputs={self.sector_out : v_fcToMotor},
                outputs={self.sector_drive : s_fc},
                conversion_factors={self.sector_drive : 1/self.consumption})
         
         return fcDrive
    
```
</details>

## Petrol Vehicle
The parts of the petrol vehicle are separated into tank (as `GenericStorage`), a potential sink for modelling purposes (as `Sink`) and a component for the tranformation into the driving distance (as `Transformer`.)

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Charging Efficiency | `eta_charge` | 1 |
| Driving Efficiency | `eta_drive` | 0.35 |
| Standby Efficiency | `eta_sb` | 1 |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0 |
| Charging Flow | `Q_charge` | 4000 |
| Driving Power | `P_drive` | 110 |
| Driving Consumption | `consumption` | 0.000044 |
| Start Tank Volume | `soc_start` | 0.05 |
| Minmum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Maximum SOC | `soc_max` | 0.05 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Driving/Transport Sector | `sector_drive` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | PetrolVehicle_ + position |
| Label Sink Component | `labelSink` | PetrolVehicleSink_ + position |
| Label Drive Component | `labelDrive` | PetrolVehicleDrive_ + position |
| Storage Balance | `balanced` | `True` |
| Charging Power Limitation | `charge_limit` | 1 |
| Driving Power Limitation | `drive_limit` | 1 |
| Heating Value Diesel | `H_diesel` | 10400 |

</details>

<details>
<summary>Code</summary>

```

class PetrolVehicle():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        P_drive = 110
        costsIn = 0
        costsOut=0
        Eta_charge = 1
        Eta_drive = 0.35
        Eta_sb = 1
        SOC_max = 0.05
        SOC_start = 0.05
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        position='Consumer_1'
        vehicle_consumption=0.000044
        chargeLimit = 1
        driveLimit = 1
        #Heating Value Diesel in kWh/m³
        H_diesel = 10400
        
        #Default Values for VW Golf 2 TDI DSG Style
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.Q_charge = kwargs.get("Q_charge", Q_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'PetrolVehicle_' + self.position
        self.labelDrive = 'PetrolVehicleDrive_' + self.position
        self.labelSink = 'PetrolVehicleSink_' + self.position
        self.balanced = kwargs.get("balanced", False)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.H_diesel = kwargs.get("H_diesel", H_diesel)
        self.costsSink=0
        
        
        
    def component(self):
        #Energy Flows
        v_petrolvCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.Q_charge*self.deltaT)
        v_petrolvDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
            
        petrolVehicle = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_petrolvCharge},
                outputs={self.sector_out: v_petrolvDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return petrolVehicle
    
    def sink(self):
        q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.costsSink)
            
        vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
        return vehicle_sink
    
    def drive(self):
        v_petvToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
        s_petv = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)


        petVDrive = solph.Transformer(
            label=self.labelDrive,
            inputs={self.sector_out : v_petvToMotor},
            outputs={self.sector_drive : s_petv},
            conversion_factors={self.sector_drive : 1/self.consumption})
        
        return petVDrive
```
</details>


## External Petrol
Petrol can only be supplied by external sources. To supply the required petrol, one possibility is a source without any purchase costs.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Petrol Flow | `Q` | 4000 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | ExternalPetrol_ + position |

</details>

<details>
<summary>Code</summary>

```

class PetrolFuel():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        
        self.Q= kwargs.get("Q", Q_charge)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'ExternalPetrol_' + self.position
        self.costs=0
        
        
    def component(self):
        q_petrol = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs)
            
        petrol = solph.Source(label=self.label, outputs={self.sector: q_petrol})
        
        return petrol
```
</details>

## External Petrol Emissions
Additionally considered emissions.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Petrol Flow | `Q` | 4000 |
| Sector | `sector` | / |
| Sector Emissions | `sector_emissions` | 0 |
| Maximum emissions | `max_emissions` | 1000000 |
| Emissions | `emissions` | 2766.4 |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | ExternalPetrol_ + position |

</details>

<details>
<summary>Code</summary>

```
class PetrolFuelEmissions():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        emissions=2766.4
        maxEmissions=1000000
        
        self.Q= kwargs.get("Q", Q_charge)
        self.sector= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ExternalPetrol_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'External\nPetrol_' + self.position
        self.costs=0
        self.color='slategrey'
        
        
    def component(self):
        q_petrol = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs)
        
        if(self.sector_emissions==0 or self.emissions==0):
  
            petrol = solph.Source(label=self.label, outputs={self.sector: q_petrol})
            
        else:
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            petrol = em.Source(label=self.label, 
                           outputs={self.sector: q_petrol, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector)
        
        return petrol
```
</details>


## Petrol Purchase
If purchase costs are considered, this block can be used.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Petrol Flow | `Q` | 4000 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | ExternalPetrol_ + position |
| Petrol Purchase Costs| `costs` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class PetrolPurchase():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        timesteps=8760

        
        self.Q= kwargs.get("Q_charge", Q_charge)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Petrolpurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)

        
        costsArray = np.zeros(self.timesteps)
            
        self.costs = kwargs.get("costs", costsArray)
        
        
    def component(self):
        q_petrolGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs)
            
        petrol = solph.Source(label=self.label, outputs={self.sector: q_petrolGridpurchase})
        
        return petrol     
```
</details>


## Petrol Purchase Emissions
Additionally considered emissions.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Petrol Flow | `Q` | 4000 |
| Sector | `sector` | / |
| Sector Emissions | `sector_emissions` | 0 |
| Maximum emissions | `max_emissions` | 1000000 |
| Emissions | `emissions` | 2766.4 |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label Vehicle Component| `label` | ExternalPetrol_ + position |
| Petrol Purchase Costs| `costs` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class PetrolPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        emissions=2766.4
        maxEmissions=1000000
        timesteps=8760
        
        self.Q= kwargs.get("Q", Q_charge)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        label = 'Petrolpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Petrol\nPurchase_' + self.position
        
        self.color='slategrey'
        
        costsArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsArray)
        
        
    def component(self):
        q_petrol = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_out)
        
        if(self.sector_emissions==0 or self.emissions==0):
  
            petrol = solph.Source(label=self.label, outputs={self.sector_out: q_petrol})
            
        else:
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            petrol = em.Source(label=self.label, 
                           outputs={self.sector_out: q_petrol, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
            
        return petrol    
```
</details>

