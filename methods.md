# Methods

File for helful and required methods for the rest of the model. Needed for saving the model results and restoring them.

```
import methods as meth

```

Import into Optimization Model code:

`import source_emissions as em`


<details>
<summary>Get Values</summary>

Converting an array to a list.

```
def values(array):
    liste = []
    for i in array:
        liste.append(i[0])
        
    return liste
```
</details>


<details>
<summary>Store the Results</summary>

Storing the results into a file.

```
def store_results(energysystem, model, scenario):
    energysystem.results = solph.processing.results(model)
    file2write =  scenario 
    energysystem.dump(file2write, 'my_dump_general.oemof')
    print("----------------Data saved----------------")
```
</details>


<details>
<summary>Restore the Results</summary>

Restoring the results from the file

```
def restore_results(scenario):
    my_energysystem = solph.EnergySystem()
    file2load =  scenario
    my_energysystem.restore(file2load, 'my_dump_general.oemof')
    results = my_energysystem.results
    print("----------------Data restored----------------")
    return results
```
</details>
