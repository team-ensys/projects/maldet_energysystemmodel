# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 08:26:14 2023

@author: Matthias Maldet
"""

import pandas as pd
import numpy as np

#Input parameters to change
resultdirectory = "results/"
scenarioname = "LSM23_environ"
wasteextension=True
greywaterextension=False


#Begin program here
file2read=resultdirectory + scenarioname + "/results.xlsx"

electricitygridpurchase=[]
electricitydemand=[]
electricityheatpump=[]
electricitysewagetreat=[]
emissions=[]
electricityfeedin=[]
disposedwaste=[]
exhaustheat=[]
accruingwaste=[]
heatdemand=[]
gassale=[]
gasAD=[]
elec2LSM=[]
LSM2elec=[]
recoveredwater=[]
greywater2water=[]
waterdemand=[]
wastetransport=[]
combustedwaste=[]
digestedwaste=[]

print("---------------------------------------------")
print("Program started")
print("---------------------------------------------")
print()

#----------------------------- LSC -----------------------------------------------------------

#LSC1 Data
consumer="LSC_1"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)

electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSC_1', 'LSC_1_Electricitysector'), 'flow')"].values))
electricitydemand.append(np.sum(data["(('LSC_1_Electricitysector', 'Electricitydemand_LSC_1'), 'flow')"].values))
electricityheatpump.append(np.sum(data["(('LSC_1_Electricitysector', 'Heatpump_LSC_1'), 'flow')"].values))
emissions.append(np.sum(data["(('LSC_1_Emissionsector', 'Emissiontotal_LSC_1'), 'flow')"].values))
electricityfeedin.append(np.sum(data["(('LSC_1_Electricitysector', 'Electricityfeedin_LSC_1'), 'flow')"].values))
accruingwaste.append(np.sum(data["(('WasteAccruing_LSC_1', 'LSC_1_Wastesector'), 'flow')"].values))
heatdemand.append(np.sum(data["(('LSC_1_Heatsector', 'Heatdemand_LSC_1'), 'flow')"].values))
elec2LSM.append(np.sum(data["(('LSC_1_Electricitysector', 'Elec2LSM_LSC_1'), 'flow')"].values))

lsm2elec1=np.sum(data["(('LSM2elecLSM1_LSC_1', 'LSC_1_Electricitysector'), 'flow')"].values)
lsm2elec2=np.sum(data["(('LSM2elecLSM2_LSC_1', 'LSC_1_Electricitysector'), 'flow')"].values)
lsm2elec3=np.sum(data["(('LSM2elecLSM3_LSC_1', 'LSC_1_Electricitysector'), 'flow')"].values)
lsm2elec4=np.sum(data["(('LSM2elecLSM4_LSC_1', 'LSC_1_Electricitysector'), 'flow')"].values)
lsm2elec5=np.sum(data["(('LSM2elecLSM5_LSC_1', 'LSC_1_Electricitysector'), 'flow')"].values)
LSM2elec.append(lsm2elec1 + lsm2elec2 + lsm2elec3 + lsm2elec4 + lsm2elec5)


recovered1=np.sum(data["(('LSCpurchaseLSM1_LSC_1', 'LSC_1_Potablewatersector'), 'flow')"].values)
recovered2=np.sum(data["(('LSCpurchaseLSM2_LSC_1', 'LSC_1_Potablewatersector'), 'flow')"].values)
recovered3=np.sum(data["(('LSCpurchaseLSM3_LSC_1', 'LSC_1_Potablewatersector'), 'flow')"].values)
recovered4=np.sum(data["(('LSCpurchaseLSM4_LSC_1', 'LSC_1_Potablewatersector'), 'flow')"].values)
recovered5=np.sum(data["(('LSCpurchaseLSM5_LSC_1', 'LSC_1_Potablewatersector'), 'flow')"].values)
recoveredwater.append(recovered1 + recovered2 + recovered3 + recovered4 + recovered5)

if greywaterextension:
    greywater2water.append(np.sum(data["(('Greywater2water_LSC_1', 'LSC_1_Potablewatersector'), 'flow')"].values))
else:
    greywater2water.append(0)
    
waterdemand.append(np.sum(data["(('LSC_1_Potablewatersector', 'Waterdemand_LSC_1'), 'flow')"].values))


#LSC2 Data
consumer="LSC_2"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)

electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSC_2', 'LSC_2_Electricitysector'), 'flow')"].values))
electricitydemand.append(np.sum(data["(('LSC_2_Electricitysector', 'Electricitydemand_LSC_2'), 'flow')"].values))
electricityheatpump.append(np.sum(data["(('LSC_2_Electricitysector', 'Heatpump_LSC_2'), 'flow')"].values))
emissions.append(np.sum(data["(('LSC_2_Emissionsector', 'Emissiontotal_LSC_2'), 'flow')"].values))
electricityfeedin.append(np.sum(data["(('LSC_2_Electricitysector', 'Electricityfeedin_LSC_2'), 'flow')"].values))
accruingwaste.append(np.sum(data["(('WasteAccruing_LSC_2', 'LSC_2_Wastesector'), 'flow')"].values))
heatdemand.append(np.sum(data["(('LSC_2_Heatsector', 'Heatdemand_LSC_2'), 'flow')"].values))
elec2LSM.append(np.sum(data["(('LSC_2_Electricitysector', 'Elec2LSM_LSC_2'), 'flow')"].values))

lsm2elec1=np.sum(data["(('LSM2elecLSM1_LSC_2', 'LSC_2_Electricitysector'), 'flow')"].values)
lsm2elec2=np.sum(data["(('LSM2elecLSM2_LSC_2', 'LSC_2_Electricitysector'), 'flow')"].values)
lsm2elec3=np.sum(data["(('LSM2elecLSM3_LSC_2', 'LSC_2_Electricitysector'), 'flow')"].values)
lsm2elec4=np.sum(data["(('LSM2elecLSM4_LSC_2', 'LSC_2_Electricitysector'), 'flow')"].values)
lsm2elec5=np.sum(data["(('LSM2elecLSM5_LSC_2', 'LSC_2_Electricitysector'), 'flow')"].values)
LSM2elec.append(lsm2elec1 + lsm2elec2 + lsm2elec3 + lsm2elec4 + lsm2elec5)


recovered1=np.sum(data["(('LSCpurchaseLSM1_LSC_2', 'LSC_2_Potablewatersector'), 'flow')"].values)
recovered2=np.sum(data["(('LSCpurchaseLSM2_LSC_2', 'LSC_2_Potablewatersector'), 'flow')"].values)
recovered3=np.sum(data["(('LSCpurchaseLSM3_LSC_2', 'LSC_2_Potablewatersector'), 'flow')"].values)
recovered4=np.sum(data["(('LSCpurchaseLSM4_LSC_2', 'LSC_2_Potablewatersector'), 'flow')"].values)
recovered5=np.sum(data["(('LSCpurchaseLSM5_LSC_2', 'LSC_2_Potablewatersector'), 'flow')"].values)
recoveredwater.append(recovered1 + recovered2 + recovered3 + recovered4 + recovered5)

if greywaterextension:
    greywater2water.append(np.sum(data["(('Greywater2water_LSC_2', 'LSC_2_Potablewatersector'), 'flow')"].values))
else:
    greywater2water.append(0)
    
waterdemand.append(np.sum(data["(('LSC_2_Potablewatersector', 'Waterdemand_LSC_2'), 'flow')"].values))

#LSC3 Data
consumer="LSC_3"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)

electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSC_3', 'LSC_3_Electricitysector'), 'flow')"].values))
electricitydemand.append(np.sum(data["(('LSC_3_Electricitysector', 'Electricitydemand_LSC_3'), 'flow')"].values))
electricityheatpump.append(np.sum(data["(('LSC_3_Electricitysector', 'Heatpump_LSC_3'), 'flow')"].values))
emissions.append(np.sum(data["(('LSC_3_Emissionsector', 'Emissiontotal_LSC_3'), 'flow')"].values))
electricityfeedin.append(np.sum(data["(('LSC_3_Electricitysector', 'Electricityfeedin_LSC_3'), 'flow')"].values))
accruingwaste.append(np.sum(data["(('WasteAccruing_LSC_3', 'LSC_3_Wastesector'), 'flow')"].values))
heatdemand.append(np.sum(data["(('LSC_3_Heatsector', 'Heatdemand_LSC_3'), 'flow')"].values))
elec2LSM.append(np.sum(data["(('LSC_3_Electricitysector', 'Elec2LSM_LSC_3'), 'flow')"].values))

lsm2elec1=np.sum(data["(('LSM2elecLSM1_LSC_3', 'LSC_3_Electricitysector'), 'flow')"].values)
lsm2elec2=np.sum(data["(('LSM2elecLSM2_LSC_3', 'LSC_3_Electricitysector'), 'flow')"].values)
lsm2elec3=np.sum(data["(('LSM2elecLSM3_LSC_3', 'LSC_3_Electricitysector'), 'flow')"].values)
lsm2elec4=np.sum(data["(('LSM2elecLSM4_LSC_3', 'LSC_3_Electricitysector'), 'flow')"].values)
lsm2elec5=np.sum(data["(('LSM2elecLSM5_LSC_3', 'LSC_3_Electricitysector'), 'flow')"].values)
LSM2elec.append(lsm2elec1 + lsm2elec2 + lsm2elec3 + lsm2elec4 + lsm2elec5)


recovered1=np.sum(data["(('LSCpurchaseLSM1_LSC_3', 'LSC_3_Potablewatersector'), 'flow')"].values)
recovered2=np.sum(data["(('LSCpurchaseLSM2_LSC_3', 'LSC_3_Potablewatersector'), 'flow')"].values)
recovered3=np.sum(data["(('LSCpurchaseLSM2_LSC_3', 'LSC_3_Potablewatersector'), 'flow')"].values)
recovered4=np.sum(data["(('LSCpurchaseLSM2_LSC_3', 'LSC_3_Potablewatersector'), 'flow')"].values)
recovered5=np.sum(data["(('LSCpurchaseLSM2_LSC_3', 'LSC_3_Potablewatersector'), 'flow')"].values)
recoveredwater.append(recovered1 + recovered2 + recovered3 + recovered4 + recovered5)

if greywaterextension:
    greywater2water.append(np.sum(data["(('Greywater2water_LSC_3', 'LSC_3_Potablewatersector'), 'flow')"].values))
else:
    greywater2water.append(0)
    
waterdemand.append(np.sum(data["(('LSC_3_Potablewatersector', 'Waterdemand_LSC_3'), 'flow')"].values))

#LSC4 Data
consumer="LSC_4"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)

electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSC_4', 'LSC_4_Electricitysector'), 'flow')"].values))
electricitydemand.append(np.sum(data["(('LSC_4_Electricitysector', 'Electricitydemand_LSC_4'), 'flow')"].values))
electricityheatpump.append(np.sum(data["(('LSC_4_Electricitysector', 'Heatpump_LSC_4'), 'flow')"].values))
emissions.append(np.sum(data["(('LSC_4_Emissionsector', 'Emissiontotal_LSC_4'), 'flow')"].values))
electricityfeedin.append(np.sum(data["(('LSC_4_Electricitysector', 'Electricityfeedin_LSC_4'), 'flow')"].values))
accruingwaste.append(np.sum(data["(('WasteAccruing_LSC_4', 'LSC_4_Wastesector'), 'flow')"].values))
heatdemand.append(np.sum(data["(('LSC_4_Heatsector', 'Heatdemand_LSC_4'), 'flow')"].values))
elec2LSM.append(np.sum(data["(('LSC_4_Electricitysector', 'Elec2LSM_LSC_4'), 'flow')"].values))

lsm2elec1=np.sum(data["(('LSM2elecLSM1_LSC_4', 'LSC_4_Electricitysector'), 'flow')"].values)
lsm2elec2=np.sum(data["(('LSM2elecLSM2_LSC_4', 'LSC_4_Electricitysector'), 'flow')"].values)
lsm2elec3=np.sum(data["(('LSM2elecLSM3_LSC_4', 'LSC_4_Electricitysector'), 'flow')"].values)
lsm2elec4=np.sum(data["(('LSM2elecLSM4_LSC_4', 'LSC_4_Electricitysector'), 'flow')"].values)
lsm2elec5=np.sum(data["(('LSM2elecLSM5_LSC_4', 'LSC_4_Electricitysector'), 'flow')"].values)
LSM2elec.append(lsm2elec1 + lsm2elec2 + lsm2elec3 + lsm2elec4 + lsm2elec5)


recovered1=np.sum(data["(('LSCpurchaseLSM1_LSC_4', 'LSC_4_Potablewatersector'), 'flow')"].values)
recovered2=np.sum(data["(('LSCpurchaseLSM2_LSC_4', 'LSC_4_Potablewatersector'), 'flow')"].values)
recovered3=np.sum(data["(('LSCpurchaseLSM2_LSC_4', 'LSC_4_Potablewatersector'), 'flow')"].values)
recovered4=np.sum(data["(('LSCpurchaseLSM2_LSC_4', 'LSC_4_Potablewatersector'), 'flow')"].values)
recovered5=np.sum(data["(('LSCpurchaseLSM2_LSC_4', 'LSC_4_Potablewatersector'), 'flow')"].values)
recoveredwater.append(recovered1 + recovered2 + recovered3 + recovered4 + recovered5)

if greywaterextension:
    greywater2water.append(np.sum(data["(('Greywater2water_LSC_4', 'LSC_4_Potablewatersector'), 'flow')"].values))
else:
    greywater2water.append(0)
    
waterdemand.append(np.sum(data["(('LSC_4_Potablewatersector', 'Waterdemand_LSC_4'), 'flow')"].values))

#LSC5 Data
consumer="LSC_5"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)

electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSC_5', 'LSC_5_Electricitysector'), 'flow')"].values))
electricitydemand.append(np.sum(data["(('LSC_5_Electricitysector', 'Electricitydemand_LSC_5'), 'flow')"].values))
electricityheatpump.append(np.sum(data["(('LSC_5_Electricitysector', 'Heatpump_LSC_5'), 'flow')"].values))
emissions.append(np.sum(data["(('LSC_5_Emissionsector', 'Emissiontotal_LSC_5'), 'flow')"].values))
electricityfeedin.append(np.sum(data["(('LSC_5_Electricitysector', 'Electricityfeedin_LSC_5'), 'flow')"].values))
accruingwaste.append(np.sum(data["(('WasteAccruing_LSC_5', 'LSC_5_Wastesector'), 'flow')"].values))
heatdemand.append(np.sum(data["(('LSC_5_Heatsector', 'Heatdemand_LSC_5'), 'flow')"].values))
elec2LSM.append(np.sum(data["(('LSC_5_Electricitysector', 'Elec2LSM_LSC_5'), 'flow')"].values))

lsm2elec1=np.sum(data["(('LSM2elecLSM1_LSC_5', 'LSC_5_Electricitysector'), 'flow')"].values)
lsm2elec2=np.sum(data["(('LSM2elecLSM2_LSC_5', 'LSC_5_Electricitysector'), 'flow')"].values)
lsm2elec3=np.sum(data["(('LSM2elecLSM3_LSC_5', 'LSC_5_Electricitysector'), 'flow')"].values)
lsm2elec4=np.sum(data["(('LSM2elecLSM4_LSC_5', 'LSC_5_Electricitysector'), 'flow')"].values)
lsm2elec5=np.sum(data["(('LSM2elecLSM5_LSC_5', 'LSC_5_Electricitysector'), 'flow')"].values)
LSM2elec.append(lsm2elec1 + lsm2elec2 + lsm2elec3 + lsm2elec4 + lsm2elec5)


recovered1=np.sum(data["(('LSCpurchaseLSM1_LSC_5', 'LSC_5_Potablewatersector'), 'flow')"].values)
recovered2=np.sum(data["(('LSCpurchaseLSM2_LSC_5', 'LSC_5_Potablewatersector'), 'flow')"].values)
recovered3=np.sum(data["(('LSCpurchaseLSM2_LSC_5', 'LSC_5_Potablewatersector'), 'flow')"].values)
recovered4=np.sum(data["(('LSCpurchaseLSM2_LSC_5', 'LSC_5_Potablewatersector'), 'flow')"].values)
recovered5=np.sum(data["(('LSCpurchaseLSM2_LSC_5', 'LSC_5_Potablewatersector'), 'flow')"].values)
recoveredwater.append(recovered1 + recovered2 + recovered3 + recovered4 + recovered5)

if greywaterextension:
    greywater2water.append(np.sum(data["(('Greywater2water_LSC_5', 'LSC_5_Potablewatersector'), 'flow')"].values))
else:
    greywater2water.append(0)
    
waterdemand.append(np.sum(data["(('LSC_5_Potablewatersector', 'Waterdemand_LSC_5'), 'flow')"].values))

#----------------------------- LSM -----------------------------------------------------------

#LSM1 Data
consumer="LSM_1"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)
electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSM_1', 'LSM_1_Electricitysector'), 'flow')"].values))
electricitysewagetreat.append(np.sum(data["(('LSM_1_Electricitysector', 'SewageTreatment_LSM_1'), 'flow')"].values))
emissions.append(np.sum(data["(('LSM_1_Emissionsector', 'Emissiontotal_LSM_1'), 'flow')"].values))

if wasteextension:
    electricityfeedin.append(np.sum(data["(('LSM_1_Electricitysector', 'Curtailment_LSM_1'), 'flow')"].values))
else:
    electricityfeedin.append(0)

accruingwaste.append(np.sum(data["(('Sludge2Hub_LSM_1', 'LSM_1_Wastesector'), 'flow')"].values))

if wasteextension:
    disposedwaste.append(np.sum(data["(('LSM_1_Wastesector', 'Wastedisposal_LSM_1'), 'flow')"].values))
else:
    disposedwaste.append(0)
    
exhaustheat.append(np.sum(data["(('LSM_1_Heatsector', 'Exhaustheat_LSM_1'), 'flow')"].values))

if wasteextension:
    gassale.append(np.sum(data["(('LSM_1_Gassector', 'Gassale_LSM_1'), 'flow')"].values))
else:
    gassale.append(0)
    
if wasteextension:
    gasAD.append(np.sum(data["(('AnaerobicDigestion_LSM_1', 'LSM_1_Gassector'), 'flow')"].values))
else:
    gasAD.append(0)
    
wastetransport1=np.sum(data["(('LSM_1_Wastesector', 'WastetransportLSM2_LSM_1'), 'flow')"])
wastetransport2=np.sum(data["(('LSM_1_Wastesector', 'WastetransportLSM3_LSM_1'), 'flow')"])
wastetransport3=np.sum(data["(('LSM_1_Wastesector', 'WastetransportLSM4_LSM_1'), 'flow')"])
wastetransport4=np.sum(data["(('LSM_1_Wastesector', 'WastetransportLSM5_LSM_1'), 'flow')"])

totalwastetransport=wastetransport1 + wastetransport2 + wastetransport3 + wastetransport4

if wasteextension:
    totalwastetransport+=(np.sum(data["(('LSM_1_Wastebiosector', 'BioWastetransportLSM2_LSM_1'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_1_Wastebiosector', 'BioWastetransportLSM3_LSM_1'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_1_Wastebiosector', 'BioWastetransportLSM4_LSM_1'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_1_Wastebiosector', 'BioWastetransportLSM5_LSM_1'), 'flow')"].values))

wastetransport.append(totalwastetransport)

combustedwaste.append(np.sum(data["(('LSM_1_Wastesector', 'Wastecombustion_LSM_1'), 'flow')"].values))

if wasteextension:
    digestedwaste.append(np.sum(data["(('LSM_1_Wastebiosector', 'AnaerobicDigestion_LSM_1'), 'flow')"].values))
else:
    digestedwaste.append(0)
    
#LSM2 Data
consumer="LSM_2"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)
electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSM_2', 'LSM_2_Electricitysector'), 'flow')"].values))
electricitysewagetreat.append(np.sum(data["(('LSM_2_Electricitysector', 'SewageTreatment_LSM_2'), 'flow')"].values))
emissions.append(np.sum(data["(('LSM_2_Emissionsector', 'Emissiontotal_LSM_2'), 'flow')"].values))

if wasteextension:
    electricityfeedin.append(np.sum(data["(('LSM_2_Electricitysector', 'Curtailment_LSM_2'), 'flow')"].values))
else:
    electricityfeedin.append(0)

accruingwaste.append(np.sum(data["(('Sludge2Hub_LSM_2', 'LSM_2_Wastesector'), 'flow')"].values))

if wasteextension:
    disposedwaste.append(np.sum(data["(('LSM_2_Wastesector', 'Wastedisposal_LSM_2'), 'flow')"].values))
else:
    disposedwaste.append(0)
    
exhaustheat.append(np.sum(data["(('LSM_2_Heatsector', 'Exhaustheat_LSM_2'), 'flow')"].values))

if wasteextension:
    gassale.append(np.sum(data["(('LSM_2_Gassector', 'Gassale_LSM_2'), 'flow')"].values))
else:
    gassale.append(0)
    
if wasteextension:
    gasAD.append(np.sum(data["(('AnaerobicDigestion_LSM_2', 'LSM_2_Gassector'), 'flow')"].values))
else:
    gasAD.append(0)
    
wastetransport1=np.sum(data["(('LSM_2_Wastesector', 'WastetransportLSM1_LSM_2'), 'flow')"])
wastetransport2=np.sum(data["(('LSM_2_Wastesector', 'WastetransportLSM3_LSM_2'), 'flow')"])
wastetransport3=np.sum(data["(('LSM_2_Wastesector', 'WastetransportLSM4_LSM_2'), 'flow')"])
wastetransport4=np.sum(data["(('LSM_2_Wastesector', 'WastetransportLSM5_LSM_2'), 'flow')"])

totalwastetransport=wastetransport1 + wastetransport2 + wastetransport3 + wastetransport4

if wasteextension:
    totalwastetransport+=(np.sum(data["(('LSM_2_Wastebiosector', 'BioWastetransportLSM1_LSM_2'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_2_Wastebiosector', 'BioWastetransportLSM3_LSM_2'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_2_Wastebiosector', 'BioWastetransportLSM4_LSM_2'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_2_Wastebiosector', 'BioWastetransportLSM5_LSM_2'), 'flow')"].values))

wastetransport.append(totalwastetransport)

combustedwaste.append(np.sum(data["(('LSM_2_Wastesector', 'Wastecombustion_LSM_2'), 'flow')"].values))

if wasteextension:
    digestedwaste.append(np.sum(data["(('LSM_2_Wastebiosector', 'AnaerobicDigestion_LSM_2'), 'flow')"].values))
else:
    digestedwaste.append(0)
    
#LSM3 Data
consumer="LSM_3"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)
electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSM_3', 'LSM_3_Electricitysector'), 'flow')"].values))
electricitysewagetreat.append(np.sum(data["(('LSM_3_Electricitysector', 'SewageTreatment_LSM_3'), 'flow')"].values))
emissions.append(np.sum(data["(('LSM_3_Emissionsector', 'Emissiontotal_LSM_3'), 'flow')"].values))
if wasteextension:
    electricityfeedin.append(np.sum(data["(('LSM_3_Electricitysector', 'Curtailment_LSM_3'), 'flow')"].values))
else:
    electricityfeedin.append(0)

accruingwaste.append(np.sum(data["(('Sludge2Hub_LSM_3', 'LSM_3_Wastesector'), 'flow')"].values))

if wasteextension:
    disposedwaste.append(np.sum(data["(('LSM_3_Wastesector', 'Wastedisposal_LSM_3'), 'flow')"].values))
else:
    disposedwaste.append(0)
    
exhaustheat.append(np.sum(data["(('LSM_3_Heatsector', 'Exhaustheat_LSM_3'), 'flow')"].values))

if wasteextension:
    gassale.append(np.sum(data["(('LSM_3_Gassector', 'Gassale_LSM_3'), 'flow')"].values))
else:
    gassale.append(0)
    
if wasteextension:
    gasAD.append(np.sum(data["(('AnaerobicDigestion_LSM_3', 'LSM_3_Gassector'), 'flow')"].values))
else:
    gasAD.append(0)
    
wastetransport1=np.sum(data["(('LSM_3_Wastesector', 'WastetransportLSM1_LSM_3'), 'flow')"])
wastetransport2=np.sum(data["(('LSM_3_Wastesector', 'WastetransportLSM2_LSM_3'), 'flow')"])
wastetransport3=np.sum(data["(('LSM_3_Wastesector', 'WastetransportLSM4_LSM_3'), 'flow')"])
wastetransport4=np.sum(data["(('LSM_3_Wastesector', 'WastetransportLSM5_LSM_3'), 'flow')"])

totalwastetransport=wastetransport1 + wastetransport2 + wastetransport3 + wastetransport4

if wasteextension:
    totalwastetransport+=(np.sum(data["(('LSM_3_Wastebiosector', 'BioWastetransportLSM1_LSM_3'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_3_Wastebiosector', 'BioWastetransportLSM2_LSM_3'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_3_Wastebiosector', 'BioWastetransportLSM4_LSM_3'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_3_Wastebiosector', 'BioWastetransportLSM5_LSM_3'), 'flow')"].values))

wastetransport.append(totalwastetransport)

combustedwaste.append(np.sum(data["(('LSM_3_Wastesector', 'Wastecombustion_LSM_3'), 'flow')"].values))

if wasteextension:
    digestedwaste.append(np.sum(data["(('LSM_3_Wastebiosector', 'AnaerobicDigestion_LSM_3'), 'flow')"].values))
else:
    digestedwaste.append(0)
    
#LSM4 Data
consumer="LSM_4"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)
electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSM_4', 'LSM_4_Electricitysector'), 'flow')"].values))
electricitysewagetreat.append(np.sum(data["(('LSM_4_Electricitysector', 'SewageTreatment_LSM_4'), 'flow')"].values))
emissions.append(np.sum(data["(('LSM_4_Emissionsector', 'Emissiontotal_LSM_4'), 'flow')"].values))
if wasteextension:
    electricityfeedin.append(np.sum(data["(('LSM_4_Electricitysector', 'Curtailment_LSM_4'), 'flow')"].values))
else:
    electricityfeedin.append(0)

accruingwaste.append(np.sum(data["(('Sludge2Hub_LSM_4', 'LSM_4_Wastesector'), 'flow')"].values))

if wasteextension:
    disposedwaste.append(np.sum(data["(('LSM_4_Wastesector', 'Wastedisposal_LSM_4'), 'flow')"].values))
else:
    disposedwaste.append(0)
    
exhaustheat.append(np.sum(data["(('LSM_4_Heatsector', 'Exhaustheat_LSM_4'), 'flow')"].values))

if wasteextension:
    gassale.append(np.sum(data["(('LSM_4_Gassector', 'Gassale_LSM_4'), 'flow')"].values))
else:
    gassale.append(0)
    
if wasteextension:
    gasAD.append(np.sum(data["(('AnaerobicDigestion_LSM_4', 'LSM_4_Gassector'), 'flow')"].values))
else:
    gasAD.append(0)
    
wastetransport1=np.sum(data["(('LSM_4_Wastesector', 'WastetransportLSM1_LSM_4'), 'flow')"])
wastetransport2=np.sum(data["(('LSM_4_Wastesector', 'WastetransportLSM2_LSM_4'), 'flow')"])
wastetransport3=np.sum(data["(('LSM_4_Wastesector', 'WastetransportLSM3_LSM_4'), 'flow')"])
wastetransport4=np.sum(data["(('LSM_4_Wastesector', 'WastetransportLSM5_LSM_4'), 'flow')"])

totalwastetransport=wastetransport1 + wastetransport2 + wastetransport3 + wastetransport4

if wasteextension:
    totalwastetransport+=(np.sum(data["(('LSM_4_Wastebiosector', 'BioWastetransportLSM1_LSM_4'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_4_Wastebiosector', 'BioWastetransportLSM2_LSM_4'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_4_Wastebiosector', 'BioWastetransportLSM3_LSM_4'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_4_Wastebiosector', 'BioWastetransportLSM5_LSM_4'), 'flow')"].values))

wastetransport.append(totalwastetransport)

combustedwaste.append(np.sum(data["(('LSM_4_Wastesector', 'Wastecombustion_LSM_4'), 'flow')"].values))

if wasteextension:
    digestedwaste.append(np.sum(data["(('LSM_4_Wastebiosector', 'AnaerobicDigestion_LSM_4'), 'flow')"].values))
else:
    digestedwaste.append(0)
    
#LSM5 Data
consumer="LSM_5"
data=pd.read_excel(file2read, index_col=0, sheet_name=consumer)
electricitygridpurchase.append(np.sum(data["(('Electricitygridpurchase_LSM_5', 'LSM_5_Electricitysector'), 'flow')"].values))
electricitysewagetreat.append(np.sum(data["(('LSM_5_Electricitysector', 'SewageTreatment_LSM_5'), 'flow')"].values))
emissions.append(np.sum(data["(('LSM_5_Emissionsector', 'Emissiontotal_LSM_5'), 'flow')"].values))
if wasteextension:
    electricityfeedin.append(np.sum(data["(('LSM_5_Electricitysector', 'Curtailment_LSM_5'), 'flow')"].values))
else:
    electricityfeedin.append(0)

accruingwaste.append(np.sum(data["(('Sludge2Hub_LSM_5', 'LSM_5_Wastesector'), 'flow')"].values))

if wasteextension:
    disposedwaste.append(np.sum(data["(('LSM_5_Wastesector', 'Wastedisposal_LSM_5'), 'flow')"].values))
else:
    disposedwaste.append(0)
    
exhaustheat.append(np.sum(data["(('LSM_5_Heatsector', 'Exhaustheat_LSM_5'), 'flow')"].values))

if wasteextension:
    gassale.append(np.sum(data["(('LSM_5_Gassector', 'Gassale_LSM_5'), 'flow')"].values))
else:
    gassale.append(0)
    
if wasteextension:
    gasAD.append(np.sum(data["(('AnaerobicDigestion_LSM_5', 'LSM_5_Gassector'), 'flow')"].values))
else:
    gasAD.append(0)
    
wastetransport1=np.sum(data["(('LSM_5_Wastesector', 'WastetransportLSM1_LSM_5'), 'flow')"])
wastetransport2=np.sum(data["(('LSM_5_Wastesector', 'WastetransportLSM2_LSM_5'), 'flow')"])
wastetransport3=np.sum(data["(('LSM_5_Wastesector', 'WastetransportLSM3_LSM_5'), 'flow')"])
wastetransport4=np.sum(data["(('LSM_5_Wastesector', 'WastetransportLSM4_LSM_5'), 'flow')"])

totalwastetransport=wastetransport1 + wastetransport2 + wastetransport3 + wastetransport4

if wasteextension:
    totalwastetransport+=(np.sum(data["(('LSM_5_Wastebiosector', 'BioWastetransportLSM1_LSM_5'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_5_Wastebiosector', 'BioWastetransportLSM2_LSM_5'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_5_Wastebiosector', 'BioWastetransportLSM3_LSM_5'), 'flow')"].values))
    totalwastetransport+=(np.sum(data["(('LSM_5_Wastebiosector', 'BioWastetransportLSM4_LSM_5'), 'flow')"].values))

wastetransport.append(totalwastetransport)

combustedwaste.append(np.sum(data["(('LSM_5_Wastesector', 'Wastecombustion_LSM_5'), 'flow')"].values))

if wasteextension:
    digestedwaste.append(np.sum(data["(('LSM_5_Wastebiosector', 'AnaerobicDigestion_LSM_5'), 'flow')"].values))
else:
    digestedwaste.append(0)
    

print("---------------------------------------------")
print("Input data loaded")
print("---------------------------------------------")
print()

#KPI writing
electricitygridpurchase=np.sum(electricitygridpurchase)
electricitydemand=np.sum(electricitydemand)
electricityheatpump=np.sum(electricityheatpump)
electricitysewagetreat=np.sum(electricitysewagetreat)
emissions=np.sum(emissions)
electricityfeedin=np.sum(electricityfeedin)
disposedwaste=np.sum(disposedwaste)
exhaustheat=np.sum(exhaustheat)
accruingwaste=np.sum(accruingwaste)
heatdemand=np.sum(heatdemand)
gassale=np.sum(gassale)
gasAD=np.sum(gasAD)
elec2LSM=np.sum(elec2LSM)
LSM2elec=np.sum(LSM2elec)
recoveredwater=np.sum(recoveredwater)
greywater2water=np.sum(greywater2water)
waterdemand=np.sum(waterdemand)
wastetransport=np.sum(wastetransport)
combustedwaste=np.sum(combustedwaste)
digestedwaste=np.sum(digestedwaste)


df=pd.DataFrame()

df["Electricitygridpurchase"] = [electricitygridpurchase]
df["GridDependence"] = [electricitygridpurchase/(electricitydemand+electricityheatpump+electricitysewagetreat)]
df["ElectricityFeedin"] = [electricityfeedin]
df["Exhaustheat"] = [exhaustheat]
df["Wastedisposal"] = [disposedwaste]


    
efficiencyIndicator = (electricityfeedin+exhaustheat+3.4*disposedwaste+gassale)/(electricitydemand+electricityheatpump+electricitysewagetreat+heatdemand+accruingwaste*3.4+gasAD)
df["Inefficiency"] = [efficiencyIndicator]

df["Elec2LSM"] = [elec2LSM]
df["LSM2elec"] = [LSM2elec]
df["Emissions"] = [emissions]
df["Waterefficiency"] = [(recoveredwater+greywater2water)/waterdemand]
df["Wastetransport"] = [wastetransport]
df["CombustionShare"] = [combustedwaste/accruingwaste]
df["ADShare"] = [digestedwaste/accruingwaste]


file2write=resultdirectory + scenarioname + "/KPI.xlsx"
df.to_excel(file2write)  

print("---------------------------------------------")
print("KPIs written")
print("---------------------------------------------")
print()
