# -*- coding: utf-8 -*-
"""
Created on Wed Dec 22 13:40:11 2021

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np

class Emissions():
    
    def __init__(self, *args, **kwargs):
        max_emissions = 1000000000
        deltaT=1
        position='Consumer_1'
        
        #CO2 Price in € per kg
        co2_price = 0.03
        
        self.max_emissions= kwargs.get("max_emissions", max_emissions)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = kwargs.get('label', 'Emissions_' + self.position)
        self.labelSankey = kwargs.get('label_sankey', 'Emissions_' + self.position)
        self.costs_in=kwargs.get("costs", co2_price)
        color='dimgrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        m_emissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions*self.deltaT, variable_costs=self.costs_in)
            
        emissions = solph.Sink(label=self.label, inputs={self.sector_in: m_emissions})
        
        return emissions
