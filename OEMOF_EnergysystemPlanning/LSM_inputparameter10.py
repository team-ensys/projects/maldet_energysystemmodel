# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 10:15:34 2022

@author: Matthias Maldet
"""

def LSM_data():
    
    timesteps=8784
    
    #Nutzbare Menge Energierückgewinnung Waste Combustion
    usable_waste_combustion=0.5
    
    #Abfallentsorgungskosten
    waste_disposal_costs = 1.5
    
    #Abfallreduktionskosten
    waste_reduction_costs=0.048
    
    #Gas Preis
    gas_price=0.00768
    
    #Maximal entsorgter Abfall
    wastedisposal_vmax=847490
    min_disposal=0
    max_disposal=0
    
    #Limit Gassale --> multipliziert mit limit, gas conversion factor AD, disposal_vmax --> zusätzlich share biowaste berücksichtigen, da nur ein gewisser Anteil Biowaste ist
    #Zusätzlich mögliche auftretende Menge an Klärschlamm berücksichtigen --> ergibt sich aus V_waterdemand*(Ct_sludge/rho_sludge)
    #sollte als Näherung bzw. um alles gut zu beschreiben passen
    #Reservefaktor um keine infeasible solutions zu generieren
    max_sludge=362
    gassale_factor=0.75
    reservefactor_gas=1.1
    gassale_limit=gassale_factor*1.53*0.225*(wastedisposal_vmax+max_sludge)*reservefactor_gas
    
    lsm_struct ={
        
        #_________________________________________ LSM Hub 1 _______________________________________________________
        
        "LSM_1" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1630,
            #"P_elgrid": 0,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1630,
            "battery_soc_start": 0,
            "battery_P_in": 1630,
            "battery_P_out": 1630,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 100000,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": usable_waste_combustion,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 0, "LSM_2": 2, "LSM_3": 1.2, "LSM_4": 2.4, "LSM_5": 2},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_wastetruck": {"LSM_1": 0*0.125/3200, "LSM_2": 2*0.125/3200, "LSM_3": 1.2*0.125/3200, "LSM_4": 2.4*0.125/3200, "LSM_5": 2*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            #----------------- Sewage ------------------------------
            
           
            #Sewage treatment
            "sewagetreat_eta_water": 0.5,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 0, "LSM_2": 2, "LSM_3": 1.2, "LSM_4": 2.4, "LSM_5": 2},
            "emissions_sludgetruck":{"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_sludgetruck": {"LSM_1": 0*0.125/4, "LSM_2": 2*0.125/4, "LSM_3": 1.2*0.125/4, "LSM_4": 2.4*0.125/4, "LSM_5": 2*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #----------------- Additional components ------------------------------
            #Waste to Biowaste
            "waste2biowaste_maxwaste" : 847490,
            "share_biowaste_min" : 0,
            "share_biowaste_max" : 0.225,
            
            #waste reduction
            "wastereduction_maxwaste" : 847490,
            "reduction_rate" : 0.2,
            "min_reduction" : 0,
            "wastereduction_costs" : waste_reduction_costs,
            
            #Waste disposal
            "wastedisposal_vmax" : wastedisposal_vmax,
            "wastedisposal_emissions" : 0.382,
            "wastedisposal_costs" : waste_disposal_costs,
            "min_disposal" : min_disposal,
            "max_disposal" : max_disposal,
            
            #Waste Anaerobic Digestion
            "wasteAD_P_in" : 0,
            "wasteAD_P_out" : 0,
            
            #Gas CHP
            "chp_P_in" : 10000,
            "chp_P_out" : 10000,
            
            #Gas Sale
            "gassale_P_max" : 10000,
            "max_gassale" : 1,
            "gas_price" : gas_price,
            "gassale_limit" : gassale_limit
            
        },
        
        #_________________________________________ LSM Hub 2 _______________________________________________________
        
        "LSM_2" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 2300,
            #"P_elgrid": 0,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 2300,
            "battery_soc_start": 0,
            "battery_P_in": 2300,
            "battery_P_out": 2300,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 100000,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": usable_waste_combustion,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 2, "LSM_2": 0, "LSM_3": 1.2, "LSM_4": 2.4, "LSM_5": 1},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_wastetruck": {"LSM_1": 2*0.125/3200, "LSM_2": 0*0.125/3200, "LSM_3": 1.2*0.125/3200, "LSM_4": 2.4*0.125/3200, "LSM_5": 1*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 0.5,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 2, "LSM_2": 0, "LSM_3": 1.2, "LSM_4": 2.4, "LSM_5": 1},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_sludgetruck": {"LSM_1": 2*0.125/4, "LSM_2": 0*0.125/4, "LSM_3": 1.2*0.125/4, "LSM_4": 2.4*0.125/4, "LSM_5": 1*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #----------------- Additional components ------------------------------
            #Waste to Biowaste
            "waste2biowaste_maxwaste" : 847490,
            "share_biowaste_min" : 0,
            "share_biowaste_max" : 0.225,
            
            #waste reduction
            "wastereduction_maxwaste" : 847490,
            "reduction_rate" : 0.2,
            "min_reduction" : 0,
            "wastereduction_costs" : waste_reduction_costs,
            
            #Waste disposal
            "wastedisposal_vmax" : wastedisposal_vmax,
            "wastedisposal_emissions" : 0.382,
            "wastedisposal_costs" : waste_disposal_costs,
            "min_disposal" : min_disposal,
            "max_disposal" : max_disposal,
            
            #Waste Anaerobic Digestion
            "wasteAD_P_in" : 0,
            "wasteAD_P_out" : 0,
            
            #Gas CHP
            "chp_P_in" : 10000,
            "chp_P_out" : 10000,
            
            #Gas Sale
            "gassale_P_max" : 10000,
            "max_gassale" : 1,
            "gas_price" : gas_price,
            "gassale_limit" : gassale_limit
            
        },
        
        
        
        #_________________________________________ LSM Hub 3 _______________________________________________________
        
        "LSM_3" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 2000,
            #"P_elgrid": 0,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 2000,
            "battery_soc_start": 0,
            "battery_P_in": 2000,
            "battery_P_out": 2000,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 100000,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": usable_waste_combustion,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 1.2, "LSM_2": 1.2, "LSM_3": 0, "LSM_4": 1.6, "LSM_5": 1.2},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_wastetruck": {"LSM_1": 1.2*0.125/3200, "LSM_2": 1.2*0.125/3200, "LSM_3": 0*0.125/3200, "LSM_4": 1.6*0.125/3200, "LSM_5": 1.2*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 0.5,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 1.2, "LSM_2": 1.2, "LSM_3": 0, "LSM_4": 1.6, "LSM_5": 1.2},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_sludgetruck": {"LSM_1": 1.2*0.125/4, "LSM_2": 1.2*0.125/4, "LSM_3": 0*0.125/4, "LSM_4": 1.6*0.125/4, "LSM_5": 1.2*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #----------------- Additional components ------------------------------
            #Waste to Biowaste
            "waste2biowaste_maxwaste" : 847490,
            "share_biowaste_min" : 0,
            "share_biowaste_max" : 0.225,
            
            #waste reduction
            "wastereduction_maxwaste" : 847490,
            "reduction_rate" : 0.2,
            "min_reduction" : 0,
            "wastereduction_costs" : waste_reduction_costs,
            
            #Waste disposal
            "wastedisposal_vmax" : wastedisposal_vmax,
            "wastedisposal_emissions" : 0.382,
            "wastedisposal_costs" : waste_disposal_costs,
            "min_disposal" : min_disposal,
            "max_disposal" : max_disposal,
            
            #Waste Anaerobic Digestion
            "wasteAD_P_in" : 0,
            "wasteAD_P_out" : 0,
            
            #Gas CHP
            "chp_P_in" : 10000,
            "chp_P_out" : 10000,
            
            #Gas Sale
            "gassale_P_max" : 10000,
            "max_gassale" : 1,
            "gas_price" : gas_price,
            "gassale_limit" : gassale_limit
            
        },
        
        #_________________________________________ LSM Hub 4 _______________________________________________________
        
        "LSM_4" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1507,
            #"P_elgrid": 0,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1370,
            "battery_soc_start": 0,
            "battery_P_in": 1370,
            "battery_P_out": 1370,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 100000,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": usable_waste_combustion,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 2.4, "LSM_2": 2.4, "LSM_3": 1.6, "LSM_4": 0, "LSM_5": 2.4},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_wastetruck": {"LSM_1": 2.4*0.125/3200, "LSM_2": 2.4*0.125/3200, "LSM_3": 1.6*0.125/3200, "LSM_4": 0*0.125/3200, "LSM_5": 2.4*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 0.5,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 2.4, "LSM_2": 2.4, "LSM_3": 1.6, "LSM_4": 0, "LSM_5": 2.4},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_sludgetruck": {"LSM_1": 2.4*0.125/4, "LSM_2": 2.4*0.125/4, "LSM_3": 1.6*0.125/4, "LSM_4": 0*0.125/4, "LSM_5": 2.4*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #----------------- Additional components ------------------------------
            #Waste to Biowaste
            "waste2biowaste_maxwaste" : 847490,
            "share_biowaste_min" : 0,
            "share_biowaste_max" : 0.225,
            
            #waste reduction
            "wastereduction_maxwaste" : 847490,
            "reduction_rate" : 0.2,
            "min_reduction" : 0,
            "wastereduction_costs" : waste_reduction_costs,
            
            #Waste disposal
            "wastedisposal_vmax" : wastedisposal_vmax,
            "wastedisposal_emissions" : 0.382,
            "wastedisposal_costs" : waste_disposal_costs,
            "min_disposal" : min_disposal,
            "max_disposal" : max_disposal,
            
            #Waste Anaerobic Digestion
            "wasteAD_P_in" : 0,
            "wasteAD_P_out" : 0,
            
            #Gas CHP
            "chp_P_in" : 10000,
            "chp_P_out" : 10000,
            
            #Gas Sale
            "gassale_P_max" : 10000,
            "max_gassale" : 1,
            "gas_price" : gas_price,
            "gassale_limit" : gassale_limit
            
        },
        
        #_________________________________________ LSM Hub 5 _______________________________________________________
        
        "LSM_5" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            #"P_elgrid": 0,
            "P_elgrid": 1000,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 190,
            "battery_soc_start": 0,
            "battery_P_in": 190,
            "battery_P_out": 190,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 100000,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": usable_waste_combustion,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 2, "LSM_2": 1, "LSM_3": 1.2, "LSM_4": 2.4, "LSM_5": 0},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_wastetruck": {"LSM_1": 2*0.125/3200, "LSM_2": 1*0.125/3200, "LSM_3": 1.2*0.125/3200, "LSM_4": 2.4*0.125/3200, "LSM_5": 0*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 0.5,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 2, "LSM_2": 1, "LSM_3": 1.2, "LSM_4": 2.4, "LSM_5": 0},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125},
            "costs_sludgetruck": {"LSM_1": 2*0.125/4, "LSM_2": 1*0.125/4, "LSM_3": 1.2*0.125/4, "LSM_4": 2.4*0.125/4, "LSM_5": 0*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #----------------- Additional components ------------------------------
            #Waste to Biowaste
            "waste2biowaste_maxwaste" : 847490,
            "share_biowaste_min" : 0,
            "share_biowaste_max" : 0.225,
            
            #waste reduction
            "wastereduction_maxwaste" : 847490,
            "reduction_rate" : 0.2,
            "min_reduction" : 0,
            "wastereduction_costs" : waste_reduction_costs,
            
            #Waste disposal
            "wastedisposal_vmax" : wastedisposal_vmax,
            "wastedisposal_emissions" : 0.382,
            "wastedisposal_costs" : waste_disposal_costs,
            "min_disposal" : min_disposal,
            "max_disposal" : max_disposal,
            
            #Waste Anaerobic Digestion
            "wasteAD_P_in" : 0,
            "wasteAD_P_out" : 0,
            
            #Gas CHP
            "chp_P_in" : 10000,
            "chp_P_out" : 10000,
            
            #Gas Sale
            "gassale_P_max" : 10000,
            "max_gassale" : 1,
            "gas_price" : gas_price,
            "gassale_limit" : gassale_limit
            
        }
        
    }
    return lsm_struct
            
            