# -*- coding: utf-8 -*-
"""
Created on Tue May 10 11:00:47 2022

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyom
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
#scenario = 'scenario_phase2_noLSCnoSewageCosts'
scenario = 'scenario_phase2_BaseXwastewater'

#Filename
filename = "input_data_noLSC_noSewageCosts.xlsx"
#filename = "input_data_noLSC.xlsx"


#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 10
timestepsTotal = 8784

data=pd.ExcelFile(filename)


#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    

# -------------- Additional parameters that are set for the simulation -------------- 

#_____________ Electricity _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzentgelte
c_grid = 0.062

#Abgabe
c_abgabe = 0.018

#Reduktion Netzentgelt Lokalbereich
red_local = 1-0.57

#Reduktion Netzentgelt Regionalbereich
red_regional = 1-0.28

#_____________ Heat _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_heat = 0.046


#_____________ Cooling _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_cool = 0.046


#---------------------- DEFINITION OF CONSUMERS ----------------------

#Consumer 1
consumer1 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_1')
consumer1.addElectricitySector()
consumer1.addHeatSector()
consumer1.addCoolingSector()
consumer1.addWaterSector()
consumer1.addWasteSector()
consumer1.addTransportSector()
consumer1.addSector(sectorname = 'Consumer1_ElectricVehiclesector')
#consumer1.addSector('testsector')
#TESTSTESTE = consumer1.getSector('testsector')
consumer1.sector2system(my_energysystem)


#Consumer 2
consumer2 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_2')
consumer2.addElectricitySector()
consumer2.addHeatSector()
consumer2.addCoolingSector()
consumer2.addWaterSector()
consumer2.addWasteSector()
consumer2.addTransportSector()
consumer2.addSector(sectorname = 'Consumer2_ElectricVehiclesector')
consumer2.sector2system(my_energysystem)

#Consumer 3
consumer3 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_3')
consumer3.addElectricitySector()
consumer3.addHeatSector()
consumer3.addCoolingSector()
consumer3.addWaterSector()
consumer3.addWasteSector()
consumer3.addTransportSector()
consumer3.addSector(sectorname = 'Consumer3_ElectricVehiclesector')
consumer3.sector2system(my_energysystem)

#Consumer 4
consumer4 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_4')
consumer4.addElectricitySector()
consumer4.addHeatSector()
consumer4.addCoolingSector()
consumer4.addWaterSector()
consumer4.addWasteSector()
consumer4.addTransportSector()
consumer4.addSector(sectorname = 'Consumer4_ElectricVehiclesector')
consumer4.sector2system(my_energysystem)

#Consumer 5
consumer5 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_5')
consumer5.addElectricitySector()
consumer5.addHeatSector()
consumer5.addCoolingSector()
consumer5.addWaterSector()
consumer5.addWasteSector()
consumer5.addTransportSector()
consumer5.addSector(sectorname = 'Consumer5_ElectricVehiclesector')
consumer5.sector2system(my_energysystem)

#Consumer 6
consumer6 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_6')
consumer6.addElectricitySector()
consumer6.addHeatSector()
consumer6.addCoolingSector()
consumer6.addWaterSector()
consumer6.addWasteSector()
consumer6.addTransportSector()
consumer6.addSector(sectorname = 'Consumer6_ElectricVehiclesector')
consumer6.sector2system(my_energysystem)

#Consumer 7
consumer7 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_7')
consumer7.addElectricitySector()
consumer7.addHeatSector()
consumer7.addCoolingSector()
consumer7.addWaterSector()
consumer7.addWasteSector()
consumer7.addTransportSector()
consumer7.addSector(sectorname = 'Consumer7_ElectricVehiclesector')
consumer7.sector2system(my_energysystem)

#Consumer 8
consumer8 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_8')
consumer8.addElectricitySector()
consumer8.addHeatSector()
consumer8.addCoolingSector()
consumer8.addWaterSector()
consumer8.addWasteSector()
consumer8.addTransportSector()
consumer8.addSector(sectorname = 'Consumer8_ElectricVehiclesector')
consumer8.sector2system(my_energysystem)

#Consumer 9
consumer9 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_9')
consumer9.addElectricitySector()
consumer9.addHeatSector()
consumer9.addCoolingSector()
consumer9.addWaterSector()
consumer9.addWasteSector()
consumer9.addTransportSector()
consumer9.addSector(sectorname = 'Consumer9_ElectricVehiclesector')
consumer9.sector2system(my_energysystem)

#Consumer 10
consumer10 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_10')
consumer10.addElectricitySector()
consumer10.addHeatSector()
consumer10.addCoolingSector()
consumer10.addWaterSector()
consumer10.addWasteSector()
consumer10.addTransportSector()
consumer10.addSector(sectorname = 'Consumer10_ElectricVehiclesector')
consumer10.sector2system(my_energysystem)

#Consumer 11
consumer11 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_11')
consumer11.addElectricitySector()
consumer11.addHeatSector()
consumer11.addCoolingSector()
consumer11.addWaterSector()
consumer11.addWasteSector()
consumer11.addTransportSector()
consumer11.addSector(sectorname = 'Consumer11_ElectricVehiclesector')
consumer11.sector2system(my_energysystem)

#Consumer 12
consumer12 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_12')
consumer12.addElectricitySector()
consumer12.addHeatSector()
consumer12.addCoolingSector()
consumer12.addWaterSector()
consumer12.addWasteSector()
consumer12.addTransportSector()
consumer12.addSector(sectorname = 'Consumer12_ElectricVehiclesector')
consumer12.sector2system(my_energysystem)

"""
#Consumer 13
consumer13 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_13')
consumer13.addElectricitySector()
consumer13.addHeatSector()
consumer13.addCoolingSector()
consumer13.addWaterSector()
consumer13.addWasteSector()
consumer13.sector2system(my_energysystem)
"""


#LSC
LSC = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC')
LSC.addEmissionSector()
LSC.addElectricitySector()
LSC.addHeatSector()
LSC.addCoolingSector()
LSC.sector2system(my_energysystem)


print("----------------Consumer Data loaded----------------")

#---------------------- Consumer 1 Components ----------------------

#_____________ Electricity _____________

#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer1.getElectricitySector(), 
                            demand_timeseries=consumer1.input_data['electricityDemand'], label='Electricitydemand_' + consumer1.label)
my_energysystem.add(electricitydemand.component())
consumer1.addComponent(electricitydemand)

#PV
pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_' + consumer1.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10)
my_energysystem.add(pv.component())
consumer1.addComponent(pv)           


#Electricity feedin
electricitygridFeedin = el_comp.GridFeedin(sector=consumer1.getElectricitySector(), revenues=consumer1.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + consumer1.label, timesteps=timesteps)
my_energysystem.add(electricitygridFeedin.component())
consumer1.addComponent(electricitygridFeedin)


#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer1.getElectricityGridSector(), sector_electricity=consumer1.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer1.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer1.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer1.addComponent(electricitygridpurchase.get_source())
consumer1.addComponent(electricitygridpurchase.get_toElec())


#Battery
battery = el_comp.Battery(sector_in=consumer1.getElectricitySector(), label='Battery_' + consumer1.label,
                          soc_max=5, SOC_start=5, P_in=5, P_out=5)   
my_energysystem.add(battery.component())
consumer1.addComponent(battery)

#Heat pump
heatpump = el_comp.Heatpump(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector(), 
                            conversion_timeseries=consumer1.input_data['copHP'], timesteps=timesteps, P_in=7, P_out=7, label='Heatpump_' + consumer1.label)
my_energysystem.add(heatpump.component())
consumer1.addComponent(heatpump)

#Electric Cooling
elcool = el_comp.ElectricCooling(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=consumer1.input_data['copCool'], P_in=7, P_out=7, label="Cooler_" + consumer1.label)
my_energysystem.add(elcool.component())
consumer1.addComponent(elcool)

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer1.input_data['electricityCosts'], consumer1.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer1.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer1.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer1.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer1.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer1.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer1.addComponent(lsc2elec)


#_____________ Heat _____________
#District heat grid purchase    
dhgrid = heat_comp.GridPurchaseEmissions(sector=consumer1.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer1.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_1', costs_power=0)        
my_energysystem.add(dhgrid.component())
consumer1.addComponent(dhgrid)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer1.getHeatSector(), demand_timeseries=consumer1.input_data['heatDemand'], label='Heatdemand_' + consumer1.label)
my_energysystem.add(heatdemand.component())
consumer1.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer1.getHotwaterSector(), demand_timeseries=consumer1.input_data['hotwaterDemand'], label='Hotwater_' + consumer1.label)
my_energysystem.add(hotwaterdemand.component())
consumer1.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer1.label)
my_energysystem.add(hotwaterBoiler.component())
consumer1.addComponent(hotwaterBoiler)

#Thermal storage
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer1.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_' + consumer1.label)
my_energysystem.add(thermalStorage.component())
consumer1.addComponent(thermalStorage)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer1.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer1.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer1.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer1.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer1.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer1.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer1.addComponent(lsc2heat)


#_____________ Cooling _____________
#District cooling grid purchase
coolgrid = cool_comp.GridPurchaseEmissions(sector=consumer1.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer1.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_1', costs_power=0)        
my_energysystem.add(coolgrid.component())
consumer1.addComponent(coolgrid)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer1.getCoolingSector(), demand_timeseries=consumer1.input_data['coolingDemand'], label='Coolingdemand_' + consumer1.label)
my_energysystem.add(cooldemand.component())
consumer1.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer1.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer1.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer1.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer1.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer1.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer1.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer1.addComponent(lsc2cool)

#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer1.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer1.label, color='purple',
                                                      demand=consumer1.input_data['waterDemand'], limit=1, costs_water=consumer1.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer1.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer1.getPotablewaterSector(), sector_out=consumer1.getSewageSector(), 
                                     demand=consumer1.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_1')
my_energysystem.add(waterDemand.component())
consumer1.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer1.getSewageSector(), costs=consumer1.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_1')        
my_energysystem.add(sewageDisposal.component())
consumer1.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer1.getWasteSector(), waste_timeseries=consumer1.input_data['waste'], 
                                    label='WasteAccruing_' + consumer1.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer1.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer1.getWasteSector(), sector_out=consumer1.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer1.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer1.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer1.getWastedisposalSector(), costs=consumer1.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_1")        
my_energysystem.add(wasteDisposal.component())
consumer1.addComponent(wasteDisposal)


#_____________ Transport _____________

#Transport demand
mobilitydemand = trans_comp.Demand(sector=consumer1.getTransportSector(), demand_timeseries=consumer1.input_data['transportDemand'], label='Transport_' + consumer1.label, label_sankey='Transport_' + consumer1.label)
my_energysystem.add(mobilitydemand.component())
consumer1.addComponent(mobilitydemand)
 
#Vehicle Consumer 1
kia1=trans_comp.ElectricVehicle(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getSector(sectorname ='Consumer1_ElectricVehiclesector'), 
                                           sector_drive=consumer1.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=7.4, P_drive=150, soc_max=64, soc_start=64,
                                           label='KiaSoul1_Consumer_1', labelDrive='KiaSoulDrive1_Consumer_1', label_sankey='KiaSoul_Consumer_1', color='red')  
my_energysystem.add(kia1.component())
my_energysystem.add(kia1.drive())
consumer1.addComponent(kia1) 


#---------------------- Consumer 2 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer2.getElectricitySector(), 
                            demand_timeseries=consumer2.input_data['electricityDemand'], label='Electricitydemand_' + consumer2.label)
my_energysystem.add(electricitydemand.component())
consumer2.addComponent(electricitydemand)

#PV
pv = el_comp.Photovoltaic(sector=consumer2.getElectricitySector(), generation_timeseries=consumer2.input_data['elGen'], 
                          label='PV_' + consumer2.label, timesteps=timesteps, area_efficiency=0.5, area_roof=50, area_factor=10) 
my_energysystem.add(pv.component())
consumer2.addComponent(pv)

#Electricity feedin
electricitygridFeedin = el_comp.GridFeedin(sector=consumer2.getElectricitySector(), revenues=consumer2.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + consumer2.label, timesteps=timesteps)
my_energysystem.add(electricitygridFeedin.component())
consumer2.addComponent(electricitygridFeedin)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer2.getElectricityGridSector(), sector_electricity=consumer2.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer2.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer2.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer2.addComponent(electricitygridpurchase.get_source())
consumer2.addComponent(electricitygridpurchase.get_toElec())

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer2.input_data['electricityCosts'], consumer2.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer2.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer2.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer2.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer2.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer2.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer2.addComponent(lsc2elec)


#_____________ Heat _____________
#District heat grid purchase    
dhgrid2 = heat_comp.GridPurchaseEmissions(sector=consumer2.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer2.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_2', costs_power=0)        
my_energysystem.add(dhgrid2.component())
consumer2.addComponent(dhgrid2)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer2.getHeatSector(), demand_timeseries=consumer2.input_data['heatDemand'], label='Heatdemand_' + consumer2.label)
my_energysystem.add(heatdemand.component())
consumer2.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer2.getHotwaterSector(), demand_timeseries=consumer2.input_data['hotwaterDemand'], label='Hotwater_' + consumer2.label)
my_energysystem.add(hotwaterdemand.component())
consumer2.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer2.getHeatSector(), sector_out=consumer2.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer2.label)
my_energysystem.add(hotwaterBoiler.component())
consumer2.addComponent(hotwaterBoiler)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer2.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer2.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer2.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer2.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer2.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer2.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer2.addComponent(lsc2heat)

#_____________ Cooling _____________
#District cooling grid purchase
coolgrid2 = cool_comp.GridPurchaseEmissions(sector=consumer2.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer2.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_2', costs_power=0)        
my_energysystem.add(coolgrid2.component())
consumer2.addComponent(coolgrid2)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer2.getCoolingSector(), demand_timeseries=consumer2.input_data['coolingDemand'], label='Coolingdemand_' + consumer2.label)
my_energysystem.add(cooldemand.component())
consumer2.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer2.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer2.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer2.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer2.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer2.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer2.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer2.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer2.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer2.label, color='purple',
                                                      demand=consumer2.input_data['waterDemand'], limit=1, costs_water=consumer2.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer2.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer2.getPotablewaterSector(), sector_out=consumer2.getSewageSector(), 
                                     demand=consumer2.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_2')
my_energysystem.add(waterDemand.component())
consumer2.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer2.getSewageSector(), costs=consumer2.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_2')        
my_energysystem.add(sewageDisposal.component())
consumer2.addComponent(sewageDisposal)

#_____________ Waste _____________  
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer2.getWasteSector(), waste_timeseries=consumer2.input_data['waste'], 
                                    label='WasteAccruing_' + consumer2.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer2.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer2.getWasteSector(), sector_out=consumer2.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer2.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer2.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer2.getWastedisposalSector(), costs=consumer2.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_2")        
my_energysystem.add(wasteDisposal.component())
consumer2.addComponent(wasteDisposal)


#_____________ Transport _____________
#Transport demand
mobilitydemand2 = trans_comp.Demand(sector=consumer2.getTransportSector(), demand_timeseries=consumer2.input_data['transportDemand'], label='Transport_' + consumer2.label, label_sankey='Transport_' + consumer2.label)
my_energysystem.add(mobilitydemand2.component())
consumer2.addComponent(mobilitydemand2)

#Vehicle Consumer 2
kia2=trans_comp.ElectricVehicle(sector_in=consumer2.getElectricitySector(), sector_out=consumer2.getSector(sectorname ='Consumer2_ElectricVehiclesector'), 
                                           sector_drive=consumer2.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=7.4, P_drive=150, soc_max=64, soc_start=64,
                                           label='KiaSoul2_Consumer_2', labelDrive='KiaSoulDrive2_Consumer_2', label_sankey='KiaSoul_Consumer_2', color='tomato') 
my_energysystem.add(kia2.component())
my_energysystem.add(kia2.drive())
consumer2.addComponent(kia2) 

#---------------------- Consumer 3 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer3.getElectricitySector(), 
                            demand_timeseries=consumer3.input_data['electricityDemand'], label='Electricitydemand_' + consumer3.label)
my_energysystem.add(electricitydemand.component())
consumer3.addComponent(electricitydemand)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer3.getElectricityGridSector(), sector_electricity=consumer3.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer3.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer3.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer3.addComponent(electricitygridpurchase.get_source())
consumer3.addComponent(electricitygridpurchase.get_toElec())

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer3.input_data['electricityCosts'], consumer3.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer3.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer3.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer3.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer3.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer3.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer3.addComponent(lsc2elec)



#_____________ Heat _____________

#District heat grid purchase    
dhgrid3 = heat_comp.GridPurchaseEmissions(sector=consumer3.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer3.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_3', costs_power=0)        
my_energysystem.add(dhgrid3.component())
consumer3.addComponent(dhgrid3)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer3.getHeatSector(), demand_timeseries=consumer3.input_data['heatDemand'], label='Heatdemand_' + consumer3.label)
my_energysystem.add(heatdemand.component())
consumer3.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer3.getHotwaterSector(), demand_timeseries=consumer3.input_data['hotwaterDemand'], label='Hotwater_' + consumer3.label)
my_energysystem.add(hotwaterdemand.component())
consumer3.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer3.getHeatSector(), sector_out=consumer3.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer3.label)
my_energysystem.add(hotwaterBoiler.component())
consumer3.addComponent(hotwaterBoiler)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer3.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer3.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer3.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer3.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer3.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer3.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer3.addComponent(lsc2heat)

#_____________ Cooling _____________
#District cooling grid purchase
coolgrid3 = cool_comp.GridPurchaseEmissions(sector=consumer3.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer3.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_3', costs_power=0)        
my_energysystem.add(coolgrid3.component())
consumer3.addComponent(coolgrid3)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer3.getCoolingSector(), demand_timeseries=consumer3.input_data['coolingDemand'], label='Coolingdemand_' + consumer3.label)
my_energysystem.add(cooldemand.component())
consumer3.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer3.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer3.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer3.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer3.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer3.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer3.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer3.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer3.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer3.label, color='purple',
                                                      demand=consumer3.input_data['waterDemand'], limit=1, costs_water=consumer3.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer3.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer3.getPotablewaterSector(), sector_out=consumer3.getSewageSector(), 
                                     demand=consumer3.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_3')
my_energysystem.add(waterDemand.component())
consumer3.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer3.getSewageSector(), costs=consumer3.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_3')        
my_energysystem.add(sewageDisposal.component())
consumer3.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer3.getWasteSector(), waste_timeseries=consumer3.input_data['waste'], 
                                    label='WasteAccruing_' + consumer3.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer3.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer3.getWasteSector(), sector_out=consumer3.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer3.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer3.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer3.getWastedisposalSector(), costs=consumer3.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_3")        
my_energysystem.add(wasteDisposal.component())
consumer3.addComponent(wasteDisposal)

    
#_____________ Transport _____________

#Transport demand
mobilitydemand3 = trans_comp.Demand(sector=consumer3.getTransportSector(), demand_timeseries=consumer3.input_data['transportDemand'], label='Transport_' + consumer3.label, label_sankey='Transport_' + consumer3.label)
my_energysystem.add(mobilitydemand3.component())
consumer3.addComponent(mobilitydemand3)

#Vehicle Consumer 3
nissan1=trans_comp.ElectricVehicle(sector_in=consumer3.getElectricitySector(), sector_out=consumer3.getSector(sectorname ='Consumer3_ElectricVehiclesector'), 
                                           sector_drive=consumer3.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=3.7, P_drive=110, soc_max=40, soc_start=40,
                                           label='NissanLeaf1_Consumer_3', labelDrive='NissanLeafDrive1_Consumer_3', label_sankey='NissanLeaf_Consumer_3', color='magenta')
my_energysystem.add(nissan1.component())
my_energysystem.add(nissan1.drive())
consumer3.addComponent(nissan1) 


#---------------------- Consumer 4 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer4.getElectricitySector(), 
                            demand_timeseries=consumer4.input_data['electricityDemand'], label='Electricitydemand_' + consumer4.label)
my_energysystem.add(electricitydemand.component())
consumer4.addComponent(electricitydemand)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer4.getElectricityGridSector(), sector_electricity=consumer4.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer4.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer4.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer4.addComponent(electricitygridpurchase.get_source())
consumer4.addComponent(electricitygridpurchase.get_toElec())

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer4.input_data['electricityCosts'], consumer4.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer4.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer4.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer4.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer4.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer4.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer4.addComponent(lsc2elec)



#_____________ Heat _____________
#District heat grid purchase    
dhgrid4 = heat_comp.GridPurchaseEmissions(sector=consumer4.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer4.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_4', costs_power=0)        
my_energysystem.add(dhgrid4.component())
consumer4.addComponent(dhgrid4)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer4.getHeatSector(), demand_timeseries=consumer4.input_data['heatDemand'], label='Heatdemand_' + consumer4.label)
my_energysystem.add(heatdemand.component())
consumer4.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer4.getHotwaterSector(), demand_timeseries=consumer4.input_data['hotwaterDemand'], label='Hotwater_' + consumer4.label)
my_energysystem.add(hotwaterdemand.component())
consumer4.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer4.getHeatSector(), sector_out=consumer4.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer4.label)
my_energysystem.add(hotwaterBoiler.component())
consumer4.addComponent(hotwaterBoiler)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer4.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer4.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer4.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer4.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer4.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer4.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer4.addComponent(lsc2heat)


#_____________ Cooling _____________
#District cooling grid purchase
coolgrid4 = cool_comp.GridPurchaseEmissions(sector=consumer4.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer4.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_4', costs_power=0)        
my_energysystem.add(coolgrid4.component())
consumer4.addComponent(coolgrid4)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer4.getCoolingSector(), demand_timeseries=consumer4.input_data['coolingDemand'], label='Coolingdemand_' + consumer4.label)
my_energysystem.add(cooldemand.component())
consumer4.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer4.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer4.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer4.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer4.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer4.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer4.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer4.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer4.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer4.label, color='purple',
                                                      demand=consumer4.input_data['waterDemand'], limit=1, costs_water=consumer4.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer4.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer4.getPotablewaterSector(), sector_out=consumer4.getSewageSector(), 
                                     demand=consumer4.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_4')
my_energysystem.add(waterDemand.component())
consumer4.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer4.getSewageSector(), costs=consumer4.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_4')        
my_energysystem.add(sewageDisposal.component())
consumer4.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer4.getWasteSector(), waste_timeseries=consumer4.input_data['waste'], 
                                    label='WasteAccruing_' + consumer4.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer4.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer4.getWasteSector(), sector_out=consumer4.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer4.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer4.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer4.getWastedisposalSector(), costs=consumer4.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_4")        
my_energysystem.add(wasteDisposal.component())
consumer4.addComponent(wasteDisposal)



#_____________ Transport _____________

#Transport demand
mobilitydemand4 = trans_comp.Demand(sector=consumer4.getTransportSector(), demand_timeseries=consumer4.input_data['transportDemand'], label='Transport_' + consumer4.label, label_sankey='Transport_' + consumer4.label)
my_energysystem.add(mobilitydemand4.component())
consumer4.addComponent(mobilitydemand4)

#Vehicle Consumer 4
nissan2=trans_comp.ElectricVehicle(sector_in=consumer4.getElectricitySector(), sector_out=consumer4.getSector(sectorname ='Consumer4_ElectricVehiclesector'), 
                                           sector_drive=consumer4.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=3.7, P_drive=110, soc_max=40, soc_start=40,
                                           label='NissanLeaf2_Consumer_4', labelDrive='NissanLeafDrive2_Consumer_4', label_sankey='NissanLeaf_Consumer_4', color='mediumvioletred')  
my_energysystem.add(nissan2.component())
my_energysystem.add(nissan2.drive())
consumer4.addComponent(nissan2) 
    

#---------------------- Consumer 5 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer5.getElectricitySector(), 
                            demand_timeseries=consumer5.input_data['electricityDemand'], label='Electricitydemand_' + consumer5.label)
my_energysystem.add(electricitydemand.component())
consumer5.addComponent(electricitydemand)

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer5.input_data['electricityCosts'], consumer5.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer5.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer5.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer5.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer5.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer5.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer5.addComponent(lsc2elec)


#PV
pv = el_comp.Photovoltaic(sector=consumer5.getElectricitySector(), generation_timeseries=consumer2.input_data['elGen'], 
                          label='PV_' + consumer5.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10) 
my_energysystem.add(pv.component())
consumer5.addComponent(pv)

#Electricity feedin
electricitygridFeedin = el_comp.GridFeedin(sector=consumer5.getElectricitySector(), revenues=consumer5.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + consumer5.label, timesteps=timesteps)
my_energysystem.add(electricitygridFeedin.component())
consumer5.addComponent(electricitygridFeedin)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer5.getElectricityGridSector(), sector_electricity=consumer5.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer5.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer5.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer5.addComponent(electricitygridpurchase.get_source())
consumer5.addComponent(electricitygridpurchase.get_toElec())


#_____________ Heat _____________
#District heat grid purchase    
dhgrid5 = heat_comp.GridPurchaseEmissions(sector=consumer5.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer5.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_5', costs_power=0)        
my_energysystem.add(dhgrid5.component())
consumer5.addComponent(dhgrid5)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer5.getHeatSector(), demand_timeseries=consumer5.input_data['heatDemand'], label='Heatdemand_' + consumer5.label)
my_energysystem.add(heatdemand.component())
consumer5.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer5.getHotwaterSector(), demand_timeseries=consumer5.input_data['hotwaterDemand'], label='Hotwater_' + consumer5.label)
my_energysystem.add(hotwaterdemand.component())
consumer5.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer5.getHeatSector(), sector_out=consumer5.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer5.label)
my_energysystem.add(hotwaterBoiler.component())
consumer5.addComponent(hotwaterBoiler)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer5.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer5.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer5.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer5.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer5.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer5.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer5.addComponent(lsc2heat)

#_____________ Cooling _____________
#District cooling grid purchase
coolgrid5 = cool_comp.GridPurchaseEmissions(sector=consumer5.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer5.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_5', costs_power=0)        
my_energysystem.add(coolgrid5.component())
consumer5.addComponent(coolgrid5)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer5.getCoolingSector(), demand_timeseries=consumer5.input_data['coolingDemand'], label='Coolingdemand_' + consumer5.label)
my_energysystem.add(cooldemand.component())
consumer5.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer5.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer5.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer5.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer5.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer5.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer5.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer5.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer5.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer5.label, color='purple',
                                                      demand=consumer5.input_data['waterDemand'], limit=1, costs_water=consumer5.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer5.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer5.getPotablewaterSector(), sector_out=consumer5.getSewageSector(), 
                                     demand=consumer5.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_5')
my_energysystem.add(waterDemand.component())
consumer5.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer5.getSewageSector(), costs=consumer5.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_5')        
my_energysystem.add(sewageDisposal.component())
consumer5.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer5.getWasteSector(), waste_timeseries=consumer5.input_data['waste'], 
                                    label='WasteAccruing_' + consumer5.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer5.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer5.getWasteSector(), sector_out=consumer5.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer5.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer5.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer5.getWastedisposalSector(), costs=consumer5.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_5")        
my_energysystem.add(wasteDisposal.component())
consumer5.addComponent(wasteDisposal)


#_____________ Transport _____________
#Transport demand
mobilitydemand5 = trans_comp.Demand(sector=consumer5.getTransportSector(), demand_timeseries=consumer5.input_data['transportDemand'], label='Transport_' + consumer5.label, label_sankey='Transport_' + consumer5.label)
my_energysystem.add(mobilitydemand5.component())
consumer5.addComponent(mobilitydemand5)

#Vehicle Consumer 5
zoe1=trans_comp.ElectricVehicle(sector_in=consumer5.getElectricitySector(), sector_out=consumer5.getSector(sectorname ='Consumer5_ElectricVehiclesector'), 
                                           sector_drive=consumer5.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=70, soc_max=40, soc_start=40,
                                           label='RenaultZoe1_Consumer_5', labelDrive='RenaultZoeDrive1_Consumer_5', label_sankey='RenaultZoe_Consumer_5', color='blue') 
my_energysystem.add(zoe1.component())
my_energysystem.add(zoe1.drive())
consumer5.addComponent(zoe1)  
    

#---------------------- Consumer 6 Components ----------------------

#_____________ Electricity _____________

#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer6.getElectricitySector(), 
                            demand_timeseries=consumer6.input_data['electricityDemand'], label='Electricitydemand_' + consumer6.label)
my_energysystem.add(electricitydemand.component())
consumer6.addComponent(electricitydemand)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer6.getElectricityGridSector(), sector_electricity=consumer6.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer6.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer6.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer6.addComponent(electricitygridpurchase.get_source())
consumer6.addComponent(electricitygridpurchase.get_toElec())


#Heat pump
heatpump = el_comp.Heatpump(sector_in = consumer6.getElectricitySector(), sector_out=consumer6.getHeatSector(), 
                            conversion_timeseries=consumer6.input_data['copHP'], timesteps=timesteps, P_in=7, P_out=7, label='Heatpump_' + consumer6.label)
my_energysystem.add(heatpump.component())
consumer6.addComponent(heatpump)

#Electric Cooling
elcool = el_comp.ElectricCooling(sector_in=consumer6.getElectricitySector(), sector_out=consumer6.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=consumer6.input_data['copCool'], P_in=7, P_out=7, label="Cooler_" + consumer6.label)
my_energysystem.add(elcool.component())
consumer6.addComponent(elcool)

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer6.input_data['electricityCosts'], consumer6.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer6.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer6.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer6.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer6.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer6.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer6.addComponent(lsc2elec)


#_____________ Heat _____________
#District heat grid purchase    
dhgrid6 = heat_comp.GridPurchaseEmissions(sector=consumer6.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer6.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_6', costs_power=0)        
my_energysystem.add(dhgrid6.component())
consumer6.addComponent(dhgrid6)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer6.getHeatSector(), demand_timeseries=consumer6.input_data['heatDemand'], label='Heatdemand_' + consumer6.label)
my_energysystem.add(heatdemand.component())
consumer6.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer6.getHotwaterSector(), demand_timeseries=consumer6.input_data['hotwaterDemand'], label='Hotwater_' + consumer6.label)
my_energysystem.add(hotwaterdemand.component())
consumer6.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer6.getHeatSector(), sector_out=consumer6.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer6.label)
my_energysystem.add(hotwaterBoiler.component())
consumer6.addComponent(hotwaterBoiler)

#Thermal storage
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer6.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_' + consumer6.label)
my_energysystem.add(thermalStorage.component())
consumer6.addComponent(thermalStorage)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer6.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer6.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer6.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer6.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer6.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer6.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer6.addComponent(lsc2heat)


#_____________ Cooling _____________
#District cooling grid purchase
coolgrid6 = cool_comp.GridPurchaseEmissions(sector=consumer6.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer6.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_6', costs_power=0)        
my_energysystem.add(coolgrid6.component())
consumer6.addComponent(coolgrid6)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer6.getCoolingSector(), demand_timeseries=consumer6.input_data['coolingDemand'], label='Coolingdemand_' + consumer6.label)
my_energysystem.add(cooldemand.component())
consumer6.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer6.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer6.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer6.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer6.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer6.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer6.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer6.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer6.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer6.label, color='purple',
                                                      demand=consumer6.input_data['waterDemand'], limit=1, costs_water=consumer6.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer6.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer6.getPotablewaterSector(), sector_out=consumer6.getSewageSector(), 
                                     demand=consumer6.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_6')
my_energysystem.add(waterDemand.component())
consumer6.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer6.getSewageSector(), costs=consumer6.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_6')        
my_energysystem.add(sewageDisposal.component())
consumer6.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer6.getWasteSector(), waste_timeseries=consumer6.input_data['waste'], 
                                    label='WasteAccruing_' + consumer6.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer6.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer6.getWasteSector(), sector_out=consumer6.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer6.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer6.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer6.getWastedisposalSector(), costs=consumer6.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_6")        
my_energysystem.add(wasteDisposal.component())
consumer6.addComponent(wasteDisposal)


#_____________ Transport _____________
#Transport demand
mobilitydemand6 = trans_comp.Demand(sector=consumer6.getTransportSector(), demand_timeseries=consumer6.input_data['transportDemand'], label='Transport_' + consumer6.label, label_sankey='Transport_' + consumer6.label)
my_energysystem.add(mobilitydemand6.component())
consumer6.addComponent(mobilitydemand6)

#Vehicle Consumer 6
zoe2=trans_comp.ElectricVehicle(sector_in=consumer6.getElectricitySector(), sector_out=consumer6.getSector(sectorname ='Consumer6_ElectricVehiclesector'), 
                                           sector_drive=consumer6.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=70, soc_max=40, soc_start=40,
                                           label='RenaultZoe2_Consumer_6', labelDrive='RenaultZoeDrive2_Consumer_6', label_sankey='RenaultZoe_Consumer_6', color='skyblue')
my_energysystem.add(zoe2.component())
my_energysystem.add(zoe2.drive())
consumer6.addComponent(zoe2) 


#---------------------- Consumer 7 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer7.getElectricitySector(), 
                            demand_timeseries=consumer7.input_data['electricityDemand'], label='Electricitydemand_' + consumer7.label)
my_energysystem.add(electricitydemand.component())
consumer7.addComponent(electricitydemand)

#PV
pv = el_comp.Photovoltaic(sector=consumer7.getElectricitySector(), generation_timeseries=consumer7.input_data['elGen'], 
                          label='PV_' + consumer7.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10) 
my_energysystem.add(pv.component())
consumer7.addComponent(pv)

#Electricity feedin
electricitygridFeedin = el_comp.GridFeedin(sector=consumer7.getElectricitySector(), revenues=consumer7.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + consumer7.label, timesteps=timesteps)
my_energysystem.add(electricitygridFeedin.component())
consumer7.addComponent(electricitygridFeedin)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer7.getElectricityGridSector(), sector_electricity=consumer7.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer7.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer7.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer7.addComponent(electricitygridpurchase.get_source())
consumer7.addComponent(electricitygridpurchase.get_toElec())

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer7.input_data['electricityCosts'], consumer7.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer7.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer7.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer7.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer7.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer7.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer7.addComponent(lsc2elec)



#_____________ Heat _____________
#District heat grid purchase    
dhgrid7 = heat_comp.GridPurchaseEmissions(sector=consumer7.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer7.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_7', costs_power=0)        
my_energysystem.add(dhgrid7.component())
consumer7.addComponent(dhgrid7)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer7.getHeatSector(), demand_timeseries=consumer7.input_data['heatDemand'], label='Heatdemand_' + consumer7.label)
my_energysystem.add(heatdemand.component())
consumer7.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer7.getHotwaterSector(), demand_timeseries=consumer7.input_data['hotwaterDemand'], label='Hotwater_' + consumer7.label)
my_energysystem.add(hotwaterdemand.component())
consumer7.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer7.getHeatSector(), sector_out=consumer7.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer7.label)
my_energysystem.add(hotwaterBoiler.component())
consumer7.addComponent(hotwaterBoiler)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer7.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer7.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer7.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer7.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer7.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer7.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer7.addComponent(lsc2heat)


#_____________ Cooling _____________
#District cooling grid purchase
coolgrid7 = cool_comp.GridPurchaseEmissions(sector=consumer7.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer7.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_7', costs_power=0)        
my_energysystem.add(coolgrid7.component())
consumer7.addComponent(coolgrid7)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer7.getCoolingSector(), demand_timeseries=consumer7.input_data['coolingDemand'], label='Coolingdemand_' + consumer7.label)
my_energysystem.add(cooldemand.component())
consumer7.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer7.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer7.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer7.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer7.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer7.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer7.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer7.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer7.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer7.label, color='purple',
                                                      demand=consumer7.input_data['waterDemand'], limit=1, costs_water=consumer7.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer7.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer7.getPotablewaterSector(), sector_out=consumer7.getSewageSector(), 
                                     demand=consumer7.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_7')
my_energysystem.add(waterDemand.component())
consumer7.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer7.getSewageSector(), costs=consumer7.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_7')        
my_energysystem.add(sewageDisposal.component())
consumer7.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer7.getWasteSector(), waste_timeseries=consumer7.input_data['waste'], 
                                    label='WasteAccruing_' + consumer7.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer7.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer7.getWasteSector(), sector_out=consumer7.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer7.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer7.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer7.getWastedisposalSector(), costs=consumer7.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_7")        
my_energysystem.add(wasteDisposal.component())
consumer7.addComponent(wasteDisposal)


#_____________ Transport _____________
#Transport demand
mobilitydemand7 = trans_comp.Demand(sector=consumer7.getTransportSector(), demand_timeseries=consumer7.input_data['transportDemand'], label='Transport_' + consumer7.label, label_sankey='Transport_' + consumer7.label)
my_energysystem.add(mobilitydemand7.component())
consumer7.addComponent(mobilitydemand7)

#Vehicle Consumer 7
vw1=trans_comp.ElectricVehicle(sector_in=consumer7.getElectricitySector(), sector_out=consumer7.getSector(sectorname ='Consumer7_ElectricVehiclesector'), 
                                           sector_drive=consumer7.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=107, soc_max=58, soc_start=58,
                                           label='VW1_Consumer_7', labelDrive='VWDrive1_Consumer_7', label_sankey='VWID3_Consumer_7', color='limegreen')
my_energysystem.add(vw1.component())
my_energysystem.add(vw1.drive())
consumer7.addComponent(vw1) 
    

#---------------------- Consumer 8 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer8.getElectricitySector(), 
                            demand_timeseries=consumer8.input_data['electricityDemand'], label='Electricitydemand_' + consumer8.label)
my_energysystem.add(electricitydemand.component())
consumer8.addComponent(electricitydemand)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer8.getElectricityGridSector(), sector_electricity=consumer8.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer8.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer8.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer8.addComponent(electricitygridpurchase.get_source())
consumer8.addComponent(electricitygridpurchase.get_toElec())

#Battery
battery = el_comp.Battery(sector_in=consumer8.getElectricitySector(), label='Battery_' + consumer8.label,
                          soc_max=5, SOC_start=5, P_in=5, P_out=5)   
my_energysystem.add(battery.component())
consumer8.addComponent(battery)

#Heat pump
heatpump = el_comp.Heatpump(sector_in = consumer8.getElectricitySector(), sector_out=consumer8.getHeatSector(), 
                            conversion_timeseries=consumer8.input_data['copHP'], timesteps=timesteps, P_in=7, P_out=7, label='Heatpump_' + consumer8.label)
my_energysystem.add(heatpump.component())
consumer8.addComponent(heatpump)

#Electric Cooling
elcool = el_comp.ElectricCooling(sector_in=consumer8.getElectricitySector(), sector_out=consumer8.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=consumer8.input_data['copCool'], P_in=7, P_out=7, label="Cooler_" + consumer8.label)
my_energysystem.add(elcool.component())
consumer8.addComponent(elcool)

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer8.input_data['electricityCosts'], consumer8.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer8.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer8.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer8.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer8.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer8.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer8.addComponent(lsc2elec)


#_____________ Heat _____________
#District heat grid purchase    
dhgrid8 = heat_comp.GridPurchaseEmissions(sector=consumer8.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer8.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_8', costs_power=0)        
my_energysystem.add(dhgrid8.component())
consumer8.addComponent(dhgrid8)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer8.getHeatSector(), demand_timeseries=consumer8.input_data['heatDemand'], label='Heatdemand_' + consumer8.label)
my_energysystem.add(heatdemand.component())
consumer8.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer8.getHotwaterSector(), demand_timeseries=consumer8.input_data['hotwaterDemand'], label='Hotwater_' + consumer8.label)
my_energysystem.add(hotwaterdemand.component())
consumer8.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer8.getHeatSector(), sector_out=consumer8.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer8.label)
my_energysystem.add(hotwaterBoiler.component())
consumer8.addComponent(hotwaterBoiler)

#Thermal storage
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer8.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_' + consumer8.label)
my_energysystem.add(thermalStorage.component())
consumer8.addComponent(thermalStorage)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer8.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer8.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer8.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer8.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer8.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer8.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer8.addComponent(lsc2heat)

#_____________ Cooling _____________
#District cooling grid purchase
coolgrid8 = cool_comp.GridPurchaseEmissions(sector=consumer8.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer8.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_8', costs_power=0)        
my_energysystem.add(coolgrid8.component())
consumer8.addComponent(coolgrid8)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer8.getCoolingSector(), demand_timeseries=consumer8.input_data['coolingDemand'], label='Coolingdemand_' + consumer8.label)
my_energysystem.add(cooldemand.component())
consumer8.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer8.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer8.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer8.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer8.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer8.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer8.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer8.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer8.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer8.label, color='purple',
                                                      demand=consumer8.input_data['waterDemand'], limit=1, costs_water=consumer8.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer8.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer8.getPotablewaterSector(), sector_out=consumer8.getSewageSector(), 
                                     demand=consumer8.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_8')
my_energysystem.add(waterDemand.component())
consumer8.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer8.getSewageSector(), costs=consumer8.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_8')        
my_energysystem.add(sewageDisposal.component())
consumer8.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer8.getWasteSector(), waste_timeseries=consumer8.input_data['waste'], 
                                    label='WasteAccruing_' + consumer8.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer8.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer8.getWasteSector(), sector_out=consumer8.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer8.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer8.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer8.getWastedisposalSector(), costs=consumer8.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_8")        
my_energysystem.add(wasteDisposal.component())
consumer8.addComponent(wasteDisposal)


#_____________ Transport _____________

#Transport demand
mobilitydemand8 = trans_comp.Demand(sector=consumer8.getTransportSector(), demand_timeseries=consumer8.input_data['transportDemand'], label='Transport_' + consumer8.label, label_sankey='Transport_' + consumer8.label)
my_energysystem.add(mobilitydemand8.component())
consumer8.addComponent(mobilitydemand8)

#Vehicle Consumer 8
vw2=trans_comp.ElectricVehicle(sector_in=consumer8.getElectricitySector(), sector_out=consumer8.getSector(sectorname ='Consumer8_ElectricVehiclesector'), 
                                           sector_drive=consumer8.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=107, soc_max=58, soc_start=58,
                                           label='VW2_Consumer_8', labelDrive='VWDrive2_Consumer_8', label_sankey='VWID3_Consumer_8', color='green')
my_energysystem.add(vw2.component())
my_energysystem.add(vw2.drive())
consumer8.addComponent(vw2) 



#---------------------- Consumer 9 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer9.getElectricitySector(), 
                            demand_timeseries=consumer9.input_data['electricityDemand'], label='Electricitydemand_' + consumer9.label)
my_energysystem.add(electricitydemand.component())
consumer9.addComponent(electricitydemand)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer9.getElectricityGridSector(), sector_electricity=consumer9.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer9.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer9.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer9.addComponent(electricitygridpurchase.get_source())
consumer9.addComponent(electricitygridpurchase.get_toElec())

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer9.input_data['electricityCosts'], consumer9.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer9.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer9.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer9.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer9.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer9.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer9.addComponent(lsc2elec)



#_____________ Heat _____________
#District heat grid purchase    
dhgrid9 = heat_comp.GridPurchaseEmissions(sector=consumer9.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer9.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_9', costs_power=0)        
my_energysystem.add(dhgrid9.component())
consumer9.addComponent(dhgrid9)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer9.getHeatSector(), demand_timeseries=consumer9.input_data['heatDemand'], label='Heatdemand_' + consumer9.label)
my_energysystem.add(heatdemand.component())
consumer9.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer9.getHotwaterSector(), demand_timeseries=consumer9.input_data['hotwaterDemand'], label='Hotwater_' + consumer9.label)
my_energysystem.add(hotwaterdemand.component())
consumer9.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer9.getHeatSector(), sector_out=consumer9.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer9.label)
my_energysystem.add(hotwaterBoiler.component())
consumer9.addComponent(hotwaterBoiler)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer9.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer9.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer9.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer9.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer9.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer9.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer9.addComponent(lsc2heat)


#_____________ Cooling _____________
#District cooling grid purchase
coolgrid9 = cool_comp.GridPurchaseEmissions(sector=consumer9.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer9.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_9', costs_power=0)        
my_energysystem.add(coolgrid9.component())
consumer9.addComponent(coolgrid9)

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer9.getCoolingSector(), demand_timeseries=consumer9.input_data['coolingDemand'], label='Coolingdemand_' + consumer9.label)
my_energysystem.add(cooldemand.component())
consumer9.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer9.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer9.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer9.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer9.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer9.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer9.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer9.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer9.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer9.label, color='purple',
                                                      demand=consumer9.input_data['waterDemand'], limit=1, costs_water=consumer9.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer9.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer9.getPotablewaterSector(), sector_out=consumer9.getSewageSector(), 
                                     demand=consumer9.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_9')
my_energysystem.add(waterDemand.component())
consumer9.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer9.getSewageSector(), costs=consumer9.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_9')        
my_energysystem.add(sewageDisposal.component())
consumer9.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer9.getWasteSector(), waste_timeseries=consumer9.input_data['waste'], 
                                    label='WasteAccruing_' + consumer9.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer9.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer9.getWasteSector(), sector_out=consumer9.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer9.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer9.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer9.getWastedisposalSector(), costs=consumer9.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_9")        
my_energysystem.add(wasteDisposal.component())
consumer9.addComponent(wasteDisposal)


#_____________ Transport _____________
#Transport demand
mobilitydemand9 = trans_comp.Demand(sector=consumer9.getTransportSector(), demand_timeseries=consumer9.input_data['transportDemand'], label='Transport_' + consumer9.label, label_sankey='Transport_' + consumer9.label)
my_energysystem.add(mobilitydemand9.component())
consumer9.addComponent(mobilitydemand9)

#Vehicle Consumer 9
kia3=trans_comp.ElectricVehicle(sector_in=consumer9.getElectricitySector(), sector_out=consumer9.getSector(sectorname ='Consumer9_ElectricVehiclesector'), 
                                           sector_drive=consumer9.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=7.4, P_drive=150, soc_max=64, soc_start=64,
                                           label='KiaSoul3_Consumer_9', labelDrive='KiaSoulDrive3_Consumer_9', label_sankey='KiaSoul_Consumer_9', color='darkred')
my_energysystem.add(kia3.component())
my_energysystem.add(kia3.drive())
consumer9.addComponent(kia3) 

  
#---------------------- Consumer 10 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer10.getElectricitySector(), 
                            demand_timeseries=consumer10.input_data['electricityDemand'], label='Electricitydemand_' + consumer10.label)
my_energysystem.add(electricitydemand.component())
consumer10.addComponent(electricitydemand)

#PV
pv = el_comp.Photovoltaic(sector=consumer10.getElectricitySector(), generation_timeseries=consumer10.input_data['elGen'], 
                          label='PV_' + consumer10.label, timesteps=timesteps, area_efficiency=0.5, area_roof=30, area_factor=10) 
my_energysystem.add(pv.component())
consumer10.addComponent(pv)

#Electricity feedin
electricitygridFeedin = el_comp.GridFeedin(sector=consumer10.getElectricitySector(), revenues=consumer10.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + consumer10.label, timesteps=timesteps)
my_energysystem.add(electricitygridFeedin.component())
consumer10.addComponent(electricitygridFeedin)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer10.getElectricityGridSector(), sector_electricity=consumer10.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer10.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer10.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer10.addComponent(electricitygridpurchase.get_source())
consumer10.addComponent(electricitygridpurchase.get_toElec())


#Battery
battery = el_comp.Battery(sector_in=consumer10.getElectricitySector(), label='Battery_' + consumer10.label,
                          soc_max=5, SOC_start=5, P_in=5, P_out=5)   
my_energysystem.add(battery.component())
consumer10.addComponent(battery)

#Heat pump
heatpump = el_comp.Heatpump(sector_in = consumer10.getElectricitySector(), sector_out=consumer10.getHeatSector(), 
                            conversion_timeseries=consumer10.input_data['copHP'], timesteps=timesteps, P_in=7, P_out=7, label='Heatpump_' + consumer10.label)
my_energysystem.add(heatpump.component())
consumer10.addComponent(heatpump)

#Electric Cooling
elcool = el_comp.ElectricCooling(sector_in=consumer10.getElectricitySector(), sector_out=consumer10.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=consumer10.input_data['copCool'], P_in=7, P_out=7, label="Cooler_" + consumer10.label)
my_energysystem.add(elcool.component())
consumer10.addComponent(elcool)

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer10.input_data['electricityCosts'], consumer10.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer10.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer10.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer10.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer10.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer10.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer10.addComponent(lsc2elec)


#_____________ Heat _____________
#District heat grid purchase    
dhgrid10 = heat_comp.GridPurchaseEmissions(sector=consumer10.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer10.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_10', costs_power=0)        
my_energysystem.add(dhgrid10.component())
consumer10.addComponent(dhgrid10)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer10.getHeatSector(), demand_timeseries=consumer10.input_data['heatDemand'], label='Heatdemand_' + consumer10.label)
my_energysystem.add(heatdemand.component())
consumer10.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer10.getHotwaterSector(), demand_timeseries=consumer10.input_data['hotwaterDemand'], label='Hotwater_' + consumer10.label)
my_energysystem.add(hotwaterdemand.component())
consumer10.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer10.getHeatSector(), sector_out=consumer10.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer10.label)
my_energysystem.add(hotwaterBoiler.component())
consumer10.addComponent(hotwaterBoiler)

#Thermal storage
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer10.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_' + consumer10.label)
my_energysystem.add(thermalStorage.component())
consumer10.addComponent(thermalStorage)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer10.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer10.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer10.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer10.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer10.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer10.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer10.addComponent(lsc2heat)


#_____________ Cooling _____________
#District cooling grid purchase
coolgrid10 = cool_comp.GridPurchaseEmissions(sector=consumer10.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer10.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_10', costs_power=0)        
my_energysystem.add(coolgrid10.component())
consumer10.addComponent(coolgrid10)    

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer10.getCoolingSector(), demand_timeseries=consumer10.input_data['coolingDemand'], label='Coolingdemand_' + consumer10.label)
my_energysystem.add(cooldemand.component())
consumer10.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer10.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer10.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer10.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer10.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer10.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer10.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer10.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer10.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer10.label, color='purple',
                                                      demand=consumer10.input_data['waterDemand'], limit=1, costs_water=consumer10.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer10.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer10.getPotablewaterSector(), sector_out=consumer10.getSewageSector(), 
                                     demand=consumer10.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_10')
my_energysystem.add(waterDemand.component())
consumer10.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer10.getSewageSector(), costs=consumer10.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_10')        
my_energysystem.add(sewageDisposal.component())
consumer10.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer10.getWasteSector(), waste_timeseries=consumer10.input_data['waste'], 
                                    label='WasteAccruing_' + consumer10.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer10.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer10.getWasteSector(), sector_out=consumer10.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer10.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer10.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer10.getWastedisposalSector(), costs=consumer10.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_10")        
my_energysystem.add(wasteDisposal.component())
consumer10.addComponent(wasteDisposal)


#_____________ Transport _____________
#Transport demand
mobilitydemand10 = trans_comp.Demand(sector=consumer10.getTransportSector(), demand_timeseries=consumer10.input_data['transportDemand'], label='Transport_' + consumer10.label, label_sankey='Transport_' + consumer10.label)
my_energysystem.add(mobilitydemand10.component())
consumer10.addComponent(mobilitydemand10)

#Vehicle Consumer 10
vw3=trans_comp.ElectricVehicle(sector_in=consumer10.getElectricitySector(), sector_out=consumer10.getSector(sectorname ='Consumer10_ElectricVehiclesector'), 
                                           sector_drive=consumer10.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=107, soc_max=58, soc_start=58,
                                           label='VW3_Consumer_10', labelDrive='VWDrive3_Consumer_10', label_sankey='VWID3_Consumer_10', color='olivedrab')
my_energysystem.add(vw3.component())
my_energysystem.add(vw3.drive())
consumer10.addComponent(vw3) 

#---------------------- Consumer 11 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer11.getElectricitySector(), 
                            demand_timeseries=consumer11.input_data['electricityDemand'], label='Electricitydemand_' + consumer11.label)
my_energysystem.add(electricitydemand.component())
consumer11.addComponent(electricitydemand)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer11.getElectricityGridSector(), sector_electricity=consumer11.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer11.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer11.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer11.addComponent(electricitygridpurchase.get_source())
consumer11.addComponent(electricitygridpurchase.get_toElec())

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer11.input_data['electricityCosts'], consumer11.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer11.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer11.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer11.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer11.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer11.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer11.addComponent(lsc2elec)


#_____________ Heat _____________
#District heat grid purchase    
dhgrid11 = heat_comp.GridPurchaseEmissions(sector=consumer11.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer11.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_11', costs_power=0)        
my_energysystem.add(dhgrid11.component())
consumer11.addComponent(dhgrid11)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer11.getHeatSector(), demand_timeseries=consumer11.input_data['heatDemand'], label='Heatdemand_' + consumer11.label)
my_energysystem.add(heatdemand.component())
consumer11.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer11.getHotwaterSector(), demand_timeseries=consumer11.input_data['hotwaterDemand'], label='Hotwater_' + consumer11.label)
my_energysystem.add(hotwaterdemand.component())
consumer11.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer11.getHeatSector(), sector_out=consumer11.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer11.label)
my_energysystem.add(hotwaterBoiler.component())
consumer11.addComponent(hotwaterBoiler)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer11.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer11.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer11.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer11.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer11.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer11.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer11.addComponent(lsc2heat)

#_____________ Cooling _____________
#District cooling grid purchase
coolgrid11 = cool_comp.GridPurchaseEmissions(sector=consumer11.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer11.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_11', costs_power=0)        
my_energysystem.add(coolgrid11.component())
consumer11.addComponent(coolgrid11)  

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer11.getCoolingSector(), demand_timeseries=consumer11.input_data['coolingDemand'], label='Coolingdemand_' + consumer11.label)
my_energysystem.add(cooldemand.component())
consumer11.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer11.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer11.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer11.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer11.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer11.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer11.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer11.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer11.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer11.label, color='purple',
                                                      demand=consumer11.input_data['waterDemand'], limit=1, costs_water=consumer11.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer11.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer11.getPotablewaterSector(), sector_out=consumer11.getSewageSector(), 
                                     demand=consumer11.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_11')
my_energysystem.add(waterDemand.component())
consumer11.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer11.getSewageSector(), costs=consumer11.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_11')        
my_energysystem.add(sewageDisposal.component())
consumer11.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer11.getWasteSector(), waste_timeseries=consumer11.input_data['waste'], 
                                    label='WasteAccruing_' + consumer11.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer11.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer11.getWasteSector(), sector_out=consumer11.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer11.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer11.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer11.getWastedisposalSector(), costs=consumer11.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_11")        
my_energysystem.add(wasteDisposal.component())
consumer11.addComponent(wasteDisposal)


#_____________ Transport _____________    
#Transport demand
mobilitydemand11 = trans_comp.Demand(sector=consumer11.getTransportSector(), demand_timeseries=consumer11.input_data['transportDemand'], label='Transport_' + consumer11.label, label_sankey='Transport_' + consumer11.label)
my_energysystem.add(mobilitydemand11.component())
consumer11.addComponent(mobilitydemand11)

#Vehicle Consumer 11
zoe3=trans_comp.ElectricVehicle(sector_in=consumer11.getElectricitySector(), sector_out=consumer11.getSector(sectorname ='Consumer11_ElectricVehiclesector'), 
                                           sector_drive=consumer11.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=70, soc_max=40, soc_start=40,
                                           label='RenaultZoe3_Consumer_11', labelDrive='RenaultZoeDrive3_Consumer_11', label_sankey='RenaultZoe_Consumer_11', color='aqua')  
my_energysystem.add(zoe3.component())
my_energysystem.add(zoe3.drive())
consumer11.addComponent(zoe3) 


#---------------------- Consumer 12 Components ----------------------

#_____________ Electricity _____________
#Electricity Demand
electricitydemand = el_comp.Demand(sector=consumer12.getElectricitySector(), 
                            demand_timeseries=consumer12.input_data['electricityDemand'], label='Electricitydemand_' + consumer12.label)
my_energysystem.add(electricitydemand.component())
consumer12.addComponent(electricitydemand)

#Electricity grid purchase
c_grid_elec=0.062
c_abgabe_elec = 0.018
electricitygridpurchase = el_comp.GridCombinedPowerMetering(sector_grid=consumer12.getElectricityGridSector(), sector_electricity=consumer12.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(consumer12.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer12.label, timesteps=timesteps) 
my_energysystem.add(electricitygridpurchase.get_source().component())
my_energysystem.add(electricitygridpurchase.get_toElec().component())
consumer12.addComponent(electricitygridpurchase.get_source())
consumer12.addComponent(electricitygridpurchase.get_toElec())

#Electricity LSC Sale
sell_tariff_lsc_elec = np.multiply(np.add(consumer12.input_data['electricityCosts'], consumer12.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer12.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer12.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec) 
my_energysystem.add(elec2lsc.component())
consumer12.addComponent(elec2lsc)

#Electricity LSC Purchase
c_grid_elec=0.062
red_local_elec = 1-0.57
c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer12.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer12.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)
my_energysystem.add(lsc2elec.component())
consumer12.addComponent(lsc2elec)



#_____________ Heat _____________
#District heat grid purchase    
dhgrid12 = heat_comp.GridPurchaseEmissions(sector=consumer12.getHeatSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer12.input_data['heatCosts'], 0.046), 
                                         emissions=0.14, timesteps=timesteps, P=10, label='Heatgridpurchase_Consumer_12', costs_power=0)        
my_energysystem.add(dhgrid12.component())
consumer12.addComponent(dhgrid12)

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer12.getHeatSector(), demand_timeseries=consumer12.input_data['heatDemand'], label='Heatdemand_' + consumer12.label)
my_energysystem.add(heatdemand.component())
consumer12.addComponent(heatdemand)

#Hot water demand
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer12.getHotwaterSector(), demand_timeseries=consumer12.input_data['hotwaterDemand'], label='Hotwater_' + consumer12.label)
my_energysystem.add(hotwaterdemand.component())
consumer12.addComponent(hotwaterdemand)

#Hot water boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer12.getHeatSector(), sector_out=consumer12.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer12.label)
my_energysystem.add(hotwaterBoiler.component())
consumer12.addComponent(hotwaterBoiler)

#Thermal storage
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer12.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_' + consumer12.label)
my_energysystem.add(thermalStorage.component())
consumer12.addComponent(thermalStorage)

#Heat LSC Sale
sell_tariff_lsc_heat = np.array(consumer12.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer12.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer12.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
my_energysystem.add(heat2lsc.component())
consumer12.addComponent(heat2lsc)

#Heat LSC Purchase
buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer12.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer12.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
my_energysystem.add(lsc2heat.component())
consumer12.addComponent(lsc2heat)

#_____________ Cooling _____________
#District cooling grid purchase
coolgrid12 = cool_comp.GridPurchaseEmissions(sector=consumer12.getCoolingSector(), sector_emissions=LSC.getEmissionsSector(), costs_energy=np.add(consumer12.input_data['coolingCosts'], 0.046), 
                                           emissions=0.14, timesteps=timesteps, P=10, label='Coolgridpurchase_Consumer_12', costs_power=0)        
my_energysystem.add(coolgrid12.component())
consumer12.addComponent(coolgrid12)  

#Cooling Demand
cooldemand = cool_comp.Demand(sector=consumer12.getCoolingSector(), demand_timeseries=consumer12.input_data['coolingDemand'], label='Coolingdemand_' + consumer12.label)
my_energysystem.add(cooldemand.component())
consumer12.addComponent(cooldemand)

#Cooling LSC Sale
sell_tariff_lsc_cool = np.array(consumer12.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer12.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer12.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
my_energysystem.add(cool2lsc.component())
consumer12.addComponent(cool2lsc)


#Cooling LSC Purchase
buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer12.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer12.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
my_energysystem.add(lsc2cool.component())
consumer12.addComponent(lsc2cool)


#_____________ Water _____________
#Pipeline Purchase
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer12.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer12.label, color='purple',
                                                      demand=consumer12.input_data['waterDemand'], limit=1, costs_water=consumer12.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer12.addComponent(pipelinepurchaseLimited)

#Water demand
waterDemand = water_comp.Waterdemand(sector_in=consumer12.getPotablewaterSector(), sector_out=consumer12.getSewageSector(), 
                                     demand=consumer12.input_data['waterDemand'], timesteps=timesteps, label='Potablewaterdemand_Consumer_12')
my_energysystem.add(waterDemand.component())
consumer12.addComponent(waterDemand)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer12.getSewageSector(), costs=consumer12.input_data['sewageCosts'], 
                                           sector_emissions = LSC.getEmissionsSector(), label='Sewagedisposal_Consumer_12')        
my_energysystem.add(sewageDisposal.component())
consumer12.addComponent(sewageDisposal)

#_____________ Waste _____________
#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=consumer12.getWasteSector(), waste_timeseries=consumer12.input_data['waste'], 
                                    label='WasteAccruing_' + consumer12.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer12.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer12.getWasteSector(), sector_out=consumer12.getWastedisposalSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer12.label, color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage.component())
consumer12.addComponent(wasteStorage)

#Waste Disposal
wasteDisposal = waste_comp.Wastedisposal(sector=consumer12.getWastedisposalSector(), costs=consumer12.input_data['wasteCosts'], 
                                         sector_emissions = LSC.getEmissionsSector(), label="WasteDisposal_Consumer_12")        
my_energysystem.add(wasteDisposal.component())
consumer12.addComponent(wasteDisposal)



#_____________ Transport _____________
#Transport demand
mobilitydemand12 = trans_comp.Demand(sector=consumer12.getTransportSector(), demand_timeseries=consumer12.input_data['transportDemand'], label='Transport_' + consumer12.label, label_sankey='Transport_' + consumer12.label)
my_energysystem.add(mobilitydemand12.component())
consumer12.addComponent(mobilitydemand12)

#Vehicle Consumer 12

nissan3=trans_comp.ElectricVehicle(sector_in=consumer12.getElectricitySector(), sector_out=consumer12.getSector(sectorname ='Consumer12_ElectricVehiclesector'), 
                                           sector_drive=consumer12.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=3.7, P_drive=110, soc_max=40, soc_start=40,
                                           label='NissanLeaf3_Consumer_12', labelDrive='NissanLeafDrive3_Consumer_12', label_sankey='NissanLeaf_Consumer_12', color='hotpink')
my_energysystem.add(nissan3.component())
my_energysystem.add(nissan3.drive())
consumer12.addComponent(nissan3) 




#---------------------- LSC Components ----------------------  

#Electricity Components
c_abgabe_elec = 0.018
elgridLSC = el_comp.GridCombinedPowerMetering(P=100, sector_grid=LSC.getElectricityGridSector(), sector_electricity=LSC.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              sector_emissions = LSC.getEmissionsSector(), emissions=0.209, costs_energy=np.add(LSC.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_LSC', timesteps=timesteps)  
my_energysystem.add(elgridLSC.get_source().component())
my_energysystem.add(elgridLSC.get_toElec().component())
LSC.addComponent(elgridLSC.get_source())
LSC.addComponent(elgridLSC.get_toElec())

#Peakpower
peakpower_elec = 33
c_power_elec = 35
c_power_elec=0
peakpowerCombined = el_comp.PeakpowerCosts(sector=LSC.getPeakpowerSector(),  
                                 label='ElectricitypeakpowerCombined', timesteps=timesteps, P=peakpower_elec, costs_power=c_power_elec)
my_energysystem.add(peakpowerCombined.component())
LSC.addComponent(peakpowerCombined)

#PV
pvLSC = el_comp.Photovoltaic(sector=LSC.getElectricitySector(), generation_timeseries=LSC.input_data['elGen'], 
                          label='PV_LSC', timesteps=timesteps, area_efficiency=0.5, area_roof=400, area_factor=10)
my_energysystem.add(pvLSC.component())
LSC.addComponent(pvLSC)

#ElectricityFeedin
elgridFeedinLSC = el_comp.GridFeedin(sector=LSC.getElectricitySector(), revenues=LSC.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_LSC', timesteps=timesteps)  
my_energysystem.add(elgridFeedinLSC.component())
LSC.addComponent(elgridFeedinLSC)

#Battery
batteryLSC = el_comp.Battery(sector_in=LSC.getElectricitySector(), label='Battery_LSC',
                          soc_max=25, SOC_start=25, P_in=25, P_out=25) 
my_energysystem.add(batteryLSC.component())
LSC.addComponent(batteryLSC)

#Heatpump
heatpumpLSC = el_comp.Heatpump(sector_in = LSC.getElectricitySector(), sector_out=LSC.getHeatSector(), 
                            conversion_timeseries=LSC.input_data['copHP'], timesteps=timesteps, P_in=30, P_out=30, label='Heatpump_LSC') 
my_energysystem.add(heatpumpLSC.component())
LSC.addComponent(heatpumpLSC)

#Electric cooling
elcoolLSC = el_comp.ElectricCooling(sector_in=LSC.getElectricitySector(), sector_out=LSC.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=LSC.input_data['copCool'], P_in=30, P_out=30, label="Cooler_LSC")
my_energysystem.add(elcoolLSC.component())
LSC.addComponent(elcoolLSC)

#Thermal Storage
thermalStorageLSC = heat_comp.ThermalStorage(sector_in=LSC.getHeatSector(), P_in=30, volume_max=1.5, label='Heatstorage_LSC')
my_energysystem.add(thermalStorageLSC.component())
LSC.addComponent(thermalStorageLSC)


#Emissions
emissions_sink = em_comp.Emissions(sector=LSC.getEmissionsSector(), costs=0, label='Emissiontotal_LSC')
my_energysystem.add(emissions_sink.component())
LSC.addComponent(emissions_sink)

#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 

print("----------------Energy System configured----------------")

#--------------------------------------------------------------------


print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
 
    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results(my_energysystem, saka, scenario)
#results = solph.processing.results(saka)
results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")

#consumerList=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, consumer10, consumer11, consumer12, consumer13, LSC]

#Store the costs
resultprocessing_lsc.costs_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                             consumer10, consumer11, consumer12, LSC], 
                                  results=results, filename='costs.csv', scenarioname=scenario, model=saka, timeseries=False)

#Store the results
resultprocessing_lsc.consumer_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                                consumer10, consumer11, consumer12, LSC], 
                                     results=results, filename='technology.csv', scenarioname=scenario)


print("----------------Results saved----------------")






