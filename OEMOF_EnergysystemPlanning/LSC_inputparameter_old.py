# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 08:42:25 2022

@author: Matthias Maldet
"""

def LSC_data():
    
    c_grid_elec = 0.062
    c_abgabe_elec = 0.018
    c_red_local = 0.43
    c_red_regional = 0.72
    c_elec_local = c_red_local*c_grid_elec+c_abgabe_elec
    c_elec_regional = c_red_regional*c_grid_elec+c_abgabe_elec
    
    lsc_struct ={
        
        #_________________________________________ LSC 1 _______________________________________________________
        
        "LSC_1" :{
            #Corresponding LSM
            "LSM" : "LSM_1",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1540,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 1,
            "area_roof": 14000,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1400,
            "battery_soc_start": 0,
            "battery_P_in": 1400,
            "battery_P_out": 1400,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 1,
            "P_elec2lsm": 1540,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435, "LSM_6": 0.1435, "LSM_7": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_local, "LSM_2": 0.1435+c_elec_regional, "LSM_3": 0.1435+c_elec_regional, 
                                    "LSM_4": 0.1435+c_elec_local, "LSM_5": 0.1435+c_elec_regional, "LSM_6": 0.1435+c_elec_regional, "LSM_7": 0.1435+c_elec_local},
            "efficiency_lsm_elec": {"LSM_1": 1, "LSM_2": 0.97986, "LSM_3": 0.97986, "LSM_4": 0.999, "LSM_5": 0.97986, "LSM_6": 0.97986, "LSM_7": 0.99988},
            "P_lsm2elec": 1540,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 980, 
            "P_heatpump_out": 980,
            
            #Thermal storage
            "P_heatstore": 980,
            "v_heatstore": 42,
            
            #Heat sale to LSM Hub
            "P_districtheat": 980,
            "efficiency_lsc_heat": 1,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03, "LSM_6": 0.03, "LSM_7": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076, "LSM_6": 0.076, "LSM_7": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 1, "LSM_2": 0.98, "LSM_3": 0.98, "LSM_4": 0.988, "LSM_5": 0.976, "LSM_6": 0.98, "LSM_7": 0.986},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 2835,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 168,
            "wastestore_balanced": False,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 5/6*1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 5/6*1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1, "LSM_6": 1, "LSM_7": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
            
            
            },
        
        #_________________________________________ LSC 2 _______________________________________________________
        
        "LSC_2" :{
            #Corresponding LSM
            "LSM" : "LSM_2",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1562,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 1,
            "area_roof": 11320,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1420,
            "battery_soc_start": 0,
            "battery_P_in": 1420,
            "battery_P_out": 1420,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 1,
            "P_elec2lsm": 1562,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435, "LSM_6": 0.1435, "LSM_7": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_regional, "LSM_2": 0.1435+c_elec_local, "LSM_3":0.1435+c_elec_local, "LSM_4": 0.1435+c_elec_regional, 
                                    "LSM_5": 0.1435+c_elec_regional, "LSM_6": 0.1435+c_elec_local, "LSM_7": 0.1435+c_elec_regional},
            "efficiency_lsm_elec": {"LSM_1": 0.97986, "LSM_2": 1, "LSM_3": 0.99996, "LSM_4": 0.9798, "LSM_5": 0.97978, "LSM_6": 0.99996, "LSM_7": 0.97978},
            "P_lsm2elec": 1562,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 994, 
            "P_heatpump_out": 994,
            
            #Thermal storage
            "P_heatstore": 994,
            "v_heatstore": 42.6,
            
            #Heat sale to LSM Hub
            "P_districtheat": 994,
            "efficiency_lsc_heat": 1,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03, "LSM_6": 0.03, "LSM_7": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076, "LSM_6": 0.076, "LSM_7": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.98, "LSM_2": 1, "LSM_3": 0.99, "LSM_4": 0.988, "LSM_5": 0.976, "LSM_6": 0.99, "LSM_7": 0.986},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 2979,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 168,
            "wastestore_balanced": False,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 5/6*1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 5/6*1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1, "LSM_6": 1, "LSM_7": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
            
            
        },
        
            
        #_________________________________________ LSC 3 _______________________________________________________
        
        "LSC_3" :{
            #Corresponding LSM
            "LSM" : "LSM_3",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 2090,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 1,
            "area_roof": 15400,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1900,
            "battery_soc_start": 0,
            "battery_P_in": 1900,
            "battery_P_out": 1900,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 1,
            "P_elec2lsm": 2090,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435, "LSM_6": 0.1435, "LSM_7": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_regional, "LSM_2": 0.1435+c_elec_local, "LSM_3":0.1435+c_elec_local, "LSM_4": 0.1435+c_elec_regional, 
                                    "LSM_5": 0.1435+c_elec_regional, "LSM_6": 0.1435+c_elec_local, "LSM_7": 0.1435+c_elec_regional},
            "efficiency_lsm_elec": {"LSM_1": 0.97986, "LSM_2": 0.99996, "LSM_3": 1, "LSM_4": 0.9798, "LSM_5": 0.97978, "LSM_6": 0.99996, "LSM_7": 0.97978},
            "P_lsm2elec": 2090,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 1330, 
            "P_heatpump_out": 1330,
            
            #Thermal storage
            "P_heatstore": 1330,
            "v_heatstore": 57,
            
            #Heat sale to LSM Hub
            "P_districtheat": 1330,
            "efficiency_lsc_heat": 1,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03, "LSM_6": 0.03, "LSM_7": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076, "LSM_6": 0.076, "LSM_7": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.98, "LSM_2": 0.99, "LSM_3": 1, "LSM_4": 0.988, "LSM_5": 0.976, "LSM_6": 0.99, "LSM_7": 0.986},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 2970,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 168,
            "wastestore_balanced": False,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 5/6*1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 5/6*1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1, "LSM_6": 1, "LSM_7": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
            
            
        },
        
        #_________________________________________ LSC 4 _______________________________________________________
        
        "LSC_4" :{
            #Corresponding LSM
            "LSM" : "LSM_4",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1331,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 1,
            "area_roof": 12100,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1210,
            "battery_soc_start": 0,
            "battery_P_in": 1210,
            "battery_P_out": 1210,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 1,
            "P_elec2lsm": 1331,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435, "LSM_6": 0.1435, "LSM_7": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_local, "LSM_2": 0.1435+c_elec_regional, "LSM_3":0.1435+c_elec_regional, "LSM_4": 0.1435+c_elec_local, 
                                    "LSM_5": 0.1435+c_elec_regional, "LSM_6": 0.1435+c_elec_regional, "LSM_7": 0.1435+c_elec_local},
            "efficiency_lsm_elec": {"LSM_1": 0.9999, "LSM_2": 0.9798, "LSM_3": 0.9798, "LSM_4": 1, "LSM_5": 0.97962, "LSM_6": 0.9798, "LSM_7": 0.99982},
            "P_lsm2elec": 1331,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 847, 
            "P_heatpump_out": 847,
            
            #Thermal storage
            "P_heatstore": 847,
            "v_heatstore": 36.3,
            
            #Heat sale to LSM Hub
            "P_districtheat": 847,
            "efficiency_lsc_heat": 1,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03, "LSM_6": 0.03, "LSM_7": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076, "LSM_6": 0.076, "LSM_7": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.988, "LSM_2": 0.988, "LSM_3": 0.988, "LSM_4": 1, "LSM_5": 0.984, "LSM_6": 0.988, "LSM_7": 0.994},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 2394,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 168,
            "wastestore_balanced": False,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 5/6*1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 5/6*1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1, "LSM_6": 1, "LSM_7": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
            
            
        },
        
        #_________________________________________ LSC 5 _______________________________________________________
        
        "LSC_5" :{
            #Corresponding LSM
            "LSM" : "LSM_5",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1507,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 1,
            "area_roof": 10100,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1370,
            "battery_soc_start": 0,
            "battery_P_in": 1370,
            "battery_P_out": 1370,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 1,
            "P_elec2lsm": 1507,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435, "LSM_6": 0.1435, "LSM_7": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_regional, "LSM_2": 0.1435+c_elec_regional, "LSM_3":0.1435+c_elec_regional, "LSM_4": 0.1435+c_elec_regional, 
                                    "LSM_5": 0.1435+c_elec_local, "LSM_6": 0.1435+c_elec_regional, "LSM_7": 0.1435+c_elec_regional},
            "efficiency_lsm_elec": {"LSM_1": 0.97968, "LSM_2": 0.97978, "LSM_3": 0.97978, "LSM_4": 0.97962, "LSM_5": 1, "LSM_6": 0.97978, "LSM_7": 0.9796},
            "P_lsm2elec": 1507,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 1370, 
            "P_heatpump_out": 1370,
            
            #Thermal storage
            "P_heatstore": 1370,
            "v_heatstore": 41,
            
            #Heat sale to LSM Hub
            "P_districtheat": 1370,
            "efficiency_lsc_heat": 1,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03, "LSM_6": 0.03, "LSM_7": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076, "LSM_6": 0.076, "LSM_7": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.976, "LSM_2": 0.976, "LSM_3": 0.976, "LSM_4": 0.984, "LSM_5": 1, "LSM_6": 0.976, "LSM_7": 0.982},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 3051,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 168,
            "wastestore_balanced": False,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 5/6*1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 5/6*1.5, "LSM_6": 1.5, "LSM_7": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1, "LSM_6": 1, "LSM_7": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
            
            
        },
        
        #_________________________________________ LSC 6 _______________________________________________________
        
        "LSC_6" :{
            #Corresponding LSM
            "LSM" : "LSM_6",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 500,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 1,
            "area_roof": 1716,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 100,
            "battery_soc_start": 0,
            "battery_P_in": 100,
            "battery_P_out": 100,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 1,
            "P_elec2lsm": 22,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435, "LSM_6": 0.1435, "LSM_7": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_regional, "LSM_2": 0.1435+c_elec_local, "LSM_3":0.1435+c_elec_local, "LSM_4": 0.1435+c_elec_regional, 
                                    "LSM_5": 0.1435+c_elec_regional, "LSM_6": 0.1435+c_elec_local, "LSM_7": 0.1435+c_elec_regional},
            "efficiency_lsm_elec": {"LSM_1": 0.97986, "LSM_2": 0.99996, "LSM_3": 0.99996, "LSM_4": 0.9798, "LSM_5": 0.97978, "LSM_6": 1, "LSM_7": 0.97978},
            "P_lsm2elec": 22,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 200, 
            "P_heatpump_out": 200,
            
            #Thermal storage
            "P_heatstore": 200,
            "v_heatstore": 8.6,
            
            #Heat sale to LSM Hub
            "P_districtheat": 200,
            "efficiency_lsc_heat": 1,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03, "LSM_6": 0.03, "LSM_7": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076, "LSM_6": 0.076, "LSM_7": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.98, "LSM_2": 0.99, "LSM_3": 0.99, "LSM_4": 0.988, "LSM_5": 0.976, "LSM_6": 1, "LSM_7": 0.986},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 677,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 168,
            "wastestore_balanced": False,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 5/6*1.5, "LSM_7": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 5/6*1.5, "LSM_7": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1, "LSM_6": 1, "LSM_7": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
            
            
        },
        
        #_________________________________________ LSC 7 _______________________________________________________
        
        "LSC_7" :{
            #Corresponding LSM
            "LSM" : "LSM_7",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 500,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 1,
            "area_roof": 2398,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 90,
            "battery_soc_start": 0,
            "battery_P_in": 90,
            "battery_P_out": 90,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 1,
            "P_elec2lsm": 55,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435, "LSM_6": 0.1435, "LSM_7": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_local, "LSM_2": 0.1435+c_elec_regional, "LSM_3":0.1435+c_elec_regional, "LSM_4": 0.1435+c_elec_local, 
                                    "LSM_5": 0.1435+c_elec_regional, "LSM_6": 0.1435+c_elec_regional, "LSM_7": 0.1435+c_elec_local},
            "efficiency_lsm_elec": {"LSM_1": 0.99988, "LSM_2": 0.97978, "LSM_3": 0.97978, "LSM_4": 0.99982, "LSM_5": 0.9796, "LSM_6": 0.97978, "LSM_7": 1},
            "P_lsm2elec": 55,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 200, 
            "P_heatpump_out": 200,
            
            #Thermal storage
            "P_heatstore": 200,
            "v_heatstore": 8.6,
            
            #Heat sale to LSM Hub
            "P_districtheat": 200,
            "efficiency_lsc_heat": 1,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03, "LSM_6": 0.03, "LSM_7": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076, "LSM_6": 0.076, "LSM_7": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.986, "LSM_2": 0.986, "LSM_3": 0.986, "LSM_4": 0.994, "LSM_5": 0.982, "LSM_6": 0.986, "LSM_7": 1},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 2049,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 168,
            "wastestore_balanced": False,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 5/6*1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5, "LSM_6": 1.5, "LSM_7": 5/6*1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1, "LSM_6": 1, "LSM_7": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
            
            
            }

        
        }
    
    return lsc_struct