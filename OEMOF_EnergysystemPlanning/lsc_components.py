# -*- coding: utf-8 -*-
"""
Created on Tue May 10 13:41:48 2022

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np
import transformerLossesMultipleOut as tlmo
import source_emissions as em
import transportDelay as transportDelay


#_____________ Delay Component for transmission delays _____________
class Delay_Transport():
    
    def __init__(self, *args, **kwargs):
        
        efficiency=1
        position='Consumer_1'
        delay=0
        costs_transmission=0
        transmissionCapacity=1000000
        deltaT=1
        timesteps=8760

        

        self.transmissionCapacity = kwargs.get("capacity_transmission", transmissionCapacity)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.efficiency = kwargs.get("efficiency", efficiency)
        self.costs_transmission = kwargs.get("costs_transmission", costs_transmission)
        self.costs_out = kwargs.get("c_out", 0)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.delay = kwargs.get("delay", delay)
        self.position = kwargs.get("position", position)
        label = 'Transmission_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Transmission_' + self.position)
        color='aquamarine'
        self.color = kwargs.get("color", color)
        self.timesteps=kwargs.get("timesteps", timesteps)
        
    def component(self):
        x_in = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs = self.costs_transmission)
        x_out = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs=self.costs_out)
            
            
            
        transmission = transportDelay.TransportDelay(
            label=self.label,
            inputs={self.sector_in : x_in},
            outputs={self.sector_out : x_out},
            sector_in = self.sector_in, sector_out=self.sector_out,
            efficiency=self.efficiency, delay=self.delay, timesteps=self.timesteps)

        
        return transmission
    
class Transport_emissions():
    
    def __init__(self, *args, **kwargs):
        

        position='Consumer_1'
        transmissionCapacity=1000000
        deltaT=1
        timesteps=8760
        
        emissions=0.125

        costs_transmission=0

        self.transmissionCapacity = kwargs.get("capacity_transmission", transmissionCapacity)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.sector_emissions = kwargs.get("sector_emissions")
        self.emissions=kwargs.get("emissions", emissions)
        self.distance=kwargs.get("distance", 1)

        self.costs_transmission = kwargs.get("costs_transmission", costs_transmission)
        self.position = kwargs.get("position", position)
        label = 'TransmissionEmissions_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'TransmissionEmissions_' + self.position)
        color='azure'
        self.color = kwargs.get("color", color)
        self.timesteps=kwargs.get("timesteps", timesteps)
        
    def component(self):
        x_in = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs = self.costs_transmission)
        x_out = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs=0)
        em_out = solph.Flow(min=0, max=1, nominal_value=1000000, variable_costs=0)
            
        transportEmissions = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : x_in},
                outputs={self.sector_out : x_out, self.sector_emissions : em_out},
                conversion_factors={self.sector_out : 1, self.sector_emissions : self.emissions*self.distance/self.transmissionCapacity})

        
        return transportEmissions
    
class Transport_emissions_extended():
    
    def __init__(self, *args, **kwargs):
        

        position='Consumer_1'
        transmissionCapacity=1000000
        deltaT=1
        timesteps=8760
        
        emissions=0.125

        

        self.transmissionCapacity = kwargs.get("capacity_transmission", transmissionCapacity)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.sector_emissions = kwargs.get("sector_emissions")
        self.emissions=kwargs.get("emissions", emissions)
        self.distance=kwargs.get("distance", 1)
        self.costs_transmission = kwargs.get("costs_transmission", 0)

        self.position = kwargs.get("position", position)
        label = 'TransmissionEmissions_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'TransmissionEmissions_' + self.position)
        color='azure'
        self.color = kwargs.get("color", color)
        self.timesteps=kwargs.get("timesteps", timesteps)
        
    def component(self):
        x_in = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs = self.costs_transmission)
        x_out = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs=0)
        em_out = solph.Flow(min=0, max=1, nominal_value=1000000, variable_costs=0)
            
        transportEmissions = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : x_in},
                outputs={self.sector_out : x_out, self.sector_emissions : em_out},
                conversion_factors={self.sector_out : 1, self.sector_emissions : self.emissions*self.distance/self.transmissionCapacity})

        
        return transportEmissions
    


# Sewage Transformer 1:!
class Sewage2Hub():
    
    def __init__(self, *args, **kwargs):
        
        efficiency=1
        position='Consumer_1'
        delay=0
        costs_transmission=0
        transmissionCapacity=1000000
        deltaT=1
        timesteps=8760

        

        self.transmissionCapacity = kwargs.get("capacity_transmission", transmissionCapacity)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.efficiency = kwargs.get("efficiency", efficiency)
        self.costs_transmission = kwargs.get("costs_transmission", costs_transmission)
        self.costs_out = kwargs.get("c_out", 0)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.delay = kwargs.get("delay", delay)
        self.position = kwargs.get("position", position)
        label = 'Sewage2Hub_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sewage2Hub_' + self.position)
        color='aquamarine'
        self.color = kwargs.get("color", color)
        self.timesteps=kwargs.get("timesteps", timesteps)
        
    def component(self):
        x_in = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs = self.costs_transmission)
        x_out = solph.Flow(min=0, max=1, nominal_value=self.transmissionCapacity*self.deltaT, variable_costs=self.costs_out)
            
            
        sewage2hub = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : x_in},
                outputs={self.sector_out : x_out},
                conversion_factors={self.sector_out : self.efficiency})

        
        return sewage2hub    
    

#_____________ Electricity _____________

#Selling Electricity to the LSC
class Electricity2LSC():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0
            PowerInLimit = 11
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_consumer = kwargs.get("rev_consumer", tariff)
            self.rev_consumer=np.multiply(rev_consumer, -1)
            self.c_lsc = kwargs.get("c_lsc", rev_consumer)
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Elec2LSC_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Elec2LSC_' + self.position)
            color = 'olivedrab'
            self.color = kwargs.get("color", color)

        def component(self):
            q_consumer2lscIn = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.rev_consumer)
            q_consumer2lscOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_lsc)
            elec2LSC = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_consumer2lscIn},
                outputs={self.sector_out : q_consumer2lscOut},
                conversion_factors={self.sector_out : self.efficiency})
            return elec2LSC
        
  
#Purchasing Electricity from the LSC
class LSC2electricity():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0
            PowerInLimit = 11
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_lsc = kwargs.get("rev_lsc", tariff)
            self.rev_lsc=np.multiply(rev_lsc, -1)
            self.c_consumer = kwargs.get("c_consumer", rev_lsc)
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'LSC2Elec_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'LSC2Elec_' + self.position)
            color = 'dodgerblue'
            self.color = kwargs.get("color", color)

        def component(self):
            q_lsc2consumerIn = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.rev_lsc)
            q_lsc2consumerOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_consumer)
            LSC2elec = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_lsc2consumerIn},
                outputs={self.sector_out : q_lsc2consumerOut},
                conversion_factors={self.sector_out : self.efficiency})
            return LSC2elec



#_____________ Heat _____________

#Selling Heat to the LSC
class Heat2LSC():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_consumer = kwargs.get("rev_consumer", tariff)
            self.rev_consumer=np.multiply(rev_consumer, -1)
            self.c_lsc = kwargs.get("c_lsc", rev_consumer)
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Heat2LSC_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heat2LSC_' + self.position)
            color = 'olivedrab'
            self.color = kwargs.get("color", color)

        def component(self):
            q_consumer2lscIn = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.rev_consumer)
            q_consumer2lscOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_lsc)
            heat2LSC = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_consumer2lscIn},
                outputs={self.sector_out : q_consumer2lscOut},
                conversion_factors={self.sector_out : self.efficiency})
            return heat2LSC



#Purchasing Heat from the LSC
class LSC2heat():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_lsc = kwargs.get("rev_lsc", tariff)
            self.rev_lsc=np.multiply(rev_lsc, -1)
            self.c_consumer = kwargs.get("c_consumer", rev_lsc)
            
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'LSC2Heat_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'LSC2Heat_' + self.position)
            color = 'dodgerblue'
            self.color = kwargs.get("color", color)

        def component(self):
            q_lsc2consumerIn = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.rev_lsc)
            q_lsc2consumerOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_consumer)
            LSC2heat = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_lsc2consumerIn},
                outputs={self.sector_out : q_lsc2consumerOut},
                conversion_factors={self.sector_out : self.efficiency})
            return LSC2heat



#_____________ Cooling _____________

#Selling Heat to the LSC
class Cool2LSC():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_consumer = kwargs.get("rev_consumer", tariff)
            self.rev_consumer=np.multiply(rev_consumer, -1)
            self.c_lsc = kwargs.get("c_lsc", rev_consumer)
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Cool2LSC_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Cool2LSC_' + self.position)
            color = 'olivedrab'
            self.color = kwargs.get("color", color)

        def component(self):
            q_consumer2lscIn = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.rev_consumer)
            q_consumer2lscOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_lsc)
            cool2LSC = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_consumer2lscIn},
                outputs={self.sector_out : q_consumer2lscOut},
                conversion_factors={self.sector_out : self.efficiency})
            return cool2LSC



#Purchasing Heat from the LSC
class LSC2cool():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_lsc = kwargs.get("rev_lsc", tariff)
            self.rev_lsc=np.multiply(rev_lsc, -1)
            self.c_consumer = kwargs.get("c_consumer", rev_lsc)
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'LSC2Cool_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'LSC2Cool_' + self.position)
            color = 'dodgerblue'
            self.color = kwargs.get("color", color)

        def component(self):
            q_lsc2consumerIn = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.rev_lsc)
            q_lsc2consumerOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_consumer)
            LSC2cool = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_lsc2consumerIn},
                outputs={self.sector_out : q_lsc2consumerOut},
                conversion_factors={self.sector_out : self.efficiency})
            return LSC2cool



#_____________ Water _____________


#Flexibility to water
#Additional input that represents the flexibility for water demand
#Input: Consumer Waterdemandsector
#Output: LSC Waterpoolsector
class WaterFlexibility():
    def __init__(self, *args, **kwargs):
        share_waterSewage=0.95
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        waterlimit = 100000
        
        #Willingness for Flexibility - default 0.5
        wff=0.5
        
        discount=1


        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'WaterFlexibility_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDemand\nSewage_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        color='rosybrown'
        self.color = kwargs.get("color", color)
        self.waterlimit= kwargs.get("waterlimit", waterlimit)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        self.wff = kwargs.get("wff", wff)
        
        self.demandFlexibility = np.multiply(self.demand, self.wff)
        
        self.demandMax = max(self.demand)
        
        costsPipelineArray = np.zeros(self.timesteps)
        self.costs_lsc = kwargs.get("costs_lsc", costsPipelineArray)
        self.discount = kwargs.get("discount", discount)
        self.costs_lsc = np.multiply(self.costs_lsc, self.discount)
        
        self.revenues_consumer = kwargs.get('revenues_consumer', 0)
        self.revenues_consumer = np.multiply(self.revenues_consumer, self.discount)
        
        
    def component(self):
        

        v_flex = solph.Flow(min=-self.demandFlexibility, max=0, nominal_value=1, variable_costs=self.revenues_consumer)
        v_toPool = solph.Flow(min=0, max=self.demandFlexibility, nominal_value=1, variable_costs=self.costs_lsc)
        
        #Conversion to sewage sector
        #Aggregation process to more consumers
        waterFlexibility = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_flex},
            outputs={self.sector_out : v_toPool},
            conversion_factors={self.sector_out : -1})
        
        return waterFlexibility  
        
class WaterFlexibilitySimple():
    def __init__(self, *args, **kwargs):
        share_waterSewage=0.95
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        waterlimit = 100000
        
        #Willingness for Flexibility - default 0.5
        wff=0.5
        
        discount=1


        self.sector_in= kwargs.get("sector_in")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'WaterFlexibility_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDemand\nSewage_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        color='rosybrown'
        self.color = kwargs.get("color", color)
        self.waterlimit= kwargs.get("waterlimit", waterlimit)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        self.wff = kwargs.get("wff", wff)
        
        self.demandFlexibility = np.multiply(self.demand, self.wff)
        
        self.demandMax = max(self.demand)
        self.discount = kwargs.get("discount", discount)

        
        self.revenues_consumer = kwargs.get('revenues_consumer', 0)
        self.revenues_consumer = np.multiply(self.revenues_consumer, self.discount)
        
        
    def component(self):
        v_flex = solph.Flow(min=0, max=self.demandFlexibility, nominal_value=1, variable_costs=self.revenues_consumer)
        
        waterFlexibility = solph.Sink(label=self.label, inputs={self.sector_in: v_flex})
        
        return waterFlexibility      

class WaterWasted():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000
        discount=1

        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'PoolwaterWasted_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Poolwater\nWasted_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='brown'
        self.color = kwargs.get("color", color)
        

        costsPipelineArray = np.zeros(self.timesteps)
            
        self.costs_pipeline = kwargs.get("costs_water", costsPipelineArray)
        self.discount = kwargs.get("discount", discount)
        self.costs_pipeline = np.multiply(self.costs_pipeline, self.discount)
        
        
    def component(self):
 
        v_poolwaterWasted = solph.Flow(nominal_value=self.V_max*self.deltaT, variable_costs=self.costs_pipeline)
        

        poolWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_poolwaterWasted})
        
        return poolWasted
    

#LSC Water purchase from consumers
#Input is the LSC watersector
#Output the water sector for the considered consumer    
class LSCWaterPurchase():
    
    def __init__(self, *args, **kwargs):
        V_connection = 100000
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        limit=1
        discount = 1

        
        self.V= kwargs.get("V", V_connection)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'WaterLSCpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nLSC\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsPipelineArray = np.zeros(self.timesteps)
            
        self.costs_pipeline = kwargs.get("costs_consumer", costsPipelineArray)
        self.discount = kwargs.get("discount", discount)
        self.costs_pipeline = np.multiply(self.costs_pipeline, self.discount)
        
        self.revenues_lsc = kwargs.get('revenues_lsc', 0)
        self.revenues_lsc = np.multiply(self.revenues_lsc, -self.discount)
        
        demandWater = np.zeros(self.timesteps)
        self.demand = kwargs.get("demand", demandWater)
        
        self.limit = kwargs.get("limit", limit)
        
        self.summedLimit = np.multiply(np.sum(self.demand), self.limit)
        
        self.efficiency= kwargs.get("efficiency", 1)
        
        
        
    def component(self):
        
        v_in = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.revenues_lsc, summed_max=self.summedLimit/(self.V*self.deltaT))
        v_out = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_pipeline, summed_max=self.summedLimit/(self.V*self.deltaT))
            
        
        waterLSCconsumer = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_in},
            outputs={self.sector_out : v_out},
            conversion_factors={self.sector_out : self.efficiency})
        
        return waterLSCconsumer    
    
    
    
class WaterrecoveryTransformer():
    
    def __init__(self, *args, **kwargs):
        V_connection = 100000
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        limit=1
        discount = 1

        
        self.V= kwargs.get("V", V_connection)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'WaterrecoveryTransformer_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waterrecovery\nTransformer_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='indigo'
        self.color = kwargs.get("color", color)
                
        
        
    def component(self):
        
        v_in = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=0)
        v_out = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=0)
            
        
        waterrecoveryTransformer = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_in},
            outputs={self.sector_out : v_out},
            conversion_factors={self.sector_out : 1})
        
        return waterrecoveryTransformer       
    
    
#_____________ Waste _____________

class WasteFlexibility():
    
    def __init__(self, *args, **kwargs):

        deltaT=1
        position='Consumer_1'
        timesteps=8760
        wfr=0
        share_recycling=1
        
        
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Wasteflexibility_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nFlexibility_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsArray = np.zeros(self.timesteps)
        
        self.revenues=kwargs.get("revenues", costsArray)
        self.revenues=np.multiply(self.revenues, -1)
        color='green'
        self.color = kwargs.get("color", color)
        
        
        accruingWaste = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("waste", accruingWaste)
        self.wfr = kwargs.get("wfr", wfr)
        
        self.demandFlexibility = np.multiply(self.demand, self.wfr)
        
        self.demandMax = max(self.demand)
        
        #Share of the waste that can be recycled
        self.share_recycling = kwargs.get("share_recycling", share_recycling)
        
        self.demandFlexibility=np.multiply(self.demandFlexibility, self.share_recycling)
        
    def component(self):


                
        m_wasteFlexibility = solph.Flow(min=0, max=self.demandFlexibility, nominal_value=1, variable_costs=self.revenues)
            
        wasteFlexibility = solph.Sink(label=self.label, inputs={self.sector_in: m_wasteFlexibility})
        

        
        return wasteFlexibility



