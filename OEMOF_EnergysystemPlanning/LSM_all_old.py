# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 10:51:17 2022

@author: Matthias Maldet
"""


import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc

import investment_components as investment_comp

import consumer_lsm as consumer_lsm
import LSC_inputparameter as LSC_inputparameter
import LSM_inputparameter as LSM_inputparameter

import constraints_LSM as constraints_LSM



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'LSM_all_year'

#Filename
filename = "input_data_LSM.xlsx"


#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 8784
timestepsTotal = 8784

data=pd.ExcelFile(filename)


#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1

yearWeightFactor=timesteps/timestepsTotal


#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2


#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
#Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    


#---------------------- DEFINITION OF CONSUMERS ----------------------
#LSC1 / LSM1
lsc1 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_1')
lsc1.addElectricitySector()
lsc1.addHeatSector()
lsc1.addDistrictheatingSector()
lsc1.addWaterSector()
lsc1.addWasteSector()
lsc1.addEmissionSector()
lsc1.sector2system(my_energysystem)

lsm1 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_1')
lsm1.addElectricitySector()
lsm1.addHeatSector()
lsm1.addWaterSector()
lsm1.addWasteSector()
lsm1.addEmissionSector()
lsm1.sector2system(my_energysystem)

#LSC2 / LSM2
lsc2 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_2')
lsc2.addElectricitySector()
lsc2.addHeatSector()
lsc2.addDistrictheatingSector()
lsc2.addWaterSector()
lsc2.addWasteSector()
lsc2.addEmissionSector()
lsc2.sector2system(my_energysystem)

lsm2 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_2')
lsm2.addElectricitySector()
lsm2.addHeatSector()
lsm2.addWaterSector()
lsm2.addWasteSector()
lsm2.addEmissionSector()
lsm2.sector2system(my_energysystem)

#LSC3 / LSM3
lsc3 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_3')
lsc3.addElectricitySector()
lsc3.addHeatSector()
lsc3.addDistrictheatingSector()
lsc3.addWaterSector()
lsc3.addWasteSector()
lsc3.addEmissionSector()
lsc3.sector2system(my_energysystem)

lsm3 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_3')
lsm3.addElectricitySector()
lsm3.addHeatSector()
lsm3.addWaterSector()
lsm3.addWasteSector()
lsm3.addEmissionSector()
lsm3.sector2system(my_energysystem)

#LSC4 / LSM4
lsc4 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_4')
lsc4.addElectricitySector()
lsc4.addHeatSector()
lsc4.addDistrictheatingSector()
lsc4.addWaterSector()
lsc4.addWasteSector()
lsc4.addEmissionSector()
lsc4.sector2system(my_energysystem)

lsm4 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_4')
lsm4.addElectricitySector()
lsm4.addHeatSector()
lsm4.addWaterSector()
lsm4.addWasteSector()
lsm4.addEmissionSector()
lsm4.sector2system(my_energysystem)

#LSC5 / LSM5
lsc5 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_5')
lsc5.addElectricitySector()
lsc5.addHeatSector()
lsc5.addDistrictheatingSector()
lsc5.addWaterSector()
lsc5.addWasteSector()
lsc5.addEmissionSector()
lsc5.sector2system(my_energysystem)

lsm5 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_5')
lsm5.addElectricitySector()
lsm5.addHeatSector()
lsm5.addWaterSector()
lsm5.addWasteSector()
lsm5.addEmissionSector()
lsm5.sector2system(my_energysystem)

#LSC6 / LSM6
lsc6 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_6')
lsc6.addElectricitySector()
lsc6.addHeatSector()
lsc6.addDistrictheatingSector()
lsc6.addWaterSector()
lsc6.addWasteSector()
lsc6.addEmissionSector()
lsc6.sector2system(my_energysystem)

lsm6 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_6')
lsm6.addElectricitySector()
lsm6.addHeatSector()
lsm6.addWaterSector()
lsm6.addWasteSector()
lsm6.addEmissionSector()
lsm6.sector2system(my_energysystem)

#LSC7 / LSM7
lsc7 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_7')
lsc7.addElectricitySector()
lsc7.addHeatSector()
lsc7.addDistrictheatingSector()
lsc7.addWaterSector()
lsc7.addWasteSector()
lsc7.addEmissionSector()
lsc7.sector2system(my_energysystem)

lsm7 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_7')
lsm7.addElectricitySector()
lsm7.addHeatSector()
lsm7.addWaterSector()
lsm7.addWasteSector()
lsm7.addEmissionSector()
lsm7.sector2system(my_energysystem)



print("----------------Consumer Data loaded----------------")

#Sets for LSC and LSM data
lscData = LSC_inputparameter.LSC_data()
lsmData = LSM_inputparameter.LSM_data()


#---------------------- LSC 1 Components ----------------------

#Assign components to consumer and energy system

#----------------------------- LSC 1 / LSM 1 -----------------------------------------------
#LSC 1
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
IDlsc = lsc1.label
lsccomponents1 = consumer_lsm.lsc_setup(LSC=lsc1, LSM=lsm1, LSM_list=LSM_list, timesteps=timesteps, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )

for component in lsccomponents1:
    my_energysystem.add(component.component())
    lsc1.addComponent(component)
    
#LSM Hub 1   
IDlsm = lsm1.label
lsmcomponents1 = consumer_lsm.lsm_setup(LSM=lsm1, LSM_list=LSM_list, timesteps=timesteps,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"]
                                        )

for component in lsmcomponents1:
    my_energysystem.add(component.component())
    lsm1.addComponent(component)
    

#----------------------------- LSC 2 / LSM 2 -----------------------------------------------
#LSC 2
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
IDlsc = lsc2.label
lsccomponents2 = consumer_lsm.lsc_setup(LSC=lsc2, LSM=lsm2, LSM_list=LSM_list, timesteps=timesteps, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )

for component in lsccomponents2:
    my_energysystem.add(component.component())
    lsc2.addComponent(component)
    
    
#LSM Hub 2    
IDlsm = lsm2.label
lsmcomponents2 = consumer_lsm.lsm_setup(LSM=lsm2, LSM_list=LSM_list, timesteps=timesteps,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"]
                                        )



for component in lsmcomponents2:
    my_energysystem.add(component.component())
    lsm2.addComponent(component)
    

#----------------------------- LSC 3 / LSM 3 -----------------------------------------------
#LSC 3
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
IDlsc = lsc3.label
lsccomponents3 = consumer_lsm.lsc_setup(LSC=lsc3, LSM=lsm3, LSM_list=LSM_list, timesteps=timesteps, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )

for component in lsccomponents3:
    my_energysystem.add(component.component())
    lsc3.addComponent(component)
    
    
#LSM Hub 3    
IDlsm = lsm3.label
lsmcomponents3 = consumer_lsm.lsm_setup(LSM=lsm3, LSM_list=LSM_list, timesteps=timesteps,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"]
                                        )



for component in lsmcomponents3:
    my_energysystem.add(component.component())
    lsm3.addComponent(component)

  
    
#----------------------------- LSC 4 / LSM 4 -----------------------------------------------
#LSC 4
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
IDlsc = lsc4.label
lsccomponents4 = consumer_lsm.lsc_setup(LSC=lsc4, LSM=lsm4, LSM_list=LSM_list, timesteps=timesteps, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )

for component in lsccomponents4:
    my_energysystem.add(component.component())
    lsc4.addComponent(component)
    
    
#LSM Hub 4    
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
lsmcomponents4 = consumer_lsm.lsm_setup(LSM=lsm4, LSM_list=LSM_list, timesteps=timesteps,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"]
                                        )



for component in lsmcomponents4:
    my_energysystem.add(component.component())
    lsm4.addComponent(component)



#----------------------------- LSC 5 / LSM 5 -----------------------------------------------
#LSC 5
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
IDlsc = lsc5.label
lsccomponents5 = consumer_lsm.lsc_setup(LSC=lsc5, LSM=lsm5, LSM_list=LSM_list, timesteps=timesteps, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )

for component in lsccomponents5:
    my_energysystem.add(component.component())
    lsc5.addComponent(component)
    
    
#LSM Hub 5    
IDlsm = lsm5.label
lsmcomponents5 = consumer_lsm.lsm_setup(LSM=lsm5, LSM_list=LSM_list, timesteps=timesteps,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"]
                                        )



for component in lsmcomponents5:
    my_energysystem.add(component.component())
    lsm5.addComponent(component)



#----------------------------- LSC 6 / LSM 6 -----------------------------------------------
#LSC 6
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
IDlsc = lsc6.label
lsccomponents6 = consumer_lsm.lsc_setup(LSC=lsc6, LSM=lsm6, LSM_list=LSM_list, timesteps=timesteps, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )

for component in lsccomponents6:
    my_energysystem.add(component.component())
    lsc6.addComponent(component)
    
    
#LSM Hub 6    
IDlsm = lsm6.label
lsmcomponents6 = consumer_lsm.lsm_setup(LSM=lsm6, LSM_list=LSM_list, timesteps=timesteps,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"]
                                        )



for component in lsmcomponents6:
    my_energysystem.add(component.component())
    lsm6.addComponent(component)
    

    
#----------------------------- LSC 7 / LSM 7 -----------------------------------------------
#LSC 7
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5, lsm6, lsm7]
IDlsc = lsc7.label
lsccomponents7 = consumer_lsm.lsc_setup(LSC=lsc7, LSM=lsm7, LSM_list=LSM_list, timesteps=timesteps, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )

for component in lsccomponents7:
    my_energysystem.add(component.component())
    lsc7.addComponent(component)
    
    
#LSM Hub 7    
IDlsm = lsm7.label
lsmcomponents7 = consumer_lsm.lsm_setup(LSM=lsm7, LSM_list=LSM_list, timesteps=timesteps,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"]
                                        )



for component in lsmcomponents7:
    my_energysystem.add(component.component())
    lsm7.addComponent(component)

 


#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 

print("----------------Energy System configured----------------")



#---------------------- Additional constraints ----------------------





#Sewage treatment
saka = constraints_LSM.lsm_sewagetreatment(model=saka, label="Sewagetreatment rule", timesteps=timesteps)


#Waste transport input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_6", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_7", lsm_list=lsm_list, capacity=100000)


#Sewage transmission input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_6", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5", "LSM_6", "LSM_7"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_7", lsm_list=lsm_list, capacity=100000)




#----------------------- Solve the model ----------------------


print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")

    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results(my_energysystem, saka, scenario)
#results = solph.processing.results(saka)
results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")


#--------------------------- Store the results --------------------------------


"""
#consumerList=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, consumer10, consumer11, consumer12, consumer13, LSC]

#Store the costs
resultprocessing_lsc.costs_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                             consumer10, consumer11, consumer12, LSC], 
                                  results=results, filename='costs.csv', scenarioname=scenario, model=saka, timeseries=False)

#Store the results
resultprocessing_lsc.consumer_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                                consumer10, consumer11, consumer12, LSC], 
                                     results=results, filename='technology.csv', scenarioname=scenario)

"""

print("----------------Results saved----------------")


#--------------------------- Code evaluation --------------------------------

#Timeseries
#test=solph.views.node(results, pv_invest.label).get("sequences")

#Investment
#solph.views.node(results, pv_invest.label).get("scalars")[0]

#Investment Satus
#solph.views.node(results, pv_invest.label).get("scalars")[1]

#Bei speicher anders!!!

