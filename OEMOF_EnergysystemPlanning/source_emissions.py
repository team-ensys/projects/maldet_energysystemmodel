# -*- coding: utf-8 -*-
"""
Created on Thu Dec 23 06:24:18 2021

@author: Matthias Maldet
"""

from oemof.network import network as on

from pyomo.core.base.block import SimpleBlock
from pyomo.environ import Constraint
from pyomo.environ import Set


class Source(on.Source):
    """An object with one output flow."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.emissions= kwargs.get("emissions", 0)
        self.emissions_sector= kwargs.get("emissions_sector", 0)
        self.conversion_sector = kwargs.get("conversion_sector", 0)
        

    def constraint_group(self):
        return Emissionconstraint
    

class Emissionconstraint(SimpleBlock):
    

    CONSTRAINT_GROUP = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _create(self, group=None):
        """Creates the relation for the class:`OffsetTransformer`.
        Parameters
        ----------
        group : list
            List of oemof.solph.custom.OffsetTransformer objects for which
            the relation of inputs and outputs is created
            e.g. group = [ostf1, ostf2, ostf3, ...]. The components inside
            the list need to hold an attribute `coefficients` of type dict
            containing the conversion factors for all inputs to outputs.
        """
        if group is None:
            return None

        m = self.parent_block()
        
        out_flows = {m: [i for i in m.outputs.keys()] for m in group}
        
        
        
        valueConversion=[]
        valueEmissions=[]
        
        #Hier Auswahlverfahren einbauen
        

        counter=0
        for n in group:
            n.index=counter
            list2Check = list(out_flows.values())[counter]
            
            for val in list2Check:
                if(str(n.conversion_sector) in str(val)):
                    valueConversion.append(val)
                elif(str(n.emissions_sector) in str(val)):
                    valueEmissions.append(val)
                    
            
            counter+=1
        
        
        self.SOURCEEMISSIONS = Set(initialize=[n for n in group])
        
        def _relation_rule1(block, n, t):
            expr=0

            #print(n.losses)
            expr -= n.emissions*sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(out_flows.keys())[n.index]) == i.label and str(valueConversion[n.index]) == o.label)
            expr += sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(out_flows.keys())[n.index]) == i.label and str(valueEmissions[n.index]) == o.label)

            return expr == 0



        self.relation = Constraint(
            self.SOURCEEMISSIONS, m.TIMESTEPS, rule=_relation_rule1)

