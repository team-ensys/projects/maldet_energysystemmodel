# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 13:31:29 2021

@author: Matthias Maldet
"""


from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import transformerLossesMultipleOut as tlmo
import StorageIntermediate as storage_int
import numpy as np

class Pipeline():
    
    def __init__(self, *args, **kwargs):
        V_connection = 100000
        deltaT=1
        position='Consumer_1'
        costs=1000
        timesteps=8760
        
        self.V= kwargs.get("V", V_connection)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Waterpipeline_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nPipeline_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color='slategrey'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        v_waterpipeline = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_model)
            
        waterpipeline = solph.Source(label=self.label, outputs={self.sector_out: v_waterpipeline})
        
        return waterpipeline
    
    
class PipelinePurchase():
    
    def __init__(self, *args, **kwargs):
        V_connection = 100000
        C_fix = 28.33
        deltaT=1
        position='Consumer_1'
        timesteps=8760

        
        self.V= kwargs.get("V", V_connection)
        self.costs_fixed = kwargs.get("costs_fixed", C_fix)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Waterpipelinepurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nPipeline\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsPipelineArray = np.zeros(self.timesteps)
            
        self.costs_pipeline = kwargs.get("costs_water", costsPipelineArray)
        
        
    def component(self):
        v_waterpipeline = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_pipeline)
            
        waterpipeline = solph.Source(label=self.label, outputs={self.sector_out: v_waterpipeline})
        
        return waterpipeline     
    
#Block only used if no greywater components at all should be considered
#The demandblock with greywater behaves the same, if the greywater share is set to zero   
#Only difference is, that the waterdemand must be set to an own sector 
class Waterdemand():
    def __init__(self, *args, **kwargs):
        share_waterSewage=0.95
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        sewage=True

        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Potablewaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDemand_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.sewage = kwargs.get("sewage", sewage)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        color='yellow'
        self.color = kwargs.get("color", color)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        if(self.sewage):
        
            v_sewageIn = solph.Flow(fix=self.demand, nominal_value=1)
            v_sewageOut = solph.Flow(min=0, max=1, nominal_value=self.demandMax*self.share_sewage)
        
            #Conversion to sewage sector
            #Aggregation process to more consumers
            water2sewage = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageIn},
                    outputs={self.sector_out : v_sewageOut},
                    conversion_factors={self.sector_out : self.share_sewage})
        
            return water2sewage  
        
        else:
            v_waterIn = solph.Flow(fix=self.demand, nominal_value=1)

            waterDemand = solph.Sink(label=self.label, inputs={self.sector_in: v_waterIn})
            
            return waterDemand

        
        
class SewageSink():
    
    def __init__(self, *args, **kwargs):
        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        v_max = 1000000

        self.sector_in= kwargs.get("sector")
        self.v_max = kwargs.get("V_max", v_max)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sewagesink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sewage\nSink_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.costs_in=0
        color='maroon'
        self.color = kwargs.get("color", color)

        
        
    def component(self):
        v_sewage = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs_in)
            
        sewageSink = solph.Sink(label=self.label, inputs={self.sector_in: v_sewage})
        
        return sewageSink
    
    
class SludgeSink():
    
    def __init__(self, *args, **kwargs):
        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        v_max = 1000000

        self.sector_in= kwargs.get("sector")
        self.v_max = kwargs.get("V_max", v_max)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sludgesink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sludge\nSink_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.costs_in=0
        color='dimgray'
        self.color = kwargs.get("color", color)

        
        
    def component(self):
        v_sewage = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs_in)
            
        sludgeSink = solph.Sink(label=self.label, inputs={self.sector_in: v_sewage})
        
        return sludgeSink
    
    
    
class SewageTreatment_noElec():

        def __init__(self, *args, **kwargs):
            
            v_max = 1000000
            
            tempdif=3
            share_waterSewage=0.95
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000
            
            eta=0.9
            
            Ct_sludge = 9.2
            rho_sludge = 1800
            
            
            costsIn=0.04
            costsOut=[0, 0, 0]
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.tempdif = kwargs.get("tempdif", tempdif)
            self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.eta = kwargs.get("eta", eta)
            self.v_max = kwargs.get("V_max", v_max)
            
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Sewagetreatment_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sewage\nTreatment_' + self.position)
            color='black'
            self.color = kwargs.get("color", color)
            
            self.limit=[self.v_max, self.v_max*self.concentration_sludge/self.rho_sludge, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]
            
            self.conversion_factor = [self.eta, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]

        def component(self):
            v_sewageWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs_in)
            v_cleanWater = solph.Flow(min=0, max=1, nominal_value=self.limit[0], variable_costs=self.costs_out[0])
            v_sludgeSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[1], variable_costs=self.costs_out[1])
            q_heatSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[2], variable_costs=self.costs_out[2])

            
            sewageTreatmentPlant = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageWaterTreat},
                outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat},
                conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1], self.sector_out[2] : self.conversion_factor[2]})
            
            
            return sewageTreatmentPlant
        
        
class SewageTreatment_noElecEmissions():

        def __init__(self, *args, **kwargs):
            
            v_max = 1000000
            
            tempdif=3
            share_waterSewage=0.95
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000
            
            eta=0.9
            
            Ct_sludge = 9.2
            rho_sludge = 1800
            
            
            costsIn=0.04
            costsOut=[0, 0, 0]
            deltaT=1
            emissions=0.3
            
            maxEmissions=1000000000
            
            position='Consumer_1'
            
            self.tempdif = kwargs.get("tempdif", tempdif)
            self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.eta = kwargs.get("eta", eta)
            self.v_max = kwargs.get("V_max", v_max)
            
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Sewagetreatment_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sewage\nTreatment_' + self.position)
            color='black'
            self.color = kwargs.get("color", color)
            
            self.limit=[self.v_max, self.v_max*self.concentration_sludge/self.rho_sludge, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, maxEmissions]
            
            self.conversion_factor = [self.eta, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, self.emissions]

        def component(self):
            v_sewageWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs_in)
            v_cleanWater = solph.Flow(min=0, max=1, nominal_value=self.limit[0], variable_costs=self.costs_out[0])
            v_sludgeSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[1], variable_costs=self.costs_out[1])
            q_heatSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[2], variable_costs=self.costs_out[2])
            m_emissions = solph.Flow(min=0, max=1, nominal_value=self.limit[3], variable_costs=0)

            
            sewageTreatmentPlant = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageWaterTreat},
                outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat, self.sector_emissions : m_emissions},
                conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1], self.sector_out[2] : self.conversion_factor[2], self.sector_emissions : self.conversion_factor[3]})
            
            
            return sewageTreatmentPlant
 
        
class SewageTreatment():

        def __init__(self, *args, **kwargs):
            
            v_max = 1000000
            
            tempdif=3
            share_waterSewage=0.9
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000
            
            eta=0.95
            waterrecovery = 0.95
            
            Ct_sludge = 9.2
            rho_sludge = 1800
            
            el_loss = 0.5
            
            costsIn=0.04
            costsOut=[0, 0, 0]
            costsLoss = 0
            deltaT=1
            emissions=0.3
            maxEmissions=1000000000
            position='Consumer_1'
            
            self.tempdif = kwargs.get("tempdif", tempdif)
            self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.eta = kwargs.get("eta", eta)
            self.eta_water = kwargs.get("eta_water", waterrecovery)
            self.v_max = kwargs.get("V_max", v_max)
            self.losses = kwargs.get("losses", el_loss)
            
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_loss = kwargs.get("c_loss", costsLoss)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_in = kwargs.get("sector_in")
            self.sector_loss = kwargs.get("sector_loss")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Sewagetreatment_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sewage\nTreatment_' + self.position)
            color='black'
            self.color = kwargs.get("color", color)
            
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            
            if(self.emissions==0 or self.sector_emissions==0):
                self.limit=[self.v_max, self.v_max*self.concentration_sludge/self.rho_sludge, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]
                
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]
                
            else:
                self.limit=[self.v_max, self.v_max*self.concentration_sludge/self.rho_sludge, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, self.max_emissions]
            
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, self.emissions]

        def component(self):
            v_sewageWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max, variable_costs=self.costs_in)
            q_elecWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.losses, variable_costs=self.costs_loss)
            v_cleanWater = solph.Flow(min=0, max=1, nominal_value=self.limit[0], variable_costs=self.costs_out[0])
            v_sludgeSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[1], variable_costs=self.costs_out[1])
            q_heatSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[2], variable_costs=self.costs_out[2])

            if(self.emissions==0 or self.sector_emissions==0):
            
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            else:
                m_sewageTreatEmissions = solph.Flow(min=0, max=1, nominal_value=self.limit[3])
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat, self.sector_emissions : m_sewageTreatEmissions},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            
            return sewageTreatment
        
class SewageHydrogen():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=17.6
            costsIn=0
            costsOut=14
            Qmax = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.Q_max = kwargs.get("Q_max", Qmax)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'SewageH2_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sewage\nH2_' + self.position)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.yearfactor = self.timesteps/timesteps
            color='lime'
            self.color = kwargs.get("color", color)



    def component(self):
            v_sewage2H2Water = solph.Flow(min=0, max=1, nominal_value=self.Q_max*self.deltaT/self.conversion_factor, variable_costs=self.costs_in)
            v_sewage2H2Hydrogen = solph.Flow(min=0, max=1, nominal_value=self.Q_max*self.deltaT, variable_costs=self.yearfactor*self.costs_out)
            
            sewage2H2 = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewage2H2Water},
                outputs={self.sector_out : v_sewage2H2Hydrogen},
                conversion_factors={self.sector_out : self.conversion_factor})
            
            return sewage2H2
        
 
class SludgeStorage():
    
    def __init__(self, *args, **kwargs):
        V_max = 1.5
        V_start = 0
        Q_max = 1.5
        costsIn = 0.01
        costsOut=0.01
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        disposal_periods=0
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Sludgestorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sludge\nStorage_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        self.balanced = kwargs.get("balanced", True)
        color='darkkhaki'
        self.color = kwargs.get("color", color)
        self.empty_end=kwargs.get("empty_end", False)
        self.t_start = kwargs.get("t_start", 0)
        
        
    def component(self):
        #Energy Flows
        v_waterstoreIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_waterstoreOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
        
        if(self.disposal_periods==0):
            sludgeStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
            
            return sludgeStorage
        
        else:
            sludgeStorage = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps, empty_end=self.empty_end, t_start=self.t_start)
        
            return sludgeStorage    
 
    
class WaterStorage():
    
    def __init__(self, *args, **kwargs):
        V_max = 1.5
        V_start = 0
        Q_max = 1.5
        costsIn = 0.01
        costsOut=0.01
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Waterstorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nStorage_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        color='chocolate'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        #Energy Flows
        v_waterstoreIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_waterstoreOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
            
        waterStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return waterStorage     
    
    
class Sludgecombustion():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.06, 0.06]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=50
            maxEmissions=1000000
            position='Consumer_1'
            
            #Sludge Parameters
            Ct_sludge = 9.2
            rho_sludge = 1800
            H_sludge = 10/3.6
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Sludgecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sludge\nCombustion_' + self.position)
            color='green'
            self.color = kwargs.get("color", color)
            
            #Factor if not all recovered energy can be used by the LSC
            self.usable_energy = kwargs.get("usable_energy", 1)
            

            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.H_sludge= kwargs.get("H_sludge", H_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000

        def component(self):
            v_sewageSludgeComb = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_sludge*self.rho_sludge), variable_costs=self.costs_in*self.rho_sludge)
            q_SludgeCombEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_SludgeCombTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])

            if(self.emissions==0 or self.sector_emissions==0):
            
                sludgeCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageSludgeComb},
                    outputs={self.sector_out[0] : q_SludgeCombEl, self.sector_out[1] : q_SludgeCombTh},
                    conversion_factors={self.sector_out[0] : self.H_sludge*self.rho_sludge*self.conversion_factor[0]*self.usable_energy, self.sector_out[1] : self.H_sludge*self.rho_sludge*self.conversion_factor[1]*self.usable_energy})
            else:
                m_sludgecombustionemissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                sludgeCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageSludgeComb},
                    outputs={self.sector_out[0] : q_SludgeCombEl, self.sector_out[1] : q_SludgeCombTh, self.sector_emissions : m_sludgecombustionemissions},
                    conversion_factors={self.sector_out[0] : self.H_sludge*self.rho_sludge*self.conversion_factor[0]*self.usable_energy, self.sector_out[1] : self.H_sludge*self.rho_sludge*self.conversion_factor[1]*self.usable_energy, self.sector_emissions : self.emissions*self.usable_energy})
            
            
            
            return sludgeCombustion
        
class MFC():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.073
            costsIn=0.043
            costsOut=0
            V_max = 20
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            rho_sludge = 1800
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.V_max = kwargs.get("V_max", V_max)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'MFC_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'MFC_' + self.position)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            color='darkcyan'
            self.color = kwargs.get("color", color)




    def component(self):
            v_sewageSludgeMFC = solph.Flow(min=0, max=1, nominal_value=self.V_max, variable_costs=self.costs_in*self.rho_sludge)
            q_sewageSludgeMFCel = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.conversion_factor/self.deltaT)

            sludgeMFC = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageSludgeMFC},
                outputs={self.sector_out : q_sewageSludgeMFCel},
                conversion_factors={self.sector_out : self.conversion_factor})
            
            return sludgeMFC


class SludgeAD():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.5
            costsIn=0.068
            costsOut=0
            P_in = 100
            P_out=100
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            rho_sludge = 1800
            
            #Heating Value Gas in kWh/kg Assumption
            H_gas = 11.42
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.P_in = kwargs.get("P_in", P_in)
            self.P_out = kwargs.get("P_out", P_out)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'SludgeAD_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sludge\nAD_' + self.position)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.H_gas = kwargs.get("H_gas", H_gas)
            color='brown'
            self.color = kwargs.get("color", color)
             



    def component(self):
            v_sewageSludgeAD = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_gas*self.rho_sludge*self.conversion_factor), variable_costs=self.rho_sludge*self.costs_in)
            q_sewageSludgeADGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)



            
            sludgeAD = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageSludgeAD},
                outputs={self.sector_out : q_sewageSludgeADGas},
                conversion_factors={self.sector_out : self.H_gas*self.rho_sludge*self.conversion_factor})
            
            return sludgeAD        
        
        
class SludgeADH2():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.0148
            costsIn=0.068
            costsOut=0
            MLimit=145
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            rho_sludge = 1800
            
            
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.M_max = kwargs.get("M_max", MLimit)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'SludgeADH2_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sludge\nADH2_' + self.position)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            color='sandybrown'
            self.color = kwargs.get("color", color)
             



    def component(self):
            v_sewageSludgeAD = solph.Flow(min=0, max=1, nominal_value=self.M_max/self.rho_sludge, variable_costs=self.rho_sludge*self.costs_in)
            q_sewageSludgeADH2 = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.conversion_factor, variable_costs=self.costs_out)



            
            sludgeADH2 = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_sewageSludgeAD},
                outputs={self.sector_out : q_sewageSludgeADH2},
                conversion_factors={self.sector_out : self.rho_sludge*self.conversion_factor})
            
            return sludgeADH2      
        
        
class Sewagedisposal():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000
        share_waterSewage=0.95
        emissions=0.3
        maxEmissions=1000000
        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sewagedisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sewage\nDisposal_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='brown'
        self.color = kwargs.get("color", color)
        
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.max_emissions=kwargs.get("max_emissions", maxEmissions)
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        
        
    def component(self):


                
        v_sewageWasted = solph.Flow(nominal_value=self.V_max, variable_costs=self.costs_in)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            sewageWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_sewageWasted})
        
        else:
            m_sewagewasteEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
            
            sewageWasted = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWasted},
                    outputs={self.sector_emissions : m_sewagewasteEmissions},
                    conversion_factors={self.sector_emissions : self.emissions})
        
        return sewageWasted
    
class Waterdisposal():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000

        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Waterdisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDisposal_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='brown'
        self.color = kwargs.get("color", color)
        
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        
        
    def component(self):


                
        v_waterWasted = solph.Flow(nominal_value=self.V_max**self.deltaT, variable_costs=self.costs_in)
        
            
        waterWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_waterWasted})
        
        
        return waterWasted
    
    
class Sludgedisposal():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000
        share_waterSewage=0.95
        Ct_sludge = 9.2
        rho_sludge = 1800
        emissions=1457
        maxEmissions=1000000
        
        minDisposal = 0
        
        self.v_scenMax=3682
        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
        self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sludgedisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sludge\nDisposal_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='peru'
        self.color = kwargs.get("color", color)
        
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.max_emissions=kwargs.get("max_emissions", maxEmissions)
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        
        self.minDisposalPercent = kwargs.get("min_disposal", minDisposal)
        
        
        
    def component(self):

          
        v_sludgeWasted = solph.Flow(nominal_value=self.V_max*self.concentration_sludge/self.rho_sludge, summed_min=self.minDisposalPercent*self.v_scenMax/(self.V_max*self.concentration_sludge/self.rho_sludge), variable_costs=self.costs_in)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            sludgeWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_sludgeWasted})
            
        else:
            m_sludgewasteEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
            
            sludgeWasted = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sludgeWasted},
                    outputs={self.sector_emissions : m_sludgewasteEmissions},
                    conversion_factors={self.sector_emissions : self.emissions})
        

        
        return sludgeWasted
    

#Water Demand Component if Greywater Output is considered
# --> Demand output as a variable
#Sector in usually water sector
#Sector Out Sewage
#Sector Greywater Greywater
#Sector waterdemand is waterdemandsector
#Penalty Costs if the use of potable water is financially punished at certain timesteps
class WaterdemandReuse():
    def __init__(self, *args, **kwargs):
        share_waterSewage=0.95
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        eta_greywater = 1
        share_greywater = 0
        penaltyCosts = 0
 

        self.sector_in= kwargs.get("sector_in")
        self.sector_out1= kwargs.get("sector_out")
        self.sector_greywater= kwargs.get("sector_greywater")
        self.sector_waterdemand= kwargs.get("sector_waterdemand")
        self.sector_out=[self.sector_out1, self.sector_greywater, self.sector_waterdemand]
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Potablewaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDemand_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        self.share_greywater = kwargs.get("share_greywater", share_greywater)
        self.eta_greywater = kwargs.get("eta_greywater", eta_greywater)
        self.penalty_costs = kwargs.get("penalty_costs", penaltyCosts)
        self.costs_out=[ 0, 0, self.penalty_costs]
        color='yellow'
        self.color = kwargs.get("color", color)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        sewageOutMax = np.multiply(self.demand, self.share_sewage)
        greywaterOutMax = np.multiply(self.demand, self.share_sewage*self.share_greywater)
        
        v_waterdemandIn = solph.Flow(min=0, max=self.demand/self.demandMax, nominal_value=self.demandMax)
        v_sewageOut = solph.Flow(min=0, max=sewageOutMax/max(sewageOutMax), nominal_value=max(sewageOutMax), variableCosts=self.costs_out[0])
        v_greywater = solph.Flow(min=0, max=greywaterOutMax/max(greywaterOutMax), nominal_value=max(greywaterOutMax), variableCosts=self.costs_out[1])
        v_waterdemandOut = solph.Flow(min=0, ax=self.demand/self.demandMax, nominal_value=self.demandMax, variableCosts=self.costs_out[2])
        
        #Conversion to sewage sector
        #Aggregation process to more consumers
        water2sewage = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterdemandIn},
            outputs={self.sector_out1 : v_sewageOut, self.sector_greywater : v_greywater, self.sector_waterdemand : v_waterdemandOut},
            conversion_factors={self.sector_out1 : self.share_sewage*(1-self.share_greywater),
                                self.sector_greywater : self.share_sewage*self.share_greywater*self.eta_greywater,
                                self.sector_waterdemand : 1})
        
        return water2sewage  
        
    
#Sink for contraint to cover the given water demand
#Sector is usually the waterdemandsector
class WaterDemandPotableGreySink():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760


        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Totalwaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDemand_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='yellow'
        self.color = kwargs.get("color", color)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        
        v_waterIn = solph.Flow(fix=self.demand, nominal_value=1)

        waterDemand = solph.Sink(label=self.label, inputs={self.sector_in: v_waterIn})
            
        return waterDemand

#Demand for the Greywater that can be used instead of potable water
#Input is the greywatersector
#Output is the waterdemandsector
#Used if the used greywater is variable
class Greywaterdemand():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        demand_share = 0.5
        minimumGreywater=0


        self.sector_in= kwargs.get("sector_in")
        self.sector_out=kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Greywaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Greywater\nDemand_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='lightgreen'
        self.color = kwargs.get("color", color)
        self.demand_share=kwargs.get("demand_share", demand_share)
        self.min_greywater=kwargs.get("min_greywater", minimumGreywater)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        maxFlowsWater = np.multiply(self.demand, self.demand_share)
        minimumOfGreywater = np.multiply(self.demand, self.min_greywater)
        
        v_waterIn = solph.Flow(min=0, max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))
        v_waterOut = solph.Flow(min=minimumOfGreywater/max(maxFlowsWater), max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))

        waterDemand = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterIn},
            outputs={self.sector_out : v_waterOut},
            conversion_factors={self.sector_out :1})
            
        return waterDemand
    
#Additional Greywater Demand that is further processsed as sewage    
class Greywaterdemand2Sewage():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        demand_share = 0.5
        minimumGreywater=0
        sewageShar = 0.5


        self.sector_in= kwargs.get("sector_in")
        self.sector_out=kwargs.get("sector_out")
        self.sector_sewage=kwargs.get("sector_sewage")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Greywaterdemand_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Greywater\nDemand_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='lightgreen'
        self.color = kwargs.get("color", color)
        self.demand_share=kwargs.get("demand_share", demand_share)
        self.min_greywater=kwargs.get("min_greywater", minimumGreywater)
        self.sewage_share = kwargs.get("sewage_share", sewageShar)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        maxFlowsWater = np.multiply(self.demand, self.demand_share)
        minimumOfGreywater = np.multiply(self.demand, self.min_greywater)
        
        v_waterIn = solph.Flow(min=0, max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))
        v_waterOut = solph.Flow(min=minimumOfGreywater/max(maxFlowsWater), max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))
        v_sewageOut = solph.Flow(min=minimumOfGreywater/max(maxFlowsWater), max=maxFlowsWater/max(maxFlowsWater), nominal_value=max(maxFlowsWater))

        waterDemand = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterIn},
            outputs={self.sector_out : v_waterOut, self.sector_sewage : v_sewageOut},
            conversion_factors={self.sector_out :1, self.sector_sewage : self.sewage_share})
            
        return waterDemand


#Input is the greywatersector
#Output the sewage sector
class Greywatersewage():
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760


        self.sector_in= kwargs.get("sector_in")
        self.sector_out=kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'GreywaterSewage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Greywater\nSewage_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='crimson'
        self.color = kwargs.get("color", color)


        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
        
        v_waterIn = solph.Flow(min=0, max=1, nominal_value=self.demandMax)
        v_waterOut = solph.Flow(min=0, max=1, nominal_value=self.demandMax)

        waterDemand = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterIn},
            outputs={self.sector_out : v_waterOut},
            conversion_factors={self.sector_out :1})
            
        return waterDemand
    

#Storage for Greywater with fixed disposal periods
#Sector is the greywater sector    
class GreywaterStorage():
    
    def __init__(self, *args, **kwargs):
        V_max = 1.5
        V_start = 0
        Q_max = 1.5
        costsIn = 0.01
        costsOut=0.01
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        disposal_periods=24
        timesteps=8760
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Greywaterstorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Greywater\nStorage_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        self.timesteps=kwargs.get("timesteps", timesteps)
        color='red'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        #Energy Flows
        v_waterstoreIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_waterstoreOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
            
        if(self.disposal_periods==0):
        
            waterStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
            return waterStorage     
        else:
        
            waterStorage = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: v_waterstoreIn},
                outputs={self.sector_out: v_waterstoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps)
        
            return waterStorage
        

#Seawater Desalination
class Seawatersource():
    
    def __init__(self, *args, **kwargs):
        V_seawater = 1000
        deltaT=1
        position='Consumer_1'
        costs=0
        
        self.V= kwargs.get("V", V_seawater)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Seawatersource' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Seawater\nSource_' + self.position)
        self.costs_out=kwargs.get("costs", costs)
        color = 'aqua'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        v_seawater = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_out)
            
        seawater = solph.Source(label=self.label, outputs={self.sector_out: v_seawater})
        
        return seawater 
    
class Desalination():
    
    def __init__(self, *args, **kwargs):
        V_desalination = 1
        K_desalination=3
        C_desalination_in = 0
        C_desalination_out = 0.44
        eta_desalination = 1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.V = kwargs.get("V", V_desalination)
        self.eta = kwargs.get("efficiency", eta_desalination)
        self.energy = kwargs.get("energy", K_desalination)
        self.costs_in = kwargs.get("c_in", C_desalination_in)
        self.costs_out = kwargs.get("c_out", C_desalination_out)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.sector_loss = kwargs.get("sector_loss")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Desalination_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Desalination_' + self.position)
        self.P = self.V*self.energy/self.deltaT
        color='olivedrab'
        self.color = kwargs.get("color", color)
        
    def component(self):
        v_seawater = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs = self.costs_in)
        v_potablewater = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_out)
            
  
        q_energy = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT)
            
            
            
        desalination = tlmo.TransformerLossesMultipleOut(
            label=self.label,
            inputs={self.sector_in : v_seawater, self.sector_loss : q_energy},
            outputs={self.sector_out : v_potablewater},
            conversion_sector = self.sector_in, losses_sector=self.sector_loss,
            conversion_factor=[self.eta], losses_factor=self.energy)

        
        return desalination
    
#Rainfall Input
class Rainwatercollect():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsOut=0
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            AreaRoof = 100
            

            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_out = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.Area_Roof = kwargs.get("area_roof", AreaRoof)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Rainwatercollection_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Rainwater_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='gold'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("generation_timeseries", generationArray)
            self.conversion_timeseries = np.multiply(self.conversion_timeseries, self.Area_Roof/1000)

        def component(self):
            
            v_rainwater = solph.Flow(fix = self.conversion_timeseries, nominal_value=1)
            
            rainwater = solph.Source(label=self.label, outputs={self.sector_out: v_rainwater})
            
            return rainwater
        
class Rainwatertank():
    
    def __init__(self, *args, **kwargs):
        V_max = 4.1
        V_start = 0
        Q_max = 10
        costsIn = 0
        costsOut=0
        Eta_in = 0.9
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Rainwatertank' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Rainwater\Tank_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        color='chocolate'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        #Energy Flows
        v_rainwaterIn = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_in)
        v_rainwaterOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
            
        rainwaterTank = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_rainwaterIn},
                outputs={self.sector_out: v_rainwaterOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return rainwaterTank  
    

class Rainwaterdisposal():
    
    def __init__(self, *args, **kwargs):

        
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        V_max = 1000000
        emissions=0

        
        
        self.V_max = kwargs.get("V_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Rainwaterdisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Rainwater\nDisposal_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='black'
        self.color = kwargs.get("color", color)
        self.emissions=kwargs.get("emissions", emissions)

        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        
        
    def component(self):

          
        v_rainwaterWaste = solph.Flow(nominal_value=self.V_max, variable_costs=self.costs_in)
        
        
            
        rainwaterWasted = solph.Sink(label=self.label, inputs={self.sector_in: v_rainwaterWaste})
            

        

        
        return rainwaterWasted
    
    
class Rainwaterpump():
    
    def __init__(self, *args, **kwargs):
        P = 1.3
        Q = 5
        eta = 0.5
        h = 4
        deltaT=1
        costsIn=0.02
        costsOut=0
        position='Consumer_1'
        rho_H2O = 1000
        g = 9.81
        
        self.P= kwargs.get("P_max", P)
        self.Q= kwargs.get("Q_max", Q)
        self.h= kwargs.get("h_max", h)
        self.eta= kwargs.get("eta", eta)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.sector_loss= kwargs.get("sector_loss")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Rainwaterpump_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Rainwaterpump_' + self.position)
        self.costs_in=kwargs.get("c_in", costsIn)
        self.costs_out=kwargs.get("c_out", costsOut)
        self.conversionFactor = rho_H2O*g*self.h/self.eta
        color='lemonchiffon'
        self.color = kwargs.get("color", color)
        
    def component(self):
        q_rainwaterpumpEl = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=0)
        v_rainwaterpumpRainwater = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_in)
        v_rainwaterpumpGreywater = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_out)
            
        
        rainwaterpump = tlmo.TransformerLossesMultipleOut(
            label=self.label,
            inputs={self.sector_in : v_rainwaterpumpRainwater, self.sector_loss : q_rainwaterpumpEl},
            outputs={self.sector_out : v_rainwaterpumpGreywater},
            conversion_sector = self.sector_in, losses_sector=self.sector_loss,
            conversion_factor=[1], losses_factor=self.conversionFactor/3600000)
        
        return rainwaterpump        


#------------------------------------------------------------------
#Additional LSC components
#------------------------------------------------------------------

#Predefined water demand with fixed values
#Timeseries from the input file required
# sector is Waterdemandsector
class WaterdemandPredefine():
    def __init__(self, *args, **kwargs):

        deltaT=1
        position='Consumer_1'
        timesteps=8760


        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'WaterdemandPredefine_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDemand\nPredefine_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='yellow'
        self.color = kwargs.get("color", color)

        
        demandWater = np.zeros(self.timesteps)
            
        self.demand = kwargs.get("demand", demandWater)
        
        self.demandMax = max(self.demand)
        
        
    def component(self):
        
 
        v_waterIn = solph.Flow(fix=self.demand, nominal_value=1)

        waterDemand = solph.Sink(label=self.label, inputs={self.sector_in: v_waterIn})
            
        return waterDemand
    
#Potable water that must be used to cover the waterdemand and that is tranformed to sewage
#Sector_in: Potable water sector
#sector_out: waterdemand sector
#sector_sewage: sewage_sector
#Important for LSC: Output should be the LSC sewage sector for all consumers to collect the sewage 
class WaterDemandSewageFlex():
    def __init__(self, *args, **kwargs):
        share_waterSewage=0.95
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        waterlimit = 100000


        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.sector_sewage=kwargs.get("sector_sewage")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'WaterdemandSewage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nDemand\nSewage_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
        color='green'
        self.color = kwargs.get("color", color)
        self.waterlimit= kwargs.get("waterlimit", waterlimit)

        

        
        
    def component(self):
        

        
        v_waterIn = solph.Flow(min=0, max=1, nominal_value=self.waterlimit)
        v_potableOut = solph.Flow(min=0, max=1, nominal_value=self.waterlimit)
        v_sewageOut = solph.Flow(min=0, max=1, nominal_value=self.waterlimit)
        
        #Conversion to sewage sector
        #Aggregation process to more consumers
        water2sewage = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_waterIn},
            outputs={self.sector_out : v_potableOut, self.sector_sewage : v_sewageOut},
            conversion_factors={self.sector_out : 1, self.sector_sewage : self.share_sewage})
        
        return water2sewage  
    
    
class PipelinePurchaseLimited():
    
    def __init__(self, *args, **kwargs):
        V_connection = 100000
        C_fix = 28.33
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        limit=1
        discount = 1

        
        self.V= kwargs.get("V", V_connection)
        self.costs_fixed = kwargs.get("costs_fixed", C_fix)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Waterpipelinepurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nPipeline\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsPipelineArray = np.zeros(self.timesteps)
            
        self.costs_pipeline = kwargs.get("costs_water", costsPipelineArray)
        self.discount = kwargs.get("discount", discount)
        self.costs_pipeline = np.multiply(self.costs_pipeline, self.discount)
        
        demandWater = np.zeros(self.timesteps)
        self.demand = kwargs.get("demand", demandWater)
        
        self.limit = kwargs.get("limit", limit)
        
        self.summedLimit = np.multiply(np.sum(self.demand), self.limit)
        
        
        
    def component(self):
        v_waterpipeline = solph.Flow(min=0, max=1, nominal_value=self.V*self.deltaT, variable_costs=self.costs_pipeline, summed_max=self.summedLimit/(self.V*self.deltaT))
            
        waterpipeline = solph.Source(label=self.label, outputs={self.sector_out: v_waterpipeline})
        
        return waterpipeline  
    
class WaterSale():
    
    def __init__(self, *args, **kwargs):
        V_max=1000000
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.V_max= kwargs.get("V_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Watersale_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nSale\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        self.maximum=kwargs.get("maximum", 1)
        
        revenuesWaterArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("revenues", revenuesWaterArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        v_watersale = solph.Flow(min=0, max=self.maximum, nominal_value=self.V_max, variable_costs=self.revenues)
            
        watersale = solph.Sink(label=self.label, inputs={self.sector_in: v_watersale})
        
        return watersale
    
class WaterSaleGreywater_old():
    
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.sewagefactor= kwargs.get("sewagefactor", 0.95)
        self.waterdemand= kwargs.get("waterdemand", 1)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Watersale_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nSale\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        self.share_greywater=kwargs.get("share_greywater", 0.5)
        
        revenuesWaterArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("water_price", revenuesWaterArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        v_watersale = solph.Flow(min=0, max=self.waterdemand, nominal_value=self.sewagefactor*self.share_greywater, variable_costs=self.revenues)
            
        watersale = solph.Sink(label=self.label, inputs={self.sector_in: v_watersale})
        
        return watersale
    
class WaterSaleGreywater():
    
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.sewagefactor= kwargs.get("sewagefactor", 0.95)
        self.waterdemand= kwargs.get("waterdemand", 1)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Watersale_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Water\nSale\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        self.share_greywater=kwargs.get("share_greywater", 0.5)
        
        revenuesWaterArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("water_price", revenuesWaterArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        v_watersale = solph.Flow(min=0, max=self.waterdemand, nominal_value=1, variable_costs=self.revenues)
            
        watersale = solph.Sink(label=self.label, inputs={self.sector_in: v_watersale})
        
        return watersale
    
class Sewage2Greywater():
    
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.sewagefactor= kwargs.get("sewagefactor", 0.95)
        self.waterdemand= kwargs.get("waterdemand", 1)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sewage2Greywater_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sewage2\Greywater\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='black'
        self.color = kwargs.get("color", color)
        self.share_greywater=kwargs.get("share_greywater", 0.5)
                  
        
        self.min_greywater=kwargs.get("min_greywater", 0)
        
        self.greywater_minvalue = np.multiply(self.waterdemand, self.min_greywater)
        
        
    def component(self):
        v_sewage2greywater = solph.Flow(min=self.greywater_minvalue, max=self.waterdemand, nominal_value=self.sewagefactor*self.share_greywater, variable_costs=0)
        v_greywater = solph.Flow(min=0, max=1, nominal_value=1000000, variable_costs=0)
            
        sewage2greywater = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_sewage2greywater},
            outputs={self.sector_out : v_greywater},
            conversion_factors={self.sector_out : 1})
        
        return sewage2greywater
    
class Sewage2GreywaterOperational():
    
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.sewagefactor= kwargs.get("sewagefactor", 0.95)
        self.waterdemand= kwargs.get("waterdemand", 1)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sewage2Greywater_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sewage2\Greywater\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='black'
        self.color = kwargs.get("color", color)
        self.share_greywater=kwargs.get("share_greywater", 0.5)
        self.v_max= kwargs.get("v_max_greywater", 1000000)
                  
        self.min_greywater=kwargs.get("min_greywater", 0)
        
        self.greywater_minvalue = np.multiply(self.waterdemand, self.min_greywater)
        
        
    def component(self):
        v_sewage2greywater = solph.Flow(min=self.greywater_minvalue, max=1, nominal_value=self.v_max, variable_costs=0)
        v_greywater = solph.Flow(min=0, max=1, nominal_value=1000000, variable_costs=0)
            
        sewage2greywater = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_sewage2greywater},
            outputs={self.sector_out : v_greywater},
            conversion_factors={self.sector_out : 1})
        
        return sewage2greywater
    
    
class Greywater2water():
    
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.max_greywater2water= kwargs.get("max_greywater2water", 0.5)
        self.waterdemand= kwargs.get("waterdemand", 1)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Greywater2water_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Greywater2\water\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='black'
        self.color = kwargs.get("color", color)

                  

        
        
    def component(self):
        v_greywater = solph.Flow(min=0, max=self.waterdemand, nominal_value=self.max_greywater2water, variable_costs=0)
        v_water = solph.Flow(min=0, max=1, nominal_value=1000000, variable_costs=0)
            
        greywater2water = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_greywater},
            outputs={self.sector_out : v_water},
            conversion_factors={self.sector_out : 1})
        
        return greywater2water


   