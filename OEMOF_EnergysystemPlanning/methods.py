# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 17:38:39 2021

@author: Matthias Maldet
"""

import oemof.solph as solph
import json
import pandas as pd
from pathlib import Path

def values(array):
    liste = []
    for i in array:
        liste.append(i[0])
        
    return liste

def store_results(energysystem, model, scenario):
    energysystem.results = solph.processing.results(model)
    file2write =  scenario 
    energysystem.dump(file2write, 'my_dump_general.oemof')
    print("----------------Data saved----------------")
    
def store_results_extended(energysystem, model, scenario, filename):
    energysystem.results = solph.processing.results(model)
    file2write =  scenario 
    energysystem.dump(file2write, filename)
    print("----------------Data saved----------------")
    
def restore_results(scenario):
    my_energysystem = solph.EnergySystem()
    file2load =  scenario
    my_energysystem.restore(file2load, 'my_dump_general.oemof')
    results = my_energysystem.results
    print("----------------Data restored----------------")
    return results

def restore_results_extended(scenario, filename):
    my_energysystem = solph.EnergySystem()
    file2load =  scenario
    my_energysystem.restore(file2load, filename)
    results = my_energysystem.results
    print("----------------Data restored----------------")
    return results

def get_soc_end(component, results, time):
    df = solph.views.node(results, component.label).get("sequences")
    
    for column in list(df.columns):
        if('storage_content' in str(column).lower()):
            return(df[column].values[time-1])
        
def list_meanvalue(liste, steps):
    newlist = []
    for i in range(0, len(liste), steps):
        value=0
        value+=liste[i]
        value+=liste[i+1]
        value+=liste[i+2]
        value+=liste[i+3]
        value/=steps
        newlist.append(value)
        
    return newlist

def list_sumvalue(liste, steps):
    newlist = []
    for i in range(0, len(liste), steps):
        value=0
        value+=liste[i]
        value+=liste[i+1]
        value+=liste[i+2]
        value+=liste[i+3]
        newlist.append(value)
        
    return newlist

def write_dict(*args, **kwargs):
    datalist = kwargs.get("datalist", 0)
    consumerlist = kwargs.get("consumerlist", 0)
    filename = kwargs.get("filename", 0)
    
    dicti={}
    index=0
    for data in datalist:
        dicti[consumerlist[index]] = data
        index+=1
    
    with open(filename, 'w') as file:
        file.write(json.dumps(dicti,  indent=1)) # use `json.loads` to do the reverse 
        
def write_investmentcosts(*args, **kwargs):
    datalist = kwargs.get("datalist", 0)
    consumerlist = kwargs.get("consumerlist", 0)
    filename = kwargs.get("filename", 0)
    
    dicti={}
    index=0
    
    totalInvest=0
    
    for data in datalist:
        
        data_new={}
        keys=data.keys()
        
        totalsum=0
        
        for key in keys:
            if('P_pv' in key):
                addval = data[key]*87.38041988
                if(data[key]>0): 
                    addval+=235.2549766

                data_new[key] = addval
                totalsum+=addval
            elif('P_battery' in key):
                addval = data[key]*117.2305066

                if(data[key]>0): 
                    addval+=105.5074559

                data_new[key] = addval
                totalsum+=addval
            elif('P_heatpump' in key):
                addval = data[key]*134.6212874

                if(data[key]>0): 
                    addval+=71.22819441

                data_new[key] = addval
                totalsum+=addval
            elif('P_districtheat' in key):
                addval = data[key]*33.6078538

                if(data[key]>0): 
                    addval+=23.52549766

                data_new[key] = addval
                totalsum+=addval
            elif('P_thermstore' in key):
                addval = data[key]*2.75584401
                if(data[key]>0): 
                    addval+=30.2470684
                data_new[key] = addval
                totalsum+=addval
            elif('P_wastecomb' in key):
                addval = data[key]*609.5986343/3.4
                if(data[key]>0): 
                    addval+=17584.57599

                data_new[key] = addval
                totalsum+=addval
            elif('SOC_wastestore' in key):
                addval = data[key]*1.172305066

                if(data[key]>0): 
                    addval+=0

                data_new[key] = addval
                totalsum+=addval
            elif('V_sewagetreat' in key):
                addval = data[key]*4705.099532

                if(data[key]>0): 
                    addval+=23727.14478

                data_new[key] = addval
                totalsum+=addval
            elif('Gas_CHP' in key):
                addval = data[key]*156.8333731

                if(data[key]>0): 
                    addval+=0
                data_new[key] = addval
                totalsum+=addval
            elif('AD' in key):
                addval = data[key]*468.9220264

                if(data[key]>0): 
                    addval+=17584.57599

                data_new[key] = addval
                totalsum+=addval
            else:
                data_new[key]=0
         
        data_new["TotalInvestmentCosts"] = totalsum
            
        dicti[consumerlist[index]] = data_new
        index+=1
        
        totalInvest+=totalsum
        
    
    dicti["Total"] = totalInvest        
    
    with open(filename, 'w') as file:
        file.write(json.dumps(dicti,  indent=1)) # use `json.loads` to do the reverse 
        
def write_objective(*args, **kwargs):
    objective = kwargs.get("objective", 0)
    filename = kwargs.get("filename", 0)
    scenariomessage = kwargs.get("scenariomessage", 0)
    scenario = kwargs.get("scenario", 0)
    
    with open(filename, "w") as f:
        f.write(scenario)
        f.write("\n\n")
        f.write(scenariomessage)
        f.write("\n\n")
        f.write("Objective:\n %s \n" % (objective));


def results_to_csv(*args, **kwargs):
    consumers = kwargs.get("consumers", 0)
    filename = kwargs.get("filename", 0)
    results = kwargs.get("results", 0)
    
    for consumer in consumers:
        
        run=0
        
        for component in consumer.components:
            if(run==0):
                df=solph.views.node(results, component.label).get("sequences")
                run+=1
            else:
                dfnew=solph.views.node(results, component.label).get("sequences")
                dfs=[df, dfnew]
                df=pd.concat(dfs, axis=1)
                
        filename2write = filename + "/" + consumer.label + ".csv"
        filepath = Path(filename2write)        
        df.to_csv(filepath)
        #df.to_excel("test.xlsx")

def results_to_excel(*args, **kwargs):
    consumers = kwargs.get("consumers", 0)
    filename = kwargs.get("filename", 0)
    results = kwargs.get("results", 0)
    
    mode="w"
    for consumer in consumers:
        
        run=0
        
        for component in consumer.components:
            if(run==0):
                df=solph.views.node(results, component.label).get("sequences")
                run+=1
                
            else:
                dfnew=solph.views.node(results, component.label).get("sequences")
                dfs=[df, dfnew]
                df=pd.concat(dfs, axis=1)
                
                
        filename2write = filename + "/results.xlsx"
        filepath = Path(filename2write)        
        writer = pd.ExcelWriter(filepath, mode=mode, engine="openpyxl")
        df.to_excel(writer, sheet_name=consumer.label)
        #writer.save()
        writer.close()
        mode="a"
    