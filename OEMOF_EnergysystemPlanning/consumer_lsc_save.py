# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 14:34:03 2022

@author: Matthias Maldet
"""

import numpy as np
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import household_components as household_comp
import math

def consumer_setup(*args, **kwargs):
    
    
    #List of Components
    components=[]
    
    #Consumer
    consumer1 = kwargs.get("consumer", 0)
    LSC = kwargs.get("LSC", 0)
    timesteps=kwargs.get("timesteps", 8784)
    
    if(consumer1==0 or LSC==0):
        return 0
    
    #_____________ Electricity _____________
    
    #Parameters Electricity
    c_grid_elec=kwargs.get("c_grid_elec", 0.062)
    c_abgabe_elec = kwargs.get("c_abgabe_elec", 0.018)
    red_local_elec = kwargs.get("red_local_elec", 1-0.57)
    red_regional_elec = kwargs.get("red_regional_elec", 1-0.28)
    
    #Components
    #Electricity
    electricitydemand = kwargs.get("electricitydemand", False)
    deactivate_electricitydemand = kwargs.get("deactivate_electricitydemand", False)
    
    #gridpurchaseNoCombinedMetering = kwargs.get("gridpurchaseNoCombinedMetering", False)
    deactivate_electricitygridpurchase = kwargs.get("deactivate_electricitygridpurchase", False)
    electricitygridpurchase = kwargs.get("electricitygridpurchase", False)
    
    #Combined purchase --> Standard true so that it is not implemented normally
    deactivate_electricitygridpurchase_combined = kwargs.get("deactivate_electricitygridpurchase_combined", True)
    electricitygridpurchase_combined = kwargs.get("electricitygridpurchase_combined", False)
    
    deactivate_pv = kwargs.get("deactivate_pv", False)
    pv = kwargs.get("pv", False)
    
    deactivate_electricitygridFeedin = kwargs.get("deactivate_electricitygridFeedin", False)
    electricitygridFeedin = kwargs.get("electricitygridFeedin", False)
    
    deactivate_battery = kwargs.get("deactivate_battery", False)
    battery = kwargs.get("battery", False)
    
    sell_tariff_lsc_elec = np.multiply(np.add(consumer1.input_data['electricityCosts'], consumer1.input_data['electricityFeedin']), 1/2)
    sell_tariff_lsc_elec = kwargs.get("sell_tariff_lsc_elec", sell_tariff_lsc_elec)
    deactivate_elec2lsc = kwargs.get("deactivate_elec2lsc", False)
    elec2lsc = kwargs.get("elec2lsc", False)
    
    c_grid_reduced_elec = np.multiply(c_grid_elec, red_local_elec)
    buy_tariff_lsc_elec = np.add(sell_tariff_lsc_elec, np.add(c_grid_reduced_elec, c_abgabe_elec))
    buy_tariff_lsc_elec = kwargs.get("buy_tariff_lsc_elec", buy_tariff_lsc_elec)
    deactivate_lsc2elec = kwargs.get("deactivate_lsc2elec", False)
    lsc2elec = kwargs.get("lsc2elec", False)
    
    deactivate_heatpump = kwargs.get("deactivate_heatpump", False)
    heatpump = kwargs.get("heatpump", False)
    
    deactivate_elcool = kwargs.get("deactivate_elcool", False)
    elcool = kwargs.get("elcool", False)
    
    #Component Logic
    
    #Demand
    if(deactivate_electricitydemand is False):
        if(electricitydemand is False):
            electricitydemand = el_comp.Demand(sector=consumer1.getElectricitySector(), 
                            demand_timeseries=consumer1.input_data['electricityDemand'], label='Electricitydemand_' + consumer1.label)            
        components.append(electricitydemand)
        
  
    #gridpurchase
    if(deactivate_electricitygridpurchase is False):
        if(electricitygridpurchase is False):
            electricitygridpurchase = el_comp.GridPurchase(sector=consumer1.getElectricitySector(), 
                              costs_energy=np.add(consumer1.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer1.label, timesteps=timesteps, costs_power=0)             
        components.append(electricitygridpurchase)
        
    #gridpurchase combined
    if(deactivate_electricitygridpurchase_combined is False and deactivate_electricitygridpurchase is True):
        if(electricitygridpurchase_combined is False):
            electricitygridpurchase_combined = el_comp.GridCombinedPowerMetering(sector_grid=consumer1.getElectricityGridSector(), sector_electricity=consumer1.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              costs_energy=np.add(consumer1.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + consumer1.label, timesteps=timesteps)        
            
        components.append(electricitygridpurchase_combined.get_source())
        components.append(electricitygridpurchase_combined.get_toElec())

    
    #pv
    if(deactivate_pv is False):
        if(pv is False):
            pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_' + consumer1.label, timesteps=timesteps, area_efficiency=0.5, area_roof=100, area_factor=10)             
        components.append(pv)
        
    
    #grid feedin
    if(deactivate_electricitygridFeedin is False):
        if(electricitygridFeedin is False):
            electricitygridFeedin = el_comp.GridFeedin(sector=consumer1.getElectricitySector(), revenues=consumer1.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + consumer1.label, timesteps=timesteps)             
        components.append(electricitygridFeedin)
        
    #battery
    if(deactivate_battery is False):
        if(battery is False):
            battery = el_comp.Battery(sector_in=consumer1.getElectricitySector(), label='Battery_' + consumer1.label,
                          soc_max=5, SOC_start=5, P_in=5, P_out=5)            
        components.append(battery)
        
    
    #Electricity Consumer to LSC
    if(deactivate_elec2lsc is False):
        if(elec2lsc is False):
            elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer1.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer1.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec)            
        components.append(elec2lsc)
        
        
    #Electricity LSC to Consumer
    if(deactivate_lsc2elec is False):
        if(lsc2elec is False):
            lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer1.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer1.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)            
        components.append(lsc2elec)
        
    #Heatpump
    if(deactivate_heatpump is False):
        if(heatpump is False):
            heatpump = el_comp.Heatpump(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector(), 
                            conversion_timeseries=consumer1.input_data['copHP'], timesteps=timesteps, P_in=7, P_out=7, label='Heatpump_' + consumer1.label)            
        components.append(heatpump)
        
    #Electric Cooling
    if(deactivate_elcool is False):
        if(elcool is False):
            elcool = el_comp.ElectricCooling(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=consumer1.input_data['copCool'], P_in=7, P_out=7, label="Cooler_" + consumer1.label)
  
        components.append(elcool)    
        
        
    
    #_____________ Heat _____________
    
    #Heat parameters
    c_grid_heat=kwargs.get("c_grid_heat", 0.046)
    
    #Heat compoonents
    heatdemand = kwargs.get("heatdemand", False)
    deactivate_heatdemand = kwargs.get("deactivate_heatdemand", False)
    
    hotwaterdemand = kwargs.get("hotwaterdemand", False)
    deactivate_hotwaterdemand = kwargs.get("deactivate_hotwaterdemand", False)
    
    hotwaterBoiler = kwargs.get("hotwaterBoiler", False)
    deactivate_hotwaterBoiler = kwargs.get("deactivate_hotwaterBoiler", False)
    
    thermalStorage = kwargs.get("thermalstorage", False)
    deactivate_thermalStorage = kwargs.get("deactivate_thermalstorage", False)
    
    sell_tariff_lsc_heat = np.array(consumer1.input_data['heatFeedin'])
    sell_tariff_lsc_heat = kwargs.get("sell_tariff_lsc_heat", sell_tariff_lsc_heat)
    deactivate_heat2lsc = kwargs.get("deactivate_heat2lsc", False)
    heat2lsc = kwargs.get("heat2lsc", False)
    
    buy_tariff_lsc_heat = np.add(sell_tariff_lsc_heat, c_grid_heat)
    buy_tariff_lsc_heat = kwargs.get("buy_tariff_lsc_heat", buy_tariff_lsc_heat)
    deactivate_lsc2heat = kwargs.get("deactivate_lsc2heat", False)
    lsc2heat = kwargs.get("lsc2heat", False)
    
    
    #Component Logic
    
    #Demand
    if(deactivate_heatdemand is False):
        if(heatdemand is False):
            heatdemand = heat_comp.Demand(sector=consumer1.getHeatSector(), demand_timeseries=consumer1.input_data['heatDemand'], label='Heatdemand_' + consumer1.label)
        components.append(heatdemand)
        
    #Demand Hotwater
    if(deactivate_hotwaterdemand is False):
        if(hotwaterdemand is False):
            hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer1.getHotwaterSector(), demand_timeseries=consumer1.input_data['hotwaterDemand'], label='Hotwater_' + consumer1.label)
        components.append(hotwaterdemand)
        
    #Hotwater Boiler
    if(deactivate_hotwaterBoiler is False):
        if(hotwaterBoiler is False):
            hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_' + consumer1.label)
        components.append(hotwaterBoiler)
        
    #Heat Storage
    if(deactivate_thermalStorage is False):
        if(thermalStorage is False):
            thermalStorage = heat_comp.ThermalStorage(sector_in=consumer1.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_' + consumer1.label)
        components.append(thermalStorage)
        
    #Heat to LSC
    if(deactivate_heat2lsc is False):
        if(heat2lsc is False):
            heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer1.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer1.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat)
        components.append(heat2lsc)
    
    #Heat LSC to Consumer
    if(deactivate_lsc2heat is False):
        if(lsc2heat is False):
            lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer1.getHeatSector(),
                                    P=10, label="LSC2heat_" + consumer1.label, rev_lsc=sell_tariff_lsc_heat, c_consumer=buy_tariff_lsc_heat)
        components.append(lsc2heat)
        
        
    #_____________ Cool _____________
    
    #Cool parameters
    c_grid_cool=kwargs.get("c_grid_cool", 0.046)
    
    #Cooling compoonents
    cooldemand = kwargs.get("cooldemand", False)
    deactivate_cooldemand = kwargs.get("deactivate_cooldemand", False)
    
    sell_tariff_lsc_cool = np.array(consumer1.input_data['coolingFeedin'])
    sell_tariff_lsc_cool = kwargs.get("sell_tariff_lsc_cool", sell_tariff_lsc_cool)
    deactivate_cool2lsc = kwargs.get("deactivate_cool2lsc", False)
    cool2lsc = kwargs.get("cool2lsc", False)
    
    buy_tariff_lsc_cool = np.add(sell_tariff_lsc_cool, c_grid_cool)
    buy_tariff_lsc_cool = kwargs.get("buy_tariff_lsc_cool", buy_tariff_lsc_cool)
    deactivate_lsc2cool = kwargs.get("deactivate_lsc2cool", False)
    lsc2cool = kwargs.get("lsc2cool", False)
    
    #Component Logic
    
    #Demand
    if(deactivate_cooldemand is False):
        if(cooldemand is False):
            cooldemand = cool_comp.Demand(sector=consumer1.getCoolingSector(), demand_timeseries=consumer1.input_data['coolingDemand'], label='Coolingdemand_' + consumer1.label)
        components.append(cooldemand)
        
    #Cool to LSC
    if(deactivate_cool2lsc is False):
        if(cool2lsc is False):
            cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer1.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="Cool2LSC_" + consumer1.label, rev_consumer=sell_tariff_lsc_cool, c_lsc=sell_tariff_lsc_cool)
        components.append(cool2lsc)
        
    #LSC to Cool
    if(deactivate_lsc2cool is False):
        if(lsc2cool is False):
            lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer1.getCoolingSector(),
                                    P=10, label="LSC2cool_" + consumer1.label, rev_lsc=sell_tariff_lsc_cool, c_consumer=buy_tariff_lsc_cool)
        components.append(lsc2cool)
        
        
    #_____________ Water _____________
    
    #water parameters
    wff = kwargs.get("willingness_for_flexibility", consumer1.get_willingness_for_waterflexibility())
    
    #water components
    pipelinepurchaseLimited = kwargs.get("pipelinepurchase_limited", False)
    deactivate_pipelinepurchaseLimited = kwargs.get("deactivate_pipelinepurchase_limited", False)
    
    lscwaterpurchaseLimited = kwargs.get("lscwaterpurchase_limited", False)
    deactivate_lscwaterpurchase = kwargs.get("deactivate_lscwaterpurchase", False)
    
    poolPurchase = kwargs.get("lsc_poolpurchase", False)
    deactivate_poolpurchase = kwargs.get("deactivate_poolpurchase", False)
    
    pipelineExcessPurchase = kwargs.get("pipelinepurchase_excess", False)
    deactivate_pipelinepurchaseExcess= kwargs.get("deactivate_pipelinepurchase_excess", False)
    
    waterdemandPredefine = kwargs.get("waterdemand_predefine", False)
    deactivate_waterdemand_predefine = kwargs.get("deactivate_waterdemand_predefine", False)
    
    waterdemandFlex = kwargs.get("waterdemand_flex", False)
    deactivate_waterdemand_flex = kwargs.get("deactivate_waterdemand_flex", False)
    
    waterFlexibility = kwargs.get("waterFlexibility", False)
    deactivate_waterflexibility = kwargs.get("deactivate_waterflexibility", False)
    
    
    #Component logic
    if(deactivate_pipelinepurchaseLimited is False):
        if(pipelinepurchaseLimited is False):
            pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer1.getPotablewaterSector(), label="PipelinepurchaseLimited_" + consumer1.label, color='purple',
                                                      demand=consumer1.input_data['waterDemand'], limit=0.5, costs_water=consumer1.input_data['waterPipelineCosts'], discount=1)

        components.append(pipelinepurchaseLimited)
        
    
    if(deactivate_lscwaterpurchase is False):
        if(lscwaterpurchaseLimited is False):
            lscwaterpurchaseLimited = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getPotablewaterSector(), sector_out=consumer1.getPotablewaterSector(), 
                                                           label="LSCpurchaseLimited_" + consumer1.label, color='brown',
                                                           demand=consumer1.input_data['waterDemand'], limit=0.5, 
                                                           costs_consumer=consumer1.input_data['waterPipelineCosts'], discount=0.75, revenues_lsc=consumer1.input_data['waterPipelineCosts'])
        components.append(lscwaterpurchaseLimited)
        
        
    if(deactivate_poolpurchase is False):
        if(poolPurchase is False):
            poolPurchase = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getWaterpoolSector(), sector_out=consumer1.getPotablewaterSector(), 
                                                           label="Waterpoolpurchase_" + consumer1.label, color='gold',
                                                           demand=consumer1.input_data['waterDemand'], limit=1, 
                                                           costs_consumer=consumer1.input_data['waterPipelineCosts'], discount=0.5, revenues_lsc=consumer1.input_data['waterPipelineCosts'])
        components.append(poolPurchase)
        
        
    if(deactivate_pipelinepurchaseExcess is False):
        if(pipelineExcessPurchase is False):
            pipelineExcessPurchase = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer1.getPotablewaterSector(), label="PipelinepurchaseExcess_" + consumer1.label, color='magenta',
                                                      demand=consumer1.input_data['waterDemand'], limit=1, costs_water=consumer1.input_data['waterPipelineCosts'], discount=2)
        components.append(pipelineExcessPurchase)
        
        
    if(deactivate_waterdemand_predefine is False):
        if(waterdemandPredefine is False):
            waterdemandPredefine = water_comp.WaterdemandPredefine(timesteps=timesteps, sector=consumer1.getWaterdemandSector(), label="WaterdemandPredefine_" + consumer1.label,
                                                       color='yellow', demand=consumer1.input_data['waterDemand'])
        components.append(waterdemandPredefine)
        
      
    if(deactivate_waterdemand_flex is False):
        if(waterdemandFlex is False):
            waterdemandFlex = water_comp.WaterDemandSewageFlex(timesteps=timesteps, sector_in=consumer1.getPotablewaterSector(), sector_out=consumer1.getWaterdemandSector(), sector_sewage=LSC.getSewageSector(),
                                                   label="WaterdemandFlexible_" + consumer1.label, color='beige')
        components.append(waterdemandFlex)
        
    if(deactivate_waterflexibility is False):
        if(waterFlexibility is False):
            waterFlexibility = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer1.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_" + consumer1.label,
                                                       color='darkcyan', demand=consumer1.input_data['waterDemand'], wff=wff,
                                                       costs_lsc=consumer1.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer1.input_data['waterPipelineCosts'])

        components.append(waterFlexibility)
        
        
    
    #_____________ Waste _____________
    
    #waste parameters
    wfr = kwargs.get("willingness_for_recycling", consumer1.get_willingness_for_recycling())
    disposalPeriodsHousehold = kwargs.get("disposalperiods_household", 168)
    v_wastetruck=kwargs.get("v_wastetruck", 10)
    s_wastetruck=kwargs.get("s_wastetruck", 20)
    delay_wastetruck = math.ceil(s_wastetruck/v_wastetruck)
    
    
    #waste components
    wasteAccruing = kwargs.get("waste_accruing", False)
    deactivate_wasteAccruing = kwargs.get("deactivate_wasteAccruing", False)
    
    wasteFlexibility = kwargs.get("wasteFlexibility", False)
    deactivate_wasteflexibility = kwargs.get("deactivate_wasteflexibility", False)
    
    wasteStorageConsumer = kwargs.get("wastestorage_consumer", False)
    deactivate_wasteStorageConsumer = kwargs.get("deactivate_wastestorageConsumer", False)
    
    wasteStorageHousehold = kwargs.get("wasteStorageHousehold", False)
    deactivate_wasteStorageHousehold = kwargs.get("deactivate_wastestorage_household", False)
    
    transmissionWaste = kwargs.get("transmissionWaste", False)
    deactivate_transmissionWaste = kwargs.get("deactivate_transmissionWaste", False)
    
    
    #component logic
    if(deactivate_wasteAccruing is False):
        if(wasteAccruing is False):
            wasteAccruing = waste_comp.Accruing(sector=consumer1.getWasteSector(), waste_timeseries=consumer1.input_data['waste'], label='WasteAccruing_' + consumer1.label, color="yellow")
        components.append(wasteAccruing)
        
        
    if(deactivate_wasteflexibility is False):
        if(wasteFlexibility is False):
            wasteFlexibility = lsc_comp.WasteFlexibility(sector=consumer1.getWasteSector(), timesteps=timesteps, revenues=consumer1.input_data['wasteMarket'],
                                             label="WasteFlexibility_" + consumer1.label, color='forestgreen', waste=consumer1.input_data['waste'], 
                                             wfr=wfr, share_recycling=1)
        components.append(wasteFlexibility)
        
        
    if(deactivate_wasteStorageConsumer is False):
        if(wasteStorageConsumer is False):
            wasteStorageConsumer = waste_comp.WasteStorage(sector_in=consumer1.getWasteSector(), sector_out=consumer1.getWastestorageSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + consumer1.label, color='mistyrose', balanced=True)
        components.append(wasteStorageConsumer)
        
        
    if(deactivate_wasteStorageHousehold is False):
        if(wasteStorageHousehold is False):
            wasteStorageHousehold = waste_comp.WasteStorage(sector_in=consumer1.getWastestorageSector(), sector_out=consumer1.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=disposalPeriodsHousehold, timesteps=timesteps,
                                       label="WastestorageHousehold_" + consumer1.label.split('_')[1], color='mediumvioletred', balanced=False, empty_end=True, t_start=168)
        components.append(wasteStorageHousehold)
        
        
    if(deactivate_transmissionWaste is False):
        if(transmissionWaste is False):
            transmissionWaste = lsc_comp.Delay_Transport(sector_in=consumer1.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_' + consumer1.label, color='aquamarine')
        components.append(transmissionWaste)
        
        
    return components


def lsc_setup(*args, **kwargs):
    
    #List of Components
    components=[]
    
    #Consumer
    LSC = kwargs.get("LSC", 0)
    timesteps=kwargs.get("timesteps", 8784)
    
    if(LSC==0):
        return 0
    
    #_____________ Electricity _____________
    
    c_grid_elec=kwargs.get("c_grid_elec", 0.062)
    c_abgabe_elec = kwargs.get("c_abgabe_elec", 0.018)
    peakpower_elec = kwargs.get("peakpower_elec", 33)
    c_power_elec = kwargs.get("c_power_elec", 35)
    
    deactivate_electricitygridpurchase = kwargs.get("deactivate_electricitygridpurchase", False)
    elgridLSC = kwargs.get("electricitygridpurchase", False)
    
    deactivate_peakpower = kwargs.get("deactivate_peakpower", False)
    peakpowerCombined = kwargs.get("peakpower", False)
    
    deactivate_pv = kwargs.get("deactivate_pv", False)
    pvLSC = kwargs.get("pv", False)
    
    deactivate_electricityfeedin = kwargs.get("deactivate_electricityfeedin", False)
    elgridFeedinLSC = kwargs.get("electricityfeedin", False)
    
    deactivate_battery = kwargs.get("deactivate_battery", False)
    batteryLSC = kwargs.get("battery", False)
    
    deactivate_heatpump = kwargs.get("deactivate_heatpump", False)
    heatpumpLSC = kwargs.get("heatpump", False)
    
    deactivate_elcool = kwargs.get("deactivate_elcool", False)
    elcoolLSC = kwargs.get("elcool", False)
    
    
    #Gridpurchase
    if(deactivate_electricitygridpurchase is False):
        if(elgridLSC is False):
            elgridLSC = el_comp.GridCombinedPowerMetering(P=22, sector_grid=LSC.getElectricityGridSector(), sector_electricity=LSC.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              costs_energy=np.add(LSC.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_LSC', timesteps=timesteps)             
        components.append(elgridLSC.get_source())
        components.append(elgridLSC.get_toElec())
        
    #Peakpower Costs
    if(deactivate_peakpower is False):
        if(peakpowerCombined is False):
            peakpowerCombined = el_comp.PeakpowerCosts(sector=LSC.getPeakpowerSector(),  
                                 label='ElectricitypeakpowerCombined', timesteps=timesteps, P=peakpower_elec, costs_power=c_power_elec)              
        components.append(peakpowerCombined)
        
    #PV
    if(deactivate_pv is False):
        if(pvLSC is False):
            pvLSC = el_comp.Photovoltaic(sector=LSC.getElectricitySector(), generation_timeseries=LSC.input_data['elGen'], 
                          label='PV_LSC', timesteps=timesteps, area_efficiency=0.5, area_roof=400, area_factor=10)              
        components.append(pvLSC)
        
    #Electricitygrid_Feedin
    if(deactivate_electricityfeedin is False):
        if(elgridFeedinLSC is False):
            elgridFeedinLSC = el_comp.GridFeedin(sector=LSC.getElectricitySector(), revenues=LSC.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_LSC', timesteps=timesteps)                
        components.append(elgridFeedinLSC)
        
    #Battery
    if(deactivate_battery is False):
        if(batteryLSC is False):
            batteryLSC = el_comp.Battery(sector_in=LSC.getElectricitySector(), label='Battery_LSC',
                          soc_max=25, SOC_start=25, P_in=25, P_out=25)               
        components.append(batteryLSC)
        
    #Heatpump
    if(deactivate_heatpump is False):
        if(heatpumpLSC is False):
            heatpumpLSC = el_comp.Heatpump(sector_in = LSC.getElectricitySector(), sector_out=LSC.getHeatSector(), 
                            conversion_timeseries=LSC.input_data['copHP'], timesteps=timesteps, P_in=30, P_out=30, label='Heatpump_LSC')               
        components.append(heatpumpLSC)
        
    #Electric Cooling
    if(deactivate_elcool is False):
        if(elcoolLSC is False):
            elcoolLSC = el_comp.ElectricCooling(sector_in=LSC.getElectricitySector(), sector_out=LSC.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=LSC.input_data['copCool'], P_in=30, P_out=30, label="Cooler_LSC")               
        components.append(elcoolLSC)
        
        
    #_____________ Heat _____________
    
    deactivate_thermalStorage = kwargs.get("deactivate_thermalStorage", False)
    thermalStorageLSC = kwargs.get("thermalStorage", False)
        
    #Peakpower Costs
    if(deactivate_thermalStorage is False):
        if(thermalStorageLSC is False):
            thermalStorageLSC = heat_comp.ThermalStorage(sector_in=LSC.getHeatSector(), P_in=30, volume_max=1.5, label='Heatstorage_LSC')              
        components.append(thermalStorageLSC)
        
        
    #_____________ Cooling _____________
    #No cooling components for LSC
    
    
    #_____________ Water _____________
    
    deactivate_waterpoolstorage = kwargs.get("deactivate_waterpoolstorage", False)
    waterPoolStorage = kwargs.get("waterPoolStorage", False)
    
    deactivate_poolwaterWasted = kwargs.get("deactivate_poolwaterWasted", False)
    poolwaterWasted = kwargs.get("poolwaterWasted", False)
    
    #Option 1 Wasted LSC Water
    deactivate_lscwaterWasted = kwargs.get("deactivate_lscwaterWasted", False)
    lscwaterWasted = kwargs.get("lscwaterWasted", False)
    
    #Option 2 LSC Water Storage --> Standard True as Wasted is the primary option
    deactivate_waterLSCStorage = kwargs.get("deactivate_waterLSCStorage", True)
    waterLSCStorage = kwargs.get("waterLSCStorage", False)
    
    #Waterpoolstorage
    if(deactivate_waterpoolstorage is False):
        if(waterPoolStorage is False):
            waterPoolStorage = water_comp.WaterStorage(sector_in=LSC.getWaterpoolSector(), eta_in=1, eta_out=1, eta_sb=0.99999999,
                                           c_in=0, c_out=0, volume_start=0, volume_max=10000, Q_max=1000,
                                           label="Waterpoolstorage_LSC", color='olivedrab', balanced=True)              
        components.append(waterPoolStorage)
        
    #Poolwater Wasted
    if(deactivate_poolwaterWasted is False):
        if(poolwaterWasted is False):
            poolwaterWasted = lsc_comp.WaterWasted(sector=LSC.getWaterpoolSector(), label='Poolwaterwasted_LSC',
                                       color='darkkhaki', costs_water=LSC.input_data['waterPipelineCosts'], discount=0.5)              
        components.append(poolwaterWasted)
        
    #Option 1 LSC Water Wasted
    if(deactivate_lscwaterWasted is False):
        if(lscwaterWasted is False):
            lscwaterWasted = lsc_comp.WaterWasted(timesteps=timesteps, sector=LSC.getPotablewaterSector(), label='LSCwaterwasted_LSC',
                                       color='brown')              
        components.append(lscwaterWasted)
        
    #Option 2 LSC Water Storage
    if(deactivate_waterLSCStorage is False):
        if(waterLSCStorage is False):
            waterLSCStorage = water_comp.WaterStorage(sector_in=LSC.getWPotablewaterSector(), eta_in=1, eta_out=1, eta_sb=1,
                                           c_in=0, c_out=0, volume_start=0, volume_max=10000, Q_max=1000,
                                           label="LSCwaterstorage_LSC", color='brown', balanced=True)             
        components.append(waterLSCStorage)
        
        
    #_____________ Sewage and Sludge _____________
    
    #Sewage Operation and transmission parameters
    costs_sewagetreat_operation = kwargs.get("costs_sewagetreatment_operation", 0.04)
    costs_sewage_surcharge_factor = kwargs.get("costs_sewage_surcharge_factor", 1.1)
    eta_waterrecovery = kwargs.get("eta_waterrecovery", 0.1)
    delay_waterrecovery = kwargs.get("delay_waterrecovery", 24)
    
    #Sludgetruck Parameters
    v_sludgetruck = kwargs.get("v_sludgetruck", 60)
    s_sludgetruck = kwargs.get("s_sludgetruck", 20)
    delay_sludgetruck = math.ceil(s_sludgetruck/v_sludgetruck)
    
    #Sludge Recovery Parameter
    sludge_recovery_used = kwargs.get("sludge_recovery_used", 0.5)
    
    deactivate_sewagetreatmentplant = kwargs.get("deactivate_sewagetreatmentplant", False)
    sewageTreatmentPlant = kwargs.get("sewageTreatmentPlant", False)
    
    deactivate_waterrecoveryDelay = kwargs.get("deactivate_waterrecoveryDelay", False)
    waterrecoveryDelay = kwargs.get("waterrecoveryDelay", False)
    
    deactivate_sludgeStoragePlant = kwargs.get("deactivate_sludgeStoragePlant", False)
    sludgeStoragePlant = kwargs.get("sludgeStoragePlant", False)
    
    deactivate_sludgetruck = kwargs.get("deactivate_sludgetruck", False)
    sludgetruck = kwargs.get("sludgetruck", False)
    
    deactivate_sludgestorageTreatment = kwargs.get("deactivate_sludgestorageTreatment", False)
    sludgestorageTreatment = kwargs.get("sludgestorageTreatment", False)
    
    deactivate_sludgecombustion = kwargs.get("deactivate_sludgecombustion", False)
    sludgecombustion = kwargs.get("sludgecombustion", False)
    
    deactivate_sludgedisposal = kwargs.get("deactivate_sludgedisposal", False)
    sludgedisposal = kwargs.get("sludgedisposal", False)
    
    #Sewage Treatment Plant
    if(deactivate_sewagetreatmentplant is False):
        if(sewageTreatmentPlant is False):
            sewageTreatmentPlant = water_comp.SewageTreatment_noElec(sector_in=LSC.getSewageSector(), 
                                                  sector_out=[LSC.getWaterrecoverysector(), LSC.getSludgeSector(), LSC.getHeatSector()], 
                                                  tempdif=0, label='SewageTreatment_LSC', color='black', c_in=costs_sewage_surcharge_factor*costs_sewagetreat_operation, eta=eta_waterrecovery)             
        components.append(sewageTreatmentPlant)
        
        
    #Delay Waterrecovery
    if(deactivate_waterrecoveryDelay is False):
        if(waterrecoveryDelay is False):
            waterrecoveryDelay = lsc_comp.Delay_Transport(sector_in=LSC.getWaterrecoverysector(), sector_out=LSC.getPotablewaterSector(), 
                                        efficiency=1, costs_transmission=0, delay=delay_waterrecovery, timesteps=timesteps, capacity_transmission=1000,
                                        label='Waterrecoverydelay_LSC', color='red')          
        components.append(waterrecoveryDelay)
        
    #Sludge Storage at Plant
    if(deactivate_sludgeStoragePlant is False):
        if(sludgeStoragePlant is False):
            sludgeStoragePlant = water_comp.SludgeStorage(sector_in=LSC.getSludgeSector(), sector_out=LSC.getSludgestorageSector(), timesteps=timesteps,
                                         c_in=0.01, c_out=0.01, volume_start=0, volume_max=1.5, Q_max=1.5, disposal_periods=24, balanced=True,
                                         label="SludgestorageSewageplant_LSC", color='darkkhaki')          
        components.append(sludgeStoragePlant)
        
    #Sludgetruck
    if(deactivate_sludgetruck is False):
        if(sludgetruck is False):
            sludgetruck = lsc_comp.Delay_Transport(sector_in=LSC.getSludgestorageSector(), sector_out=LSC.getSludgetreatmentSectorIn(), 
                                        efficiency=1, costs_transmission=0.7, delay=delay_sludgetruck, timesteps=timesteps, capacity_transmission=20,
                                        label='Sludgetransport_LSC', color='aquamarine')          
        components.append(sludgetruck)
        
    #Sludgestorage at Treatment Area
    if(deactivate_sludgestorageTreatment is False):
        if(sludgestorageTreatment is False):
            sludgestorageTreatment= water_comp.SludgeStorage(sector_in=LSC.getSludgetreatmentSectorIn(), sector_out=LSC.getSludgetreatmentSectorOut(), timesteps=timesteps,
                                         c_in=0.01, c_out=0.01, volume_start=0.1, volume_max=1.5, Q_max=1.5, disposal_periods=0, balanced=True,
                                         label="SludgestorageTreatment_LSC", color='darkgoldenrod')        
        components.append(sludgestorageTreatment)
        
    #Sludge Incineration Plant
    if(deactivate_sludgecombustion is False):
        if(sludgecombustion is False):
            sludgecombustion = water_comp.Sludgecombustion(sector_in=LSC.getSludgetreatmentSectorOut(), sector_out=[LSC.getElectricitySector(), LSC.getHeatSector()],
                                               conversion_factor=[0.35, 0.4], c_in=0, c_out=[0.06, 0.06], P_in=100, P_out=100, usable_energy=sludge_recovery_used,
                                               label='Sludgecombustion_LSC', color='lightsteelblue')       
        components.append(sludgecombustion)
        
    #Sludge Disposal
    if(deactivate_sludgedisposal is False):
        if(sludgedisposal is False):
            sludgedisposal = water_comp.Sludgedisposal(sector=LSC.getSludgetreatmentSectorOut(), costs=LSC.input_data['sludgeCosts'],
                                           label='Sludgedisposal_LSC', color='steelblue')           
        components.append(sludgedisposal)
        
        
    #_____________ Waste _____________
        
    #Used Recovered Waste
    waste_recovery_used = kwargs.get("waste_recovery_used", 0.5)
    
    deactivate_wastestorageTreatment = kwargs.get("deactivate_wastestorageTreatment", False)
    wasteStorageTreatment = kwargs.get("wasteStorageTreatment", False)
    
    deactivate_wasteCombustion = kwargs.get("deactivate_wasteCombustion", False)
    wasteCombustion = kwargs.get("wasteCombustion", False)
    
    deactivate_wasteDisposal = kwargs.get("deactivate_wasteDisposal", False)
    wasteDisposal = kwargs.get("wasteDisposal", False)
    
    
    #Waste storage at treatment site
    if(deactivate_wastestorageTreatment is False):
        if(wasteStorageTreatment is False):
            wasteStorageTreatment = waste_comp.WasteStorage(sector_in=LSC.getWastestorageSector(), sector_out=LSC.getWastedisposalSector(), 
                                       volume_max=12, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="WastestorageDisposal_LSC", color='mistyrose', balanced=True)          
        components.append(wasteStorageTreatment)
        
    #Waste Combustion
    if(deactivate_wasteCombustion is False):
        if(wasteCombustion is False):
            wasteCombustion = waste_comp.Wastecombustion(sector_in=LSC.getWastedisposalSector(), sector_out=[LSC.getElectricitySector(), LSC.getHeatSector()],
                                             c_in=0, c_out=[0.04, 0.04], conversion_factor=[0.35, 0.4], P_in=100, P_out=100, usable_energy=waste_recovery_used,
                                             label='Wastecombustion_LSC', color='lightsteelblue')         
        components.append(wasteCombustion)
        
    #Waste Disposal
    if(deactivate_wasteDisposal is False):
        if(wasteDisposal is False):
            wasteDisposal = waste_comp.Wastedisposal(sector=LSC.getWastedisposalSector(), costs=LSC.input_data['wasteCosts'],
                                         label='Wastedisposal_LSC', color='steelblue', min_disposal=0)        
        components.append(wasteDisposal)
        
        
    #_____________ Household Components _____________
    
    deactivate_washingMaschine = kwargs.get("deactivate_washingMaschine", False)
    washingMaschine = kwargs.get("washingMaschine", False)
    
    deactivate_washingMaschineWaterinput = kwargs.get("deactivate_washingMaschineWaterinput", False)
    waterpipelinePurchaseWashing = kwargs.get("washingMaschineWaterinput", False)
    
    deactivate_washingMaschineWaterrecovery = kwargs.get("deactivate_washingMaschineWaterrecovery", False)
    waterrecoveryWash = kwargs.get("washingMaschineWaterrecovery", False)
    
    #Washing Maschine
    if(deactivate_washingMaschine is False):
        if(washingMaschine is False):
            washingMaschine = household_comp.WashingMaschine(sector_in=LSC.getElectricitySector(), sector_out=LSC.getHeatSector(), 
                                                 sector_inAdditional=LSC.getHeatSector(), sector_loss=LSC.getWaterwashingSector(), 
                                                 Twash=LSC.input_data['washTemperature'], timesteps=timesteps, position='LSC',
                                                 max_elec=1, max_therm=1)        
        components.append(washingMaschine.get_elec())
        components.append(washingMaschine.get_heatrecovery())
        components.append(washingMaschine.get_waterinput())
        components.append(washingMaschine.get_elecHeat())
        components.append(washingMaschine.get_thermHeat())
        
        
    #Water Pipeline Purchase for Washing Maschine
    if(deactivate_washingMaschineWaterinput is False):
        if(waterpipelinePurchaseWashing is False):
            waterpipelinePurchaseWashing = water_comp.PipelinePurchase(sector=LSC.getWaterwashingSector(), costs_water=LSC.input_data['waterPipelineCosts'], timesteps=timesteps,
                                                           label='WaterwashingPipeline_LSC', color='lemonchiffon')           
        components.append(waterpipelinePurchaseWashing)
        
    #Recovered Water for Washing Maschine
    if(deactivate_washingMaschineWaterrecovery is False):
        if(waterrecoveryWash is False):
            waterrecoveryWash = lsc_comp.WaterrecoveryTransformer(sector_in=LSC.getWaterrecoverysector(), sector_out=LSC.getWaterwashingSector(), 
                                                      label='WaterrecoveryWashing_LSC', color='mediumturquoise')           
        components.append(waterrecoveryWash)
        
    return components

    
    
def lsc_transportdemand(*args, **kwargs):
    
    #List of Components
    components=[]
    
    #Consumer
    LSC = kwargs.get("LSC", 0)
    consumers = kwargs.get("consumers", [])
    
    if(LSC==0 or len(consumers)==0):
        return 0
    
    for consumer in consumers:
        if(np.sum(consumer.input_data['transportDemand']) != 0):
            mobilitydemand = trans_comp.Demand(sector=LSC.getTransportSector(), demand_timeseries=consumer.input_data['transportDemand'], label='Transport_' + consumer.label, label_sankey='Transport_' + consumer.label)
            components.append(mobilitydemand)
            
    return components


def lsc_vehicles(*args, **kwargs):
    
    #List of Components
    components=[]
    
    #Consumer
    LSC = kwargs.get("LSC", 0)
    timesteps = kwargs.get("timesteps", 8784)
    
    if(LSC==0):
        return 0
    
    
    deactivate_vehicle1 = kwargs.get("deactivate_vehicle1", False)
    kia = kwargs.get("vehicle1", False)
    
    deactivate_vehicle2 = kwargs.get("deactivate_vehicle2", False)
    nissan = kwargs.get("vehicle2", False)
    
    deactivate_vehicle3 = kwargs.get("deactivate_vehicle3", False)
    zoe = kwargs.get("vehicle3", False)
    
    
    #Kia Soul
    if(deactivate_vehicle1 is False):
        if(kia is False):
            kia=trans_comp.ElectricVehicle(sector_in=LSC.getElectricitySector(), sector_out=LSC.getSector(sectorname ='LSC_ElectricVehicle1sector'), 
                                           sector_drive=LSC.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=2.3, P_drive=150, soc_max=64, soc_start=64,
                                           label='KiaSoul_', labelDrive='KiaSoulDrive_', label_sankey='KiaSoul_', color='red')          
        components.append(kia)
        
    #Nissan Leaf
    if(deactivate_vehicle2 is False):
        if(nissan is False):
            nissan=trans_comp.ElectricVehicle(sector_in=LSC.getElectricitySector(), sector_out=LSC.getSector(sectorname ='LSC_ElectricVehicle2sector'), 
                                           sector_drive=LSC.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=3.7, P_drive=110, soc_max=40, soc_start=40,
                                           label='NissanLeaf_', labelDrive='NissanLeafDrive_', label_sankey='NissanLeaf_', color='magenta')          
        components.append(nissan)
        
    #Renault Zoe
    if(deactivate_vehicle3 is False):
        if(zoe is False):
            zoe=trans_comp.ElectricVehicle(sector_in=LSC.getElectricitySector(), sector_out=LSC.getSector(sectorname ='LSC_ElectricVehicle3sector'), 
                                           sector_drive=LSC.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=70, soc_max=40, soc_start=40,
                                           label='RenaultZoe_', labelDrive='RenaultZoeDrive_', label_sankey='RenaultZoe_', color='blue')         
        components.append(zoe)
    
            
    return components

    

def get_component_by_name(*args, **kwargs):
    
    components = kwargs.get("components", 0)
    name = kwargs.get("name", 0)
    
    if(name==0 or components==0):
        return 0
    
    for component in components:
        if(name in component.label):
            return component
    
    
        
        
    
    
    