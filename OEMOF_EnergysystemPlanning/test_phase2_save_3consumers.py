# -*- coding: utf-8 -*-
"""
Created on Tue May 10 11:00:47 2022

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'test_phase2_transport'

#Filename
filename = "Input_Model_Hour.xlsx"

#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 8784
timestepsTotal = 8784

#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    

# -------------- Additional parameters that are set for the simulation -------------- 

#_____________ Electricity _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzentgelte
c_grid = 0.062

#Abgabe
c_abgabe = 0.018

#Reduktion Netzentgelt Lokalbereich
red_local = 1-0.57

#Reduktion Netzentgelt Regionalbereich
red_regional = 1-0.28

#_____________ Heat _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_heat = 0.046


#_____________ Cooling _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_cool = 0.046


#---------------------- DEFINITION OF CONSUMERS ----------------------

#Consumer 1
consumer1 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_1')
consumer1.addElectricitySector()
consumer1.addHeatSector()
consumer1.addCoolingSector()
consumer1.addWaterSector()
consumer1.addWasteSector()
#consumer1.addSector('testsector')
#TESTSTESTE = consumer1.getSector('testsector')
consumer1.sector2system(my_energysystem)

#Consumer 2
consumer2 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_2')
consumer2.addElectricitySector()
consumer2.addHeatSector()
consumer2.addCoolingSector()
consumer2.addWaterSector()
consumer2.addWasteSector()
consumer2.sector2system(my_energysystem)

#Consumer 3
consumer3 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_3')
consumer3.addElectricitySector()
consumer3.addHeatSector()
consumer3.addCoolingSector()
consumer3.addWaterSector()
consumer3.addWasteSector()
consumer3.sector2system(my_energysystem)

#LSC
LSC = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='LSC')
LSC.addElectricitySector()
LSC.addHeatSector()
LSC.addCoolingSector()
LSC.addWaterSector()
LSC.addWaterpoolSector()
LSC.addWasteSector()
LSC.addTransportSector()
LSC.sector2system(my_energysystem)



#---------------------- Consumer 1 Components ----------------------

#_____________ Electricity _____________

#Demand
elecdemand = el_comp.Demand(sector=consumer1.getElectricitySector(), 
                            demand_timeseries=consumer1.input_data['electricityDemand'], label='Electricitydemand_Consumer_1')
my_energysystem.add(elecdemand.component())
consumer1.addComponent(elecdemand)


#Grid Purchase no combined metering
"""
elgrid = el_comp.GridPurchase(sector=consumer1.getElectricitySector(), 
                              costs_energy=np.add(consumer1.input_data['electricityCosts'], c_grid + c_abgabe), 
                              label='Electricitygridpurchase_Consumer_1', timesteps=timesteps, costs_power=0)        
my_energysystem.add(elgrid.component())
consumer1.addComponent(elgrid)
"""

#Grid Purchase combined metering
#LSC Peakpower sector to evaluate the combined peak power --> the peakpower component must also be assigned to this sector

elgrid = el_comp.GridCombinedPowerMetering(sector_grid=consumer1.getElectricityGridSector(), sector_electricity=consumer1.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              costs_energy=np.add(consumer1.input_data['electricityCosts'], c_grid + c_abgabe), 
                              label='Electricitygridpurchase_Consumer_1', timesteps=timesteps)        

elgridGrid = elgrid.get_source()
my_energysystem.add(elgridGrid.component())
consumer1.addComponent(elgridGrid)

elgridElec = elgrid.get_toElec()
my_energysystem.add(elgridElec.component())
consumer1.addComponent(elgridElec)



#Photovoltaic
pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_Consumer_1', timesteps=timesteps, area_efficiency=0.5, area_roof=100, area_factor=10)
my_energysystem.add(pv.component())
consumer1.addComponent(pv)


#Grid Feedin
elgridFeedin = el_comp.GridFeedin(sector=consumer1.getElectricitySector(), revenues=consumer1.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_Consumer_1', timesteps=timesteps)        
my_energysystem.add(elgridFeedin.component())
consumer1.addComponent(elgridFeedin)

#Battery
battery = el_comp.Battery(sector_in=consumer1.getElectricitySector(), label='Battery_Consumer_1',
                          soc_max=5, SOC_start=5, P_in=5, P_out=5)
my_energysystem.add(battery.component())
consumer1.addComponent(battery)

#Electricity Consumer to LSC
sell_tariff = np.multiply(np.add(consumer1.input_data['electricityCosts'], consumer1.input_data['electricityFeedin']), 1/2)
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer1.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_Consumer_1", rev_consumer=sell_tariff, c_lsc=sell_tariff)
my_energysystem.add(elec2lsc.component())
consumer1.addComponent(elec2lsc)

#Electricity LSC to Consumer
c_grid_reduced = np.multiply(c_grid, red_local)
buy_tariff = np.add(sell_tariff, np.add(c_grid_reduced, c_abgabe))
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer1.getElectricitySector(),
                                    P=11, label="LSC2elec_Consumer_1", rev_lsc=sell_tariff, c_consumer=buy_tariff)
my_energysystem.add(lsc2elec.component())
consumer1.addComponent(lsc2elec)

#Heatpump
heatpump = el_comp.Heatpump(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector(), 
                            conversion_timeseries=consumer1.input_data['copHP'], timesteps=timesteps, P_in=7, P_out=7, label='Heatpump_Consumer_1') 
my_energysystem.add(heatpump.component())
consumer1.addComponent(heatpump)

#Electric Cooling
elcool = el_comp.ElectricCooling(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=consumer1.input_data['copCool'], P_in=7, P_out=7, label="Cooler_Consumer_1")
my_energysystem.add(elcool.component())
consumer1.addComponent(elcool)


#_____________ Heat _____________

#Demand Heat
heatdemand = heat_comp.Demand(sector=consumer1.getHeatSector(), demand_timeseries=consumer1.input_data['heatDemand'], label='Heatdemand_Consumer_1')
my_energysystem.add(heatdemand.component())
consumer1.addComponent(heatdemand)

#Demand Hotwater
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer1.getHotwaterSector(), demand_timeseries=consumer1.input_data['hotwaterDemand'], label='Hotwater_Consumer_1')
my_energysystem.add(hotwaterdemand.component())
consumer1.addComponent(hotwaterdemand)


#Hot Water Boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_Consumer_1')
my_energysystem.add(hotwaterBoiler.component())
consumer1.addComponent(hotwaterBoiler)

#Heat Storage
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer1.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_Consumer_1')
my_energysystem.add(thermalStorage.component())
consumer1.addComponent(thermalStorage)

#Heat Consumer to LSC
sell_tariff_heat = np.array(consumer1.input_data['heatFeedin'])
heat2lsc = lsc_comp.Heat2LSC(sector_in=consumer1.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_Consumer_1", rev_consumer=sell_tariff_heat, c_lsc=sell_tariff_heat)
my_energysystem.add(heat2lsc.component())
consumer1.addComponent(heat2lsc)

#Heat LSC to Consumer
buy_tariff_heat = np.add(sell_tariff_heat, c_grid_heat)
lsc2heat = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer1.getHeatSector(),
                                    P=10, label="LSC2heat_Consumer_1", rev_lsc=sell_tariff_heat, c_consumer=buy_tariff_heat)
my_energysystem.add(lsc2heat.component())
consumer1.addComponent(lsc2heat)


#_____________ Cooling _____________

#Demand
coolingdemand = cool_comp.Demand(sector=consumer1.getCoolingSector(), demand_timeseries=consumer1.input_data['coolingDemand'], label='Coolingdemand_Consumer_1')
my_energysystem.add(coolingdemand.component())
consumer1.addComponent(coolingdemand)

#Cooling Consumer to LSC
sell_tariff_cool = np.array(consumer1.input_data['coolingFeedin'])
cool2lsc = lsc_comp.Cool2LSC(sector_in=consumer1.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="CoolingLSC_Consumer_1", rev_consumer=sell_tariff_cool, c_lsc=sell_tariff_cool)
my_energysystem.add(cool2lsc.component())
consumer1.addComponent(cool2lsc)

#Cooling LSC to Consumer
buy_tariff_cool = np.add(sell_tariff_cool, c_grid_cool)
lsc2cool = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer1.getCoolingSector(),
                                    P=10, label="LSC2cool_Consumer_1", rev_lsc=sell_tariff_cool, c_consumer=buy_tariff_cool)
my_energysystem.add(lsc2cool.component())
consumer1.addComponent(lsc2cool)



#_____________ Water _____________

#Pipeline Purchase Limited
pipelinepurchaseLimited = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer1.getPotablewaterSector(), label="Pipelinepurchase_Limited_Consumer_1", color='purple',
                                                      demand=consumer1.input_data['waterDemand'], limit=0.5, costs_water=consumer1.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited.component())
consumer1.addComponent(pipelinepurchaseLimited)


#LSC Purchase Limited
lscwaterpurchaseLimited = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getPotablewaterSector(), sector_out=consumer1.getPotablewaterSector(), 
                                                           label="LSCpurchase_Limited_Consumer_1", color='brown',
                                                           demand=consumer1.input_data['waterDemand'], limit=0.5, 
                                                           costs_consumer=consumer1.input_data['waterPipelineCosts'], discount=0.75, revenues_lsc=consumer1.input_data['waterPipelineCosts'])
my_energysystem.add(lscwaterpurchaseLimited.component())
consumer1.addComponent(lscwaterpurchaseLimited)

#Water Consumption Pool Purchase
poolPurchase = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getWaterpoolSector(), sector_out=consumer1.getPotablewaterSector(), 
                                                           label="Waterpoolpurchase_Consumer_1", color='gold',
                                                           demand=consumer1.input_data['waterDemand'], limit=1, 
                                                           costs_consumer=consumer1.input_data['waterPipelineCosts'], discount=0.5, revenues_lsc=consumer1.input_data['waterPipelineCosts'])
my_energysystem.add(poolPurchase.component())
consumer1.addComponent(poolPurchase)

#Unlimited pipeline purchase at high costs
pipelineExcessPurchase = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer1.getPotablewaterSector(), label="Pipelinepurchase_Excess_Consumer_1", color='magenta',
                                                      demand=consumer1.input_data['waterDemand'], limit=1, costs_water=consumer1.input_data['waterPipelineCosts'], discount=2)
my_energysystem.add(pipelineExcessPurchase.component())
consumer1.addComponent(pipelineExcessPurchase)


#Predefined water demand
waterdemandPredefine = water_comp.WaterdemandPredefine(timesteps=timesteps, sector=consumer1.getWaterdemandSector(), label="Waterdemand_Predefine_Consumer_1",
                                                       color='yellow', demand=consumer1.input_data['waterDemand'])
my_energysystem.add(waterdemandPredefine.component())
consumer1.addComponent(waterdemandPredefine)


#Flexible water demand provision and sewage output
waterdemandFlex = water_comp.WaterDemandSewageFlex(timesteps=timesteps, sector_in=consumer1.getPotablewaterSector(), sector_out=consumer1.getWaterdemandSector(), sector_sewage=LSC.getSewageSector(),
                                                   label="Waterdemand_Flexible_Consumer_1", color='beige')
my_energysystem.add(waterdemandFlex.component())
consumer1.addComponent(waterdemandFlex)


#Flexibility of water to consumption pool
willingness_for_flexibility = consumer1.get_willingness_for_waterflexibility()
#willingness_for_flexibility = 0.024024024024024024024
waterFlexibility = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer1.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility_2pool_Consumer_1",
                                                       color='darkcyan', demand=consumer1.input_data['waterDemand'], wff=willingness_for_flexibility,
                                                       costs_lsc=consumer1.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer1.input_data['waterPipelineCosts'])
my_energysystem.add(waterFlexibility.component())
consumer1.addComponent(waterFlexibility)


#_____________ Waste _____________

#Accruing
wasteAccruing = waste_comp.Accruing(sector=consumer1.getWasteSector(), waste_timeseries=consumer1.input_data['waste'], label='WasteAccruing_Consumer_1', color="yellow")
my_energysystem.add(wasteAccruing.component())
consumer1.addComponent(wasteAccruing)


#Waste Reduction and Recycling Flexibility
wfr1=consumer1.get_willingness_for_recycling()
#wfr1=1
wasteFlexibility = lsc_comp.WasteFlexibility(sector=consumer1.getWasteSector(), timesteps=timesteps, revenues=consumer1.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_1", color='forestgreen', waste=consumer1.input_data['waste'], 
                                             wfr=wfr1, share_recycling=1)
my_energysystem.add(wasteFlexibility.component())
consumer1.addComponent(wasteFlexibility)

#Waste Storage for Consumer
wasteStorageConsumer1 = waste_comp.WasteStorage(sector_in=consumer1.getWasteSector(), sector_out=consumer1.getWastestorageSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_Consumer_1", color='mistyrose', balanced=True)
my_energysystem.add(wasteStorageConsumer1.component())
consumer1.addComponent(wasteStorageConsumer1)


#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold1 = waste_comp.WasteStorage(sector_in=consumer1.getWastestorageSector(), sector_out=consumer1.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_1", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)
my_energysystem.add(wasteStorageHousehold1.component())
consumer1.addComponent(wasteStorageHousehold1)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 20
delay_wastetruck = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste = lsc_comp.Delay_Transport(sector_in=consumer1.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_1', color='aquamarine')
my_energysystem.add(transmissionWaste.component())
consumer1.addComponent(transmissionWaste)



#---------------------- Consumer 2 Components ----------------------

#_____________ Electricity _____________

#Demand
elecdemand2 = el_comp.Demand(sector=consumer2.getElectricitySector(), 
                             demand_timeseries=consumer2.input_data['electricityDemand'], label='Electricitydemand_Consumer_2')
my_energysystem.add(elecdemand2.component())
consumer2.addComponent(elecdemand2)


#Grid Purchase no combined metering
"""
elgrid2 = el_comp.GridPurchase(sector=consumer2.getElectricitySector(), 
                               costs_energy=np.add(consumer2.input_data['electricityCosts'], c_grid + c_abgabe), 
                               label='Electricitygridpurchase_Consumer_2', timesteps=timesteps, costs_power=0)        
my_energysystem.add(elgrid2.component())
consumer2.addComponent(elgrid2)
"""

#Grid Purchase combined metering
elgrid2 = el_comp.GridCombinedPowerMetering(sector_grid=consumer2.getElectricityGridSector(), sector_electricity=consumer2.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              costs_energy=np.add(consumer2.input_data['electricityCosts'], c_grid + c_abgabe), 
                              label='Electricitygridpurchase_Consumer_2', timesteps=timesteps)        

elgridGrid2 = elgrid2.get_source()
my_energysystem.add(elgridGrid2.component())
consumer2.addComponent(elgridGrid2)

elgridElec2 = elgrid2.get_toElec()
my_energysystem.add(elgridElec2.component())
consumer2.addComponent(elgridElec2)

#Photovoltaic
pv2 = el_comp.Photovoltaic(sector=consumer2.getElectricitySector(), generation_timeseries=consumer2.input_data['elGen'], 
                           label='PV_Consumer_2', timesteps=timesteps, area_efficiency=0.5, area_roof=100, area_factor=10)
my_energysystem.add(pv2.component())
consumer2.addComponent(pv2)


#Grid Feedin
elgridFeedin2 = el_comp.GridFeedin(sector=consumer2.getElectricitySector(), revenues=consumer2.input_data['electricityFeedin'], 
                                   label='Electricityfeedin_Consumer_2', timesteps=timesteps)        
my_energysystem.add(elgridFeedin2.component())
consumer2.addComponent(elgridFeedin2)

#Electricity Consumer to LSC
sell_tariff2 = np.multiply(np.add(consumer2.input_data['electricityCosts'], consumer2.input_data['electricityFeedin']), 1/2)
elec2lsc2 = lsc_comp.Electricity2LSC(sector_in=consumer2.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_Consumer_2", rev_consumer=sell_tariff2, c_lsc=sell_tariff2)
my_energysystem.add(elec2lsc2.component())
consumer2.addComponent(elec2lsc2)

#Electricity LSC to Consumer
c_grid_reduced2 = np.multiply(c_grid, red_local)
buy_tariff2 = np.add(sell_tariff2, np.add(c_grid_reduced2, c_abgabe))
lsc2elec2 = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer2.getElectricitySector(),
                                    P=11, label="LSC2elec_Consumer_2", rev_lsc=sell_tariff2, c_consumer=buy_tariff2)
my_energysystem.add(lsc2elec2.component())
consumer2.addComponent(lsc2elec2)


#_____________ Heat _____________

#Demand Heat
heatdemand2 = heat_comp.Demand(sector=consumer2.getHeatSector(), demand_timeseries=consumer2.input_data['heatDemand'], label='Heatdemand_Consumer_2')
my_energysystem.add(heatdemand2.component())
consumer2.addComponent(heatdemand2)

#Demand Hotwater
hotwaterdemand2 = heat_comp.Hotwaterdemand(sector=consumer2.getHotwaterSector(), demand_timeseries=consumer2.input_data['hotwaterDemand'], label='Hotwater_Consumer_2')
my_energysystem.add(hotwaterdemand2.component())
consumer2.addComponent(hotwaterdemand2)


#Hot Water Boiler
hotwaterBoiler2 = heat_comp.HotwaterBoiler(sector_in=consumer2.getHeatSector(), sector_out=consumer2.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_Consumer_2')
my_energysystem.add(hotwaterBoiler2.component())
consumer2.addComponent(hotwaterBoiler2)

#Heat Consumer to LSC
sell_tariff_heat2 = np.array(consumer2.input_data['heatFeedin'])
heat2lsc2 = lsc_comp.Heat2LSC(sector_in=consumer2.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_Consumer_2", rev_consumer=sell_tariff_heat2, c_lsc=sell_tariff_heat2)
my_energysystem.add(heat2lsc2.component())
consumer2.addComponent(heat2lsc2)

#Heat LSC to Consumer
buy_tariff_heat2 = np.add(sell_tariff_heat2, c_grid_heat)
lsc2heat2 = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer2.getHeatSector(),
                                    P=10, label="LSC2heat_Consumer_2", rev_lsc=sell_tariff_heat2, c_consumer=buy_tariff_heat2)
my_energysystem.add(lsc2heat2.component())
consumer2.addComponent(lsc2heat2)


#_____________ Cooling _____________

#Demand
coolingdemand2 = cool_comp.Demand(sector=consumer2.getCoolingSector(), demand_timeseries=consumer2.input_data['coolingDemand'], label='Coolingdemand_Consumer_2')
my_energysystem.add(coolingdemand2.component())
consumer2.addComponent(coolingdemand2)

#Cooling Consumer to LSC
sell_tariff_cool2 = np.array(consumer2.input_data['coolingFeedin'])
cool2lsc2 = lsc_comp.Cool2LSC(sector_in=consumer2.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="CoolingLSC_Consumer_2", rev_consumer=sell_tariff_cool2, c_lsc=sell_tariff_cool2)
my_energysystem.add(cool2lsc2.component())
consumer2.addComponent(cool2lsc2)

#Cooling LSC to Consumer
buy_tariff_cool2 = np.add(sell_tariff_cool2, c_grid_cool)
lsc2cool2 = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer2.getCoolingSector(),
                                    P=10, label="LSC2cool_Consumer_2", rev_lsc=sell_tariff_cool2, c_consumer=buy_tariff_cool2)
my_energysystem.add(lsc2cool2.component())
consumer2.addComponent(lsc2cool2)



#_____________ Water _____________

#Pipeline Purchase Limited
pipelinepurchaseLimited2 = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer2.getPotablewaterSector(), label="Pipelinepurchase_Limited_Consumer_2", color='purple',
                                                      demand=consumer2.input_data['waterDemand'], limit=0.5, costs_water=consumer2.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited2.component())
consumer2.addComponent(pipelinepurchaseLimited2)

#LSC Purchase Limited
lscwaterpurchaseLimited2 = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getPotablewaterSector(), sector_out=consumer2.getPotablewaterSector(), 
                                                           label="LSCpurchase_Limited_Consumer_2", color='brown',
                                                           demand=consumer2.input_data['waterDemand'], limit=0.5, 
                                                           costs_consumer=consumer2.input_data['waterPipelineCosts'], discount=0.75, revenues_lsc=consumer2.input_data['waterPipelineCosts'])
my_energysystem.add(lscwaterpurchaseLimited2.component())
consumer2.addComponent(lscwaterpurchaseLimited2)

#Water Consumption Pool Purchase
poolPurchase2 = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getWaterpoolSector(), sector_out=consumer2.getPotablewaterSector(), 
                                                           label="Waterpoolpurchase_Consumer_2", color='gold',
                                                           demand=consumer2.input_data['waterDemand'], limit=1, 
                                                           costs_consumer=consumer2.input_data['waterPipelineCosts'], discount=0.5, revenues_lsc=consumer2.input_data['waterPipelineCosts'])
my_energysystem.add(poolPurchase2.component())
consumer2.addComponent(poolPurchase2)

#Unlimited pipeline purchase at high costs
pipelineExcessPurchase2 = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer2.getPotablewaterSector(), label="Pipelinepurchase_Excess_Consumer_2", color='magenta',
                                                      demand=consumer2.input_data['waterDemand'], limit=1, costs_water=consumer2.input_data['waterPipelineCosts'], discount=2)
my_energysystem.add(pipelineExcessPurchase2.component())
consumer2.addComponent(pipelineExcessPurchase2)


#Predefined water demand
waterdemandPredefine2 = water_comp.WaterdemandPredefine(timesteps=timesteps, sector=consumer2.getWaterdemandSector(), label="Waterdemand_Predefine_Consumer_2",
                                                       color='yellow', demand=consumer2.input_data['waterDemand'])
my_energysystem.add(waterdemandPredefine2.component())
consumer2.addComponent(waterdemandPredefine2)


#Flexible water demand provision and sewage output
waterdemandFlex2 = water_comp.WaterDemandSewageFlex(timesteps=timesteps, sector_in=consumer2.getPotablewaterSector(), sector_out=consumer2.getWaterdemandSector(), sector_sewage=LSC.getSewageSector(),
                                                   label="Waterdemand_Flexible_Consumer_2", color='beige')
my_energysystem.add(waterdemandFlex2.component())
consumer2.addComponent(waterdemandFlex2)


#Flexibility of water to consumption pool
willingness_for_flexibility2 = consumer2.get_willingness_for_waterflexibility()
#willingness_for_flexibility2 = 0.1136136136136136
waterFlexibility2 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer2.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility_2pool_Consumer_2",
                                                       color='darkcyan', demand=consumer2.input_data['waterDemand'], wff=willingness_for_flexibility2,
                                                       costs_lsc=consumer2.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer2.input_data['waterPipelineCosts'])
my_energysystem.add(waterFlexibility2.component())
consumer2.addComponent(waterFlexibility2)


#_____________ Waste _____________

#Accruing
wasteAccruing2 = waste_comp.Accruing(sector=consumer2.getWasteSector(), waste_timeseries=consumer2.input_data['waste'], label='WasteAccruing_Consumer_2', color="yellow")
my_energysystem.add(wasteAccruing2.component())
consumer2.addComponent(wasteAccruing2)


#Waste Reduction and Recycling Flexibility
wfr2=consumer2.get_willingness_for_recycling()
#wfr2=1
wasteFlexibility2 = lsc_comp.WasteFlexibility(sector=consumer2.getWasteSector(), timesteps=timesteps, revenues=consumer2.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_2", color='forestgreen', waste=consumer2.input_data['waste'], 
                                             wfr=wfr2, share_recycling=1)
my_energysystem.add(wasteFlexibility2.component())
consumer2.addComponent(wasteFlexibility2)

#Waste Storage for Consumer
wasteStorageConsumer2 = waste_comp.WasteStorage(sector_in=consumer2.getWasteSector(), sector_out=consumer2.getWastestorageSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_Consumer_2", color='mistyrose', balanced=True)
my_energysystem.add(wasteStorageConsumer2.component())
consumer2.addComponent(wasteStorageConsumer2)


#Waste Storage for Household
wasteStorageHousehold2 = waste_comp.WasteStorage(sector_in=consumer2.getWastestorageSector(), sector_out=consumer2.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_2", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)
my_energysystem.add(wasteStorageHousehold2.component())
consumer2.addComponent(wasteStorageHousehold2)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck2 = 10
s_wastetruck2 = 20
delay_wastetruck2 = math.ceil(s_wastetruck2/v_wastetruck2)
transmissionWaste2 = lsc_comp.Delay_Transport(sector_in=consumer2.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck2, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_2', color='aquamarine')
my_energysystem.add(transmissionWaste2.component())
consumer2.addComponent(transmissionWaste2)


#---------------------- Consumer 3 Components ----------------------

#_____________ Electricity _____________

#Demand
elecdemand3 = el_comp.Demand(sector=consumer3.getElectricitySector(), 
                             demand_timeseries=consumer3.input_data['electricityDemand'], label='Electricitydemand_Consumer_3')
my_energysystem.add(elecdemand3.component())
consumer3.addComponent(elecdemand3)


#Grid Purchase no combined metering
"""
elgrid3 = el_comp.GridPurchase(sector=consumer3.getElectricitySector(), 
                               costs_energy=np.add(consumer3.input_data['electricityCosts'], c_grid + c_abgabe), 
                               label='Electricitygridpurchase_Consumer_3', timesteps=timesteps, costs_power=0)        
my_energysystem.add(elgrid3.component())
consumer3.addComponent(elgrid3)
"""


#Grid Purchase combined metering
elgrid3 = el_comp.GridCombinedPowerMetering(sector_grid=consumer3.getElectricityGridSector(), sector_electricity=consumer3.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              costs_energy=np.add(consumer3.input_data['electricityCosts'], c_grid + c_abgabe), 
                              label='Electricitygridpurchase_Consumer_3', timesteps=timesteps)        

elgridGrid3 = elgrid3.get_source()
my_energysystem.add(elgridGrid3.component())
consumer3.addComponent(elgridGrid3)

elgridElec3 = elgrid3.get_toElec()
my_energysystem.add(elgridElec3.component())
consumer3.addComponent(elgridElec3)


#Grid Feedin - does not make sense without PV system
#elgridFeedin3 = el_comp.GridFeedin(sector=consumer3.getElectricitySector(), revenues=consumer3.input_data['electricityFeedin'], 
                                   #label='Electricityfeedin_Consumer_3', timesteps=timesteps)        
#my_energysystem.add(elgridFeedin3.component())
#consumer3.addComponent(elgridFeedin3)

#Electricity Consumer to LSC
sell_tariff3 = np.multiply(np.add(consumer3.input_data['electricityCosts'], consumer3.input_data['electricityFeedin']), 1/2)
elec2lsc3 = lsc_comp.Electricity2LSC(sector_in=consumer3.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_Consumer_3", rev_consumer=sell_tariff3, c_lsc=sell_tariff3)
my_energysystem.add(elec2lsc3.component())
consumer3.addComponent(elec2lsc3)

#Electricity LSC to Consumer
c_grid_reduced3 = np.multiply(c_grid, red_local)
buy_tariff3 = np.add(sell_tariff3, np.add(c_grid_reduced3, c_abgabe))
lsc2elec3 = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer3.getElectricitySector(),
                                    P=11, label="LSC2elec_Consumer_3", rev_lsc=sell_tariff3, c_consumer=buy_tariff3)
my_energysystem.add(lsc2elec3.component())
consumer3.addComponent(lsc2elec3)


#_____________ Heat _____________

#Demand Heat
heatdemand3 = heat_comp.Demand(sector=consumer3.getHeatSector(), demand_timeseries=consumer3.input_data['heatDemand'], label='Heatdemand_Consumer_3')
my_energysystem.add(heatdemand3.component())
consumer3.addComponent(heatdemand3)

#Demand Hotwater
hotwaterdemand3 = heat_comp.Hotwaterdemand(sector=consumer3.getHotwaterSector(), demand_timeseries=consumer3.input_data['hotwaterDemand'], label='Hotwater_Consumer_3')
my_energysystem.add(hotwaterdemand3.component())
consumer3.addComponent(hotwaterdemand3)


#Hot Water Boiler
hotwaterBoiler3 = heat_comp.HotwaterBoiler(sector_in=consumer3.getHeatSector(), sector_out=consumer3.getHotwaterSector(), P_in=21, P_out=21, label='Boiler_Consumer_3')
my_energysystem.add(hotwaterBoiler3.component())
consumer3.addComponent(hotwaterBoiler3)

#Heat Consumer to LSC
sell_tariff_heat3 = np.array(consumer3.input_data['heatFeedin'])
heat2lsc3 = lsc_comp.Heat2LSC(sector_in=consumer3.getHeatSector(), sector_out=LSC.getHeatSector(),
                                    P=10, label="Heat2LSC_Consumer_3", rev_consumer=sell_tariff_heat3, c_lsc=sell_tariff_heat3)
my_energysystem.add(heat2lsc3.component())
consumer3.addComponent(heat2lsc3)

#Heat LSC to Consumer
buy_tariff_heat3 = np.add(sell_tariff_heat3, c_grid_heat)
lsc2heat3 = lsc_comp.LSC2heat(sector_in=LSC.getHeatSector(), sector_out=consumer3.getHeatSector(),
                                    P=10, label="LSC2heat_Consumer_3", rev_lsc=sell_tariff_heat3, c_consumer=buy_tariff_heat3)
my_energysystem.add(lsc2heat3.component())
consumer3.addComponent(lsc2heat3)


#_____________ Cooling _____________

#Demand
coolingdemand3 = cool_comp.Demand(sector=consumer3.getCoolingSector(), demand_timeseries=consumer3.input_data['coolingDemand'], label='Coolingdemand_Consumer_3')
my_energysystem.add(coolingdemand3.component())
consumer3.addComponent(coolingdemand3)

#Cooling Consumer to LSC
sell_tariff_cool3 = np.array(consumer3.input_data['coolingFeedin'])
cool2lsc3 = lsc_comp.Cool2LSC(sector_in=consumer3.getCoolingSector(), sector_out=LSC.getCoolingSector(),
                                    P=10, label="CoolingLSC_Consumer_3", rev_consumer=sell_tariff_cool3, c_lsc=sell_tariff_cool3)
my_energysystem.add(cool2lsc3.component())
consumer3.addComponent(cool2lsc3)

#Cooling LSC to Consumer
buy_tariff_cool3 = np.add(sell_tariff_cool3, c_grid_cool)
lsc2cool3 = lsc_comp.LSC2cool(sector_in=LSC.getCoolingSector(), sector_out=consumer3.getCoolingSector(),
                                    P=10, label="LSC2cool_Consumer_3", rev_lsc=sell_tariff_cool3, c_consumer=buy_tariff_cool3)
my_energysystem.add(lsc2cool3.component())
consumer3.addComponent(lsc2cool3)


#_____________ Water _____________

#Pipeline Purchase Limited
pipelinepurchaseLimited3 = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer3.getPotablewaterSector(), label="Pipelinepurchase_Limited_Consumer_3", color='purple',
                                                      demand=consumer3.input_data['waterDemand'], limit=0.5, costs_water=consumer3.input_data['waterPipelineCosts'], discount=1)
my_energysystem.add(pipelinepurchaseLimited3.component())
consumer3.addComponent(pipelinepurchaseLimited3)

#LSC Purchase Limited
lscwaterpurchaseLimited3 = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getPotablewaterSector(), sector_out=consumer3.getPotablewaterSector(), 
                                                           label="LSCpurchase_Limited_Consumer_3", color='brown',
                                                           demand=consumer3.input_data['waterDemand'], limit=0.5, 
                                                           costs_consumer=consumer3.input_data['waterPipelineCosts'], discount=0.75, revenues_lsc=consumer3.input_data['waterPipelineCosts'])
my_energysystem.add(lscwaterpurchaseLimited3.component())
consumer3.addComponent(lscwaterpurchaseLimited3)

#Water Consumption Pool Purchase
poolPurchase3 = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=LSC.getWaterpoolSector(), sector_out=consumer3.getPotablewaterSector(), 
                                                           label="Waterpoolpurchase_Consumer_3", color='gold',
                                                           demand=consumer3.input_data['waterDemand'], limit=1, 
                                                           costs_consumer=consumer3.input_data['waterPipelineCosts'], discount=0.5, revenues_lsc=consumer3.input_data['waterPipelineCosts'])
my_energysystem.add(poolPurchase3.component())
consumer3.addComponent(poolPurchase3)

#Unlimited pipeline purchase at high costs
pipelineExcessPurchase3 = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=consumer3.getPotablewaterSector(), label="Pipelinepurchase_Excess_Consumer_3", color='magenta',
                                                      demand=consumer3.input_data['waterDemand'], limit=1, costs_water=consumer3.input_data['waterPipelineCosts'], discount=2)
my_energysystem.add(pipelineExcessPurchase3.component())
consumer3.addComponent(pipelineExcessPurchase3)


#Predefined water demand
waterdemandPredefine3 = water_comp.WaterdemandPredefine(timesteps=timesteps, sector=consumer3.getWaterdemandSector(), label="Waterdemand_Predefine_Consumer_3",
                                                       color='yellow', demand=consumer3.input_data['waterDemand'])
my_energysystem.add(waterdemandPredefine3.component())
consumer3.addComponent(waterdemandPredefine3)


#Flexible water demand provision and sewage output
waterdemandFlex3 = water_comp.WaterDemandSewageFlex(timesteps=timesteps, sector_in=consumer3.getPotablewaterSector(), sector_out=consumer3.getWaterdemandSector(), sector_sewage=LSC.getSewageSector(),
                                                   label="Waterdemand_Flexible_Consumer_3", color='beige')
my_energysystem.add(waterdemandFlex3.component())
consumer3.addComponent(waterdemandFlex3)


#Flexibility of water to consumption pool
willingness_for_flexibility3 = consumer3.get_willingness_for_waterflexibility()
#willingness_for_flexibility3 = 0.0035035035035035035
waterFlexibility3 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer3.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility_2pool_Consumer_3",
                                                       color='darkcyan', demand=consumer3.input_data['waterDemand'], wff=willingness_for_flexibility3,
                                                       costs_lsc=consumer3.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer3.input_data['waterPipelineCosts'])
my_energysystem.add(waterFlexibility3.component())
consumer3.addComponent(waterFlexibility3)


#_____________ Waste _____________

#Accruing
wasteAccruing3 = waste_comp.Accruing(sector=consumer3.getWasteSector(), waste_timeseries=consumer3.input_data['waste'], label='WasteAccruing_Consumer_3', color="yellow")
my_energysystem.add(wasteAccruing3.component())
consumer3.addComponent(wasteAccruing3)


#Waste Reduction and Recycling Flexibility
#Willingness for Recycling definition
wfr3=consumer3.get_willingness_for_recycling()
#wfr3=0
wasteFlexibility3 = lsc_comp.WasteFlexibility(sector=consumer3.getWasteSector(), timesteps=timesteps, revenues=consumer3.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_3", color='forestgreen', waste=consumer3.input_data['waste'], 
                                             wfr=wfr3, share_recycling=1)
my_energysystem.add(wasteFlexibility3.component())
consumer3.addComponent(wasteFlexibility3)

#Waste Storage for Consumer
wasteStorageConsumer3 = waste_comp.WasteStorage(sector_in=consumer3.getWasteSector(), sector_out=consumer3.getWastestorageSector(), 
                                       volume_max=0.1, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_Consumer_3", color='mistyrose', balanced=True)
my_energysystem.add(wasteStorageConsumer3.component())
consumer3.addComponent(wasteStorageConsumer3)


#Waste Storage for Household
#Disposal Periods - 1 to other consumers because of longer distance
wasteStorageHousehold3 = waste_comp.WasteStorage(sector_in=consumer3.getWastestorageSector(), sector_out=consumer3.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_3", color='mediumvioletred', balanced=False, empty_end=True, t_start=167)
my_energysystem.add(wasteStorageHousehold3.component())
consumer3.addComponent(wasteStorageHousehold3)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 30km, Average Speed 10km/h (Time to load waste) --> 3h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck3 = 10
s_wastetruck3 = 30
delay_wastetruck3 = math.ceil(s_wastetruck3/v_wastetruck3)
transmissionWaste3 = lsc_comp.Delay_Transport(sector_in=consumer3.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck3, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_3', color='aquamarine')
my_energysystem.add(transmissionWaste3.component())
consumer3.addComponent(transmissionWaste3)



#---------------------- LSC Components ----------------------

#_____________ Electricity _____________

#Grid Purchase no combined metering
"""
elgridLSC = el_comp.GridPurchase(sector=LSC.getElectricitySector(), 
                                 costs_energy=np.add(LSC.input_data['electricityCosts'], c_grid + c_abgabe), 
                                 label='Electricitygridpurchase_LSC', timesteps=timesteps, P=30, costs_power=0)        
my_energysystem.add(elgridLSC.component())
LSC.addComponent(elgridLSC)
"""

#Grid Purchase combined metering
elgridLSC = el_comp.GridCombinedPowerMetering(sector_grid=LSC.getElectricityGridSector(), sector_electricity=LSC.getElectricitySector(), sector_power=LSC.getPeakpowerSector(), 
                              costs_energy=np.add(LSC.input_data['electricityCosts'], c_grid + c_abgabe), 
                              label='Electricitygridpurchase_LSC', timesteps=timesteps)        

elgridGridLSC = elgridLSC.get_source()
my_energysystem.add(elgridGridLSC.component())
LSC.addComponent(elgridGridLSC)

elgridElecLSC = elgridLSC.get_toElec()
my_energysystem.add(elgridElecLSC.component())
LSC.addComponent(elgridElecLSC)


#Peakpower Costs
peakpowerCombined = el_comp.PeakpowerCosts(sector=LSC.getPeakpowerSector(),  
                                 label='Electricitypeakpower_Combined', timesteps=timesteps, P=33, costs_power=35)        
my_energysystem.add(peakpowerCombined.component())
LSC.addComponent(peakpowerCombined)

#Photovoltaic
pvLSC = el_comp.Photovoltaic(sector=LSC.getElectricitySector(), generation_timeseries=LSC.input_data['elGen'], 
                          label='PV_LSC', timesteps=timesteps, area_efficiency=0.5, area_roof=400, area_factor=10)
my_energysystem.add(pvLSC.component())
LSC.addComponent(pvLSC)


#Grid Feedin
elgridFeedinLSC = el_comp.GridFeedin(sector=LSC.getElectricitySector(), revenues=LSC.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_LSC', timesteps=timesteps)        
my_energysystem.add(elgridFeedinLSC.component())
LSC.addComponent(elgridFeedinLSC)



#Battery
batteryLSC = el_comp.Battery(sector_in=LSC.getElectricitySector(), label='Battery_LSC',
                          soc_max=25, SOC_start=25, P_in=25, P_out=25)
my_energysystem.add(batteryLSC.component())
LSC.addComponent(batteryLSC)


#Heatpump
heatpumpLSC = el_comp.Heatpump(sector_in = LSC.getElectricitySector(), sector_out=LSC.getHeatSector(), 
                            conversion_timeseries=LSC.input_data['copHP'], timesteps=timesteps, P_in=30, P_out=30, label='Heatpump_LSC') 
my_energysystem.add(heatpumpLSC.component())
LSC.addComponent(heatpumpLSC)


#Electric Cooling
elcoolLSC = el_comp.ElectricCooling(sector_in=LSC.getElectricitySector(), sector_out=LSC.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=LSC.input_data['copCool'], P_in=30, P_out=30, label="Cooler_LSC")
my_energysystem.add(elcoolLSC.component())
LSC.addComponent(elcoolLSC)





#_____________ Heat _____________

#Heat Storage
thermalStorageLSC = heat_comp.ThermalStorage(sector_in=LSC.getHeatSector(), P_in=30, volume_max=1.5, label='Heatstorage_LSC')
my_energysystem.add(thermalStorageLSC.component())
LSC.addComponent(thermalStorageLSC)


#_____________ Cooling _____________

#No cooling components for LSC


#_____________ Water _____________


#Waterpool Storage
waterPoolStorage = water_comp.WaterStorage(sector_in=LSC.getWaterpoolSector(), eta_in=1, eta_out=1, eta_sb=0.99999999,
                                           c_in=0, c_out=0, volume_start=0, volume_max=10000, Q_max=1000,
                                           label="Waterpoolstorage_LSC", color='olivedrab', balanced=True)
my_energysystem.add(waterPoolStorage.component())
LSC.addComponent(waterPoolStorage)


#Wasted Pool Water
poolwaterWasted = lsc_comp.WaterWasted(sector=LSC.getWaterpoolSector(), label='Poolwater_wasted_LSC',
                                       color='darkkhaki', costs_water=LSC.input_data['waterPipelineCosts'], discount=0.5)
my_energysystem.add(poolwaterWasted.component())
LSC.addComponent(poolwaterWasted)


#LSC Water Option1 : Wasted Water
lscwaterWasted = lsc_comp.WaterWasted(timesteps=timesteps, sector=LSC.getPotablewaterSector(), label='LSCwater_wasted_LSC',
                                       color='darkchocolate')
my_energysystem.add(lscwaterWasted.component())
LSC.addComponent(lscwaterWasted)


#Option2: LSC water Storage
#waterLSCStorage = water_comp.WaterStorage(sector_in=LSC.getWPotablewaterSector(), eta_in=1, eta_out=1, eta_sb=1,
#                                           c_in=0, c_out=0, volume_start=0, volume_max=10000, Q_max=1000,
#                                           label="LSCwaterstorage_LSC", color='darkchocolate', balanced=True)
#my_energysystem.add(waterLSCStorage.component())
#LSC.addComponent(waterLSCStorage)



#_____________ Sewage and Sludge _____________


#Sewage Treatment Plant
#Summed costs per m³ sewage
costs_sewagetreat_operation = 0.04
costs_sewage_surcharge_factor=1.1
#Factor for water recovered that can be directly assigned to the LSC --> if the factor is set too high, other operations like LSC purchase might be hindered
eta_waterrecovery=0.1
#Sector Waterreovery as the recovered water must be transported and can only be used with time delay and not immediately
sewageTreatmentPlant = water_comp.SewageTreatment_noElec(sector_in=LSC.getSewageSector(), 
                                                  sector_out=[LSC.getWaterrecoverysector(), LSC.getSludgeSector(), LSC.getHeatSector()], 
                                                  tempdif=0, label='SewageTreatment_LSC', color='black', c_in=costs_sewage_surcharge_factor*costs_sewagetreat_operation, eta=eta_waterrecovery)
my_energysystem.add(sewageTreatmentPlant.component())
LSC.addComponent(sewageTreatmentPlant)


#Water recovery usage delay (implemented as transmission)
delay_waterrecovery=24
waterrecoveryDelay = lsc_comp.Delay_Transport(sector_in=LSC.getWaterrecoverysector(), sector_out=LSC.getPotablewaterSector(), 
                                        efficiency=1, costs_transmission=0, delay=delay_waterrecovery, timesteps=timesteps, capacity_transmission=1000,
                                        label='Waterrecoverydelay_LSC', color='red')
my_energysystem.add(waterrecoveryDelay.component())
LSC.addComponent(waterrecoveryDelay)


#Sludge Storage at Sewage Treatment Plant - disposal once a day
sludgeStoragePlant = water_comp.SludgeStorage(sector_in=LSC.getSludgeSector(), sector_out=LSC.getSludgestorageSector(), timesteps=timesteps,
                                         c_in=0.01, c_out=0.01, volume_start=0, volume_max=1.5, Q_max=1.5, disposal_periods=24, balanced=True,
                                         label="Sludgestorage_Sewageplant_LSC", color='darkkhaki')
my_energysystem.add(sludgeStoragePlant.component())
LSC.addComponent(sludgeStoragePlant)


#Sludge Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,5€/m³ 
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 20m³ 
#Distance 20km, Average Speed 60km/h --> 20min --> round up --> 1 timestep
v_sludgetruck = 60
s_sludgetruck = 20
delay_sludgetruck = math.ceil(s_sludgetruck/v_sludgetruck)
transmission = lsc_comp.Delay_Transport(sector_in=LSC.getSludgestorageSector(), sector_out=LSC.getSludgetreatmentSectorIn(), 
                                        efficiency=1, costs_transmission=0.7, delay=delay_sludgetruck, timesteps=timesteps, capacity_transmission=20,
                                        label='Sludgetransport_LSC', color='aquamarine')
my_energysystem.add(transmission.component())
LSC.addComponent(transmission)


#Sludge Storage at Treatment site - no disposal periods
sludgeStorageTreatment= water_comp.SludgeStorage(sector_in=LSC.getSludgetreatmentSectorIn(), sector_out=LSC.getSludgetreatmentSectorOut(), timesteps=timesteps,
                                         c_in=0.01, c_out=0.01, volume_start=0.1, volume_max=1.5, Q_max=1.5, disposal_periods=0, balanced=True,
                                         label="Sludgestorage_Treatment_LSC", color='darkgoldenrod')
my_energysystem.add(sludgeStorageTreatment.component())
LSC.addComponent(sludgeStorageTreatment)


#Sludge Incineration Plant
#Potential of the sludge recovery that can be used by the LSC
sludge_recovery_used=0.5
sludgecombustion = water_comp.Sludgecombustion(sector_in=LSC.getSludgetreatmentSectorOut(), sector_out=[LSC.getElectricitySector(), LSC.getHeatSector()],
                                               conversion_factor=[0.35, 0.4], c_in=0, c_out=[0.06, 0.06], P_in=100, P_out=100, usable_energy=sludge_recovery_used,
                                               label='Sludgecombustion_LSC', color='lightsteelblue')
my_energysystem.add(sludgecombustion.component())
LSC.addComponent(sludgecombustion)



#Sludge Disposal
sludgeDisposal = water_comp.Sludgedisposal(sector=LSC.getSludgetreatmentSectorOut(), costs=LSC.input_data['sludgeCosts'],
                                           label='Sludgedisposal_LSC', color='steelblue')        
my_energysystem.add(sludgeDisposal.component())
LSC.addComponent(sludgeDisposal)


#_____________ Waste _____________
#Waste Storage for Waste Treatment
#Option1: Storage Volume so high that it should not be a problem
#Optoin2: Waste is emptied for each consumer once a week --> assumption that it can be stored for one month --> 4*1m³ --> 4m³ per consumer --> Asusmption 12m³
wasteStorageTreatment = waste_comp.WasteStorage(sector_in=LSC.getWastestorageSector(), sector_out=LSC.getWastedisposalSector(), 
                                       volume_max=12, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_Disposal_LSC", color='mistyrose', balanced=True)
my_energysystem.add(wasteStorageTreatment.component())
LSC.addComponent(wasteStorageTreatment)


#Waste Combustion
#Usable energy recovery for waste
waste_recovery_used=0.5
wasteCombustion = waste_comp.Wastecombustion(sector_in=LSC.getWastedisposalSector(), sector_out=[LSC.getElectricitySector(), LSC.getHeatSector()],
                                             c_in=0, c_out=[0.04, 0.04], conversion_factor=[0.35, 0.4], P_in=100, P_out=100, usable_energy=waste_recovery_used,
                                             label='Wastecombustion_LSC', color='lightsteelblue')
my_energysystem.add(wasteCombustion.component())
LSC.addComponent(wasteCombustion)

#Waste Disposal
#Emissions bei Disposal --> nur einrechnen wenn minimaler Kostenbetrieb geplant ist, und die Kosten nicht an einen "unbedeutenden" Drittanbieter verrrechnet werden
wasteDisposal = waste_comp.Wastedisposal(sector=LSC.getWastedisposalSector(), costs=LSC.input_data['wasteCosts'],
                                         label='Wastedisposal_LSC', color='steelblue', min_disposal=0)
my_energysystem.add(wasteDisposal.component())
LSC.addComponent(wasteDisposal)



#___________ Transport ____________________

#Mobility Demand for all consumers --> assigned to the LSC Transport sector for carsharing
mobilitydemand1 = trans_comp.Demand(sector=LSC.getTransportSector(), demand_timeseries=consumer1.input_data['transportDemand'], label='Transport1_Consumer_1', label_sankey='Transport1_Consumer_1')
my_energysystem.add(mobilitydemand1.component())
consumer1.addComponent(mobilitydemand1)

mobilitydemand2 = trans_comp.Demand(sector=LSC.getTransportSector(), demand_timeseries=consumer2.input_data['transportDemand'], label='Transport2_Consumer_1', label_sankey='Transport2_Consumer_1')
my_energysystem.add(mobilitydemand2.component())
consumer2.addComponent(mobilitydemand2)

mobilitydemand3 = trans_comp.Demand(sector=LSC.getTransportSector(), demand_timeseries=consumer3.input_data['transportDemand'], label='Transport3_Consumer_1', label_sankey='Transport3_Consumer_1')
my_energysystem.add(mobilitydemand3.component())
consumer3.addComponent(mobilitydemand3)



#Assumed that three different cars exist to cover the transport demand: Kia Soul, Nissan Leaf and Renault Zoe
#Electric Vehicle
#Kia Soul
kia=trans_comp.ElectricVehicle(sector_in=LSC.getElectricitySector(), sector_out=LSC.getElectricVehicleSector(), 
                                           sector_drive=LSC.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=2.3, P_drive=150, soc_max=64, soc_start=64,
                                           label='KiaSoul_', labelDrive='KiaSoulDrive_', label_sankey='KiaSoul_', color='red')
my_energysystem.add(kia.component())
my_energysystem.add(kia.drive())
LSC.addComponent(kia)


nissan=trans_comp.ElectricVehicle(sector_in=LSC.getElectricitySector(), sector_out=LSC.getElectricVehicleSector(), 
                                           sector_drive=LSC.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=3.7, P_drive=110, soc_max=40, soc_start=40,
                                           label='NissanLeaf_', labelDrive='NissanLeafDrive_', label_sankey='NissanLeaf_', color='magenta')
my_energysystem.add(nissan.component())
my_energysystem.add(nissan.drive())
LSC.addComponent(nissan)


zoe=trans_comp.ElectricVehicle(sector_in=LSC.getElectricitySector(), sector_out=LSC.getElectricVehicleSector(), 
                                           sector_drive=LSC.getTransportSector(), timesteps=timesteps, consumption=0.2, P_charge=11, P_drive=70, soc_max=40, soc_start=40,
                                           label='RenaultZoe_', labelDrive='RenaultZoeDrive_', label_sankey='RenaultZoe_', color='blue')
my_energysystem.add(zoe.component())
my_energysystem.add(zoe.drive())
LSC.addComponent(zoe)


#___________ Household Components ____________________

#Washing Maschine
washingMaschine = household_comp.WashingMaschine(sector_in=LSC.getElectricitySector(), sector_out=LSC.getHeatSector(), 
                                                 sector_inAdditional=LSC.getHotwaterSector(), sector_loss=LSC.getWaterwashingSector(), 
                                                 Twash=LSC.input_data['washTemperature'], timesteps=timesteps,
                                                 max_elec=1, max_therm=1)

washingMaschineElec = washingMaschine.get_elec()
my_energysystem.add(washingMaschineElec.component())
LSC.addComponent(washingMaschineElec)


washingMaschineRecovery = washingMaschine.get_heatrecovery()
my_energysystem.add(washingMaschineRecovery.component())
LSC.addComponent(washingMaschineRecovery)



washingMaschineWater = washingMaschine.get_waterinput()
my_energysystem.add(washingMaschineWater.component())
consumer1.addComponent(washingMaschineWater)



washingElecVar = washingMaschine.get_elecHeat()
my_energysystem.add(washingElecVar.component())
LSC.addComponent(washingElecVar)


washingThermVar = washingMaschine.get_thermHeat()
my_energysystem.add(washingThermVar.component())
LSC.addComponent(washingThermVar)


#Water Pipeline Purchase for washing maschine
waterpipelinePurchaseWashing = water_comp.PipelinePurchase(sector=LSC.getWaterwashingSector(), costs_water=LSC.input_data['waterPipelineCosts'], timesteps=timesteps,
                                                           label='Waterwashing_Pipeline_LSC', color='lemonchiffon')        
my_energysystem.add(waterpipelinePurchaseWashing.component())
LSC.addComponent(waterpipelinePurchaseWashing)


#Recovered water for washing machine
waterrecoveryWash = lsc_comp.WaterrecoveryTransformer(sector_in=LSC.getWaterrecoverysector(), sector_out=LSC.getWaterwashingSector(), 
                                                      label='Waterrecovery_Washing_LSC', color='mediumturquoise')
my_energysystem.add(waterrecoveryWash.component())
LSC.addComponent(waterrecoveryWash)



#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 




#---------------------- Additional constraints ----------------------

#Prevent Charging and Discharging at same time
saka = constraints.waterstorage_chargeblock(model=saka, storage=waterPoolStorage)
#saka = constraints.waterstorage_chargeblock(model=saka, storage=waterPoolStorage)

saka = constraints.transport_decision_EVs_flex(vehicles=[kia, nissan, zoe], componentName='TransportBinaryEV', timesteps=timesteps, model=saka,
                                                      demands=[consumer1.input_data['transportDemand'], consumer2.input_data['transportDemand'], consumer3.input_data['transportDemand']])

"""
#Activation of Excess Pipeline purchase only if casual purchase is exceeded
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=pipelinepurchaseLimited, pipeline_excess=pipelineExcessPurchase)
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=pipelinepurchaseLimited2, pipeline_excess=pipelineExcessPurchase2)
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=pipelinepurchaseLimited3, pipeline_excess=pipelineExcessPurchase3)
"""

#--------------------------------------------------------------------


if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    saka.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, saka, scenario)


results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")

