# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 10:51:17 2022

@author: Matthias Maldet
"""


import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc

import investment_components as investment_comp

import consumer_lsm as consumer_lsm
import consumer_lsm_cluster_extended as consumer_lsm_cluster
import consumer_lsm_operational_extended as consumer_lsm_operational


import constraints_LSM as constraints_LSM

import clustering as clustering

import LSC_inputparameter12 as LSC_inputparameter
import LSM_inputparameter12 as LSM_inputparameter



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'LSM12_wasteAD'

#Message for scenario
scenariomessage = "Waste AD"

#Filename
filename = "input_data_LSM.xlsx"


#Timestep for Optimisation
deltaT = 1

#Timesteps Considered --> default clustering 360, default operational 8784
timesteps=360
timesteps_operational = 8784
timestepsTotal = 8784
clusterPerMonth = 30

data=pd.ExcelFile(filename)

#unit capacity for one greywater unit
unit_capacity=0.0125

#Clustered data
list_consumers = ["LSC_1", "LSM_1", "LSC_2", "LSM_2", "LSC_3", "LSM_3", "LSC_4", "LSM_4", "LSC_5", "LSM_5"]
#Annual clustering
#data_clustered=clustering.cluster_data(timesteps=timesteps, timestepsTotal=timestepsTotal, data=data, list_consumers=list_consumers, sorting=True) 

#Monthly clustering  
data_clustered=clustering.cluster_data_month(timestepsTotal=timestepsTotal, data=data, list_consumers=list_consumers, sorting=True, clusterPerMonth=clusterPerMonth, max_begin=False)


#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1

yearWeightFactor=timesteps/timestepsTotal


#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2


#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'


 

   
#__________________________________________ STEP 1 INVESTMENT OPTIMIZATION WITH CLUSTERING______________________________________________________________________________________________________________
print("____________________________________________________________________________________")
print("---------------- STEP 1 INVESTMENT OPTIMIZATION WITH CLUSTERING --------------------")
print("____________________________________________________________________________________")
print()

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)

#Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)


 


#---------------------- DEFINITION OF CONSUMERS ----------------------
#LSC1 / LSM1
lsc1 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_1')
lsc1.addElectricitySector()
lsc1.addHeatSector()
lsc1.addDistrictheatingSector()
lsc1.addWaterSector()
lsc1.addWasteSector()
lsc1.addEmissionSector()
lsc1.addWasteBioSector()
lsc1.addGasSector()
lsc1.sector2system(my_energysystem)
#Clustering data
lsc1.clustering(data=data_clustered[lsc1.label])

lsm1 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_1')
lsm1.addElectricitySector()
lsm1.addHeatSector()
lsm1.addWaterSector()
lsm1.addWasteSector()
lsm1.addEmissionSector()
lsm1.addWasteBioSector()
lsm1.addGasSector()
lsm1.sector2system(my_energysystem)
#Clustering data
lsm1.clustering(data=data_clustered[lsm1.label])

#LSC2 / LSM2
lsc2 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_2')
lsc2.addElectricitySector()
lsc2.addHeatSector()
lsc2.addDistrictheatingSector()
lsc2.addWaterSector()
lsc2.addWasteSector()
lsc2.addEmissionSector()
lsc2.addWasteBioSector()
lsc2.addGasSector()
lsc2.sector2system(my_energysystem)
#Clustering data
lsc2.clustering(data=data_clustered[lsc2.label])

lsm2 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_2')
lsm2.addElectricitySector()
lsm2.addHeatSector()
lsm2.addWaterSector()
lsm2.addWasteSector()
lsm2.addEmissionSector()
lsm2.addWasteBioSector()
lsm2.addGasSector()
lsm2.sector2system(my_energysystem)
#Clustering data
lsm2.clustering(data=data_clustered[lsm2.label])

#LSC3 / LSM3
lsc3 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_3')
lsc3.addElectricitySector()
lsc3.addHeatSector()
lsc3.addDistrictheatingSector()
lsc3.addWaterSector()
lsc3.addWasteSector()
lsc3.addEmissionSector()
lsc3.addWasteBioSector()
lsc3.addGasSector()
lsc3.sector2system(my_energysystem)
#Clustering data
lsc3.clustering(data=data_clustered[lsc3.label])

lsm3 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_3')
lsm3.addElectricitySector()
lsm3.addHeatSector()
lsm3.addWaterSector()
lsm3.addWasteSector()
lsm3.addEmissionSector()
lsm3.addWasteBioSector()
lsm3.addGasSector()
lsm3.sector2system(my_energysystem)
#Clustering data
lsm3.clustering(data=data_clustered[lsm3.label])

#LSC4 / LSM4
lsc4 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_4')
lsc4.addElectricitySector()
lsc4.addHeatSector()
lsc4.addDistrictheatingSector()
lsc4.addWaterSector()
lsc4.addWasteSector()
lsc4.addEmissionSector()
lsc4.addWasteBioSector()
lsc4.addGasSector()
lsc4.sector2system(my_energysystem)
#Clustering data
lsc4.clustering(data=data_clustered[lsc4.label])

lsm4 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_4')
lsm4.addElectricitySector()
lsm4.addHeatSector()
lsm4.addWaterSector()
lsm4.addWasteSector()
lsm4.addEmissionSector()
lsm4.addWasteBioSector()
lsm4.addGasSector()
lsm4.sector2system(my_energysystem)
#Clustering data
lsm4.clustering(data=data_clustered[lsm4.label])

#LSC5 / LSM5
lsc5 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_5')
lsc5.addElectricitySector()
lsc5.addHeatSector()
lsc5.addDistrictheatingSector()
lsc5.addWaterSector()
lsc5.addWasteSector()
lsc5.addEmissionSector()
lsc5.addWasteBioSector()
lsc5.addGasSector()
lsc5.sector2system(my_energysystem)
#Clustering data
lsc5.clustering(data=data_clustered[lsc5.label])

lsm5 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_5')
lsm5.addElectricitySector()
lsm5.addHeatSector()
lsm5.addWaterSector()
lsm5.addWasteSector()
lsm5.addEmissionSector()
lsm5.addWasteBioSector()
lsm5.addGasSector()
lsm5.sector2system(my_energysystem)
#Clustering data
lsm5.clustering(data=data_clustered[lsm5.label])





print("----------------Consumer Data loaded----------------")


#Sets for LSC and LSM data
lscData = LSC_inputparameter.LSC_data()
lsmData = LSM_inputparameter.LSM_data()



#---------------------- LSC 1 Components ----------------------

#Assign components to consumer and energy system

#----------------------------- LSC 1 / LSM 1 -----------------------------------------------
#LSC 1
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc1.label
lsccomponents1 = consumer_lsm_cluster.lsc_setup(LSC=lsc1, LSM=lsm1, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents1:
    my_energysystem.add(component.component())
    lsc1.addComponent(component)
    
#LSM Hub 1   
IDlsm = lsm1.label
lsmcomponents1 = consumer_lsm_cluster.lsm_setup(LSM=lsm1, LSM_list=LSM_list, LSC=lsc1, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsmData[IDlsm]["wasteAD_P_in"], wasteAD_P_out= lsmData[IDlsm]["wasteAD_P_out"], 
                                        chp_P_in= lsmData[IDlsm]["chp_P_in"], chp_P_out= lsmData[IDlsm]["chp_P_out"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )

for component in lsmcomponents1:
    my_energysystem.add(component.component())
    lsm1.addComponent(component)
    

#----------------------------- LSC 2 / LSM 2 -----------------------------------------------
#LSC 2
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc2.label
lsccomponents2 = consumer_lsm_cluster.lsc_setup(LSC=lsc2, LSM=lsm2, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents2:
    my_energysystem.add(component.component())
    lsc2.addComponent(component)
    
    
#LSM Hub 2    
IDlsm = lsm2.label
lsmcomponents2 = consumer_lsm_cluster.lsm_setup(LSM=lsm2, LSM_list=LSM_list, LSC=lsc2, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsmData[IDlsm]["wasteAD_P_in"], wasteAD_P_out= lsmData[IDlsm]["wasteAD_P_out"], 
                                        chp_P_in= lsmData[IDlsm]["chp_P_in"], chp_P_out= lsmData[IDlsm]["chp_P_out"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )



for component in lsmcomponents2:
    my_energysystem.add(component.component())
    lsm2.addComponent(component)
    

#----------------------------- LSC 3 / LSM 3 -----------------------------------------------
#LSC 3
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc3.label
lsccomponents3 = consumer_lsm_cluster.lsc_setup(LSC=lsc3, LSM=lsm3, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents3:
    my_energysystem.add(component.component())
    lsc3.addComponent(component)
    
    
#LSM Hub 3    
IDlsm = lsm3.label
lsmcomponents3 = consumer_lsm_cluster.lsm_setup(LSM=lsm3, LSM_list=LSM_list, LSC=lsc3, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsmData[IDlsm]["wasteAD_P_in"], wasteAD_P_out= lsmData[IDlsm]["wasteAD_P_out"], 
                                        chp_P_in= lsmData[IDlsm]["chp_P_in"], chp_P_out= lsmData[IDlsm]["chp_P_out"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )



for component in lsmcomponents3:
    my_energysystem.add(component.component())
    lsm3.addComponent(component)

  
    
#----------------------------- LSC 4 / LSM 4 -----------------------------------------------
#LSC 4
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc4.label
lsccomponents4 = consumer_lsm_cluster.lsc_setup(LSC=lsc4, LSM=lsm4, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents4:
    my_energysystem.add(component.component())
    lsc4.addComponent(component)
    
    
#LSM Hub 4    
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
lsmcomponents4 = consumer_lsm_cluster.lsm_setup(LSM=lsm4, LSM_list=LSM_list, LSC=lsc4, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsmData[IDlsm]["wasteAD_P_in"], wasteAD_P_out= lsmData[IDlsm]["wasteAD_P_out"], 
                                        chp_P_in= lsmData[IDlsm]["chp_P_in"], chp_P_out= lsmData[IDlsm]["chp_P_out"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )



for component in lsmcomponents4:
    my_energysystem.add(component.component())
    lsm4.addComponent(component)



#----------------------------- LSC 5 / LSM 5 -----------------------------------------------
#LSC 5
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc5.label
lsccomponents5 = consumer_lsm_cluster.lsc_setup(LSC=lsc5, LSM=lsm5, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents5:
    my_energysystem.add(component.component())
    lsc5.addComponent(component)
    
    
#LSM Hub 5    
IDlsm = lsm5.label
lsmcomponents5 = consumer_lsm_cluster.lsm_setup(LSM=lsm5, LSM_list=LSM_list, LSC=lsc5, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        area_efficiency=lsmData[IDlsm]["area_efficiency"], area_roof=lsmData[IDlsm]["area_roof"], area_factor=lsmData[IDlsm]["area_factor"],
                                        battery_soc_max=lsmData[IDlsm]["battery_soc_max"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsmData[IDlsm]["battery_P_in"],
                                        battery_P_out=lsmData[IDlsm]["battery_P_out"], P_heatpump_in=lsmData[IDlsm]["P_heatpump_in"], P_heatpump_out=lsmData[IDlsm]["P_heatpump_out"],
                                        P_heatstore=lsmData[IDlsm]["P_heatstore"], v_heatstore=lsmData[IDlsm]["v_heatstore"], wastecomb_P_in=lsmData[IDlsm]["wastecomb_P_in"], 
                                        wastecomb_P_out=lsmData[IDlsm]["wastecomb_P_out"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsmData[IDlsm]["wastestore_v_max"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsmData[IDlsm]["wasteAD_P_in"], wasteAD_P_out= lsmData[IDlsm]["wasteAD_P_out"], 
                                        chp_P_in= lsmData[IDlsm]["chp_P_in"], chp_P_out= lsmData[IDlsm]["chp_P_out"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )



for component in lsmcomponents5:
    my_energysystem.add(component.component())
    lsm5.addComponent(component)






#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 

print("----------------Energy System configured----------------")



#---------------------- Additional constraints ----------------------





#Sewage treatment
saka = constraints_LSM.lsm_sewagetreatment(model=saka, label="Sewagetreatment rule", timesteps=timesteps)


#Waste transport input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)


#Biowaste transport input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)



#Sewage transmission input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)

"""
#Greywater investment for limitation
#LSC 1
saka=constraints_LSM.lsm_greywaterinstallation(model=saka, label="Greywaterrule_LSC_1", LSC="LSC_1", timesteps=timesteps, unit_capacity=unit_capacity, number_households=163)
saka.objective=constraints_LSM.add_objective_greywater(timesteps=timesteps, timestepsTotal=timestepsTotal, model=saka, variable=saka.MyBlock_Greywaterrule_LSC_1.int_greywater)
#LSC 2
saka=constraints_LSM.lsm_greywaterinstallation(model=saka, label="Greywaterrule_LSC_2", LSC="LSC_2", timesteps=timesteps, unit_capacity=unit_capacity, number_households=230)
saka.objective=constraints_LSM.add_objective_greywater(timesteps=timesteps, timestepsTotal=timestepsTotal, model=saka, variable=saka.MyBlock_Greywaterrule_LSC_2.int_greywater)
#LSC 3
saka=constraints_LSM.lsm_greywaterinstallation(model=saka, label="Greywaterrule_LSC_3", LSC="LSC_3", timesteps=timesteps, unit_capacity=unit_capacity, number_households=200)
saka.objective=constraints_LSM.add_objective_greywater(timesteps=timesteps, timestepsTotal=timestepsTotal, model=saka, variable=saka.MyBlock_Greywaterrule_LSC_3.int_greywater)
#LSC 4
saka=constraints_LSM.lsm_greywaterinstallation(model=saka, label="Greywaterrule_LSC_4", LSC="LSC_4", timesteps=timesteps, unit_capacity=unit_capacity, number_households=137)
saka.objective=constraints_LSM.add_objective_greywater(timesteps=timesteps, timestepsTotal=timestepsTotal, model=saka, variable=saka.MyBlock_Greywaterrule_LSC_4.int_greywater)
#LSC 5
saka=constraints_LSM.lsm_greywaterinstallation(model=saka, label="Greywaterrule_LSC_5", LSC="LSC_5", timesteps=timesteps, unit_capacity=unit_capacity, number_households=7)
saka.objective=constraints_LSM.add_objective_greywater(timesteps=timesteps, timestepsTotal=timestepsTotal, model=saka, variable=saka.MyBlock_Greywaterrule_LSC_5.int_greywater)
"""



#modelIn.objective = modelIn.objective + myBlock.int_greywater[timesteps-1]*10000


#----------------------- Solve the model ----------------------


print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka_investment.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")

    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results_extended(my_energysystem, saka, scenario, 'my_dump_investment.oemof')
#results = solph.processing.results(saka)
results_investment = meth.restore_results_extended(scenario, 'my_dump_investment.oemof')

print("----------------optimisation solved----------------")


#_______________________________________________________________________________________________________________________________________________________________________________________________________



#__________________________________________ STEP 2 OPERATIONAL MODEL ______________________________________________________________________________________________________________
print("____________________________________________________________________________________")
print("-------------------------- STEP 2 OPERATIONAL MODEL---------------------------------")
print("____________________________________________________________________________________")
print()


#Results from Investment Decision
lsc1_investment=consumer_lsm_operational.get_investment_results_lsc(consumer=lsc1, results=results_investment)
lsc2_investment=consumer_lsm_operational.get_investment_results_lsc(consumer=lsc2, results=results_investment)
lsc3_investment=consumer_lsm_operational.get_investment_results_lsc(consumer=lsc3, results=results_investment)
lsc4_investment=consumer_lsm_operational.get_investment_results_lsc(consumer=lsc4, results=results_investment)
lsc5_investment=consumer_lsm_operational.get_investment_results_lsc(consumer=lsc5, results=results_investment)

"""
#Add Greywater System capacities
lsc1_investment["v_max_greywater"] = solph.views.node(results_investment, "MyBlock_Greywaterrule_LSC_1").get("sequences").values[-1][0]*unit_capacity
lsc2_investment["v_max_greywater"] = solph.views.node(results_investment, "MyBlock_Greywaterrule_LSC_2").get("sequences").values[-1][0]*unit_capacity
lsc3_investment["v_max_greywater"] = solph.views.node(results_investment, "MyBlock_Greywaterrule_LSC_3").get("sequences").values[-1][0]*unit_capacity
lsc4_investment["v_max_greywater"] = solph.views.node(results_investment, "MyBlock_Greywaterrule_LSC_4").get("sequences").values[-1][0]*unit_capacity
lsc5_investment["v_max_greywater"] = solph.views.node(results_investment, "MyBlock_Greywaterrule_LSC_5").get("sequences").values[-1][0]*unit_capacity
"""

lsm1_investment=consumer_lsm_operational.get_investment_results_lsm(consumer=lsm1, results=results_investment)
lsm2_investment=consumer_lsm_operational.get_investment_results_lsm(consumer=lsm2, results=results_investment)
lsm3_investment=consumer_lsm_operational.get_investment_results_lsm(consumer=lsm3, results=results_investment)
lsm4_investment=consumer_lsm_operational.get_investment_results_lsm(consumer=lsm4, results=results_investment)
lsm5_investment=consumer_lsm_operational.get_investment_results_lsm(consumer=lsm5, results=results_investment)



#Save investment results to txt file
consumerlist_save=["LSC1", "LSC2", "LSC3", "LSC4", "LSC5", "LSM1", "LSM2", "LSM3", "LSM4", "LSM5"]
datalist_save=[lsc1_investment, lsc2_investment, lsc3_investment, lsc4_investment, lsc5_investment, lsm1_investment, lsm2_investment, lsm3_investment, lsm4_investment, lsm5_investment]
filename_save = scenario + "/investment.txt"
meth.write_dict(filename=filename_save, consumerlist=consumerlist_save, datalist=datalist_save)
consumerlist_save=["LSC1", "LSC2", "LSC3", "LSC4", "LSC5", "LSM1", "LSM2", "LSM3", "LSM4", "LSM5"]
datalist_save=[lsc1_investment, lsc2_investment, lsc3_investment, lsc4_investment, lsc5_investment, lsm1_investment, lsm2_investment, lsm3_investment, lsm4_investment, lsm5_investment]
filename_costs = scenario + "/costs_invest.txt"
meth.write_investmentcosts(filename=filename_costs, consumerlist=consumerlist_save, datalist=datalist_save)


timesteps=timesteps_operational

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)

#Define Energy System
my_energysystem_new = solph.EnergySystem(timeindex=my_index)


#---------------------- DEFINITION OF CONSUMERS ----------------------
#LSC1 / LSM1
lsc1 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_1')
lsc1.addElectricitySector()
lsc1.addHeatSector()
lsc1.addDistrictheatingSector()
lsc1.addWaterSector()
lsc1.addWasteSector()
lsc1.addEmissionSector()
lsc1.addWasteBioSector()
lsc1.addGasSector()
lsc1.sector2system(my_energysystem_new)


lsm1 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_1')
lsm1.addElectricitySector()
lsm1.addHeatSector()
lsm1.addWaterSector()
lsm1.addWasteSector()
lsm1.addEmissionSector()
lsm1.addWasteBioSector()
lsm1.addGasSector()
lsm1.sector2system(my_energysystem_new)


#LSC2 / LSM2
lsc2 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_2')
lsc2.addElectricitySector()
lsc2.addHeatSector()
lsc2.addDistrictheatingSector()
lsc2.addWaterSector()
lsc2.addWasteSector()
lsc2.addEmissionSector()
lsc2.addWasteBioSector()
lsc2.addGasSector()
lsc2.sector2system(my_energysystem_new)


lsm2 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_2')
lsm2.addElectricitySector()
lsm2.addHeatSector()
lsm2.addWaterSector()
lsm2.addWasteSector()
lsm2.addEmissionSector()
lsm2.addWasteBioSector()
lsm2.addGasSector()
lsm2.sector2system(my_energysystem_new)


#LSC3 / LSM3
lsc3 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_3')
lsc3.addElectricitySector()
lsc3.addHeatSector()
lsc3.addDistrictheatingSector()
lsc3.addWaterSector()
lsc3.addWasteSector()
lsc3.addEmissionSector()
lsc3.addWasteBioSector()
lsc3.addGasSector()
lsc3.sector2system(my_energysystem_new)


lsm3 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_3')
lsm3.addElectricitySector()
lsm3.addHeatSector()
lsm3.addWaterSector()
lsm3.addWasteSector()
lsm3.addEmissionSector()
lsm3.addWasteBioSector()
lsm3.addGasSector()
lsm3.sector2system(my_energysystem_new)


#LSC4 / LSM4
lsc4 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_4')
lsc4.addElectricitySector()
lsc4.addHeatSector()
lsc4.addDistrictheatingSector()
lsc4.addWaterSector()
lsc4.addWasteSector()
lsc4.addEmissionSector()
lsc4.addWasteBioSector()
lsc4.addGasSector()
lsc4.sector2system(my_energysystem_new)


lsm4 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_4')
lsm4.addElectricitySector()
lsm4.addHeatSector()
lsm4.addWaterSector()
lsm4.addWasteSector()
lsm4.addEmissionSector()
lsm4.addWasteBioSector()
lsm4.addGasSector()
lsm4.sector2system(my_energysystem_new)


#LSC5 / LSM5
lsc5 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC_5')
lsc5.addElectricitySector()
lsc5.addHeatSector()
lsc5.addDistrictheatingSector()
lsc5.addWaterSector()
lsc5.addWasteSector()
lsc5.addEmissionSector()
lsc5.addWasteBioSector()
lsc5.addGasSector()
lsc5.sector2system(my_energysystem_new)


lsm5 = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM_5')
lsm5.addElectricitySector()
lsm5.addHeatSector()
lsm5.addWaterSector()
lsm5.addWasteSector()
lsm5.addEmissionSector()
lsm5.addWasteBioSector()
lsm5.addGasSector()
lsm5.sector2system(my_energysystem_new)

print("----------------Consumer Data loaded----------------")


#Sets for LSC and LSM data
lscData = LSC_inputparameter.LSC_data()
lsmData = LSM_inputparameter.LSM_data()

#----------------------------- LSC 1 / LSM 1 -----------------------------------------------
#LSC 1
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc1.label
lsccomponents1 = consumer_lsm_operational.lsc_setup(LSC=lsc1, LSM=lsm1, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        P_pv=lsc1_investment["P_pv"],
                                        battery_soc_max=lsc1_investment["SOC_battery"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lsc1_investment["P_battery"], battery_P_out=lsc1_investment["P_battery"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lsc1_investment["P_heatpump"], P_heatpump_out=lsc1_investment["P_heatpump"],
                                        P_heatstore=0, v_heatstore=0, 
                                        P_districtheat=lsc1_investment["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], v_max_greywater=1000000, max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents1:
    my_energysystem_new.add(component.component())
    lsc1.addComponent(component)
    
#LSM Hub 1   
IDlsm = lsm1.label
lsmcomponents1 = consumer_lsm_operational.lsm_setup(LSM=lsm1, LSM_list=LSM_list, LSC=lsc1, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        P_pv=lsm1_investment["P_pv"],
                                        battery_soc_max=lsm1_investment["SOC_battery"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsm1_investment["P_battery"],
                                        battery_P_out=lsm1_investment["P_battery"], P_heatpump_in=lsm1_investment["P_heatpump"], P_heatpump_out=lsm1_investment["P_heatpump"],
                                        P_heatstore=lsm1_investment["P_thermstore"], v_heatstore=lsm1_investment["V_thermstore"], wastecomb_P_in=lsm1_investment["P_wastecomb"], 
                                        wastecomb_P_out=lsm1_investment["P_wastecomb"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsm1_investment["SOC_wastestore"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], vmax_sewagetreat=lsm1_investment["V_sewagetreat"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsm1_investment["P_wasteAD"], wasteAD_P_out= lsm1_investment["P_wasteAD"], 
                                        chp_P_in= lsm1_investment["P_CHP"], chp_P_out= lsm1_investment["P_CHP"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )

for component in lsmcomponents1:
    my_energysystem_new.add(component.component())
    lsm1.addComponent(component)
    

#----------------------------- LSC 2 / LSM 2 -----------------------------------------------
#LSC 2
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc2.label
lsccomponents2 = consumer_lsm_operational.lsc_setup(LSC=lsc2, LSM=lsm2, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        P_pv=lsc2_investment["P_pv"],
                                        battery_soc_max=lsc2_investment["SOC_battery"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lsc2_investment["P_battery"], battery_P_out=lsc2_investment["P_battery"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lsc2_investment["P_heatpump"], P_heatpump_out=lsc2_investment["P_heatpump"],
                                        P_heatstore=0, v_heatstore=0, 
                                        P_districtheat=lsc2_investment["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], v_max_greywater=1000000, max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents2:
    my_energysystem_new.add(component.component())
    lsc2.addComponent(component)
    
#LSM Hub 2   
IDlsm = lsm2.label
lsmcomponents2 = consumer_lsm_operational.lsm_setup(LSM=lsm2, LSM_list=LSM_list, LSC=lsc2, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        P_pv=lsm2_investment["P_pv"],
                                        battery_soc_max=lsm2_investment["SOC_battery"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsm2_investment["P_battery"],
                                        battery_P_out=lsm2_investment["P_battery"], P_heatpump_in=lsm2_investment["P_heatpump"], P_heatpump_out=lsm2_investment["P_heatpump"],
                                        P_heatstore=lsm2_investment["P_thermstore"], v_heatstore=lsm2_investment["V_thermstore"], wastecomb_P_in=lsm2_investment["P_wastecomb"], 
                                        wastecomb_P_out=lsm2_investment["P_wastecomb"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsm2_investment["SOC_wastestore"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], vmax_sewagetreat=lsm2_investment["V_sewagetreat"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsm2_investment["P_wasteAD"], wasteAD_P_out= lsm2_investment["P_wasteAD"], 
                                        chp_P_in= lsm2_investment["P_CHP"], chp_P_out= lsm2_investment["P_CHP"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )

for component in lsmcomponents2:
    my_energysystem_new.add(component.component())
    lsm2.addComponent(component)
    
    
#----------------------------- LSC 3 / LSM 3 -----------------------------------------------
#LSC 3
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc3.label
lsccomponents3 = consumer_lsm_operational.lsc_setup(LSC=lsc3, LSM=lsm3, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        P_pv=lsc3_investment["P_pv"],
                                        battery_soc_max=lsc3_investment["SOC_battery"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lsc3_investment["P_battery"], battery_P_out=lsc3_investment["P_battery"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lsc3_investment["P_heatpump"], P_heatpump_out=lsc3_investment["P_heatpump"],
                                        P_heatstore=0, v_heatstore=0, 
                                        P_districtheat=lsc3_investment["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], v_max_greywater=1000000, max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents3:
    my_energysystem_new.add(component.component())
    lsc3.addComponent(component)
    
#LSM Hub 3   
IDlsm = lsm3.label
lsmcomponents3 = consumer_lsm_operational.lsm_setup(LSM=lsm3, LSM_list=LSM_list, LSC=lsc3, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        P_pv=lsm3_investment["P_pv"],
                                        battery_soc_max=lsm3_investment["SOC_battery"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsm3_investment["P_battery"],
                                        battery_P_out=lsm3_investment["P_battery"], P_heatpump_in=lsm3_investment["P_heatpump"], P_heatpump_out=lsm3_investment["P_heatpump"],
                                        P_heatstore=lsm3_investment["P_thermstore"], v_heatstore=lsm3_investment["V_thermstore"], wastecomb_P_in=lsm3_investment["P_wastecomb"], 
                                        wastecomb_P_out=lsm3_investment["P_wastecomb"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsm3_investment["SOC_wastestore"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], vmax_sewagetreat=lsm3_investment["V_sewagetreat"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsm3_investment["P_wasteAD"], wasteAD_P_out= lsm3_investment["P_wasteAD"], 
                                        chp_P_in= lsm3_investment["P_CHP"], chp_P_out= lsm3_investment["P_CHP"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )

for component in lsmcomponents3:
    my_energysystem_new.add(component.component())
    lsm3.addComponent(component)
    
    
#----------------------------- LSC 4 / LSM 4 -----------------------------------------------
#LSC 4
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc4.label
lsccomponents4 = consumer_lsm_operational.lsc_setup(LSC=lsc4, LSM=lsm4, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        P_pv=lsc4_investment["P_pv"],
                                        battery_soc_max=lsc4_investment["SOC_battery"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lsc4_investment["P_battery"], battery_P_out=lsc4_investment["P_battery"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lsc4_investment["P_heatpump"], P_heatpump_out=lsc4_investment["P_heatpump"],
                                        P_heatstore=0, v_heatstore=0, 
                                        P_districtheat=lsc4_investment["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], v_max_greywater=1000000, max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents4:
    my_energysystem_new.add(component.component())
    lsc4.addComponent(component)
    
#LSM Hub 4   
IDlsm = lsm4.label
lsmcomponents4 = consumer_lsm_operational.lsm_setup(LSM=lsm4, LSM_list=LSM_list, LSC=lsc4, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        P_pv=lsm4_investment["P_pv"],
                                        battery_soc_max=lsm4_investment["SOC_battery"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsm4_investment["P_battery"],
                                        battery_P_out=lsm4_investment["P_battery"], P_heatpump_in=lsm4_investment["P_heatpump"], P_heatpump_out=lsm4_investment["P_heatpump"],
                                        P_heatstore=lsm4_investment["P_thermstore"], v_heatstore=lsm4_investment["V_thermstore"], wastecomb_P_in=lsm4_investment["P_wastecomb"], 
                                        wastecomb_P_out=lsm4_investment["P_wastecomb"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsm4_investment["SOC_wastestore"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], vmax_sewagetreat=lsm4_investment["V_sewagetreat"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsm4_investment["P_wasteAD"], wasteAD_P_out= lsm4_investment["P_wasteAD"], 
                                        chp_P_in= lsm4_investment["P_CHP"], chp_P_out= lsm4_investment["P_CHP"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )

for component in lsmcomponents4:
    my_energysystem_new.add(component.component())
    lsm4.addComponent(component)
    
    
#----------------------------- LSC 5 / LSM 5 -----------------------------------------------
#LSC 1
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc5.label
lsccomponents5 = consumer_lsm_operational.lsc_setup(LSC=lsc5, LSM=lsm5, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        P_pv=lsc5_investment["P_pv"],
                                        battery_soc_max=lsc5_investment["SOC_battery"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lsc5_investment["P_battery"], battery_P_out=lsc5_investment["P_battery"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lsc5_investment["P_heatpump"], P_heatpump_out=lsc5_investment["P_heatpump"],
                                        P_heatstore=0, v_heatstore=0, 
                                        P_districtheat=lsc5_investment["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"],
                                        share_greywater=lscData[IDlsc]["share_greywater"], greywater_price=lscData[IDlsc]["greywater_price"], v_max_greywater=1000000, max_greywater2water=lscData[IDlsc]["max_greywater2water"]
                                        
                                        )

for component in lsccomponents5:
    my_energysystem_new.add(component.component())
    lsc5.addComponent(component)
    
#LSM Hub 5   
IDlsm = lsm5.label
lsmcomponents5 = consumer_lsm_operational.lsm_setup(LSM=lsm5, LSM_list=LSM_list, LSC=lsc5, timesteps=timesteps, timestepsTotal=timestepsTotal,
                                        c_grid_elec=lsmData[IDlsm]["c_grid_elec"], c_abgabe_elec=lsmData[IDlsm]["c_abgabe_elec"], c_grid_power=lsmData[IDlsm]["c_grid_power"],
                                        P_elgrid=lsmData[IDlsm]["P_elgrid"], em_elgrid=lsmData[IDlsm]["em_elgrid"], 
                                        P_pv=lsm5_investment["P_pv"],
                                        battery_soc_max=lsm5_investment["SOC_battery"], battery_soc_start=lsmData[IDlsm]["battery_soc_start"], battery_P_in=lsm5_investment["P_battery"],
                                        battery_P_out=lsm5_investment["P_battery"], P_heatpump_in=lsm5_investment["P_heatpump"], P_heatpump_out=lsm5_investment["P_heatpump"],
                                        P_heatstore=lsm5_investment["P_thermstore"], v_heatstore=lsm5_investment["V_thermstore"], wastecomb_P_in=lsm5_investment["P_wastecomb"], 
                                        wastecomb_P_out=lsm5_investment["P_wastecomb"], wastecomb_usable_energy=lsmData[IDlsm]["wastecomb_usable_energy"],
                                        wastestore_v_max=lsm5_investment["SOC_wastestore"], wastestore_v_start=lsmData[IDlsm]["wastestore_v_start"], wastestore_disposal_periods=lsmData[IDlsm]["wastestore_disposal_periods"],
                                        wastestore_balanced=lsmData[IDlsm]["wastestore_balanced"], transmissioncapacity_wastetruck=lsmData[IDlsm]["transmissioncapacity_wastetruck"], distance_wastetruck=lsmData[IDlsm]["distance_wastetruck"],
                                        emissions_wastetruck=lsmData[IDlsm]["emissions_wastetruck"], costs_wastetruck=lsmData[IDlsm]["costs_wastetruck"], delay_wastetruck=lsmData[IDlsm]["delay_wastetruck"],
                                        wasted_water_costs=lsmData[IDlsm]["wasted_water_costs"], vmax_sewagetreat=lsm5_investment["V_sewagetreat"], sewagetreat_eta_water=lsmData[IDlsm]["sewagetreat_eta_water"], sewagetreat_emissions=lsmData[IDlsm]["sewagetreat_emissions"],
                                        sewagetreat_tempdif=lsmData[IDlsm]["sewagetreat_tempdif"], sludgecomb_P_in=lsmData[IDlsm]["sludgecomb_P_in"], sludgecomb_P_out=lsmData[IDlsm]["sludgecomb_P_out"],
                                        sludgecomb_usable_energy=lsmData[IDlsm]["sludgecomb_usable_energy"], sludgestore_v_max=lsmData[IDlsm]["sludgestore_v_max"], sludgestore_v_start=lsmData[IDlsm]["sludgestore_v_start"],
                                        sludgestore_disposal_periods=lsmData[IDlsm]["sludgestore_disposal_periods"], sludgestore_balanced=lsmData[IDlsm]["sludgestore_balanced"], 
                                        transmissioncapacity_sludgetruck=lsmData[IDlsm]["transmissioncapacity_sludgetruck"], distance_sludgetruck=lsmData[IDlsm]["distance_sludgetruck"], emissions_sludgetruck=lsmData[IDlsm]["emissions_sludgetruck"],
                                        costs_sludgetruck=lsmData[IDlsm]["costs_sludgetruck"], delay_sludgetruck=lsmData[IDlsm]["delay_sludgetruck"], co2price=lsmData[IDlsm]["co2price"],
                                        P_exhaustheat=lsmData[IDlsm]["P_exhaustheat"], P_districtheatsource=lsmData[IDlsm]["P_districtheatsource"],  
                                        c_districtheatsource=lsmData[IDlsm]["c_districtheatsource"],  em_districtheatsource=lsmData[IDlsm]["em_districtheatsource"],
                                        waste2biowaste_maxwaste= lsmData[IDlsm]["waste2biowaste_maxwaste"], share_biowaste_min= lsmData[IDlsm]["share_biowaste_min"], share_biowaste_max= lsmData[IDlsm]["share_biowaste_max"], 
                                        wastereduction_maxwaste= lsmData[IDlsm]["wastereduction_maxwaste"], reduction_rate= lsmData[IDlsm]["reduction_rate"], wastereduction_costs= lsmData[IDlsm]["wastereduction_costs"],
                                        min_reduction=lsmData[IDlsm]["min_reduction"], wastedisposal_vmax= lsmData[IDlsm]["wastedisposal_vmax"], wastedisposal_emissions= lsmData[IDlsm]["wastedisposal_emissions"], wastedisposal_costs= lsmData[IDlsm]["wastedisposal_costs"],
                                        min_disposal=lsmData[IDlsm]["min_disposal"], max_disposal=lsmData[IDlsm]["max_disposal"], wasteAD_P_in= lsm5_investment["P_wasteAD"], wasteAD_P_out= lsm5_investment["P_wasteAD"], 
                                        chp_P_in= lsm5_investment["P_CHP"], chp_P_out= lsm5_investment["P_CHP"],
                                        gassale_P_max= lsmData[IDlsm]["gassale_P_max"], max_gassale= lsmData[IDlsm]["max_gassale"], gas_price=lsmData[IDlsm]["gas_price"], gassale_limit=lsmData[IDlsm]["gassale_limit"]
                                        )

for component in lsmcomponents5:
    my_energysystem_new.add(component.component())
    lsm5.addComponent(component)
    
    
#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem_new) 

print("----------------Energy System configured----------------")


#---------------------- Additional constraints ----------------------


#Sewage treatment
saka = constraints_LSM.lsm_sewagetreatment(model=saka, label="Sewagetreatment rule", timesteps=timesteps)


#Waste transport input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_wastetransportblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)


#Biowaste transport input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_biowastetransportblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)


#Sewage transmission input/output at same time blocking
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_1", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_2", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_3", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_4", lsm_list=lsm_list, capacity=100000)
lsm_list=["LSM_1", "LSM_2", "LSM_3", "LSM_4", "LSM_5"]
saka = constraints_LSM.lsm_sewagetransmissionblock(model=saka, lsm="LSM_5", lsm_list=lsm_list, capacity=100000)





#----------------------- Solve the model ----------------------


print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")

    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results(my_energysystem_new, saka, scenario)
#results = solph.processing.results(saka)
results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")


#_______________________________________________________________________________________________________________________________________________________________________________________________________


#--------------------------- Store the results --------------------------------


"""
#consumerList=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, consumer10, consumer11, consumer12, consumer13, LSC]

#Store the costs
resultprocessing_lsc.costs_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                             consumer10, consumer11, consumer12, LSC], 
                                  results=results, filename='costs.csv', scenarioname=scenario, model=saka, timeseries=False)

#Store the results
resultprocessing_lsc.consumer_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                                consumer10, consumer11, consumer12, LSC], 
                                     results=results, filename='technology.csv', scenarioname=scenario)

"""

#Save objective and other important information to text file
filename_save = scenario + "/objective.txt"
meth.write_objective(scenario=scenario, scenariomessage=scenariomessage, objective=saka.objective(), filename=filename_save)


#Optional saving in csv or excel
results2save =results_investment
results2save = results
list_consumers=[lsc1, lsc2, lsc3, lsc4, lsc5, lsm1, lsm2, lsm3, lsm4, lsm5]

#meth.results_to_csv(consumers=list_consumers, results=results2save, filename=scenario)
meth.results_to_excel(consumers=list_consumers, results=results2save, filename=scenario)

print("----------------Results saved----------------")


#--------------------------- Code evaluation --------------------------------

#Timeseries
#test=solph.views.node(results, pv_invest.label).get("sequences")

#Investment
#solph.views.node(results, pv_invest.label).get("scalars")[0]

#Investment Satus
#solph.views.node(results, pv_invest.label).get("scalars")[1]

#Bei speicher anders!!!

