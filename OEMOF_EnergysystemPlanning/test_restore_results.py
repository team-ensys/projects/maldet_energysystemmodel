# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 11:56:16 2022

@author: Matthias Maldet
"""

import methods as meth

scenario = 'results/RESULTS_LSM/LSM3_Trading'
results = meth.restore_results_extended(scenario, "my_dump_investment.oemof")