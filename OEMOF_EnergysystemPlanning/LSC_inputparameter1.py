# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 08:42:25 2022

@author: Matthias Maldet
"""

def LSC_data():
    
    c_grid_elec = 0.062
    c_abgabe_elec = 0.018
    c_red_local = 0.43
    c_red_regional = 0.72
    c_elec_local = c_red_local*c_grid_elec+c_abgabe_elec
    c_elec_regional = c_red_regional*c_grid_elec+c_abgabe_elec
    share_greywater=0.5
    greywater_price=1
    min_greywater=0.1
    
    max_greywater2water=0.5
    
    lsc_struct ={
        
        #_________________________________________ LSC 1 _______________________________________________________
        
        "LSC_1" :{
            #Corresponding LSM
            "LSM" : "LSM_1",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1630,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 0,
            "area_roof": 16300,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1630,
            "battery_soc_start": 0,
            "battery_P_in": 1630,
            "battery_P_out": 1630,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 0.9999,
            "P_elec2lsm": 0,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_local, "LSM_2": 0.1435+c_elec_regional, 
                                    "LSM_3": 0.1435+c_elec_local, "LSM_4": 0.1435+c_elec_regional, "LSM_5": 0.1435+c_elec_regional},
            "efficiency_lsm_elec": {"LSM_1": 0.9999, "LSM_2": 0.97986, "LSM_3": 0.999, "LSM_4": 0.97986, "LSM_5": 0.97986},
            "P_lsm2elec": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 1141, 
            "P_heatpump_out": 1141,
            
            #Thermal storage
            "P_heatstore": 1141,
            "v_heatstore": 49,
            
            #Heat sale to LSM Hub
            "P_districtheat": 1141,
            "efficiency_lsc_heat": 0.9999,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.9999, "LSM_2": 0.98, "LSM_3": 0.988, "LSM_4": 0.976, "LSM_5": 0.98},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 3348,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 0,
            "wastestore_balanced": True,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 5/6*1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 5/6*1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #Greywater Sale
            "greywater_price" : greywater_price,
            "share_greywater" : share_greywater,
            "max_greywater2water" : max_greywater2water,
            "min_greywater" : min_greywater
            
            
            
            },
        
        #_________________________________________ LSC 2 _______________________________________________________
        
        "LSC_2" :{
            #Corresponding LSM
            "LSM" : "LSM_2",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 2300,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 0,
            "area_roof": 21620,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 2300,
            "battery_soc_start": 0,
            "battery_P_in": 2300,
            "battery_P_out": 2300,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 0.9999,
            "P_elec2lsm": 0,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_regional, "LSM_2": 0.1435+c_elec_local, "LSM_3": 0.1435+c_elec_regional, 
                                    "LSM_4": 0.1435+c_elec_regional, "LSM_5": 0.1435+c_elec_local},
            "efficiency_lsm_elec": {"LSM_1": 0.97986, "LSM_2": 0.9999, "LSM_3": 0.9798, "LSM_4": 0.97978, "LSM_5": 0.99996},
            "P_lsm2elec": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 1610, 
            "P_heatpump_out": 1610,
            
            #Thermal storage
            "P_heatstore": 1610,
            "v_heatstore": 69,
            
            #Heat sale to LSM Hub
            "P_districtheat": 1610,
            "efficiency_lsc_heat": 0.9999,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.98, "LSM_2": 0.9999, "LSM_3": 0.988, "LSM_4": 0.976, "LSM_5": 0.99},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 4077,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 0,
            "wastestore_balanced": True,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 5/6*1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 5/6*1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #Greywater Sale
            "greywater_price" : greywater_price,
            "share_greywater" : share_greywater,
            "max_greywater2water" : max_greywater2water,
            "min_greywater" : min_greywater
            
            
            
        },
        
            
        
        
        #_________________________________________ LSC 3 _______________________________________________________
        
        "LSC_3" :{
            #Corresponding LSM
            "LSM" : "LSM_3",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 2000,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 0,
            "area_roof": 20000,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 2000,
            "battery_soc_start": 0,
            "battery_P_in": 2000,
            "battery_P_out": 2000,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 0.9999,
            "P_elec2lsm": 0,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_local, "LSM_2": 0.1435+c_elec_regional, "LSM_3": 0.1435+c_elec_local, 
                                    "LSM_4": 0.1435+c_elec_regional, "LSM_5": 0.1435+c_elec_regional},
            "efficiency_lsm_elec": {"LSM_1": 0.9999, "LSM_2": 0.9798, "LSM_3": 0.9999, "LSM_4": 0.97962, "LSM_5": 0.9798},
            "P_lsm2elec": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 1400, 
            "P_heatpump_out": 1400,
            
            #Thermal storage
            "P_heatstore": 1400,
            "v_heatstore": 60,
            
            #Heat sale to LSM Hub
            "P_districtheat": 1400,
            "efficiency_lsc_heat": 0.9999,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.988, "LSM_2": 0.988, "LSM_3": 0.9999, "LSM_4": 0.984, "LSM_5": 0.988},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 3753,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 0,
            "wastestore_balanced": True,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5*5/6, "LSM_4": 1.5, "LSM_5": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5*5/6, "LSM_4": 1.5, "LSM_5": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #Greywater Sale
            "greywater_price" : greywater_price,
            "share_greywater" : share_greywater,
            "max_greywater2water" : max_greywater2water,
            "min_greywater" : min_greywater
            
            
            
        },
        
        #_________________________________________ LSC 4 _______________________________________________________
        
        "LSC_4" :{
            #Corresponding LSM
            "LSM" : "LSM_4",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1507,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 0,
            "area_roof": 10100,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 1370,
            "battery_soc_start": 0,
            "battery_P_in": 1370,
            "battery_P_out": 1370,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 0.9999,
            "P_elec2lsm": 0,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_regional, "LSM_2": 0.1435+c_elec_regional, "LSM_3": 0.1435+c_elec_regional, 
                                    "LSM_4": 0.1435+c_elec_local, "LSM_5": 0.1435+c_elec_regional},
            "efficiency_lsm_elec": {"LSM_1": 0.97968, "LSM_2": 0.97978, "LSM_3": 0.97962, "LSM_4": 0.9999, "LSM_5": 0.97978},
            "P_lsm2elec": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 1370, 
            "P_heatpump_out": 1370,
            
            #Thermal storage
            "P_heatstore": 1370,
            "v_heatstore": 41,
            
            #Heat sale to LSM Hub
            "P_districtheat": 1370,
            "efficiency_lsc_heat": 0.9999,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.976, "LSM_2": 0.976, "LSM_3": 0.984, "LSM_4": 0.9999, "LSM_5": 0.976},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 3051,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 0,
            "wastestore_balanced": True,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5*5/6, "LSM_5": 1.5},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5*5/6, "LSM_5": 1.5},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #Greywater Sale
            "greywater_price" : greywater_price,
            "share_greywater" : share_greywater,
            "max_greywater2water" : max_greywater2water,
            "min_greywater" : min_greywater
            
            
            
        },
        
        #_________________________________________ LSC 5 _______________________________________________________
        
        "LSC_5" :{
            #Corresponding LSM
            "LSM" : "LSM_5",
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec" : 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1000,
            "em_elgrid": 0.209,
            
            #PV
            "area_efficiency": 0,
            "area_roof": 4114,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 190,
            "battery_soc_start": 0,
            "battery_P_in": 190,
            "battery_P_out": 190,
            
            #Electricity sale to LSM Hub
            "sell_tariff_lsc_elec" : 0.1435,
            "efficiency_lsc_elec": 0.9999,
            "P_elec2lsm": 0,
            
            #Purchase electricity from LSM Hubs
            "rev_tariff_lsm_elec": {"LSM_1": 0.1435, "LSM_2": 0.1435, "LSM_3":0.1435, "LSM_4": 0.1435, "LSM_5": 0.1435},
            "buy_tariff_lsm_elec": {"LSM_1": 0.1435+c_elec_regional, "LSM_2": 0.1435+c_elec_local, "LSM_3": 0.1435+c_elec_regional, 
                                    "LSM_4": 0.1435+c_elec_regional, "LSM_5": 0.1435+c_elec_local},
            "efficiency_lsm_elec": {"LSM_1": 0.97986, "LSM_2": 0.99996, "LSM_3": 0.9798, "LSM_4": 0.97978, "LSM_5": 0.9999},
            "P_lsm2elec": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump
            "P_heatpump_in": 400, 
            "P_heatpump_out": 400,
            
            #Thermal storage
            "P_heatstore": 400,
            "v_heatstore": 17.2,
            
            #Heat sale to LSM Hub
            "P_districtheat": 400,
            "efficiency_lsc_heat": 0.9999,
            
            #Purchase heat from LSM Hubs
            "rev_tariff_lsm_heat": {"LSM_1": 0.03, "LSM_2": 0.03, "LSM_3": 0.03, "LSM_4": 0.03, "LSM_5": 0.03},
            "buy_tariff_lsm_heat": {"LSM_1": 0.076, "LSM_2": 0.076, "LSM_3": 0.076, "LSM_4": 0.076, "LSM_5": 0.076},
            "efficiency_lsm_heat": {"LSM_1": 0.98, "LSM_2": 0.99, "LSM_3": 0.988, "LSM_4": 0.976, "LSM_5": 0.9999},
            
            
            #----------------- Waste ------------------------------
            
            #Waste storage
            "v_max_wastestore": 2726,
            "v_start_wastestore": 0,
            "waste_disposal_periods": 0,
            "wastestore_balanced": True,
            "wastestore_empty_end" : False,
            
            
            #----------------- Water ------------------------------
            
            #Water pipeline purchase
            "waterpurchase_limit": 1,
            "waterpurchase_discount": 1,
            
            
            #LSM recovered water purchase
            "lsmwaterpurchase_limit": 1,
            "lsmwaterpurchase_discount": 0.9,
            "rev_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5*5/6},
            "buy_tariff_lsm_water": {"LSM_1": 1.5, "LSM_2": 1.5, "LSM_3": 1.5, "LSM_4": 1.5, "LSM_5": 1.5*5/6},
            "efficiency_lsm_water": {"LSM_1": 1, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1, "LSM_5": 1},
            
            #Water reduction
            "wff": 0,
            "revenues_waterreduction": 0,
            
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0.03,
            
            #Greywater Sale
            "greywater_price" : greywater_price,
            "share_greywater" : share_greywater,
            "max_greywater2water" : max_greywater2water,
            "min_greywater" : min_greywater
            
            
            
        }
        
        

        
        }
    
    return lsc_struct