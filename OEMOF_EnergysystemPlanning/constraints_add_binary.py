# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 10:28:54 2021

@author: Matthias Maldet
"""

import pyomo.environ as po
import pyomo.core as pyomo
import numpy as np
#import pyomo_rules

def transport_decision(*args, **kwargs):
    
    #Vehicle Objects
    electricVehicle = kwargs.get("electric_vehicle", 0)
    biofuelVehicle = kwargs.get("biofuel_vehicle", 0)
    fuelcellVehicle = kwargs.get("fuelcell_vehicle", 0)
    petrolVehicle = kwargs.get("petrol_vehicle", 0)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    
    
    #Transport Demand
    timesteps = kwargs.get("timesteps", 8760)
    demandArray = np.zeros(timesteps)
            
    demand = kwargs.get("demand_timeseries", demandArray)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if electricVehicle:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle Drive'
            )
            
        if biofuelVehicle:
    
            
            # Biofuel Vehicle Binary
            myBlock.bin_BioV = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Biofuel Vehicle Drive')
    
        if fuelcellVehicle:
            
            # Fuelcell Vehicle Binary
            myBlock.bin_FC = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Fuelcell Vehicle Drive')
    
        if petrolVehicle:

            # Petrol Vehicle Binary
            myBlock.bin_PET = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Petrol Vehicle Drive')
    
    
        modelIn.add_component('MyBlockVehicleBinary', myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        def binarySumRule(model, t):
            return (model.bin_EV[t] + model.bin_BioV[t] + model.bin_FC[t] + model.bin_PET[t] <= 1)

        def binaryTransportConstraintRule(model, t):
            return ((model.bin_EV[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle.labelDrive)) 
                                                            + (model.bin_BioV[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==biofuelVehicle.labelDrive)) 
                                                            + (model.bin_FC[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==fuelcellVehicle.labelDrive))
                                                            + (model.bin_PET[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==petrolVehicle.labelDrive)) 
                                                            == demand[t])
    
        PmaxCharge_EV = electricVehicle.P_charge
        def chargePowerEVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle.label)) 
                                                            <= (1-model.bin_EV[t]) * PmaxCharge_EV)
    
        Flow_BioV = biofuelVehicle.Q_charge
        def chargePowerBioVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==biofuelVehicle.label)) 
                                                            <= (1-model.bin_BioV[t]) * Flow_BioV)
    
        Flow_fc = fuelcellVehicle.Q_charge
        def chargePowerFCVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==fuelcellVehicle.label)) 
                                                            <= (1-model.bin_FC[t]) * Flow_fc)
    
        Flow_petV = petrolVehicle.Q_charge
        def chargePowerPetVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==petrolVehicle.label)) 
                                                            <= (1-model.bin_PET[t]) * Flow_petV)
    
    
                                                            #<=100)
    
        if(electricVehicle != 0 or biofuelVehicle != 0 or fuelcellVehicle != 0 or petrolVehicle != 0):
            myBlock.binaryTransportBalance = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule,
                doc='Transport Balance')
            
            myBlock.binarySum = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule,
                doc='Sum of Tranfport Binaries')
        
        
            if electricVehicle:
                myBlock.evCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule,
                    doc='EV Charging')
        
            if biofuelVehicle:
                myBlock.biovCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBioVRule,
                    doc='Biofuel Vehicle Charging')
        
            if fuelcellVehicle:
                myBlock.fuelcellvCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerFCVRule,
                    doc='Fuelcell Vehicle Charging')
        
            if petrolVehicle:
                myBlock.petvCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerPetVRule,
                    doc='Petrol Vehicle Charging')
            
        
        return modelIn


def transport_decision_EVs_flex(*args, **kwargs):
    
    #Vehicle Objects
    vehicles = kwargs.get("vehicles", 0)
    #Model
    modelIn = kwargs.get("model", 0)
    
            
    demands = kwargs.get("demands", 0)
    
    componentName = kwargs.get("componentName", 'DummyComponent')
    
    
    if(modelIn==0 or vehicles==0 or demands==0):
        print('Model not found!')
        return 0
    
    
    peakpowers=[]
    
    for i in vehicles:
        peakpowers.append(i.P_charge)
    
    
    maxIndex=max(len(vehicles), len(demands))
    elementnumbers = list(range(maxIndex))
    
    myBlock = po.Block()
    
    #Set with timesteps of OEMOF Model
    myBlock.t = pyomo.Set(
        initialize=modelIn.TIMESTEPS,
        ordered=True,
        doc='Set of timesteps')
    
    #Set with Vehicles
    rangeI=(range(len(vehicles)))
    myBlock.i = pyomo.Set(
        initialize=rangeI,
        ordered=True,
        doc='Set of vehicles')
    
     
    rangeJ=(range(len(demands)))
    #Set with demands
    myBlock.j = pyomo.Set(
        initialize=rangeJ,
        ordered=True,
        doc='Set of demands')
    
    
    """
    # tuples
    #Vehicle to transport demand tuple
    myBlock.veh_trans = pyomo.Set(
        within=myBlock.i*myBlock.j,
        doc='Tuple for vehicle to demand assignment')
    """
    
    
    
    #Binary variables
    myBlock.bin_EV = pyomo.Var(
                myBlock.i, myBlock.j, myBlock.t,
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle to demand'
            )
    

    """
    #Input flows for driving and charging
    driveflowSets = []
    chargeflowSets = []
    for vehicle in vehicles:
        myBlock.driveFlow = pyomo.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==vehicle.labelDrive])
        driveflowSets.append(myBlock.driveFlow)
        myBlock.chargeFlow = pyomo.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])==vehicle.label])
        chargeflowSets.append(myBlock.chargeFlow)
    """
    

        
    
    modelIn.add_component(componentName, myBlock)
    
      
    #Constraints    
    
    def binarySumRuleDemands(model, j, t):
            return (sum(model.bin_EV[i, j, t] for i in model.i)  <= 1)
        
    myBlock.binaryTransportBalanceDemands = pyomo.Constraint(
                myBlock.j, myBlock.t,
                rule=binarySumRuleDemands,
                doc='Binary Sum for Demands')
    
   
    
    def binarySumRuleVehicles(model, i, t):
            return (sum(model.bin_EV[i, j, t] for j in model.j)  <= 1)
        
    myBlock.binaryTransportBalanceVehicles = pyomo.Constraint(
                myBlock.i, myBlock.t,
                rule=binarySumRuleVehicles,
                doc='Binary Sum for Vehicles')
    
    
    
    #Transport Summe: Nur ein Fahrzeug kann einen Bedarf decken
    def transportsumRule(model, j, t):
        return (sum(model.bin_EV[i, j, t]*sum(modelIn.flow[k, o, t] for (k, o) in modelIn.FLOWS if k.label==vehicles[i].labelDrive) for i in model.i)  == demands[j][t])
           

    myBlock.transportCoverage = pyomo.Constraint(
                myBlock.j, myBlock.t,
                rule=transportsumRule,
                doc='Binary transport demand coverage')
    
    
    #Charge blocking
    def chargeblockRule(model, i, j, t):
        return ((sum(modelIn.flow[k, o, t] for (k, o) in modelIn.FLOWS if o.label==vehicles[i].label)) <= (1-model.bin_EV[i, j, t])*peakpowers[i])
           
    
    myBlock.chargeblock = pyomo.Constraint(
                myBlock.i, myBlock.j, myBlock.t, 
                rule=chargeblockRule,
                doc='Binary charge blocking rule')
    
    return modelIn    


def transport_decision_EVs(*args, **kwargs):
    
    #Vehicle Objects
    electricVehicle1 = kwargs.get("electric_vehicle1", 0)
    electricVehicle2 = kwargs.get("electric_vehicle2", 0)
    electricVehicle3 = kwargs.get("electric_vehicle3", 0)
    
    componentName = kwargs.get("componentName", 'DummyComponent')
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    
    
    #Transport Demand
    timesteps = kwargs.get("timesteps", 8760)
    demandArray = np.zeros(timesteps)
            
    demand1 = kwargs.get("demand_timeseries1", demandArray)
    demand2 = kwargs.get("demand_timeseries2", demandArray)
    demand3 = kwargs.get("demand_timeseries3", demandArray)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if electricVehicle1:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV11 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 1 Drive1'
            )
            
        if electricVehicle1:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV12 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 1 Drive2'
            )
            
        if electricVehicle1:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV13 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 1 Drive3'
            )
            
        if electricVehicle2:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV21 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 2 Drive1'
            )
            
        if electricVehicle2:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV22 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 2 Drive2'
            )
            
        if electricVehicle2:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV23 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 2 Drive3'
            )
            
            
        
        if electricVehicle3:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV31 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 3 Drive1'
            )
    
        if electricVehicle3:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV32 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 3 Drive2'
            )
    
        if electricVehicle3:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV33 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 3 Drive3'
            )
    
    
        modelIn.add_component(componentName, myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        def binarySumRule1(model, t):
            return (model.bin_EV11[t] + model.bin_EV21[t] + model.bin_EV31[t] <= 1)
        
        def binarySumRule2(model, t):
            return (model.bin_EV12[t] + model.bin_EV22[t] + model.bin_EV32[t] <= 1)
        
        def binarySumRule3(model, t):
            return (model.bin_EV13[t] + model.bin_EV23[t] + model.bin_EV33[t] <= 1)
        
        def binarySumRule4(model, t):
            return (model.bin_EV11[t] + model.bin_EV12[t] + model.bin_EV13[t] <= 1)
        
        def binarySumRule5(model, t):
            return (model.bin_EV21[t] + model.bin_EV22[t] + model.bin_EV23[t] <= 1)
        
        def binarySumRule6(model, t):
            return (model.bin_EV31[t] + model.bin_EV32[t] + model.bin_EV33[t] <= 1)
        


        def binaryTransportConstraintRule1(model, t):
            return ((model.bin_EV11[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle1.labelDrive)) 
                                                            + (model.bin_EV21[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle2.labelDrive)) 
                                                            + (model.bin_EV31[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle3.labelDrive)) 
                                                            == demand1[t])
        
        def binaryTransportConstraintRule2(model, t):
            return ((model.bin_EV12[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle1.labelDrive)) 
                                                            + (model.bin_EV22[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle2.labelDrive)) 
                                                            + (model.bin_EV32[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle3.labelDrive)) 
                                                            == demand2[t])
        
        def binaryTransportConstraintRule3(model, t):
            return ((model.bin_EV13[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle1.labelDrive)) 
                                                            + (model.bin_EV23[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle2.labelDrive)) 
                                                            + (model.bin_EV33[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle3.labelDrive)) 
                                                            == demand3[t])
    
        PmaxCharge_EV1 = electricVehicle1.P_charge
        def chargePowerEVRule11(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle1.label)) 
                                                            <= (1-model.bin_EV11[t]) * PmaxCharge_EV1)
        
        def chargePowerEVRule12(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle1.label)) 
                                                            <= (1-model.bin_EV12[t]) * PmaxCharge_EV1)
    
        def chargePowerEVRule13(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle1.label)) 
                                                            <= (1-model.bin_EV13[t]) * PmaxCharge_EV1)
    
    
        PmaxCharge_EV2 = electricVehicle2.P_charge
        def chargePowerEVRule21(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle2.label)) 
                                                            <= (1-model.bin_EV21[t]) * PmaxCharge_EV2)
        
        def chargePowerEVRule22(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle2.label)) 
                                                            <= (1-model.bin_EV22[t]) * PmaxCharge_EV2)
        
        def chargePowerEVRule23(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle2.label)) 
                                                            <= (1-model.bin_EV23[t]) * PmaxCharge_EV2)
        
        PmaxCharge_EV3 = electricVehicle3.P_charge
        def chargePowerEVRule31(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle3.label)) 
                                                            <= (1-model.bin_EV31[t]) * PmaxCharge_EV3)
        
        def chargePowerEVRule32(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle3.label)) 
                                                            <= (1-model.bin_EV32[t]) * PmaxCharge_EV3)
        
        def chargePowerEVRule33(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle3.label)) 
                                                            <= (1-model.bin_EV33[t]) * PmaxCharge_EV3)
    
    

    
        if(electricVehicle1 != 0 or electricVehicle2 != 0 or electricVehicle3 != 0):
            myBlock.binaryTransportBalance1 = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule1,
                doc='Transport Balance1')
            
            
            myBlock.binaryTransportBalance2 = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule2,
                doc='Transport Balance2')
            
            myBlock.binaryTransportBalance3 = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule3,
                doc='Transport Balance3')
            
            
            myBlock.binarySum1 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule1,
                doc='Sum1 of Transport Binaries')
            
            myBlock.binarySum2 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule2,
                doc='Sum2 of Transport Binaries')
            
            myBlock.binarySum3 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule3,
                doc='Sum3 of Transport Binaries')
            
            myBlock.binarySum4 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule4,
                doc='Sum4 of Transport Binaries')
            
            myBlock.binarySum5 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule5,
                doc='Sum5 of Transport Binaries')
            
            myBlock.binarySum6 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule6,
                doc='Sum6 of Transport Binaries')
        
        
            
            if electricVehicle1:
                myBlock.evCharging1 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule11,
                    doc='EV Charging1')
                
            if electricVehicle1:
                myBlock.evCharging2 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule12,
                    doc='EV Charging2')
                
            if electricVehicle1:
                myBlock.evCharging3 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule13,
                    doc='EV Charging3')
        
            if electricVehicle2:
                myBlock.evCharging4 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule21,
                    doc='EV Charging4')
                
            if electricVehicle2:
                myBlock.evCharging5 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule22,
                    doc='EV Charging5')
                
            if electricVehicle2:
                myBlock.evCharging6 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule23,
                    doc='EV Charging6')
                
            if electricVehicle3:
                myBlock.evCharging7 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule31,
                    doc='EV Charging7')
                
            if electricVehicle3:
                myBlock.evCharging8 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule32,
                    doc='EV Charging8')
                
            if electricVehicle3:
                myBlock.evCharging9 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule33,
                    doc='EV Charging9')
            
        
        return modelIn
    


def transport_decision_EVs_plus1(*args, **kwargs):
    
    #Vehicle Objects
    electricVehicle1 = kwargs.get("electric_vehicle1", 0)
    electricVehicle2 = kwargs.get("electric_vehicle2", 0)
    electricVehicle3 = kwargs.get("electric_vehicle3", 0)
    electricVehicle4 = kwargs.get("electric_vehicle4", 0)
    
    componentName = kwargs.get("componentName", 'DummyComponent')
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    
    
    #Transport Demand
    timesteps = kwargs.get("timesteps", 8760)
    demandArray = np.zeros(timesteps)
            
    demand1 = kwargs.get("demand_timeseries1", demandArray)
    demand2 = kwargs.get("demand_timeseries2", demandArray)
    demand3 = kwargs.get("demand_timeseries3", demandArray)
    demand4 = kwargs.get("demand_timeseries4", demandArray)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if electricVehicle1:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV11 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 1 Drive1'
            )
            
        if electricVehicle1:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV12 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 1 Drive2'
            )
            
        if electricVehicle1:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV13 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 1 Drive3'
            )
            
        if electricVehicle1:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV14 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 1 Drive4'
            )
            
        if electricVehicle2:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV21 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 2 Drive1'
            )
            
        if electricVehicle2:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV22 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 2 Drive2'
            )
            
        if electricVehicle2:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV23 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 2 Drive3'
            )
            
        if electricVehicle2:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV24 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 2 Drive4'
            )
            
            
        
        if electricVehicle3:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV31 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 3 Drive1'
            )
    
        if electricVehicle3:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV32 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 3 Drive2'
            )
    
        if electricVehicle3:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV33 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 3 Drive3'
            )
            
        if electricVehicle3:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV34 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 3 Drive4'
            )
    
    
        if electricVehicle4:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV41 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 4 Drive1'
            )
    
        if electricVehicle4:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV42 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 4 Drive2'
            )
    
        if electricVehicle4:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV43 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 4 Drive3'
            )
            
        if electricVehicle4:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV44 = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle 4 Drive4'
            )
    
        modelIn.add_component(componentName, myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        def binarySumRule1(model, t):
            return (model.bin_EV11[t] + model.bin_EV21[t] + model.bin_EV31[t] + model.bin_EV41[t] <= 1)
        
        def binarySumRule2(model, t):
            return (model.bin_EV12[t] + model.bin_EV22[t] + model.bin_EV32[t] + model.bin_EV42[t] <= 1)
        
        def binarySumRule3(model, t):
            return (model.bin_EV13[t] + model.bin_EV23[t] + model.bin_EV33[t] + model.bin_EV43[t] <= 1)
        
        def binarySumRule4(model, t):
            return (model.bin_EV11[t] + model.bin_EV12[t] + model.bin_EV13[t] + model.bin_EV14[t] <= 1)
        
        def binarySumRule5(model, t):
            return (model.bin_EV21[t] + model.bin_EV22[t] + model.bin_EV23[t] + model.bin_EV24[t] <= 1)
        
        def binarySumRule6(model, t):
            return (model.bin_EV31[t] + model.bin_EV32[t] + model.bin_EV33[t] + model.bin_EV34[t] <= 1)
        
        def binarySumRule7(model, t):
            return (model.bin_EV14[t] + model.bin_EV24[t] + model.bin_EV34[t] + model.bin_EV44[t] <= 1)
        
        def binarySumRule8(model, t):
            return (model.bin_EV41[t] + model.bin_EV42[t] + model.bin_EV43[t] + model.bin_EV44[t] <= 1)

        def binaryTransportConstraintRule1(model, t):
            return ((model.bin_EV11[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle1.labelDrive)) 
                                                            + (model.bin_EV21[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle2.labelDrive)) 
                                                            + (model.bin_EV31[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle3.labelDrive))
                                                            + (model.bin_EV41[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle4.labelDrive))
                                                            == demand1[t])
        
        def binaryTransportConstraintRule2(model, t):
            return ((model.bin_EV12[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle1.labelDrive)) 
                                                            + (model.bin_EV22[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle2.labelDrive)) 
                                                            + (model.bin_EV32[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle3.labelDrive))
                                                            + (model.bin_EV42[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle4.labelDrive))
                                                            == demand2[t])
        
        def binaryTransportConstraintRule3(model, t):
            return ((model.bin_EV13[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle1.labelDrive)) 
                                                            + (model.bin_EV23[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle2.labelDrive)) 
                                                            + (model.bin_EV33[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle3.labelDrive))
                                                            + (model.bin_EV43[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle4.labelDrive))
                                                            == demand3[t])
        
        def binaryTransportConstraintRule4(model, t):
            return ((model.bin_EV14[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle1.labelDrive)) 
                                                            + (model.bin_EV24[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle2.labelDrive)) 
                                                            + (model.bin_EV34[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle3.labelDrive))
                                                            + (model.bin_EV44[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle4.labelDrive))
                                                            == demand4[t])
    
        PmaxCharge_EV1 = electricVehicle1.P_charge
        def chargePowerEVRule11(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle1.label)) 
                                                            <= (1-model.bin_EV11[t]) * PmaxCharge_EV1)
        
        def chargePowerEVRule12(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle1.label)) 
                                                            <= (1-model.bin_EV12[t]) * PmaxCharge_EV1)
    
        def chargePowerEVRule13(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle1.label)) 
                                                            <= (1-model.bin_EV13[t]) * PmaxCharge_EV1)
        
        def chargePowerEVRule14(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle1.label)) 
                                                            <= (1-model.bin_EV14[t]) * PmaxCharge_EV1)
    
    
        PmaxCharge_EV2 = electricVehicle2.P_charge
        def chargePowerEVRule21(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle2.label)) 
                                                            <= (1-model.bin_EV21[t]) * PmaxCharge_EV2)
        
        def chargePowerEVRule22(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle2.label)) 
                                                            <= (1-model.bin_EV22[t]) * PmaxCharge_EV2)
        
        def chargePowerEVRule23(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle2.label)) 
                                                            <= (1-model.bin_EV23[t]) * PmaxCharge_EV2)
        
        def chargePowerEVRule24(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle2.label)) 
                                                            <= (1-model.bin_EV24[t]) * PmaxCharge_EV2)
        
        PmaxCharge_EV3 = electricVehicle3.P_charge
        def chargePowerEVRule31(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle3.label)) 
                                                            <= (1-model.bin_EV31[t]) * PmaxCharge_EV3)
        
        def chargePowerEVRule32(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle3.label)) 
                                                            <= (1-model.bin_EV32[t]) * PmaxCharge_EV3)
        
        def chargePowerEVRule33(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle3.label)) 
                                                            <= (1-model.bin_EV33[t]) * PmaxCharge_EV3)
        
        def chargePowerEVRule34(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle3.label)) 
                                                            <= (1-model.bin_EV34[t]) * PmaxCharge_EV3)
        
        PmaxCharge_EV4 = electricVehicle4.P_charge
        def chargePowerEVRule41(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle4.label)) 
                                                            <= (1-model.bin_EV41[t]) * PmaxCharge_EV4)
        
        def chargePowerEVRule42(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle4.label)) 
                                                            <= (1-model.bin_EV42[t]) * PmaxCharge_EV4)
        
        def chargePowerEVRule43(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle4.label)) 
                                                            <= (1-model.bin_EV43[t]) * PmaxCharge_EV4)
        
        def chargePowerEVRule44(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle4.label)) 
                                                            <= (1-model.bin_EV44[t]) * PmaxCharge_EV4)
    
    

    
        if(electricVehicle1 != 0 or electricVehicle2 != 0 or electricVehicle3 != 0 or electricVehicle4 != 0):
            myBlock.binaryTransportBalance1 = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule1,
                doc='Transport Balance1')
            
            
            myBlock.binaryTransportBalance2 = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule2,
                doc='Transport Balance2')
            
            myBlock.binaryTransportBalance3 = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule3,
                doc='Transport Balance3')
            
            myBlock.binaryTransportBalance4 = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule4,
                doc='Transport Balance4')
            
            
            myBlock.binarySum1 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule1,
                doc='Sum1 of Transport Binaries')
            
            myBlock.binarySum2 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule2,
                doc='Sum2 of Transport Binaries')
            
            myBlock.binarySum3 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule3,
                doc='Sum3 of Transport Binaries')
            
            myBlock.binarySum4 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule4,
                doc='Sum4 of Transport Binaries')
            
            myBlock.binarySum5 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule5,
                doc='Sum5 of Transport Binaries')
            
            myBlock.binarySum6 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule6,
                doc='Sum6 of Transport Binaries')
            
            myBlock.binarySum7 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule7,
                doc='Sum7 of Transport Binaries')
            
            myBlock.binarySum8 = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule8,
                doc='Sum8 of Transport Binaries')
        
        
            
            if electricVehicle1:
                myBlock.evCharging1 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule11,
                    doc='EV Charging1')
                
            if electricVehicle1:
                myBlock.evCharging2 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule12,
                    doc='EV Charging2')
                
            if electricVehicle1:
                myBlock.evCharging3 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule13,
                    doc='EV Charging3')
                
            if electricVehicle1:
                myBlock.evCharging4 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule14,
                    doc='EV Charging4')
        
            if electricVehicle2:
                myBlock.evCharging5 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule21,
                    doc='EV Charging5')
                
            if electricVehicle2:
                myBlock.evCharging6 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule22,
                    doc='EV Charging6')
                
            if electricVehicle2:
                myBlock.evCharging7 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule23,
                    doc='EV Charging7')
                
            if electricVehicle2:
                myBlock.evCharging8 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule24,
                    doc='EV Charging8')
                
            if electricVehicle3:
                myBlock.evCharging9 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule31,
                    doc='EV Charging9')
                
            if electricVehicle3:
                myBlock.evCharging10 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule32,
                    doc='EV Charging10')
                
            if electricVehicle3:
                myBlock.evCharging11 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule33,
                    doc='EV Charging11')
                
            if electricVehicle3:
                myBlock.evCharging12 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule34,
                    doc='EV Charging12')
                
            if electricVehicle4:
                myBlock.evCharging13 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule41,
                    doc='EV Charging13')
                
            if electricVehicle4:
                myBlock.evCharging14 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule42,
                    doc='EV Charging14')
                
            if electricVehicle4:
                myBlock.evCharging15 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule43,
                    doc='EV Charging15')
                
            if electricVehicle4:
                myBlock.evCharging16 = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule44,
                    doc='EV Charging16')
            
        
        return modelIn
    
        
def battery_chargeblock(*args, **kwargs):
    
    #Vehicle Objects
    battery = kwargs.get("battery", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if battery:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_battery = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Charge and Discharge Block'
            )
            
      
    
        modelIn.add_component('MyBlockBattery_' + battery.label, myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        
        PmaxCharge_Battery = battery.P_in
        def chargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==battery.label)) 
                                                            <= (model.bin_battery[t]) * PmaxCharge_Battery)
        
        PmaxDischarge_Battery = battery.P_out
        def dischargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==battery.label)) 
                                                            <= (1-model.bin_battery[t]) * PmaxDischarge_Battery)
    

        
        
        
        if battery:
                
                myBlock.batteryCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBatteryRule,
                    doc='Battery Charging')
                
                myBlock.batteryDischarging = pyomo.Constraint(
                    myBlock.t,
                    rule=dischargePowerBatteryRule,
                    doc='Battery Discharging')
        
           
            
        
        return modelIn  
        
    
def thermalstorage_chargeblock(*args, **kwargs):
    
    #Vehicle Objects
    battery = kwargs.get("storage", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if battery:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_thermalstorage = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Charge and Discharge Block'
            )
            
      
    
        modelIn.add_component('MyBlockBattery_' + battery.label, myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        
        PmaxCharge_Battery = battery.P_in
        def chargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==battery.label)) 
                                                            <= (model.bin_thermalstorage[t]) * PmaxCharge_Battery)
        
        PmaxDischarge_Battery = battery.P_out
        def dischargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==battery.label)) 
                                                            <= (1-model.bin_thermalstorage[t]) * PmaxDischarge_Battery)
    

        
        
        
        if battery:
                
                myBlock.storageCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBatteryRule,
                    doc='Storage Charging')
                
                myBlock.storageDischarging = pyomo.Constraint(
                    myBlock.t,
                    rule=dischargePowerBatteryRule,
                    doc='Storage Discharging')
        
           
            
        
        return modelIn  


def waterstorage_chargeblock_old(*args, **kwargs):
    
    #Vehicle Objects
    battery = kwargs.get("storage", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if battery:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_waterstorage = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Charge and Discharge Block'
            )
            
      
    
        modelIn.add_component('MyBlockBattery_' + battery.label, myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        
        Q_max = battery.q_max
        def chargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==battery.label)) 
                                                            <= (model.bin_waterstorage[t]) * Q_max)
        
        
        def dischargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==battery.label)) 
                                                            <= (1-model.bin_waterstorage[t]) * Q_max)
    

        
        
        
        if battery:
                
                myBlock.storageCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBatteryRule,
                    doc='Water Storage Charging')
                
                myBlock.storageDischarging = pyomo.Constraint(
                    myBlock.t,
                    rule=dischargePowerBatteryRule,
                    doc='Water Storage Discharging')
        
           
            
        
        return modelIn  
 

def waterstorage_chargeblock(*args, **kwargs):
    
    #Vehicle Objects
    battery = kwargs.get("storage", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if battery:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_waterstorage = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Charge and Discharge Block'
            )
            
      
    
        modelIn.add_component('MyBlockBattery_' + battery.label, myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        #getTheFlows
        #flow[0] wenn das Label im ersten Teil ist --> Output Flow eines Transformators oder Flow in Sink
        #flow[1] wenn das Label im zweiten Teil ist --> Input Flow eines Transformators oder Flow aus Source
        myBlock.Batinflows = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])==battery.label])
        myBlock.Batoutflows = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==battery.label])
        
        
        Q_max = battery.q_max
        def chargePowerBatteryRule(model, t):
            return (sum(modelIn.flow[i, o, t] for (i, o) in myBlock.Batinflows) <= (model.bin_waterstorage[t]) * Q_max)
        
        
        def dischargePowerBatteryRule(model, t):
            return (sum(modelIn.flow[i, o, t] for (i, o) in myBlock.Batoutflows) <= (1-model.bin_waterstorage[t]) * Q_max)
    

        
        
        
        if battery:
                
                myBlock.storageCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBatteryRule,
                    doc='Water Storage Charging')
                
                myBlock.storageDischarging = pyomo.Constraint(
                    myBlock.t,
                    rule=dischargePowerBatteryRule,
                    doc='Water Storage Discharging')
        
           
            
        
        return modelIn  
               

