# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 08:13:18 2022

@author: Matthias Maldet
"""

import pyomo.environ as po
import pyomo.core as pyomo
import numpy as np
#import pyomo_rules

def potablewater_limit(*args, **kwargs):
    
    #Vehicle Objects
    electrolysis = kwargs.get("electrolysis", 0)
    waterdemand = kwargs.get("waterdemand", 0)
    
    limit = kwargs.get("waterlimit", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlockWaterlimit', myBlock)
        
        def waterlimitRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if ((i.label==str(electrolysis.sector_loss) and o.label==electrolysis.label)
                                                                               or (i.label==waterdemand.label and o.label==str(waterdemand.sector_waterdemand)))))
                                                                                <= limit[t]) 
        
    if electrolysis and waterdemand:
        
        myBlock.waterlimit = pyomo.Constraint(
            myBlock.t,
            rule=waterlimitRule,
            doc='WaterdemandLimit')
                
        
           
            
        
    return modelIn  


def potablewater_limitXElectrolysis(*args, **kwargs):
    
    #Vehicle Objects
    waterdemand = kwargs.get("waterdemand", 0)
    
    limit = kwargs.get("waterlimit", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlockWaterlimit', myBlock)
        
        def waterlimitRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if ((i.label==waterdemand.label and o.label==str(waterdemand.sector_waterdemand)))))
                                                                                <= limit[t]) 
        
    if waterdemand:
        
        myBlock.waterlimit = pyomo.Constraint(
            myBlock.t,
            rule=waterlimitRule,
            doc='WaterdemandLimit')
                
        
           
            
        
    return modelIn  
        
