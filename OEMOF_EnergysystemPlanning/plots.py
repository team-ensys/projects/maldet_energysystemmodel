# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 07:56:34 2021

@author: Matthias Maldet
"""

import pandas as pd
import os as os
#import shutil as shutil
#import sys as sys
import matplotlib.pyplot as plt
import matplotlib.sankey as msank
import numpy as np
import oemof.solph as solph
import pandas as pd


DPI = 300


def orientationSet(value):
    value+=2
    if(value>=2):
        value=-1
        
    return value

def getUnit(string):
    if('electricity' in string.lower() or 'heat' in string.lower() or 'hotwater' in string.lower() or 'gas' in string.lower() or 'cooling' in string.lower() or 'electricvehicle' in string.lower()):
        return 'kWh'
    elif('waste' in string.lower()):
        return 'kg'
    elif('watersector' in string.lower() or 'sludge' in string.lower() or 'sewage' in string.lower() or 'hydrogensector' in string.lower() or 'biofuel' in string.lower() or 'fuelcell' in string.lower() or 'petrol' in string.lower() or 'h2' in string.lower()):
        return 'm³'
    elif('mobility' in string.lower()):
        return 'km'
    else:
        return ' '


def create_folders(scenario, consumers):
    #plotting the results
    #create the output folders
    direc = scenario + '/plots'
    createOutSectorFolders(direc, 'electricity', consumers)
    createOutSectorFolders(direc, 'heat', consumers)
    createOutSectorFolders(direc, 'gas', consumers)
    createOutSectorFolders(direc, 'waste', consumers)
    createOutSectorFolders(direc, 'water', consumers)
    createOutSectorFolders(direc, 'greywater', consumers)
    createOutSectorFolders(direc, 'waterdemandTotal', consumers)
    createOutSectorFolders(direc, 'cooling', consumers)
    createOutSectorFolders(direc, 'hydrogen', consumers)
    createOutSectorFolders(direc, 'transport', consumers)
    createOutSectorFolders(direc, 'emissions', consumers)
    createOutCostFolders(direc, consumers)

#Create Folder
def createOutSectorFolders(resultdirectory, foldername, consumers):
    plotDirectory = os.path.join(resultdirectory, foldername)
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'single_timerows')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    for consumer in consumers:
        plotDirectory = os.path.join(resultdirectory, foldername, 'single_timerows', str(consumer.label))
        if not os.path.exists(plotDirectory):
            os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'in_out_share')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'in_out_timerows')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'anual_load_curve')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    for consumer in consumers:
        plotDirectory = os.path.join(resultdirectory, foldername, 'anual_load_curve', str(consumer.label))
        if not os.path.exists(plotDirectory):
            os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'sankey')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
#Create Folder
def createOutCostFolders(resultdirectory, consumers):
    plotDirectory = os.path.join(resultdirectory, 'costs')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    for consumer in consumers:
        plotDirectory = os.path.join(resultdirectory, 'costs', str(consumer.label))
        if not os.path.exists(plotDirectory):
            os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, 'costs', str(consumer.label), 'servicecosts')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        

            
    plotDirectory = os.path.join(resultdirectory, 'costs', str(consumer.label), 'costs')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, 'costs', str(consumer.label), 'costsXgrid')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, 'costs', str(consumer.label), 'servicecostsXgrid')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)


def pie_OutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)
    unit = kwargs.get("unit", "kWh")
    factor = kwargs.get("factor", 1)

    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if(sectorname in str(element.sector_in)):
                                    plotcomponents.append(element)
                                
                            if hasattr(element, 'sector_loss'):
                                if(sectorname in str(element.sector_loss)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_inAdditional'):
                                if(sectorname in str(element.sector_inAdditional)):
                                    plotcomponents.append(element)
                                
                                    

                    else:
                        if hasattr(component, 'sector_in'):
                            if(sectorname in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if(sectorname in str(component.sector_loss)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_inAdditional'):
                                if(sectorname in str(component.sector_inAdditional)):
                                    plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    

                    
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            

        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][0])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    values.append(elementValue/totalOutput)
                    

                    label = element.label.split('_')[0] + ', ' + str(round(elementValue/factor, 2)) + ' ' + unit
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 4)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = name + ' Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/' + folder + '/in_out_share/'
                filename = consumer.label + '_' + name + 'Out.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key]/factor, 2)) + ' ' + unit
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 4)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = name + ' Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/' + folder + '/in_out_share/'
        filename =  'overall_' + name + 'elecOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()      
        
        
def pie_InShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)
    unit = kwargs.get("unit", "kWh")
    factor = kwargs.get("factor", 1)
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if(sectorname in str(element.sector_out)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_emissions'):
                                if(sectorname in str(element.sector_emissions)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if(sectorname in str(component.sector_out)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_emissions'):
                                if(sectorname in str(component.sector_emissions)):
                                    plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][1])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                    
                if(totalOutput==0):
                    totalOutput=1
            
                
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][1])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue/factor, 2)) + ' ' + unit
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 4)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle =  name + ' Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/' + folder + '/in_out_share/'
                filename = consumer.label + '_' + name + 'In.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()  
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key]/factor, 2)) + ' ' + unit
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 4)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = name + ' Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/' + folder + '/in_out_share/'
        filename = 'overall' + '_' + name + 'In.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()


def bar_OutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)
    unit = kwargs.get("unit", "kWh")
    factor = kwargs.get("factor", 1)
    
    
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if(sectorname in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if(sectorname in str(element.sector_loss)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_inAdditional'):
                                if(sectorname in str(element.sector_inAdditional)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if(sectorname in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if(sectorname in str(component.sector_loss)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_inAdditional'):
                                if(sectorname in str(component.sector_inAdditional)):
                                    plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][0])):
                                del printElement[col]
                    
                    if(len(printElement.filter(regex=sectorname).groupby([lambda x : x.month]).sum().values)<12):
                        return 0
                    
                    if(round(printElement.filter(regex=sectorname).groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        listAppend=[]
                        dataframe = printElement.filter(regex=sectorname).groupby([lambda x : x.month]).sum().values
                        for i in dataframe:
                            listAppend.append(i[0])
                            
                         
                        df[str(element.label.split('_')[0])] = listAppend
                        colors.append(element.color)
                        
                        
                figtitle = name + ' Output: ' + consumer.label
                filesave = scenario + '/plots/' + folder + '/in_out_timerows/'
                filename = consumer.label + '_' + name + 'Out.png'
                        
                if df.empty:
                    continue
                df=df/factor
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel(name + ' in ' + unit)
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = name + ' Output Overall'
        filesave = scenario + '/plots/' + folder + '/in_out_timerows/'
        filename = 'overall' + '_' + name + 'Out.png'
        
        if dfTotal.empty:
            return 0
        #dfTotal=dfTotal/factor
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel(name + ' in ' + unit)
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()  
        
        
def bar_InShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)
    unit = kwargs.get("unit", "kWh")
    factor = kwargs.get("factor", 1)
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if(sectorname in str(element.sector_out)):
                                    plotcomponents.append(element)
                                    
                            if hasattr(element, 'sector_emissions'):
                                if(sectorname in str(element.sector_emissions)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if(sectorname in str(component.sector_out)):
                                plotcomponents.append(component)
                                
                        if hasattr(component, 'sector_emissions'):
                                if(sectorname in str(component.sector_emissions)):
                                    plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][1])):
                                del printElement[col]
                    
                    if(len(printElement.filter(regex=sectorname).groupby([lambda x : x.month]).sum().values)<12):
                        return 0
                    
                    if(round(printElement.filter(regex=sectorname).groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        listAppend=[]
                        dataframe = printElement.filter(regex=sectorname).groupby([lambda x : x.month]).sum().values
                        for i in dataframe:
                            listAppend.append(i[0])
                        df[str(element.label.split('_')[0])] = listAppend
                        colors.append(element.color)
                        
                        
                figtitle = name + ' Input: ' + consumer.label
                filesave = scenario + '/plots/' + folder + '/in_out_timerows/'
                filename = consumer.label + '_' + name + 'In.png'
                        
                if df.empty:
                    continue
                df=df/factor
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel(name + ' in ' + unit)
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = name + ' Input Overall'
        filesave = scenario + '/plots/' + folder + '/in_out_timerows/'
        filename = 'overall' + '_' + name + 'In.png'
        
        if dfTotal.empty:
            return 0
        #dfTotal=dfTotal/factor
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel(name + ' in ' + unit)
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()
        
        
def pie_transportShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_drive'):
                                if('Mobilitysector' in str(element.sector_drive)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_drive'):
                            if('Mobilitysector' in str(component.sector_drive)):
                                plotcomponents.append(component)
               

                for element in plotcomponents:
                    printElement = solph.views.node(results, element.labelDrive).get("sequences")
                    elementValue=(printElement.filter(regex='Mobilitysector')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
            
                if(totalOutput==0):
                    totalOutput=1
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.labelDrive).get("sequences")
                    elementValue=(printElement.filter(regex='Mobilitysector')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' km'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 4)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Transport Share: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/transport/in_out_share/'
                filename = consumer.label + '_transportShare.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                
        if(outputAll==0):
            outputAll=1
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' km'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 4)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Transport Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/transport/in_out_share/'
        filename =  'overall_transportOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
        
        
def bar_transportShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_drive'):
                                if('Mobilitysector' in str(element.sector_drive)):
                                    plotcomponents.append(element)
                            
                    else:
                        if hasattr(component, 'sector_drive'):
                            if('Mobilitysector' in str(component.sector_drive)):
                                plotcomponents.append(component)
                        
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.labelDrive).get("sequences")
                    
                    if(len(printElement.filter(regex='Mobility').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Mobilitysector' in str(col[0][0])):
                                del printElement[col]
                    
                    if(len(printElement.filter(regex='Mobilitysector').groupby([lambda x : x.month]).sum().values)<12):
                        return 0
                    
                    if(round(printElement.filter(regex='Mobilitysector').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        listAppend=[]
                        dataframe = printElement.filter(regex='Mobilitysector').groupby([lambda x : x.month]).sum().values
                        for i in dataframe:
                            listAppend.append(i[0])
                        df[str(element.labelDrive.split('_')[0])] = listAppend
                        colors.append(element.color)
                        
                        
                figtitle = 'Transport: ' + consumer.label
                filesave = scenario + '/plots/transport/in_out_timerows/'
                filename = consumer.label + '_transportOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Distance in km')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Transport Overall'
        filesave = scenario + '/plots/transport/in_out_timerows/'
        filename = 'overall_transportOut.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Distance in km')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()  
        
 
def sankey(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)
    color = kwargs.get("color", 'dodgerblue')
    
    outputAll = 0
    inputAll=0
    totalDictOut={}
    totalOrientationDictOut={}
    totalDictIn={}
    totalOrientationDictIn={}
    totalValues=[]
    totalLabels=[]
    totalOrientations=[]
    orientation=-1
    
    outTotal=0
    inTotal=0
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            
            mylabels=[]
            values=[]
            orientations=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if(sectorname in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if(sectorname in str(element.sector_loss)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_in'):
                            if(sectorname in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if(sectorname in str(component.sector_loss)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
                    
                if(outputAll==0):
                    outputAll=1
            
                outTotal=totalOutput
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][0])):
                                del printElement[col]
                     
                                
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    
                    if(round(elementValue/totalOutput, 2)!=0): 
                
                        values.append(-round(elementValue/totalOutput, 4))
                        label = str(element.labelSankey.split('_')[0]) + '\n' + str(round(100*elementValue/totalOutput, 1)) + "%"
                        mylabels.append(label)
                        orientation=orientationSet(orientation)
                        orientations.append(orientation)
                        
                    if(element.label.split('_')[0] in totalDictOut):
                        totalDictOut[element.labelSankey.split('_')[0]] = totalDictOut[element.label.split('_')[0]] + elementValue
                    else:
                        totalDictOut[element.labelSankey.split('_')[0]]=elementValue
                        totalOrientationDictOut[element.labelSankey.split('_')[0]] = orientation
                    
             

            plotcomponents=[]
    
            totalOutput=0
                    
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if(sectorname in str(element.sector_out)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_emissions'):
                                if(sectorname in str(element.sector_emissions)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if(sectorname in str(component.sector_out)):
                                plotcomponents.append(component)
                        
                        if hasattr(component, 'sector_emissions'):
                                if(sectorname in str(component.sector_emissions)):
                                    plotcomponents.append(component)

              
            for element in plotcomponents:
                printElement = solph.views.node(results, element.label).get("sequences")
                    
                if(len(printElement.filter(regex=sectorname).columns) > 1):
                    for col in printElement.columns:
                        if not( sectorname in str(col[0][1])):
                            del printElement[col]
                                
                    
                elementValue=(printElement.filter(regex=sectorname)).values.sum()
                totalOutput+=elementValue
                inputAll+=elementValue
                    
                    
            if(totalOutput==0):
                totalOutput=1
                
            if(inputAll==0):
                inputAll=1
            
            
            inTotal=totalOutput
            
            for element in plotcomponents:
                printElement = solph.views.node(results, element.label).get("sequences")
                    
                if(len(printElement.filter(regex=sectorname).columns) > 1):
                    for col in printElement.columns:
                        if not( sectorname in str(col[0][1])):
                            del printElement[col]
                                
                   
                elementValue=(printElement.filter(regex=sectorname)).values.sum()
                
                if(round(elementValue/totalOutput, 2)!=0): 
                
                    values.append(round(elementValue/totalOutput, 4))
                    label = str(element.labelSankey.split('_')[0]) + '\n' + str(round(100*elementValue/totalOutput, 1)) + "%"
                    mylabels.append(label)
                    orientation=orientationSet(orientation)
                    orientations.append(orientation)
                 
                
                 
                if(element.label.split('_')[0] in totalDictIn):
                        totalDictIn[element.labelSankey.split('_')[0]] = totalDictIn[element.label.split('_')[0]] + elementValue
                else:
                        totalDictIn[element.labelSankey.split('_')[0]]=elementValue
                        totalOrientationDictIn[element.labelSankey.split('_')[0]] = orientation
            
                
            if(len(values)!=0): 
                filesave = scenario + '/plots/' + folder + '/sankey/'
                filename = consumer.label + '_' + name + 'Sankey.png'
                
                patchname = name + '\n' + consumer.label
            
                sankey = msank.Sankey(unit=None)
                sankey.add(flows=values, labels=mylabels, orientations=orientations,  patchlabel=patchname, facecolor=color)
                diagrams = sankey.finish()
    
                for i in range(0, len(diagrams[0].texts)):
                    diagrams[0].texts[i].set_label(mylabels[i])
                    diagrams[0].texts[i].set_fontsize(3)
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf() 
            
        #Total Values
        for key in totalDictOut:
            if(round(totalDictOut[key]/outputAll, 2)!=0):
                label = key + '\n' + str(round(100*totalDictOut[key]/outputAll, 2)) + '%'
                totalLabels.append(label)
                totalValues.append(-totalDictOut[key]/outputAll)
                totalOrientations.append(totalOrientationDictOut[key])
                
        for key in totalDictIn:    
            if(round(totalDictIn[key]/inputAll, 2)!=0):
                label = key + '\n' + str(round(100*totalDictIn[key]/inputAll, 2)) + '%'
                totalLabels.append(label)
                totalValues.append(totalDictIn[key]/inputAll)
                totalOrientations.append(totalOrientationDictIn[key])
                
        if(len(totalValues)!=0):        
            filesave = scenario + '/plots/' + folder + '/sankey/'
            filename =  'Overall_' + name + 'Sankey.png'
                
            patchname = name
            
            
            
            sankey = msank.Sankey(unit=None)
            sankey.add(flows=totalValues, labels=totalLabels, orientations=totalOrientations,  patchlabel=patchname, facecolor=color)
            diagrams = sankey.finish()
    
            for i in range(0, len(diagrams[0].texts)):
                diagrams[0].texts[i].set_label(totalLabels[i])
                diagrams[0].texts[i].set_fontsize(3)
    
            if os.path.isfile(os.path.join(filesave, filename)):
                os.remove(os.path.join(filesave, filename))
            plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
            plt.clf()
 
def sankeyTransport(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')

    
    outputAll = 0
    inputAll=0
    totalDictOut={}
    totalOrientationDictOut={}
    totalDictIn={}
    totalOrientationDictIn={}
    totalValues=[]
    totalLabels=[]
    totalOrientations=[]
    orientation=-1
    
    outTotal=0
    inTotal=0
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            
            mylabels=[]
            values=[]
            orientations=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_drive'):
                                if('Mobilitysector' in str(element.sector_drive)):
                                    plotcomponents.append(element)


                    else:
                        if hasattr(component, 'sector_drive'):
                            if('Mobilitysector' in str(component.sector_drive)):
                                plotcomponents.append(component)


                
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.labelDrive).get("sequences")
                    
                    if(len(printElement.filter(regex='Mobilitysector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Mobilitysector' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Mobilitysector')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
                    
                if(outputAll==0):
                    outputAll=1
            
                outTotal=totalOutput
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.labelDrive).get("sequences")
                    
                    if(len(printElement.filter(regex='Mobilitysector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Mobilitysector' in str(col[0][0])):
                                del printElement[col]
                     
                                
                    
                    elementValue=(printElement.filter(regex='Mobilitysector')).values.sum()
                    
                    if(round(elementValue/totalOutput, 4)!=0): 
                
                        values.append(round(elementValue/totalOutput, 4))
                        label = str(element.labelSankey.split('_')[0]) + '\n' + str(round(100*elementValue/totalOutput, 1)) + "%"
                        mylabels.append(label)
                        orientation=orientationSet(orientation)
                        orientations.append(orientation)
                        
                    if(element.label.split('_')[0] in totalDictOut):
                        totalDictOut[element.labelSankey.split('_')[0]] = totalDictOut[element.labelSankey.split('_')[0]] + elementValue
                    else:
                        totalDictOut[element.labelSankey.split('_')[0]]=elementValue
                        totalOrientationDictOut[element.labelSankey.split('_')[0]] = orientation
                    
             

            values.append(-1)
            label = 'Drive\n100%'
            mylabels.append(label)
            orientation=orientationSet(orientation)
            orientations.append(orientation)
                
            if(len(values)!=0): 
                filesave = scenario + '/plots/' + 'transport' + '/sankey/'
                filename = consumer.label + '_' + 'Transport' + 'Sankey.png'
                
                patchname = 'Transport' + '\n' + consumer.label
            
                sankey = msank.Sankey(unit=None)
                sankey.add(flows=values, labels=mylabels, orientations=orientations,  patchlabel=patchname, facecolor='violet')
                diagrams = sankey.finish()
    
                for i in range(0, len(diagrams[0].texts)):
                    diagrams[0].texts[i].set_label(mylabels[i])
                    diagrams[0].texts[i].set_fontsize(3)
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf() 
            
        #Total Values
        for key in totalDictOut:
            if(round(totalDictOut[key]/outputAll, 4)!=0):
                label = key + '\n' + str(round(100*totalDictOut[key]/outputAll, 2)) + '%'
                totalLabels.append(label)
                totalValues.append(totalDictOut[key]/outputAll)
                totalOrientations.append(totalOrientationDictOut[key])
                

                
                
        totalValues.append(-1)
        label = 'Drive\n100%'
        totalLabels.append(label)
        orientation=orientationSet(orientation)
        totalOrientations.append(orientation)
        
        
                
        if(len(totalValues)!=0):        
            filesave = scenario + '/plots/' + 'transport' + '/sankey/'
            filename =  'Overall_' + 'Transport_' + 'Sankey.png'
                
            patchname = 'Transport'
            
            
            
            sankey = msank.Sankey(unit=None)
            sankey.add(flows=totalValues, labels=totalLabels, orientations=totalOrientations,  patchlabel=patchname, facecolor='violet')
            diagrams = sankey.finish()
    
            for i in range(0, len(diagrams[0].texts)):
                diagrams[0].texts[i].set_label(totalLabels[i])
                diagrams[0].texts[i].set_fontsize(3)
    
            if os.path.isfile(os.path.join(filesave, filename)):
                os.remove(os.path.join(filesave, filename))
            plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
            plt.clf()
        

def anualLoadCurve(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    timesteps = kwargs.get("timerange", 1)
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)

    colors=['blue', 'red', 'green', 'brown', 'slategrey', 'indigo', 'lime', 'aqua', 'black']
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if(name.lower() in str(type(element)).split('.')[0]):
                                plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if(sectorname in str(element.sector_loss)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_emissions'):
                                if(sectorname in str(element.sector_emissions)):
                                    plotcomponents.append(element)

                    else:
                        if(name.lower() in str(type(component)).split('.')[0]):
                                plotcomponents.append(component)
                                
                        if hasattr(component, 'sector_loss'):
                                if(sectorname in str(component.sector_loss)):
                                    plotcomponents.append(component)
                        if hasattr(component, 'sector_emissions'):
                                if(sectorname in str(component.sector_emissions)):
                                    plotcomponents.append(component)
                        

                
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    #printElement = printElement.resample('24H').mean()
                    printElement.sort_values(by=printElement.columns[0],  ascending=False, inplace=True)
                    printElement.reset_index(drop=True, inplace=True)
                    printElement = printElement.div(timesteps)
                    
                    
                    
                    #Plotting
                   
                    
                    numberFlows=len(printElement.columns)
                    
                    days = len(printElement.index)
                    x = list(range(0, days))
                    
                    figtitle = element.label
                    
                    fig, axs = plt.subplots(numberFlows+1)
                    fig.suptitle(figtitle)
                    
                    

                    for i in range(0, numberFlows):

                        df = printElement.iloc[:, i]
                        dfVal = df.values
                        
                        colorlabel = colors[i]
                        
                        #axs[i].plot(x, dfVal)
                        axs[i].plot(x, dfVal, colorlabel)
                        
                        #Label Definition
                        colList = str(printElement.columns[i]).split(',')
                        label=''
                        
                        if('content' in colList[2]):
                            label='Storage_Content'
                            label += ' in ' + getUnit(str(element.sector_in))
                            #Nach sector_in? --> was spricht dagegen?
                        elif(element.label.lower() in colList[0].lower()):
                            label = str(colList[1]).split('_')[2][:-2]+ '_Output'
                            label += ' in ' + getUnit(str(colList[1]).split('_')[2][:-2])
                        else:
                            label = str(colList[0]).split('_')[2][:-1]+ '_Input'
                            label += ' in ' + getUnit(str(colList[0]).split('_')[2][:-1])
                        
                        
                        axs[i].set_ylabel(label, fontsize=3)
                        
                        ymax=max(dfVal)*1.1
                        xmax=len(dfVal)
                        
                        axs[i].set_xlim([0, xmax])
                        axs[i].set_ylim([0, ymax])


                    axs[numberFlows-1].set_xlabel(' t in hours', fontsize=5)
                    #ax.set_xlim(0, 366)
                    
                    
                    fig.subplots_adjust(hspace=0.5)
                    fig.delaxes(axs[numberFlows])
    
                    filesave = os.path.join(scenario, 'plots')
                    filesave = os.path.join(filesave, folder)
                    filesave = os.path.join(filesave, 'anual_load_curve')
                    filesave = os.path.join(filesave, consumer.label)
                    
                    filename =  element.label + '.png'

                    if os.path.isfile(os.path.join(filesave, filename)):
                        os.remove(os.path.join(filesave, filename))

                    fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                    fig.clf()


def timerows(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    timesteps = kwargs.get("timerange", 1)
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)

    colors=['blue', 'red', 'green', 'brown', 'slategrey', 'indigo', 'lime', 'aqua', 'black']
    
    if(results):
        
        for consumer in consumers:
            plotcomponents=[]
    
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if(name.lower() in str(type(element)).split('.')[0]):
                                plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if(sectorname in str(element.sector_loss)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_emissions'):
                                if(sectorname in str(element.sector_emissions)):
                                    plotcomponents.append(element)

                    else:
                        if(name.lower() in str(type(component)).split('.')[0]):
                                plotcomponents.append(component)
                                
                        if hasattr(component, 'sector_loss'):
                                if(sectorname in str(component.sector_loss)):
                                    plotcomponents.append(component)
                        if hasattr(component, 'sector_emissions'):
                                if(sectorname in str(component.sector_emissions)):
                                    plotcomponents.append(component)
                        

                
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    printElement = printElement.resample('24H').mean()
                    printElement = printElement.div(timesteps)
                    
                    
                    
                    #Plotting
                   
                    
                    numberFlows=len(printElement.columns)
                    
                    days = len(printElement.index)
                    x = list(range(0, days))
                    
                    figtitle = element.label
                    
                    fig, axs = plt.subplots(numberFlows+1)
                    fig.suptitle(figtitle)
                    
                    

                    for i in range(0, numberFlows):

                        df = printElement.iloc[:, i]
                        dfVal = df.values
                        
                        colorlabel = colors[i]
                        
                        axs[i].plot(x, dfVal, colorlabel)
                        
                        #Label Definition
                        colList = str(printElement.columns[i]).split(',')
                        label=''
                        
                        if('content' in colList[2]):
                            label='Storage_Content'
                            label += ' in ' + getUnit(str(element.sector_in))
                            #Nach sector_in? --> was spricht dagegen?
                        elif(element.label.lower() in colList[0].lower()):
                            label = str(colList[1]).split('_')[2][:-2]+ '_Output'
                            label += ' in ' + getUnit(str(colList[1]).split('_')[2][:-2])
                        else:
                            label = str(colList[0]).split('_')[2][:-1]+ '_Input'
                            label += ' in ' + getUnit(str(colList[0]).split('_')[2][:-1])
                        
                        
                        axs[i].set_ylabel(label, fontsize=3)
                        
                        ymax=max(dfVal)*1.1
                        xmax=len(dfVal)
                        
                        axs[i].set_xlim([0, xmax])
                        axs[i].set_ylim([0, ymax])


                    axs[numberFlows-1].set_xlabel(' t in days', fontsize=5)
                    #ax.set_xlim(0, 366)
                    
                    
                    fig.subplots_adjust(hspace=0.5)
                    fig.delaxes(axs[numberFlows])
    
                    filesave = scenario + '/plots/' + folder + '/single_timerows/' + consumer.label
                    filename =  element.label + '.png'

                    if os.path.isfile(os.path.join(filesave, filename)):
                        os.remove(os.path.join(filesave, filename))

                    fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                    fig.clf()
                    
                    
def plot_costs_pie(*args, **kwargs):

    costs  = kwargs.get("costs", 0)
    scenario = kwargs.get('scenario', ' ')
    folder = kwargs.get('folder', ' ')
    subfolder = kwargs.get('subfolder', ' ')
    sector = kwargs.get('sectorname', ' ')
    consumer = kwargs.get('consumer', ' ')
    elements = kwargs.get('elements', 0)
    plotunit = kwargs.get('unit', '€')
    
    if('€' in plotunit):
        string = 'Costs'
    else:
        string='Output'
    
    
    figtitle = string + '_' + sector + '_' + consumer.label
    
    values={}
    valuesTemp={}
    totalValue=1
    columnNames=[]
    colors=[]
    
    keys=list(costs.keys())    
    
    for key in keys:
        if('total' in key.lower()):
            totalValue=costs[key]
        else:
            valuesTemp[key] = costs[key]
            
    
            
    keys=list(valuesTemp.keys())
            
    for key in keys:
        if(round(valuesTemp[key]/totalValue*100, 2)>0):
            values[key] = valuesTemp[key]/totalValue
            
            label = key.split('_')[0] + ' ' + str(round(values[key]*totalValue, 2)) + ' ' + plotunit
            columnNames.append(label)
            
            if(elements):
                for comp in elements:
                    if(key.lower() in comp.label.lower()):
                        colors.append(comp.color)
            else:
                colors = ['red', 'blue', 'green', 'deepskyblue', 'darkcyan', 'brown', 'slategray', 'gold', 'orange']


    patches, texts = plt.pie(values.values(), colors=colors)
    plt.legend(patches, columnNames, bbox_to_anchor=(1.2, 1.1))
    plt.suptitle(figtitle)
    
    filesave = scenario + '/plots/' + folder + '/' + consumer.label + '/' + subfolder
    filename =  sector + '.png'

    if os.path.isfile(os.path.join(filesave, filename)):
        os.remove(os.path.join(filesave, filename))
    plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
    plt.clf()  
    
    
def plot_costs_bar(*args, **kwargs):

    costs  = kwargs.get("costs", 0)
    scenario = kwargs.get('scenario', ' ')
    folder = kwargs.get('folder', ' ')
    subfolder = kwargs.get('subfolder', ' ')
    sector = kwargs.get('sectorname', ' ')
    consumer = kwargs.get('consumer', ' ')
    elements = kwargs.get('elements', 0)
    plotunit = kwargs.get('unit', '€')
    
    if('€' in plotunit):
        string = 'Costs'
    else:
        string='Output'
    
    figtitle = string + '_' + sector + '_' + consumer.label
    
    values={}
    valuesTemp={}
    totalValue=1
    columnNames=[]
    colors=[]
    legendlabels=[]
    
    keys=list(costs.keys())    
    
    for key in keys:
        if('total' in key.lower()):
            totalValue=costs[key]
        else:
            valuesTemp[key] = costs[key]
            
    
            
    keys=list(valuesTemp.keys())
            
    for key in keys:
        if(abs(round(valuesTemp[key]/totalValue*100, 2))>0):
            values[key] = valuesTemp[key]
            
            label = key.split('_')[0]
            columnNames.append(label)
            
            if(elements):
                for comp in elements:
                    if(key.lower() in comp.label.lower()):
                        colors.append(comp.color)
            else:
                colors = ['red', 'blue', 'green', 'deepskyblue', 'darkcyan', 'brown', 'slategray', 'gold', 'orange']


    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])

    ax.bar(columnNames, list(values.values()), color=colors)
    
    if('€' in plotunit):
        string = 'Costs'
    else:
        string='Output'
    
    ax.set_ylabel(string + ' in ' + plotunit) 
    
    #ax.set_xticks([])
    ax.xaxis.set_tick_params(labelsize=5)
    
    
    
    for value in values.values():
        legendlabels.append(str(round(value, 2)) + ' ' + plotunit)
        
    
    
    # Legend with Values
    ax.legend(labels=legendlabels, handles=ax.patches)

    

    
    ax.set_title(figtitle)
    ax.axhline(y=0, color='black')
    
    filesave = scenario + '/plots/' + folder + '/' + consumer.label + '/' + subfolder
    filename =  sector + '.png'

    if os.path.isfile(os.path.join(filesave, filename)):
        os.remove(os.path.join(filesave, filename))
    plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
    plt.clf()  


def plot_totalcosts_pie(*args, **kwargs):

    costs  = kwargs.get("costs", 0)
    scenario = kwargs.get('scenario', ' ')
    folder = kwargs.get('folder', ' ')
    subfolder = kwargs.get('subfolder', ' ')
    consumer = kwargs.get('consumer', ' ')
    nameAdd = kwargs.get('name', '')
    plotunit = kwargs.get('unit', '€')
    
    if('€' in plotunit):
        string = 'Costs'
    else:
        string='Output'

    
    figtitle = 'Total_' + string + '_' + consumer.label
    
    values={}
    valuesTemp={}
    totalValue=1
    columnNames=[]
    colors = ['red', 'green', 'slategrey', 'brown', 'blue', 'aqua', 'orange', 'gold', 'darkkhaki']
    
    keys=list(costs.keys())    
    
    for key in keys:
        if('total' in key.lower()):
            totalValue=costs[key]
        else:
            valuesTemp[key] = costs[key]
            
    
            
    keys=list(valuesTemp.keys())
            
    for key in keys:
        
        if(round(valuesTemp[key], 2)>0):
            values[key] = valuesTemp[key]/totalValue
            
            label = key + ' ' + str(round(values[key]*totalValue, 2)) + ' ' + plotunit
            columnNames.append(label)
            
           
                


    patches, texts = plt.pie(values.values(), colors=colors)
    plt.legend(patches, columnNames, bbox_to_anchor=(1.2, 1.1))
    plt.suptitle(figtitle)
    
    filesave = scenario + '/plots/' + folder + '/' + consumer.label + '/' + subfolder
    filename =  'total' + nameAdd + '.png'

    if os.path.isfile(os.path.join(filesave, filename)):
        os.remove(os.path.join(filesave, filename))
    plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
    plt.clf()  
    
    
def plot_totalcosts_bar(*args, **kwargs):

    costs  = kwargs.get("costs", 0)
    scenario = kwargs.get('scenario', ' ')
    folder = kwargs.get('folder', ' ')
    subfolder = kwargs.get('subfolder', ' ')
    consumer = kwargs.get('consumer', ' ')
    plotunit = kwargs.get('unit', '€')

    
    if('€' in plotunit):
        string = 'Costs'
    else:
        string='Output'

    
    figtitle = 'Total_' + string + '_' + consumer.label
    
    values={}
    valuesTemp={}
    columnNames=[]
    colors = ['red', 'green', 'slategrey', 'brown', 'blue', 'aqua', 'orange', 'gold', 'darkkhaki']
    legendlabels=[]
    
    keys=list(costs.keys())    
    
    for key in keys:
        
        valuesTemp[key] = costs[key]
            
    
            
    keys=list(valuesTemp.keys())
            
    for key in keys:
        
        
        values[key] = valuesTemp[key]
            
        label = key
        columnNames.append(label)
            
           
    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])

    ax.bar(columnNames, list(values.values()), color=colors)
    
    if('€' in plotunit):
        string = 'Costs'
    else:
        string='Output'
    
    ax.set_ylabel(string + ' in ' + plotunit) 
    
    #ax.set_xticks([])
    ax.xaxis.set_tick_params(labelsize=5)
    
    
    
    for value in values.values():
        legendlabels.append(str(round(value, 2)) + ' ' + plotunit)
        
    
    
    # Legend with Values
    #ax.legend(labels=legendlabels, handles=ax.patches, loc="upper right")

    

    
    ax.set_title(figtitle)
    ax.axhline(y=0, color='black')
    

    
    filesave = scenario + '/plots/' + folder + '/' + consumer.label + '/' + subfolder
    filename =  'total_bar' + '.png'

    if os.path.isfile(os.path.join(filesave, filename)):
        os.remove(os.path.join(filesave, filename))
    plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
    plt.clf()  

