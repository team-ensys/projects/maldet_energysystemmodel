# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 15:51:57 2021

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np
import source_emissions as em


class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Gasgrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Gasgrid_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color='burlywood'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_gasGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
            
        gasGrid = solph.Source(label=self.label, outputs={self.sector_out: q_gasGrid})
        
        return gasGrid
    
    
class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Gasgrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Gas\nGrid_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color = 'burlywood'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_gasGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            gasGrid = solph.Source(label=self.label, outputs={self.sector_out: q_gasGrid})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            gasGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_gasGrid, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return gasGrid


class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_fix = 36
        deltaT=1
        position='Consumer_1'
        timesteps=8760

        
        self.P= kwargs.get("P", P_connection)
        self.costs_fixed = kwargs.get("costs_fixed", C_fix)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Gasgridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Gasgrid\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='burlywood'
        self.color = kwargs.get("color", color)

        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_gasGridpurchase = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_out)
            
        gasGrid = solph.Source(label=self.label, outputs={self.sector_out: q_gasGridpurchase})
        
        return gasGrid     

    
    
class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_in = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Gasdemand_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Gas\nDemand_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='yellow'
            self.color = kwargs.get("color", color)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_gasDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            gasDemand = solph.Sink(label=self.label, inputs={self.sector_in: q_gasDemand})
            
            return gasDemand
        
        
class Gasboiler():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.95
            costsIn=0
            costsOut=0.001
            PowerInLimit = 21
            PowerOutLimit = 21
            deltaT=1
            emissions=0.201
            maxEmissions=1000000
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Gasboiler_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Gas\nBoiler_' + self.position)
            color='darkcyan'
            self.color = kwargs.get("color", color)

        def component(self):
            q_gasBoilGas = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_gasBoilTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            if(self.emissions==0 or self.sector_emissions==0):
                gasBoiler = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBoilGas},
                    outputs={self.sector_out : q_gasBoilTh},
                    conversion_factors={self.sector_out : self.conversion_factor})
                
            else:
                m_gasBoilEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                gasBoiler = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBoilGas},
                    outputs={self.sector_out : q_gasBoilTh, self.sector_emissions : m_gasBoilEmissions},
                    conversion_factors={self.sector_out : self.conversion_factor, self.sector_emissions : self.emissions})
                
            
            return gasBoiler
 
class Blockheat():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.44
            costsIn=0
            costsOut=0.003
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.201
            maxEmissions=1000000
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'GasBlockheat_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Gas\nBlockheat_' + self.position)
            color='orange'
            self.color = kwargs.get("color", color)

        def component(self):
            q_gasBlockHeatGas = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_gasBlockHeatEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                blockHeat = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBlockHeatGas},
                    outputs={self.sector_out : q_gasBlockHeatEl},
                    conversion_factors={self.sector_out : self.conversion_factor})
                
            else:
                m_gasblockheatEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                blockHeat = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBlockHeatGas},
                    outputs={self.sector_out : q_gasBlockHeatEl, self.sector_emissions : m_gasblockheatEmissions},
                    conversion_factors={self.sector_out : self.conversion_factor, self.sector_emissions : self.emissions})
                
            return blockHeat
        
class CHP():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.01, 0.01]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.201
            maxEmissions=1000000
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'GasCHP_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Gas\nCHP_' + self.position)
            color='blue'
            self.color = kwargs.get("color", color)

        def component(self):
            q_gasCHPGas = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_gasCHPEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_gasCHPTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                gasCHP = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasCHPGas},
                    outputs={self.sector_out[0] : q_gasCHPEl, self.sector_out[1] : q_gasCHPTh},
                    conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1]})
                
            else:
                m_gasCHPEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                gasCHP = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasCHPGas},
                    outputs={self.sector_out[0] : q_gasCHPEl, self.sector_out[1] : q_gasCHPTh, self.sector_emissions : m_gasCHPEmissions},
                    conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1], self.sector_emissions : self.emissions})
            
            
            return gasCHP
        
class GasSale():
    
    def __init__(self, *args, **kwargs):
        P_max=1000000
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.P_max= kwargs.get("P_max", P_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Gassale_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Gas\nSale\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        self.max_gassale=kwargs.get("max_gassale", 1)
        self.gassale_limit=kwargs.get("gassale_limit", 1)
        
        revenuesGasArray = np.zeros(self.timesteps)
        
            
        self.revenues = kwargs.get("gas_price", revenuesGasArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        q_gassale = solph.Flow(min=0, max=1, nominal_value=self.P_max*self.max_gassale, variable_costs=self.revenues, summed_max=self.gassale_limit*self.timesteps/(self.timestepsTotal*self.P_max*self.max_gassale))
            
        gassale = solph.Sink(label=self.label, inputs={self.sector_in: q_gassale})
        
        return gassale
        
        
        
        