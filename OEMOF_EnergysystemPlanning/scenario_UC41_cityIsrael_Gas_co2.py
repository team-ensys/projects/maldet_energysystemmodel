# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 13:50:12 2021

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import get_costs as get_costs



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'scenario_UC4_1_NewEmPlot'

#Filename
filename = "input_data_UC4.xlsx"

#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 8784
timestepsTotal = 8784

#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


scenario = 'results/' + scenario

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal


#CO2 Price
co2Price = 0


#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)



#Consumer is a residential area with 50 households
consumer1 = Consumer.Consumer(timesteps=timesteps, filename=filename)
consumer1.addAllSectors()
consumer1.sector2system(my_energysystem)

#___________ ELECTRICITY ____________________

#Electricity Grid
#Grid Connection Power 5473*11kW=60203kW
#elgrid = el_comp.Grid(sector=consumer1.getElectricitySector(), P=30800, costs=consumer1.input_data['electricityCosts'], timesteps=timesteps)   
elgrid = el_comp.GridEmissions(sector=consumer1.getElectricitySector(), P=30800, costs=consumer1.input_data['electricityCosts'], timesteps=timesteps, emissions=0.6, sector_emissions=consumer1.getEmissionsSector())       
my_energysystem.add(elgrid.component())
consumer1.addComponent(elgrid)

#Decentral Generation Plant implemented as PV
#Rooftop Area 92000m²
pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), 
                          generation_timeseries=consumer1.input_data['elGen'], 
                          timesteps=timesteps, 
                          area_roof=92000, area_efficiency=0.15)
my_energysystem.add(pv.component())
consumer1.addComponent(pv)


#Heat Pump
#8/27 of the households with heatpumps --> 12600kW
heatpump = el_comp.Heatpump(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector(), 
                            conversion_timeseries=consumer1.input_data['copHP'], timesteps=timesteps,
                            P_in=12600, P_out=12600) 
my_energysystem.add(heatpump.component())
consumer1.addComponent(heatpump)

#Battery Storage
# 3% households with storages of 10kW --> sum 285kWh
battery = el_comp.Battery(sector_in=consumer1.getElectricitySector(), P_in=285, P_out=285, soc_max=285)
my_energysystem.add(battery.component())
consumer1.addComponent(battery)


#Seawater Source
#Input for Seawater --> Limit must be equal to the lmit for the desalination
seawater=water_comp.Seawatersource(sector=consumer1.getSeawaterSector(), V=1580)
my_energysystem.add(seawater.component())
consumer1.addComponent(seawater)


#Water Desalination
#Electricity Input: 11475kW Power --> Volume 1850m³
desalination = water_comp.Desalination(sector_in=consumer1.getSeawaterSector(), sector_out=consumer1.getPotablewaterSector(), sector_loss=consumer1.getElectricitySector(), V=1580)
my_energysystem.add(desalination.component())
consumer1.addComponent(desalination)



#Electricity Demand
#Differences in the Input Timeseries
elecdemand = el_comp.Demand(sector=consumer1.getElectricitySector(), demand_timeseries=consumer1.input_data['electricityDemand'], timesteps=timesteps)
my_energysystem.add(elecdemand.component())
consumer1.addComponent(elecdemand)

#Grid Sink - Assumption Same as for Grid Connection
elgridSink = el_comp.GridSink(sector=consumer1.getElectricitySector(), P=30800)        
my_energysystem.add(elgridSink.component())
consumer1.addComponent(elgridSink)



#___________ Heat ____________________

#Demand Heat
#Differences in the Input Timeseries
heatdemand = heat_comp.Demand(sector=consumer1.getHeatSector(), demand_timeseries=consumer1.input_data['heatDemand'], timesteps=timesteps)
my_energysystem.add(heatdemand.component())
consumer1.addComponent(heatdemand)

#Demand Hotwater
#Differences in the Input Timeseries
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer1.getHotwaterSector(), demand_timeseries=consumer1.input_data['hotwaterDemand'], timesteps=timesteps)
my_energysystem.add(hotwaterdemand.component())
consumer1.addComponent(hotwaterdemand)

#Hot Water Boiler
#Scale of Demand by factor 1575 --> scale of boiler by factor 1575 --> 1575*21kW=33075kW
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getHotwaterSector(), 
                                          sector_loss=consumer1.getPotablewaterSector(), 
                                          P_in=33075, P_out=33075)
my_energysystem.add(hotwaterBoiler.component())
consumer1.addComponent(hotwaterBoiler)


#Heat Storage
#Scaling by factor 1575 --> Pmax=11025kW, Vmax=473m³
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer1.getHeatSector(), P_in=11025, P_out=11025, volume_max=473)
my_energysystem.add(thermalStorage.component())
consumer1.addComponent(thermalStorage)



#___________ Waste ____________________

#Accruing
#Factor 2866 for more households (based on persons per hh) in input timeseries
wasteAccruing = waste_comp.Accruing(sector=consumer1.getWasteaccruingSector(), waste_timeseries=consumer1.input_data['waste'])
my_energysystem.add(wasteAccruing.component())
consumer1.addComponent(wasteAccruing)


#Waste Storage
#Factor 2866 for Storage Volume to have enough capacity for accruing waste
#Disposal only once a week
wasteStorage = waste_comp.WasteStorage(sector_in=consumer1.getWasteaccruingSector(), sector_out=consumer1.getWasteSector(), 
                                       disposal_periods=168, timesteps=timesteps, volume_max=1343)
my_energysystem.add(wasteStorage.component())
consumer1.addComponent(wasteStorage)

#Waste Depository
wasteDepository = waste_comp.WasteStorage(sector_in=consumer1.getWasteSector(), sector_out=consumer1.getWastedisposalSector(), 
                                       disposal_periods=0, timesteps=timesteps, volume_max=51580, label='WasteDepository', volume_start=0)
my_energysystem.add(wasteDepository.component())
consumer1.addComponent(wasteDepository)


#Waste Disposal
#Factor 3065 for Storage Volume to have enough disposal capacity for waste storage
wasteDisposal = waste_comp.Wastedisposal(sector=consumer1.getWastedisposalSector(), costs=consumer1.input_data['wasteCosts'], volume_max=1343, sector_emissions=consumer1.getEmissionsSector())        
my_energysystem.add(wasteDisposal.component())
consumer1.addComponent(wasteDisposal)

#Waste Combustion
#1343kW of Waste Combustion Plant are reserved for the city per hour --> for 168h: 367500kW
#Higher than max power --> Assumption 75000kW
wasteCombustion = waste_comp.Wastecombustion(sector_in=consumer1.getWastedisposalSector(), 
                                             sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()], 
                                             P_in=75000, P_out=75000, sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(wasteCombustion.component())
consumer1.addComponent(wasteCombustion)


# ----- Waste AD ---
#Waste to Biogas (Anaerobic Digestion)
#Same assumptions as for waste combustion regarding power
wasteBiogas = waste_comp.WasteBiogas(sector_in=consumer1.getWastedisposalSector(), sector_out=consumer1.getGasSector(),
                                     P_in=75000, P_out=75000)
my_energysystem.add(wasteBiogas.component())
consumer1.addComponent(wasteBiogas)



#___________ Water ____________________

#Water Demand
#Input Timeseries multiplied by 3992
waterDemand = water_comp.Waterdemand(sector_in=consumer1.getPotablewaterSector(), sector_out=consumer1.getSewageSector(), demand=consumer1.input_data['waterDemand'], timesteps=timesteps)
my_energysystem.add(waterDemand.component())
consumer1.addComponent(waterDemand)


#Water Storage
#Multiplication with 3992 of the storage dimensions --> Maximum volume 5988m³ , maximum flow 5988m³/h
waterstorage = water_comp.WaterStorage(sector_in=consumer1.getPotablewaterSector(), volume_max=5988, Q_max=5988)
my_energysystem.add(waterstorage.component())
consumer1.addComponent(waterstorage)


#Sewage Treatment Plant
#No changes required here
sewageTreatmentPlant = water_comp.SewageTreatment(sector_in=consumer1.getSewageSector(), sector_loss=consumer1.getElectricitySector(), 
                                                  sector_out=[consumer1.getPotablewaterSector(), consumer1.getSludgeSector(), consumer1.getHeatSector()], 
                                                  tempdif=0, eta_water=0.95, sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(sewageTreatmentPlant.component())
consumer1.addComponent(sewageTreatmentPlant)


#--- Sludge ---

#Sludge Storage
#Multiplication with 3992 of the storage dimensions
sludgeStorage = water_comp.SludgeStorage(sector_in=consumer1.getSludgeSector(), sector_out=consumer1.getSludgestorageSector(), 
                                         timesteps=timesteps, volume_max=5988, Q_max=5988)
my_energysystem.add(sludgeStorage.component())
consumer1.addComponent(sludgeStorage)


#Sludge Combustion
#Assumptions as for waste combustion
sludgecombustion = water_comp.Sludgecombustion(sector_in=consumer1.getSludgestorageSector(), 
                                               sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()],
                                               P_in=2190, P_out=2910, sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(sludgecombustion.component())
consumer1.addComponent(sludgecombustion)


#Sewage Disposal
#sewageDisposal = water_comp.Sewagedisposal(sector=consumer1.getSewageSector(), costs=consumer1.input_data['sewageCosts'])        
#my_energysystem.add(sewageDisposal.component())
#consumer1.addComponent(sewageDisposal)

#Sludge Disposal
sludgeDisposal = water_comp.Sludgedisposal(sector=consumer1.getSludgestorageSector(), costs=consumer1.input_data['sludgeCosts'], sector_emissions=consumer1.getEmissionsSector())        
my_energysystem.add(sludgeDisposal.component())
consumer1.addComponent(sludgeDisposal)


# ----- Sludge AD ---
#Sludge Anaerobic Digestion
#Same assumptions as for waste AD and sludge combustion
sludgeAD = water_comp.SludgeAD(sector_in=consumer1.getSludgestorageSector(), sector_out=consumer1.getGasSector(),
                              P_in=2190, P_out=2190)
my_energysystem.add(sludgeAD.component())
consumer1.addComponent(sludgeAD)





#___________ Gas ____________________

#Gas Demand
#Changes in Input
gasdemand = gas_comp.Demand(sector=consumer1.getGasSector(), demand_timeseries=consumer1.input_data['gasDemand'], timesteps=timesteps)
my_energysystem.add(gasdemand.component())
consumer1.addComponent(gasdemand)


#Gas Grid
#Factor 2800*19/27  households with gas --> Connection Power 19704kW
gasgrid = gas_comp.GridEmissions(sector=consumer1.getGasSector(), P=100000, costs=consumer1.input_data['gasCosts'], timesteps=timesteps, emissions=0.02, sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(gasgrid.component())
consumer1.addComponent(gasgrid)

#Blockheat
# 19/27 decentral gas --> Input 6336kW --> eta 0,44 --> P_out 2788kW
blockheat = gas_comp.Blockheat(sector_in=consumer1.getGasSector(), sector_out=consumer1.getElectricitySector(), P_in=6336, P_out=2788, sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(blockheat.component())
consumer1.addComponent(blockheat)

#Gasboiler
# 19/27 decentral gas --> Input 9164kW --> Output with eta 0,95 8705kW
gasboiler = gas_comp.Gasboiler(sector_in=consumer1.getGasSector(), sector_out=consumer1.getHeatSector(), P_in=31482, P_out=31482, sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(gasboiler.component())
consumer1.addComponent(gasboiler)

#Gas CHP
gasCHP = gas_comp.CHP(sector_in=consumer1.getGasSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()], P_in=31482, P_out=31482, sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(gasCHP.component())
consumer1.addComponent(gasCHP)


"""

#___________ Hydrogen ____________________

#Electrolysis
#Small cities can have a electrolysis capacity of up to 3000kW according to scenarion
electrolysis = el_comp.Electrolysis(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getHydrogenSector(), sector_loss=consumer1.getPotablewaterSector(), P=3000,
                                    water_demand=0.001, max_water=0)
my_energysystem.add(electrolysis.component())
consumer1.addComponent(electrolysis)



#Hydrogen Storage
#Capacity dependent on extension scenarios --> about 1.5GWh to 6GWh for small cities can be assumed
#Input power must be as high as the electrolysis power
#Output power must be as high as the fuelcell power 
#--> both 3000kW
hydrogenStorage = hydro_comp.Hydrogenstorage(sector_in=consumer1.getHydrogenSector(), P_in=3000, P_out=3000, soc_start=0, soc_max=1500000, balanced=True)
my_energysystem.add(hydrogenStorage.component())
consumer1.addComponent(hydrogenStorage)


#Fuelcell
#Assumption of the fuelcell power 3000kW
fuelcell = hydro_comp.Fuelcell(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getElectricitySector(), P=3000)
my_energysystem.add(fuelcell.component())
consumer1.addComponent(fuelcell)

#Grid Sink
hydrogenSink = hydro_comp.HydrogenSink(sector=consumer1.getHydrogenSector(), V_max=1500000)        
my_energysystem.add(hydrogenSink.component())
consumer1.addComponent(hydrogenSink)

"""


#___________ Emissions ____________________

emissions = em_comp.Emissions(sector=consumer1.getEmissionsSector(), costs=co2Price)
my_energysystem.add(emissions.component())
consumer1.addComponent(emissions)

print("----------------model set up----------------")





#___________ Solve Model ____________________

messi = solph.Model(my_energysystem) 

#Prevent Charging and Discharging at same time
messi = constraints.battery_chargeblock(model=messi, battery=battery)
messi = constraints.thermalstorage_chargeblock(model=messi, storage=thermalStorage)


if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/scenario.lp'
        messi.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    messi.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, messi, scenario)


results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")
    
#messi.solve(solver=solver, solve_kwargs={'tee': True})

#my_energysystem.results = solph.processing.results(messi)
    

consumer1.sortComponents()
consumer1.sortServiceComponents()
consumer1.add2componentList()





#Getting the Costs
costsElecSector = get_costs.getSectorCosts(results=results, components=consumer1.electricityComponents, label='Consumer_1')
costsHeatSector = get_costs.getSectorCosts(results=results, components=consumer1.heatComponents, label='Consumer_1')
costsGasSector = get_costs.getSectorCosts(results=results, components=consumer1.gasComponents, label='Consumer_1')
costsWasteSector = get_costs.getSectorCosts(results=results, components=consumer1.wasteComponents, label='Consumer_1')
costsWaterSector = get_costs.getSectorCosts(results=results, components=consumer1.waterComponents, label='Consumer_1')
costsHydrogenSector = get_costs.getSectorCosts(results=results, components=consumer1.hydrogenComponents, label='Consumer_1')
costsEmissionsSector = get_costs.getSectorCosts(results=results, components=consumer1.emissionComponents, label='Consumer_1')
costsEmissionsOutput = get_costs.getEmissionCosts(results=results, components=consumer1.emissionOutputComponents, sectorname='Emissions', costs=co2Price)


#Costs without Grid
costsElecSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.electricityComponents, label='Consumer_1', exclude_grid=True)
costsHeatSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.heatComponents, label='Consumer_1', exclude_grid=True)
costsGasSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.gasComponents, label='Consumer_1', exclude_grid=True)
costsWasteSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.wasteComponents, label='Consumer_1', exclude_grid=True)
costsWaterSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.waterComponents, label='Consumer_1', exclude_grid=True)
costsHydrogenSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.hydrogenComponents, label='Consumer_1', exclude_grid=True)
costsEmissionsSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.emissionComponents, label='Consumer_1', exclude_grid=True)
costsEmissionsOutputXgrid = get_costs.getEmissionCosts(results=results, components=consumer1.emissionOutputComponents, sectorname='Emissions', costs=co2Price, exclude_grid=True)


#Getting the Service Costs
costsServiceElecSector = get_costs.getSectorCosts(results=results, components=consumer1.electricityServiceComponents, label='Consumer_1')
costsServiceHeatSector = get_costs.getSectorCosts(results=results, components=consumer1.heatServiceComponents, label='Consumer_1')
costsServiceGasSector = get_costs.getSectorCosts(results=results, components=consumer1.gasServiceComponents, label='Consumer_1')
costsServiceWasteSector = get_costs.getSectorCosts(results=results, components=consumer1.wasteServiceComponents, label='Consumer_1')
costsServiceWaterSector = get_costs.getSectorCosts(results=results, components=consumer1.waterServiceComponents, label='Consumer_1')
costsServiceHydrogenSector = get_costs.getSectorCosts(results=results, components=consumer1.hydrogenServiceComponents, label='Consumer_1')


#Service Costs without Grid
costsServiceElecSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.electricityServiceComponents, label='Consumer_1', exclude_grid=True)
costsServiceHeatSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.heatServiceComponents, label='Consumer_1', exclude_grid=True)
costsServiceGasSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.gasServiceComponents, label='Consumer_1', exclude_grid=True)
costsServiceWasteSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.wasteServiceComponents, label='Consumer_1', exclude_grid=True)
costsServiceWaterSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.waterServiceComponents, label='Consumer_1', exclude_grid=True)
costsServiceHydrogenSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.hydrogenServiceComponents, label='Consumer_1', exclude_grid=True)


#total cost lists
sectorList = ['Electricity', 'Heat', 'Gas', 'Waste', 'Water', 'Emissions']

costList = [costsElecSector, costsHeatSector, costsGasSector, costsWasteSector, costsWaterSector, costsEmissionsSector]
totalCosts = get_costs.getTotalCosts(sectorlist=sectorList, costslist=costList, consumer=consumer1)

costListCo2Xgrid = [costsElecSector, costsHeatSector, costsGasSector, costsWasteSector, costsWaterSector, costsEmissionsSectorXgrid]
totalCostsCo2Xgrid = get_costs.getTotalCosts(sectorlist=sectorList, costslist=costListCo2Xgrid, consumer=consumer1)

costListXgrid = [costsElecSectorXgrid, costsHeatSectorXgrid, costsGasSectorXgrid, costsWasteSectorXgrid, costsWaterSectorXgrid, costsEmissionsSectorXgrid]
totalCostsXgrid = get_costs.getTotalCosts(sectorlist=sectorList, costslist=costListXgrid, consumer=consumer1)

costServiceList = [costsServiceElecSector, costsServiceHeatSector, costsServiceGasSector, costsServiceWasteSector, costsServiceWaterSector, costsEmissionsSector]
totalServiceCosts = get_costs.getTotalCosts(sectorlist=sectorList, costslist=costServiceList, consumer=consumer1)

costServiceListXgrid = [costsServiceElecSectorXgrid, costsServiceHeatSectorXgrid, costsServiceGasSectorXgrid, costsServiceWasteSectorXgrid, costsServiceWaterSectorXgrid, costsEmissionsSectorXgrid]
totalServiceCostsXgrid = get_costs.getTotalCosts(sectorlist=sectorList, costslist=costServiceListXgrid, consumer=consumer1)




#___________ Plots ____________________
plots.create_folders(scenario, [consumer1])



#Electricity Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', color='dodgerblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity')


#Heat Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', color='red')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat')


#Gas Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', color='lightgrey')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas')


#Waste Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastesector', label='Waste', unit='kg')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastesector', label='Waste', unit='kg')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastesector', label='Waste', unit='kg')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastesector', label='Waste', unit='kg')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastesector', label='Waste', color='saddlebrown')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastesector', label='Waste')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastesector', label='Waste')


plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', color='saddlebrown')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste')


#Water Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', color='aqua')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water')


plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', color='lime')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage')


plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', color='slategrey')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge')


plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', color='deepskyblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Water')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Water')


plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', color='aliceblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='Water')
#plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='Water')


#Hydrogen Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', color='limegreen')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen')

#CO2 Emission Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='t', factor=1000)
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='t', factor=1000)
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='t', factor=1000)
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='t', factor=1000)
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', color='slategrey')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions')


#Costs Plots

#Sector Costs
plots.plot_costs_bar(consumer=consumer1, costs=costsElecSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Electricity', elements=consumer1.electricityComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsHeatSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Heat', elements=consumer1.heatComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsGasSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Gas', elements=consumer1.gasComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsWasteSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Waste', elements=consumer1.wasteComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsWaterSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Water', elements=consumer1.waterComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsHydrogenSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Hydrogen', elements=consumer1.hydrogenComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Emissions', elements=consumer1.emissionComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsOutput, scenario=scenario, folder='costs', subfolder='costs', sectorname='EmissionsOutput', elements=consumer1.emissionOutputComponents)



#Sector Costs Without Grid
plots.plot_costs_bar(consumer=consumer1, costs=costsElecSectorXgrid, scenario=scenario, folder='costs', subfolder='costsXgrid', sectorname='Electricity', elements=consumer1.electricityComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsHeatSectorXgrid, scenario=scenario, folder='costs', subfolder='costsXgrid', sectorname='Heat', elements=consumer1.heatComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsGasSectorXgrid, scenario=scenario, folder='costs', subfolder='costsXgrid', sectorname='Gas', elements=consumer1.gasComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsWasteSectorXgrid, scenario=scenario, folder='costs', subfolder='costsXgrid', sectorname='Waste', elements=consumer1.wasteComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsWaterSectorXgrid, scenario=scenario, folder='costs', subfolder='costsXgrid', sectorname='Water', elements=consumer1.waterComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsHydrogenSectorXgrid, scenario=scenario, folder='costs', subfolder='costsXgrid', sectorname='Hydrogen', elements=consumer1.hydrogenComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsSectorXgrid, scenario=scenario, folder='costs', subfolder='costs', sectorname='Emissions', elements=consumer1.emissionComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsOutputXgrid, scenario=scenario, folder='costs', subfolder='costs', sectorname='EmissionsOutput', elements=consumer1.emissionOutputComponents)



#Service Costs
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceElecSector, scenario=scenario, folder='costs', subfolder='servicecosts', sectorname='Electricity', elements=consumer1.electricityServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceHeatSector, scenario=scenario, folder='costs', subfolder='servicecosts', sectorname='Heat', elements=consumer1.heatServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceGasSector, scenario=scenario, folder='costs', subfolder='servicecosts', sectorname='Gas', elements=consumer1.gasServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceWasteSector, scenario=scenario, folder='costs', subfolder='servicecosts', sectorname='Waste', elements=consumer1.wasteServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceWaterSector, scenario=scenario, folder='costs', subfolder='servicecosts', sectorname='Water', elements=consumer1.waterServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceHydrogenSector, scenario=scenario, folder='costs', subfolder='servicecosts', sectorname='Hydrogen', elements=consumer1.hydrogenServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsSector, scenario=scenario, folder='costs', subfolder='costs', sectorname='Emissions', elements=consumer1.emissionComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsOutput, scenario=scenario, folder='costs', subfolder='costs', sectorname='EmissionsOutput', elements=consumer1.emissionOutputComponents)



#Service Costs without Grid
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceElecSectorXgrid, scenario=scenario, folder='costs', subfolder='servicecostsXgrid', sectorname='Electricity', elements=consumer1.electricityServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceHeatSectorXgrid, scenario=scenario, folder='costs', subfolder='servicecostsXgrid', sectorname='Heat', elements=consumer1.heatServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceGasSectorXgrid, scenario=scenario, folder='costs', subfolder='servicecostsXgrid', sectorname='Gas', elements=consumer1.gasServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceWasteSectorXgrid, scenario=scenario, folder='costs', subfolder='servicecostsXgrid', sectorname='Waste', elements=consumer1.wasteServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceWaterSectorXgrid, scenario=scenario, folder='costs', subfolder='servicecostsXgrid', sectorname='Water', elements=consumer1.waterServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsServiceHydrogenSectorXgrid, scenario=scenario, folder='costs', subfolder='servicecostsXgrid', sectorname='Hydrogen', elements=consumer1.hydrogenServiceComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsSectorXgrid, scenario=scenario, folder='costs', subfolder='costs', sectorname='Emissions', elements=consumer1.emissionComponents)
plots.plot_costs_bar(consumer=consumer1, costs=costsEmissionsOutputXgrid, scenario=scenario, folder='costs', subfolder='costs', sectorname='EmissionsOutput', elements=consumer1.emissionOutputComponents)



#Total Costs
plots.plot_totalcosts_pie(consumer=consumer1, costs=totalCosts, scenario=scenario, folder='costs', subfolder='costs')
plots.plot_totalcosts_pie(consumer=consumer1, costs=totalCostsCo2Xgrid, scenario=scenario, folder='costs', subfolder='costs', name='_xGrid')
plots.plot_totalcosts_pie(consumer=consumer1, costs=totalCostsXgrid, scenario=scenario, folder='costs', subfolder='costsXgrid')
plots.plot_totalcosts_pie(consumer=consumer1, costs=totalServiceCosts, scenario=scenario, folder='costs', subfolder='servicecosts')
plots.plot_totalcosts_pie(consumer=consumer1, costs=totalServiceCostsXgrid, scenario=scenario, folder='costs', subfolder='servicecostsXgrid')


print("----------------plots done----------------")

