# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 09:25:50 2022

@author: Matthias Maldet
"""

import pyomo.environ as po
import pyomo.core as pyomo
import numpy as np

def flow_limitation_excesspurchase(*args, **kwargs):
    
    #Vehicle Objects
    pipeline_limited = kwargs.get("pipeline_limited", 0)
    pipeline_excess = kwargs.get("pipeline_excess", 0)

    
    bigM1 = 10000
    bigM2 = 10000
    eps = 0.0001
    eps2 = 1.1*eps
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0 or pipeline_limited==0 or pipeline_excess==0):
        print('Model not found!')
        return 0
        
    else:
     
        limitSum=pipeline_limited.summedLimit
     
        
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        myBlock.bin_excess = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Charge and Discharge Block'
            )
        
        
        myBlock.diff = pyomo.Var(
                myBlock.t, 
                within=pyomo.NonNegativeReals,
                doc='Difference of Summed Max and actual flow'
            )
        
        
        
        modelIn.add_component('MyBlockExcesspurchase_' + pipeline_limited.label, myBlock)
        
        
        #Get the required Flows
        myBlock.Limitflow = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==pipeline_limited.label])
        myBlock.Excessflow = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==pipeline_excess.label])
        
        
        #Rule for the current flow from the limited purchase
        def differencerule(model, t):
            if(t==0):
                return(model.diff[t] == limitSum -  sum(modelIn.flow[i, o, t] for (i, o) in myBlock.Limitflow))
            else:
                 return(model.diff[t] == model.diff[t-1] - sum(modelIn.flow[i, o, t] for (i, o) in myBlock.Limitflow))
             
                
        def bigmRule1(model, t):
            return(bigM1 * model.bin_excess[t] + eps >= model.diff[t])
        
        
        def bigmRule2(model, t):
            return(-(1-model.bin_excess[t])*bigM2 + eps2  <= model.diff[t])
        
        
        def excessLimit(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.Excessflow) <= pipeline_excess.V * (1- model.bin_excess[t]))
        
        
        myBlock.WaterpurchaseDiff = pyomo.Constraint(
                    myBlock.t,
                    rule=differencerule,
                    doc='Currently purchased limited water')
        
        
        myBlock.bigM1 = pyomo.Constraint(
                    myBlock.t,
                    rule=bigmRule1,
                    doc='First Big M Rule')
        
        myBlock.bigM2 = pyomo.Constraint(
                    myBlock.t,
                    rule=bigmRule2,
                    doc='Second Big M Rule')
        
        myBlock.ExcessLimitation = pyomo.Constraint(
                    myBlock.t,
                    rule=excessLimit,
                    doc='Limitation for Excess Purchase')
        
        
        return modelIn
    
def lsc_block(*args, **kwargs):
    
    #Vehicle Objects
    sale = kwargs.get("sale", 0)
    purchase = kwargs.get("purchase", 0)
    label = kwargs.get("label", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        
        #Set with Vehicles
            
        #Binary optimization variables
        #Electric Vehicle Binary
        myBlock.bin_lsc = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Sale Purchase Block'
        )
            
      
    
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.purchase = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==purchase.label])
        myBlock.sale = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==sale.label])
    
    
        def purchaserule(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.purchase) <= model.bin_lsc[t]*purchase.P)
         
        def salerule(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.sale) <= (1-model.bin_lsc[t])*sale.P)


        myBlock.lscPurchaseBin = pyomo.Constraint(
            myBlock.t,
            rule=purchaserule,
            doc='LSC Purchase Rule')
        
                
        myBlock.lscSaleBin = pyomo.Constraint(
            myBlock.t,
            rule=salerule,
            doc='LSC Sale Rule')
        
           
            
        
        return modelIn         
    
def watersale_limit(*args, **kwargs):
    
    #Vehicle Objects
    limit = kwargs.get("limit", 1)
    sewageTreatment = kwargs.get("sewage_treatment", 0)
    watersale = kwargs.get("watersale", 0)
    label = kwargs.get("label", 0)

    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        

      
    
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.sewagetreat = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])==sewageTreatment.label and 'Sewagesector' in str(flow[0])])
        myBlock.sale = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])==watersale.label])
    
    
        def watersalerule(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.sale) <= limit*sum(modelIn.flow[i, o, t] for (i, o) in myBlock.sewagetreat))
         


        myBlock.lscWatersalerule = pyomo.Constraint(
            myBlock.t,
            rule=watersalerule,
            doc='LSC Watersale Rule')
        
        return modelIn
        
                
 
        

        
