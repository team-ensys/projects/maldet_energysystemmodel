# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 18:48:43 2021

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np
import source_emissions as em

class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_in = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Coolingdemand_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Cooling\nDemand_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='yellow'
            self.color = kwargs.get("color", color)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_coolingDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            coolingDemand = solph.Sink(label=self.label, inputs={self.sector_in: q_coolingDemand})
            
            return coolingDemand
        
        
class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Coolinggrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Cooling\nGrid_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color='slategrey'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_coolingGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
            
        coolGrid = solph.Source(label=self.label, outputs={self.sector_out: q_coolingGrid})
        
        return coolGrid  
    
class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0.28
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Coolinggrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Cooling\nGrid_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color = 'slategrey'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_coolingGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            coolGrid = solph.Source(label=self.label, outputs={self.sector_out: q_coolingGrid})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            coolGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_coolingGrid, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return coolGrid

class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        existingGrid = 0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Coolinggridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Cooling\nGrid\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
    def component(self):
        q_coolGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))
            
        coolGrid = solph.Source(label=self.label, outputs={self.sector_out: q_coolGridpurchase})
        
        return coolGrid   
    
    
class GridPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        existingGrid=0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Coolinggridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Cooling\nGrid\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
        
    def component(self):
        q_coolGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))

        
        if(self.emissions==0 or self.sector_emissions==0):
            
            coolGrid = solph.Source(label=self.label, outputs={self.sector_out: q_coolGridpurchase})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            coolGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_coolGridpurchase, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return coolGrid
        

        