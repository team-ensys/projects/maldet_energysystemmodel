# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 08:33:12 2023

@author: Matthias Maldet
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from matplotlib import cm
from matplotlib.ticker import LinearLocator
import plotly.offline as pyo


#Input parameters to change
file2read = "plots_lsc.xlsx"

savedirectory='plots_lsc/'

#----------------------------- Heatmap scenario 3 -----------------------------------------------------------

data=pd.read_excel(file2read, index_col=0, sheet_name="base_classification")