# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 10:21:03 2021

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import StorageIntermediate as storage_int
import numpy as np


class Demand():

        
        def __init__(self, *args, **kwargs):
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_in = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Hydrogendemand_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Hydrogen\nDemand_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='yellow'
            self.color = kwargs.get("color", color)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            v_hydrogenDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            hydrogenDemand = solph.Sink(label=self.label, inputs={self.sector_in: v_hydrogenDemand})
            
            return hydrogenDemand
        
        
class Hydrogenboiler():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.95
            costsIn=0
            costsOut=0.001
            PowerInLimit = 21
            PowerOutLimit = 21
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Hydrogenboiler_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Hydrogen\nBoiler_' + self.position)
            color='khaki'
            self.color = kwargs.get("color", color)

        def component(self):
            v_hydrogenBoilHydro = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in)
            q_hydrogenBoilTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            hydrogenBoiler = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_hydrogenBoilHydro},
                outputs={self.sector_out : q_hydrogenBoilTh},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_hydrogen})
            return hydrogenBoiler
        
        
class Methanation():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.25
            costsIn=0
            costsOut=0.15
            PowerInLimit = 500
            PowerOutLimit = 500
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_gas = 11.42
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.H_gas = kwargs.get("H_gas", H_gas)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Methanation_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Methanation_' + self.position)
            color='green'
            self.color = kwargs.get("color", color)

        def component(self):
            v_methanationHydrogen = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_gas*self.conversion_factor), variable_costs=self.costs_in)
            q_methanationGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT/(self.H_gas), variable_costs=self.costs_out/self.H_gas)
            
            methanation = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_methanationHydrogen},
                outputs={self.sector_out : q_methanationGas},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_gas})
            return methanation

class Hydrogenstorage():
    
        def __init__(self, *args, **kwargs):
            
            P_inMax = 8
            P_outMax = 8
            costsIn = 0.016
            costsOut= 0.016
            Eta_in = 0.6
            Eta_out = 0.6
            Eta_sb = 0.999
            SOC_max = 300
            SOC_start = 0
            Storagelevelmin = 0
            Storagelevelmax=1
            emissions=0
            deltaT=1
            position='Consumer_1'
        
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
        
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.balanced = kwargs.get("balanced", True)
            self.eta_in = kwargs.get("eta_in", Eta_in)
            self.eta_out = kwargs.get("eta_out", Eta_out)
            self.eta_sb = kwargs.get("eta_sb", Eta_sb)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", P_inMax)
            self.P_out = kwargs.get("P_out", P_outMax)
            self.soc_start = kwargs.get("soc_start", SOC_start)
            self.level_min = kwargs.get("level_min", Storagelevelmin)
            self.level_max = kwargs.get("level_max", Storagelevelmax)
            self.soc_max = kwargs.get("soc_max", SOC_max)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out", self.sector_in)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Hydrogenstorage_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Hydrogen\nStorage_' + self.position)
            color='red'
            self.color = kwargs.get("color", color)
            
        
        
        def component(self):
            #Energy Flows
            v_hydrostoreIn = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in*self.H_hydrogen)
            v_hydrostoreOut = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in*self.H_hydrogen)
            
            hydroStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_hydrostoreIn},
                outputs={self.sector_out: v_hydrostoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in*self.H_hydrogen, outflow_conversion_factor=self.eta_out/self.H_hydrogen,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
            
        
            return hydroStorage        
        
class CHP():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.03, 0.03]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
            
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'HydrogenCHP_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Hydrogen\nCHP_' + self.position)
            color='deepskyblue'
            self.color = kwargs.get("color", color)
            

        def component(self):
            v_CHPhydroH2 = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in)
            q_CHPhydroEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out[0])
            q_CHPhydroTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out[1])
            
            CHPhydro = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_CHPhydroH2},
                outputs={self.sector_out[0] : q_CHPhydroEl, self.sector_out[1] : q_CHPhydroTh},
                conversion_factors={self.sector_out[0] : self.conversion_factor[0]*self.H_hydrogen, self.sector_out[1] : self.conversion_factor[1]*self.H_hydrogen})
            
            
            return CHPhydro


        


class HydrogenGasgrid():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.9
            V_max = 30000
            share_gasgridMax=0.1
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            
            #Heating value for hydrogen in kWh/m³
            H_gas = 11.42
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.V_max = kwargs.get("V_max", V_max)
            self.share_max = kwargs.get("share_max", share_gasgridMax)
            self.H_gas = kwargs.get("H_gas", H_gas)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'H2GridFeedin_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'H2Grid\nFeedin' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='snow'
            self.color = kwargs.get("color", color)
            
            revenuesEnergyArray = np.zeros(self.timesteps)
            
            self.revenues = kwargs.get("revenues", revenuesEnergyArray)
            self.revenues=np.multiply(self.revenues, -1)

        def component(self):
            v_h2tohydrogenGridIn = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.share_max, summed_max=1)
            v_h2tohydrogenGridOut = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.share_max, summed_max=1)
            #v_h2tohydrogenGridSold = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.share_max, summed_max=1, variable_costs=self.revenues)
            
            #Transformer for real value to feed into gas grid
            hydro2gas = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_h2tohydrogenGridIn},
                outputs={self.sector_out : v_h2tohydrogenGridOut},
                conversion_factors={self.sector_out : self.conversion_factor})
            
   
            
            #hydro2gasgrid = solph.Sink(label='hydrogen2gasgrid', inputs={hydrogen2gasgrid_sector: v_h2tohydrogenGridSold})
            return hydro2gas
        
 
class Fuelcell():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.6
            costsIn=0
            costsOut=0.018
            P = 5
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
            
            self.efficiency = kwargs.get("efficiency", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P = kwargs.get("P", P)
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Fuelcell_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Fuelcell_' + self.position)
            color='royalblue'
            self.color = kwargs.get("color", color)
            

        def component(self):
            v_fuelcellH2 = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in)
            q_fuelcellH2 = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_out)
            
            fuelcell = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_fuelcellH2},
                outputs={self.sector_out : q_fuelcellH2},
                conversion_factors={self.sector_out : self.efficiency*self.H_hydrogen})
            return fuelcell    
 
    
class HydrogenSink():
    
    def __init__(self, *args, **kwargs):
        V_max=30000
        deltaT=1
        position='Consumer_1'
        
        self.V_max= kwargs.get("V_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Hydrogensink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Hydrogen\nSink_' + self.position)
        self.costs_in=0
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        v_hydrogenfeedin = solph.Flow(min=0, max=1, nominal_value=self.V_max, variable_costs=self.costs_in)
            
        hydroSink = solph.Sink(label=self.label, inputs={self.sector_in: v_hydrogenfeedin})
        
        return hydroSink
        
       