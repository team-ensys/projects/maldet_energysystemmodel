import json
import pandas as pd
import numpy as np

#Input parameters to change
resultdirectory = "results/RESULTS_LSM_Round1/"
scenarioname = "LSM19_greywater"

df = pd.DataFrame()

data_total=[]
objective_total=[]

scenarios=["LSM1_noPV", "LSM2_PV", "LSM3_trade", "LSM4_lessPVnoTrad", "LSM5_lessPVTrad",
           "LSM6_lessPVLSM", "LSM7_waste20", "LSM8_waste50", "LSM9_waste90", "LSM10_wastered",
           "LSM11_wasteDisp", "LSM12_wasteAD", "LSM13_ADminBio", "LSM14_GasCHP", "LSM15_CHPhigh", 
           "LSM16_wasteLow", "LSM17_wasteHigh", "LSM18_combMinBio", "LSM19_greywater", "LSM20_greywaterMin",
           "LSM21_aut", "LSM22_2040", "LSM23_environ", "LSM24_eff", "LSM25_comp"]

for scenarioname in scenarios:

    #read investment costs
    file2read=resultdirectory + scenarioname + "/costs_invest.txt"
       
    # Opening JSON file
    f = open(file2read,)
       
    
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
    
    data_total.append(data["Total"])
    
    #Read objective
    file2read=resultdirectory + scenarioname + "/objective.txt"
       
    f = open(file2read,)
    lines = f.readlines()
    
    index=0
    result=0
    for line in lines:
        if("objective" in str(line).lower()):
            result=lines[index+1]
        index+=1
        
    objective_total.append(float(result))
    
    
df["Scenario"] = scenarios
df["Investment costs in €"] = data_total
df["Objective in €"] = objective_total

file2write=resultdirectory +  "/total_costs.xlsx"
df.to_excel(file2write) 
