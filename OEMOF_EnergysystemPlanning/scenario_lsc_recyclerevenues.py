# -*- coding: utf-8 -*-
"""
Created on Tue May 10 11:00:47 2022

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'scenario_phase2_recyclerevenues'

#Filename
filename = "input_data_extended.xlsx"


#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 8784
timestepsTotal = 8784

data=pd.ExcelFile(filename)


#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    

# -------------- Additional parameters that are set for the simulation -------------- 

#_____________ Electricity _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzentgelte
c_grid = 0.062

#Abgabe
c_abgabe = 0.018

#Reduktion Netzentgelt Lokalbereich
red_local = 1-0.57

#Reduktion Netzentgelt Regionalbereich
red_regional = 1-0.28

#_____________ Heat _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_heat = 0.046


#_____________ Cooling _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_cool = 0.046


#---------------------- DEFINITION OF CONSUMERS ----------------------

#Consumer 1
consumer1 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_1')
consumer1.addElectricitySector()
consumer1.addHeatSector()
consumer1.addCoolingSector()
consumer1.addWaterSector()
consumer1.addWasteSector()
#consumer1.addSector('testsector')
#TESTSTESTE = consumer1.getSector('testsector')
consumer1.sector2system(my_energysystem)


#Consumer 2
consumer2 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_2')
consumer2.addElectricitySector()
consumer2.addHeatSector()
consumer2.addCoolingSector()
consumer2.addWaterSector()
consumer2.addWasteSector()
consumer2.sector2system(my_energysystem)

#Consumer 3
consumer3 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_3')
consumer3.addElectricitySector()
consumer3.addHeatSector()
consumer3.addCoolingSector()
consumer3.addWaterSector()
consumer3.addWasteSector()
consumer3.sector2system(my_energysystem)

#Consumer 4
consumer4 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_4')
consumer4.addElectricitySector()
consumer4.addHeatSector()
consumer4.addCoolingSector()
consumer4.addWaterSector()
consumer4.addWasteSector()
consumer4.sector2system(my_energysystem)

#Consumer 5
consumer5 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_5')
consumer5.addElectricitySector()
consumer5.addHeatSector()
consumer5.addCoolingSector()
consumer5.addWaterSector()
consumer5.addWasteSector()
consumer5.sector2system(my_energysystem)

#Consumer 6
consumer6 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_6')
consumer6.addElectricitySector()
consumer6.addHeatSector()
consumer6.addCoolingSector()
consumer6.addWaterSector()
consumer6.addWasteSector()
consumer6.sector2system(my_energysystem)

#Consumer 7
consumer7 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_7')
consumer7.addElectricitySector()
consumer7.addHeatSector()
consumer7.addCoolingSector()
consumer7.addWaterSector()
consumer7.addWasteSector()
consumer7.sector2system(my_energysystem)

#Consumer 8
consumer8 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_8')
consumer8.addElectricitySector()
consumer8.addHeatSector()
consumer8.addCoolingSector()
consumer8.addWaterSector()
consumer8.addWasteSector()
consumer8.sector2system(my_energysystem)

#Consumer 9
consumer9 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_9')
consumer9.addElectricitySector()
consumer9.addHeatSector()
consumer9.addCoolingSector()
consumer9.addWaterSector()
consumer9.addWasteSector()
consumer9.sector2system(my_energysystem)

#Consumer 10
consumer10 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_10')
consumer10.addElectricitySector()
consumer10.addHeatSector()
consumer10.addCoolingSector()
consumer10.addWaterSector()
consumer10.addWasteSector()
consumer10.sector2system(my_energysystem)

#Consumer 11
consumer11 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_11')
consumer11.addElectricitySector()
consumer11.addHeatSector()
consumer11.addCoolingSector()
consumer11.addWaterSector()
consumer11.addWasteSector()
consumer11.sector2system(my_energysystem)

#Consumer 12
consumer12 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_12')
consumer12.addElectricitySector()
consumer12.addHeatSector()
consumer12.addCoolingSector()
consumer12.addWaterSector()
consumer12.addWasteSector()
consumer12.sector2system(my_energysystem)

#Additional consumers initially not considered
"""
#Consumer 13
consumer13 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_13')
consumer13.addElectricitySector()
consumer13.addHeatSector()
consumer13.addCoolingSector()
consumer13.addWaterSector()
consumer13.addWasteSector()
consumer13.sector2system(my_energysystem)

#Consumer 14
consumer14 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_14')
consumer14.addElectricitySector()
consumer14.addHeatSector()
consumer14.addCoolingSector()
consumer14.addWaterSector()
consumer14.addWasteSector()
consumer14.sector2system(my_energysystem)

#Consumer 15
consumer15 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_15')
consumer15.addElectricitySector()
consumer15.addHeatSector()
consumer15.addCoolingSector()
consumer15.addWaterSector()
consumer15.addWasteSector()
consumer15.sector2system(my_energysystem)

#Consumer 16
consumer16 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_16')
consumer16.addElectricitySector()
consumer16.addHeatSector()
consumer16.addCoolingSector()
consumer16.addWaterSector()
consumer16.addWasteSector()
consumer16.sector2system(my_energysystem)

#Consumer 17
consumer17 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_17')
consumer17.addElectricitySector()
consumer17.addHeatSector()
consumer17.addCoolingSector()
consumer17.addWaterSector()
consumer17.addWasteSector()
consumer17.sector2system(my_energysystem)

#Consumer 18
consumer18 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_18')
consumer18.addElectricitySector()
consumer18.addHeatSector()
consumer18.addCoolingSector()
consumer18.addWaterSector()
consumer18.addWasteSector()
consumer18.sector2system(my_energysystem)
"""


#LSC
LSC = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC')
LSC.addElectricitySector()
LSC.addHeatSector()
LSC.addCoolingSector()
LSC.addWaterSector()
LSC.addWaterpoolSector()
LSC.addWasteSector()
LSC.addTransportSector()
LSC.addSector(sectorname = 'ElectricVehicle1sector')
LSC.addSector(sectorname ='ElectricVehicle2sector')
LSC.addSector(sectorname ='ElectricVehicle3sector')
LSC.addSector(sectorname ='ElectricVehicle4sector')
LSC.addSector(sectorname ='ElectricVehicle5sector')
LSC.addSector(sectorname ='ElectricVehicle6sector')
LSC.addSector(sectorname ='ElectricVehicle7sector')
LSC.addSector(sectorname ='ElectricVehicle8sector')
LSC.addSector(sectorname ='ElectricVehicle9sector')
LSC.addSector(sectorname ='ElectricVehicle10sector')
LSC.addSector(sectorname ='ElectricVehicle11sector')
LSC.addSector(sectorname ='ElectricVehicle12sector')
LSC.addEmissionSector()
LSC.sector2system(my_energysystem)


print("----------------Consumer Data loaded----------------")

#---------------------- Consumer 1 Components ----------------------

#_____________ Electricity _____________
#PV
pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_' + consumer1.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10)             


#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility = consumer1.get_willingness_for_waterflexibility()
willingness_for_flexibility = 0.250106
#willingness_for_flexibility=consumer1.get_willingness_for_waterflexibility_costdependent(costs=consumer1.input_data['waterPipelineCosts'][0])
waterFlexibility = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer1.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_1",
                                                       color='darkcyan', demand=consumer1.input_data['waterDemand'], wff=willingness_for_flexibility,
                                                       costs_lsc=consumer1.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer1.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr1=consumer1.get_willingness_for_recycling()
wfr1=0.2027168
#wfr1 = consumer1.get_willingness_for_recycling_costdependent(costs=consumer1.input_data['wastetransmissioncosts'][0])
wasteFlexibility = lsc_comp.WasteFlexibility(sector=consumer1.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_1", color='forestgreen', waste=consumer1.input_data['waste'], 
                                             wfr=wfr1, share_recycling=1, revenues=np.multiply(consumer1.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold1 = waste_comp.WasteStorage(sector_in=consumer1.getWastestorageSector(), sector_out=consumer1.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_1", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 20
delay_wastetruck = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste = lsc_comp.Delay_Transport(sector_in=consumer1.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer1.input_data["wastetransmissioncosts"], delay=delay_wastetruck, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_1', color='aquamarine')

transmissionWasteEmissions = lsc_comp.Transport_emissions(sector_in=consumer1.getEmissionsWastetruckSector(), sector_out=consumer1.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_1', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents1 = consumer_lsc.consumer_setup(  consumer=consumer1, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility, wasteFlexibility=wasteFlexibility, wasteStorageHousehold=wasteStorageHousehold1, transmissionWaste=transmissionWaste, pv=pv, transmissionWasteEmissions=transmissionWasteEmissions)

#Assign components to consumer and energy system
for component in consumercomponents1:
    my_energysystem.add(component.component())
    consumer1.addComponent(component)
    

#---------------------- Consumer 2 Components ----------------------

#_____________ Electricity _____________
pv = el_comp.Photovoltaic(sector=consumer2.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_' + consumer2.label, timesteps=timesteps, area_efficiency=0.5, area_roof=50, area_factor=10) 

#_____________ Heat _____________
#No additional heat components

#_____________ Cooling _____________
#No additional cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility2 = consumer2.get_willingness_for_waterflexibility()
willingness_for_flexibility2 = 0.250106
#willingness_for_flexibility2=consumer2.get_willingness_for_waterflexibility_costdependent(costs=consumer2.input_data['waterPipelineCosts'][0])
waterFlexibility2 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer2.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_2",
                                                       color='darkcyan', demand=consumer2.input_data['waterDemand'], wff=willingness_for_flexibility2,
                                                       costs_lsc=consumer2.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer2.input_data['waterPipelineCosts'])


#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr2=consumer2.get_willingness_for_recycling()
wfr2=0.2027168
#wfr2 = consumer2.get_willingness_for_recycling_costdependent(costs=consumer2.input_data['wastetransmissioncosts'][0])
wasteFlexibility2 = lsc_comp.WasteFlexibility(sector=consumer2.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_2", color='forestgreen', waste=consumer2.input_data['waste'], 
                                             wfr=wfr2, share_recycling=1, revenues=np.multiply(consumer2.input_data['wasteCosts'], 1/4))


#Waste Storage for Household
wasteStorageHousehold2 = waste_comp.WasteStorage(sector_in=consumer2.getWastestorageSector(), sector_out=consumer2.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_2", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
v_wastetruck2 = 10
s_wastetruck2 = 20
delay_wastetruck2 = math.ceil(s_wastetruck2/v_wastetruck2)
transmissionWaste2 = lsc_comp.Delay_Transport(sector_in=consumer2.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer2.input_data["wastetransmissioncosts"], delay=delay_wastetruck2, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_2', color='aquamarine')

transmissionWasteEmissions2 = lsc_comp.Transport_emissions(sector_in=consumer2.getEmissionsWastetruckSector(), sector_out=consumer2.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck2*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_2', color='azure')



#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents2 = consumer_lsc.consumer_setup(  consumer=consumer2, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility2, wasteFlexibility=wasteFlexibility2, wasteStorageHousehold=wasteStorageHousehold2, transmissionWaste=transmissionWaste2, transmissionWasteEmissions=transmissionWasteEmissions2)

#Assign components to consumer and energy system
for component in consumercomponents2:
    my_energysystem.add(component.component())
    consumer2.addComponent(component)


#---------------------- Consumer 3 Components ----------------------

#_____________ Electricity _____________

#Grid Purchase combined metering
#No additional electricity components


#_____________ Heat _____________
#No additional heat components

#_____________ Cooling _____________
#No additional cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility3 = consumer3.get_willingness_for_waterflexibility()
willingness_for_flexibility3 = 0.250106
#willingness_for_flexibility3=consumer3.get_willingness_for_waterflexibility_costdependent(costs=consumer3.input_data['waterPipelineCosts'][0])
waterFlexibility3 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer3.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_3",
                                                       color='darkcyan', demand=consumer3.input_data['waterDemand'], wff=willingness_for_flexibility3,
                                                       costs_lsc=consumer3.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer3.input_data['waterPipelineCosts'])


#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#Willingness for Recycling definition
#wfr3=consumer3.get_willingness_for_recycling()
wfr3=0.2027168
#wfr3 = consumer3.get_willingness_for_recycling_costdependent(costs=consumer3.input_data['wastetransmissioncosts'][0])
wasteFlexibility3 = lsc_comp.WasteFlexibility(sector=consumer3.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_3", color='forestgreen', waste=consumer3.input_data['waste'], 
                                             wfr=wfr3, share_recycling=1, revenues=np.multiply(consumer3.input_data['wasteCosts'], 1/4))



#Waste Storage for Household
#Disposal Periods - 1 to other consumers because of longer distance
wasteStorageHousehold3 = waste_comp.WasteStorage(sector_in=consumer3.getWastestorageSector(), sector_out=consumer3.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_3", color='mediumvioletred', balanced=False, empty_end=True, t_start=167)



#Waste Transport with Truck
v_wastetruck3 = 10
s_wastetruck3 = 20
delay_wastetruck3 = math.ceil(s_wastetruck3/v_wastetruck3)
transmissionWaste3 = lsc_comp.Delay_Transport(sector_in=consumer3.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer3.input_data["wastetransmissioncosts"], delay=delay_wastetruck3, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_3', color='aquamarine')

transmissionWasteEmissions3 = lsc_comp.Transport_emissions(sector_in=consumer3.getEmissionsWastetruckSector(), sector_out=consumer3.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck3*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_3', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents3 = consumer_lsc.consumer_setup(  consumer=consumer3, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility3, wasteFlexibility=wasteFlexibility3, wasteStorageHousehold=wasteStorageHousehold3, transmissionWaste=transmissionWaste3, transmissionWasteEmissions=transmissionWasteEmissions3)

#Assign components to consumer and energy system
for component in consumercomponents3:
    my_energysystem.add(component.component())
    consumer3.addComponent(component)
    



#---------------------- Consumer 4 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility4 = consumer4.get_willingness_for_waterflexibility()
willingness_for_flexibility4 = 0.250106
#willingness_for_flexibility4=consumer4.get_willingness_for_waterflexibility_costdependent(costs=consumer4.input_data['waterPipelineCosts'][0])
waterFlexibility4 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer4.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_4",
                                                       color='darkcyan', demand=consumer4.input_data['waterDemand'], wff=willingness_for_flexibility4,
                                                       costs_lsc=consumer4.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer4.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr4=consumer4.get_willingness_for_recycling()
wfr4=0.2027168
#wfr4 = consumer4.get_willingness_for_recycling_costdependent(costs=consumer4.input_data['wastetransmissioncosts'][0])
wasteFlexibility4 = lsc_comp.WasteFlexibility(sector=consumer4.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_4", color='forestgreen', waste=consumer4.input_data['waste'], 
                                             wfr=wfr4, share_recycling=1, revenues=np.multiply(consumer4.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold4 = waste_comp.WasteStorage(sector_in=consumer4.getWastestorageSector(), sector_out=consumer4.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_4", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck4 = 10
s_wastetruck4 = 20
delay_wastetruck4 = math.ceil(s_wastetruck4/v_wastetruck4)
transmissionWaste4 = lsc_comp.Delay_Transport(sector_in=consumer4.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer4.input_data["wastetransmissioncosts"], delay=delay_wastetruck4, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_4', color='aquamarine')

transmissionWasteEmissions4 = lsc_comp.Transport_emissions(sector_in=consumer4.getEmissionsWastetruckSector(), sector_out=consumer4.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck4*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_4', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents4 = consumer_lsc.consumer_setup(  consumer=consumer4, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility4, wasteFlexibility=wasteFlexibility4, wasteStorageHousehold=wasteStorageHousehold4, transmissionWaste=transmissionWaste4, transmissionWasteEmissions=transmissionWasteEmissions4)

#Assign components to consumer and energy system
for component in consumercomponents4:
    my_energysystem.add(component.component())
    consumer4.addComponent(component)    
    

    
 
#---------------------- Consumer 5 Components ----------------------

#_____________ Electricity _____________
pv = el_comp.Photovoltaic(sector=consumer5.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_' + consumer5.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10) 

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility5 = consumer5.get_willingness_for_waterflexibility()
willingness_for_flexibility5 = 0.250106
#willingness_for_flexibility5=consumer5.get_willingness_for_waterflexibility_costdependent(costs=consumer5.input_data['waterPipelineCosts'][0])
waterFlexibility5 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer5.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_5",
                                                       color='darkcyan', demand=consumer5.input_data['waterDemand'], wff=willingness_for_flexibility5,
                                                       costs_lsc=consumer5.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer5.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr5=consumer5.get_willingness_for_recycling()
wfr5=0.2027168
#wfr5 = consumer5.get_willingness_for_recycling_costdependent(costs=consumer5.input_data['wastetransmissioncosts'][0])
wasteFlexibility5 = lsc_comp.WasteFlexibility(sector=consumer5.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_5", color='forestgreen', waste=consumer5.input_data['waste'], 
                                             wfr=wfr5, share_recycling=1, revenues=np.multiply(consumer5.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold5 = waste_comp.WasteStorage(sector_in=consumer5.getWastestorageSector(), sector_out=consumer5.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_5", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck5 = 10
s_wastetruck5 = 20
delay_wastetruck5 = math.ceil(s_wastetruck5/v_wastetruck5)
transmissionWaste5 = lsc_comp.Delay_Transport(sector_in=consumer5.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer5.input_data["wastetransmissioncosts"], delay=delay_wastetruck5, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_5', color='aquamarine')

transmissionWasteEmissions5 = lsc_comp.Transport_emissions(sector_in=consumer5.getEmissionsWastetruckSector(), sector_out=consumer5.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck5*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_5', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents5 = consumer_lsc.consumer_setup(  consumer=consumer5, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility5, wasteFlexibility=wasteFlexibility5, wasteStorageHousehold=wasteStorageHousehold5, transmissionWaste=transmissionWaste5, pv=pv, transmissionWasteEmissions=transmissionWasteEmissions5)

#Assign components to consumer and energy system
for component in consumercomponents5:
    my_energysystem.add(component.component())
    consumer5.addComponent(component)   


    
#---------------------- Consumer 6 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility6 = consumer6.get_willingness_for_waterflexibility()
willingness_for_flexibility6 = 0.250106
#willingness_for_flexibility6=consumer6.get_willingness_for_waterflexibility_costdependent(costs=consumer6.input_data['waterPipelineCosts'][0])
waterFlexibility6 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer6.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_6",
                                                       color='darkcyan', demand=consumer6.input_data['waterDemand'], wff=willingness_for_flexibility6,
                                                       costs_lsc=consumer6.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer6.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr6=consumer6.get_willingness_for_recycling()
wfr6=0.2027168
#wfr6 = consumer6.get_willingness_for_recycling_costdependent(costs=consumer6.input_data['wastetransmissioncosts'][0])
wasteFlexibility6 = lsc_comp.WasteFlexibility(sector=consumer6.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_6", color='forestgreen', waste=consumer6.input_data['waste'], 
                                             wfr=wfr6, share_recycling=1, revenues=np.multiply(consumer6.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold6 = waste_comp.WasteStorage(sector_in=consumer6.getWastestorageSector(), sector_out=consumer6.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_6", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck6 = 10
s_wastetruck6 = 20
delay_wastetruck6 = math.ceil(s_wastetruck6/v_wastetruck6)
transmissionWaste6 = lsc_comp.Delay_Transport(sector_in=consumer6.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer6.input_data["wastetransmissioncosts"], delay=delay_wastetruck6, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_6', color='aquamarine')

transmissionWasteEmissions6 = lsc_comp.Transport_emissions(sector_in=consumer6.getEmissionsWastetruckSector(), sector_out=consumer6.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck6*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_6', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents6 = consumer_lsc.consumer_setup(  consumer=consumer6, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility6, wasteFlexibility=wasteFlexibility6, wasteStorageHousehold=wasteStorageHousehold6, transmissionWaste=transmissionWaste6, transmissionWasteEmissions=transmissionWasteEmissions6)

#Assign components to consumer and energy system
for component in consumercomponents6:
    my_energysystem.add(component.component())
    consumer6.addComponent(component)   



#---------------------- Consumer 7 Components ----------------------

#_____________ Electricity _____________
pv = el_comp.Photovoltaic(sector=consumer7.getElectricitySector(), generation_timeseries=consumer7.input_data['elGen'], 
                          label='PV_' + consumer7.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10) 

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility7 = consumer7.get_willingness_for_waterflexibility()
willingness_for_flexibility7 = 0.250106
#willingness_for_flexibility7=consumer7.get_willingness_for_waterflexibility_costdependent(costs=consumer7.input_data['waterPipelineCosts'][0])
waterFlexibility7 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer7.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_7",
                                                       color='darkcyan', demand=consumer7.input_data['waterDemand'], wff=willingness_for_flexibility7,
                                                       costs_lsc=consumer7.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer7.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr7=consumer7.get_willingness_for_recycling()
wfr7=0.2027168
#wfr7 = consumer7.get_willingness_for_recycling_costdependent(costs=consumer7.input_data['wastetransmissioncosts'][0])
wasteFlexibility7 = lsc_comp.WasteFlexibility(sector=consumer7.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_7", color='forestgreen', waste=consumer7.input_data['waste'], 
                                             wfr=wfr7, share_recycling=1, revenues=np.multiply(consumer7.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold7 = waste_comp.WasteStorage(sector_in=consumer7.getWastestorageSector(), sector_out=consumer7.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_7", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck7 = 10
s_wastetruck7 = 20
delay_wastetruck7 = math.ceil(s_wastetruck7/v_wastetruck7)
transmissionWaste7 = lsc_comp.Delay_Transport(sector_in=consumer7.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer7.input_data["wastetransmissioncosts"], delay=delay_wastetruck7, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_7', color='aquamarine')

transmissionWasteEmissions7 = lsc_comp.Transport_emissions(sector_in=consumer7.getEmissionsWastetruckSector(), sector_out=consumer7.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck7*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_7', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents7 = consumer_lsc.consumer_setup(  consumer=consumer7, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility7, wasteFlexibility=wasteFlexibility7, wasteStorageHousehold=wasteStorageHousehold7, transmissionWaste=transmissionWaste7, pv=pv, transmissionWasteEmissions=transmissionWasteEmissions7)

#Assign components to consumer and energy system
for component in consumercomponents7:
    my_energysystem.add(component.component())
    consumer7.addComponent(component)   
    

    

#---------------------- Consumer 8 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility8 = consumer8.get_willingness_for_waterflexibility()
willingness_for_flexibility8 = 0.250106
#willingness_for_flexibility8=consumer8.get_willingness_for_waterflexibility_costdependent(costs=consumer8.input_data['waterPipelineCosts'][0])
waterFlexibility8 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer8.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_8",
                                                       color='darkcyan', demand=consumer8.input_data['waterDemand'], wff=willingness_for_flexibility8,
                                                       costs_lsc=consumer8.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer8.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr8=consumer8.get_willingness_for_recycling()
wfr8=0.2027168
#wfr8 = consumer8.get_willingness_for_recycling_costdependent(costs=consumer8.input_data['wastetransmissioncosts'][0])
wasteFlexibility8 = lsc_comp.WasteFlexibility(sector=consumer8.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_8", color='forestgreen', waste=consumer8.input_data['waste'], 
                                             wfr=wfr8, share_recycling=1, revenues=np.multiply(consumer8.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold8 = waste_comp.WasteStorage(sector_in=consumer8.getWastestorageSector(), sector_out=consumer8.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_8", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck8 = 10
s_wastetruck8 = 20
delay_wastetruck8 = math.ceil(s_wastetruck8/v_wastetruck8)
transmissionWaste8 = lsc_comp.Delay_Transport(sector_in=consumer8.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer8.input_data["wastetransmissioncosts"], delay=delay_wastetruck8, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_8', color='aquamarine')

transmissionWasteEmissions8 = lsc_comp.Transport_emissions(sector_in=consumer8.getEmissionsWastetruckSector(), sector_out=consumer8.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck8*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_8', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents8 = consumer_lsc.consumer_setup(  consumer=consumer8, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility8, wasteFlexibility=wasteFlexibility8, wasteStorageHousehold=wasteStorageHousehold8, transmissionWaste=transmissionWaste8, transmissionWasteEmissions=transmissionWasteEmissions8)

#Assign components to consumer and energy system
for component in consumercomponents8:
    my_energysystem.add(component.component())
    consumer8.addComponent(component)   



#---------------------- Consumer 9 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility9 = consumer9.get_willingness_for_waterflexibility()
willingness_for_flexibility9 = 0.250106
#willingness_for_flexibility9=consumer9.get_willingness_for_waterflexibility_costdependent(costs=consumer9.input_data['waterPipelineCosts'][0])
waterFlexibility9 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer9.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_9",
                                                       color='darkcyan', demand=consumer9.input_data['waterDemand'], wff=willingness_for_flexibility9,
                                                       costs_lsc=consumer9.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer9.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr9=consumer9.get_willingness_for_recycling()
wfr9=0.2027168
#wfr9 = consumer9.get_willingness_for_recycling_costdependent(costs=consumer9.input_data['wastetransmissioncosts'][0])
wasteFlexibility9 = lsc_comp.WasteFlexibility(sector=consumer9.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_9", color='forestgreen', waste=consumer9.input_data['waste'], 
                                             wfr=wfr9, share_recycling=1, revenues=np.multiply(consumer9.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold9 = waste_comp.WasteStorage(sector_in=consumer9.getWastestorageSector(), sector_out=consumer9.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_9", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck9 = 10
s_wastetruck9 = 20
delay_wastetruck9 = math.ceil(s_wastetruck9/v_wastetruck9)
transmissionWaste9 = lsc_comp.Delay_Transport(sector_in=consumer9.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer9.input_data["wastetransmissioncosts"], delay=delay_wastetruck9, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_9', color='aquamarine')

transmissionWasteEmissions9 = lsc_comp.Transport_emissions(sector_in=consumer9.getEmissionsWastetruckSector(), sector_out=consumer9.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck9*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_9', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents9 = consumer_lsc.consumer_setup(  consumer=consumer9, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility9, wasteFlexibility=wasteFlexibility9, wasteStorageHousehold=wasteStorageHousehold9, transmissionWaste=transmissionWaste9, transmissionWasteEmissions=transmissionWasteEmissions9)

#Assign components to consumer and energy system
for component in consumercomponents9:
    my_energysystem.add(component.component())
    consumer9.addComponent(component)   
    

    
    
#---------------------- Consumer 10 Components ----------------------

#_____________ Electricity _____________
pv = el_comp.Photovoltaic(sector=consumer10.getElectricitySector(), generation_timeseries=consumer10.input_data['elGen'], 
                          label='PV_' + consumer10.label, timesteps=timesteps, area_efficiency=0.5, area_roof=30, area_factor=10) 

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility10 = consumer10.get_willingness_for_waterflexibility()
willingness_for_flexibility10 = 0.250106
#willingness_for_flexibility10=consumer10.get_willingness_for_waterflexibility_costdependent(costs=consumer10.input_data['waterPipelineCosts'][0])
waterFlexibility10 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer10.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_10",
                                                       color='darkcyan', demand=consumer10.input_data['waterDemand'], wff=willingness_for_flexibility10,
                                                       costs_lsc=consumer10.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer10.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr10=consumer10.get_willingness_for_recycling()
wfr10=0.2027168
#wfr10 = consumer10.get_willingness_for_recycling_costdependent(costs=consumer10.input_data['wastetransmissioncosts'][0])
wasteFlexibility10 = lsc_comp.WasteFlexibility(sector=consumer10.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_10", color='forestgreen', waste=consumer10.input_data['waste'], 
                                             wfr=wfr10, share_recycling=1, revenues=np.multiply(consumer10.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold10 = waste_comp.WasteStorage(sector_in=consumer10.getWastestorageSector(), sector_out=consumer10.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_10", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck10 = 10
s_wastetruck10 = 20
delay_wastetruck10 = math.ceil(s_wastetruck10/v_wastetruck10)
transmissionWaste10 = lsc_comp.Delay_Transport(sector_in=consumer10.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer10.input_data["wastetransmissioncosts"], delay=delay_wastetruck10, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_10', color='aquamarine')

transmissionWasteEmissions10 = lsc_comp.Transport_emissions(sector_in=consumer10.getEmissionsWastetruckSector(), sector_out=consumer10.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck10*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_10', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents10 = consumer_lsc.consumer_setup(  consumer=consumer10, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility10, wasteFlexibility=wasteFlexibility10, wasteStorageHousehold=wasteStorageHousehold10, transmissionWaste=transmissionWaste10, pv=pv, transmissionWasteEmissions=transmissionWasteEmissions10)

#Assign components to consumer and energy system
for component in consumercomponents10:
    my_energysystem.add(component.component())
    consumer10.addComponent(component)   
    

   
#---------------------- Consumer 11 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility11 = consumer11.get_willingness_for_waterflexibility()
willingness_for_flexibility11 = 0.250106
#willingness_for_flexibility11=consumer11.get_willingness_for_waterflexibility_costdependent(costs=consumer11.input_data['waterPipelineCosts'][0])
waterFlexibility11 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer11.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_11",
                                                       color='darkcyan', demand=consumer11.input_data['waterDemand'], wff=willingness_for_flexibility11,
                                                       costs_lsc=consumer11.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer11.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr11=consumer11.get_willingness_for_recycling()
wfr11=0.2027168
#wfr11 = consumer11.get_willingness_for_recycling_costdependent(costs=consumer11.input_data['wastetransmissioncosts'][0])
wasteFlexibility11 = lsc_comp.WasteFlexibility(sector=consumer11.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_11", color='forestgreen', waste=consumer11.input_data['waste'], 
                                             wfr=wfr11, share_recycling=1, revenues=np.multiply(consumer11.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold11 = waste_comp.WasteStorage(sector_in=consumer11.getWastestorageSector(), sector_out=consumer11.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_11", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck11 = 10
s_wastetruck11 = 20
delay_wastetruck11 = math.ceil(s_wastetruck11/v_wastetruck11)
transmissionWaste11 = lsc_comp.Delay_Transport(sector_in=consumer11.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer11.input_data["wastetransmissioncosts"], delay=delay_wastetruck11, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_11', color='aquamarine')

transmissionWasteEmissions11 = lsc_comp.Transport_emissions(sector_in=consumer11.getEmissionsWastetruckSector(), sector_out=consumer11.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck11*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_11', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents11 = consumer_lsc.consumer_setup(  consumer=consumer11, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility11, wasteFlexibility=wasteFlexibility11, wasteStorageHousehold=wasteStorageHousehold11, transmissionWaste=transmissionWaste11, transmissionWasteEmissions=transmissionWasteEmissions11)

#Assign components to consumer and energy system
for component in consumercomponents11:
    my_energysystem.add(component.component())
    consumer11.addComponent(component)   
    

    
#---------------------- Consumer 12 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
#willingness_for_flexibility12 = consumer12.get_willingness_for_waterflexibility()
willingness_for_flexibility12 = 0.250106
#willingness_for_flexibility12=consumer12.get_willingness_for_waterflexibility_costdependent(costs=consumer12.input_data['waterPipelineCosts'][0])
waterFlexibility12 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer12.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_12",
                                                       color='darkcyan', demand=consumer12.input_data['waterDemand'], wff=willingness_for_flexibility12,
                                                       costs_lsc=consumer12.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer12.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#wfr12=consumer12.get_willingness_for_recycling()
wfr12=0.2027168
#wfr12 = consumer12.get_willingness_for_recycling_costdependent(costs=consumer12.input_data['wastetransmissioncosts'][0])
wasteFlexibility12 = lsc_comp.WasteFlexibility(sector=consumer12.getWasteSector(), timesteps=timesteps,
                                             label="WasteFlexibility_Consumer_12", color='forestgreen', waste=consumer12.input_data['waste'], 
                                             wfr=wfr12, share_recycling=1, revenues=np.multiply(consumer12.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold12 = waste_comp.WasteStorage(sector_in=consumer12.getWastestorageSector(), sector_out=consumer12.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_12", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck12 = 10
s_wastetruck12 = 20
delay_wastetruck12 = math.ceil(s_wastetruck12/v_wastetruck12)
transmissionWaste12 = lsc_comp.Delay_Transport(sector_in=consumer12.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer12.input_data["wastetransmissioncosts"], delay=delay_wastetruck12, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_12', color='aquamarine')

transmissionWasteEmissions12 = lsc_comp.Transport_emissions(sector_in=consumer12.getEmissionsWastetruckSector(), sector_out=consumer12.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck12*52, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_12', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents12 = consumer_lsc.consumer_setup(  consumer=consumer12, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility12, wasteFlexibility=wasteFlexibility12, wasteStorageHousehold=wasteStorageHousehold12, transmissionWaste=transmissionWaste12, transmissionWasteEmissions=transmissionWasteEmissions12)

#Assign components to consumer and energy system
for component in consumercomponents12:
    my_energysystem.add(component.component())
    consumer12.addComponent(component)   

"""
#---------------------- Consumer 13 Components ----------------------

#_____________ Electricity _____________
#PV
pv13 = el_comp.Photovoltaic(sector=consumer13.getElectricitySector(), generation_timeseries=consumer13.input_data['elGen'], 
                          label='PV_' + consumer13.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10)             


#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility13 = consumer13.get_willingness_for_waterflexibility()
willingness_for_flexibility13 = 0.25
willingness_for_flexibility13=consumer13.get_willingness_for_waterflexibility_costdependent(costs=consumer13.input_data['waterPipelineCosts'][0])
waterFlexibility13 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer13.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_13",
                                                       color='darkcyan', demand=consumer13.input_data['waterDemand'], wff=willingness_for_flexibility13,
                                                       costs_lsc=consumer13.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer13.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr13=consumer13.get_willingness_for_recycling()
wfr13=0.3
wfr13 = consumer13.get_willingness_for_recycling_costdependent(costs=consumer13.input_data['wastetransmissioncosts'][0])
wasteFlexibility13 = lsc_comp.WasteFlexibility(sector=consumer13.getWasteSector(), timesteps=timesteps, revenues=consumer13.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_13", color='forestgreen', waste=consumer13.input_data['waste'], 
                                             wfr=wfr13, share_recycling=1, revenues=np.multiply(consumer13.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold13 = waste_comp.WasteStorage(sector_in=consumer13.getWastestorageSector(), sector_out=consumer13.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_13", color='mediumvioletred', balanced=False, empty_end=True, t_start=169)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 25km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 25
delay_wastetruck13 = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste13 = lsc_comp.Delay_Transport(sector_in=consumer13.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer13.input_data["wastetransmissioncosts"], delay=delay_wastetruck13, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_13', color='aquamarine')

transmissionWasteEmissions13 = lsc_comp.Transport_emissions(sector_in=consumer13.getEmissionsWastetruckSector(), sector_out=consumer13.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_13', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents13 = consumer_lsc.consumer_setup(  consumer=consumer13, LSC=LSC, timesteps=timesteps, red_local_elec=0.43, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility13, wasteFlexibility=wasteFlexibility13, wasteStorageHousehold=wasteStorageHousehold13, transmissionWaste=transmissionWaste13, pv=pv13, transmissionWasteEmissions=transmissionWasteEmissions13)

#Assign components to consumer and energy system
for component in consumercomponents13:
    my_energysystem.add(component.component())
    consumer13.addComponent(component)
    

#---------------------- Consumer 14 Components ----------------------

#_____________ Electricity _____________
#PV
pv14 = el_comp.Photovoltaic(sector=consumer14.getElectricitySector(), generation_timeseries=consumer14.input_data['elGen'], 
                          label='PV_' + consumer14.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10)             


#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility14 = consumer14.get_willingness_for_waterflexibility()
willingness_for_flexibility14 = 0.25
willingness_for_flexibility14=consumer14.get_willingness_for_waterflexibility_costdependent(costs=consumer14.input_data['waterPipelineCosts'][0])
waterFlexibility14 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer14.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_14",
                                                       color='darkcyan', demand=consumer14.input_data['waterDemand'], wff=willingness_for_flexibility14,
                                                       costs_lsc=consumer14.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer14.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr14=consumer14.get_willingness_for_recycling()
wfr14=0.3
wfr14 = consumer14.get_willingness_for_recycling_costdependent(costs=consumer14.input_data['wastetransmissioncosts'][0])
wasteFlexibility14 = lsc_comp.WasteFlexibility(sector=consumer14.getWasteSector(), timesteps=timesteps, revenues=consumer14.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_14", color='forestgreen', waste=consumer14.input_data['waste'], 
                                             wfr=wfr14, share_recycling=1, revenues=np.multiply(consumer14.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold14 = waste_comp.WasteStorage(sector_in=consumer14.getWastestorageSector(), sector_out=consumer14.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_14", color='mediumvioletred', balanced=False, empty_end=True, t_start=169)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 25km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 25
delay_wastetruck14 = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste14 = lsc_comp.Delay_Transport(sector_in=consumer14.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer14.input_data["wastetransmissioncosts"], delay=delay_wastetruck14, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_14', color='aquamarine')

transmissionWasteEmissions14 = lsc_comp.Transport_emissions(sector_in=consumer14.getEmissionsWastetruckSector(), sector_out=consumer14.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_14', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents14 = consumer_lsc.consumer_setup(  consumer=consumer14, LSC=LSC, timesteps=timesteps, red_local_elec=0.43, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility14, wasteFlexibility=wasteFlexibility14, wasteStorageHousehold=wasteStorageHousehold14, transmissionWaste=transmissionWaste14, pv=pv14, transmissionWasteEmissions=transmissionWasteEmissions14)

#Assign components to consumer and energy system
for component in consumercomponents14:
    my_energysystem.add(component.component())
    consumer14.addComponent(component)
    
 
#---------------------- Consumer 15 Components ----------------------

#_____________ Electricity _____________      


#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility15 = consumer15.get_willingness_for_waterflexibility()
willingness_for_flexibility15 = 0.25
willingness_for_flexibility15=consumer15.get_willingness_for_waterflexibility_costdependent(costs=consumer15.input_data['waterPipelineCosts'][0])
waterFlexibility15 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer15.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_15",
                                                       color='darkcyan', demand=consumer15.input_data['waterDemand'], wff=willingness_for_flexibility15,
                                                       costs_lsc=consumer15.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer15.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr15=consumer15.get_willingness_for_recycling()
wfr15=0.3
wfr15 = consumer15.get_willingness_for_recycling_costdependent(costs=consumer15.input_data['wastetransmissioncosts'][0])
wasteFlexibility15 = lsc_comp.WasteFlexibility(sector=consumer15.getWasteSector(), timesteps=timesteps, revenues=consumer15.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_15", color='forestgreen', waste=consumer15.input_data['waste'], 
                                             wfr=wfr15, share_recycling=1, revenues=np.multiply(consumer15.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold15 = waste_comp.WasteStorage(sector_in=consumer15.getWastestorageSector(), sector_out=consumer15.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_15", color='mediumvioletred', balanced=False, empty_end=True, t_start=169)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 25km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 25
delay_wastetruck15 = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste15 = lsc_comp.Delay_Transport(sector_in=consumer15.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer15.input_data["wastetransmissioncosts"], delay=delay_wastetruck15, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_15', color='aquamarine')

transmissionWasteEmissions15 = lsc_comp.Transport_emissions(sector_in=consumer15.getEmissionsWastetruckSector(), sector_out=consumer15.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_15', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents15 = consumer_lsc.consumer_setup(  consumer=consumer15, LSC=LSC, timesteps=timesteps, red_local_elec=0.43, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility15, wasteFlexibility=wasteFlexibility15, wasteStorageHousehold=wasteStorageHousehold15, transmissionWaste=transmissionWaste15, transmissionWasteEmissions=transmissionWasteEmissions15)

#Assign components to consumer and energy system
for component in consumercomponents15:
    my_energysystem.add(component.component())
    consumer15.addComponent(component)
    
    
#---------------------- Consumer 16 Components ----------------------

#_____________ Electricity _____________
#PV
pv16 = el_comp.Photovoltaic(sector=consumer16.getElectricitySector(), generation_timeseries=consumer16.input_data['elGen'], 
                          label='PV_' + consumer16.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10)             


#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility16 = consumer16.get_willingness_for_waterflexibility()
willingness_for_flexibility16 = 0.25
willingness_for_flexibility16=consumer16.get_willingness_for_waterflexibility_costdependent(costs=consumer16.input_data['waterPipelineCosts'][0])
waterFlexibility16 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer16.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_16",
                                                       color='darkcyan', demand=consumer16.input_data['waterDemand'], wff=willingness_for_flexibility16,
                                                       costs_lsc=consumer16.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer16.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr16=consumer16.get_willingness_for_recycling()
wfr16=0.3
wfr16 = consumer16.get_willingness_for_recycling_costdependent(costs=consumer16.input_data['wastetransmissioncosts'][0])
wasteFlexibility16 = lsc_comp.WasteFlexibility(sector=consumer16.getWasteSector(), timesteps=timesteps, revenues=consumer16.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_16", color='forestgreen', waste=consumer16.input_data['waste'], 
                                             wfr=wfr16, share_recycling=1, revenues=np.multiply(consumer16.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold16 = waste_comp.WasteStorage(sector_in=consumer16.getWastestorageSector(), sector_out=consumer16.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_16", color='mediumvioletred', balanced=False, empty_end=True, t_start=192)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 25km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 30
delay_wastetruck16 = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste16 = lsc_comp.Delay_Transport(sector_in=consumer16.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer16.input_data["wastetransmissioncosts"], delay=delay_wastetruck16, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_16', color='aquamarine')

transmissionWasteEmissions16 = lsc_comp.Transport_emissions(sector_in=consumer16.getEmissionsWastetruckSector(), sector_out=consumer16.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_16', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents16 = consumer_lsc.consumer_setup(  consumer=consumer16, LSC=LSC, timesteps=timesteps, red_local_elec=0.72, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility16, wasteFlexibility=wasteFlexibility16, wasteStorageHousehold=wasteStorageHousehold16, transmissionWaste=transmissionWaste16, pv=pv16, transmissionWasteEmissions=transmissionWasteEmissions16)

#Assign components to consumer and energy system
for component in consumercomponents16:
    my_energysystem.add(component.component())
    consumer16.addComponent(component)
    
    
#---------------------- Consumer 17 Components ----------------------

#_____________ Electricity _____________
#PV
pv17 = el_comp.Photovoltaic(sector=consumer17.getElectricitySector(), generation_timeseries=consumer17.input_data['elGen'], 
                          label='PV_' + consumer17.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10)             


#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility17 = consumer17.get_willingness_for_waterflexibility()
willingness_for_flexibility17 = 0.25
willingness_for_flexibility17=consumer17.get_willingness_for_waterflexibility_costdependent(costs=consumer17.input_data['waterPipelineCosts'][0])
waterFlexibility17 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer17.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_17",
                                                       color='darkcyan', demand=consumer17.input_data['waterDemand'], wff=willingness_for_flexibility17,
                                                       costs_lsc=consumer17.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer17.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr17=consumer17.get_willingness_for_recycling()
wfr17=0.3
wfr17 = consumer17.get_willingness_for_recycling_costdependent(costs=consumer17.input_data['wastetransmissioncosts'][0])
wasteFlexibility17 = lsc_comp.WasteFlexibility(sector=consumer17.getWasteSector(), timesteps=timesteps, revenues=consumer17.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_17", color='forestgreen', waste=consumer17.input_data['waste'], 
                                             wfr=wfr17, share_recycling=1, revenues=np.multiply(consumer17.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold17 = waste_comp.WasteStorage(sector_in=consumer17.getWastestorageSector(), sector_out=consumer17.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_17", color='mediumvioletred', balanced=False, empty_end=True, t_start=192)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 25km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 30
delay_wastetruck17 = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste17 = lsc_comp.Delay_Transport(sector_in=consumer17.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer17.input_data["wastetransmissioncosts"], delay=delay_wastetruck17, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_17', color='aquamarine')

transmissionWasteEmissions17 = lsc_comp.Transport_emissions(sector_in=consumer17.getEmissionsWastetruckSector(), sector_out=consumer17.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_17', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents17 = consumer_lsc.consumer_setup(  consumer=consumer17, LSC=LSC, timesteps=timesteps, red_local_elec=0.72, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility17, wasteFlexibility=wasteFlexibility17, wasteStorageHousehold=wasteStorageHousehold17, transmissionWaste=transmissionWaste17, pv=pv17, transmissionWasteEmissions=transmissionWasteEmissions17)

#Assign components to consumer and energy system
for component in consumercomponents17:
    my_energysystem.add(component.component())
    consumer17.addComponent(component)
    
    
#---------------------- Consumer 18 Components ----------------------

#_____________ Electricity _____________      


#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility18 = consumer18.get_willingness_for_waterflexibility()
willingness_for_flexibility18 = 0.25
willingness_for_flexibility18=consumer18.get_willingness_for_waterflexibility_costdependent(costs=consumer18.input_data['waterPipelineCosts'][0])
waterFlexibility18 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer18.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_18",
                                                       color='darkcyan', demand=consumer18.input_data['waterDemand'], wff=willingness_for_flexibility18,
                                                       costs_lsc=consumer18.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer18.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr18=consumer18.get_willingness_for_recycling()
wfr18=0.3
wfr18 = consumer18.get_willingness_for_recycling_costdependent(costs=consumer18.input_data['wastetransmissioncosts'][0])
wasteFlexibility18 = lsc_comp.WasteFlexibility(sector=consumer18.getWasteSector(), timesteps=timesteps, revenues=consumer18.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_18", color='forestgreen', waste=consumer18.input_data['waste'], 
                                             wfr=wfr18, share_recycling=1, revenues=np.multiply(consumer18.input_data['wasteCosts'], 1/4))

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold18 = waste_comp.WasteStorage(sector_in=consumer18.getWastestorageSector(), sector_out=consumer18.getEmissionsWastetruckSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_18", color='mediumvioletred', balanced=False, empty_end=True, t_start=192)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 25km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 30
delay_wastetruck18 = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste18 = lsc_comp.Delay_Transport(sector_in=consumer18.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=consumer18.input_data["wastetransmissioncosts"], delay=delay_wastetruck18, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_18', color='aquamarine')

transmissionWasteEmissions18 = lsc_comp.Transport_emissions(sector_in=consumer18.getEmissionsWastetruckSector(), sector_out=consumer18.getWastedisposalSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=800, distance=s_wastetruck, emissions=0.125,
                                        label='WastetransportEmissions_Consumer_18', color='azure')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents18 = consumer_lsc.consumer_setup(  consumer=consumer18, LSC=LSC, timesteps=timesteps, red_local_elec=0.72, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility18, wasteFlexibility=wasteFlexibility18, wasteStorageHousehold=wasteStorageHousehold18, transmissionWaste=transmissionWaste18, transmissionWasteEmissions=transmissionWasteEmissions18)

#Assign components to consumer and energy system
for component in consumercomponents18:
    my_energysystem.add(component.component())
    consumer18.addComponent(component)

"""

#---------------------- LSC Components ----------------------

#Heatpump
heatpumpLSC = el_comp.Heatpump(sector_in = LSC.getElectricitySector(), sector_out=LSC.getHeatSector(), 
                            conversion_timeseries=LSC.input_data['copHP'], timesteps=timesteps, P_in=100, P_out=100, label='Heatpump_LSC') 

#Cooler
elcoolLSC = el_comp.ElectricCooling(sector_in=LSC.getElectricitySector(), sector_out=LSC.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=LSC.input_data['copCool'], P_in=100, P_out=100, label="Cooler_LSC")


#Sludge Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,5€/m³ 
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 20m³ 
#Distance 20km, Average Speed 60km/h --> 20min --> round up --> 1 timestep
v_sludgetruck = 60
s_sludgetruck = 20
delay_sludgetruck = math.ceil(s_sludgetruck/v_sludgetruck)
sludgetruckLSC = lsc_comp.Delay_Transport(sector_in=LSC.getSludgestorageSector(), sector_out=LSC.getSludgetreatmentSectorIn(), 
                                        efficiency=1, costs_transmission=LSC.input_data["wastetransmissioncosts"], delay=delay_sludgetruck, timesteps=timesteps, capacity_transmission=20,
                                        label='Sludgetransport_LSC', color='aquamarine')

sludgetruckEmissions = lsc_comp.Transport_emissions(sector_in=LSC.getEmissionsSludgetruckSector(), sector_out=LSC.getSludgestorageSector(), sector_emissions=LSC.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=20, distance=s_sludgetruck*365, emissions=0.125,
                                        label='SludgetransportEmissions_LSC', color='azure')


#_____________ LSC Standard Definition _____________

#Consumer definition with function
lscComponents = consumer_lsc.lsc_setup(LSC=LSC, timesteps=timesteps, sludgetruck = sludgetruckLSC, heatpump=heatpumpLSC, elcool=elcoolLSC, sludgetruckEmissions=sludgetruckEmissions, co2price=0)

#Assign components to consumer and energy system
for component in lscComponents:
    my_energysystem.add(component.component())
    LSC.addComponent(component)
    
    
#_____________ Optional LSC Exhaust Heat _____________

#District Heat Grid Sink
exhaustHeatLSC = heat_comp.GridSink(sector=LSC.getHeatSector(), label='Exhaustheat_LSC', P=100000, costs_model=0, color='slategrey')        
my_energysystem.add(exhaustHeatLSC.component())
LSC.addComponent(exhaustHeatLSC)
    
 
#_____________ LSC Transport Demand _____________

lscTransportDemand = consumer_lsc.lsc_transportdemand(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, consumer10, consumer11, consumer12], LSC=LSC)

#Assign components to consumer and energy system
for component in lscTransportDemand:
    my_energysystem.add(component.component())
    if('Consumer_10' in component.label):
        consumer10.addComponent(component)
    elif('Consumer_11' in component.label):
        consumer11.addComponent(component)
    elif('Consumer_12' in component.label):
        consumer12.addComponent(component)
    elif('Consumer_1' in component.label):
        consumer1.addComponent(component)
    elif('Consumer_2' in component.label):
        consumer2.addComponent(component)
    elif('Consumer_3' in component.label):
        consumer3.addComponent(component)
    elif('Consumer_4' in component.label):
        consumer4.addComponent(component)
    elif('Consumer_5' in component.label):
        consumer5.addComponent(component)
    elif('Consumer_6' in component.label):
        consumer6.addComponent(component)
    elif('Consumer_7' in component.label):
        consumer7.addComponent(component)
    elif('Consumer_8' in component.label):
        consumer8.addComponent(component)
    elif('Consumer_9' in component.label):
        consumer9.addComponent(component)
    else:
        LSC.addComponent(component)


#_____________ LSC Vehicles _____________

lsc_vehicles = consumer_lsc.lsc_vehicles(LSC=LSC, timesteps=timesteps)

#Assign components to consumer and energy system
for component in lsc_vehicles:
    my_energysystem.add(component.component())
    my_energysystem.add(component.drive())
    LSC.addComponent(component)


#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 

print("----------------Energy System configured----------------")


#---------------------- Additional constraints ----------------------

#Prevent Charging and Discharging at same time
saka = constraints.waterstorage_chargeblock(model=saka, storage=consumer_lsc.get_component_by_name(components=LSC.components, name='Waterpoolstorage'))


#Prevent Charging and Discharging at same time
#saka = constraints.battery_chargeblock(model=saka, battery=consumer_lsc.get_component_by_name(components=LSC.components, name='Heatstorage_LSC'))
#saka = constraints.waterstorage_chargeblock(model=saka, storage=waterPoolStorage)

vehiclelist = []
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='KiaSoul1'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='KiaSoul2'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='NissanLeaf1'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='NissanLeaf2'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='RenaultZoe1'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='RenaultZoe2'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='VW1'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='VW2'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='KiaSoul3'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='NissanLeaf3'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='RenaultZoe3'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='VW3'))

saka = constraints.transport_decision_EVs_flex(vehicles=vehiclelist, componentName='TransportBinaryEV', timesteps=timesteps, model=saka,
                                                      demands=[consumer1.input_data['transportDemand'], consumer2.input_data['transportDemand'], consumer3.input_data['transportDemand'],
                                                               consumer4.input_data['transportDemand'], consumer5.input_data['transportDemand'], consumer6.input_data['transportDemand'],
                                                               consumer7.input_data['transportDemand'], consumer8.input_data['transportDemand'], consumer9.input_data['transportDemand'],
                                                               consumer10.input_data['transportDemand'], consumer11.input_data['transportDemand'], consumer12.input_data['transportDemand']])

#Washing Maschine Constraint
washingList = []
washing = (consumer_lsc.get_component_by_name(components=LSC.components, name='ElecHeatWashingMaschine_LSC')).outer
washingElecVar = consumer_lsc.get_component_by_name(components=LSC.components, name='ElecHeatWashingMaschine_LSC')
washingThermVar = consumer_lsc.get_component_by_name(components=LSC.components, name='ThermHeatWashingMaschine_LSC')

#Heatsum for Washing Maschine
saka = washing.heating_sum(elec_heat=washingElecVar, eta=washing.eta_elec, therm_heat=washingThermVar, consumer=LSC, water=washing.water, 
                                  Twash=washing.Twash, Tref=washing.Tref, energy=washing.var_th/10, model=saka)

"""
#Activation of Excess Pipeline purchase only if casual purchase is exceeded
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=consumer_lsc.get_component_by_name(components=consumer1.components, name='PipelinepurchaseLimited'), 
                                                      pipeline_excess=consumer_lsc.get_component_by_name(components=consumer1.components, name='PipelinepurchaseExcess'))
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=consumer_lsc.get_component_by_name(components=consumer2.components, name='PipelinepurchaseLimited'), 
                                                      pipeline_excess=consumer_lsc.get_component_by_name(components=consumer2.components, name='PipelinepurchaseExcess'))
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=consumer_lsc.get_component_by_name(components=consumer3.components, name='PipelinepurchaseLimited'), 
                                                      pipeline_excess=consumer_lsc.get_component_by_name(components=consumer3.components, name='PipelinepurchaseExcess'))
"""


#--------------------------------------------------------------------

"""
if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    saka.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, saka, scenario)


results = meth.restore_results(scenario)

"""
print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
 
    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results(my_energysystem, saka, scenario)
#results = solph.processing.results(saka)
results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")

#consumerList=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, consumer10, consumer11, consumer12, consumer13, LSC]

#Store the costs
resultprocessing_lsc.costs_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                             consumer10, consumer11, consumer12, LSC], 
                                  results=results, filename='costs.csv', scenarioname=scenario, model=saka, timeseries=False)

#Store the results
resultprocessing_lsc.consumer_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                                consumer10, consumer11, consumer12, LSC], 
                                     results=results, filename='technology.csv', scenarioname=scenario)


print("----------------Results saved----------------")



#__________________________ Result Total Costs of the Previous Run ________________________________________
#Just a comment - no code
#Total Costs ohne NB mit Big M = 5206.62
#Total Costs Run 1 mit Big M = 4932.75

