# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 08:35:19 2022

@author: Matthias Maldet
"""

import plots_phase2 as plots
import pandas as pd




def run_plots_phase2(timesteps, plotlib, filename, sectors, units, resultdirectory):
    
    #creating the filename
    filename_input = plotlib + '/' + filename

    #reading of the dataframe to plot
    dataframe_to_plot = pd.ExcelFile(filename_input)

    #Sheetnames of the dataframe = consumers
    sheetnames = dataframe_to_plot.sheet_names    

    #create the folders
    plots.create_folders(resultdirectory, sectors, sheetnames)


    
    sheetnames.remove('Objective')
    sheetnames.remove('LSC')

    #____________________________ SANKEY ________________________________________________
    dataframes=[]
    #Plotting the sankey diagrame
    consumer_index=0
    for consumer in sheetnames:    
        #Consumer_1 data for plot testing
        dataframe_consumer = dataframe_to_plot.parse(sheetnames[consumer_index])
        dataframes.append(dataframe_consumer)
    
        listnumber=0
        for sector in sectors:
            plots.plot_sankey(sectorname=sector, dataframe=dataframe_consumer, consumer=consumer, directory=resultdirectory, unit=units[listnumber])
            listnumber+=1
   
        consumer_index+=1
    


    listnumber=0
    for sector in sectors:
        plots.plot_sankeyLSC(sectorname=sector, dataframes=dataframes, dataframe = dataframe_to_plot.parse('LSC'), consumer='LSC', directory=resultdirectory, unit=units[listnumber])
        listnumber+=1
      
    consumer_index=0  
    dataframes=[]
    for consumer in sheetnames:    
        #Consumer_1 data for plot testing
        dataframe_consumer = dataframe_to_plot.parse(sheetnames[consumer_index])
        dataframes.append(dataframe_consumer)
        plots.plot_sankey_emissions(dataframe=dataframe_consumer, consumer=consumer, directory=resultdirectory)
        consumer_index+=1

    
    #____________________________ Bar chart LSC share ________________________________________________

    sectorsBar = ['Electricitysector', 'Heatsector', 'Coolingsector', 'Waterdemandsector']
    consumersBar = sheetnames

    for consumer in sheetnames:    
        #Consumer_1 data for plot testing
        dataframe_consumer = dataframe_to_plot.parse(consumer)
        plots.plot_lscshareSector_bar(sectors=sectorsBar, dataframe=dataframe_consumer, consumer=consumer, directory=resultdirectory)
    
    
    for sector in sectorsBar:    
        #Consumer_1 data for plot testing
        plots.plot_lscshareConsumer_bar(sectorname=sector, dataframes=dataframe_to_plot, consumers=consumersBar, directory=resultdirectory)
      
  
      
    #____________________________ Heatmap ________________________________________________
    
    sectorsHeatmap = ['Electricitysector', 'Heatsector', 'Coolingsector', 'Waterdemandsector']    
    column_names = ['Electricitysector', 'Heatsector', 'Coolingsector', 'Waterdemandsector']

    dfIn = pd.DataFrame(columns = column_names)
    dfOut = pd.DataFrame(columns = column_names)

    for consumer in sheetnames:    
        #Consumer_1 data for plot testing
        dataframe_consumer = dataframe_to_plot.parse(consumer)
        dfIn = plots.get_dataframe_heatmap_in(sectors=sectorsHeatmap, dataframe=dataframe_consumer, consumer=consumer, dataframe_in=dfIn)
        dfOut = plots.get_dataframe_heatmap_out(sectors=sectorsHeatmap, dataframe=dataframe_consumer, consumer=consumer, dataframe_out=dfOut)
     
    dfIn=dfIn.round(2)
    dfOut=dfOut.round(2)
    
    #Plot Input 
    plots.plot_heatmap(title="LSC to Consumer Input", dataframe=dfIn, columns=column_names, in_out='in', directory=resultdirectory)
    #Plot Output
    plots.plot_heatmap(title="Consumer to LSC Output", dataframe=dfOut, columns=column_names, in_out='out', directory=resultdirectory)

 
    
    #____________________________ Load Curves ________________________________________________
    
    loadcurvelist = sheetnames
    loadcurvelist.append('LSC')
    
    for consumer in loadcurvelist:    
        #Consumer_1 data for plot testing
        dataframe_consumer = dataframe_to_plot.parse(consumer)
        plots.plot_loadcurve(dataframe=dataframe_consumer, directory=resultdirectory)
    loadcurvelist.remove('LSC')
    
    
    #____________________________ Storage Plots ________________________________________________
    consumersStorage = sheetnames
    consumersStorage.append('LSC')
    plots.plot_storages(sectors=sectors, dataframes=dataframe_to_plot, consumers=consumersStorage, directory=resultdirectory, timesteps=timesteps, frequency='24H')
    consumersStorage.remove('LSC')

    
    #____________________________ Waste and Sludge Sankey ________________________________________________
    plots.plot_waste_sludge_sankey(directory=resultdirectory, dataframes=dataframe_to_plot)

    
    #____________________________ Vehicle heatmap ________________________________________________
    consumersTransport = sheetnames
    
    plots.plot_transport(dataframes=dataframe_to_plot, consumers=consumersTransport, directory=resultdirectory)
    
    
    #____________________________ Peakpower Loadcurves ________________________________________________
    consumersPeakpower = sheetnames
    consumersPeakpower.append('LSC')
    plots.plot_peakpowerloadcurve(dataframes=dataframe_to_plot, consumers=consumersPeakpower, directory=resultdirectory)
    consumersPeakpower.remove('LSC')
    
    
    #____________________________ Flexibility Barcharts ________________________________________________
    consumersFlexibility = sheetnames
    plots.plot_flexibilitybar(dataframes=dataframe_to_plot, consumers=consumersFlexibility, directory=resultdirectory)
    
    
    #____________________________ Own Consumption Bar Charts ________________________________________________
    consumersOwnconsumption = sheetnames
    consumersOwnconsumption.append('LSC')
    plots.plot_pvconsumption(dataframes=dataframe_to_plot, consumers=consumersOwnconsumption, directory=resultdirectory)
    consumersOwnconsumption.remove('LSC')
    
    
    #____________________________ Waste and Sludge Delay Count ________________________________________________
    consumersDelay = sheetnames
    consumersDelay.append('LSC')
    plots.plot_transportdelay(dataframes=dataframe_to_plot, consumers=consumersDelay, directory=resultdirectory)
    consumersDelay.remove('LSC')
    
    
    #____________________________ Waterpool Sankey ________________________________________________
    consumersWaterpool = sheetnames
    consumersWaterpool.append('LSC')
    plots.plot_pool_sankey(dataframes=dataframe_to_plot, consumers=consumersWaterpool, directory=resultdirectory)
    consumersWaterpool.remove('LSC')
    
  
    #____________________________ Scatter Trading ________________________________________________
    sectorsScatter = ['Electricitysector', 'Heatsector', 'Coolingsector', 'Waterdemandsector']
    consumersScatter = sheetnames
    
        
    for sector in sectorsScatter:    
        #Consumer_1 data for plot testing
        plots.plot_scatter(sectorname=sector, dataframes=dataframe_to_plot, consumers=consumersScatter, directory=resultdirectory)

    #____________________________ Scatter Trading Sorted ________________________________________________
    sectorsScatterSorted = ['Electricitysector', 'Heatsector', 'Coolingsector', 'Waterdemandsector']
    consumersScatterSorted = sheetnames
    
        
    for sector in sectorsScatterSorted:    
        #Consumer_1 data for plot testing
        plots.plot_scatter_sorted(sectorname=sector, dataframes=dataframe_to_plot, consumers=consumersScatterSorted, directory=resultdirectory)
        
  
    #____________________________ Emissions Barchart ________________________________________________
    consumersEmissions = sheetnames
    consumersEmissions.append('LSC')
    plots.bar_emissions(dataframes=dataframe_to_plot, consumers=consumersEmissions, directory=resultdirectory)
    consumersEmissions.remove('LSC')
    


def run_plots_phase2_violin(timesteps, plotlib, filename, sectors, units, resultdirectory):

    #creating the filename
    filename_input = plotlib + '/' + filename

    #reading of the dataframe to plot
    dataframe_to_plot = pd.ExcelFile(filename_input)
    dataframe_to_plot_violin = dataframe_to_plot

    #Sheetnames of the dataframe = consumers
    sheetnames = dataframe_to_plot.sheet_names    

    #create the folders
    plots.create_folders(resultdirectory, sectors, sheetnames)


    
    sheetnames.remove('Objective')
    sheetnames.remove('LSC')


    #____________________________ Violin Plots ________________________________________________
    
    sectorsViolin = ['Electricitysector', 'Heatsector', 'Coolingsector', 'Waterdemandsector']
    consumersViolinNumber = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13']
    consumersViolin = sheetnames
    
        
    for sector in sectorsViolin:    
        #Consumer_1 data for plot testing
        dfOut = pd.DataFrame(columns = consumersViolinNumber)
        dfIn = pd.DataFrame(columns = consumersViolinNumber)
        plots.plot_violinSector(sectorname=sector, dataframes=dataframe_to_plot_violin, consumers=consumersViolin, directory=resultdirectory, dataframeOut=dfOut, dataframeIn=dfIn)
 
    
    
def run_plots_phase2_costs(timesteps, plotlib, filename_costs, sectors, units, resultdirectory):
    #creating the filename
    filename_input = plotlib + '/' + filename_costs

    #reading of the dataframe to plot
    dataframe_to_plot = pd.ExcelFile(filename_input)

    #Sheetnames of the dataframe = consumers
    sheetnames = dataframe_to_plot.sheet_names   
    
    #create the folders
    plots.create_folders(resultdirectory, sectors, sheetnames)
    
    #plot function for costs
    plots.plot_costs(consumers=sheetnames, dataframes=dataframe_to_plot, sectors=sectors, directory=resultdirectory)

    
    
    

#____________________________ Main Programme ________________________________________________

#Data to be defined before plotting
timesteps = 8784
plotlib = 'results/scenario_phase2_addCons'
filename = 'technology_addCons.xlsx'
filename_costs = 'costs_addCons.xlsx'
resultdirectory = 'results/scenario_phase2_addCons'

#List with sectors to consider
sectors = ['Electricitysector', 'Heatsector', 'HotWater', 'Coolingsector', 'Potablewatersector', 'Waterpoolsector', 'Waterdemandsector', 'Sewagesector', 
           'Sludgesector', 'Waterrecoverysector', 'Sludgestoragesector', 'SludgetreatmentsectorIN', 'SludgetreatmentsectorOUT', 'Wastesector', 'Wastestoragesector', 
           'Wastedisposalsector', 'Mobilitysector', 'Waterwashingsector', 'Electricvehicle1sector', 'Electricvehicle2sector', 'Electricvehicle3sector', 'Electricvehicle4sector',
           'Electricvehicle5sector', 'Electricvehicle6sector', 'Electricvehicle7sector', 'Electricvehicle8sector', 'Electricvehicle9sector', 'Electricvehicle10sector',
           'Electricvehicle11sector', 'Electricvehicle12sector',
           'Emissionsector', 'EmissionsWastetrucksector', 'Emissionssludgetrucksector']


units = ['kWh', 'kWh', 'kWh', 'kWh', 'm³', 'm³', 'm³', 'm³', 
           'm³', 'm³', 'm³', 'm³', 'm³', 'kg', 'kg', 
           'kg', 'km', 'm³', 'km', 'km', 'km', 'km','km','km','km','km','km','km','km','km', 'kg', 'kg', 'm³']

run_plots_phase2(timesteps, plotlib, filename, sectors, units, resultdirectory)  
run_plots_phase2_violin(timesteps, plotlib, filename, sectors, units, resultdirectory) 


sectorsCosts=sectors
#sectorsCosts.append('Electricitygridsector')
sectorsCosts.append('Peakpowersector')

run_plots_phase2_costs(timesteps, plotlib, filename_costs, sectorsCosts, units, resultdirectory) 