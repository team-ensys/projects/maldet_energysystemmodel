# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 07:49:07 2023

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc

import investment_components as investment_comp
import constraints_LSM as constraints_LSM
import sdg_methods as sdg_methods
import constraints_sdg as constraints_sdg

import pyomo.environ as pyo


# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'sdg_lsc_market_sdgTotal_expensive'

#Filename
filename = "input_data_sdg.xlsx"

#Scenario message
scenariomessage="Test scenario UN SDG market"


#Timestep for Optimisation
deltaT = 1

#Timesteps Considered --> default clustering 348, default operational 8784
timesteps=8784
#timesteps_operational = 8784
timestepsTotal = timesteps


data=pd.ExcelFile(filename)

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 


#Business as usual values
em_bau=19807.851
c_bau=23766


#Incentives
greywatercosts=520000
sewagedisposal=10.9
districtheatAddition=0.038
heatpumpRed=0
wasteaddition=0.15
CO2add=0.07



# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1

yearWeightFactor=timesteps/timestepsTotal


#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2


#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)

#Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)


#---------------------- DEFINITION OF CONSUMERS ----------------------
#LSC1 / LSM1
lsc = Consumer.Consumer(timesteps=timesteps, data=data, label='LSC')
lsc.addElectricitySector()
lsc.addHeatSector()
lsc.addDistrictheatingSector()
lsc.addWaterSector()
lsc.addWasteSector()
lsc.addEmissionSector()
lsc.sector2system(my_energysystem)

print("----------------Consumer Data loaded----------------")


# ---------------------------- Electricity Sector ------------------------------------------

#PV
pv_invest = investment_comp.Photovoltaic_Investment_simple(sector=lsc.getElectricitySector(), generation_timeseries=lsc.input_data['elGen'], 
                          label='PV_' + lsc.label, timesteps=timesteps, area_efficiency=1, area_roof=300, 
                          area_factor=10, timestepsTotal=timestepsTotal)
my_energysystem.add(pv_invest.component())
lsc.addComponent(pv_invest)


#Electricity grid purchase
electricitygridpurchase = el_comp.GridEmissions(sector=lsc.getElectricitySector(), sector_emissions=lsc.getEmissionsSector(),
                                                costs_energy=lsc.input_data['electricityCosts'], costs_model=lsc.input_data['electricityCosts'], 
                                                label='Electricitygridpurchase_' + lsc.label, timesteps=timesteps, 
                                                costs_power=0, emissions=0.209, P=1000)
my_energysystem.add(electricitygridpurchase.component())
lsc.addComponent(electricitygridpurchase)


#Electricity feedin
electricitygridFeedin = el_comp.GridFeedin(sector=lsc.getElectricitySector(), revenues=lsc.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + lsc.label, timesteps=timesteps, P=1000)
my_energysystem.add(electricitygridFeedin.component())
lsc.addComponent(electricitygridFeedin)  
    

#Electricity demand
electricitydemand = el_comp.Demand(sector=lsc.getElectricitySector(), 
                                       demand_timeseries=lsc.input_data['electricityDemand'], label='Electricitydemand_' + lsc.label)
my_energysystem.add(electricitydemand.component())
lsc.addComponent(electricitydemand)

#Battery
battery_invest = investment_comp.Battery_Investment_simple(sector_in=lsc.getElectricitySector(), label='Battery_' + lsc.label,
                          soc_max=30, SOC_start=0, P_in=30, P_out=30, timestepsTotal=timestepsTotal, timesteps=timesteps)
my_energysystem.add(battery_invest.component())
lsc.addComponent(battery_invest)


    
# ---------------------------- Heat Sector ------------------------------------------

#Heat pump
heatpump_invest = investment_comp.Heatpump_Investment_simple(sector_in = lsc.getElectricitySector(), sector_out=lsc.getHeatSector(), 
                            conversion_timeseries=lsc.input_data['copHP'], timesteps=timesteps, P_in=30, P_out=30, 
                            label='Heatpump_' + lsc.label, timestepsTotal=timestepsTotal, c_inv_var=945-heatpumpRed)
my_energysystem.add(heatpump_invest.component())
lsc.addComponent(heatpump_invest)
    

#District heat investment
districtheat_invest = investment_comp.DistrictHeat_Investment_simple(sector_in = lsc.getDistrictheatingSector(), sector_out=lsc.getHeatSector(), 
                        timesteps=timesteps, P=30, label='Districtheatinvest_' + lsc.label, timestepsTotal=timestepsTotal)
my_energysystem.add(districtheat_invest.component())
lsc.addComponent(districtheat_invest)


#External heat source
dhPurchase = heat_comp.GridEmissions(sector=lsc.getDistrictheatingSector(), sector_emissions=lsc.getEmissionsSector(), 
                                         P=30, emissions=0.188, costs_model=lsc.input_data["heatCosts"][0]+districtheatAddition, costs=lsc.input_data["heatCosts"],
                                         label="Districtheatsource_" + lsc.label, color="royalblue", timerange=deltaT)
my_energysystem.add(dhPurchase.component())
lsc.addComponent(dhPurchase) 


#Heat demand
heatdemand = heat_comp.Demand(sector=lsc.getHeatSector(), demand_timeseries=lsc.input_data['heatDemand'], 
                                  label='Heatdemand_' + lsc.label)
my_energysystem.add(heatdemand.component())
lsc.addComponent(heatdemand)
   

# ---------------------------- Water Sector ------------------------------------------

#Water pipeline purchase
pipelinepurchase = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=lsc.getPotablewaterSector(), 
                                                      label="PipelinepurchaseLimited_" + lsc.label, color='purple',
                                                      demand=lsc.input_data['waterDemand'], limit=1, 
                                                      costs_water=lsc.input_data['waterPipelineCosts'], discount=0)
my_energysystem.add(pipelinepurchase.component())
lsc.addComponent(pipelinepurchase)


#Water demand
waterdemand = water_comp.Waterdemand(sector_in=lsc.getPotablewaterSector(), sector_out=lsc.getSewageSector(), 
                                         demand=lsc.input_data['waterDemand'], timesteps=timesteps,
                                         label="Waterdemand_" + lsc.label, color='yellow')
my_energysystem.add(waterdemand.component())
lsc.addComponent(waterdemand)


#Water reduction    
waterReduction = lsc_comp.WaterFlexibilitySimple(timesteps=timesteps, sector_in=lsc.getPotablewaterSector(), label="Waterreduction_" + lsc.label,
                                                       color='darkcyan', demand=lsc.input_data['waterDemand'], wff=0,
                                                       revenues_consumer=0)
my_energysystem.add(waterReduction.component())
lsc.addComponent(waterReduction)


# ---------------------------- Sewage Sector ------------------------------------------

#Sewage disposal
sewage_disposal = water_comp.Sewagedisposal(share_sewage=0.95, V_max=1000000, sector=lsc.getSewageSector(), 
                                            label="Sewagedisposal_" + lsc.label, timesteps=timesteps, sector_emissions=lsc.getEmissionsSector(),
                                            emissions=0.3, max_emissions=1000000, costs=lsc.input_data["sewageCosts"][0]+ sewagedisposal, color="black")
my_energysystem.add(sewage_disposal.component())
lsc.addComponent(sewage_disposal)


# ---------------------------- Greywater Sector ------------------------------------------

#Sewage to greywater
sewage2greywater = investment_comp.Sewage2Greywater_Investment(waterdemand=lsc.input_data["waterDemand"], sector_in=lsc.getSewageSector(), sector_out=lsc.getGreywaterSector(),
                                                   label="Sewage2Greywater_" + lsc.label, share_greywater=0.5, min_greywater=0, c_inv_var=greywatercosts)
my_energysystem.add(sewage2greywater.component())
lsc.addComponent(sewage2greywater)
    
    
#Greywater coverage of water demand
greywater2water = water_comp.Greywater2water(waterdemand=lsc.input_data["waterDemand"], sector_in=lsc.getGreywaterSector(), sector_out=lsc.getPotablewaterSector(),
                                                   label="Greywater2water_" + lsc.label, max_greywater2water=0.5, greywatermax=0.15)
my_energysystem.add(greywater2water.component())
lsc.addComponent(greywater2water)


# ---------------------------- Waste Sector ------------------------------------------

#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=lsc.getWasteSector(), waste_timeseries=lsc.input_data['waste'], 
                                        label='WasteAccruing_' + lsc.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
lsc.addComponent(wasteAccruing)


#Waste recycling
wasteRecycling = waste_comp.Wastedisposal(volume_max=1000000, sector=lsc.getWasteSector(), min_disposal=0, label="Wasterecycling_" + lsc.label,
                                          timesteps=timesteps, sector_emissions=lsc.getEmissionsSector(), emissions=0,
                                          max_emissions=1000000, costs=0.38, color="red")
my_energysystem.add(wasteRecycling.component())
lsc.addComponent(wasteRecycling)


#Waste disposal
wasteDisposal = waste_comp.Wastedisposal(volume_max=1000000, sector=lsc.getWasteSector(), min_disposal=0, label="Wastedisposal_" + lsc.label,
                                          timesteps=timesteps, sector_emissions=lsc.getEmissionsSector(), emissions=0.125,
                                          max_emissions=1000000, costs=0.23+wasteaddition, color="red")
my_energysystem.add(wasteDisposal.component())
lsc.addComponent(wasteDisposal)


#Waste reduction
wasteReduction=waste_comp.WasteReduction(M_max=10000000, sector=lsc.getWasteSector(), label="Wastereduction_" + lsc.label, 
                                         timesteps=timesteps, color="tan", reduction_rate=0, min_reduction=0,
                                         wastereduction_costs=0, timeseries_waste=lsc.input_data['waste'])
my_energysystem.add(wasteReduction.component())
lsc.addComponent(wasteReduction)


#Waste storage
wasteStorage = waste_comp.WasteStorage(sector_in=lsc.getWasteSector(), sector_out=lsc.getWasteSector(), 
                                       volume_max=2.85, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + lsc.label, color='mistyrose', balanced=True, empty_end=True)
my_energysystem.add(wasteStorage.component())
lsc.addComponent(wasteStorage)
    


#Waste storage extension
wasteStorage_extension = investment_comp.WasteStorage_Investment_simple(sector_in=lsc.getWasteSector(), sector_out=lsc.getWasteSector(), 
                                       volume_max=8.51, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="WastestorageExtension_" + lsc.label, color='chocolate', balanced=True, timestepsTotal=timestepsTotal)
my_energysystem.add(wasteStorage_extension.component())
lsc.addComponent(wasteStorage_extension)


# ---------------------------- Emission Sector ------------------------------------------

#Total emissions
emissions_sink = em_comp.Emissions(sector=lsc.getEmissionsSector(), costs=0.03+CO2add, label='Emissiontotal_' + lsc.label)
my_energysystem.add(emissions_sink.component())
lsc.addComponent(emissions_sink)



#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 




# ---------------------------- Additional constraints ------------------------------------------



#del saka.dual
#saka.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT)
#saka.receive_duals()


print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")

    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results(my_energysystem, saka, scenario)
#results = solph.processing.results(saka)
results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")


# ---------------------------- Save the results ------------------------------------------
#Investment capacities
results_investment=sdg_methods.get_investment_results_lsc(consumer=lsc, results=results)
filename_save = scenario + "/investment.txt"
sdg_methods.write_dict(datalist=[results_investment], consumerlist=["LSC"], filename=filename_save)

#Investment costs
filename_save = scenario + "/investment_costs.txt"
sdg_methods.write_investmentcosts(datalist=[results_investment], consumerlist=["LSC"], filename=filename_save)

#Objective
filename_save = scenario + "/objective.txt"
meth.write_objective(scenario=scenario, scenariomessage=scenariomessage, objective=saka.objective(), filename=filename_save)

#SDG contribution
sdgcontribution = sdg_methods.get_sdg_lsc(results=results, model=saka, consumer=lsc, cost_old=c_bau, em_old=em_bau, renElgrid=0.8, renDhgrid=0.33)
filename_save = scenario + "/sdgcontribution.txt"
sdg_methods.write_dict(datalist=[sdgcontribution], consumerlist=["LSC"], filename=filename_save)

#Dual variables
#dualvar = sdg_methods.get_duals_struct(model=saka)
#filename_save = scenario + "/dualvar.txt"
#sdg_methods.write_dict(datalist=[dualvar], consumerlist=["LSC"], filename=filename_save)

#Time series
meth.results_to_excel(consumers=[lsc], results=results, filename=scenario)

print("----------------Results written----------------")

