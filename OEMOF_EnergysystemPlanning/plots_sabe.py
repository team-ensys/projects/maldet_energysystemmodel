# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 07:56:34 2021

@author: Matthias Maldet
"""

import pandas as pd
import os as os
#import shutil as shutil
#import sys as sys
import matplotlib.pyplot as plt
import matplotlib.sankey as msank
import numpy as np
import oemof.solph as solph
import pandas as pd


DPI = 300

def create_folders(scenario):
    #plotting the results
    #create the output folders
    direc = scenario + '/plots'
    createOutSectorFolders(direc, 'electricity')
    createOutSectorFolders(direc, 'heat')
    createOutSectorFolders(direc, 'gas')
    createOutSectorFolders(direc, 'waste')
    createOutSectorFolders(direc, 'water')
    createOutSectorFolders(direc, 'cooling')
    createOutSectorFolders(direc, 'hydrogen')
    createOutSectorFolders(direc, 'transport')
    createOutCostFolders(direc)

#Create Folder
def createOutSectorFolders(resultdirectory, foldername):
    plotDirectory = os.path.join(resultdirectory, foldername)
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'single_timerows')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'in_out_share')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'in_out_timerows')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'anual_load_curve')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, 'sankey')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
#Create Folder
def createOutCostFolders(resultdirectory):
    plotDirectory = os.path.join(resultdirectory, 'costs')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
    plotDirectory = os.path.join(resultdirectory, 'servicecosts')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)


def pie_OutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)
    unit = kwargs.get("unit", "kWh")
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if(sectorname in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if(sectorname in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if(sectorname in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if(sectorname in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( 'sectorname' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][0])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' ' + unit
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = name + ' Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/' + folder + '/in_out_share/'
                filename = consumer.label + '_' + name + 'Out.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' ' + unit
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = name + ' Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/' + folder + '/in_out_share/'
        filename =  'overall_' + name + 'elecOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()      
        
        
def pie_InShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    folder = kwargs.get("folder", "electricity")
    sectorname = kwargs.get("sectorname", "Electricity")
    name = kwargs.get("label", sectorname)
    unit = kwargs.get("unit", "kWh")
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if(sectorname in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if(sectorname in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][1])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex=sectorname).columns) > 1):
                        for col in printElement.columns:
                            if not( sectorname in str(col[0][1])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex=sectorname)).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' ' + unit
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle =  name + ' Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/' + folder + '/in_out_share/'
                filename = consumer.label + '_' + name + 'In.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()  
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' ' + unit
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = name + ' Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/' + folder + '/in_out_share/'
        filename = 'overall' + '_' + name + 'In.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
        
def pie_electricityOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Electricity' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Electricity' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Electricity' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Electricity' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Electricity').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Electricity' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Electricity')).values.sum()
                    
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Electricity').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Electricity' in str(col[0][0])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex='Electricity')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Electricity Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/electricity/in_out_share/'
                filename = consumer.label + '_elecOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Electricity Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/electricity/in_out_share/'
        filename =  'overall_elecOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
            
        
 
def pie_electricityInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Electricity' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Electricity' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Electricity').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Electricity' in str(col[0][1])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex='Electricity')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Electricity').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Electricity' in str(col[0][1])):
                                del printElement[col]
                                
                    
                    elementValue=(printElement.filter(regex='Electricity')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Electricity Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/electricity/in_out_share/'
                filename = consumer.label + '_elecIn.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()  
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Electricity Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/electricity/in_out_share/'
        filename =  'overall_elecIn.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
      
                
def pie_heatOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Heat' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Heat' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Heat' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Heat' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Heat').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Heat' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Heat')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Heat').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Heat' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Heat')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Heat Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/heat/in_out_share/'
                filename = consumer.label + '_heatOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()  
                
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Heat Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/heat/in_out_share/'
        filename =  'overall_heatOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()

def pie_heatInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Heat' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Heat' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Heat').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Heat' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Heat')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Heat').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Heat' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Heat')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Heat Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/heat/in_out_share/'
                filename = consumer.label + '_heatIn.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                plt.clf()
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Heat Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/heat/in_out_share/'
        filename =  'overall_heatIn.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_gasOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Gas' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Gas' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Gas' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Gas' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Gas').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Gas' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Gas')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Gas').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Gas' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Gas')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Gas Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/gas/in_out_share/'
                filename = consumer.label + '_gasOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        
        if(outputAll==0):
            outputAll=1        
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Gas Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/gas/in_out_share/'
        filename =  'overall_gasOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_gasInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Gas' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Gas' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Gas').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Gas' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Gas')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Gas').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Gas' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Gas')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Gas Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/gas/in_out_share/'
                filename = consumer.label + '_gasIn.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Gas Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/gas/in_out_share/'
        filename =  'overall_gasIn.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_wasteOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Wastedisposal' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Wastedisposal' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Wastedisposal' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Wastedisposal' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Wastedisposal').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Wastedisposal' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Wastedisposal')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Wastedisposal').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Wastedisposal' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Wastedisposal')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kg'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Waste Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/waste/in_out_share/'
                filename = consumer.label + '_wasteOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
         
        if(outputAll==0):
            outputAll=1       
         
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kg'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Waste Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/waste/in_out_share/'
        filename =  'overall_wasteOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
                
def pie_wasteInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Wastedisposal' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Wastedisposal' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Wastedisposal').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Wastedisposal' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Wastedisposal')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Wastedisposal').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Wastedisposal' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Wastedisposal')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kg'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Waste Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/waste/in_out_share/'
                filename = consumer.label + '_wasteIn.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        
        if(outputAll==0):
            outputAll=1
                
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kg'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Waste Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/waste/in_out_share/'
        filename =  'overall_wasteIn.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
                
def pie_waterInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Watersector' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Watersector' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Watersector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Watersector' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Watersector')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Watersector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Watersector' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Watersector')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' m³'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Water Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/water/in_out_share/'
                filename = consumer.label + '_waterIn.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
          
        if(outputAll==0):
            outputAll=1      
          
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' m³'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Water Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/water/in_out_share/'
        filename =  'overall_waterIn.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_waterOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Watersector' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Watersector' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Watersector' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Watersector' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Watersector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Watersector' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Watersector')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Watersector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Watersector' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Watersector')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' m³'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Water Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/water/in_out_share/'
                filename = consumer.label + '_waterOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        
        if(outputAll==0):
            outputAll=1        
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' m³'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Water Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/water/in_out_share/'
        filename =  'overall_waterOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()

def pie_sewageOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Sewage' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Sewage' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Sewage' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Sewage' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Sewage').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Sewage' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Sewage')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
            
                if(totalOutput==0):
                    totalOutput=1
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Sewage').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Sewage' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Sewage')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' m³'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Sewage Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/water/in_out_share/'
                filename = consumer.label + '_sewageOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
         
        if(outputAll==0):
            outputAll=1       
         
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' m³'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Sewage Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/water/in_out_share/'
        filename =  'overall_sewageOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                

def pie_sludgeOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Sludgestorage' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Sludgestorage' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Sludgestorage' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Sludgestorage' in str(component.sector_loss)):
                                plotcomponents.append(component)
               

                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Sludgestorage').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Sludgestorage' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Sludgestorage')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
            
                if(totalOutput==0):
                    totalOutput=1
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Sludgestorage').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Sludgestorage' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Sludgestorage')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' m³'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Sludge Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/water/in_out_share/'
                filename = consumer.label + '_sludgeOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
         
        if(outputAll==0):
            outputAll=1
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' m³'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Sludge Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/water/in_out_share/'
        filename =  'overall_sludge.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()


def pie_coolingInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Cooling' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Cooling' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Cooling').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Cooling' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Cooling')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Cooling').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Cooling' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Cooling')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Cooling Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/cooling/in_out_share/'
                filename = consumer.label + '_coolingIn.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight") 
                
        if(outputAll==0):
            outputAll=1
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Cooling Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/cooling/in_out_share/'
        filename =  'overall_coolingIn.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_coolingOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Cooling' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Cooling' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Cooling' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Cooling' in str(component.sector_loss)):
                                plotcomponents.append(component)
               

                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Cooling').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Cooling' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Cooling')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
            
                if(totalOutput==0):
                    totalOutput=1
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Cooling').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Cooling' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Cooling')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' kWh'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Cooling Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/cooling/in_out_share/'
                filename = consumer.label + '_coolingOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                
        if(outputAll==0):
            outputAll=1
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' kWh'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Cooling Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/cooling/in_out_share/'
        filename =  'overall_coolingOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_hydrogenInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Hydrogensector' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Hydrogensector' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Hydrogensector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Hydrogensector' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Hydrogensector')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
                    
                if(totalOutput==0):
                    totalOutput=1
            
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Hydrogensector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Hydrogensector' in str(col[0][1])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Hydrogensector')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' m³'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Hydrogen Input: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/hydrogen/in_out_share/'
                filename = consumer.label + '_hydrogenIn.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                
        if(outputAll==0):
            outputAll=1
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' m³'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Hydrogen Input Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/hydrogen/in_out_share/'
        filename =  'overall_hydrogenIn.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_hydrogenOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Hydrogensector' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Hydrogensector' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Hydrogensector' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Hydrogensector' in str(component.sector_loss)):
                                plotcomponents.append(component)
               

                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Hydrogensector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Hydrogensector' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Hydrogensector')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
            
                if(totalOutput==0):
                    totalOutput=1
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Hydrogensector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Hydrogensector' in str(col[0][0])):
                                del printElement[col]
                    
                    elementValue=(printElement.filter(regex='Hydrogensector')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' m³'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Hydrogen Output: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/hydrogen/in_out_share/'
                filename = consumer.label + '_hydrogenOut.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                
        if(outputAll==0):
            outputAll=1
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' m³'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Hydrogen Output Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/hydrogen/in_out_share/'
        filename =  'overall_hydrogenOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
                
def pie_transportOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    outputAll = 0
    totalDict={}
    totalColorDict={}
    totalValues=[]
    totalLabels=[]
    totalColors=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
    
            totalOutput=0
            mylabels=[]
            values=[]
            colors=[]
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_drive'):
                                if('Mobility' in str(element.sector_drive)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_drive'):
                            if('Mobility' in str(component.sector_drive)):
                                plotcomponents.append(component)
               

                for element in plotcomponents:
                    printElement = solph.views.node(results, element.labelDrive).get("sequences")
                    elementValue=(printElement.filter(regex='Mobility')).values.sum()
                    totalOutput+=elementValue
                    outputAll+=elementValue
            
                if(totalOutput==0):
                    totalOutput=1
        
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.labelDrive).get("sequences")
                    elementValue=(printElement.filter(regex='Mobility')).values.sum()
                    values.append(elementValue/totalOutput)
                    label = element.label.split('_')[0] + ', ' + str(round(elementValue, 2)) + ' km'
                    mylabels.append(label)
                    colors.append(element.color)
                    
                    if(element.label.split('_')[0] in totalDict):
                        totalDict[element.label.split('_')[0]] = totalDict[element.label.split('_')[0]] + elementValue
                    else:
                        totalDict[element.label.split('_')[0]]=elementValue
                        totalColorDict[element.label.split('_')[0]] = element.color
            
                values = [-number if number < 0 else number for number in values]

                valTemp = []
                labelTemp =[]
                colorTemp = []
    
                for i in range (0, len(values)):
                    if(round(values[i], 2)!=0):
                        valTemp.append(values[i])
                        labelTemp.append(mylabels[i])
                        colorTemp.append(colors[i])
    
                values = valTemp
                mylabels=labelTemp
                colors=colorTemp
                
                patches, texts = plt.pie(values, colors=colors)
                plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
                figtitle = 'Transport Share: ' + consumer.label
        
                plt.suptitle(figtitle)
    
    
                filesave = scenario + '/plots/transport/in_out_share/'
                filename = consumer.label + '_transportShare.png'

                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                
        if(outputAll==0):
            outputAll=1
        
        for key in totalDict:    
            label = key + ', ' + str(round(totalDict[key], 2)) + ' km'
            totalDict[key] /=  outputAll
            totalLabels.append(label)
            totalValues.append(totalDict[key])
            totalColors.append(totalColorDict[key])
            
        totalValues = [-number if number < 0 else number for number in totalValues]

        valTemp = []
        labelTemp =[]
        colorTemp = []
    
        for i in range (0, len(totalValues)):
            if(round(totalValues[i], 2)!=0):
                valTemp.append(totalValues[i])
                labelTemp.append(totalLabels[i])
                colorTemp.append(totalColors[i])
    
        values = valTemp
        mylabels=labelTemp
        colors=colorTemp
                
        patches, texts = plt.pie(values, colors=colors)
        plt.legend(patches, mylabels, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Transport Overall'
        
        plt.suptitle(figtitle)
    
    
        filesave = scenario + '/plots/transport/in_out_share/'
        filename =  'overall_transportOut.png'

        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        plt.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        plt.clf()
        
        
def bar_electricityOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Electricity' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Electricity' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Electricity' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Electricity' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Electricity').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Electricity' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Electricity').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Electricity').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Electricity Output: ' + consumer.label
                filesave = scenario + '/plots/electricity/in_out_timerows/'
                filename = consumer.label + '_elecOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Electricity Output Overall'
        filesave = scenario + '/plots/electricity/in_out_timerows/'
        filename = 'overall_elecOut.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()  
                
        
def bar_electricityInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Electricity' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Electricity' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Electricity').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Electricity' in str(col[0][1])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Electricity').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Electricity').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Electricity Input: ' + consumer.label
                filesave = scenario + '/plots/electricity/in_out_timerows/'
                filename = consumer.label + '_elecIn.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Electricity Input Overall'
        filesave = scenario + '/plots/electricity/in_out_timerows/'
        filename = 'overall_elecIn.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf() 
        
        
def bar_heatOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Heat' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Heat' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Heat' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Heat' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Heat').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Heat' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Heat').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Heat').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Heat Output: ' + consumer.label
                filesave = scenario + '/plots/heat/in_out_timerows/'
                filename = consumer.label + '_heatOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Heat Output Overall'
        filesave = scenario + '/plots/heat/in_out_timerows/'
        filename = 'overall_heatOut.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()  
                
                
    
def bar_heatInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Heat' in str(element.sector_out)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_out'):
                            if('Heat' in str(component.sector_out)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Heat').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Heat' in str(col[0][1])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Heat').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Heat').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Heat Output: ' + consumer.label
                filesave = scenario + '/plots/heat/in_out_timerows/'
                filename = consumer.label + '_heatIn.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Heat Input Overall'
        filesave = scenario + '/plots/heat/in_out_timerows/'
        filename = 'overall_heatIn.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()  
                
    
def bar_gasOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Gas' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Gas' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Gas' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Gas' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Gas').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Gas' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Gas').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Gas').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Gas Output: ' + consumer.label
                filesave = scenario + '/plots/gas/in_out_timerows/'
                filename = consumer.label + '_gasOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Gas Output Overall'
        filesave = scenario + '/plots/gas/in_out_timerows/'
        filename = 'overall_gasOut.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()  
 
    
def bar_gasInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Gas' in str(element.sector_out)):
                                    plotcomponents.append(element)
                            
                    else:
                        if hasattr(component, 'sector_out'):
                            if('Gas' in str(component.sector_out)):
                                plotcomponents.append(component)
                        
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Gas').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Gas' in str(col[0][1])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Gas').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Gas').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Gas Output: ' + consumer.label
                filesave = scenario + '/plots/gas/in_out_timerows/'
                filename = consumer.label + '_gasIn.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Gas Input Overall'
        filesave = scenario + '/plots/gas/in_out_timerows/'
        filename = 'overall_gasIn.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()
        

def bar_wasteOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Wastedisposal' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Wastedisposal' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Wastedisposal' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Wastedisposal' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Wastedisposal').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Wastedisposal' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Wastedisposal').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Wastedisposal').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Wastedisposal: ' + consumer.label
                filesave = scenario + '/plots/waste/in_out_timerows/'
                filename = consumer.label + '_wasteDisposal.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Waste in kg')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Wastedisposal Overall'
        filesave = scenario + '/plots/waste/in_out_timerows/'
        filename = 'overall_wasteDisposal.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Waste in kg')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf() 
        
        
def bar_wasteInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Wastedisposal' in str(element.sector_out)):
                                    plotcomponents.append(element)

                    else:
                        if hasattr(component, 'sector_out'):
                            if('Wastedisposal' in str(component.sector_out)):
                                plotcomponents.append(component)
                        
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Wastedisposal').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Wastedisposal' in str(col[0][1])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Wastedisposal').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Wastedisposal').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Waste: ' + consumer.label
                filesave = scenario + '/plots/waste/in_out_timerows/'
                filename = consumer.label + '_wasteIn.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Waste in kg')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Waste Overall'
        filesave = scenario + '/plots/waste/in_out_timerows/'
        filename = 'overall_wasteIn.png'
         
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Waste in kg')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()  
 

def bar_waterOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Watersector' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Watersector' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Watersector' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Watersector' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Watersector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Watersector' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Watersector').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Watersector').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Water Out: ' + consumer.label
                filesave = scenario + '/plots/water/in_out_timerows/'
                filename = consumer.label + '_waterOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Water in m³')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Water Output Overall'
        filesave = scenario + '/plots/water/in_out_timerows/'
        filename = 'overall_waterOut.png'
         
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Water in m³')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf() 
        
        
def bar_waterInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Watersector' in str(element.sector_out)):
                                    plotcomponents.append(element)
                            
                    else:
                        if hasattr(component, 'sector_out'):
                            if('Watersector' in str(component.sector_out)):
                                plotcomponents.append(component)
                       
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Watersector').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Watersector' in str(col[0][1])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Watersector').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Watersector').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Water In: ' + consumer.label
                filesave = scenario + '/plots/water/in_out_timerows/'
                filename = consumer.label + '_waterIn.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Water in m³')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Water Input Overall'
        filesave = scenario + '/plots/water/in_out_timerows/'
        filename = 'overall_waterIn.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Water in m³')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf() 
        
        
def bar_sewageOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Sewage' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Sewage' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Sewage' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Sewage' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Sewage').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Sewage' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Sewage').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Sewage').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Sewage Out: ' + consumer.label
                filesave = scenario + '/plots/water/in_out_timerows/'
                filename = consumer.label + '_sewageOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Sewage in m³')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Sewage Output Overall'
        filesave = scenario + '/plots/water/in_out_timerows/'
        filename = 'overall_sewageOut.png'
         
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Sewage in m³')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf() 
        

def bar_sludgeOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Sludgestorage' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Sludgestorage' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Sludgestorage' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Sludgestorage' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Sludgestorage').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Sludgestorage' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Sludgestorage').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Sludgestorage').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Sludge Out: ' + consumer.label
                filesave = scenario + '/plots/water/in_out_timerows/'
                filename = consumer.label + '_sludgeOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Sludge in m³')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Sludge Output Overall'
        filesave = scenario + '/plots/water/in_out_timerows/'
        filename = 'overall_sludgeOut.png'
            
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Sludge in m³')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf() 
        
        
def bar_coolingOutShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_in'):
                                if('Cooling' in str(element.sector_in)):
                                    plotcomponents.append(element)
                            if hasattr(element, 'sector_loss'):
                                if('Cooling' in str(element.sector_loss)):
                                    plotcomponents.append(element)
                    else:
                        if hasattr(component, 'sector_in'):
                            if('Cooling' in str(component.sector_in)):
                                plotcomponents.append(component)
                        if hasattr(component, 'sector_loss'):
                            if('Cooling' in str(component.sector_loss)):
                                plotcomponents.append(component)
               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Cooling').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Cooling' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Cooling').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Cooling').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Cooling Output: ' + consumer.label
                filesave = scenario + '/plots/cooling/in_out_timerows/'
                filename = consumer.label + '_coolOut.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Cooling Output Overall'
        filesave = scenario + '/plots/cooling/in_out_timerows/'
        filename = 'overall_coolingOut.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()
        

def bar_coolingInShare(*args, **kwargs):

    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    scenario = kwargs.get("scenario", 'base')
    
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    dfTotal = pd.DataFrame(index=months)
    colorsTotal=[]
    
    if(results):
        for consumer in consumers:
            plotcomponents=[]
            colors=[]
            
            df = pd.DataFrame(index=months)
            
            if(consumer):
                for component in consumer.components:
                    if isinstance(component, list):
                        for element in component:
                            if hasattr(element, 'sector_out'):
                                if('Cooling' in str(element.sector_out)):
                                    plotcomponents.append(element)
                            
                    else:
                        if hasattr(component, 'sector_out'):
                            if('Cooling' in str(component.sector_out)):
                                plotcomponents.append(component)

               
                for element in plotcomponents:
                    printElement = solph.views.node(results, element.label).get("sequences")
                    
                    if(len(printElement.filter(regex='Cooling').columns) > 1):
                        for col in printElement.columns:
                            if not( 'Cooling' in str(col[0][0])):
                                del printElement[col]
                    
                    if(round(printElement.filter(regex='Cooling').groupby([lambda x : x.month]).sum().values.sum(), 2)>0):
                        dataframe = printElement.filter(regex='Cooling').groupby([lambda x : x.month]).sum().values
                        df[str(element.label.split('_')[0])] = dataframe[0][0]
                        colors.append(element.color)
                        
                        
                figtitle = 'Cooling Input: ' + consumer.label
                filesave = scenario + '/plots/cooling/in_out_timerows/'
                filename = consumer.label + '_coolIn.png'
                        
                if df.empty:
                    continue
                plot = df.plot.bar(stacked=True, title=figtitle, color=colors)
                plot.legend(bbox_to_anchor=(1.1, 1.1))
                plot.set_ylabel('Energy in kWh')
    
                fig = plot.get_figure()
    
                if os.path.isfile(os.path.join(filesave, filename)):
                    os.remove(os.path.join(filesave, filename))
                fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
                fig.clf()  
                    
                dfTotal = dfTotal.add(df, fill_value=0)
                
                if(len(colors)>len(colorsTotal)):
                    colorsTotal = colors
                
            
        figtitle = 'Cooling Input Overall'
        filesave = scenario + '/plots/cooling/in_out_timerows/'
        filename = 'overall_coolingIn.png'
        
        if dfTotal.empty:
            return 0
        plot = dfTotal.plot.bar(stacked=True, title=figtitle, color=colorsTotal)
        plot.legend(bbox_to_anchor=(1.1, 1.1))
        plot.set_ylabel('Energy in kWh')
    
        fig = plot.get_figure()
    
        if os.path.isfile(os.path.join(filesave, filename)):
            os.remove(os.path.join(filesave, filename))
        fig.savefig(os.path.join(filesave, filename), dpi=DPI, bbox_inches="tight")
        fig.clf()      
        
 