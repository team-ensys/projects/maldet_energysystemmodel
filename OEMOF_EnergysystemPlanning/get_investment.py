import json
import pandas as pd
import numpy as np

#Input parameters to change
resultdirectory = "results/RESULTS_LSM_PAPER/"
scenarioname = "LSM19_greywater"

df = pd.DataFrame()

tot_pv_lsc=[]
tot_heatpump_lsc=[]
tot_battery_lsc=[]
tot_dh_lsc=[]
tot_greywater_lsc=[]

tot_pv_lsm=[]
tot_wastestore_lsm=[]
tot_sewagetreat_lsm=[]
tot_wastecomb_lsm=[]
tot_wasteAD_lsm=[]
tot_chp_lsm=[]

scenarios=["LSM1_noPV", "LSM2_PV", "LSM3_trade", "LSM4_lessPVnoTrad", "LSM5_lessPVTrad",
           "LSM6_lessPVLSM", "LSM7_waste20", "LSM8_waste50", "LSM9_waste90", "LSM10_wastered",
           "LSM11_wasteDisp", "LSM12_wasteAD", "LSM13_ADminBio", "LSM14_GasCHP", "LSM15_CHPhigh", 
           "LSM16_wasteLow", "LSM17_wasteHigh", "LSM18_combMinBio", "LSM19_greywater", "LSM20_greywaterMin",
           "LSM21_comp", "LSM22_aut", "LSM23_environ", "LSM24_eff", "LSM25_2040", "LSM26_2040lowem"]

for scenarioname in scenarios:

    file2read=resultdirectory + scenarioname + "/investment.txt"
       
    # Opening JSON file
    f = open(file2read,)
       
    
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
       
    consumer_list=["LSC1", "LSC2", "LSC3", "LSC4", "LSC5"]
    
    pv_lsc=[]
    heatpump_lsc=[]
    battery_lsc=[]
    dh_lsc=[]
    greywater_lsc=[]
    
    for consumer in consumer_list:
        data2search = data[consumer]
        
        if "P_pv" in data2search.keys():
            pv_lsc.append(data2search["P_pv"])
        else:
            pv_lsc.append(0)
            
        if "P_battery" in data2search.keys():
            battery_lsc.append(data2search["P_battery"])
        else:
            battery_lsc.append(0)
            
        if "P_heatpump" in data2search.keys():
            heatpump_lsc.append(data2search["P_heatpump"])
        else:
            heatpump_lsc.append(0)
            
        if "P_districtheat" in data2search.keys():
            dh_lsc.append(data2search["P_districtheat"])
        else:
            dh_lsc.append(0)
            
        if "v_max_greywater" in data2search.keys():
            greywater_lsc.append(data2search["v_max_greywater"])
        else:
            greywater_lsc.append(0)
            
    consumer_list=["LSM1", "LSM2", "LSM3", "LSM4", "LSM5"]
    
    pv_lsm=[]
    wastestore_lsm=[]
    sewagetreat_lsm=[]
    wastecomb_lsm=[]
    wasteAD_lsm=[]
    chp_lsm=[]
    
    for consumer in consumer_list:
        data2search = data[consumer]
        
        if "P_pv" in data2search.keys():
            pv_lsm.append(data2search["P_pv"])
        else:
            pv_lsm.append(0)
    
        if "P_wastecomb" in data2search.keys():
            wastecomb_lsm.append(data2search["P_wastecomb"])
        else:
            wastecomb_lsm.append(0)
            
        if "SOC_wastestore" in data2search.keys():
            wastestore_lsm.append(data2search["SOC_wastestore"])
        else:
            wastestore_lsm.append(0)
            
        if "V_sewagetreat" in data2search.keys():
            sewagetreat_lsm.append(data2search["V_sewagetreat"])
        else:
            sewagetreat_lsm.append(0)
            
        if "P_wasteAD" in data2search.keys():
            wasteAD_lsm.append(data2search["P_wasteAD"])
        else:
            wasteAD_lsm.append(0)
            
        if "P_CHP" in data2search.keys():
            chp_lsm.append(data2search["P_CHP"])
        else:
            chp_lsm.append(0)
    
    tot_pv_lsc.append(np.sum(pv_lsc))
    tot_heatpump_lsc.append(np.sum(heatpump_lsc))
    tot_battery_lsc.append(np.sum(battery_lsc))
    tot_dh_lsc.append(np.sum(dh_lsc))
    tot_greywater_lsc.append(np.sum(greywater_lsc))
    
    
    tot_pv_lsm.append(np.sum(pv_lsm))
    tot_wastestore_lsm.append(np.sum(wastestore_lsm))
    tot_sewagetreat_lsm.append(np.sum(sewagetreat_lsm))
    tot_wastecomb_lsm.append(np.sum(wastecomb_lsm))
    tot_wasteAD_lsm.append(np.sum(wasteAD_lsm))
    tot_chp_lsm.append(np.sum(chp_lsm))

    
       
       
    # Closing file
    f.close()

df["Scenario"] = scenarios

df["PV_LSC in kWp"]=tot_pv_lsc
df["Heatpump_LSC in kW"]=tot_heatpump_lsc
df["Battery_LSC in kWh"]=tot_battery_lsc
df["DistrictHeat_LSC in kW"]=tot_dh_lsc
df["Greywater_LSC in m³"]=tot_greywater_lsc

df["PV_LSM in kWp"]=tot_pv_lsm
df["Wastestore_LSM in kg"]=tot_wastestore_lsm
df["Sewagetreat_LSM in m³"]=tot_sewagetreat_lsm
df["Wastecomb_LSM in kW"]=tot_wastecomb_lsm
df["WasteAD_LSM in kW"]=tot_wasteAD_lsm
df["CHP_LSM in kW"]=tot_chp_lsm

file2write=resultdirectory +  "/total_investments.xlsx"
df.to_excel(file2write) 


df = pd.DataFrame()

data_total=[]
objective_total=[]

scenarios=["LSM1_noPV", "LSM2_PV", "LSM3_trade", "LSM4_lessPVnoTrad", "LSM5_lessPVTrad",
           "LSM6_lessPVLSM", "LSM7_waste20", "LSM8_waste50", "LSM9_waste90", "LSM10_wastered",
           "LSM11_wasteDisp", "LSM12_wasteAD", "LSM13_ADminBio", "LSM14_GasCHP", "LSM15_CHPhigh", 
           "LSM16_wasteLow", "LSM17_wasteHigh", "LSM18_combMinBio", "LSM19_greywater", "LSM20_greywaterMin",
           "LSM21_comp", "LSM22_aut", "LSM23_environ", "LSM24_eff", "LSM25_2040", "LSM26_2040lowem"]


for scenarioname in scenarios:

    #read investment costs
    file2read=resultdirectory + scenarioname + "/costs_invest.txt"
       
    # Opening JSON file
    f = open(file2read,)
       
    
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
    
    data_total.append(data["Total"])
    
    #Read objective
    file2read=resultdirectory + scenarioname + "/objective.txt"
       
    f = open(file2read,)
    lines = f.readlines()
    
    index=0
    result=0
    for line in lines:
        if("objective" in str(line).lower()):
            result=lines[index+1]
        index+=1
        
    objective_total.append(float(result))
    
    
df["Scenario"] = scenarios
df["Investment costs in €"] = data_total
df["Objective in €"] = objective_total

file2write=resultdirectory +  "/total_costs.xlsx"
df.to_excel(file2write) 
    
