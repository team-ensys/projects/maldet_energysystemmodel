# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 17:44:55 2022

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np
import transformerLossesMultipleOut as tlmo
import source_emissions as em
import pyomo.environ as po
import pyomo.core as pyomo


class WashingMaschine():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class


            position='Consumer_1'
            timesteps = 8760
            energySewage = 1.2
            Tref = 20
            Twash = 30
            Q_fix = 0.15 
            water=0.05
            maxVariableEnergy = 1.64
            washingEfficiency=0.656
            
            #Electrical Energy per kWh
            var_el=0.025
            
            #Electrical Energy per kWh
            var_th=0.0164
            
            

            
            
            
            self.Qfix = kwargs.get("Qfix", Q_fix)
            self.water = kwargs.get("water", water)
            self.sewage_energy = kwargs.get("sewage_energy", energySewage)
            self.eta_elec = kwargs.get("eta_elec", washingEfficiency)
            self.sewage_share = kwargs.get("sewage_share", 0.95)
            self.maxVariableEnergy = kwargs.get("maxVariableEnergy", maxVariableEnergy)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.var_el = kwargs.get("var_el", var_el)
            self.var_th = kwargs.get("var_th", var_th)
            
           
            self.Tref = kwargs.get("Tref", Tref)
            self.Twash = kwargs.get("Twash", np.full(self.timesteps, Twash))

            
            self.min_elec = kwargs.get("min_elec", 0)
            self.max_elec = kwargs.get("max_elec", 1)
            self.min_therm = kwargs.get("min_therm", 0)
            self.max_therm = kwargs.get("max_therm", 1)


            #self.costs_in = kwargs.get("c_in", 0)
            #self.costs_out = kwargs.get("c_out", 0)
            #self.costs_loss = kwargs.get("c_loss", 0)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_inAdditional = kwargs.get("sector_inAdditional")
            self.sector_loss = kwargs.get("sector_loss", 0)
            
            
            self.position = kwargs.get("position", position)
            label = 'WashingMaschine_' + self.position
            self.label = kwargs.get("label", label)
            self.label_elecfix = 'ElecFix_' + self.label
            self.label_elecvar = 'ElecHeating_' + self.label
            self.label_thermvar = 'ThermHeating_' + self.label
            self.label_recovery = 'ThermRecovery_' + self.label
            self.label_water = 'Water_' + self.label
            self.labelSankey = kwargs.get('label_sankey', 'Washing\nMaschine_' + self.position)
            
            color='teal'
            self.color = kwargs.get("color", color)
            
            self.cycles = []
            for i in self.Twash:
                if(i-self.Tref>0):
                    self.cycles.append(1)
                else:
                    self.cycles.append(0)
            
            
        def get_elec(self):
            return self.Elec(self)
        
        def get_heatrecovery(self):
            return self.Heatrecovery(self)
        
        def get_waterinput(self):
            return self.Water(self)
        
        def get_elecHeat(self):
            return self.ElecHeat(self)
        
        def get_thermHeat(self):
            return self.ThermHeat(self)
 
    
        #Electricity Component           
            
        class Elec():
            def __init__(self, outer):
                self.outer = outer
                self.position = self.outer.position
                self.label = 'ElecFix' + self.outer.label
                self.color='navy'
                
                self.sector_in = self.outer.sector_in

                
                self.labelSankey = self.outer.labelSankey
                
            

            def component(self):
                q_elWashingFix = solph.Flow(fix=np.multiply(self.outer.Qfix, self.outer.cycles), nominal_value=1)
                washingmaschineElecFix = solph.Sink(
                        label=self.label,
                        inputs={self.sector_in : q_elWashingFix})
            
                return washingmaschineElecFix
            
            
        class ElecHeat():
            def __init__(self, outer):
                self.outer = outer
                self.position = self.outer.position
                self.label = 'ElecHeat' + self.outer.label
                self.color='hotpink'
                
                self.sector_in = self.outer.sector_in

                
                self.labelSankey = self.outer.labelSankey
                
            
            

            def component(self):
                
                nominal = np.multiply(self.outer.var_el, np.subtract(self.outer.Twash,self.outer.Tref))
                
                q_elWashingVar = solph.Flow(min=np.multiply(nominal, self.outer.min_elec), max=np.multiply(nominal, self.outer.max_elec), nominal_value=1)
                washingmaschineElecVar = solph.Sink(
                        label=self.label,
                        inputs={self.sector_in : q_elWashingVar})
            
                return washingmaschineElecVar
 
    
        class ThermHeat():
            def __init__(self, outer):
                self.outer = outer
                self.position = self.outer.position
                self.label = 'ThermHeat' + self.outer.label
                self.color='hotpink'
                

                self.sector_inAdditional = self.outer.sector_inAdditional

                
                self.labelSankey = self.outer.labelSankey
                
            

            def component(self):
                nominal = np.multiply(self.outer.var_th, np.subtract(self.outer.Twash,self.outer.Tref))
                q_thermWashingVar = solph.Flow(min=np.multiply(nominal, self.outer.min_therm), max=np.multiply(nominal, self.outer.max_therm), nominal_value=1)
                washingmaschineThermVar = solph.Sink(
                        label=self.label,
                        inputs={self.sector_inAdditional : q_thermWashingVar})
            
                return washingmaschineThermVar    
            
            
 
        #Water Component  
            
        class Water():
            def __init__(self, outer):
                self.outer = outer
                self.position = self.outer.position
                self.label = 'Water' + self.outer.label
                self.color='darkorange'
                
                self.sector_loss = self.outer.sector_loss
                
                self.labelSankey = self.outer.labelSankey
                
            

            def component(self):
                q_WashingWater = solph.Flow(fix=np.multiply(self.outer.cycles, self.outer.water), nominal_value=1)
                washingmaschineWater = solph.Sink(
                        label=self.label,
                        inputs={self.sector_loss : q_WashingWater})
            
                return washingmaschineWater
            

        #Heatrecovery Component  
            
        class Heatrecovery():
            def __init__(self, outer):
                self.outer = outer
                self.position = self.outer.position
                self.label = 'HeatRecovery' + self.outer.label
                self.color='yellowgreen'
                

                self.sector_out = self.outer.sector_out

                
                self.labelSankey = self.outer.labelSankey
                
                
            

            def component(self):
                q_thWashing = solph.Flow(fix=np.multiply(np.multiply(self.outer.sewage_energy*self.outer.water/5*self.outer.sewage_share, (np.subtract(self.outer.Twash,self.outer.Tref))), self.outer.cycles), nominal_value=1)
                washingmaschineHeatRecover = solph.Source(
                        label=self.label,
                        outputs={self.sector_out : q_thWashing})
            
                return washingmaschineHeatRecover
            
            
        def heating_sum(*args, **kwargs):
    
            #Vehicle Objects
            elecheat = kwargs.get("elec_heat", 0)
            eta = kwargs.get("eta", 0)
            thermheat = kwargs.get("therm_heat", 0)
            consumer = kwargs.get("consumer", 0)
            water = kwargs.get("water", 0)
            waterHeated = kwargs.get("waterHeated", 1/5)
            Twash = kwargs.get("Twash", 30)
            Tref = kwargs.get("Tref", 20)
            energy = kwargs.get("energy", 0.00164)
    
            #Model
            modelIn = kwargs.get("model", 0)
            

    
            if(modelIn==0):
                print('Model not found!')
                return 0
        
            else:
                #Add the additional constraints to the model
                #Block for Pyomo Environment
                myBlock = po.Block()
    
            #Set with timesteps of OEMOF Model
            myBlock.t = pyomo.Set(
                initialize=modelIn.TIMESTEPS,
                ordered=True,
                doc='Set of timesteps')
    
            if elecheat and thermheat:
                #Set with Vehicles
            
      
    
                modelIn.add_component('MyBlockWashingmachine_' + consumer.label, myBlock)
    
    
        
        
                def washingrule(model, t):
                    return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==elecheat.label)*eta) 
                            +(sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==thermheat.label))
                                                            == (Twash[t]-Tref)*water*waterHeated*energy*1000)
        
       
    

        
        
        
            if elecheat and thermheat:
                
                myBlock.washing = pyomo.Constraint(
                    myBlock.t,
                    rule=washingrule,
                    doc='washing')
                
           
            
        
            return modelIn  


