# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 10:51:17 2022

@author: Matthias Maldet
"""


import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc

import investment_components as investment_comp



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'LSM_base'

#Filename
filename = "input_data_base.xlsx"


#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 200
timestepsTotal = 8784

data=pd.ExcelFile(filename)


#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1

yearWeightFactor=timesteps/timestepsTotal


#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2


#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
#Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    

# -------------- Additional parameters that are set for the simulation -------------- 

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzentgelte
c_grid = 0.062

#Abgabe
c_abgabe = 0.018

#Reduktion Netzentgelt Lokalbereich
red_local = 1-0.57

#Reduktion Netzentgelt Regionalbereich
red_regional = 1-0.28

#_____________ Heat _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_heat = 0.046


#---------------------- DEFINITION OF CONSUMERS ----------------------
consumer1 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_1')
consumer1.addElectricitySector()
consumer1.addHeatSector()
consumer1.addWaterSector()
consumer1.addWasteSector()
consumer1.sector2system(my_energysystem)

consumer2 = Consumer.Consumer(timesteps=timesteps, data=data, label='Consumer_2')
consumer2.addElectricitySector()
consumer2.addHeatSector()
consumer2.sector2system(my_energysystem)



print("----------------Consumer Data loaded----------------")

#---------------------- LSC 1 Components ----------------------

#___________________________________ Electricity _____________________________________________________________

#Electricity demand
electricitydemand = el_comp.Demand(sector=consumer1.getElectricitySector(), 
                            demand_timeseries=consumer1.input_data['electricityDemand'], label='Electricitydemand_' + consumer1.label) 
my_energysystem.add(electricitydemand.component())
consumer1.addComponent(electricitydemand)


#Electricity grid
electricitygridpurchase = el_comp.GridPurchase(sector=consumer1.getElectricitySector(), 
                              costs_energy=np.add(consumer1.input_data['electricityCosts'], c_grid + c_abgabe), 
                              label='Electricitygridpurchase_' + consumer1.label, timesteps=timesteps, costs_power=0)       
my_energysystem.add(electricitygridpurchase.component())
consumer1.addComponent(electricitygridpurchase)

#PV Investment
pv_invest = investment_comp.Photovoltaic_Investment(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_' + consumer1.label, timesteps=timesteps, area_efficiency=0.5, area_roof=100, area_factor=10)
my_energysystem.add(pv_invest.component())
consumer1.addComponent(pv_invest)

#Electricity Feedin
electricitygridFeedin = el_comp.GridFeedin(sector=consumer1.getElectricitySector(), revenues=consumer1.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + consumer1.label, timesteps=timesteps)
my_energysystem.add(electricitygridFeedin.component())
consumer1.addComponent(electricitygridFeedin)

#Battery
battery_invest = investment_comp.Battery_Investment(sector_in=consumer1.getElectricitySector(), label='Battery_' + consumer1.label,
                          soc_max=5, SOC_start=0, P_in=5, P_out=5)
my_energysystem.add(battery_invest.component())
consumer1.addComponent(battery_invest)


#___________________________________ Heat _____________________________________________________________

#Heat demand
heatdemand = heat_comp.Demand(sector=consumer1.getHeatSector(), demand_timeseries=np.add(consumer1.input_data['heatDemand'], consumer1.input_data['hotwaterDemand']), label='Heatdemand_' + consumer1.label)
my_energysystem.add(heatdemand.component())
consumer1.addComponent(heatdemand)

#Heatpump
heatpump_invest = investment_comp.Heatpump_Investment(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector(), 
                            conversion_timeseries=consumer1.input_data['copHP'], timesteps=timesteps, P_in=7, P_out=7, label='Heatpump_' + consumer1.label)
my_energysystem.add(heatpump_invest.component())
consumer1.addComponent(heatpump_invest)

#Thermal Storage
thermalStorage_invest = investment_comp.ThermalStorage_Investment(sector_in=consumer1.getHeatSector(), P_in=7, volume_max=0.3, label='Heatstorage_' + consumer1.label)
my_energysystem.add(thermalStorage_invest.component())
consumer1.addComponent(thermalStorage_invest)

#Sale to LSM and decision for district heat access
heat2lsm = investment_comp.Heat2LSM_Investment(sector_in=consumer1.getHeatSector(), sector_out=consumer2.getHeatSector(),
                                    P=10, label="Heat2LSC_" + consumer1.label, rev_consumer=0.001, c_lsc=0.001)
my_energysystem.add(heat2lsm.component())
consumer1.addComponent(heat2lsm)


#___________________________________ Waste _____________________________________________________________

#Waste combustion
wasteCombustion_invest = investment_comp.Wastecombustion_Investment(sector_in=consumer1.getWastedisposalSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()],
                                             c_in=0, c_out=[0.04, 0.04], conversion_factor=[0.35, 0.4], P_in=100, P_out=100, 
                                             usable_energy=1, label='Wastecombustion_' + consumer1.label, color='lightsteelblue')
my_energysystem.add(wasteCombustion_invest.component())
consumer1.addComponent(wasteCombustion_invest)


#Waste storage
wasteStorage_invest = investment_comp.WasteStorage_Investment(sector_in=consumer1.getWastestorageSector(), sector_out=consumer1.getWastedisposalSector(), 
                                       volume_max=12, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="WastestorageDisposal_LSC", color='mistyrose', balanced=True)
my_energysystem.add(wasteStorage_invest.component())
consumer1.addComponent(wasteStorage_invest)    


#___________________________________ Sewage _____________________________________________________________
#Sludge combustion
sludgecombustion_invest = investment_comp.Sludgecombustion_Investment(sector_in=consumer1.getSludgetreatmentSectorOut(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()],
                                               conversion_factor=[0.35, 0.4], c_in=0, c_out=[0.06, 0.06], P_in=100, P_out=100, usable_energy=1,
                                               label='Sludgecombustion_LSC', color='lightsteelblue') 
my_energysystem.add(sludgecombustion_invest.component())
consumer1.addComponent(sludgecombustion_invest)


#Sludge Storage
sludgestorage_invest= investment_comp.SludgeStorage_Investment(sector_in=consumer1.getSludgetreatmentSectorIn(), sector_out=consumer1.getSludgetreatmentSectorOut(), timesteps=timesteps,
                                         c_in=0.01, c_out=0.01, volume_start=0, volume_max=1.5, Q_max=1.5, disposal_periods=0, balanced=True,
                                         label="SludgestorageTreatment_LSC", color='darkgoldenrod')
my_energysystem.add(sludgestorage_invest.component())
consumer1.addComponent(sludgestorage_invest)

#Sewage treatment plant
sewageTreatmentPlant_invest = investment_comp.SewageTreatment_Investment(sector_in=consumer1.getSewageSector(), sector_loss=consumer1.getElectricitySector(),
                                                  sector_out=[consumer1.getWaterrecoverysector(), consumer1.getSludgeSector(), consumer1.getHeatSector()],
                                                  tempdif=0, label='SewageTreatment_LSC', color='black', c_in=0.04, eta_water=1)
my_energysystem.add(sewageTreatmentPlant_invest.component())
consumer1.addComponent(sewageTreatmentPlant_invest)


#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 

print("----------------Energy System configured----------------")



#---------------------- Additional constraints ----------------------


#----------------------- Solve the model ----------------------

if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    saka.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, saka, scenario)


results = meth.restore_results(scenario)

print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")

    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results(my_energysystem, saka, scenario)
#results = solph.processing.results(saka)
results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")


#--------------------------- Store the results --------------------------------


"""
#consumerList=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, consumer10, consumer11, consumer12, consumer13, LSC]

#Store the costs
resultprocessing_lsc.costs_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                             consumer10, consumer11, consumer12, LSC], 
                                  results=results, filename='costs.csv', scenarioname=scenario, model=saka, timeseries=False)

#Store the results
resultprocessing_lsc.consumer_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                                consumer10, consumer11, consumer12, LSC], 
                                     results=results, filename='technology.csv', scenarioname=scenario)

"""

print("----------------Results saved----------------")


#--------------------------- Code evaluation --------------------------------

#Timeseries
#test=solph.views.node(results, pv_invest.label).get("sequences")

#Investment
#solph.views.node(results, pv_invest.label).get("scalars")[0]

#Investment Satus
#solph.views.node(results, pv_invest.label).get("scalars")[1]

#Bei speicher anders!!!

