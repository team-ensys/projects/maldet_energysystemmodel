# -*- coding: utf-8 -*-
"""
Created on Tue May 24 16:38:53 2022

@author: Matthias Maldet
"""

import numpy as np

#Generator Willingness for Flexibility
"""

saving = np.linspace(0, 0.5, 1000)
probability = 39.62*np.exp(-6.208*saving)

sumProbab = np.sum(probability)

probability = np.multiply(probability, 1/sumProbab)

number = np.random.choice(saving, p=probability)

print('Generated Number: \n')
print(number)
"""

#Generator Willingness for Recycling
saving = np.linspace(0, 0.3, 1000)
probability = -333.33*np.multiply(saving, saving) + 263.33*saving + 1.6667


sumProbab = np.sum(probability)

probability = np.multiply(probability, 1/sumProbab)

number = np.random.choice(saving, p=probability)

print('Generated Number: \n')
print(number)
