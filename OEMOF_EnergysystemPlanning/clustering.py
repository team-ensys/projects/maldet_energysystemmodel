# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 08:40:54 2023

@author: Matthias Maldet
"""

#k-means implementation
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import numpy as np
import pandas as pd


def cluster_data(*args, **kwargs):
    
    #Vehicle Objects
    data = kwargs.get("data", 0)
    list_consumers = kwargs.get("list_consumers", 0)
    timesteps = kwargs.get("timesteps", 8784)
    timestepsTotal = kwargs.get("timestepsTotal", 8784)
    sorting = kwargs.get("sorting", True)
    max_begin = kwargs.get("max_begin", True)
    

    firstrun=True
    
    data_to_cluster=[]
    
    for consumer in list_consumers:
        data_means=data.parse(consumer, nrows=timestepsTotal)
        data_means=data_means.drop("Generation", axis="columns")
        columnNames = list(data_means.columns)
        dimColumns = len(columnNames)
    
        #PV Extension
        data_pv_val=data_means["EL_Gen"]
        mean_pv=np.sum(data_pv_val)
        
        #Values for LSC dataframe
        data_kmeans = data_means.values
        
        if firstrun:
            data_to_cluster=data_kmeans
            firstrun=False
        else:
            data_to_cluster=np.append(data_to_cluster, data_kmeans, axis=1)
    
    #Append timestamp to try to get time reality
    times = np.transpose(np.array([list(range(1, 8785))]))
    data_to_cluster=np.append(data_to_cluster, times, axis=1)
    
    #Old maximum for normalizing to max demand
    old_max=np.max(data_to_cluster, 0)
    old_min=np.min(data_to_cluster, 0)
    
    kmeans_kwargs = {
     "init": "random",
    "n_init": 10,
    "max_iter": 300,
    "random_state": 42,
    }
    
    kmeans = KMeans(n_clusters=timesteps, **kmeans_kwargs)
    kmeans.fit(data_to_cluster)
    
    #Centers of clusters
    kmeans_centers=kmeans.cluster_centers_
    
    
    #new maximum for normalizing
    new_max=np.max(kmeans_centers, 0)
    new_min=np.min(kmeans_centers, 0)
    
    #Factor for normalizing --> replace nan, as they emerge in division by 0
    max_factor = np.divide(old_max, new_max)
    max_factor[np.isnan(max_factor)] = 0
    
    min_factor = np.divide(old_min, new_min)
    min_factor[np.isnan(min_factor)] = 0
    
    max_factor_new=[]
    
    counter=1
    for element in max_factor:
        if(counter%10==0):
            max_factor_new.append(min_factor[counter-1])
        else:
            max_factor_new.append(max_factor[counter-1])
            
        counter+=1

    #Normalized Centers
    kmeans_centers=np.multiply(kmeans_centers, max_factor_new)
    
    #Sort te dataframe based on time
    # ___________________________ !!!!!!!!!!!!!!! _________________________________________________
    #remove if not required
    
    if sorting:
    
        kmeans_centers=kmeans_centers[kmeans_centers[:, -1].argsort()]
    # ___________________________ !!!!!!!!!!!!!!! _________________________________________________
    
    #Sorting algorithm
    if(max_begin):
        array2sort=kmeans_centers[:,1]
        max_pos = np.argmax(array2sort)
        row2add=kmeans_centers[max_pos]
        kmeans_centers=np.delete(kmeans_centers, (max_pos), axis=0)
        kmeans_centers=np.insert(kmeans_centers, 0, row2add, axis=0)
        
    #Iteration over elements to get new dataframes
    #Struct to return dataframes
    data_clustered={}
    iterator=1
    
    for consumer in list_consumers:
        data=kmeans_centers[: , (iterator-1)*dimColumns: iterator*dimColumns ]
        iterator+=1
        
        #create new dataframe
        df=pd.DataFrame(data, columns=columnNames)
        
        df["EL_Gen"] = df["EL_Gen"]*timesteps/timestepsTotal*mean_pv/np.sum(df["EL_Gen"])
        
        #save dataframe in struct
        data_clustered[consumer] = df
        
    return data_clustered


def cluster_data_month(*args, **kwargs):
    
    #Vehicle Objects
    data = kwargs.get("data", 0)
    list_consumers = kwargs.get("list_consumers", 0)
    timestepsTotal = kwargs.get("timestepsTotal", 8784)
    sorting = kwargs.get("sorting", True)
    clusterPerMonth = kwargs.get("clusterPerMonth", 29)
    max_begin = kwargs.get("max_begin", True)
    

    
    firstrun=True
    
    data_to_cluster=[]
    kmeans_centers_new=[]
    
    for consumer in list_consumers:
        data_means=data.parse(consumer, nrows=timestepsTotal)
        data_means=data_means.drop("Generation", axis="columns")
        columnNames = list(data_means.columns)
        dimColumns = len(columnNames)
        
        #PV Extension
        data_pv_val=data_means["EL_Gen"]
        mean_pv=np.sum(data_pv_val)
    
        #Values for LSC dataframe
        data_kmeans = data_means.values
        
        if firstrun:
            data_to_cluster=data_kmeans
            firstrun=False
        else:
            data_to_cluster=np.append(data_to_cluster, data_kmeans, axis=1)
    
    #Append timestamp to try to get time reality
    times = np.transpose(np.array([list(range(1, 8785))]))
    data_to_cluster=np.append(data_to_cluster, times, axis=1)
    
    kmeans_kwargs = {
         "init": "random",
        "n_init": 10,
        "max_iter": 300,
        "random_state": 42,
    }
    
    firstrun=True
    for i in range(1, 13):
        if(i==1):
            range_down=0
            range_up=745
        elif(i==2):
            range_down=745
            range_up=1441
        elif(i==3):
            range_down=1441
            range_up=2185
        elif(i==4):
            range_down=2185
            range_up=2905
        elif(i==5):
            range_down=2905
            range_up=3649
        elif(i==6):
            range_down=3649
            range_up=4369
        elif(i==7):
            range_down=4369
            range_up=5115
        elif(i==8):
            range_down=5115
            range_up=5857
        elif(i==9):
            range_down=5857
            range_up=6577
        elif(i==10):
            range_down=6577
            range_up=7321
        elif(i==11):
            range_down=7321
            range_up=8041
        elif(i==12):
            range_down=8041
            range_up=8784
            
        data_sep = data_to_cluster[range_down:range_up, :]
    
        kmeans = KMeans(n_clusters=clusterPerMonth, **kmeans_kwargs)
        kmeans.fit(data_sep)
    
        kmeans_centers=kmeans.cluster_centers_
        
        #Old maximum for normalizing to max demand
        old_max=np.max(data_sep, 0)
        old_min=np.min(data_sep, 0)
        
        #new maximum for normalizing
        new_max=np.max(kmeans_centers, 0)
        new_min=np.min(kmeans_centers, 0)
        
        
        #Factor for normalizing --> replace nan, as they emerge in division by 0
        max_factor = np.divide(old_max, new_max)
        max_factor[np.isnan(max_factor)] = 0
        
        min_factor = np.divide(old_min, new_min)
        min_factor[np.isnan(min_factor)] = 0
        
        max_factor_new=[]
        
        counter=1
        for element in max_factor:
            if(counter%10==0):
                max_factor_new.append(min_factor[counter-1])
            else:
                max_factor_new.append(max_factor[counter-1])
                
            counter+=1
    
        #Normalized Centers
        kmeans_centers=np.multiply(kmeans_centers, max_factor_new)
        
        #Sort te dataframe based on time
        if sorting:
            kmeans_centers=kmeans_centers[kmeans_centers[:, -1].argsort()]
        
        
        #Append the kmeans centers for all months to data
        if firstrun:
            kmeans_centers_new=kmeans_centers
            firstrun=False
        else:
            kmeans_centers_new=np.append(kmeans_centers_new, kmeans_centers, axis=0)
    
    #Sorting algorithm
    if(max_begin):
        array2sort=kmeans_centers_new[:,1]
        max_pos = np.argmax(array2sort)
        row2add=kmeans_centers_new[max_pos]
        kmeans_centers_new=np.delete(kmeans_centers_new, (max_pos), axis=0)
        kmeans_centers_new=np.insert(kmeans_centers_new, 0, row2add, axis=0)
    
    #Iteration over elements to get new dataframes
    #Struct to return dataframes
    data_clustered={}
    iterator=1
    
    for consumer in list_consumers:
        data=kmeans_centers_new[: , (iterator-1)*dimColumns: iterator*dimColumns ]
        iterator+=1
        
        #create new dataframe
        df=pd.DataFrame(data, columns=columnNames)
        
        df["EL_Gen"] = df["EL_Gen"]*12*clusterPerMonth/timestepsTotal*mean_pv/np.sum(df["EL_Gen"])
        
        #save dataframe in struct
        data_clustered[consumer] = df
        
    return data_clustered


def get_elbow_curve(*args, **kwargs):
    
    #Vehicle Objects
    data = kwargs.get("data", 0)
    list_consumers = kwargs.get("consumers", 0)
    timesteps = kwargs.get("timesteps", 8784)
    


    firstrun=True
    
    data_to_cluster=[]
    
    for consumer in list_consumers:
        data_means=data.parse(consumer, nrows=timesteps)
        data_means=data_means.drop("Generation", axis="columns")


    
        #Values for LSC dataframe
        data_kmeans = data_means.values
        
        if firstrun:
            data_to_cluster=data_kmeans
            firstrun=False
        else:
            data_to_cluster=np.append(data_to_cluster, data_kmeans, axis=1)
    
    kmeans_kwargs = {
     "init": "random",
    "n_init": 10,
    "max_iter": 300,
    "random_state": 42,
    }
    
    # A list holds the SSE values for each k
    sse = []
    for k in range(1, 10, 1):
        print("Iteration number: " + str(k))
        print()
        kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
        kmeans.fit(data_kmeans)
        sse.append(kmeans.inertia_)
        
    plt.style.use("fivethirtyeight")
    plt.plot(range(1, 10, 1), sse)
    plt.xticks(range(1, 10, 1))
    plt.xlabel("Number of Clusters")
    plt.ylabel("SSE")
    plt.show()

    
    