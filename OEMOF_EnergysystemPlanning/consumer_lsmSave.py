# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 08:06:32 2022

@author: Matthias Maldet
"""

import numpy as np
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import household_components as household_comp
import math
import investment_components as investment_comp

def lsc_setup(*args, **kwargs):
    
    #List of Components
    components=[]
    
    lsc = kwargs.get("LSC", 0)
    lsm = kwargs.get("LSM", 0)
    lsm_list=kwargs.get("LSM_list", 0)
    timesteps=kwargs.get("timesteps", 8784)
    
    if(lsc==0 or lsm==0 or lsm_list==0):
        return 0
    
    
    #----------------- Electricity ------------------------------

    #Electricitydemand
    electricitydemand = el_comp.Demand(sector=lsc.getElectricitySector(), 
                                       demand_timeseries=lsc.input_data['electricityDemand'], label='Electricitydemand_' + lsc.label)            
    components.append(electricitydemand)
    
    #Electricity grid consumption
    c_grid_elec=kwargs.get("c_grid_elec", 0.062)
    c_abgabe_elec = kwargs.get("c_abgabe_elec", 0.018)
    red_local_elec = kwargs.get("red_local_elec", 1-0.57)
    red_regional_elec = kwargs.get("red_regional_elec", 1-0.28)
    c_grid_power=kwargs.get("c_grid_power", 0)
    
    P_elgrid = kwargs.get("P_elgrid", 11)
    em_elgrid = kwargs.get("em_elgrid", 0.209)
    
    electricitygridpurchase = el_comp.GridEmissions(sector=lsc.getElectricitySector(), sector_emissions=lsc.getEmissionsSector(),
                              costs_energy=np.add(lsc.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), costs_model=np.add(lsc.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + lsc.label, timesteps=timesteps, costs_power=c_grid_power, emissions=em_elgrid, P=P_elgrid)
    components.append(electricitygridpurchase)
    
    
    #Electricity feedin
    electricitygridFeedin = el_comp.GridFeedin(sector=lsc.getElectricitySector(), revenues=lsc.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + lsc.label, timesteps=timesteps, P=P_elgrid)  
    components.append(electricitygridFeedin)
    
    
    #PV Investment
    area_efficiency=kwargs.get("area_efficiency", 1)
    area_roof=kwargs.get("area_roof", 100)
    area_factor=kwargs.get("area_factor", 10)
    
    pv_invest = investment_comp.Photovoltaic_Investment(sector=lsc.getElectricitySector(), generation_timeseries=lsc.input_data['elGen'], 
                          label='PV_' + lsc.label, timesteps=timesteps, area_efficiency=area_efficiency, area_roof=area_roof, area_factor=area_factor)
    components.append(pv_invest)
    
    
    #Battery Investment
    battery_soc_max=kwargs.get("battery_soc_max", 5)
    battery_soc_start=kwargs.get("battery_soc_start", 0)
    battery_P_in=kwargs.get("battery_P_in", 5)
    battery_P_out=kwargs.get("battery_P_out", 5)
    
    battery_invest = investment_comp.Battery_Investment(sector_in=lsc.getElectricitySector(), label='Battery_' + lsc.label,
                          soc_max=battery_soc_max, SOC_start=battery_soc_start, P_in=battery_P_in, P_out=battery_P_out)
    components.append(battery_invest)
    
    
    #Sale electricity to LSM Hub
    sell_tariff_lsc_elec = np.multiply(np.add(lsc.input_data['electricityCosts'], lsc.input_data['electricityFeedin']), 1/2)
    sell_tariff_lsc_elec = kwargs.get("sell_tariff_lsc_elec", sell_tariff_lsc_elec)
    efficiency_lsc_elec = kwargs.get("efficiency_lsc_elec", 1)
    P_elec2lsm = kwargs.get("P_elec2lsm", 11)
    
    elec2lsm = lsc_comp.Electricity2LSC(sector_in=lsc.getElectricitySector(), sector_out=lsm.getElectricitySector(),
                                    P=P_elec2lsm, label="Elec2LSM_" + lsc.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec, efficiency=efficiency_lsc_elec)
    components.append(elec2lsm)
    
    
    #Purchase electricity from LSM Hubs
    rev_tariff_lsm_elec = kwargs.get("rev_tariff_lsm_elec", {0})
    buy_tariff_lsm_elec = kwargs.get("buy_tariff_lsm_elec", {0})
    efficiency_lsm_elec = kwargs.get("efficiency_lsm_elec", {1})
    P_lsm2elec = kwargs.get("P_lsm2elec", 11)
    
    for lsm_buy in lsm_list:
        
        labelsplit = lsm_buy.label.split('_')
        
        lsm2elec = lsc_comp.LSC2electricity(sector_in=lsm_buy.getElectricitySector(), sector_out=lsc.getElectricitySector(),
                                    P=P_lsm2elec, label="LSM2elec" + labelsplit[0] + labelsplit[1] + "_" + lsc.label, 
                                    rev_lsc=rev_tariff_lsm_elec[lsm_buy.label], c_consumer=buy_tariff_lsm_elec[lsm_buy.label], efficiency=efficiency_lsm_elec[lsm_buy.label]) 
        components.append(lsm2elec)
        
        
    #----------------- Heat ------------------------------
    
    #Heatdemand
    heatdemand = heat_comp.Demand(sector=lsc.getHeatSector(), 
                                  demand_timeseries=np.add(lsc.input_data['heatDemand'], lsc.input_data['hotwaterDemand']), 
                                  label='Heatdemand_' + lsc.label)
    components.append(heatdemand)
    
    
    #Heatpump
    P_heatpump_in = kwargs.get("P_heatpump_in", 7)
    P_heatpump_out = kwargs.get("P_heatpump_out", 7)
    
    heatpump_invest = investment_comp.Heatpump_Investment(sector_in = lsc.getElectricitySector(), sector_out=lsc.getHeatSector(), 
                            conversion_timeseries=lsc.input_data['copHP'], timesteps=timesteps, P_in=P_heatpump_in, P_out=P_heatpump_out, label='Heatpump_' + lsc.label)
    components.append(heatpump_invest)
    
    
    #Thermal storage
    P_heatstore = kwargs.get("P_heatstore", 7)
    v_heatstore = kwargs.get("v_heatstore", 0.3)
    
    thermalStorage_invest = investment_comp.ThermalStorage_Investment(sector_in=lsc.getHeatSector(), 
                                                                      P_in=P_heatstore, volume_max=v_heatstore, label='Heatstorage_' + lsc.label)
    components.append(thermalStorage_invest)
    
    
    #Sale heat to LSM and decision for district heat access
    P_districtheat = kwargs.get("P_districtheat", 10)
    sell_tariff_lsc_heat = np.array(lsm.input_data['heatFeedin'])
    efficiency_lsc_heat = kwargs.get("efficiency_lsc_heat", 1)
    
    heat2lsm = investment_comp.Heat2LSM_Investment(sector_in=lsc.getHeatSector(), sector_out=lsm.getHeatSector(),
                                    P=P_districtheat, label="Heat2LSM_" + lsc.label, rev_consumer=sell_tariff_lsc_heat, c_lsc=sell_tariff_lsc_heat,
                                    efficiency=efficiency_lsc_heat)
    components.append(heat2lsm)


    #Purchase heat from LSM Hubs
    rev_tariff_lsm_heat = kwargs.get("rev_tariff_lsm_heat", {0})
    buy_tariff_lsm_heat = kwargs.get("buy_tariff_lsm_heat", {0})
    efficiency_lsm_heat = kwargs.get("efficiency_lsm_heat", {1})
    
    for lsm_buy in lsm_list:
        
        labelsplit = lsm_buy.label.split('_')
        
        
        lsm2heat = investment_comp.LSM2heat_Investment(sector_in=lsm_buy.getHeatSector(), sector_out=lsc.getHeatSector(),
                                    P=P_districtheat, label="LSM2heat" + labelsplit[0] + labelsplit[1] + "_" + lsc.label, 
                                    rev_lsc=rev_tariff_lsm_heat[lsm_buy.label], c_consumer=buy_tariff_lsm_heat[lsm_buy.label], efficiency=efficiency_lsm_heat[lsm_buy.label])
        components.append(lsm2heat)
        
        
    #----------------- Waste ------------------------------
    
    #Accruing waste
    wasteAccruing = waste_comp.Accruing(sector=lsc.getWasteSector(), waste_timeseries=lsc.input_data['waste'], 
                                        label='WasteAccruing_' + lsc.label, color="yellow")
    components.append(wasteAccruing)
    
    
    #Waste storage
    v_max_wastestore = kwargs.get("v_max_wastestore", 0.1)
    v_start_wastestore = kwargs.get("v_start_wastestore", 0)
    waste_disposal_periods = kwargs.get("waste_disposal_periods", 0)
    wastestore_balanced = kwargs.get("wastestore_balanced", True)
    
    wasteStorageConsumer = waste_comp.WasteStorage(sector_in=lsc.getWasteSector(), sector_out=lsm.getWasteSector(), 
                                       volume_max=v_max_wastestore, volume_start=v_start_wastestore, disposal_periods=waste_disposal_periods, timesteps=timesteps,
                                       label="Wastestorage_" + lsm.label, color='mistyrose', balanced=wastestore_balanced)
    components.append(wasteStorageConsumer)


    #----------------- Water ------------------------------
    
    #Water demand
    waterdemand = water_comp.Waterdemand(sector_in=lsc.getPotablewaterSector(), sector_out=lsc.getSewageSector(), 
                                         demand=lsc.input_data['waterDemand'], timesteps=timesteps,
                                         label="Waterdemand_" + lsc.label, color='yellow')
    components.append(waterdemand)
    
    
    #Pipeline purchase
    waterpurchase_limit = kwargs.get("waterpurchase_limit", 1)
    waterpurchase_discount = kwargs.get("waterpurchase_discount", 0)
    
    pipelinepurchase = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=lsc.getPotablewaterSector(), label="PipelinepurchaseLimited_" + lsc.label, color='purple',
                                                      demand=lsc.input_data['waterDemand'], limit=waterpurchase_limit, costs_water=lsc.input_data['waterPipelineCosts'], discount=waterpurchase_discount)
    components.append(pipelinepurchase)
    
    #LSM Hub purchase
    lsmwaterpurchase_limit = kwargs.get("lsmwaterpurchase_limit", 1)
    lsmwaterpurchase_discount = kwargs.get("lsmwaterpurchase_discount", 0)
    rev_tariff_lsm_water = kwargs.get("rev_tariff_lsm_water", 0)
    buy_tariff_lsm_water = kwargs.get("buy_tariff_lsm_water", 0)
    efficiency_lsm_water = kwargs.get("efficiency_lsm_water", 1)
    
    for lsm_buy in lsm_list:
        
        labelsplit = lsm_buy.label.split('_')
    
        lscwaterpurchase = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=lsm_buy.getPotablewaterSector(), sector_out=lsc.getPotablewaterSector(), 
                                                           label="LSCpurchase" + labelsplit[0] + labelsplit[1] + "_" + lsc.label, color='brown',
                                                           demand=lsc.input_data['waterDemand'], limit=lsmwaterpurchase_limit, 
                                                           costs_consumer=buy_tariff_lsm_water[lsm_buy.label], discount=lsmwaterpurchase_discount, revenues_lsc=rev_tariff_lsm_water[lsm_buy.label],
                                                           efficiency=efficiency_lsm_water[lsm_buy.label])
        components.append(lscwaterpurchase)
        

    #Water reduction
    wff = kwargs.get("wff", 0)
    revenues_waterreduction = kwargs.get("revenues_waterreduction", 0)
    
    waterReduction = lsc_comp.WaterFlexibilitySimple(timesteps=timesteps, sector_in=lsc.getPotablewaterSector(), label="Waterreduction_" + lsc.label,
                                                       color='darkcyan', demand=lsc.input_data['waterDemand'], wff=wff,
                                                       revenues_consumer=revenues_waterreduction)
    components.append(waterReduction)
    
    
    #----------------- Sewage ------------------------------
    
    #Sewage from LSC to corresponding LSM hub
    sewage2Hub = lsc_comp.Sewage2Hub(sector_in=lsc.getSewageSector(), sector_out=lsm.getSewageSector(), timesteps=timesteps, 
                                    label="Sewage2Hub_" + lsc.label)
    components.append(sewage2Hub)
    
    #Optional extension to greywater uses or other alternative sewage uses
    
    #___________ Emissions ____________________

    co2price=kwargs.get("co2price", 0)
    
    emissions_sink = em_comp.Emissions(sector=lsc.getEmissionsSector(), costs=co2price, label='Emissiontotal_' + lsc.label)
    components.append(emissions_sink)
    
    return components


def lsm_setup(*args, **kwargs):
    
    #List of Components
    components=[]
    
    lsm = kwargs.get("LSM", 0)
    lsm_list=kwargs.get("LSM_list", 0)
    timesteps=kwargs.get("timesteps", 8784)
    
    if(lsm==0):
        return 0
    
    if(lsm in lsm_list):
        lsm_list.remove(lsm)
    
    
    #----------------- Electricity ------------------------------
    #Electricity grid consumption
    c_grid_elec=kwargs.get("c_grid_elec", 0.062)
    c_abgabe_elec = kwargs.get("c_abgabe_elec", 0.018)
    c_grid_power=kwargs.get("c_grid_power", 0)
    
    P_elgrid = kwargs.get("P_elgrid", 11)
    em_elgrid = kwargs.get("em_elgrid", 0.209)
    
    electricitygridpurchase = el_comp.GridEmissions(sector=lsm.getElectricitySector(), sector_emissions=lsm.getEmissionsSector(), 
                              costs_energy=np.add(lsm.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), costs_model=np.add(lsm.input_data['electricityCosts'], c_grid_elec + c_abgabe_elec), 
                              label='Electricitygridpurchase_' + lsm.label, timesteps=timesteps, costs_power=c_grid_power, emissions=em_elgrid, P=P_elgrid)
    components.append(electricitygridpurchase)
    
    
    #Electricity feedin
    electricitygridFeedin = el_comp.GridFeedin(sector=lsm.getElectricitySector(), revenues=lsm.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + lsm.label, timesteps=timesteps, P=P_elgrid)  
    components.append(electricitygridFeedin)
    
    
    #PV Investment
    area_efficiency=kwargs.get("area_efficiency", 1)
    area_roof=kwargs.get("area_roof", 100)
    area_factor=kwargs.get("area_factor", 10)
    
    pv_invest = investment_comp.Photovoltaic_Investment(sector=lsm.getElectricitySector(), generation_timeseries=lsm.input_data['elGen'], 
                          label='PV_' + lsm.label, timesteps=timesteps, area_efficiency=area_efficiency, area_roof=area_roof, area_factor=area_factor)
    components.append(pv_invest)
    
    
    #Battery Investment
    battery_soc_max=kwargs.get("battery_soc_max", 5)
    battery_soc_start=kwargs.get("battery_soc_start", 0)
    battery_P_in=kwargs.get("battery_P_in", 5)
    battery_P_out=kwargs.get("battery_P_out", 5)
    
    battery_invest = investment_comp.Battery_Investment(sector_in=lsm.getElectricitySector(), label='Battery_' + lsm.label,
                          soc_max=battery_soc_max, SOC_start=battery_soc_start, P_in=battery_P_in, P_out=battery_P_out)
    components.append(battery_invest)
    
    
    #----------------- Heat ------------------------------
    
    #Heatpump
    P_heatpump_in = kwargs.get("P_heatpump_in", 7)
    P_heatpump_out = kwargs.get("P_heatpump_out", 7)
    
    heatpump_invest = investment_comp.Heatpump_Investment(sector_in = lsm.getElectricitySector(), sector_out=lsm.getHeatSector(), 
                            conversion_timeseries=lsm.input_data['copHP'], timesteps=timesteps, P_in=P_heatpump_in, P_out=P_heatpump_out, label='Heatpump_' + lsm.label)
    components.append(heatpump_invest)
    
    
    #Thermal storage
    P_heatstore = kwargs.get("P_heatstore", 7)
    v_heatstore = kwargs.get("v_heatstore", 0.3)
    
    thermalStorage_invest = investment_comp.ThermalStorage_Investment(sector_in=lsm.getHeatSector(), 
                                                                      P_in=P_heatstore, volume_max=v_heatstore, label='Heatstorage_' + lsm.label)
    components.append(thermalStorage_invest)
    
    #Exhaust heat
    exhaustHeat = heat_comp.GridSink(sector=lsm.getHeatSector(), label='Exhaustheat_' + lsm.label, P=1000000, costs_model=0, color='slategrey')  
    components.append(exhaustHeat)
    
    
    #----------------- Waste ------------------------------
    
    #Waste combustion
    wastecomb_P_in=kwargs.get("wastecomb_P_in", 100)
    wastecomb_P_out=kwargs.get("wastecomb_P_out", 100)
    wastecomb_usable_energy=kwargs.get("wastecomb_usable_energy", 1)
    
    wasteCombustion_invest = investment_comp.Wastecombustion_Investment(sector_in=lsm.getWasteSector(), 
                                                                        sector_out=[lsm.getElectricitySector(), lsm.getHeatSector()],
                                             c_in=0, c_out=[0.04, 0.04], conversion_factor=[0.35, 0.4], P_in=wastecomb_P_in, P_out=wastecomb_P_out, 
                                             usable_energy=wastecomb_usable_energy, label='Wastecombustion_' + lsm.label, color='lightsteelblue')
    components.append(wasteCombustion_invest)
    
    
    #Waste storage
    wastestore_v_max=kwargs.get("wastestore_v_max", 12)
    wastestore_v_start=kwargs.get("wastestore_v_start", 0)
    wastestore_disposal_periods=kwargs.get("wastestore_disposal_periods", 0)
    wastestore_balanced=kwargs.get("wastestore_balanced", True)
    
    wasteStorage_invest = investment_comp.WasteStorage_Investment(sector_in=lsm.getWasteSector(), 
                                                                  sector_out=lsm.getWasteSector(), 
                                       volume_max=wastestore_v_max, volume_start=wastestore_v_start, disposal_periods=wastestore_disposal_periods, timesteps=timesteps,
                                       label="WastestorageDisposal_" + lsm.label, color='mistyrose', balanced=wastestore_balanced)
    components.append(wasteStorage_invest)
    
    
    #Waste Transmission to other LSM Hubs
    capacityTransmissionWastetruck=kwargs.get("transmissioncapacity_wastetruck", 20)
    distance_wastetruck=kwargs.get("distance_wastetruck", {20})
    emissions_wastetruck=kwargs.get("emissions_wastetruck", {0})
    costs_wastetruck=kwargs.get("costs_wastetruck", {0})
    delay_wastetruck=kwargs.get("delay_wastetruck", {0})

    
    for lsm_buy in lsm_list:
        
        labelsplit = lsm_buy.label.split('_')
        
        wastetruckEmissions = lsc_comp.Transport_emissions(sector_in=lsm.getWasteSector(), sector_out=lsm.getEmissionsWastetruckSector(), sector_emissions=lsm.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=capacityTransmissionWastetruck, distance=distance_wastetruck[lsm_buy.label], emissions=emissions_wastetruck[lsm_buy.label],
                                        label='WastetransportEmissions' + labelsplit[0] + labelsplit[1] + '_' + lsm.label, color='azure')
        components.append(wastetruckEmissions)
        
        transmissionWaste = lsc_comp.Delay_Transport(sector_in=lsm.getEmissionsWastetruckSector(), sector_out=lsm_buy.getWasteSector(), 
                                        efficiency=1, costs_transmission=costs_wastetruck[lsm_buy.label], delay=delay_wastetruck[lsm_buy.label], timesteps=timesteps, 
                                        capacity_transmission=capacityTransmissionWastetruck,
                                        label='Wastetransport' + labelsplit[0] + labelsplit[1] + '_' + lsm.label, color='aquamarine')
        components.append(transmissionWaste)
        
    
    #----------------- Water ------------------------------
    
    #Waste water from recovery
    wasted_water_costs=kwargs.get("wasted_water_costs", 0)
    
    water_waste = water_comp.Waterdisposal(sector=lsm.getPotablewaterSector(), costs=wasted_water_costs, label="WastedWater_" + lsm.label, color='brown')
    components.append(water_waste)
        
    #----------------- Sewage ------------------------------
    
    #Sewage treatment plant
    sewagetreat_eta_water=kwargs.get("sewagetreat_eta_water", 1)
    sewagetreat_emissions=kwargs.get("sewagetreat_emissions", 0.3)
    sewagetreat_tempdif=kwargs.get("sewagetreat_tempdif", 0)
    
    sewageTreatmentPlant_invest = investment_comp.SewageTreatment_Investment(sector_in=lsm.getSewageSector(), sector_loss=lsm.getElectricitySector(),
                                                  sector_out=[lsm.getPotablewaterSector(), lsm.getSludgeSector(), lsm.getHeatSector()],
                                                  sector_emissions=lsm.getEmissionsSector(), emissions=sewagetreat_emissions,
                                                  tempdif=sewagetreat_tempdif, label='SewageTreatment_' + lsm.label, color='black', c_in=0.04, eta_water=sewagetreat_eta_water)
    components.append(sewageTreatmentPlant_invest)
    
    
    #----------------- Sludge ------------------------------
    #Sludge combustion
    sludgecomb_P_in=kwargs.get("sludgecomb_P_in", 100)
    sludgecomb_P_out=kwargs.get("sludgecomb_P_out", 100)
    sludgecomb_usable_energy=kwargs.get("sludgecomb_usable_energy", 1)
    
    sludgecombustion_invest = investment_comp.Sludgecombustion_Investment(sector_in=lsm.getSludgeSector(), sector_out=[lsm.getElectricitySector(), lsm.getHeatSector()],
                                               conversion_factor=[0.35, 0.4], c_in=0, c_out=[0.06, 0.06], P_in=sludgecomb_P_in, P_out=sludgecomb_P_out, usable_energy=sludgecomb_usable_energy,
                                               label='Sludgecombustion_' + lsm.label, color='lightsteelblue') 
    components.append(sludgecombustion_invest)
    
    
    #Sludge storage
    sludgestore_v_max=kwargs.get("sludgestore_v_max", 1.5)
    sludgestore_Q_max=kwargs.get("sludgestore_Q_max", sludgestore_v_max)
    sludgestore_v_start=kwargs.get("sludgestore_v_start", 0)
    sludgestore_disposal_periods=kwargs.get("sludgestore_disposal_periods", 0)
    sludgestore_balanced=kwargs.get("sludgestore_balanced", True)
    
    sludgestorage_invest= investment_comp.SludgeStorage_Investment(sector_in=lsm.getSludgeSector(), sector_out=lsm.getSludgeSector(), timesteps=timesteps,
                                         c_in=0.01, c_out=0.01, volume_start=sludgestore_v_start, volume_max=sludgestore_v_max, Q_max=sludgestore_Q_max, disposal_periods=sludgestore_disposal_periods, balanced=sludgestore_balanced,
                                         label="SludgestorageTreatment_" + lsm.label, color='darkgoldenrod')
    components.append(sludgestorage_invest)
    
    
    #Sludge Transmission to other LSM Hubs
    capacityTransmissionSludgetruck=kwargs.get("transmissioncapacity_sludgetruck", 20)
    distance_sludgetruck=kwargs.get("distance_sludgetruck", {20})
    emissions_sludgetruck=kwargs.get("emissions_sludgetruck", {0})
    costs_sludgetruck=kwargs.get("costs_sludgetruck", {0})
    delay_sludgetruck=kwargs.get("delay_sludgetruck", {0})

    
    for lsm_buy in lsm_list:
        
        labelsplit = lsm_buy.label.split('_')
        
        sludgetruckEmissions = lsc_comp.Transport_emissions(sector_in=lsm.getSludgeSector(), sector_out=lsm.getEmissionsSludgetruckSector(), sector_emissions=lsm.getEmissionsSector(), 
                                        timesteps=timesteps, capacity_transmission=capacityTransmissionSludgetruck, distance=distance_sludgetruck[lsm_buy.label], emissions=emissions_sludgetruck[lsm_buy.label],
                                        label='SludgetransportEmissions' + labelsplit[0] + labelsplit[1] + '_' + lsm.label, color='azure')
        components.append(sludgetruckEmissions)
        
        transmissionSludge = lsc_comp.Delay_Transport(sector_in=lsm.getEmissionsSludgetruckSector(), sector_out=lsm_buy.getSludgeSector(), 
                                        efficiency=1, costs_transmission=costs_sludgetruck[lsm_buy.label], delay=delay_sludgetruck[lsm_buy.label], timesteps=timesteps, 
                                        capacity_transmission=capacityTransmissionSludgetruck,
                                        label='Sludgetransport' + labelsplit[0] + labelsplit[1] + '_' + lsm.label, color='aquamarine')
        components.append(transmissionSludge)
        
    
    
    #___________ Emissions ____________________

    co2price=kwargs.get("co2price", 0)
    
    emissions_sink = em_comp.Emissions(sector=lsm.getEmissionsSector(), costs=co2price, label='Emissiontotal_' + lsm.label)
    components.append(emissions_sink)
    
    return components
