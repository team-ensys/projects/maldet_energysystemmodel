# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 07:11:04 2021

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import StorageIntermediate as storage_int
import numpy as np


class Accruing():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_out = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'WasteAccruing_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nAccruing_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.waste_timeseries = kwargs.get("waste_timeseries", demandArray)
            color='yellow'
            self.color = kwargs.get("color", color)

        def component(self):
            
            m_waste = solph.Flow(fix=self.waste_timeseries, nominal_value=1)
            
            wasteAccruing = solph.Source(label=self.label, outputs={self.sector_out: m_waste})
            
            return wasteAccruing    


class WasteStorage():
    
    def __init__(self, *args, **kwargs):
        rho_municipalwaste = 100
        costsIn = 0
        costsOut=0
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 1
        V_max = 0.24
        V_start = 0
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        disposal_periods=0
        position='Consumer_1'
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        #self.v_start=V_start
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'WasteStorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nStorage_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        color='mistyrose'
        self.color = kwargs.get("color", color)
        self.empty_end = kwargs.get("empty_end", False)
        self.t_start = kwargs.get("t_start", 0)
        
        
        
    def component(self):
        #Energy Flows
        m_waste2storeWasteStoreIn = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_in)
        m_waste2storeWasteStoreOut = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_out)
        
        if(self.v_max==0):
            soc_divisor=1
        else:
            soc_divisor=self.v_max
        
        
        if(self.disposal_periods==0):
             wastestore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/soc_divisor, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
             return wastestore
        else:
        
            wastestore = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/soc_divisor, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps, empty_end=self.empty_end, t_start=self.t_start)
        
            return wastestore
    
    
class WastestorageSink():
    
    def __init__(self, *args, **kwargs):
        rho_municipalwaste = 100
        V_max=0.24
        deltaT=1
        position='Consumer_1'
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.v_max= kwargs.get("volume_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'WastestorageSink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nStorage\nSink_' + self.position)
        self.costs_in=0
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        m_storageDisposal = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_in)
            
        wasteDisposal = solph.Sink(label=self.label, inputs={self.sector_in: m_storageDisposal})
        
        return wasteDisposal
 



class WasteBiogas_old():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.5
            share_biowaste=0.225
            eta=0.9
            costsIn=0.068
            costsOut=1.265
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0
            position='Consumer_1'
            H_waste=3.4
            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'WasteBiogas_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nBiogas_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='magenta'
            self.color = kwargs.get("color", color)

    def component(self):
            m_wasteAd = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_waste*self.share_biowaste*self.eta*self.conversion_factor), variable_costs=self.costs_in)
            q_wasteAdGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            wasteAd = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteAd},
                outputs={self.sector_out : q_wasteAdGas},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_waste*self.share_biowaste*self.eta})
            
            
            return wasteAd
        
class WasteBiogas():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.5
            #Rest of biowaste is wrongly disposed biowaste that is therefore not treated
            share_biowaste=0.225
            eta=0.9
            costsIn=0.068
            costsOut=1.265
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.125
            maxEmissions=1000000
            position='Consumer_1'
            H_waste=3.4
            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'WasteBiogas_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nBiogas_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='magenta'
            self.color = kwargs.get("color", color)

    def component(self):
            m_wasteAd = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_waste*self.share_biowaste*self.eta*self.conversion_factor), variable_costs=self.costs_in)
            q_wasteAdGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            m_wasteADEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
            
            wasteAd = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteAd},
                outputs={self.sector_out : q_wasteAdGas, self.sector_emissions: m_wasteADEmissions},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_waste*self.share_biowaste*self.eta, 
                                    self.sector_emissions : self.emissions})
            
            
            return wasteAd
        
        

class WasteHydrogenAD():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.0148
            share_biowaste=0.225
            eta=0.9
            costsIn=0.068
            costsOut=0
            MLimit = 145
            deltaT=1
            emissions=0
            position='Consumer_1'

            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.M_max = kwargs.get("M_max", MLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'WasteADH2_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nADH2_' + self.position)
            color='plum'
            self.color = kwargs.get("color", color)

    def component(self):
            m_wasteAd = solph.Flow(min=0, max=1, nominal_value=self.M_max, variable_costs=self.costs_in)
            q_wasteAdH2 = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.share_biowaste*self.eta*self.conversion_factor, variable_costs=self.costs_out)
            
            wasteAdH2 = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteAd},
                outputs={self.sector_out : q_wasteAdH2},
                conversion_factors={self.sector_out : self.share_biowaste*self.eta*self.conversion_factor})
            
            
            return wasteAdH2    


class Wastecombustion():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.004, 0.004]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            #emissions=0.22
            #Emissions waste incineration
            emissions=0.125
            maxEmissions=1000000
            position='Consumer_1'
            H_waste=3.4
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Wastecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nCombustion_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='brown'
            self.color = kwargs.get("color", color)
            
            #Factor if not all recovered energy can be used by the LSC
            self.usable_energy = kwargs.get("usable_energy", 1)
            

    def component(self):
            m_wasteCombustion = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_waste, variable_costs=self.costs_in)
            q_wasteCombustionEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_wasteCombustionTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy})
                
            else:
                m_wastecombustionEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl, self.sector_emissions : m_wastecombustionEmissions},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy, self.sector_emissions : self.emissions})
            
            
            return wasteCombustion
        
class Wastecombustion_old():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.004, 0.004]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.22
            maxEmissions=1000000
            position='Consumer_1'
            H_waste=3.4
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Wastecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nCombustion_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='brown'
            self.color = kwargs.get("color", color)
            
            #Factor if not all recovered energy can be used by the LSC
            self.usable_energy = kwargs.get("usable_energy", 1)
            

    def component(self):
            m_wasteCombustion = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_waste, variable_costs=self.costs_in)
            q_wasteCombustionEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_wasteCombustionTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy})
                
            else:
                m_wastecombustionEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl, self.sector_emissions : m_wastecombustionEmissions},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy, self.sector_emissions : self.emissions*self.usable_energy})
            
            
            return wasteCombustion
        
        
class Fermentation():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.5
            share_biowaste=0.225
            eta=0.9
            costsIn=0
            costsOut=2.07
            rate_max=0.01
            rate_fermentation=0.0888
            deltaT=1
            emissions=0
            position='Consumer_1'
            H_waste=3.4
            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.rate = kwargs.get("rate", rate_fermentation)
            self.rate_max = kwargs.get("rate_max", rate_max)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'WasteFermentation_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nFermentation_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='lightgrey'
            self.color = kwargs.get("color", color)
            

    def component(self):
            m_wasteFerm = solph.Flow(min=0, max=1, nominal_value=self.rate_max*self.deltaT/(self.rate*self.share_biowaste*self.eta), variable_costs=self.costs_in)
            v_wasteFermHydro = solph.Flow(min=0, max=1, nominal_value=self.rate_max*self.deltaT, variable_costs=self.costs_out)
            
            wasteFerm = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteFerm},
                outputs={self.sector_out : v_wasteFermHydro},
                conversion_factors={self.sector_out : self.rate*self.share_biowaste*self.eta})
            
            
            
            return wasteFerm
        
        
class WasteBiofuel():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class

            share_biowaste=0.225
            eta=0.9
            costsIn=0
            costsOut=530
            M_max=700
            deltaT=1
            emissions=0
            position='Consumer_1'
            rho_biofuel=880
            water_content=0.5
            water_recovery=0.6
            
            self.eta = kwargs.get("eta", eta)

            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.M_max = kwargs.get("M_max", M_max)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.factor_processing = self.deltaT/24
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'WasteBiofuel_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nBiofuel_' + self.position)
            self.rho_biofuel = kwargs.get("rho_biofuel", rho_biofuel)
            self.water_content = kwargs.get("water_content", water_content)
            self.water_recovery = kwargs.get("water_recovery", water_recovery)
            self.rho_water=1000
            color='blue'
            self.color = kwargs.get("color", color)
            

    def component(self):
            m_waste2biofuel = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.factor_processing, variable_costs=self.costs_in)
            v_waste2biofuel = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.factor_processing/self.rho_biofuel*self.share_biowaste*self.eta, variable_costs=self.costs_out)
            
            if(self.water_recovery==0):
            
                waste2biofuel = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_waste2biofuel},
                    outputs={self.sector_out[0] : v_waste2biofuel},
                    conversion_factors={self.sector_out[0] : self.factor_processing/self.rho_biofuel*self.share_biowaste*self.eta})
                
            else:
                v_waterRecovered = solph.Flow(min=0, max=1, nominal_value= self.M_max*self.water_content*self.share_biowaste*self.water_recovery/self.rho_water)
                
                waste2biofuel = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_waste2biofuel},
                    outputs={self.sector_out[0] : v_waste2biofuel, self.sector_out[1] : v_waterRecovered},
                    conversion_factors={self.sector_out[0] : self.factor_processing/self.rho_biofuel*self.share_biowaste*self.eta, 
                                        self.sector_out[1] : self.water_content*self.share_biowaste*self.water_recovery/self.rho_water})
            
            
            
            
            return waste2biofuel
        
        
class BiofuelSink():
    
    def __init__(self, *args, **kwargs):
        share_biowaste=0.225
        eta=0.9
        M_max=700
        deltaT=1
        emissions=0
        position='Consumer_1'
        rho_biofuel=880
            
        self.eta = kwargs.get("eta", eta)

        self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)

        self.costs_in = 0
        self.M_max = kwargs.get("M_max", M_max)
        self.sector_in = kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.factor_processing = self.deltaT/24
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'BiofuelSink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Biofuel\nSink_' + self.position)
        self.rho_biofuel = kwargs.get("rho_biofuel", rho_biofuel)
        color='cyan'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        v_waste2biofuel = solph.Flow(min=0, max=1, nominal_value=self.M_max*self.factor_processing/self.rho_biofuel*self.share_biowaste*self.eta, variable_costs=self.costs_in)
            
        wasteDisposal = solph.Sink(label=self.label, inputs={self.sector_in: v_waste2biofuel})
        
        return wasteDisposal
    
class Wastedisposal():
    
    def __init__(self, *args, **kwargs):

        rho_municipalwaste = 100
        V_max=0.24
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        emissions=0.382
        maxEmissions=1000000
        minDisposal=0
        
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.v_max= kwargs.get("volume_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.minDisposalPercent = kwargs.get("min_disposal", minDisposal)
        self.position = kwargs.get("position", position)
        label = 'Wastedisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nDisposal_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.inputWaste = kwargs.get("waste_in", 0)
        
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.max_emissions=kwargs.get("max_emissions", maxEmissions)
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        color='red'
        self.color = kwargs.get("color", color)
        
        self.minDisposal = np.sum(np.multiply(self.inputWaste, self.minDisposalPercent))
        
        
    def component(self):


                
        m_wasteDispose = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_in, summed_min=self.minDisposal/(self.v_max*self.rho_municipalwaste))
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            
            wasteDispose = solph.Sink(label=self.label, inputs={self.sector_in: m_wasteDispose})
            
        else:
             m_wastedisposalEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
             wasteDispose = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteDispose},
                    outputs={self.sector_emissions : m_wastedisposalEmissions},
                    conversion_factors={self.sector_emissions : self.emissions})
            
        

        
        return wasteDispose
    
    
class Wastemarket():
    
    def __init__(self, *args, **kwargs):

        rho_municipalwaste = 100
        V_max=0.24
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.v_max= kwargs.get("volume_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Wastemarket_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nMarket_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsArray = np.zeros(self.timesteps)
        
        self.revenues=kwargs.get("revenues", costsArray)
        self.revenues=np.multiply(self.revenues, -1)
        color='green'
        self.color = kwargs.get("color", color)
        
        
    def component(self):


                
        m_waste2Market = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.revenues)
            
        waste2Market = solph.Sink(label=self.label, inputs={self.sector_in: m_waste2Market})
        

        
        return waste2Market
    
# Waste to Biowaste Transformer
class Waste2Biowaste_old():
    
    def __init__(self, *args, **kwargs):
        
        share_biowaste=1
        position='Consumer_1'
        delay=0
        costs_transmission=0
        max_waste=10000000
        deltaT=1
        timesteps=8760

  
        self.max_waste = kwargs.get("max_waste", max_waste)
        self.deltaT = kwargs.get("timerange", deltaT)
        #Min share damit gesamter Biowaste verarbeitet wird --> sonst Gefahr, dass einfach alles restlos entsorgt wird, um Investition zu übergehen
        self.share_biowaste_min = kwargs.get("share_biowaste_min", 0)
        self.share_biowaste_max = kwargs.get("share_biowaste_max", share_biowaste)
        self.costs_transmission = kwargs.get("costs_transmission", costs_transmission)
        self.costs_out = kwargs.get("c_out", 0)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.delay = kwargs.get("delay", delay)
        self.position = kwargs.get("position", position)
        label = 'Sewage2Hub_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste2Biowaste_' + self.position)
        color='aquamarine'
        self.color = kwargs.get("color", color)
        self.timesteps=kwargs.get("timesteps", timesteps)
        self.timestepsTotal=kwargs.get("timestepsTotal", timesteps)
        
        self.max_waste=self.max_waste*self.timesteps/self.timestepsTotal
        
    def component(self):
        x_in = solph.Flow(min=0, max=1, nominal_value=self.max_waste*self.deltaT, variable_costs = self.costs_transmission, 
                          summed_min= self.max_waste*self.share_biowaste_min, summed_max=self.share_biowaste_max)
        x_out = solph.Flow(min=0, max=1, nominal_value=self.max_waste*self.deltaT, variable_costs=self.costs_out)
            
            
        waste2biowaste = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : x_in},
                outputs={self.sector_out : x_out},
                conversion_factors={self.sector_out : 1})

        
        return waste2biowaste  
  
# Waste to Biowaste Transformer
class Waste2Biowaste():
    
    def __init__(self, *args, **kwargs):
        
        share_biowaste=1
        position='Consumer_1'
        delay=0
        costs_transmission=0
        max_waste=10000000
        deltaT=1
        timesteps=8760

  
        self.max_waste = kwargs.get("max_waste", max_waste)
        self.deltaT = kwargs.get("timerange", deltaT)
        #Min share damit gesamter Biowaste verarbeitet wird --> sonst Gefahr, dass einfach alles restlos entsorgt wird, um Investition zu übergehen
        self.share_biowaste_min = kwargs.get("share_biowaste_min", 0)
        self.share_biowaste_max = kwargs.get("share_biowaste_max", share_biowaste)
        self.costs_transmission = kwargs.get("costs_transmission", costs_transmission)
        self.costs_out = kwargs.get("c_out", 0)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.delay = kwargs.get("delay", delay)
        self.position = kwargs.get("position", position)
        label = 'Sewage2Hub_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste2Biowaste_' + self.position)
        color='aquamarine'
        self.color = kwargs.get("color", color)
        self.timesteps=kwargs.get("timesteps", timesteps)
        self.timestepsTotal=kwargs.get("timestepsTotal", timesteps)
        
        self.timeseries_waste=kwargs.get("timeseries_waste", np.zeros(8784))
        
        self.min_waste_array = np.multiply(self.timeseries_waste, self.share_biowaste_min)
        self.max_waste_array = np.multiply(self.timeseries_waste, self.share_biowaste_max)
        
        
        self.max_waste=self.max_waste*self.timesteps/self.timestepsTotal
        
    def component(self):
        x_in = solph.Flow(min=self.min_waste_array, max=self.max_waste*self.deltaT, nominal_value=1, variable_costs = self.costs_transmission)
        x_out = solph.Flow(min=0, max=1, nominal_value=self.max_waste*self.deltaT, variable_costs=self.costs_out)
            
            
        waste2biowaste = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : x_in},
                outputs={self.sector_out : x_out},
                conversion_factors={self.sector_out : 1})

        
        return waste2biowaste  
    

class WasteReduction_old():
    
    def __init__(self, *args, **kwargs):
        M_max=10000000
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.M_max= kwargs.get("M_max", M_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Wastereduction_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nReduction\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        self.reduction_rate=kwargs.get("reduction_rate", 0.2)
        
        revenuesWasteArray = np.zeros(self.timesteps)
            
        self.costs = kwargs.get("wastereduction_costs", revenuesWasteArray)
        self.timestepsTotal=kwargs.get("timestepsTotal", timesteps)
        
        self.M_max=self.M_max*self.timesteps/self.timestepsTotal
        
        
    def component(self):
        m_wastereduction = solph.Flow(min=0, max=1, nominal_value=self.M_max, variable_costs=self.costs, summed_max=self.reduction_rate)
            
        wastereduction = solph.Sink(label=self.label, inputs={self.sector_in: m_wastereduction})
        
        return wastereduction
    
    
class WasteReduction():
    
    def __init__(self, *args, **kwargs):
        M_max=10000000
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.M_max= kwargs.get("M_max", M_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Wastereduction_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nReduction\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        self.reduction_rate=kwargs.get("reduction_rate", 0.2)
        self.min_reduction=kwargs.get("min_reduction", 0)
        
        revenuesWasteArray = np.zeros(self.timesteps)
            
        self.costs = kwargs.get("wastereduction_costs", revenuesWasteArray)
        self.timestepsTotal=kwargs.get("timestepsTotal", timesteps)
        
        self.M_max=self.M_max*self.timesteps/self.timestepsTotal
        
        self.timeseries_waste=kwargs.get("timeseries_waste", np.zeros(8784))
        
        self.min_waste_array = np.multiply(self.timeseries_waste, self.min_reduction)
        self.max_waste_array = np.multiply(self.timeseries_waste, self.reduction_rate)
        
    def component(self):
        m_wastereduction = solph.Flow(min=self.min_reduction, max=self.max_waste_array, nominal_value=1, variable_costs=self.costs)
            
        wastereduction = solph.Sink(label=self.label, inputs={self.sector_in: m_wastereduction})
        
        return wastereduction
    
class Wastedisposal_extended():
    
    def __init__(self, *args, **kwargs):

        rho_municipalwaste = 100
        V_max=0.24
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        emissions=0.382
        maxEmissions=1000000

        
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.v_max= kwargs.get("volume_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.minDisposalPercent = kwargs.get("min_disposal", 0)
        self.maxDisposalPercent = kwargs.get("max_disposal", 1)
        self.position = kwargs.get("position", position)
        label = 'Wastedisposal_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nDisposal_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.inputWaste = kwargs.get("waste_in", 0)
        
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.max_emissions=kwargs.get("max_emissions", maxEmissions)
        
        costsArray = np.zeros(self.timesteps)
        
        self.costs_in=kwargs.get("costs", costsArray)
        color='red'
        self.color = kwargs.get("color", color)
        
        self.minDisposal = (np.multiply(self.inputWaste, self.minDisposalPercent))
        self.maxDisposal = (np.multiply(self.inputWaste, self.maxDisposalPercent))
        
        
        
        
    def component(self):


                
        m_wasteDispose = solph.Flow(min=self.minDisposal, max=self.maxDisposal, nominal_value=1, variable_costs=self.costs_in)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            
            wasteDispose = solph.Sink(label=self.label, inputs={self.sector_in: m_wasteDispose})
            
        else:
             m_wastedisposalEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
             wasteDispose = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteDispose},
                    outputs={self.sector_emissions : m_wastedisposalEmissions},
                    conversion_factors={self.sector_emissions : self.emissions})
            
        

        
        return wasteDispose
    
    