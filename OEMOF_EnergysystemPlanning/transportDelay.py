# -*- coding: utf-8 -*-
"""
Created on Wed May 25 11:20:42 2021

@author: Matthias Maldet
"""


from oemof.network import network
from pyomo.core.base.block import SimpleBlock
from pyomo.environ import Constraint
from pyomo.environ import Set



class TransportDelay(network.Transformer):
    

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.delay= kwargs.get("delay", 0)
        self.timesteps=kwargs.get("timesteps", 8760)
        self.efficiency= kwargs.get("efficiency", 1)
        self.sector_input = kwargs.get("sector_in")
        self.sector_output = kwargs.get("sector_out")
        self.index=0


    def constraint_group(self):
        return TransportDelayBlock


class TransportDelayBlock(SimpleBlock):
    

    CONSTRAINT_GROUP = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _create(self, group=None):
        """Creates the relation for the class:`OffsetTransformer`.
        Parameters
        ----------
        group : list
            List of oemof.solph.custom.OffsetTransformer objects for which
            the relation of inputs and outputs is created
            e.g. group = [ostf1, ostf2, ostf3, ...]. The components inside
            the list need to hold an attribute `coefficients` of type dict
            containing the conversion factors for all inputs to outputs.
        """
        if group is None:
            return None

        m = self.parent_block()
        
        in_flows = {m: [i for i in m.inputs.keys()] for m in group}
        out_flows = {m: [i for i in m.outputs.keys()] for m in group}


        counter=0
        for n in group:
            n.index=counter
            counter+=1
        

        self.TRANSPORTDELAY = Set(initialize=[n for n in group])


        def _delay_rule(block, n, t):
            expr=0
            if(t>n.timesteps-n.delay-1):
                return Constraint.Skip
            else:
                expr -= n.efficiency*sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(in_flows.keys())[n.index]) == o.label)
                expr += sum(m.flow[i, o, t+n.delay] for (i, o) in m.FLOWS if  str(list(out_flows.keys())[n.index]) == i.label)

                return expr == 0
            
            
        def _initial_rule(block, n, t):
            if(t<n.delay and n.delay!=0):
                return(sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(out_flows.keys())[n.index]) == i.label)==0)
            else:
                return Constraint.Skip



        self.relation = Constraint(
            self.TRANSPORTDELAY, m.TIMESTEPS, rule=_delay_rule
        )
        
        self.initial = Constraint(
            self.TRANSPORTDELAY, m.TIMESTEPS, rule=_initial_rule
        )
        
        