# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 14:47:14 2022

@author: Matthias Maldet
"""

import pyomo.environ as po
import pyomo.core as pyomo
import numpy as np
#import pyomo_rules

from pyomo.environ import Binary
from pyomo.environ import Constraint
from pyomo.environ import Expression
from pyomo.environ import NonNegativeReals
from pyomo.environ import Set
from pyomo.environ import Var

def flexibility_sum(*args, **kwargs):
    
    #Vehicle Objects
    component = kwargs.get("component", 0)
    interval = kwargs.get("interval", 0)

    
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0 or component==0 or interval==0):
        print('Model not found!')
        return 0
    
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        componentName = component.label + '_FlexibilitySum'
        
        modelIn.add_component(componentName, myBlock)
        
        def componentsumRule(model, t):
            

            
            summe=0
            for count in range(0, interval):
                

                if(t%interval==0):
                    

                    summe+=(sum(modelIn.flow[i, o, t+count] for (i, o) in modelIn.FLOWS if ((i.label==str(component.label) and o.label==str(component.sector_out)))))
                    
                else:
                    return Constraint.Skip
            
            return summe==0
        
        
    if component:
        
        myBlock.componentsum = pyomo.Constraint(
            myBlock.t,
            rule=componentsumRule,
            doc='Intervalsum')
                
        
     
    return modelIn      
            
        