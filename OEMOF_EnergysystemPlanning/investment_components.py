# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 11:52:32 2022

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np
import transformerLossesMultipleOut as tlmo
import source_emissions as em
import StorageIntermediate as storage_int

from oemof.tools import economics


class Photovoltaic_Investment():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsOut=0
            AreaFactor = 10
            AreaRoof = 100
            AreaEfficiency = 1
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            
            self.costs_out = kwargs.get("c_out", costsOut)
            self.Area_Factor = kwargs.get("area_factor", AreaFactor)
            self.Area_Roof = kwargs.get("area_roof", AreaRoof)
            self.Area_Efficiency = kwargs.get("area_efficiency", AreaEfficiency)
            self.sector_out = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Photovoltaic_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'PV_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            color='gold'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("generation_timeseries", generationArray)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 20)
            self.c_inv_var = kwargs.get("c_inv_var", 1300)
            self.c_inv_fix = kwargs.get("c_inv_fix", 3500)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal

        def component(self):
            
            Q_pvGenerationValue = solph.Flow(fix = self.conversion_timeseries, 
                                             investment=solph.Investment(minimum=0, maximum=self.Area_Efficiency*self.Area_Roof/self.Area_Factor, existing=self.existing, 
                                                                         ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
            
            pvGeneration = solph.Source(label=self.label, outputs={self.sector_out: Q_pvGenerationValue})
            
            return pvGeneration
        
class Photovoltaic_Investment_simple():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsOut=0
            AreaFactor = 10
            AreaRoof = 100
            AreaEfficiency = 1
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            
            self.costs_out = kwargs.get("c_out", costsOut)
            self.Area_Factor = kwargs.get("area_factor", AreaFactor)
            self.Area_Roof = kwargs.get("area_roof", AreaRoof)
            self.Area_Efficiency = kwargs.get("area_efficiency", AreaEfficiency)
            self.sector_out = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Photovoltaic_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'PV_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            color='gold'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("generation_timeseries", generationArray)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 20)
            self.c_inv_var = kwargs.get("c_inv_var", 1300)
            self.c_inv_fix = kwargs.get("c_inv_fix", 3500)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal

        def component(self):
            
            Q_pvGenerationValue = solph.Flow(fix = self.conversion_timeseries, 
                                             investment=solph.Investment(minimum=0, maximum=self.Area_Efficiency*self.Area_Roof/self.Area_Factor, existing=self.existing, 
                                                                         ep_costs=self.annuity_var))
            
            pvGeneration = solph.Source(label=self.label, outputs={self.sector_out: Q_pvGenerationValue})
            
            return pvGeneration



class Battery_Investment():
    
    def __init__(self, *args, **kwargs):
        P_inMax = 10
        P_outMax = 10
        costsIn = 0.003
        costsOut=0.003
        Eta_in = 0.95
        Eta_out = 0.95
        Eta_sb = 0.999
        SOC_max = 10
        SOC_start = 10
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_in = kwargs.get("P_in", P_inMax)
        self.P_out = kwargs.get("P_out", P_outMax)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Battery_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Battery_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        color = 'red'
        self.color = kwargs.get("color", color)
        
        self.timesteps = kwargs.get("timesteps", 8760)
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        
        #Extension investment decision
        self.existing = kwargs.get("existing", 0)
        self.lifespan = kwargs.get("lifespan", 10)
        self.c_inv_var = kwargs.get("c_inv_var", 1000)
        self.c_inv_fix = kwargs.get("c_inv_fix", 900)
        self.wacc = kwargs.get("wacc", 0.03)
        self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
        self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
        
        self.annuity_var*=self.timesteps/self.timestepsTotal
        self.annuity_fix*=self.timesteps/self.timestepsTotal
        
        
    def component(self):
        #Energy Flows
        q_elec2battery = solph.Flow(variable_costs=self.costs_in, 
                                    investment=solph.Investment(minimum=0, maximum=self.P_in*self.deltaT, existing=self.existing, 
                                                                         ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
        q_battery2elec = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
        
        if(self.soc_max==0):
            soc_divisor=1
        else:
            soc_divisor=self.soc_max

            
        battery = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_elec2battery},
                outputs={self.sector_out: q_battery2elec},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/soc_divisor, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return battery
    
    
class Battery_Investment_simple():
    
    def __init__(self, *args, **kwargs):
        P_inMax = 10
        P_outMax = 10
        costsIn = 0.003
        costsOut=0.003
        Eta_in = 0.95
        Eta_out = 0.95
        Eta_sb = 0.999
        SOC_max = 10
        SOC_start = 10
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_in = kwargs.get("P_in", P_inMax)
        self.P_out = kwargs.get("P_out", P_outMax)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Battery_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Battery_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        color = 'red'
        self.color = kwargs.get("color", color)
        
        self.timesteps = kwargs.get("timesteps", 8760)
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        
        #Extension investment decision
        self.existing = kwargs.get("existing", 0)
        self.lifespan = kwargs.get("lifespan", 10)
        self.c_inv_var = kwargs.get("c_inv_var", 1000)
        self.c_inv_fix = kwargs.get("c_inv_fix", 900)
        self.wacc = kwargs.get("wacc", 0.03)
        self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
        self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
        
        self.annuity_var*=self.timesteps/self.timestepsTotal
        self.annuity_fix*=self.timesteps/self.timestepsTotal
        
        
    def component(self):
        #Energy Flows
        q_elec2battery = solph.Flow(variable_costs=self.costs_in, 
                                    investment=solph.Investment(minimum=0, maximum=self.P_in*self.deltaT, existing=self.existing, 
                                                                         ep_costs=self.annuity_var))
        q_battery2elec = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
        
        if(self.soc_max==0):
            soc_divisor=1
        else:
            soc_divisor=self.soc_max

            
        battery = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_elec2battery},
                outputs={self.sector_out: q_battery2elec},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/soc_divisor, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return battery
    
    
class Heatpump_Investment():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsIn=0
            costsOut=0.0015
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            

            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Heatpump_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heatpump_' + self.position)
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            color='orange'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = np.array(kwargs.get("conversion_timeseries", generationArray))
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 8)
            self.c_inv_var = kwargs.get("c_inv_var", 945)
            self.c_inv_fix = kwargs.get("c_inv_fix", 500)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal

        def component(self):
            q_HPel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_HPth = solph.Flow(variable_costs=self.costs_out,
                                investment=solph.Investment(minimum=0, maximum=self.P_out*self.deltaT, existing=self.existing, 
                                                                         ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
            
            heatpump = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_HPel},
                outputs={self.sector_out : q_HPth},
                conversion_factors={self.sector_out : self.conversion_timeseries})
            return heatpump
        
        
class Heatpump_Investment_simple():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsIn=0
            costsOut=0.0015
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            

            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Heatpump_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heatpump_' + self.position)
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            color='orange'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = np.array(kwargs.get("conversion_timeseries", generationArray))
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 8)
            self.c_inv_var = kwargs.get("c_inv_var", 945)
            self.c_inv_fix = kwargs.get("c_inv_fix", 500)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal

        def component(self):
            q_HPel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_HPth = solph.Flow(variable_costs=self.costs_out,
                                investment=solph.Investment(minimum=0, maximum=self.P_out*self.deltaT, existing=self.existing, 
                                                                         ep_costs=self.annuity_var))
            
            heatpump = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_HPel},
                outputs={self.sector_out : q_HPth},
                conversion_factors={self.sector_out : self.conversion_timeseries})
            return heatpump


class ThermalStorage_Investment():
    
    def __init__(self, *args, **kwargs):
        P_inMax = 7
        costsIn = 0.0005
        costsOut=0.0005
        Eta_in = 0.8
        Eta_out = 0.8
        Eta_sb = 0.999
        V_max = 0.3
        V_start = 0
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        tempdif=45
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_in = kwargs.get("P_in", P_inMax)
        self.P_out = kwargs.get("P_out", self.P_in)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'ThermalStorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Thermal\nStorage_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        color='red'
        self.color = kwargs.get("color", color)
        self.tempdif = kwargs.get("tempdif", tempdif)
            
        self.thCapacityWater = 4.190/3600
        self.rhoWater = 1000
        
        self.soc_max = self.thCapacityWater*self.rhoWater*self.tempdif*self.v_max
        
        self.timesteps = kwargs.get("timesteps", 8760)
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        
        #Extension investment decision
        self.existing = kwargs.get("existing", 0)
        self.lifespan = kwargs.get("lifespan", 20)
        self.c_inv_var = kwargs.get("c_inv_var", 41)
        self.c_inv_fix = kwargs.get("c_inv_fix", 450)
        self.wacc = kwargs.get("wacc", 0.03)
        self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
        self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
        
        self.annuity_var*=self.timesteps/self.timestepsTotal
        self.annuity_fix*=self.timesteps/self.timestepsTotal
        
    def component(self):
        #Energy Flows
        q_thStoreIn = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.P_in*self.deltaT, existing=self.existing, 
                                                                         ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
        q_thStoreOut = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
        
        if(self.v_max==0):
            soc_divisor=1
        else:
            soc_divisor=self.soc_max
            
        thStore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_thStoreIn},
                outputs={self.sector_out: q_thStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.v_start/soc_divisor, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return thStore
    
    
#Heat Sale on LSC Hub via existing or extended district heat grid
#Investment decision in district heat and selling process in one block
#Optimizer decides whether investment in district heat is done
class Heat2LSM_Investment():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0.001
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_consumer = kwargs.get("rev_consumer", tariff)
            self.rev_consumer=np.multiply(rev_consumer, -1)
            self.c_lsc = kwargs.get("c_lsc", rev_consumer)
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Heat2LSC_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heat2LSC_' + self.position)
            color = 'olivedrab'
            self.color = kwargs.get("color", color)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 30)
            self.c_inv_var = kwargs.get("c_inv_var", 500)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal



        def component(self):
            q_consumer2lscIn = solph.Flow(variable_costs=self.rev_consumer, investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, 
                                                                                                        existing=self.existing, ep_costs=self.annuity_var))
            q_consumer2lscOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_lsc)
            heat2LSC = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_consumer2lscIn},
                outputs={self.sector_out : q_consumer2lscOut},
                conversion_factors={self.sector_out : self.efficiency})
            return heat2LSC
   
        
#District heat investment
class DistrictHeat_Investment():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", 1)

            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'DistrictHeatInvest_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heat2LSC_' + self.position)
            color = 'olivedrab'
            self.color = kwargs.get("color", color)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 20)
            self.c_inv_var = kwargs.get("c_inv_var", 500)
            self.c_inv_fix = kwargs.get("c_inv_fix", 350)
            #self.c_inv_fix = kwargs.get("c_inv_fix", 0)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = self.c_inv_fix
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal


        def component(self):
            
            if(self.c_inv_fix!=0):
                q_dh_in = solph.Flow(variable_costs=0, investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, 
                                                                                                        existing=self.existing, ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
            else:
                q_dh_in = solph.Flow(variable_costs=0, investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, 
                                                                                                        existing=self.existing, ep_costs=self.annuity_var))
                
            q_dh_out = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=0)
            
            districtheat = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_dh_in},
                outputs={self.sector_out : q_dh_out},
                conversion_factors={self.sector_out : self.efficiency})
            
            return districtheat
        
        
    
#District heat investment
class DistrictHeat_Investment_simple():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", 1)

            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'DistrictHeatInvest_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heat2LSC_' + self.position)
            color = 'olivedrab'
            self.color = kwargs.get("color", color)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 20)
            self.c_inv_var = kwargs.get("c_inv_var", 500)
            self.c_inv_fix = kwargs.get("c_inv_fix", 350)
            #self.c_inv_fix = kwargs.get("c_inv_fix", 0)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = self.c_inv_fix
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal


        def component(self):
            
            if(self.c_inv_fix!=0):
                q_dh_in = solph.Flow(variable_costs=0, investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, 
                                                                                                        existing=self.existing, ep_costs=self.annuity_var))
            else:
                q_dh_in = solph.Flow(variable_costs=0, investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, 
                                                                                                        existing=self.existing, ep_costs=self.annuity_var))
                
            q_dh_out = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=0)
            
            districtheat = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_dh_in},
                outputs={self.sector_out : q_dh_out},
                conversion_factors={self.sector_out : self.efficiency})
            
            return districtheat
        
        
#Purchasing Heat from the LSC
class LSM2heat_Investment():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            efficiency=0.999
            tariff=0
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", efficiency)
            
            rev_lsc = kwargs.get("rev_lsc", tariff)
            self.rev_lsc=np.multiply(rev_lsc, -1)
            self.c_consumer = kwargs.get("c_consumer", rev_lsc)
            
            
            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'LSC2Heat_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'LSC2Heat_' + self.position)
            color = 'dodgerblue'
            self.color = kwargs.get("color", color)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 30)
            self.c_inv_var = kwargs.get("c_inv_var", 0)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal


        def component(self):
            q_lsc2consumerIn = solph.Flow(variable_costs=self.rev_lsc, investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, 
                                                                                                        existing=self.existing, ep_costs=self.annuity_var))
            q_lsc2consumerOut = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.c_consumer)
            LSC2heat = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_lsc2consumerIn},
                outputs={self.sector_out : q_lsc2consumerOut},
                conversion_factors={self.sector_out : self.efficiency})
            return LSC2heat
        

class Wastecombustion_Investment():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.004, 0.004]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.125
            maxEmissions=1000000
            position='Consumer_1'
            H_waste=3.4
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Wastecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nCombustion_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='brown'
            self.color = kwargs.get("color", color)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
            #Factor if not all recovered energy can be used by the LSC
            self.usable_energy = kwargs.get("usable_energy", 1)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 10)
            self.c_inv_var = kwargs.get("c_inv_var", 5200)
            self.c_inv_fix = kwargs.get("c_inv_fix", 150000)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal
            

    def component(self):
            m_wasteCombustion = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.P_in*self.deltaT/self.H_waste, 
                                                                                                     existing=self.existing, ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
            q_wasteCombustionEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_wasteCombustionTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy})
                
            else:
                m_wastecombustionEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl, self.sector_emissions : m_wastecombustionEmissions},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy, self.sector_emissions : self.emissions})
            
            
            return wasteCombustion
        
        
class Wastecombustion_Investment_simple():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.004, 0.004]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.125
            maxEmissions=1000000
            position='Consumer_1'
            H_waste=3.4
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Wastecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nCombustion_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='brown'
            self.color = kwargs.get("color", color)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
            #Factor if not all recovered energy can be used by the LSC
            self.usable_energy = kwargs.get("usable_energy", 1)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 10)
            self.c_inv_var = kwargs.get("c_inv_var", 5200)
            self.c_inv_fix = kwargs.get("c_inv_fix", 150000)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal
            

    def component(self):
            m_wasteCombustion = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.P_in*self.deltaT/self.H_waste, 
                                                                                                     existing=self.existing, ep_costs=self.annuity_var))
            q_wasteCombustionEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_wasteCombustionTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy})
                
            else:
                m_wastecombustionEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                wasteCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : m_wasteCombustion},
                    outputs={self.sector_out[1] : q_wasteCombustionTh, self.sector_out[0] : q_wasteCombustionEl, self.sector_emissions : m_wastecombustionEmissions},
                    conversion_factors={self.sector_out[1] : self.conversion_factor[1]*self.H_waste*self.usable_energy, self.sector_out[0] : self.conversion_factor[0]*self.H_waste*self.usable_energy, self.sector_emissions : self.emissions})
            
            
            return wasteCombustion
        
        
class WasteStorage_Investment():
    
    def __init__(self, *args, **kwargs):
        rho_municipalwaste = 100
        costsIn = 0
        costsOut=0
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 1
        V_max = 0.24
        V_start = 0
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        disposal_periods=0
        position='Consumer_1'
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        #self.v_start=V_start
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'WasteStorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nStorage_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        color='mistyrose'
        self.color = kwargs.get("color", color)
        self.empty_end = kwargs.get("empty_end", False)
        self.t_start = kwargs.get("t_start", 0)
        
        self.timesteps = kwargs.get("timesteps", 8760)
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        
        #Extension investment decision
        self.existing = kwargs.get("existing", 0)
        self.lifespan = kwargs.get("lifespan", 10)
        self.c_inv_var = kwargs.get("c_inv_var", 10)
        #self.c_inv_fix = kwargs.get("c_inv_fix", 6200)
        self.c_inv_fix = kwargs.get("c_inv_fix", 0)
        self.wacc = kwargs.get("wacc", 0.03)
        self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
        self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
        
        self.annuity_var*=self.timesteps/self.timestepsTotal
        self.annuity_fix*=self.timesteps/self.timestepsTotal
        
        
    def component(self):
        #Energy Flows
        m_waste2storeWasteStoreIn = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.v_max*self.rho_municipalwaste*self.deltaT, 
                                                                                                         existing=self.existing, ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
        m_waste2storeWasteStoreOut = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste*self.deltaT, variable_costs=self.costs_out)
        
        
        if(self.disposal_periods==0):
             wastestore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
             return wastestore
        else:
        
            wastestore = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps, empty_end=self.empty_end, t_start=self.t_start)
        
            return wastestore
        
        
class WasteStorage_Investment_simple():
    
    def __init__(self, *args, **kwargs):
        rho_municipalwaste = 100
        costsIn = 0
        costsOut=0
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 1
        V_max = 0.24
        V_start = 0
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        disposal_periods=0
        position='Consumer_1'
        
        self.rho_municipalwaste = kwargs.get("rho", rho_municipalwaste)
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        #self.v_start=V_start
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'WasteStorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Waste\nStorage_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        color='mistyrose'
        self.color = kwargs.get("color", color)
        self.empty_end = kwargs.get("empty_end", False)
        self.t_start = kwargs.get("t_start", 0)
        
        self.timesteps = kwargs.get("timesteps", 8760)
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        
        #Extension investment decision
        self.existing = kwargs.get("existing", 0)
        self.lifespan = kwargs.get("lifespan", 10)
        self.c_inv_var = kwargs.get("c_inv_var", 10)
        #self.c_inv_fix = kwargs.get("c_inv_fix", 6200)
        self.c_inv_fix = kwargs.get("c_inv_fix", 0)
        self.wacc = kwargs.get("wacc", 0.03)
        self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
        self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
        
        self.annuity_var*=self.timesteps/self.timestepsTotal
        self.annuity_fix*=self.timesteps/self.timestepsTotal
        
        
    def component(self):
        #Energy Flows
        m_waste2storeWasteStoreIn = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.v_max*self.rho_municipalwaste*self.deltaT, 
                                                                                                         existing=self.existing, ep_costs=self.annuity_var))
        m_waste2storeWasteStoreOut = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste*self.deltaT, variable_costs=self.costs_out)
        
        
        if(self.disposal_periods==0):
             wastestore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
             return wastestore
        else:
        
            wastestore = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps, empty_end=self.empty_end, t_start=self.t_start)
        
            return wastestore
        

class Sludgecombustion_Investment():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.06, 0.06]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=50
            maxEmissions=1000000
            position='Consumer_1'
            
            #Sludge Parameters
            Ct_sludge = 9.2
            rho_sludge = 1800
            H_sludge = 10/3.6
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Sludgecombustion_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sludge\nCombustion_' + self.position)
            color='green'
            self.color = kwargs.get("color", color)
            
            #Factor if not all recovered energy can be used by the LSC
            self.usable_energy = kwargs.get("usable_energy", 1)
            

            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.H_sludge= kwargs.get("H_sludge", H_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 30)
            self.c_inv_var = kwargs.get("c_inv_var", 11000000)
            self.c_inv_fix = kwargs.get("c_inv_fix", 150000)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal

        def component(self):
            v_sewageSludgeComb = solph.Flow(variable_costs=self.costs_in*self.rho_sludge, investment=solph.Investment(minimum=0, maximum=self.P_in*self.deltaT/(self.H_sludge*self.rho_sludge), 
                                                                                                                      existing=self.existing, ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
            q_SludgeCombEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_SludgeCombTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])

            if(self.emissions==0 or self.sector_emissions==0):
            
                sludgeCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageSludgeComb},
                    outputs={self.sector_out[0] : q_SludgeCombEl, self.sector_out[1] : q_SludgeCombTh},
                    conversion_factors={self.sector_out[0] : self.H_sludge*self.rho_sludge*self.conversion_factor[0]*self.usable_energy, self.sector_out[1] : self.H_sludge*self.rho_sludge*self.conversion_factor[1]*self.usable_energy})
            else:
                m_sludgecombustionemissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                sludgeCombustion = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : v_sewageSludgeComb},
                    outputs={self.sector_out[0] : q_SludgeCombEl, self.sector_out[1] : q_SludgeCombTh, self.sector_emissions : m_sludgecombustionemissions},
                    conversion_factors={self.sector_out[0] : self.H_sludge*self.rho_sludge*self.conversion_factor[0]*self.usable_energy, self.sector_out[1] : self.H_sludge*self.rho_sludge*self.conversion_factor[1]*self.usable_energy, self.sector_emissions : self.emissions*self.usable_energy})
            
            
            
            return sludgeCombustion
        
        
class SludgeStorage_Investment():
    
    def __init__(self, *args, **kwargs):
        V_max = 1.5
        V_start = 0
        Q_max = 1.5
        costsIn = 0.01
        costsOut=0.01
        Eta_in = 1
        Eta_out = 1
        Eta_sb = 0.999
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        disposal_periods=0
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.q_max = kwargs.get("Q_max", Q_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Sludgestorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sludge\nStorage_' + self.position)
        self.timesteps = kwargs.get("timesteps", 8760)
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        self.disposal_periods=kwargs.get("disposal_periods", disposal_periods)
        self.balanced = kwargs.get("balanced", True)
        color='darkkhaki'
        self.color = kwargs.get("color", color)
        self.empty_end=kwargs.get("empty_end", False)
        self.t_start = kwargs.get("t_start", 0)
        
        #Extension investment decision
        self.existing = kwargs.get("existing", 0)
        self.lifespan = kwargs.get("lifespan", 33)
        self.c_inv_var = kwargs.get("c_inv_var", 750)
        self.c_inv_fix = kwargs.get("c_inv_fix", 6200)
        self.wacc = kwargs.get("wacc", 0.03)
        self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
        self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
        
        self.annuity_var*=self.timesteps/self.timestepsTotal
        self.annuity_fix*=self.timesteps/self.timestepsTotal
        
        
    def component(self):
        #Energy Flows
        v_sludgestoreIn = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.q_max*self.deltaT, 
                                                                                               existing=self.existing, ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
        v_sludgestoreOut = solph.Flow(min=0, max=1, nominal_value=self.q_max*self.deltaT, variable_costs=self.costs_out)
        
        if(self.disposal_periods==0):
            sludgeStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_sludgestoreIn},
                outputs={self.sector_out: v_sludgestoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
            
            return sludgeStorage
        
        else:
            sludgeStorage = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: v_sludgestoreIn},
                outputs={self.sector_out: v_sludgestoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps, empty_end=self.empty_end, t_start=self.t_start)
        
            return sludgeStorage    
        
        
class SewageTreatment_Investment():

        def __init__(self, *args, **kwargs):
            
            v_max = 1000000
            
            tempdif=3
            share_waterSewage=0.9
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000
            
            eta=0.95
            waterrecovery = 0.95
            
            Ct_sludge = 9.2
            rho_sludge = 1800
            
            el_loss = 0.5
            
            costsIn=0.04
            costsOut=[0, 0, 0]
            costsLoss = 0
            deltaT=1
            emissions=0.3
            maxEmissions=1000000000
            position='Consumer_1'
            
            self.tempdif = kwargs.get("tempdif", tempdif)
            self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.eta = kwargs.get("eta", eta)
            self.eta_water = kwargs.get("eta_water", waterrecovery)
            self.v_max = kwargs.get("V_max", v_max)
            self.losses = kwargs.get("losses", el_loss)
            
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_loss = kwargs.get("c_loss", costsLoss)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_in = kwargs.get("sector_in")
            self.sector_loss = kwargs.get("sector_loss")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Sewagetreatment_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sewage\nTreatment_' + self.position)
            color='black'
            self.color = kwargs.get("color", color)
            
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
        
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 20)
            self.c_inv_var = kwargs.get("c_inv_var", 70000)
            self.c_inv_fix = kwargs.get("c_inv_fix", 353000)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal
            
            if(self.emissions==0 or self.sector_emissions==0):
                self.limit=[self.v_max*self.deltaT, self.v_max*self.concentration_sludge/self.rho_sludge*self.deltaT, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater*self.deltaT]
                
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]
                
            else:
                self.limit=[self.v_max*self.deltaT, self.v_max*self.concentration_sludge/self.rho_sludge*self.deltaT, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater*self.deltaT, self.max_emissions*self.deltaT]
            
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, self.emissions]

        def component(self):
            v_sewageWaterTreat = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.v_max*self.deltaT, 
                                                                                                      existing=self.existing, ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
            q_elecWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.losses*self.deltaT, variable_costs=self.costs_loss)
            v_cleanWater = solph.Flow(min=0, max=1, nominal_value=self.limit[0], variable_costs=self.costs_out[0])
            v_sludgeSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[1], variable_costs=self.costs_out[1])
            q_heatSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[2], variable_costs=self.costs_out[2])

            if(self.emissions==0 or self.sector_emissions==0):
            
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            else:
                m_sewageTreatEmissions = solph.Flow(min=0, max=1, nominal_value=self.limit[3])
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat, self.sector_emissions : m_sewageTreatEmissions},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            
            return sewageTreatment
        
        
class SewageTreatment_Investment_simple():

        def __init__(self, *args, **kwargs):
            
            v_max = 1000000
            
            tempdif=3
            share_waterSewage=0.9
            
            self.thCapacityWater = 4.190/3600
            self.rhoWater = 1000
            
            eta=0.95
            waterrecovery = 0.95
            
            Ct_sludge = 9.2
            rho_sludge = 1800
            
            el_loss = 0.5
            
            costsIn=0.04
            costsOut=[0, 0, 0]
            costsLoss = 0
            deltaT=1
            emissions=0.3
            maxEmissions=1000000000
            position='Consumer_1'
            
            self.tempdif = kwargs.get("tempdif", tempdif)
            self.share_sewage = kwargs.get("share_sewage", share_waterSewage)
            self.concentration_sludge= kwargs.get("concentration_sludge", Ct_sludge)
            self.rho_sludge = kwargs.get("rho_sludge", rho_sludge)
            self.eta = kwargs.get("eta", eta)
            self.eta_water = kwargs.get("eta_water", waterrecovery)
            self.v_max = kwargs.get("V_max", v_max)
            self.losses = kwargs.get("losses", el_loss)
            
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_loss = kwargs.get("c_loss", costsLoss)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_in = kwargs.get("sector_in")
            self.sector_loss = kwargs.get("sector_loss")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Sewagetreatment_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Sewage\nTreatment_' + self.position)
            color='black'
            self.color = kwargs.get("color", color)
            
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
        
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 20)
            self.c_inv_var = kwargs.get("c_inv_var", 70000)
            self.c_inv_fix = kwargs.get("c_inv_fix", 353000)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal
            
            if(self.emissions==0 or self.sector_emissions==0):
                self.limit=[self.v_max*self.deltaT, self.v_max*self.concentration_sludge/self.rho_sludge*self.deltaT, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater*self.deltaT]
                
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater]
                
            else:
                self.limit=[self.v_max*self.deltaT, self.v_max*self.concentration_sludge/self.rho_sludge*self.deltaT, self.v_max*self.eta*self.thCapacityWater*self.tempdif*self.rhoWater*self.deltaT, self.max_emissions*self.deltaT]
            
                self.conversion_factor = [self.eta_water, self.concentration_sludge/self.rho_sludge, self.eta*self.thCapacityWater*self.tempdif*self.rhoWater, self.emissions]

        def component(self):
            v_sewageWaterTreat = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.v_max*self.deltaT, 
                                                                                                      existing=self.existing, ep_costs=self.annuity_var))
            q_elecWaterTreat = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.losses*self.deltaT, variable_costs=self.costs_loss)
            v_cleanWater = solph.Flow(min=0, max=1, nominal_value=self.limit[0], variable_costs=self.costs_out[0])
            v_sludgeSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[1], variable_costs=self.costs_out[1])
            q_heatSewageTreat = solph.Flow(min=0, max=1, nominal_value=self.limit[2], variable_costs=self.costs_out[2])

            if(self.emissions==0 or self.sector_emissions==0):
            
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            else:
                m_sewageTreatEmissions = solph.Flow(min=0, max=1, nominal_value=self.limit[3])
                sewageTreatment = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : v_sewageWaterTreat, self.sector_loss : q_elecWaterTreat},
                    outputs={self.sector_out[0] : v_cleanWater, self.sector_out[1] : v_sludgeSewageTreat, self.sector_out[2] : q_heatSewageTreat, self.sector_emissions : m_sewageTreatEmissions},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=self.conversion_factor, losses_factor=self.losses)
                
            
            return sewageTreatment
        
class GasCHP_Investment():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.01, 0.01]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0
            maxEmissions=1000000
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'GasCHP_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Gas\nCHP_' + self.position)
            color='blue'
            self.color = kwargs.get("color", color)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
        
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 16)
            self.c_inv_var = kwargs.get("c_inv_var", 1970)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)

            
            self.annuity_var*=self.timesteps/self.timestepsTotal

            
            

        def component(self):
            
            
            q_gasCHPGas = solph.Flow(variable_costs=self.costs_in, investment=solph.Investment(minimum=0, maximum=self.P_in*self.deltaT, 
                                                                                               existing=self.existing, ep_costs=self.annuity_var))
            q_gasCHPEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_gasCHPTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                gasCHP = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasCHPGas},
                    outputs={self.sector_out[0] : q_gasCHPEl, self.sector_out[1] : q_gasCHPTh},
                    conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1]})
                
            else:
                m_gasCHPEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                gasCHP = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasCHPGas},
                    outputs={self.sector_out[0] : q_gasCHPEl, self.sector_out[1] : q_gasCHPTh, self.sector_emissions : m_gasCHPEmissions},
                    conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1], self.sector_emissions : self.emissions})
            
            
            return gasCHP
        
        
class WasteBiogas_Investment():
    def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversion_factor=0.5
            share_biowaste=0.225
            eta=0.9
            costsIn=0.068
            costsOut=1.265
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.125
            maxEmissions=1000000
            position='Consumer_1'
            H_waste=3.4
            
            self.eta = kwargs.get("eta", eta)
            self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
            self.share_biowaste = kwargs.get("share_biowaste", share_biowaste)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'WasteBiogas_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Waste\nBiogas_' + self.position)
            self.H_waste = kwargs.get("H_waste", H_waste)
            color='magenta'
            self.color = kwargs.get("color", color)
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            
        
            #Extension investment decision
            self.existing = kwargs.get("existing", 0)
            self.lifespan = kwargs.get("lifespan", 10)
            self.c_inv_var = kwargs.get("c_inv_var", 4000)
            self.c_inv_fix = kwargs.get("c_inv_fix", 150000)
            self.wacc = kwargs.get("wacc", 0.03)
            self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            self.annuity_fix = economics.annuity(self.c_inv_fix, self.lifespan, self.wacc)
            
            self.annuity_var*=self.timesteps/self.timestepsTotal
            self.annuity_fix*=self.timesteps/self.timestepsTotal

    def component(self):
            m_wasteAd = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_waste*self.share_biowaste*self.eta), variable_costs=self.costs_in)
            q_wasteAdGas = solph.Flow(variable_costs=self.costs_out, investment=solph.Investment(minimum=0, maximum=self.P_out*self.deltaT,
                                                                                                 existing=self.existing, ep_costs=self.annuity_var, nonconvex=True, offset=self.annuity_fix))
            
            m_wasteADEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
            
            wasteAd = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : m_wasteAd},
                outputs={self.sector_out : q_wasteAdGas, self.sector_emissions: m_wasteADEmissions},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_waste*self.share_biowaste*self.eta, 
                                    self.sector_emissions : self.emissions})
            
            
            return wasteAd
        
        
class Sewage2Greywater_Investment():
    
    def __init__(self, *args, **kwargs):
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.sewagefactor= kwargs.get("sewagefactor", 0.95)
        self.waterdemand= kwargs.get("waterdemand", 1)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Sewage2Greywater_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Sewage2\Greywater\n' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='black'
        self.color = kwargs.get("color", color)
        self.share_greywater=kwargs.get("share_greywater", 0.5)
                  
        
        self.min_greywater=kwargs.get("min_greywater", 0)
        
        self.greywater_minvalue = np.multiply(self.waterdemand, self.min_greywater)
        
        #Extension investment decision
        self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
        self.greywatermax= kwargs.get("greywatermax", 0.15)
        self.existing = kwargs.get("existing", 0)
        self.lifespan = kwargs.get("lifespan", 15)
        self.c_inv_var = kwargs.get("c_inv_var", 520000)
        self.wacc = kwargs.get("wacc", 0.03)
        self.annuity_var = economics.annuity(self.c_inv_var, self.lifespan, self.wacc)
            
        self.annuity_var*=self.timesteps/self.timestepsTotal
        
        
    def component(self):
        v_sewage2greywater = solph.Flow(min=self.greywater_minvalue, max=self.waterdemand, nominal_value=self.sewagefactor*self.share_greywater, variable_costs=0)
        v_greywater = solph.Flow(variable_costs=0, investment=solph.Investment(minimum=0, maximum=self.greywatermax,
                                                                                                 existing=self.existing, ep_costs=self.annuity_var))
            
        sewage2greywater = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : v_sewage2greywater},
            outputs={self.sector_out : v_greywater},
            conversion_factors={self.sector_out : 1})
        
        return sewage2greywater
        
