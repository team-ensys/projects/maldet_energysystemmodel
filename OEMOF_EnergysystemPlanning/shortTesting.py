# -*- coding: utf-8 -*-
"""
Created on Wed May 25 09:00:23 2022

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'test_short'

#Filename
filename = "Input_Model_Hour.xlsx"

#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 3
timestepsTotal = 8784

#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    

# -------------- Additional parameters that are set for the simulation -------------- 

#_____________ Electricity _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzentgelte
c_grid = 0.062

#Abgabe
c_abgabe = 0.018

#Reduktion Netzentgelt Lokalbereich
red_local = 1-0.57

#Reduktion Netzentgelt Regionalbereich
red_regional = 1-0.28

#_____________ Heat _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_heat = 0.046


#_____________ Cooling _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_cool = 0.046


#---------------------- DEFINITION OF CONSUMERS ----------------------

#Consumer 1
consumer1 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_1')
consumer1.addElectricitySector()
consumer1.addHeatSector()
consumer1.addCoolingSector()
#consumer1.addSector('testsector')
#TESTSTESTE = consumer1.getSector('testsector')
consumer1.sector2system(my_energysystem)

#Consumer 2
consumer2 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_2')
consumer2.addElectricitySector()
consumer2.addHeatSector()
consumer2.addCoolingSector()
consumer2.sector2system(my_energysystem)


#Grid Purchase no combined metering
elgrid = el_comp.Grid(sector=consumer1.getElectricitySector(), minimum=0.0001)        
my_energysystem.add(elgrid.component())
consumer1.addComponent(elgrid)



#Grid Sink
elgridSink = el_comp.GridSink(sector=consumer2.getElectricitySector())        
my_energysystem.add(elgridSink.component())
consumer2.addComponent(elgridSink)



#Transmission with delay
transmission = lsc_comp.Delay_Transport(sector_in=consumer1.getElectricitySector(), sector_out=consumer2.getElectricitySector(), efficiency=1, costs_transmission=0, delay=1, timesteps=timesteps)
my_energysystem.add(transmission.component())
consumer1.addComponent(transmission)


#Grid Purchase no combined metering
elgrid2 = el_comp.Grid(sector=consumer1.getHeatSector(), minimum=0.0001, label='Grid_Maidenless')        
my_energysystem.add(elgrid2.component())
consumer1.addComponent(elgrid2)



#Grid Sink
elgridSink2 = el_comp.GridSink(sector=consumer2.getHeatSector(), label='Sink_Maidenless')        
my_energysystem.add(elgridSink2.component())
consumer2.addComponent(elgridSink2)



#Transmission with delay
transmission2 = lsc_comp.Delay_Transport(sector_in=consumer1.getHeatSector(), sector_out=consumer2.getHeatSector(), efficiency=1, costs_transmission=0, delay=1, timesteps=timesteps, label="Transmission_Maidenless")
my_energysystem.add(transmission2.component())
consumer1.addComponent(transmission2)


#---------------------- Solve Model ----------------------
messi = solph.Model(my_energysystem) 

if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/saka.lp'
        messi.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    messi.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, messi, scenario)


results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")

