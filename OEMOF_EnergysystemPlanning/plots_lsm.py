# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 14:06:23 2023

@author: Matthias Maldet
"""

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from matplotlib import cm
from matplotlib.ticker import LinearLocator
import plotly.offline as pyo


#Input parameters to change
resultdirectory = "results/RESULTS_LSM_PAPER/"
dataname = "plotdata.xlsx"

savedirectory=resultdirectory + 'plots/'




#Begin program here
file2read=resultdirectory + dataname



#----------------------------- Heatmap scenario 3 -----------------------------------------------------------

data=pd.read_excel(file2read, index_col=0, sheet_name="heatmap_scen3")
sns.set()
plt.figure(figsize=(6, 6), 
           dpi = 600)
ax = sns.heatmap(data, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, vmin=0, vmax=1300000)
ax.set_title('Trading', fontsize = 12)
plt.show()
fig=ax.get_figure()

fig.savefig(savedirectory + "heatmap_scen3.png") 


#----------------------------- Heatmap scenario 5 -----------------------------------------------------------

data=pd.read_excel(file2read, index_col=0, sheet_name="heatmap_scen5")
sns.set()
plt.figure(figsize=(6, 6), 
           dpi = 600)
ax = sns.heatmap(data, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, vmin=0, vmax=1300000)
ax.set_title('Trading, LSC1,4 no PV', fontsize = 12)
plt.show()
fig=ax.get_figure()
fig.set_dpi(300)
fig.set_size_inches(18.5, 10.5)
fig.savefig(savedirectory + "heatmap_scen5.png") 

#----------------------------- Heatmap scenario 6 -----------------------------------------------------------

data=pd.read_excel(file2read, index_col=0, sheet_name="heatmap_scen6")
sns.set()
plt.figure(figsize=(6, 6), 
           dpi = 600)
ax = sns.heatmap(data, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, vmin=0, vmax=1300000)
ax.set_title('Trading, LSM PV', fontsize = 12)
plt.show()
fig=ax.get_figure()
fig.set_dpi(300)
fig.set_size_inches(18.5, 10.5)
fig.savefig(savedirectory + "heatmap_scen6.png") 

#----------------------------- Heatmap scenario 8 -----------------------------------------------------------

data=pd.read_excel(file2read, index_col=0, sheet_name="heatmap_scen8")
sns.set()
plt.figure(figsize=(6, 6), 
           dpi = 600)
ax = sns.heatmap(data, linewidths=2, square=True, cmap='Blues', cbar_kws={'label': 'Traded electricity in kWh'}, vmin=0, vmax=1300000)
plt.show()
fig=ax.get_figure()
fig.savefig(savedirectory + "heatmap_scen8.png") 


#----------------------------- Loadcurves scenario 1,2,3 -----------------------------------------------------------
data=pd.read_excel(file2read, index_col=0, sheet_name="loadcurve_scen123")

cols = data.columns

fig, axs = plt.subplots(3, sharex=True)
fig.set_dpi(600)
fig.set_size_inches(18.5, 10.5)
#fig.suptitle('Electricity grid consumption', fontsize = 12)
axs[0].plot(data.index, np.sort(data[cols[0]].values)[::-1])
axs[0].set_title('No PV', fontsize = 10)
axs[0].xaxis.label.set_visible(False)
axs[1].plot(data.index, np.sort(data[cols[1]].values)[::-1], color='green')
axs[1].set_title('PV', fontsize = 10)
axs[1].set_ylabel('Electricity grid consumption in kWh')
axs[2].plot(data.index, np.sort(data[cols[2]].values)[::-1], color='red')
axs[2].set_title('Trading', fontsize = 10)
axs[2].set_xlabel('t in h')
fig.savefig(savedirectory + "electricity_loadcurve_123.png") 

fig, axs = plt.subplots(3, sharex=True)
fig.set_dpi(600)
fig.set_size_inches(18.5, 10.5)
#fig.suptitle('Electricity grid consumption', fontsize = 12)
axs[0].plot(data.index, np.sort(data[cols[3]].values)[::-1])
axs[0].set_title('No PV', fontsize = 10)
axs[0].xaxis.label.set_visible(False)
axs[1].plot(data.index, np.sort(data[cols[4]].values)[::-1], color='green')
axs[1].set_title('PV', fontsize = 10)
axs[1].set_ylabel('Emissions in kgCO2')
axs[2].plot(data.index, np.sort(data[cols[5]].values)[::-1], color='red')
axs[2].set_title('Trading', fontsize = 10)
axs[2].set_xlabel('t in h')
fig.savefig(savedirectory + "emission_loadcurve_123.png") 

#----------------------------- Loadcurves scenario 4,5,6 -----------------------------------------------------------
data=pd.read_excel(file2read, index_col=0, sheet_name="loadcurve_scen456")

cols = data.columns

fig, axs = plt.subplots(3, sharex=True)
fig.set_dpi(600)
fig.set_size_inches(18.5, 10.5)
#fig.suptitle('Electricity grid consumption', fontsize = 12)
axs[0].plot(data.index, np.sort(data[cols[0]].values)[::-1])
axs[0].set_title('Less PV', fontsize = 10)
axs[0].xaxis.label.set_visible(False)
axs[1].plot(data.index, np.sort(data[cols[1]].values)[::-1], color='green')
axs[1].set_title('Less PV trading', fontsize = 10)
axs[1].set_ylabel('Electricity grid consumption in kWh')
axs[2].plot(data.index, np.sort(data[cols[2]].values)[::-1], color='red')
axs[2].set_title('Less PV market', fontsize = 10)
axs[2].set_xlabel('t in h')
fig.savefig(savedirectory + "electricity_loadcurve_456.png") 

fig, axs = plt.subplots(3, sharex=True)
fig.set_dpi(600)
fig.set_size_inches(18.5, 10.5)
#fig.suptitle('Electricity grid consumption', fontsize = 12)
axs[0].plot(data.index, np.sort(data[cols[3]].values)[::-1])
axs[0].set_title('Less PV', fontsize = 10)
axs[0].xaxis.label.set_visible(False)
axs[1].plot(data.index, np.sort(data[cols[4]].values)[::-1], color='green')
axs[1].set_title('Less PV trading', fontsize = 10)
axs[1].set_ylabel('Emissions in kgCO2')
axs[2].plot(data.index, np.sort(data[cols[5]].values)[::-1], color='red')
axs[2].set_title('Less PV market', fontsize = 10)
axs[2].set_xlabel('t in h')
fig.savefig(savedirectory + "emission_loadcurve_456.png") 


#-----------------------------Barcharts 1 - 6 -----------------------------------------------------------

#Bar chart PV
data=pd.read_excel(file2read, index_col=0, sheet_name="PV_costs_123456")

cols = data.columns

fig = plt.figure()
fig.set_dpi(600)
#fig.set_size_inches(18.5, 10.5)
ax = fig.add_axes([0,0,1,1])
ax.set_ylabel('PV capacity in kWp')
x = data.index
y = data[cols[0]]
ax.bar(x,y)
plt.xticks(size = 7)
plt.savefig(savedirectory + "pv_123456.png") 
plt.show()

#Bar chart costs



fig = plt.figure()
fig.set_dpi(600)
ax = fig.add_axes([0,0,1,1])
ax.set_ylabel('Costs in k€')
x = data.index
y = [list(np.multiply(data[cols[1]].values, 1/1000)), list(np.multiply(data[cols[2]].values, 1/1000)), list(np.multiply(data[cols[3]].values, 1/1000))]
X = np.arange(4)
ax.bar(X - 0.25, y[0], color = 'b', width = 0.25)
ax.bar(X + 0.00, y[1], color = 'g', width = 0.25)
ax.bar(X + 0.25, y[2], color = 'r', width = 0.25)
ax.legend(labels=[cols[1], cols[2], cols[3]])
plt.xticks(X, x, size = 7)
fig.savefig(savedirectory + "costs_123456.png") 
plt.show()


#-----------------------------Circular economy plots -----------------------------------------------------------
data=pd.read_excel(file2read, index_col=0, sheet_name="circular_economy")

cols = data.columns

plt.figure(dpi=600)
plt.plot(data.index, np.multiply(data[cols[0]].values, 1/1000))
plt.plot(data.index, np.multiply(data[cols[1]].values, 1/1000))
plt.plot(data.index, np.multiply(data[cols[2]].values, 1/1000))
plt.plot(data.index, np.multiply(data[cols[3]].values, 1/1000))
plt.xlabel('Scenario')
plt.xticks(fontsize=8, rotation=90)
plt.gca().legend(cols)
plt.ylabel('Energy in MWh')
plt.savefig(savedirectory + "elec_circular.png")


fig, ax1 = plt.subplots()
fig.set_dpi(600)
plt.xticks(fontsize=8, rotation=0)
ax2 = ax1.twinx()
ax1.plot(data.index, np.multiply(data[cols[4]].values, 1/1000), 'k-')


ax2.plot(data.index, np.multiply(data[cols[5]].values, 1/1000), 'b-')
ax1.set_xlabel('Scenario')
ax1.set_ylabel('Emissions in tCO2', color='k')
ax1.set_ylim([0, 800])
ax2.set_ylabel('Costs in k€', color='b')
ax2.set_ylim([0, 2200])
plt.savefig(savedirectory + "CO2costs_circular.png")


fig, axs = plt.subplots(2, sharex=True)
fig.set_dpi(600)
fig.set_size_inches(18.5, 10.5)
#fig.suptitle('Electricity grid consumption', fontsize = 12)
axs[0].plot(data.index, np.multiply(data[cols[4]].values, 1/1000), 'k-')
axs[0].set_ylabel('Emissions in tCO2', color='k', fontsize=20)
axs[0].xaxis.label.set_visible(False)
axs[1].plot(data.index, np.multiply(data[cols[5]].values, 1/1000), 'b-')
axs[1].set_ylabel('Costs in k€', color='b', fontsize=20)
axs[1].set_xlabel('Waste treatment options', fontsize=20)
fig.savefig(savedirectory + "CO2costs_circular.png") 


data=pd.read_excel(file2read, index_col=0, sheet_name="circular_economy_invest")

cols = data.columns

"""
fig, ax1 = plt.subplots()
fig.set_dpi(600)
plt.xticks(fontsize=8, rotation=90)
ax2 = ax1.twinx()
ax1.plot(data.index, data[cols[0]], 'r-')
ax2.plot(data.index, data[cols[1]], 'g-')
ax1.set_xlabel('Scenario')
ax1.set_ylabel('Waste combustion in kW', color='r')
ax2.set_ylabel('Waste anaerobic digestion in kW', color='g')
plt.savefig(savedirectory + "technology_circular.png")
"""

fig, axs = plt.subplots(2, sharex=True)
fig.set_dpi(600)
fig.set_size_inches(18.5, 10.5)
#fig.suptitle('Electricity grid consumption', fontsize = 12)
axs[0].plot(data.index, data[cols[0]], 'r-')
axs[0].set_ylabel('Waste combustion in kW', color='r', fontsize=20)
axs[0].xaxis.label.set_visible(False)
axs[1].plot(data.index, data[cols[1]], 'g-')
axs[1].set_ylabel('Waste anaerobic digestion in kW', color='g', fontsize=20)
axs[1].set_xlabel('Waste treatment options', fontsize=20)
fig.savefig(savedirectory + "technology_circular.png") 


#-----------------------------Circular economy sankey -----------------------------------------------------------
data=pd.read_excel(file2read, index_col=0, sheet_name="sankey")

cols = data.columns

#Scenario 11
row=data.iloc[0]
value = list(row.values)
label = list(cols.values)


fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = [ label[0] + "\n" + str(round(value[0])) + 't', label[1]+ "\n" + str(round(value[1])) + 't', label[2]+ "\n" + str(round(value[2])) + 't', 
               label[3]+ "\n" + str(round(value[3])) + 't', label[4]+ "\n" + str(round(value[4])) + 't'],
      color = ["blue", "yellow", "brown", "red", "green"]
    ),
    link = dict(
      source = [0, 0, 0, 0],
      target = [1, 2, 3, 4],
      value = [value[1], value[2], value[3], value[4]],
  ))])


fig.update_layout(title_text="Waste incineration and reduction", font_size=10)
fig.show()
fig.write_image(savedirectory + "sankey_11wastedisp.png", scale=3)

#Scenario 12
row=data.iloc[1]
value = list(row.values)
label = list(cols.values)


fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = [ label[0] + "\n" + str(round(value[0])) + 't', label[1]+ "\n" + str(round(value[1])) + 't', label[2]+ "\n" + str(round(value[2])) + 't', 
               label[3]+ "\n" + str(round(value[3])) + 't', label[4]+ "\n" + str(round(value[4])) + 't'],
      color = ["blue", "yellow", "brown", "red", "green"]
    ),
    link = dict(
      source = [0, 0, 0, 0],
      target = [1, 2, 3, 4],
      value = [value[1], value[2], value[3], value[4]],
  ))])


fig.update_layout(title_text="Waste Anaerobic Digestion", font_size=10)
fig.show()
fig.write_image(savedirectory + "sankey_12wasteAD.png", scale=3)

#Scenario 16
row=data.iloc[2]
value = list(row.values)
label = list(cols.values)


fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = [ label[0] + "\n" + str(round(value[0])) + 't', label[1]+ "\n" + str(round(value[1])) + 't', label[2]+ "\n" + str(round(value[2])) + 't', 
               label[3]+ "\n" + str(round(value[3])) + 't', label[4]+ "\n" + str(round(value[4])) + 't'],
      color = ["blue", "yellow", "brown", "red", "green"]
    ),
    link = dict(
      source = [0, 0, 0, 0],
      target = [1, 2, 3, 4],
      value = [value[1], value[2], value[3], value[4]],
  ))])


fig.update_layout(title_text="Waste Portfolio Optimization", font_size=10)
fig.show()
fig.write_image(savedirectory + "sankey_16wastePortfolio.png", scale=3)


#Scenario 18
row=data.iloc[3]
value = list(row.values)
label = list(cols.values)


fig = go.Figure(data=[go.Sankey(
    node = dict(
      pad = 15,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = [ label[0] + "\n" + str(round(value[0])) + 't', label[1]+ "\n" + str(round(value[1])) + 't', label[2]+ "\n" + str(round(value[2])) + 't', 
               label[3]+ "\n" + str(round(value[3])) + 't', label[4]+ "\n" + str(round(value[4])) + 't'],
      color = ["blue", "yellow", "brown", "red", "green"]
    ),
    link = dict(
      source = [0, 0, 0, 0],
      target = [1, 2, 3, 4],
      value = [value[1], value[2], value[3], value[4]],
  ))])


fig.update_layout(title_text="Waste Portfolio Optimization min biowaste", font_size=10)
fig.show()
fig.write_image(savedirectory + "sankey_18wastePortfolioMinBio.png", scale=3)



#-----------------------------Policy KPI -----------------------------------------------------------
data=pd.read_excel(file2read, index_col=0, sheet_name="policy_KPI")

cols = data.columns


plt.figure(dpi=600)
plt.plot(data.index, np.multiply(data[cols[0]].values, 1/1000))
plt.plot(data.index, np.multiply(data[cols[1]].values, 1/1000))
#plt.plot(data.index, np.multiply(data[cols[2]].values, 1/1000))
plt.xlabel('Scenario')
plt.xticks(fontsize=8, rotation=90)
plt.gca().legend(cols)
plt.ylabel('Energy in MWh')
plt.savefig(savedirectory + "policy_KPIenergy.png")
plt.show()

plt.figure(dpi=600)
plt.plot(data.index, data[cols[3]].values)
plt.xlabel('Scenario')
plt.xticks(fontsize=8, rotation=90)
plt.ylabel('Inefficiency')
plt.savefig(savedirectory + "policy_KPIinefficiency.png")
plt.show()

"""
fig, ax1 = plt.subplots()
fig.set_dpi(600)
plt.xticks(fontsize=8, rotation=90)
ax2 = ax1.twinx()
ax1.plot(data.index, np.multiply(data[cols[4]].values, 1/1000), 'k-')
ax2.plot(data.index, np.multiply(data[cols[5]].values, 1/1000), 'b-')
ax1.set_xlabel('Scenario')
ax1.set_ylabel('Emissions in tCO2', color='k')
ax2.set_ylabel('Costs in k€', color='b')
plt.savefig(savedirectory + "policy_KPIcostemission.png")
"""

fig, axs = plt.subplots(2, sharex=True)
fig.set_dpi(600)
fig.set_size_inches(18.5, 10.5)
#fig.suptitle('Electricity grid consumption', fontsize = 12)
axs[0].plot(data.index, np.multiply(data[cols[4]].values, 1/1000), 'k-')
axs[0].set_ylabel('Emissions in tCO2', color='k', fontsize=20)
axs[0].xaxis.label.set_visible(False)
axs[1].plot(data.index, np.multiply(data[cols[5]].values, 1/1000), 'b-')
axs[1].set_ylabel('Costs in k€', color='b', fontsize=20)
axs[1].set_xlabel('Strategy/Policy', fontsize=20)
fig.savefig(savedirectory + "policy_KPIcostemission.png") 


#-----------------------------Policy Investment -----------------------------------------------------------
#Bar chart PV
data=pd.read_excel(file2read, index_col=0, sheet_name="policy_investment")

cols = data.columns



#Bar chart PV
fig = plt.figure()
fig.set_dpi(600)
ax = fig.add_axes([0,0,1,1])
ax.set_ylabel('PV capacity in kWp')
x = data.index
y = [data[cols[0]], data[cols[4]]]
X = np.arange(5)
ax.bar(X - 0.15, y[0], color = 'b', width = 0.30)
ax.bar(X + 0.15, y[1], color = 'r', width = 0.30)
ax.legend(labels=[cols[0], cols[4]])
plt.xticks(X, x, size = 8)
fig.savefig(savedirectory + "pv_policy.png") 
plt.show()

#Bar chart heat
fig = plt.figure()
fig.set_dpi(600)
ax = fig.add_axes([0,0,1,1])
ax.set_ylabel('Heat technology capacity in kW')
x = data.index
y = [data[cols[1]], data[cols[3]]]
X = np.arange(5)
ax.bar(X - 0.15, y[0], color = 'g', width = 0.30)
ax.bar(X + 0.15, y[1], color = 'y', width = 0.30)
ax.legend(labels=[cols[1], cols[3]])
plt.xticks(X, x, size = 8)
fig.savefig(savedirectory + "heat_policy.png") 
plt.show()

#Bar chart waste
fig = plt.figure()
fig.set_dpi(600)
ax = fig.add_axes([0,0,1,1])
ax.set_ylabel('Waste treatment technology capacity in kW')
x = data.index
y = [data[cols[5]], data[cols[6]]]
X = np.arange(5)
ax.bar(X - 0.15, y[0], color = '#8A2515', width = 0.30)
ax.bar(X + 0.15, y[1], color = '#378324', width = 0.30)
ax.legend(labels=[cols[5], cols[6]])
plt.xticks(X, x, size = 8)
fig.savefig(savedirectory + "waste_policy.png") 
plt.show()

#Bar char battery
fig = plt.figure()
fig.set_dpi(600)
ax = fig.add_axes([0,0,1,1])
ax.set_ylabel('Battery capacity in kWh')
x = data.index
y = data[cols[2]]
ax.bar(x,y, color="#3A5AB6")
plt.xticks(size = 8)
plt.savefig(savedirectory + "battery_policy.png") 
plt.show()


#-----------------------------3d Scenario Plot -----------------------------------------------------------

data=pd.read_excel(file2read, index_col=0, sheet_name="policy_3d")

cols = data.columns


# Make data.
x = data[cols[0]]
y = data[cols[1]]
z = data[cols[2]]


fig = plt.figure(figsize =(32, 18)) 
fig.set_dpi(600)
ax = plt.axes(projection ='3d')
my_cmap = plt.get_cmap('YlGnBu')
 
# Creating plot
ax.plot_trisurf(x, y, z,
                linewidth = 0.2,
                antialiased = True,
                cmap = my_cmap, alpha=0.25);
ax.scatter(x[0], y[0], z[0], marker="*", label=data.index.values[0], s=100, c="black")
ax.scatter(x[1], y[1], z[1], marker="o", label=data.index.values[1], s=100, c="red")
ax.scatter(x[2], y[2], z[2], marker="x", label=data.index.values[2], s=100, c="blue")
ax.scatter(x[3], y[3], z[3], marker="s", label=data.index.values[3], s=100, c="green")
ax.scatter(x[4], y[4], z[4], marker="<", label=data.index.values[4], s=100, c="cyan")
plt.legend(loc="upper right")

ax.set_xlabel('Total costs in k€', fontsize=14)
ax.set_ylabel('Total emissions in t', fontsize=14)
ax.set_zlabel('Grid consumption in MWh', fontsize=14)
#ax.view_init(20, 60)
plt.savefig(savedirectory + "policy3d_gridconsumption.png") 


# Make data.
x = data[cols[0]]
y = data[cols[1]]
z = data[cols[3]]

fig = plt.figure(figsize =(32, 18)) 
fig.set_dpi(600)
ax = plt.axes(projection ='3d')
my_cmap = plt.get_cmap('YlGnBu')
 
# Creating plot
ax.plot_trisurf(x, y, z,
                linewidth = 0.2,
                antialiased = True,
                cmap = my_cmap, alpha=0.25);
ax.scatter(x[0], y[0], z[0], marker="*", label=data.index.values[0], s=100, c="black")
ax.scatter(x[1], y[1], z[1], marker="o", label=data.index.values[1], s=100, c="red")
ax.scatter(x[2], y[2], z[2], marker="x", label=data.index.values[2], s=100, c="blue")
ax.scatter(x[3], y[3], z[3], marker="s", label=data.index.values[3], s=100, c="green")
ax.scatter(x[4], y[4], z[4], marker="<", label=data.index.values[4], s=100, c="cyan")
plt.legend(loc="upper right")

ax.set_xlabel('Total costs in k€', fontsize=14)
ax.set_ylabel('Total emissions in t', fontsize=14)
ax.set_zlabel('Non-local utilization', fontsize=14)
#ax.view_init(20, 60)
plt.savefig(savedirectory + "policy3d_inefficiency.png") 



#-----------------------------Radar plot -----------------------------------------------------------

data=pd.read_excel(file2read, index_col=0, sheet_name="policy_3d")

cols = data.columns

#data =data.drop(columns=["Inefficiency"])

#data["Inefficiency"]=data["Inefficiency"]*10

categories = ['Total costs in k€', 'Total emissions in t', 'Grid cons. in MWh', 'Non-local utilization']
categories = [*categories, categories[0]]

standard = data.iloc[0].values
low_emission = data.iloc[1].values
localutil = data.iloc[2].values
policy2040 = data.iloc[3].values
pol2040neutral = data.iloc[4].values

standard = [*standard, standard[0]]
low_emission = [*low_emission, low_emission[0]]
localutil = [*localutil, localutil[0]]
policy2040 = [*policy2040, policy2040[0]]
pol2040neutral = [*pol2040neutral, pol2040neutral[0]]

label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(standard))


fig = go.Figure(
    data=[
        go.Scatterpolar(r=standard, theta=categories, name=data.index[0]),
        go.Scatterpolar(r=low_emission, theta=categories, name=data.index[1]),
        go.Scatterpolar(r=localutil, theta=categories, name=data.index[2]),
        go.Scatterpolar(r=policy2040, theta=categories, name=data.index[3]),
        go.Scatterpolar(r=pol2040neutral, theta=categories, name=data.index[4]),
    ],
    layout=go.Layout(
        polar={'radialaxis': {'visible': True}},
        showlegend=True
    )
)

fig.update_layout(
    autosize=False,
    width=1000,
    height=800,)
fig.write_image(savedirectory + "radarmap.png", scale=5)






#-----------------------------Barcharts validation -----------------------------------------------------------

#Bar chart PV
data=pd.read_excel(file2read, index_col=0, sheet_name="validation")

cols = data.columns

fig = plt.figure()
fig.set_dpi(600)
#fig.set_size_inches(18.5, 10.5)
ax = fig.add_axes([0,0,1,1])
ax.set_ylabel('Deviation in %')
x = data.index
y = data[cols[0]]
ax.bar(x,y)
plt.xticks(size = 9)
plt.savefig(savedirectory + "pv_123456.png") 
plt.show()

