# -*- coding: utf-8 -*-
"""
Created on Tue Mar  7 07:49:07 2023

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc

import investment_components as investment_comp
import constraints_LSM as constraints_LSM
import sdg_methods as sdg_methods
import constraints_sdg as constraints_sdg

import pyomo.environ as pyo


# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'sdg_lsm_market_decrease'

#Filename
filename = "input_data_sdg.xlsx"

#Scenario message
scenariomessage="Test scenario UN SDG market"


#Timestep for Optimisation
deltaT = 1

#Timesteps Considered --> default clustering 348, default operational 8784
timesteps=8784
#timesteps_operational = 8784
timestepsTotal = timesteps


data=pd.ExcelFile(filename)

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 


#Business as usual values
em_bau=2438128.886
c_bau=1610666


#Incentives
watersewage=0
greywatercosts=520000-400000
districtheatAddition=0
heatpumpRed=400
wasteaddition=0
recyclered=0.37
CO2add=0.1



# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1

yearWeightFactor=timesteps/timestepsTotal


#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2


#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)

#Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)


#---------------------- DEFINITION OF CONSUMERS ----------------------
#LSC1 / LSM1
lsc = Consumer.Consumer(timesteps=timesteps, data=data, label='LSM')
lsc.addElectricitySector()
lsc.addHeatSector()
lsc.addDistrictheatingSector()
lsc.addWaterSector()
lsc.addWasteSector()
lsc.addEmissionSector()
lsc.sector2system(my_energysystem)

print("----------------Consumer Data loaded----------------")


# ---------------------------- Electricity Sector ------------------------------------------

#PV
pv_invest = investment_comp.Photovoltaic_Investment_simple(sector=lsc.getElectricitySector(), generation_timeseries=lsc.input_data['elGen'], 
                          label='PV_' + lsc.label, timesteps=timesteps, area_efficiency=1, area_roof=72140, 
                          area_factor=10, timestepsTotal=timestepsTotal)
my_energysystem.add(pv_invest.component())
lsc.addComponent(pv_invest)


#Electricity grid purchase
electricitygridpurchase = el_comp.GridEmissions(sector=lsc.getElectricitySector(), sector_emissions=lsc.getEmissionsSector(),
                                                costs_energy=lsc.input_data['electricityCosts'], costs_model=lsc.input_data['electricityCosts'], 
                                                label='Electricitygridpurchase_' + lsc.label, timesteps=timesteps, 
                                                costs_power=0, emissions=0.209, P=100000)
my_energysystem.add(electricitygridpurchase.component())
lsc.addComponent(electricitygridpurchase)


#Electricity feedin
electricitygridFeedin = el_comp.GridFeedin(sector=lsc.getElectricitySector(), revenues=lsc.input_data['electricityFeedin'], 
                                  label='Electricityfeedin_' + lsc.label, timesteps=timesteps, P=100000)
my_energysystem.add(electricitygridFeedin.component())
lsc.addComponent(electricitygridFeedin)  
    

#Electricity demand
electricitydemand = el_comp.Demand(sector=lsc.getElectricitySector(), 
                                       demand_timeseries=lsc.input_data['electricityDemand'], label='Electricitydemand_' + lsc.label)
my_energysystem.add(electricitydemand.component())
lsc.addComponent(electricitydemand)

#Battery
battery_invest = investment_comp.Battery_Investment_simple(sector_in=lsc.getElectricitySector(), label='Battery_' + lsc.label,
                          soc_max=7220, SOC_start=0, P_in=7220, P_out=7220, timestepsTotal=timestepsTotal, timesteps=timesteps)
my_energysystem.add(battery_invest.component())
lsc.addComponent(battery_invest)


#Additional Electricity demand for sensitivity
electricitydemandSensitivity = el_comp.Demand(sector=lsc.getElectricitySector(), 
                                       demand_timeseries=100, label='ElectricitydemandSensitivity_' + lsc.label)
my_energysystem.add(electricitydemandSensitivity.component())
lsc.addComponent(electricitydemandSensitivity)


    
# ---------------------------- Heat Sector ------------------------------------------

#Heat pump
heatpump_invest = investment_comp.Heatpump_Investment_simple(sector_in = lsc.getElectricitySector(), sector_out=lsc.getHeatSector(), 
                            conversion_timeseries=lsc.input_data['copHP'], timesteps=timesteps, P_in=5000, P_out=5000, 
                            label='Heatpump_' + lsc.label, timestepsTotal=timestepsTotal, c_inv_var=945-heatpumpRed)
my_energysystem.add(heatpump_invest.component())
lsc.addComponent(heatpump_invest)
    

#District heat investment
districtheat_invest = investment_comp.DistrictHeat_Investment_simple(sector_in = lsc.getDistrictheatingSector(), sector_out=lsc.getHeatSector(), 
                        timesteps=timesteps, P=5000, label='Districtheatinvest_' + lsc.label, timestepsTotal=timestepsTotal)
my_energysystem.add(districtheat_invest.component())
lsc.addComponent(districtheat_invest)


#External heat source
dhPurchase = heat_comp.GridEmissions(sector=lsc.getDistrictheatingSector(), sector_emissions=lsc.getEmissionsSector(), 
                                         P=5000, emissions=0.188, costs_model=lsc.input_data["heatCosts"][0]+districtheatAddition, costs=lsc.input_data["heatCosts"],
                                         label="Districtheatsource_" + lsc.label, color="royalblue", timerange=deltaT)
my_energysystem.add(dhPurchase.component())
lsc.addComponent(dhPurchase) 


#Heat demand
heatdemand = heat_comp.Demand(sector=lsc.getHeatSector(), demand_timeseries=lsc.input_data['heatDemand'], 
                                  label='Heatdemand_' + lsc.label)
my_energysystem.add(heatdemand.component())
lsc.addComponent(heatdemand)


#Exhaust heat
exhaustHeat = heat_comp.GridSink(sector=lsc.getDistrictheatingSector(), label='Exhaustheat_' + 
                                 lsc.label, P=100000, costs_model=0, color='slategrey')  
my_energysystem.add(exhaustHeat.component())
lsc.addComponent(exhaustHeat)

   

# ---------------------------- Water Sector ------------------------------------------

#Water pipeline purchase
pipelinepurchase = water_comp.PipelinePurchaseLimited(timesteps=timesteps, sector=lsc.getPotablewaterSector(), 
                                                      label="PipelinepurchaseLimited_" + lsc.label, color='purple',
                                                      demand=lsc.input_data['waterDemand'], limit=1, 
                                                      costs_water=lsc.input_data['waterPipelineCosts'], discount=0)
my_energysystem.add(pipelinepurchase.component())
lsc.addComponent(pipelinepurchase)


#Water demand
waterdemand = water_comp.Waterdemand(sector_in=lsc.getPotablewaterSector(), sector_out=lsc.getSewageSector(), 
                                         demand=lsc.input_data['waterDemand'], timesteps=timesteps,
                                         label="Waterdemand_" + lsc.label, color='yellow')
my_energysystem.add(waterdemand.component())
lsc.addComponent(waterdemand)


#Water reduction    
waterReduction = lsc_comp.WaterFlexibilitySimple(timesteps=timesteps, sector_in=lsc.getPotablewaterSector(), label="Waterreduction_" + lsc.label,
                                                       color='darkcyan', demand=lsc.input_data['waterDemand'], wff=0,
                                                       revenues_consumer=0)
my_energysystem.add(waterReduction.component())
lsc.addComponent(waterReduction)


#Recovered water purchase
lscwaterpurchase = lsc_comp.LSCWaterPurchase(timesteps=timesteps, sector_in=lsc.getWaterdemandSector(), sector_out=lsc.getPotablewaterSector(), 
                                                           label="Recoveredwater_" +  lsc.label, color='brown',
                                                           demand=lsc.input_data['waterDemand'], limit=1, 
                                                           costs_consumer=1.5, discount=0.9, revenues_lsc=0,
                                                           efficiency=1)
my_energysystem.add(lscwaterpurchase.component())
lsc.addComponent(lscwaterpurchase)


# ---------------------------- Sewage Sector ------------------------------------------

#Sewage treatment plant
sewageTreatmentPlant_invest = investment_comp.SewageTreatment_Investment_simple(sector_in=lsc.getSewageSector(), sector_loss=lsc.getElectricitySector(),
                                              sector_out=[lsc.getWaterdemandSector(), lsc.getSludgeSector(), lsc.getHeatSector()],
                                              sector_emissions=lsc.getEmissionsSector(), emissions=0.3,
                                              tempdif=0, label='SewageTreatment_' + lsc.label, color='black', c_in=0.04+watersewage, eta_water=0.5, timestepsTotal=timestepsTotal, timesteps=timesteps)
my_energysystem.add(sewageTreatmentPlant_invest.component())
lsc.addComponent(sewageTreatmentPlant_invest)


#Sludge to waste sector
sludge2Hub = lsc_comp.Sewage2Hub(sector_in=lsc.getSludgeSector(), sector_out=lsc.getWasteSector(), timesteps=timesteps, 
                                    label="Sludge2Hub_" + lsc.label, efficiency=1800)
my_energysystem.add(sludge2Hub.component())
lsc.addComponent(sludge2Hub)

# ---------------------------- Greywater Sector ------------------------------------------

#Sewage to greywater
sewage2greywater = investment_comp.Sewage2Greywater_Investment(waterdemand=lsc.input_data["waterDemand"], sector_in=lsc.getSewageSector(), sector_out=lsc.getGreywaterSector(),
                                                   label="Sewage2Greywater_" + lsc.label, share_greywater=0.5, min_greywater=0, greywatermax=100, c_inv_var=greywatercosts)
my_energysystem.add(sewage2greywater.component())
lsc.addComponent(sewage2greywater)
    
    
#Greywater coverage of water demand
greywater2water = water_comp.Greywater2water(waterdemand=lsc.input_data["waterDemand"], sector_in=lsc.getGreywaterSector(), sector_out=lsc.getPotablewaterSector(),
                                                   label="Greywater2water_" + lsc.label, max_greywater2water=0.5, greywatermax=100)
my_energysystem.add(greywater2water.component())
lsc.addComponent(greywater2water)


# ---------------------------- Waste Sector ------------------------------------------

#Accruing waste
wasteAccruing = waste_comp.Accruing(sector=lsc.getWasteSector(), waste_timeseries=lsc.input_data['waste'], 
                                        label='WasteAccruing_' + lsc.label, color="yellow")
my_energysystem.add(wasteAccruing.component())
lsc.addComponent(wasteAccruing)


#Waste incineration
wasteCombustion_invest = investment_comp.Wastecombustion_Investment_simple(sector_in=lsc.getWasteSector(), 
                                                                    sector_out=[lsc.getElectricitySector(), lsc.getDistrictheatingSector()], sector_emissions=lsc.getEmissionsSector(),
                                             c_in=wasteaddition, c_out=[0.04, 0.04], conversion_factor=[0.35, 0.4], P_in=272000, P_out=272000, emissions=0.125,
                                             usable_energy=0.5, label='Wastecombustion_' + lsc.label, color='lightsteelblue', timestepsTotal=timestepsTotal, timesteps=timesteps)
my_energysystem.add(wasteCombustion_invest.component())
lsc.addComponent(wasteCombustion_invest)


#Waste recycling
wasteRecycling = waste_comp.Wastedisposal(volume_max=1000000, sector=lsc.getWasteSector(), min_disposal=0, label="Wasterecycling_" + lsc.label,
                                          timesteps=timesteps, sector_emissions=lsc.getEmissionsSector(), emissions=0,
                                          max_emissions=1000000, costs=0.38-recyclered, color="red")
my_energysystem.add(wasteRecycling.component())
lsc.addComponent(wasteRecycling)



#Waste reduction
wasteReduction=waste_comp.WasteReduction(M_max=10000000, sector=lsc.getWasteSector(), label="Wastereduction_" + lsc.label, 
                                         timesteps=timesteps, color="tan", reduction_rate=0, min_reduction=0,
                                         wastereduction_costs=0, timeseries_waste=lsc.input_data['waste'])
my_energysystem.add(wasteReduction.component())
lsc.addComponent(wasteReduction)


#Waste storage
wasteStorage = waste_comp.WasteStorage(sector_in=lsc.getWasteSector(), sector_out=lsc.getWasteSector(), 
                                       volume_max=20000, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="Wastestorage_" + lsc.label, color='mistyrose', balanced=True, empty_end=True)
my_energysystem.add(wasteStorage.component())
lsc.addComponent(wasteStorage)
    


#Waste storage extension
wasteStorage_extension = investment_comp.WasteStorage_Investment_simple(sector_in=lsc.getWasteSector(), sector_out=lsc.getWasteSector(), 
                                       volume_max=60000, volume_start=0, disposal_periods=0, timesteps=timesteps,
                                       label="WastestorageExtension_" + lsc.label, color='chocolate', balanced=True, timestepsTotal=timestepsTotal)
my_energysystem.add(wasteStorage_extension.component())
lsc.addComponent(wasteStorage_extension)


# ---------------------------- Emission Sector ------------------------------------------

#Total emissions
emissions_sink = em_comp.Emissions(sector=lsc.getEmissionsSector(), costs=0.03+CO2add, label='Emissiontotal_' + lsc.label)
my_energysystem.add(emissions_sink.component())
lsc.addComponent(emissions_sink)



#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 




# ---------------------------- Additional constraints ------------------------------------------

#SDG constraints
#saka=constraints_sdg.lsm_sdg6(model=saka, label="SDG6rule_LSM", timesteps=timesteps, waterdemand=lsc.input_data["waterDemand"], limit=limitSDG6)
#saka=constraints_sdg.lsm_sdg7(model=saka, label="SDG7rule_LSM", timesteps=timesteps, ren_elec=0.8, ren_heat=0.33, biogene=0.887, limit=limitSDG7)
#saka=constraints_sdg.lsm_sdg12(model=saka, label="SDG12rule_LSM", timesteps=timesteps, waste=lsc.input_data["waste"], limit=limitSDG12)
#saka=constraints_sdg.lsm_sdg13(model=saka, label="SDG13rule_LSM", timesteps=timesteps, emissions=em_bau, limit=limitSDG13)

#del saka.dual
#saka.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT)
#saka.receive_duals()


print("----------------Model set up----------------")

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")

    
saka.solve(solver=solver, solve_kwargs={'tee': True})
meth.store_results(my_energysystem, saka, scenario)
#results = solph.processing.results(saka)
results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")



# ---------------------------- Save the results ------------------------------------------
#Investment capacities
results_investment=sdg_methods.get_investment_results_lsm(consumer=lsc, results=results)
filename_save = scenario + "/investment.txt"
sdg_methods.write_dict(datalist=[results_investment], consumerlist=["LSM"], filename=filename_save)



#Objective
filename_save = scenario + "/objective.txt"
meth.write_objective(scenario=scenario, scenariomessage=scenariomessage, objective=saka.objective(), filename=filename_save)


#SDG contribution
sdgcontribution = sdg_methods.get_sdg_lsm(results=results, model=saka, consumer=lsc, cost_old=c_bau, em_old=em_bau, renElgrid=0.8, renDhgrid=0.33, biogene=0.887)
filename_save = scenario + "/sdgcontribution.txt"
sdg_methods.write_dict(datalist=[sdgcontribution], consumerlist=["LSM"], filename=filename_save)

"""
#Dual variables
dualvar = sdg_methods.get_duals_struct_lsm(model=saka)
filename_save = scenario + "/dualvar.txt"
sdg_methods.write_dict(datalist=[dualvar], consumerlist=["LSM"], filename=filename_save)
"""

#Time series
meth.results_to_excel(consumers=[lsc], results=results, filename=scenario)


print("----------------Results written----------------")

