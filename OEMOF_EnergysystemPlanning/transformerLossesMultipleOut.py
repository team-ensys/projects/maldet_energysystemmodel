# -*- coding: utf-8 -*-
"""
Created on Wed Dec 22 06:12:56 2021

@author: Matthias Maldet
"""

from oemof.network import network
from pyomo.core.base.block import SimpleBlock
from pyomo.environ import Constraint
from pyomo.environ import Set



class TransformerLossesMultipleOut(network.Transformer):
    

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.losses= kwargs.get("losses_factor", 10)
        self.conversion= kwargs.get("conversion_factor", [10])
        self.sector_conversion = kwargs.get("conversion_sector")
        self.sector_losses = kwargs.get("losses_sector")
        self.index=0
        self.len_conversion = len(self.conversion)


    def constraint_group(self):
        return TransformerLossesMultipleOutBlock


class TransformerLossesMultipleOutBlock(SimpleBlock):
    

    CONSTRAINT_GROUP = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _create(self, group=None):
        """Creates the relation for the class:`OffsetTransformer`.
        Parameters
        ----------
        group : list
            List of oemof.solph.custom.OffsetTransformer objects for which
            the relation of inputs and outputs is created
            e.g. group = [ostf1, ostf2, ostf3, ...]. The components inside
            the list need to hold an attribute `coefficients` of type dict
            containing the conversion factors for all inputs to outputs.
        """
        if group is None:
            return None

        m = self.parent_block()
        
        in_flows = {m: [i for i in m.inputs.keys()] for m in group}
        out_flows = {m: [i for i in m.outputs.keys()] for m in group}
        

        
        
        valueLosses=[]
        valueConversion=[]
        
        #Hier Auswahlverfahren einbauen
        

        counter=0
        maxlen=0
        for n in group:
            
            if(n.len_conversion>maxlen):
                maxlen=n.len_conversion
            
            n.index=counter
            list2Check = list(in_flows.values())[counter]
            
            for val in list2Check:
                if(str(n.sector_conversion) in str(val)):
                    valueConversion.append(val)
                elif(str(n.sector_losses) in str(val)):
                    valueLosses.append(val)
                    
            
            counter+=1
            


        self.TRANSFORMERWITHLOSSES = Set(initialize=[n for n in group])
        self.indexset = Set(initialize=range(maxlen))


            


        def _relation_rule(block, n, t):
            expr=0
            #index=0
            #count=0
            #for key in list(in_flows.keys()):
                #if str(n) in str(key):
                    #index = count
                #count+=1
            #print(n.losses)
            expr -= n.losses*sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(in_flows.keys())[n.index]) in o.label and str(valueConversion[n.index]) in i.label)
            expr += sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(in_flows.keys())[n.index]) in o.label and str(valueLosses[n.index]) in i.label)

            return expr == 0



        self.relation = Constraint(
            self.TRANSFORMERWITHLOSSES, m.TIMESTEPS, rule=_relation_rule
        )
        
        
        def _relation1_rule(block, n, t, indi):
            expr=0

            if(indi>=len(n.conversion)):
                return Constraint.Skip
            else:
                expr -= n.conversion[indi]*sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(in_flows.keys())[n.index]) in o.label and str(valueConversion[n.index]) in i.label)
                expr += sum(m.flow[i, o, t] for (i, o) in m.FLOWS if  str(list(out_flows.keys())[n.index]) in i.label and str(list(out_flows.values())[n.index][indi]) in o.label)
                

                return expr == 0



        self.relation1 = Constraint(
            self.TRANSFORMERWITHLOSSES, m.TIMESTEPS, self.indexset, rule=_relation1_rule
        )

        
        