# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 13:50:12 2021

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'selected_topics_scenario'

#Filename
filename = "Input_Model_Hour.xlsx"

#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 24
timestepsTotal = 8784

#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


scenario = 'results/' + scenario

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    
#adding busses to the energy system which represent the energy sectors
#Some are represented as buses for modelling purposes, even if they are not own energy sectors



consumer1 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx')
consumer1.addAllSectors()
consumer1.sector2system(my_energysystem)

#___________ ELECTRICITY ____________________



print("----------------model set up----------------")

#___________ Solve Model ____________________

messi = solph.Model(my_energysystem) 




if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/messi.lp'
        messi.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    messi.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, messi, scenario)


results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")
    
#messi.solve(solver=solver, solve_kwargs={'tee': True})

#my_energysystem.results = solph.processing.results(messi)
    

consumer1.sortComponents()