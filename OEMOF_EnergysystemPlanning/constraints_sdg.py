# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 06:58:19 2023

@author: Matthias Maldet
"""

import pyomo.environ as po
import pyomo.core as pyomo
import numpy as np

def lsc_sdg6(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    waterdemand = kwargs.get("waterdemand", 0)
    
    waterdemand=np.sum(waterdemand)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.greywater = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Greywater2water_LSC" and str(flow[1]) == "LSC_Potablewatersector"])
        myBlock.waterreduction = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSC_Potablewatersector" and str(flow[1]) == "Waterreduction_LSC"])
        
        
        
        
        def sdg6rule(model):
            expr=0
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.greywater )) for t in range(timesteps))
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.waterreduction )) for t in range(timesteps))
            expr-=waterdemand*limit
            
            return expr>=0
            
            
        
        myBlock.sdg6rule = pyomo.Constraint(
            rule=sdg6rule,
            doc='LSC SDG6 rule')
        
        
        
        return modelIn       
    
    
def lsc_sdg7(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    ren_elec = kwargs.get("ren_elec", 0.8)
    ren_heat = kwargs.get("ren_heat", 0.33)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.pv = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="PV_LSC" and str(flow[1]) == "LSC_Electricitysector"])
        myBlock.elgrid = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Electricitygridpurchase_LSC" and str(flow[1]) == "LSC_Electricitysector"])
        myBlock.heatpump = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Heatpump_LSC" and str(flow[1]) == "LSC_Heatsector"])
        myBlock.dh = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Districtheatsource_LSC" and str(flow[1]) == "LSC_Districtheatingsector"])
        myBlock.elfeedin = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSC_Electricitysector" and str(flow[1]) == "Electricityfeedin_LSC"])

        
        
        
        def sdg7rule(model):
            expr=0
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.pv )) for t in range(timesteps)))*(1-limit)
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.elgrid )) for t in range(timesteps)))*(ren_elec-limit)
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.heatpump )) for t in range(timesteps)))*(1-limit)
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.dh )) for t in range(timesteps)))*(ren_heat-limit)
            expr-=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.elfeedin )) for t in range(timesteps)))
            
            return expr>=0
            
            
        
        myBlock.sdg7rule = pyomo.Constraint(
            rule=sdg7rule,
            doc='LSC SDG7 rule')
        
        
        
        return modelIn       
    
    
def lsc_sdg12(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    waste = kwargs.get("waste", 0)
    
    waste=np.sum(waste)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.recycling = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSC_Wastesector" and str(flow[1]) == "Wasterecycling_LSC"])
        myBlock.wastereduction = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSC_Wastesector" and str(flow[1]) == "Wastereduction_LSC"])
        
        
        
        
        def sdg12rule(model):
            expr=0
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.recycling )) for t in range(timesteps))
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.wastereduction )) for t in range(timesteps))
            expr-=waste*limit
            
            return expr>=0
            
            
        
        myBlock.sdg12rule = pyomo.Constraint(
            rule=sdg12rule,
            doc='LSC SDG12 rule')
        
        
        
        return modelIn       
    
    
def lsc_sdg13(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    emissions = kwargs.get("emissions", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.emissions = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSC_Emissionsector" and str(flow[1]) == "Emissiontotal_LSC"])
        
        
        
        
        def sdg13rule(model):
            expr=0
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.emissions )) for t in range(timesteps))
            expr+=emissions*(1-limit)
            
            return expr>=0
            
            
        
        myBlock.sdg13rule = pyomo.Constraint(
            rule=sdg13rule,
            doc='LSC SDG13 rule')
        
        
        
        return modelIn       
    
    
def lsm_sdg6(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    waterdemand = kwargs.get("waterdemand", 0)
    
    waterdemand=np.sum(waterdemand)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.greywater = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Greywater2water_LSM" and str(flow[1]) == "LSM_Potablewatersector"])
        myBlock.waterreduction = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSM_Potablewatersector" and str(flow[1]) == "Waterreduction_LSM"])
        myBlock.recoveredwater = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Recoveredwater_LSM" and str(flow[1]) == "LSM_Potablewatersector"])

        
        
        
        def sdg6rule(model):
            expr=0
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.greywater )) for t in range(timesteps))
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.waterreduction )) for t in range(timesteps))
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.recoveredwater )) for t in range(timesteps))
            expr-=waterdemand*limit
            
            return expr>=0
            
            
        
        myBlock.sdg6rule = pyomo.Constraint(
            rule=sdg6rule,
            doc='LSM SDG6 rule')
        
        return modelIn 
    
    
def lsm_sdg7(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    ren_elec = kwargs.get("ren_elec", 0.8)
    ren_heat = kwargs.get("ren_heat", 0.33)
    biogene = kwargs.get("biogene", 0.887)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.pv = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="PV_LSM" and str(flow[1]) == "LSM_Electricitysector"])
        myBlock.elgrid = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Electricitygridpurchase_LSM" and str(flow[1]) == "LSM_Electricitysector"])
        myBlock.heatpump = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Heatpump_LSM" and str(flow[1]) == "LSM_Heatsector"])
        myBlock.dh = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Districtheatsource_LSM" and str(flow[1]) == "LSM_Districtheatingsector"])
        myBlock.elfeedin = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSM_Electricitysector" and str(flow[1]) == "Electricityfeedin_LSM"])
        myBlock.exhaust = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSM_Districtheatingsector" and str(flow[1]) == "Exhaustheat_LSM"])
        myBlock.wastecombelec = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastecombustion_LSM" and str(flow[1]) == "LSM_Electricitysector"])
        myBlock.wastecombheat = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastecombustion_LSM" and str(flow[1]) == "LSM_Districtheatingsector"])

        
        
        
        def sdg7rule(model):
            expr=0
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.pv )) for t in range(timesteps)))*(1-limit)
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.elgrid )) for t in range(timesteps)))*(ren_elec-limit)
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.heatpump )) for t in range(timesteps)))*(1-limit)
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.dh )) for t in range(timesteps)))*(ren_heat-limit)
            expr-=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.elfeedin )) for t in range(timesteps)))
            expr-=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.exhaust )) for t in range(timesteps)))
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.wastecombelec )) for t in range(timesteps)))*(biogene-limit)
            expr+=(sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.wastecombheat )) for t in range(timesteps)))*(biogene-limit)
            
            return expr>=0
            
            
        
        myBlock.sdg7rule = pyomo.Constraint(
            rule=sdg7rule,
            doc='LSM SDG7 rule')
        
        
        
        return modelIn   
    
def lsm_sdg12(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    waste = kwargs.get("waste", 0)
    
    
    waste=np.sum(waste)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.recycling = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSM_Wastesector" and str(flow[1]) == "Wasterecycling_LSM"])
        myBlock.wastereduction = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSM_Wastesector" and str(flow[1]) == "Wastereduction_LSM"])
        myBlock.sludge = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sludge2Hub_LSM" and str(flow[1]) == "LSM_Wastesector"])

        
        
        
        def sdg12rule(model):
            expr=0
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.recycling )) for t in range(timesteps))
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.wastereduction )) for t in range(timesteps))
            expr-=waste*limit
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.sludge )) for t in range(timesteps))*limit
            
            return expr>=0
            
            
        
        myBlock.sdg12rule = pyomo.Constraint(
            rule=sdg12rule,
            doc='LSM SDG12 rule')
        
        
        
        return modelIn       
    
def lsm_sdg13(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    limit = kwargs.get("limit", 0)
    emissions = kwargs.get("emissions", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.emissions = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="LSM_Emissionsector" and str(flow[1]) == "Emissiontotal_LSM"])
        
        
        
        
        def sdg13rule(model):
            expr=0
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.emissions )) for t in range(timesteps))
            expr+=emissions*(1-limit)
            
            return expr>=0
            
            
        
        myBlock.sdg13rule = pyomo.Constraint(
            rule=sdg13rule,
            doc='LSM SDG13 rule')
        
        
        
        return modelIn    