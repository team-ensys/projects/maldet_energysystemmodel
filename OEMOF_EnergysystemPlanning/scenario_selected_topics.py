# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 13:50:12 2021

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'selected_topics_scenario_testBattery'

#Filename
filename = "Input_Model_Hour.xlsx"

#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 8784
timestepsTotal = 8784

#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


scenario = 'results/' + scenario

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    
#adding busses to the energy system which represent the energy sectors
#Some are represented as buses for modelling purposes, even if they are not own energy sectors



consumer1 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx')
consumer1.addAllSectors()
consumer1.sector2system(my_energysystem)

#___________ ELECTRICITY ____________________

#Grid
elgrid = el_comp.GridEmissions(sector=consumer1.getElectricitySector())        
my_energysystem.add(elgrid.component())
consumer1.addComponent(elgrid)

#Heatpump
heatpump = el_comp.Heatpump(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector(), 
                            conversion_timeseries=consumer1.input_data['copHP'], timesteps=timesteps) 
my_energysystem.add(heatpump.component())
consumer1.addComponent(heatpump)

#Battery
battery = el_comp.Battery(sector_in=consumer1.getElectricitySector())
my_energysystem.add(battery.component())
consumer1.addComponent(battery)

#Groundwaterwell
groundwaterwell = el_comp.Groundwaterwell(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getPotablewaterSector())
my_energysystem.add(groundwaterwell.component())
consumer1.addComponent(groundwaterwell)

#Electrolysis
electrolysis = el_comp.Electrolysis(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getHydrogenSector(), sector_loss=consumer1.getPotablewaterSector())
my_energysystem.add(electrolysis.component())
consumer1.addComponent(electrolysis)

#Photovoltaic
pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], timesteps=timesteps)
my_energysystem.add(pv.component())
consumer1.addComponent(pv)

#Demand
elecdemand = el_comp.Demand(sector=consumer1.getElectricitySector(), demand_timeseries=consumer1.input_data['electricityDemand'])
my_energysystem.add(elecdemand.component())
consumer1.addComponent(elecdemand)

#Grid Sink
#elgridSink = el_comp.GridSink(sector=consumer1.getElectricitySector())        
#my_energysystem.add(elgridSink.component())
#consumer1.addComponent(elgridSink)


#___________ HEAT ____________________

#Grid
dhgrid = heat_comp.GridEmissions(sector=consumer1.getHeatSector())        
my_energysystem.add(dhgrid.component())
consumer1.addComponent(dhgrid)

#Hot Water Boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getHotwaterSector(), sector_loss=consumer1.getPotablewaterSector())
my_energysystem.add(hotwaterBoiler.component())
consumer1.addComponent(hotwaterBoiler)

#Demand Heat
heatdemand = heat_comp.Demand(sector=consumer1.getHeatSector(), demand_timeseries=consumer1.input_data['heatDemand'])
my_energysystem.add(heatdemand.component())
consumer1.addComponent(heatdemand)

#Demand Hotwater
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer1.getHotwaterSector(), demand_timeseries=consumer1.input_data['hotwaterDemand'])
my_energysystem.add(hotwaterdemand.component())
consumer1.addComponent(hotwaterdemand)

#District Heat Grid Sink
#dhgridSink = heat_comp.GridSink(sector=consumer1.getHeatSector())        
#my_energysystem.add(dhgridSink.component())
#consumer1.addComponent(dhgridSink)


#___________ Waste ____________________

#Accruing
wasteAccruing = waste_comp.Accruing(sector=consumer1.getWasteSector(), waste_timeseries=consumer1.input_data['waste'])
my_energysystem.add(wasteAccruing.component())
consumer1.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer1.getWasteSector(), sector_out=consumer1.getWastedisposalSector(), disposal_periods=2, timesteps=timesteps)
my_energysystem.add(wasteStorage.component())
consumer1.addComponent(wasteStorage)

#Wastecombustion
wasteCombustion = waste_comp.Wastecombustion(sector_in=consumer1.getWastedisposalSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()])
my_energysystem.add(wasteCombustion.component())
consumer1.addComponent(wasteCombustion)

#Waste Disposal
#Emissions bei Disposal --> nur einrechnen wenn minimaler Kostenbetrieb geplant ist, und die Kosten nicht an einen "unbedeutenden" Drittanbieter verrrechnet werden
#wasteDisposal = waste_comp.Wastedisposal(sector=consumer1.getWastedisposalSector(), costs=consumer1.input_data['wasteCosts'])
wasteDisposal = waste_comp.Wastedisposal(sector=consumer1.getWastedisposalSector(), costs=consumer1.input_data['wasteCosts'])        
my_energysystem.add(wasteDisposal.component())
consumer1.addComponent(wasteDisposal)

#___________ Water ____________________

#Pipeline
waterpipeline=water_comp.Pipeline(sector=consumer1.getPotablewaterSector())
my_energysystem.add(waterpipeline.component())
consumer1.addComponent(waterpipeline)

#Water Demand
waterDemand = water_comp.Waterdemand(sector_in=consumer1.getPotablewaterSector(), sector_out=consumer1.getSewageSector(), demand=consumer1.input_data['waterDemand'], timesteps=timesteps)
my_energysystem.add(waterDemand.component())
consumer1.addComponent(waterDemand)

#Sewage Treatment Plant
sewageTreatmentPlant = water_comp.SewageTreatment(sector_in=consumer1.getSewageSector(), sector_loss=consumer1.getElectricitySector(), 
                                                  sector_out=[consumer1.getPotablewaterSector(), consumer1.getSludgeSector(), consumer1.getHeatSector()])
my_energysystem.add(sewageTreatmentPlant.component())
consumer1.addComponent(sewageTreatmentPlant)

#Sludge Storage
sludgeStorage = water_comp.SludgeStorage(sector_in=consumer1.getSludgeSector(), sector_out=consumer1.getSludgestorageSector(), timesteps=timesteps)
my_energysystem.add(sludgeStorage.component())
consumer1.addComponent(sludgeStorage)


#Sludge Combustion
sludgecombustion = water_comp.Sludgecombustion(sector_in=consumer1.getSludgestorageSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()])
my_energysystem.add(sludgecombustion.component())
consumer1.addComponent(sludgecombustion)

#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer1.getSewageSector(), costs=consumer1.input_data['sewageCosts'])        
my_energysystem.add(sewageDisposal.component())
consumer1.addComponent(sewageDisposal)

#Sludge Disposal
sludgeDisposal = water_comp.Sludgedisposal(sector=consumer1.getSludgestorageSector(), costs=consumer1.input_data['sludgeCosts'])        
my_energysystem.add(sludgeDisposal.component())
consumer1.addComponent(sludgeDisposal)

#___________ Hydrogen ____________________
#Hydrogen CHP
hydrogenCHP = hydro_comp.CHP(sector_in=consumer1.getHydrogenSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()])
my_energysystem.add(hydrogenCHP.component())
consumer1.addComponent(hydrogenCHP)

#Hydrogen Sink
hydrogenSink = hydro_comp.HydrogenSink(sector=consumer1.getHydrogenSector())        
my_energysystem.add(hydrogenSink.component())
consumer1.addComponent(hydrogenSink)



print("----------------model set up----------------")

#___________ Solve Model ____________________

messi = solph.Model(my_energysystem) 

messi = constraints.battery_chargeblock(battery=battery, model=messi)



if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/messi.lp'
        messi.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    messi.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, messi, scenario)


results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")
    
#messi.solve(solver=solver, solve_kwargs={'tee': True})

#my_energysystem.results = solph.processing.results(messi)
    

consumer1.sortComponents()



#___________ Plots ____________________
plots.create_folders(scenario, [consumer1])



#Electricity Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', color='dodgerblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity')


#Heat Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', color='red')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat')

#Waste Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', color='saddlebrown')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste')


#Water Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', color='aqua')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water')


plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', color='lime')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage')


plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', color='slategrey')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge')

#Hydrogen Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', color='limegreen')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen')


