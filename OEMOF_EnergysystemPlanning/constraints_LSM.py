# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 11:17:05 2022

@author: Matthias Maldet
"""

import pyomo.environ as po
import pyomo.core as pyomo
import numpy as np
import pyomo.kernel as pmo
from pyomo.environ import (Constraint)
from oemof.tools import economics

def lsm_districtheatinvest(*args, **kwargs):
    
    #Vehicle Objects
    lsc = kwargs.get("lsc", 0)
    label = kwargs.get("label", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    districtheat_sale = "DistrictheatinvestSale_" + lsc
    districtheat_buy = "DistrictheatinvestBuy_" + lsc

    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.sale = po.Set(initialize=[flow for flow in modelIn.InvestmentFlow.invest if str(flow[1])==districtheat_sale])
        myBlock.buy = po.Set(initialize=[flow for flow in modelIn.InvestmentFlow.invest if str(flow[1])==districtheat_buy])
        
        
        def districtheatinvest_rule(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.sale) == sum(modelIn.flow[i, o, t] for (i, o) in myBlock.buy))
        
        
        myBlock.districtheatinvest = pyomo.Constraint(
            myBlock.t,
            rule=districtheatinvest_rule,
            doc='District heat invest rule')
        
        
        
        return modelIn
    
    
    
    
def lsm_wastecombustion(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_1"])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_2"])
        
        """
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_3"])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_4"])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_5"])
        myBlock.input6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_6"])
        myBlock.input7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_7"])
        """
        
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_1"])
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_2"])
        
        """
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_3"])
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_4"])
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_5"])
        myBlock.output6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_6"])
        myBlock.output7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_7"])
        """
        
        def wastecombrule(model):
            expr=0
            #LSM1
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input1 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output1 )) for t in range(timesteps))
            
            #LSM2
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input2 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output2 )) for t in range(timesteps))
            
            """
            #LSM3
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input3 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output3 )) for t in range(timesteps))
            
            #LSM4
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input4 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output4 )) for t in range(timesteps))
            
            #LSM5
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input5 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output5 )) for t in range(timesteps))
            
            #LSM6
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input6 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output6 )) for t in range(timesteps))
            
            #LSM7
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input7 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output7 )) for t in range(timesteps))
            """
            
            return expr==0
            
            
        
        myBlock.wastecombrule = pyomo.Constraint(
            rule=wastecombrule,
            doc='Waste Combustion Rule')
        
        
        
        return modelIn
        
        
def lsm_sludgecombustion(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_1" and str(flow[1]) == "LSM_1_Sludgesector"])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_2" and str(flow[1]) == "LSM_2_Sludgesector"])
        
        """
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_3" and str(flow[1]) == "LSM_3_Sludgesector"])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_4" and str(flow[1]) == "LSM_4_Sludgesector"])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_5" and str(flow[1]) == "LSM_5_Sludgesector"])
        myBlock.input6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_6" and str(flow[1]) == "LSM_6_Sludgesector"])
        myBlock.input7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_7" and str(flow[1]) == "LSM_7_Sludgesector"])
        """
        
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Sludgecombustion_LSM_1"])
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Sludgecombustion_LSM_2"])
        
        """
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Sludgecombustion_LSM_3"])
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Sludgecombustion_LSM_4"])
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Sludgecombustion_LSM_5"])
        myBlock.output6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Sludgecombustion_LSM_6"])
        myBlock.output7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Sludgecombustion_LSM_7"])
        """
        
        def sludgecombrule(model):
            expr=0
            #LSM1
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input1 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output1 )) for t in range(timesteps))
            
            #LSM2
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input2 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output2 )) for t in range(timesteps))
            
            """
            #LSM3
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input3 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output3 )) for t in range(timesteps))
            
            #LSM4
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input4 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output4 )) for t in range(timesteps))
            
            #LSM5
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input5 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output5 )) for t in range(timesteps))
            
            #LSM6
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input6 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output6 )) for t in range(timesteps))
            
            #LSM7
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input7 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output7 )) for t in range(timesteps))
            """
            
            return expr==0
            
            
        
        myBlock.sludgecombrule = pyomo.Constraint(
            rule=sludgecombrule,
            doc='Sludge Combustion Rule')
        
        
        
        return modelIn        
        
def lsm_combustion(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    rho_sludge = kwargs.get("rho_sludge", 1800)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        
        #Waste inputs
        myBlock.input11 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_1"])
        myBlock.input21 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_2"])
        
        """
        myBlock.input31 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_3"])
        myBlock.input41 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_4"])
        myBlock.input51 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_5"])
        myBlock.input61 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_6"])
        myBlock.input71 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastestorage_LSM_7"])
        """
        
        #Sludge Inputs
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_1" and str(flow[1]) == "LSM_1_Sludgesector"])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_2" and str(flow[1]) == "LSM_2_Sludgesector"])
        
        """
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_3" and str(flow[1]) == "LSM_3_Sludgesector"])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_4" and str(flow[1]) == "LSM_4_Sludgesector"])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_5" and str(flow[1]) == "LSM_5_Sludgesector"])
        myBlock.input6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_6" and str(flow[1]) == "LSM_6_Sludgesector"])
        myBlock.input7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="SewageTreatment_LSM_7" and str(flow[1]) == "LSM_7_Sludgesector"])
        """
        
        #Outsputs to combustion
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_1"])
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_2"])
        
        """
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_3"])
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_4"])
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_5"])
        myBlock.output6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_6"])
        myBlock.output7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="Wastecombustion_LSM_7"])
        """
        
        def sludgecombrule(model):
            expr=0
            #LSM1
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input11 )) for t in range(timesteps))
            expr+=rho_sludge*sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input1 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output1 )) for t in range(timesteps))
            
            #LSM2
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input21 )) for t in range(timesteps))
            expr+=rho_sludge*sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input2 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output2 )) for t in range(timesteps))
            
            """
            #LSM3
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input31 )) for t in range(timesteps))
            expr+=rho_sludge*sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input3 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output3 )) for t in range(timesteps))
            
            #LSM4
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input41 )) for t in range(timesteps))
            expr+=rho_sludge*sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input4 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output4 )) for t in range(timesteps))
            
            #LSM5
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input51 )) for t in range(timesteps))
            expr+=rho_sludge*sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input5 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output5 )) for t in range(timesteps))
            
            #LSM6
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input61 )) for t in range(timesteps))
            expr+=rho_sludge*sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input6 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output6 )) for t in range(timesteps))
            
            #LSM7
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input71 )) for t in range(timesteps))
            expr+=rho_sludge*sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input7 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output7 )) for t in range(timesteps))
            """
            
            return expr==0
            
            
        
        myBlock.sludgecombrule = pyomo.Constraint(
            rule=sludgecombrule,
            doc='Combustion Rule')
        
        
        
        return modelIn            
    
def lsm_sewagetreatment(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        #Get the required Flows
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewage2Hub_LSC_1" ])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewage2Hub_LSC_2" ])
        
        #Comment here for base scenario
        
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewage2Hub_LSC_3" ])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewage2Hub_LSC_4" ])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewage2Hub_LSC_5" ])
        #myBlock.input6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewage2Hub_LSC_6" ])
        #myBlock.input7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewage2Hub_LSC_7" ])
        
        #end comment for base scenario
        
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="SewageTreatment_LSM_1" and str(flow[0])=="LSM_1_Sewagesector"])
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="SewageTreatment_LSM_2" and str(flow[0])=="LSM_2_Sewagesector"])
        
        #Comment here for base scenario
        
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="SewageTreatment_LSM_3" and str(flow[0])=="LSM_3_Sewagesector"])
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="SewageTreatment_LSM_4" and str(flow[0])=="LSM_4_Sewagesector"])
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="SewageTreatment_LSM_5" and str(flow[0])=="LSM_5_Sewagesector"])
        #myBlock.output6 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="SewageTreatment_LSM_6" and str(flow[0])=="LSM_6_Sewagesector"])
        #myBlock.output7 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])=="SewageTreatment_LSM_7" and str(flow[0])=="LSM_7_Sewagesector"])
        
        #end comment for base scenario
        
        def sewagetreatmentrule(model):
            expr=0
            #LSM1
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input1 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output1 )) for t in range(timesteps))
            
            #LSM2
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input2 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output2 )) for t in range(timesteps))
            
            #Comment here for base scenario
            
            #LSM3
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input3 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output3 )) for t in range(timesteps))
            
            #LSM4
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input4 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output4 )) for t in range(timesteps))
            
            #LSM5
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input5 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output5 )) for t in range(timesteps))
            
            """
            #LSM6
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input6 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output6 )) for t in range(timesteps))
            
            #LSM7
            expr+=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.input7 )) for t in range(timesteps))
            expr-=sum((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output7 )) for t in range(timesteps))
            """
            
            #End comment for base scenario
            
            return expr==0
            
            
        
        myBlock.sewagetreatmentrule = pyomo.Constraint(
            rule=sewagetreatmentrule,
            doc='Sewage Treatment Rule')
        
        
        
        return modelIn        
    
    
#Waste transport block
def lsm_wastetransportblock(*args, **kwargs):
    
    #Vehicle Objects
    
    #LSM mit _
    lsm = kwargs.get("lsm", 0)
    
    #LSM Liste mit _
    lsm_list = kwargs.get("lsm_list", 0)
    capacity = kwargs.get("capacity", 0)
    
    if(lsm in lsm_list):
        lsm_list.remove(lsm)
        
    lsm_listSplit=[]
    
        
    for lsmCheck in lsm_list:
        lsmSplit = lsmCheck.split('_')
        #LSM Liste ohne _
        lsm_listSplit.append(lsmSplit[0]+lsmSplit[1])
        
    lsmInter=lsm.split('_')
    
    #LSM ohne _
    lsm_split=lsmInter[0]+lsmInter[1]
        
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        
        #Set with Vehicles
            
        #Binary optimization variables
        #Electric Vehicle Binary
        myBlock.bin_lsc = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Waste Transport Block'
        )
            
      
        label = "TransportblockWaste_" + lsm
    
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        
        
        #Get the required Flows
        #LSM destination 1
        myBlock.output0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "Wastetransport" + lsm_listSplit[0] + "_" + lsm])
        myBlock.input0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[0] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output0) <= model.bin_lsc[t]*capacity)
         
        def inputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input0) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule0,
            doc='LSC Wastetransport Output Rule 0')
        
                
        myBlock.lscWasteInputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule0,
            doc='LSC Wastetransport Input Rule 0')
        
        #Comment here for base
        
        
        #LSM destination 2
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "Wastetransport" + lsm_listSplit[1] + "_" + lsm])
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[1] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output1) <= model.bin_lsc[t]*capacity)
         
        def inputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input1) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule1,
            doc='LSC Wastetransport Output Rule 1')
        
                
        myBlock.lscWasteInputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule1,
            doc='LSC Wastetransport Input Rule 1')
        
        
        #LSM destination 3
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "Wastetransport" + lsm_listSplit[2] + "_" + lsm])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[2] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output2) <= model.bin_lsc[t]*capacity)
         
        def inputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input2) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule2,
            doc='LSC Wastetransport Output Rule 2')
        
                
        myBlock.lscWasteInputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule2,
            doc='LSC Wastetransport Input Rule 2')
        
        
        #LSM destination 4
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "Wastetransport" + lsm_listSplit[3] + "_" + lsm])
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[3] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output3) <= model.bin_lsc[t]*capacity)
         
        def inputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input3) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule3,
            doc='LSC Wastetransport Output Rule 3')
        
                
        myBlock.lscWasteInputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule3,
            doc='LSC Wastetransport Input Rule 3')
        
        """
        #LSM destination 5
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "WastetransportEmissions" + lsm_listSplit[4] + "_" + lsm])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[4] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output4) <= model.bin_lsc[t]*capacity)
         
        def inputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input4) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule4,
            doc='LSC Wastetransport Output Rule 0')
        
                
        myBlock.lscWasteInputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule4,
            doc='LSC Wastetransport Input Rule 4')
        
        
        #LSM destination 6
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "WastetransportEmissions" + lsm_listSplit[5] + "_" + lsm])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[5] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output5) <= model.bin_lsc[t]*capacity)
         
        def inputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input5) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule5,
            doc='LSC Wastetransport Output Rule 5')
        
                
        myBlock.lscWasteInputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule5,
            doc='LSC Wastetransport Input Rule 5')
        
        """ 
        
        #end comment here for base    
        
        return modelIn 

#Waste transport block
def lsm_biowastetransportblock(*args, **kwargs):
    
    #Vehicle Objects
    
    #LSM mit _
    lsm = kwargs.get("lsm", 0)
    
    #LSM Liste mit _
    lsm_list = kwargs.get("lsm_list", 0)
    capacity = kwargs.get("capacity", 0)
    
    if(lsm in lsm_list):
        lsm_list.remove(lsm)
        
    lsm_listSplit=[]
    
        
    for lsmCheck in lsm_list:
        lsmSplit = lsmCheck.split('_')
        #LSM Liste ohne _
        lsm_listSplit.append(lsmSplit[0]+lsmSplit[1])
        
    lsmInter=lsm.split('_')
    
    #LSM ohne _
    lsm_split=lsmInter[0]+lsmInter[1]
        
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        
        #Set with Vehicles
            
        #Binary optimization variables
        #Electric Vehicle Binary
        myBlock.bin_lsc = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Waste Transport Block'
        )
            
      
        label = "TransportblockBioWaste_" + lsm
    
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        
        
        #Get the required Flows
        #LSM destination 1
        myBlock.output0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastebiosector" and str(flow[1])== "BioWastetransport" + lsm_listSplit[0] + "_" + lsm])
        myBlock.input0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="BioWastetransport" + lsm_split + "_" + lsm_list[0] and str(flow[1])== lsm + "_Wastebiosector"])
    
        
    
        def outputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output0) <= model.bin_lsc[t]*capacity)
         
        def inputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input0) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule0,
            doc='LSC Wastetransport Output Rule 0')
        
                
        myBlock.lscWasteInputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule0,
            doc='LSC Wastetransport Input Rule 0')
        
        #Comment here for base
        
        
        #LSM destination 2
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastebiosector" and str(flow[1])== "BioWastetransport" + lsm_listSplit[1] + "_" + lsm])
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="BioWastetransport" + lsm_split + "_" + lsm_list[1] and str(flow[1])== lsm + "_Wastebiosector"])
    
        
    
        def outputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output1) <= model.bin_lsc[t]*capacity)
         
        def inputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input1) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule1,
            doc='LSC Wastetransport Output Rule 1')
        
                
        myBlock.lscWasteInputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule1,
            doc='LSC Wastetransport Input Rule 1')
        
        
        #LSM destination 3
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastebiosector" and str(flow[1])== "BioWastetransport" + lsm_listSplit[2] + "_" + lsm])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="BioWastetransport" + lsm_split + "_" + lsm_list[2] and str(flow[1])== lsm + "_Wastebiosector"])
    
        
    
        def outputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output2) <= model.bin_lsc[t]*capacity)
         
        def inputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input2) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule2,
            doc='LSC Wastetransport Output Rule 2')
        
                
        myBlock.lscWasteInputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule2,
            doc='LSC Wastetransport Input Rule 2')
        
        
        #LSM destination 4
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastebiosector" and str(flow[1])== "BioWastetransport" + lsm_listSplit[3] + "_" + lsm])
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="BioWastetransport" + lsm_split + "_" + lsm_list[3] and str(flow[1])== lsm + "_Wastebiosector"])
    
        
    
        def outputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output3) <= model.bin_lsc[t]*capacity)
         
        def inputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input3) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule3,
            doc='LSC Wastetransport Output Rule 3')
        
                
        myBlock.lscWasteInputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule3,
            doc='LSC Wastetransport Input Rule 3')
        
        """
        #LSM destination 5
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "WastetransportEmissions" + lsm_listSplit[4] + "_" + lsm])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[4] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output4) <= model.bin_lsc[t]*capacity)
         
        def inputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input4) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule4,
            doc='LSC Wastetransport Output Rule 0')
        
                
        myBlock.lscWasteInputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule4,
            doc='LSC Wastetransport Input Rule 4')
        
        
        #LSM destination 6
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Wastesector" and str(flow[1])== "WastetransportEmissions" + lsm_listSplit[5] + "_" + lsm])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Wastetransport" + lsm_split + "_" + lsm_list[5] and str(flow[1])== lsm + "_Wastesector"])
    
        
    
        def outputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output5) <= model.bin_lsc[t]*capacity)
         
        def inputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input5) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscWasteOutputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule5,
            doc='LSC Wastetransport Output Rule 5')
        
                
        myBlock.lscWasteInputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule5,
            doc='LSC Wastetransport Input Rule 5')
        
        """ 
        
        #end comment here for base    
        
        return modelIn     

#Waste transport block
def lsm_sludgetransportblock(*args, **kwargs):
    
    #Vehicle Objects
    
    #LSM mit _
    lsm = kwargs.get("lsm", 0)
    
    #LSM Liste mit _
    lsm_list = kwargs.get("lsm_list", 0)
    capacity = kwargs.get("capacity", 0)
    
    if(lsm in lsm_list):
        lsm_list.remove(lsm)
        
    lsm_listSplit=[]
    
        
    for lsmCheck in lsm_list:
        lsmSplit = lsmCheck.split('_')
        #LSM Liste ohne _
        lsm_listSplit.append(lsmSplit[0]+lsmSplit[1])
        
    lsmInter=lsm.split('_')
    
    #LSM ohne _
    lsm_split=lsmInter[0]+lsmInter[1]
        
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        
        #Set with Vehicles
            
        #Binary optimization variables
        #Electric Vehicle Binary
        myBlock.bin_lsc = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Sludge Transport Block'
        )
            
      
        label = "TransportblockSludge_" + lsm
    
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        
        
        #Get the required Flows
        #LSM destination 1
        myBlock.output0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sludgesector" and str(flow[1])== "SludgetransportEmissions" + lsm_listSplit[0] + "_" + lsm])
        myBlock.input0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sludgetransport" + lsm_split + "_" + lsm_list[0] and str(flow[1])== lsm + "_Sludgesector"])
    
        
    
        def outputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output0) <= model.bin_lsc[t]*capacity)
         
        def inputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input0) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSludgeOutputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule0,
            doc='LSC Sludgetransport Output Rule 0')
        
                
        myBlock.lscSludgeInputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule0,
            doc='LSC Sludgetransport Input Rule 0')
        
        """
        
        #LSM destination 2
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sludgesector" and str(flow[1])== "SludgetransportEmissions" + lsm_listSplit[1] + "_" + lsm])
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sludgetransport" + lsm_split + "_" + lsm_list[1] and str(flow[1])== lsm + "_Sludgesector"])
    
        
    
        def outputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output1) <= model.bin_lsc[t]*capacity)
         
        def inputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input1) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSludgeOutputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule1,
            doc='LSC Sludgetransport Output Rule 1')
        
                
        myBlock.lscSludgeInputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule1,
            doc='LSC Sludgetransport Input Rule 1')
        
        
        #LSM destination 3
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sludgesector" and str(flow[1])== "SludgetransportEmissions" + lsm_listSplit[2] + "_" + lsm])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sludgetransport" + lsm_split + "_" + lsm_list[2] and str(flow[1])== lsm + "_Sludgesector"])
    
        
    
        def outputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output2) <= model.bin_lsc[t]*capacity)
         
        def inputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input2) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSludgeOutputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule2,
            doc='LSC Sludgetransport Output Rule 2')
        
                
        myBlock.lscSludgeInputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule2,
            doc='LSC Sludgetransport Input Rule 2')
        
        
        #LSM destination 4
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sludgesector" and str(flow[1])== "SludgetransportEmissions" + lsm_listSplit[3] + "_" + lsm])
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sludgetransport" + lsm_split + "_" + lsm_list[3] and str(flow[1])== lsm + "_Sludgesector"])
    
        
    
        def outputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output3) <= model.bin_lsc[t]*capacity)
         
        def inputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input3) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSludgeOutputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule3,
            doc='LSC Sludgetransport Output Rule 3')
        
                
        myBlock.lscSludgeInputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule3,
            doc='LSC Sludgetransport Input Rule 3')
        
        #LSM destination 5
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sludgesector" and str(flow[1])== "SludgetransportEmissions" + lsm_listSplit[4] + "_" + lsm])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sludgetransport" + lsm_split + "_" + lsm_list[4] and str(flow[1])== lsm + "_Sludgesector"])
    
        
    
        def outputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output4) <= model.bin_lsc[t]*capacity)
         
        def inputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input4) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSludgeOutputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule4,
            doc='LSC Sludgetransport Output Rule 0')
        
                
        myBlock.lscSludgeInputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule4,
            doc='LSC Sludgetransport Input Rule 4')
        
        
        #LSM destination 6
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sludgesector" and str(flow[1])== "SludgetransportEmissions" + lsm_listSplit[5] + "_" + lsm])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sludgetransport" + lsm_split + "_" + lsm_list[5] and str(flow[1])== lsm + "_Sludgesector"])
    
        
    
        def outputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output5) <= model.bin_lsc[t]*capacity)
         
        def inputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input5) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSludgeOutputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule5,
            doc='LSC Sludgetransport Output Rule 5')
        
                
        myBlock.lscSludgeInputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule5,
            doc='LSC Sludgetransport Input Rule 5')
        
         
        """
            
        
        return modelIn 
    
#Waste transport block
def lsm_sewagetransmissionblock_old(*args, **kwargs):
    
    #Vehicle Objects
    
    #LSM mit _
    lsm = kwargs.get("lsm", 0)
    
    #LSM Liste mit _
    lsm_list = kwargs.get("lsm_list", 0)
    capacity = kwargs.get("capacity", 0)
    
    if(lsm in lsm_list):
        lsm_list.remove(lsm)
        
    lsm_listSplit=[]
    
        
    for lsmCheck in lsm_list:
        lsmSplit = lsmCheck.split('_')
        #LSM Liste ohne _
        lsm_listSplit.append(lsmSplit[0]+lsmSplit[1])
        
    lsmInter=lsm.split('_')
    
    #LSM ohne _
    lsm_split=lsmInter[0]+lsmInter[1]
        
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        
        #Set with Vehicles
            
        #Binary optimization variables
        #Electric Vehicle Binary
        myBlock.bin_lsc = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Sewage Transmission Block'
        )
            
      
        label = "Sewagetransmissionblock_" + lsm
    
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        
        
        #Get the required Flows
        #LSM destination 1
        myBlock.output0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[0] + "_" + lsm])
        myBlock.input0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[0] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output0) <= model.bin_lsc[t]*capacity)
         
        def inputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input0) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule0,
            doc='LSC Sewagetransmission Output Rule 0')
        
                
        myBlock.lscSewageInputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule0,
            doc='LSC Sewagetransmission Input Rule 0')
        
        #Comment here for base
        
        #LSM destination 2
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[1] + "_" + lsm])
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[1] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output1) <= model.bin_lsc[t]*capacity)
         
        def inputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input1) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule1,
            doc='LSC Sewagetransmission Output Rule 1')
        
                
        myBlock.lscSewageInputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule1,
            doc='LSC Sewagetransmission Input Rule 1')
        
        
        #LSM destination 3
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[2] + "_" + lsm])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[2] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output2) <= model.bin_lsc[t]*capacity)
         
        def inputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input2) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule2,
            doc='LSC Sewagetransmission Output Rule 2')
        
                
        myBlock.lscSewageInputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule2,
            doc='LSC Sewagetransmission Input Rule 2')
        
        
        #LSM destination 4
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[3] + "_" + lsm])
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[3] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output3) <= model.bin_lsc[t]*capacity)
         
        def inputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input3) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule3,
            doc='LSC Sewagetransmission Output Rule 3')
        
                
        myBlock.lscSewageInputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule3,
            doc='LSC Sewagetransmission Input Rule 3')
        
        #LSM destination 5
        myBlock.output4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[4] + "_" + lsm])
        myBlock.input4 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[4] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output4) <= model.bin_lsc[t]*capacity)
         
        def inputrule4(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input4) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule4,
            doc='LSC Sewagetransmission Output Rule 0')
        
                
        myBlock.lscSewageInputrule4 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule4,
            doc='LSC Sewagetransmission Input Rule 4')
        
        
        #LSM destination 6
        myBlock.output5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[5] + "_" + lsm])
        myBlock.input5 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[5] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output5) <= model.bin_lsc[t]*capacity)
         
        def inputrule5(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input5) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule5,
            doc='LSC Sewagetransmission Output Rule 5')
        
                
        myBlock.lscSewageInputrule5 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule5,
            doc='LSC Sewagetransmission Input Rule 5')
        
         
        
        #End comment here for base    
        
        return modelIn 
 

def lsm_sewagetransmissionblock(*args, **kwargs):
    
    #Vehicle Objects
    
    #LSM mit _
    lsm = kwargs.get("lsm", 0)
    
    #LSM Liste mit _
    lsm_list = kwargs.get("lsm_list", 0)
    capacity = kwargs.get("capacity", 0)
    
    if(lsm in lsm_list):
        lsm_list.remove(lsm)
        
    lsm_listSplit=[]
    
        
    for lsmCheck in lsm_list:
        lsmSplit = lsmCheck.split('_')
        #LSM Liste ohne _
        lsm_listSplit.append(lsmSplit[0]+lsmSplit[1])
        
    lsmInter=lsm.split('_')
    
    #LSM ohne _
    lsm_split=lsmInter[0]+lsmInter[1]
        
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
    #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        
        #Set with Vehicles
            
        #Binary optimization variables
        #Electric Vehicle Binary
        myBlock.bin_lsc = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Sewage Transmission Block'
        )
            
      
        label = "Sewagetransmissionblock_" + lsm
    
        modelIn.add_component('MyBlock_' + label, myBlock)
        
        
        
        #Get the required Flows
        #LSM destination 1
        myBlock.output0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[0] + "_" + lsm])
        myBlock.input0 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[0] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output0) <= model.bin_lsc[t]*capacity)
         
        def inputrule0(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input0) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule0,
            doc='LSC Sewagetransmission Output Rule 0')
        
                
        myBlock.lscSewageInputrule0 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule0,
            doc='LSC Sewagetransmission Input Rule 0')
        
        #Comment here for base
        
        #LSM destination 2
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[1] + "_" + lsm])
        myBlock.input1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[1] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output1) <= model.bin_lsc[t]*capacity)
         
        def inputrule1(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input1) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule1,
            doc='LSC Sewagetransmission Output Rule 1')
        
                
        myBlock.lscSewageInputrule1 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule1,
            doc='LSC Sewagetransmission Input Rule 1')
        
        
        #LSM destination 3
        myBlock.output2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[2] + "_" + lsm])
        myBlock.input2 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[2] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output2) <= model.bin_lsc[t]*capacity)
         
        def inputrule2(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input2) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule2,
            doc='LSC Sewagetransmission Output Rule 2')
        
                
        myBlock.lscSewageInputrule2 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule2,
            doc='LSC Sewagetransmission Input Rule 2')
        
        
        #LSM destination 4
        myBlock.output3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])==lsm + "_Sewagesector" and str(flow[1])== "Sewagetransmission" + lsm_listSplit[3] + "_" + lsm])
        myBlock.input3 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[0])=="Sewagetransmission" + lsm_split + "_" + lsm_list[3] and str(flow[1])== lsm + "_Sewagesector"])
    
        
    
        def outputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.output3) <= model.bin_lsc[t]*capacity)
         
        def inputrule3(model, t):
            return(sum(modelIn.flow[i, o, t] for (i, o) in myBlock.input3) <= (1-model.bin_lsc[t])*capacity)


        myBlock.lscSewageOutputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=outputrule3,
            doc='LSC Sewagetransmission Output Rule 3')
        
                
        myBlock.lscSewageInputrule3 = pyomo.Constraint(
            myBlock.t,
            rule=inputrule3,
            doc='LSC Sewagetransmission Input Rule 3')
        
        
        #End comment here for base    
        
        return modelIn 
    
def lsm_greywaterinstallation(*args, **kwargs):
    
    label = kwargs.get("label", 0)
    timesteps = kwargs.get("timesteps", 8760)
    number_households = kwargs.get("number_households", 100)
    unit_capacity = kwargs.get("unit_capacity", 0.0125)
    lsc = kwargs.get("LSC", "LSC_1")
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        #integer decision rule
        myBlock.int_greywater = pyomo.Var(
            myBlock.t, 
            within=pyomo.NonNegativeIntegers,
            doc='Integer for Greywater Investment'
        )
        
        
        modelIn.add_component('MyBlock_' + label, myBlock)
        

        
        #Flow for Sewage to greywater
        
        label_system= "Sewage2Greywater_" + lsc
        label_sector = lsc + "_Sewagesector"
        
        myBlock.output1 = po.Set(initialize=[flow for flow in modelIn.flows if str(flow[1])==label_system and str(flow[0])==label_sector])
       
        
        #end comment for base scenario
        
        def limitationrule(model, t):

            return((sum(modelIn.flow[i, o , t] for (i, o) in myBlock.output1 )) <= model.int_greywater[t] * unit_capacity)
            
        myBlock.limitationrule = pyomo.Constraint(
            myBlock.t,
            rule=limitationrule,
            doc='Rule for greywater flow limitation')
       
        def integerrule(model, t):

            return(model.int_greywater[t] <= number_households)
            
        
        myBlock.integerrule = pyomo.Constraint(
            myBlock.t,
            rule=integerrule,
            doc='Rule for maximum investment')
        
        def intvarrule(model, t):
            if(t==0):
                return(Constraint.Skip)
            else:
                return(model.int_greywater[t] >= model.int_greywater[t-1])
            
        
        myBlock.integervarrule = pyomo.Constraint(
            myBlock.t,
            rule=intvarrule,
            doc='Rule for integer variable')
        
        
        
        return modelIn        
    
def add_objective_greywater(*args, **kwargs):
    lifespan = kwargs.get("lifespan", 15)
    capex = kwargs.get("capex", 6500)
    wacc = kwargs.get("wacc", 0.03)
    variable=kwargs.get("variable", 0)
    model=kwargs.get("model", 0)
    timesteps=kwargs.get("timesteps", 8784)
    timestepsTotal=kwargs.get("timestepsTotal", timesteps)
    
    annuity_var = economics.annuity(capex, lifespan, wacc)
    annuity_var=annuity_var*timesteps/timestepsTotal
    
    expr=0
    expr+=variable[timesteps-1]*annuity_var
    expr+=model.objective
    
    new_objective=po.Objective(expr=expr, sense=po.minimize)
    
    return new_objective
               