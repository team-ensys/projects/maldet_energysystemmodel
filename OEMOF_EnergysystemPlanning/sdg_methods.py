# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 17:38:39 2021

@author: Matthias Maldet
"""

import oemof.solph as solph
import json
import pandas as pd
from pathlib import Path
import consumer_lsc as consumer_lsc
import math
import numpy as np


def get_investment_results_lsc(*args, **kwargs):
    consumer=kwargs.get("consumer", 0)
    results_investment=kwargs.get("results", 8784)
    
    H_waste=3.4
    
    #PV
    pv = consumer_lsc.get_component_by_name(components=consumer.components, name='PV_' + consumer.label)
    pv_power=solph.views.node(results_investment, pv.label).get("scalars")
    
    if pv_power is None:
        pv_power=0
    else:
        pv_power=pv_power[0]
        

    #Battery
    battery = consumer_lsc.get_component_by_name(components=consumer.components, name='Battery_' + consumer.label)
    battery_power=solph.views.node(results_investment, battery.label).get("scalars")
    
    if battery_power is None:
        battery_power=0
    else:
        battery_power=battery_power[1]
        
        
    battery_socmax=battery_power
    
    
    #Heatpump
    heatpump = consumer_lsc.get_component_by_name(components=consumer.components, name='Heatpump_' + consumer.label)
    heatpump_power=solph.views.node(results_investment, heatpump.label).get("scalars")
    
    if heatpump_power is None:
        heatpump_power=0
    else:
        heatpump_power=heatpump_power[0]
 
        
        
    #District heat
    dh = consumer_lsc.get_component_by_name(components=consumer.components, name='Districtheatinvest_' + consumer.label)
    dh_power=solph.views.node(results_investment, dh.label).get("scalars")
    
    if dh_power is None:
        dh_power=0
    else:
        dh_power=dh_power[0]
        

    


    
    """
    #Waste combustion
    wastecomb = consumer_lsc.get_component_by_name(components=consumer.components, name='Wastecombustion_' + consumer.label)
    wastecomb_power=solph.views.node(results_investment, wastecomb.label).get("scalars")
    
    if wastecomb_power is None:
        wastecomb_power=0
    else:
        wastecomb_power=wastecomb_power[0]*H_waste
        
    if(wastecomb_power>0.00001):
        wastecomb_power=math.ceil(wastecomb_power)
    else:
        wastecomb_power=0
    """
    
    #Waste storage
    wastestore = consumer_lsc.get_component_by_name(components=consumer.components, name='WastestorageExtension_' + consumer.label)
    wastestore_soc=solph.views.node(results_investment, wastestore.label).get("scalars")
    
    if wastestore_soc is None:
        wastestore_soc=0
    else:
        wastestore_soc=wastestore_soc[0]
        

    """
    #Sewage Treatment
    sewageTreat = consumer_lsc.get_component_by_name(components=consumer.components, name='SewageTreatment_' + consumer.label)
    sewageTreat_volume=solph.views.node(results_investment, sewageTreat.label).get("scalars")
    
    if sewageTreat_volume is None:
        sewageTreat_volume=0
    else:
        sewageTreat_volume=sewageTreat_volume[0]
        
    if(sewageTreat_volume>0.00001):
        sewageTreat_volume=math.ceil(sewageTreat_volume)
    else:
        sewageTreat_volume=0
    """
    
    
    #Greywater
    greywater = consumer_lsc.get_component_by_name(components=consumer.components, name='Sewage2Greywater_' + consumer.label)
    greywater=solph.views.node(results_investment, greywater.label).get("scalars")
    
    if greywater is None:
        greywater=0
    else:
        greywater=greywater[0]
        

    
    investment_capacities={

        "P_pv" : pv_power,
        "SOC_battery" : battery_socmax,
        "P_heatpump" : heatpump_power,
        "P_dh" : dh_power,
        "SOC_wastestore" : wastestore_soc,
        "Greywatersystems" : greywater
        }
    
    return investment_capacities


def get_investment_results_lsm(*args, **kwargs):
    consumer=kwargs.get("consumer", 0)
    results_investment=kwargs.get("results", 8784)
    
    H_waste=3.4
    
    #PV
    pv = consumer_lsc.get_component_by_name(components=consumer.components, name='PV_' + consumer.label)
    pv_power=solph.views.node(results_investment, pv.label).get("scalars")
    
    if pv_power is None:
        pv_power=0
    else:
        pv_power=pv_power[0]
        

    #Battery
    battery = consumer_lsc.get_component_by_name(components=consumer.components, name='Battery_' + consumer.label)
    battery_power=solph.views.node(results_investment, battery.label).get("scalars")
    
    if battery_power is None:
        battery_power=0
    else:
        battery_power=battery_power[1]
        
        
    battery_socmax=battery_power
    
    
    #Heatpump
    heatpump = consumer_lsc.get_component_by_name(components=consumer.components, name='Heatpump_' + consumer.label)
    heatpump_power=solph.views.node(results_investment, heatpump.label).get("scalars")
    
    if heatpump_power is None:
        heatpump_power=0
    else:
        heatpump_power=heatpump_power[0]
 
        
        
    #District heat
    dh = consumer_lsc.get_component_by_name(components=consumer.components, name='Districtheatinvest_' + consumer.label)
    dh_power=solph.views.node(results_investment, dh.label).get("scalars")
    
    if dh_power is None:
        dh_power=0
    else:
        dh_power=dh_power[0]
        

    


    
    
    #Waste combustion
    wastecomb = consumer_lsc.get_component_by_name(components=consumer.components, name='Wastecombustion_' + consumer.label)
    wastecomb_power=solph.views.node(results_investment, wastecomb.label).get("scalars")
    
    if wastecomb_power is None:
        wastecomb_power=0
    else:
        wastecomb_power=wastecomb_power[0]*H_waste
        
    
    
    
    #Waste storage
    wastestore = consumer_lsc.get_component_by_name(components=consumer.components, name='WastestorageExtension_' + consumer.label)
    wastestore_soc=solph.views.node(results_investment, wastestore.label).get("scalars")
    
    if wastestore_soc is None:
        wastestore_soc=0
    else:
        wastestore_soc=wastestore_soc[0]
        

    
    #Sewage Treatment
    sewageTreat = consumer_lsc.get_component_by_name(components=consumer.components, name='SewageTreatment_' + consumer.label)
    sewageTreat_volume=solph.views.node(results_investment, sewageTreat.label).get("scalars")
    
    if sewageTreat_volume is None:
        sewageTreat_volume=0
    else:
        sewageTreat_volume=sewageTreat_volume[0]
        

    
    
    
    #Greywater
    greywater = consumer_lsc.get_component_by_name(components=consumer.components, name='Sewage2Greywater_' + consumer.label)
    greywater=solph.views.node(results_investment, greywater.label).get("scalars")
    
    if greywater is None:
        greywater=0
    else:
        greywater=greywater[0]
        

    
    investment_capacities={

        "P_pv" : pv_power,
        "SOC_battery" : battery_socmax,
        "P_heatpump" : heatpump_power,
        "P_dh" : dh_power,
        "SOC_wastestore" : wastestore_soc,
        "Greywatersystems" : greywater,
        "P_wastecomb" : wastecomb_power,
        "V_sewagetreat" : sewageTreat_volume
        }
    
    return investment_capacities
        
def write_dict(*args, **kwargs):
    datalist = kwargs.get("datalist", 0)
    consumerlist = kwargs.get("consumerlist", 0)
    filename = kwargs.get("filename", 0)
    
    dicti={}
    index=0
    for data in datalist:
        dicti[consumerlist[index]] = data
        index+=1
        
    
    with open(filename, 'w') as file:
        file.write(json.dumps(dicti,  indent=1)) # use `json.loads` to do the reverse 
        
def write_investmentcosts(*args, **kwargs):
    datalist = kwargs.get("datalist", 0)
    consumerlist = kwargs.get("consumerlist", 0)
    filename = kwargs.get("filename", 0)
    
    dicti={}
    index=0
    
    totalInvest=0
    
    for data in datalist:
        
        data_new={}
        keys=data.keys()
        
        totalsum=0
        
        for key in keys:
            if('P_pv' in key):
                addval = data[key]*87.38041988
                if(data[key]>0): 
                    addval+=235.2549766

                data_new[key] = addval
                totalsum+=addval
            elif('P_battery' in key):
                addval = data[key]*117.2305066

                if(data[key]>0): 
                    addval+=105.5074559

                data_new[key] = addval
                totalsum+=addval
            elif('P_heatpump' in key):
                addval = data[key]*134.6212874

                if(data[key]>0): 
                    addval+=71.22819441

                data_new[key] = addval
                totalsum+=addval
            elif('P_districtheat' in key):
                addval = data[key]*33.6078538

                if(data[key]>0): 
                    addval+=23.52549766

                data_new[key] = addval
                totalsum+=addval
            elif('P_thermstore' in key):
                addval = data[key]*2.75584401
                if(data[key]>0): 
                    addval+=30.2470684
                data_new[key] = addval
                totalsum+=addval
            elif('P_wastecomb' in key):
                addval = data[key]*609.5986343/3.4
                if(data[key]>0): 
                    addval+=17584.57599

                data_new[key] = addval
                totalsum+=addval
            elif('SOC_wastestore' in key):
                addval = data[key]*1.172305066

                if(data[key]>0): 
                    addval+=0

                data_new[key] = addval
                totalsum+=addval
            elif('V_sewagetreat' in key):
                addval = data[key]*4705.099532

                if(data[key]>0): 
                    addval+=23727.14478

                data_new[key] = addval
                totalsum+=addval
            elif('Gas_CHP' in key):
                addval = data[key]*156.8333731

                if(data[key]>0): 
                    addval+=0
                data_new[key] = addval
                totalsum+=addval
            elif('AD' in key):
                addval = data[key]*468.9220264

                if(data[key]>0): 
                    addval+=17584.57599

                data_new[key] = addval
                totalsum+=addval
            elif('Greywater' in key):
                addval = data[key]*43558.62184


                data_new[key] = addval
                totalsum+=addval
            else:
                data_new[key]=0
         
        data_new["TotalInvestmentCosts"] = totalsum
            
        dicti[consumerlist[index]] = data_new
        index+=1
        
        totalInvest+=totalsum
        
    
    dicti["Total"] = totalInvest        
    
    with open(filename, 'w') as file:
        file.write(json.dumps(dicti,  indent=1)) # use `json.loads` to do the reverse 
        
def write_objective(*args, **kwargs):
    objective = kwargs.get("objective", 0)
    filename = kwargs.get("filename", 0)
    scenariomessage = kwargs.get("scenariomessage", 0)
    scenario = kwargs.get("scenario", 0)
    
    with open(filename, "w") as f:
        f.write(scenario)
        f.write("\n\n")
        f.write(scenariomessage)
        f.write("\n\n")
        f.write("Objective:\n %s \n" % (objective));
        
        
def get_component_by_name(*args, **kwargs):
    
    components = kwargs.get("components", 0)
    name = kwargs.get("name", 0)
    
    if(name==0 or components==0):
        return 0
    
    for component in components:
        if(name in component.label):
            return component
        
        
def get_sdg_lsc(*args, **kwargs):
    results=kwargs.get("results", 0)
    model=kwargs.get("model", 0)
    consumer=kwargs.get("consumer", 0)
    cost_old=kwargs.get("cost_old", 23766)
    em_old=kwargs.get("em_old", 19807.851)
    
    renElgrid=kwargs.get("renElgrid", 0.8)
    renDhgrid=kwargs.get("renDhgrid", 0.33)
    
    
    #SDG 1
    costs_new=model.objective()
    sdg1=(cost_old-costs_new)/cost_old
    
    
    #SDG 6
    greywater2water_component=get_component_by_name(components=consumer.components, name="Greywater2water_LSC")
    greywater2water=(solph.views.node(results, greywater2water_component.label).get("sequences"))
    greywater2water=np.sum(greywater2water[greywater2water.columns[0]].values)
    
    waterreduction_component=get_component_by_name(components=consumer.components, name="Waterreduction_LSC")
    waterreduction=(solph.views.node(results, waterreduction_component.label).get("sequences"))
    waterreduction=np.sum(waterreduction[waterreduction.columns[0]].values)
    
    waterdemand=np.sum(consumer.input_data['waterDemand'])
    
    sdg6 = (greywater2water+waterreduction)/waterdemand
    
    #SDG 7
    pv_component=get_component_by_name(components=consumer.components, name="PV_LSC")
    pv=np.sum(solph.views.node(results, pv_component.label).get("sequences").values)
    
    elgrid_component=get_component_by_name(components=consumer.components, name="Electricitygridpurchase_LSC")
    elgrid=(solph.views.node(results, elgrid_component.label).get("sequences"))
    elgrid=np.sum(elgrid[elgrid.columns[0]].values)
    
    heatpump_component=get_component_by_name(components=consumer.components, name="Heatpump_LSC")
    heatpump=(solph.views.node(results, heatpump_component.label).get("sequences"))
    heatpump=np.sum(heatpump[heatpump.columns[0]].values)
    
    dh_component=get_component_by_name(components=consumer.components, name="Districtheatsource_LSC")
    dh=(solph.views.node(results, dh_component.label).get("sequences"))
    dh=np.sum(dh[dh.columns[0]].values)
    
    feedin_component=get_component_by_name(components=consumer.components, name="Electricityfeedin_LSC")
    feedin=np.sum(solph.views.node(results, feedin_component.label).get("sequences").values)
    
    sdg7 = (pv-feedin+elgrid*renElgrid+heatpump+dh*renDhgrid)/(pv+elgrid+heatpump+dh)
    
    #SDG 12
    wasterecycle_component=get_component_by_name(components=consumer.components, name="Wasterecycling_LSC")
    wasterecycle=(solph.views.node(results, wasterecycle_component.label).get("sequences"))
    wasterecycle=np.sum(wasterecycle[wasterecycle.columns[0]].values)
    
    wastereduction_component=get_component_by_name(components=consumer.components, name="Wastereduction_LSC")
    wastereduction=(solph.views.node(results, wastereduction_component.label).get("sequences"))
    wastereduction=np.sum(wastereduction[wastereduction.columns[0]].values)
    
    waste=np.sum(consumer.input_data['waste'])
    
    sdg12=(wasterecycle+wastereduction)/waste
    
    
    #SDG 13
    emission_component=get_component_by_name(components=consumer.components, name="Emissiontotal_LSC")
    emission=(solph.views.node(results, emission_component.label).get("sequences"))
    emission=np.sum(emission[emission.columns[0]].values)
    
    sdg13=(em_old-emission)/em_old
    
    
    #SDG 11
    
    sumSDG=(sdg1 + sdg6 + sdg7 + sdg12 + sdg13)*100
    
    sdg1new = sdg1 * 100/sumSDG
    sdg6new = sdg6 * 100/sumSDG
    sdg7new = sdg7 * 100/sumSDG
    sdg12new = sdg12 * 100/sumSDG
    sdg13new = sdg13 * 100/sumSDG
    
    sdg11={
        "SDG1 to 11" : sdg1new,
        "SDG6 to 11" : sdg6new,
        "SDG7 to 11" : sdg7new,
        "SDG12 to 11" : sdg12new,
        "SDG13 to 11" : sdg13new,
        }
    
    
    #Total SDG Struct
    sdgTotal={
        "SDG1" : sdg1,
        "SDG6" : sdg6,
        "SDG7" : sdg7,
        "SDG12" : sdg12,
        "SDG13" : sdg13,
        "SDG11" : sdg11
        }
    
    return sdgTotal

def get_sdg_lsm(*args, **kwargs):
    results=kwargs.get("results", 0)
    model=kwargs.get("model", 0)
    consumer=kwargs.get("consumer", 0)
    cost_old=kwargs.get("cost_old", 1610666)
    em_old=kwargs.get("em_old", 2438128.886)
    
    renElgrid=kwargs.get("renElgrid", 0.8)
    renDhgrid=kwargs.get("renDhgrid", 0.33)
    biogene=kwargs.get("biogene", 0.887)
    
    
    #SDG 1
    costs_new=model.objective()
    sdg1=(cost_old-costs_new)/cost_old
    
    
    #SDG 6
    greywater2water_component=get_component_by_name(components=consumer.components, name="Greywater2water_LSM")
    greywater2water=(solph.views.node(results, greywater2water_component.label).get("sequences"))
    greywater2water=np.sum(greywater2water[greywater2water.columns[0]].values)
    
    waterreduction_component=get_component_by_name(components=consumer.components, name="Waterreduction_LSM")
    waterreduction=(solph.views.node(results, waterreduction_component.label).get("sequences"))
    waterreduction=np.sum(waterreduction[waterreduction.columns[0]].values)
    
    recoveredwater_component=get_component_by_name(components=consumer.components, name="Recoveredwater_LSM")
    recoveredwater=(solph.views.node(results, recoveredwater_component.label).get("sequences"))
    recoveredwater=np.sum(recoveredwater[recoveredwater.columns[0]].values)
    
    
    
    waterdemand=np.sum(consumer.input_data['waterDemand'])
    
    sdg6 = (greywater2water+waterreduction+recoveredwater)/waterdemand
    
    #SDG 7
    pv_component=get_component_by_name(components=consumer.components, name="PV_LSM")
    pv=np.sum(solph.views.node(results, pv_component.label).get("sequences").values)
    
    elgrid_component=get_component_by_name(components=consumer.components, name="Electricitygridpurchase_LSM")
    elgrid=(solph.views.node(results, elgrid_component.label).get("sequences"))
    elgrid=np.sum(elgrid[elgrid.columns[0]].values)
    
    heatpump_component=get_component_by_name(components=consumer.components, name="Heatpump_LSM")
    heatpump=(solph.views.node(results, heatpump_component.label).get("sequences"))
    heatpump=np.sum(heatpump[heatpump.columns[0]].values)
    
    dh_component=get_component_by_name(components=consumer.components, name="Districtheatsource_LSM")
    dh=(solph.views.node(results, dh_component.label).get("sequences"))
    dh=np.sum(dh[dh.columns[0]].values)
    
    feedin_component=get_component_by_name(components=consumer.components, name="Electricityfeedin_LSM")
    feedin=np.sum(solph.views.node(results, feedin_component.label).get("sequences").values)
    
    exhaust_component=get_component_by_name(components=consumer.components, name="Exhaustheat_LSM")
    exhaust=np.sum(solph.views.node(results, exhaust_component.label).get("sequences").values)
    
    wastecombelec_component=get_component_by_name(components=consumer.components, name="Wastecombustion_LSM")
    wastecombelec=(solph.views.node(results, wastecombelec_component.label).get("sequences"))
    wastecombelec=np.sum(wastecombelec[wastecombelec.columns[2]].values)
    
    
    wastecombheat_component=get_component_by_name(components=consumer.components, name="Wastecombustion_LSM")
    wastecombheat=(solph.views.node(results, wastecombheat_component.label).get("sequences"))
    wastecombheat=np.sum(wastecombheat[wastecombheat.columns[2]].values)
    
    #sdg7 = (pv+elgrid*renElgrid+heatpump+dh*renDhgrid - exhaust + biogene*wastecombelec + biogene * wastecombheat)/(pv+elgrid+heatpump+dh+wastecombelec +wastecombheat)
    sdg7 = (pv-feedin+elgrid*renElgrid+heatpump+dh*renDhgrid - exhaust + biogene*wastecombelec + biogene * wastecombheat)/(pv+elgrid+heatpump+dh+wastecombelec +wastecombheat)
    
    #SDG 12
    wasterecycle_component=get_component_by_name(components=consumer.components, name="Wasterecycling_LSM")
    wasterecycle=(solph.views.node(results, wasterecycle_component.label).get("sequences"))
    wasterecycle=np.sum(wasterecycle[wasterecycle.columns[0]].values)
    
    wastereduction_component=get_component_by_name(components=consumer.components, name="Wastereduction_LSM")
    wastereduction=(solph.views.node(results, wastereduction_component.label).get("sequences"))
    wastereduction=np.sum(wastereduction[wastereduction.columns[0]].values)
    
    waste=np.sum(consumer.input_data['waste'])
    
    sludge_component=get_component_by_name(components=consumer.components, name="Sludge2Hub_LSM")
    sludge=(solph.views.node(results, sludge_component.label).get("sequences"))
    sludge=np.sum(sludge[sludge.columns[1]].values)
    
    sdg12=(wasterecycle+wastereduction)/(waste+sludge)
    
    
    #SDG 13
    emission_component=get_component_by_name(components=consumer.components, name="Emissiontotal_LSM")
    emission=(solph.views.node(results, emission_component.label).get("sequences"))
    emission=np.sum(emission[emission.columns[0]].values)
    
    sdg13=(em_old-emission)/em_old
    
    
    #SDG 11
    
    sumSDG=(sdg1 + sdg6 + sdg7 + sdg12 + sdg13)*100
    
    sdg1new = sdg1 * 100/sumSDG
    sdg6new = sdg6 * 100/sumSDG
    sdg7new = sdg7 * 100/sumSDG
    sdg12new = sdg12 * 100/sumSDG
    sdg13new = sdg13 * 100/sumSDG
    
    sdg11={
        "SDG1 to 11" : sdg1new,
        "SDG6 to 11" : sdg6new,
        "SDG7 to 11" : sdg7new,
        "SDG12 to 11" : sdg12new,
        "SDG13 to 11" : sdg13new,
        }
    
    
    #Total SDG Struct
    sdgTotal={
        "SDG1" : sdg1,
        "SDG6" : sdg6,
        "SDG7" : sdg7,
        "SDG12" : sdg12,
        "SDG13" : sdg13,
        "SDG11" : sdg11
        }
    
    return sdgTotal
    
    
def get_duals_struct(*args, **kwargs):
    model=kwargs.get("model", 0)
    
    sdg6=model.dual[model.MyBlock_SDG6rule_LSC.sdg6rule]
    sdg7=model.dual[model.MyBlock_SDG7rule_LSC.sdg7rule]
    sdg12=model.dual[model.MyBlock_SDG12rule_LSC.sdg12rule]
    sdg13=model.dual[model.MyBlock_SDG13rule_LSC.sdg13rule]
    
    #Total SDG Struct
    sdgTotal={
        "lambda_SDG6" : sdg6,
        "lambda_SDG7" : sdg7,
        "lambda_SDG12" : sdg12,
        "lambda_SDG13" : sdg13,
        }
    
    return sdgTotal
    
    
def get_duals_struct_lsm(*args, **kwargs):
    model=kwargs.get("model", 0)
    
    sdg6=model.dual[model.MyBlock_SDG6rule_LSM.sdg6rule]
    sdg7=model.dual[model.MyBlock_SDG7rule_LSM.sdg7rule]
    sdg12=model.dual[model.MyBlock_SDG12rule_LSM.sdg12rule]
    sdg13=model.dual[model.MyBlock_SDG13rule_LSM.sdg13rule]
    
    #Total SDG Struct
    sdgTotal={
        "lambda_SDG6" : sdg6,
        "lambda_SDG7" : sdg7,
        "lambda_SDG12" : sdg12,
        "lambda_SDG13" : sdg13,
        }
    
    return sdgTotal