# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 14:40:00 2021

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import numpy as np
import transformerLossesMultipleOut as tlmo
import source_emissions as em



class Power2Heat():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.95
            costsIn=0.0005
            costsOut=0
            PowerInLimit = 360
            PowerOutLimit = 360
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'P2H_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'P2H_' + self.position)
            color = 'palegoldenrod'
            self.color = kwargs.get("color", color)

        def component(self):
            q_p2hel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_p2hth = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            p2h = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_p2hel},
                outputs={self.sector_out : q_p2hth},
                conversion_factors={self.sector_out : self.conversion_factor})
            return p2h
        
        
class Battery():
    
    def __init__(self, *args, **kwargs):
        P_inMax = 10
        P_outMax = 10
        costsIn = 0.003
        costsOut=0.003
        Eta_in = 0.95
        Eta_out = 0.95
        Eta_sb = 0.999
        SOC_max = 10
        SOC_start = 10
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_in = kwargs.get("P_in", P_inMax)
        self.P_out = kwargs.get("P_out", P_outMax)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Battery_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Battery_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        color = 'red'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        #Energy Flows
        q_elec2battery = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
        q_battery2elec = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)

            
        if(self.soc_max==0):
            soc_divisor=1
        else:
            soc_divisor=self.soc_max

            
        battery = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_elec2battery},
                outputs={self.sector_out: q_battery2elec},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/soc_divisor, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return battery
    
    
class Heatpump():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsIn=0
            costsOut=0.0015
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            

            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Heatpump_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heatpump_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='orange'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = np.array(kwargs.get("conversion_timeseries", generationArray))

        def component(self):
            q_HPel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_HPth = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            heatpump = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_HPel},
                outputs={self.sector_out : q_HPth},
                conversion_factors={self.sector_out : self.conversion_timeseries})
            return heatpump


class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        costs=1000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Electricitygrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color = 'slategrey'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        self.minimum= kwargs.get("minimum", 0)
        self.maximum= kwargs.get("maximum", 1)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=self.minimum, max=self.maximum, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
            
        elGrid = solph.Source(label=self.label, outputs={self.sector_out: q_electricityGridfeedin})
        
        return elGrid
    
    
class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Electricitygrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color = 'slategrey'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            elGrid = solph.Source(label=self.label, outputs={self.sector_out: q_electricityGridfeedin})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            elGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_electricityGridfeedin, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return elGrid
    

    
class GridSink():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ElectricitygridSink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid\nSink_' + self.position)
        self.costs_in=0
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_in)
            
        elGrid = solph.Sink(label=self.label, inputs={self.sector_in: q_electricityGridfeedin})
        
        return elGrid


    
class ElectrolysisOld():
    
    def __init__(self, *args, **kwargs):
        P_electrolysis = 1000
        K_electrolysis=0.283
        C_elecrolysis_in = 0
        C_elecrolysisout = 0.068
        Water_electrolysis = 0.001
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.P = kwargs.get("P", P_electrolysis)
        self.f = kwargs.get("conversion_factor", K_electrolysis)
        self.Q = self.P*self.f
        self.Water = kwargs.get("water_demand", Water_electrolysis)
        self.costs_in = kwargs.get("c_in", C_elecrolysis_in)
        self.costs_out = kwargs.get("c_out", C_elecrolysisout)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.sector_loss = kwargs.get("sector_loss")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Electrolysis_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electrolysis_' + self.position)
        color='brown'
        self.color = kwargs.get("color", color)
        
    def component(self):
        v_hydrogenElectrolysis = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs = self.costs_in)
        q_hydrogenElectrolysis = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_out)
            
  
        v_water2electrolysis = solph.Flow(min=-1, max=0, nominal_value=self.Q*self.deltaT*self.Water)
            
            
            
        electrolysis = solph.Transformer(
            label=self.label,
            inputs={self.sector_in : q_hydrogenElectrolysis},
            outputs={self.sector_out : v_hydrogenElectrolysis, self.sector_loss : v_water2electrolysis},
            conversion_factors={self.sector_out : self.f, self.sector_loss : -self.f*self.Water})
        
        return electrolysis
    
    
class Electrolysis():
    
    def __init__(self, *args, **kwargs):
        P_electrolysis = 1000
        K_electrolysis=0.283
        C_elecrolysis_in = 0
        C_elecrolysisout = 0.068
        Water_electrolysis = 0.001
        emissions=0
        deltaT=1
        position='Consumer_1'
        summedmax = 0
        
        self.P = kwargs.get("P", P_electrolysis)
        self.f = kwargs.get("conversion_factor", K_electrolysis)
        self.maxWater = kwargs.get("max_water", summedmax)
        self.Q = self.P*self.f
        self.Water = kwargs.get("water_demand", Water_electrolysis)
        self.costs_in = kwargs.get("c_in", C_elecrolysis_in)
        self.costs_out = kwargs.get("c_out", C_elecrolysisout)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.sector_loss = kwargs.get("sector_loss")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'Electrolysis_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electrolysis_' + self.position)
        color='brown'
        self.color = kwargs.get("color", color)
        
    def component(self):
        v_hydrogenElectrolysis = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs = self.costs_in)
        q_hydrogenElectrolysis = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_out)
            
  
        if(self.maxWater==0):
            v_water2electrolysis = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT*self.Water)
        else:
            v_water2electrolysis = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT*self.Water, summed_max=self.maxWater/(self.Q*self.deltaT*self.Water))
            
            
            
        electrolysis = tlmo.TransformerLossesMultipleOut(
            label=self.label,
            inputs={self.sector_in : q_hydrogenElectrolysis, self.sector_loss : v_water2electrolysis},
            outputs={self.sector_out : v_hydrogenElectrolysis},
            conversion_sector = self.sector_in, losses_sector=self.sector_loss,
            conversion_factor=[self.f], losses_factor=self.f*self.Water)

        
        return electrolysis
        
    
class Groundwaterwell():
    
    def __init__(self, *args, **kwargs):
        P = 0.1635
        Q = 3
        eta = 0.5
        h = 10
        deltaT=1
        costsIn=0
        costsOut=0
        position='Consumer_1'
        rho_H2O = 1000
        g = 9.81
        
        self.P= kwargs.get("P_max", P)
        self.Q= kwargs.get("Q_max", Q)
        self.h= kwargs.get("h_max", h)
        self.eta= kwargs.get("eta", eta)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Groundwaterwell_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Groundwaterwell_' + self.position)
        self.costs_in=kwargs.get("c_in", costsIn)
        self.costs_out=kwargs.get("c_out", costsOut)
        self.conversionFactor = rho_H2O*g*self.h/self.eta
        color='darkseagreen'
        self.color = kwargs.get("color", color)
        
    def component(self):
        q_GroundwaterwellEl = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_in)
        v_GroundwaterwellWater = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_out)
            
        groundwaterwell = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_GroundwaterwellEl},
                outputs={self.sector_out : v_GroundwaterwellWater},
                conversion_factors={self.sector_out : 3600000/self.conversionFactor})
        
        return groundwaterwell
    

class Pipelinepump():
    
    def __init__(self, *args, **kwargs):
        P = 18.2
        conversion_factor = 1/0.337
        deltaT=1
        costsIn=0
        costsOut=0.58
        position='Consumer_1'

        
        self.P= kwargs.get("P_max", P)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Pipelinepump_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Pipelinepump_' + self.position)
        self.costs_in=kwargs.get("c_in", costsIn)
        self.costs_out=kwargs.get("c_out", costsOut)
        self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
        color='dodgerblue'
        self.color = kwargs.get("color", color)
        
    def component(self):
        q_PipelinepumpEl = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_in)
        v_PipelinepumpWater = solph.Flow(min=0, max=1, nominal_value=self.P/self.conversion_factor*self.deltaT, variable_costs=self.costs_out)
            
        pipelinepump = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_PipelinepumpEl},
                outputs={self.sector_out : v_PipelinepumpWater},
                conversion_factors={self.sector_out : self.conversion_factor})
        
        return pipelinepump
    
    
class ElectricCooling():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsIn=0
            costsOut=0.002
            efficiency=0.95
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            

            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.eta = kwargs.get("eta", efficiency)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'ElectricCooling_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Electric\nCooling_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='aqua'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = np.array(kwargs.get("conversion_timeseries", generationArray))
            self.conversion = np.multiply(self.conversion_timeseries, self.eta)

        def component(self):
            q_elCoolel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_elCoolcool = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            elcool = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_elCoolel},
                outputs={self.sector_out : q_elCoolcool},
                conversion_factors={self.sector_out : self.conversion})
            return elcool
        
        
class Photovoltaic():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsOut=0
            AreaFactor = 10
            AreaRoof = 100
            AreaEfficiency = 1
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            
            self.costs_out = kwargs.get("c_out", costsOut)
            self.Area_Factor = kwargs.get("area_factor", AreaFactor)
            self.Area_Roof = kwargs.get("area_roof", AreaRoof)
            self.Area_Efficiency = kwargs.get("area_efficiency", AreaEfficiency)
            self.sector_out = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Photovoltaic_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'PV_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='gold'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("generation_timeseries", generationArray)

        def component(self):
            
            Q_pvGenerationValue = solph.Flow(fix = self.conversion_timeseries, nominal_value=self.Area_Efficiency*self.Area_Roof/self.Area_Factor)
            
            pvGeneration = solph.Source(label=self.label, outputs={self.sector_out: Q_pvGenerationValue})
            
            return pvGeneration
        
        
class Photovoltaic_Power():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsOut=0
            Power=10
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P = kwargs.get("P", Power)
            self.sector_out = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Photovoltaic_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'PV_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='gold'
            self.color = kwargs.get("color", color)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("generation_timeseries", generationArray)

        def component(self):
            
            Q_pvGenerationValue = solph.Flow(fix = self.conversion_timeseries, nominal_value=self.P)
            
            pvGeneration = solph.Source(label=self.label, outputs={self.sector_out: Q_pvGenerationValue})
            
            return pvGeneration


class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_in = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Electricitydemand_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Electricity\nDemand_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='yellow'
            self.color = kwargs.get("color", color)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_electricityDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            elecDemand = solph.Sink(label=self.label, inputs={self.sector_in: q_electricityDemand})
            
            return elecDemand    
    

class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        C_power = 35
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        existingGrid = 0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Electricitygridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        self.costs_model = kwargs.get("costs_model", self.costs_out)
        
        self.minimum= kwargs.get("minimum", 0)
        self.maximum= kwargs.get("maximum", 1)
        
        
    def component(self):
        q_electricityGridpurchase = solph.Flow(min=self.minimum, max=self.maximum, variable_costs=self.costs_model,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))
            
        elGrid = solph.Source(label=self.label, outputs={self.sector_out: q_electricityGridpurchase})
        
        return elGrid     



class GridPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        C_power = 35
        deltaT=1
        position='Consumer_1'
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        existingGrid=0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Electricitygridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
        
    def component(self):
        q_electricityGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))

        
        if(self.emissions==0 or self.sector_emissions==0):
            
            elGrid = solph.Source(label=self.label, outputs={self.sector_out: q_electricityGridpurchase})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            elGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_electricityGridpurchase, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return elGrid
        

class GridFeedin():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ElectricitygridFeedin_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid\nFeedin_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        
        revenuesEnergyArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("revenues", revenuesEnergyArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.revenues)
            
        elGridFeedIn = solph.Sink(label=self.label, inputs={self.sector_in: q_electricityGridfeedin})
        
        return elGridFeedIn

        
class GridFeedinMinMax():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        summedMin=0
        summedMax=100000000
        
        self.P= kwargs.get("P", P_connection)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ElectricitygridFeedin_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid\nFeedin_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='tan'
        self.color = kwargs.get("color", color)
        
        revenuesEnergyArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("revenues", revenuesEnergyArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        self.summedMin = kwargs.get("summed_min", summedMin)
        self.summedMax = kwargs.get("summed_max", summedMax)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.revenues, summed_min=self.summedMin/self.P, summed_max=self.summedMax/self.P)
            
        elGridFeedIn = solph.Sink(label=self.label, inputs={self.sector_in: q_electricityGridfeedin})
        
        return elGridFeedIn

    
class flexibilityComponent():
    
    def __init__(self, *args, **kwargs):
        P = 11
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        minimum=-1
        maximum=1
        
        self.P= kwargs.get("P", P)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ElectricityFlexibility_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nFlexibility_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='lightsalmon'
        self.color = kwargs.get("color", color)
        

        
        self.minimum = kwargs.get("minimum", minimum)
        self.maximum = kwargs.get("maximum", maximum)
        
        
    def component(self):
        q_flexibility = solph.Flow(min=self.minimum, max=self.maximum, nominal_value=self.P*self.deltaT, variable_costs=0)
            
        flexibility = solph.Source(label=self.label, inputs={self.sector_out: q_flexibility})
        
        return flexibility

            
        
       
class GridCombinedPowerMetering():
    
    #Class for combined power metering within an energy community
    #This class must be used within the model instead of Grid purchase

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class


            P_connection = 11

            deltaT=1
            position='Consumer_1'
            timesteps=8760
            existingGrid = 0
            emissions=0.209
        
            self.P= kwargs.get("P", P_connection)
            
            
            self.sector_grid= kwargs.get("sector_grid")
            self.sector_electricity= kwargs.get("sector_electricity")
            self.sector_power= kwargs.get("sector_power")
            self.sector_emissions= kwargs.get("sector_emissions", 0)
            
            self.deltaT = kwargs.get("timerange", deltaT)
            
            self.emissions=kwargs.get("emissions", emissions)
            
            self.position = kwargs.get("position", position)
            label = 'ElectricitygridCombinedMetering_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Electricitygrid\nCombined\nMetering_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            self.existingGrid = kwargs.get("existing", existingGrid)
            color='slategrey'
            self.color = kwargs.get("color", color)
        
            costsEnergyArray = np.zeros(self.timesteps)
            
            self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
            self.costs_model = kwargs.get("costs_model", self.costs_out)
            
            
            
            self.label_source = 'Gridsource' + self.label
            self.label_transformer = 'Grid2Elec' + self.label
            
            
            color='teal'
            self.color = kwargs.get("color", color)
            
            
            
            
        def get_source(self):
            return self.GridSource(self)
        
        def get_toElec(self):
            return self.Grid2Elec(self)
        

        #Electricity Component           
            
        class GridSource():
            def __init__(self, outer):
                self.outer = outer
                self.position = self.outer.position
                self.label = 'Gridsource' + self.outer.label
                self.color='navy'
                
                self.sector_grid = self.outer.sector_grid

                
                self.labelSankey = self.outer.labelSankey
                
            

            def component(self):
                q_electricityGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.outer.costs_model, nominal_value=self.outer.P*self.outer.deltaT)
            
                elGrid = solph.Source(label=self.label, outputs={self.sector_grid: q_electricityGridpurchase})
        
                return elGrid
            
            
        class Grid2Elec():
            def __init__(self, outer):
                self.outer = outer
                self.position = self.outer.position
                self.label = 'Grid2Elec' + self.outer.label
                self.color='navy'
                
                self.sector_grid = self.outer.sector_grid
                self.sector_electricity = self.outer.sector_electricity
                self.sector_power = self.outer.sector_power
                self.sector_emissions = self.outer.sector_emissions
                
                self.emissions = self.outer.emissions

                
                self.labelSankey = self.outer.labelSankey
                
            

            def component(self):
                q_gridin = solph.Flow(min=0, max=1, variable_costs=0, nominal_value=self.outer.P*self.outer.deltaT)
                q_grid2elec = solph.Flow(min=0, max=1, variable_costs=0, nominal_value=self.outer.P*self.outer.deltaT)
                q_grid2power = solph.Flow(min=0, max=1, variable_costs=0, nominal_value=self.outer.P*self.outer.deltaT)
                
                if(self.sector_emissions==0):
            
                    grid2el = solph.Transformer(
                        label=self.label,
                        inputs={self.sector_grid : q_gridin},
                        outputs={self.sector_electricity : q_grid2elec, self.sector_power : q_grid2power},
                        conversion_factors={self.sector_electricity : 1, self.sector_power : 1})
                    
                else:
                   m_emissions = solph.Flow(min=0, max=1, variable_costs=0, nominal_value=10000) 
                   
                   grid2el = solph.Transformer(
                        label=self.label,
                        inputs={self.sector_grid : q_gridin},
                        outputs={self.sector_electricity : q_grid2elec, self.sector_power : q_grid2power, self.sector_emissions : m_emissions},
                        conversion_factors={self.sector_electricity : 1, self.sector_power : 1, self.sector_emissions : self.emissions})
        
                return grid2el
        
        

class PeakpowerCosts():
    
    def __init__(self, *args, **kwargs):
        P_connection = 1000
        C_power = 35
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        existingGrid = 0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ElectricitygridCombinedPower_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Electricity\nGrid\nCombinedPower_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        color='navy'
        self.color = kwargs.get("color", color)
        

        
        
    def component(self):
        q_power = solph.Flow(min=0, max=1, variable_costs=0,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))
            
        power = solph.Sink(label=self.label, inputs={self.sector_out: q_power})
        
        return power     
        
 