# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 17:11:41 2021

@author: Matthias Maldet
"""

from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import StorageIntermediate as storage_int
import numpy as np
import source_emissions as em


class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_in = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Mobilitydemand_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Mobility\nDemand_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            color='yellow'
            self.color = kwargs.get("color", color)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            s_transportDemand = solph.Flow(fix = self.demand_timeseries, nominal_value=1)
            
            transportDemand = solph.Sink(label=self.label, inputs={self.sector_in: s_transportDemand})
            
            return transportDemand    

class ElectricVehicle():
    
    def __init__(self, *args, **kwargs):
        P_charge = 11
        P_drive = 110
        costsIn = 0
        costsOut=0
        Eta_charge = 0.95
        Eta_drive = 0.9
        Eta_sb = 0.999
        SOC_max = 40
        SOC_start = 40
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        vehicle_consumption=0.194
        chargeLimit = 1
        driveLimit = 1
        timesteps=8760
        
        
        #Default Values for Nissan Leaf Standard Edition
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_charge = kwargs.get("P_charge", P_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("soc_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'ElectricVehicle_' + self.position
        self.label = kwargs.get("label", label)
        labelDrive = 'ElectricVehicleDrive_' + self.position
        self.labelDrive = kwargs.get("labelDrive", labelDrive)
        self.labelSink = 'ElectricVehicleSink_' + self.position
        self.labelSankey = kwargs.get('label_sankey', 'Electric\nVehicle_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.sinkCosts=0
        color='indigo'
        self.color = kwargs.get("color", color)
        
    def component(self):
        #Energy Flows
        q_evCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.P_charge*self.deltaT)
        q_evDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT)
            
        electricVehicleStore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_evCharge},
                outputs={self.sector_out: q_evDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return electricVehicleStore
    
    def sink(self):
        q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.sinkCosts)
            
        vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
        return vehicle_sink
    
    def drive(self):
        q_evToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT)
        s_EV = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)


        evDrive = solph.Transformer(
            label=self.labelDrive,
            inputs={self.sector_out : q_evToMotor},
            outputs={self.sector_drive : s_EV},
            conversion_factors={self.sector_drive : 1/self.consumption})
        
        return evDrive
    
    
    
class BiofuelVehicle():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        P_drive = 110
        costsIn = 0
        costsOut=0
        Eta_charge = 1
        Eta_drive = 0.35
        Eta_sb = 1
        SOC_max = 0.05
        SOC_start = 0.05
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        vehicle_consumption=0.000044
        chargeLimit = 1
        driveLimit = 1
        #Heating Value Diesel in kWh/m³
        H_diesel = 10400
        timesteps=8760
        self.costsSink=0
        
        #Default Values for VW Golf 2 TDI DSG Style
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.Q_charge = kwargs.get("Q_charge", Q_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("soc_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'BiofuelVehicle_' + self.position
        self.label = kwargs.get("label", label)
        labelDrive = 'BiofuelVehicleDrive_' + self.position
        self.labelDrive = kwargs.get("labelDrive", labelDrive)
        self.labelSink = 'BiofuelVehicleSink_' + self.position
        self.labelSankey = kwargs.get('label_sankey', 'Biofuel\nVehicle_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.H_diesel = kwargs.get("H_diesel", H_diesel)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='blue'
        self.color = kwargs.get("color", color)
        
        
        
    def component(self):
        #Energy Flows
        v_biovCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.Q_charge*self.deltaT)
        v_biovDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
            
        biofuelVehicle = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_biovCharge},
                outputs={self.sector_out: v_biovDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return biofuelVehicle
    
    def sink(self):
        q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.costsSink)
            
        vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
        return vehicle_sink
    
    def drive(self):
        v_biovToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
        s_Biov = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)

        biovDrive = solph.Transformer(
            label=self.labelDrive,
            inputs={self.sector_out : v_biovToMotor},
            outputs={self.sector_drive : s_Biov},
            conversion_factors={self.sector_drive : 1/self.consumption})
        
        return biovDrive
    
    
    
    
class FuelcellVehicle():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        P_drive = 120
        costsIn = 0
        costsOut=0
        Eta_charge = 1
        Eta_drive = 0.6
        Eta_sb = 1
        SOC_max = 0.158
        SOC_start = 0.158
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        position='Consumer_1'
        vehicle_consumption=0.095
        chargeLimit = 1
        driveLimit = 1
        #Heating Value Diesel in kWh/m³
        H_hydrogen = 3
        
        #Default Values for Hyundai Nexo
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.Q_charge = kwargs.get("Q_charge", Q_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'FuelcellVehicle_' + self.position
        self.label = kwargs.get("label", label)
        labelDrive = 'FuelcellVehicleDrive_' + self.position
        self.labelDrive = kwargs.get("labelDrive", labelDrive)
        self.labelSink = 'FuelcellVehicleSink_' + self.position
        self.labelSankey = kwargs.get('label_sankey', 'Fuelcell\nVehicle_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
        self.costsSink=0
        color='brown'
        self.color = kwargs.get("color", color)
        
        
        
    def component(self):
        v_fcCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.Q_charge*self.deltaT)
        v_fcDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_hydrogen)
            
        fcVehicle = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_fcCharge},
                outputs={self.sector_out: v_fcDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return fcVehicle
    
    def sink(self):
       q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.costsSink)
            
       vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
       return vehicle_sink 
   
    def drive(self):
         v_fcToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_hydrogen)
         s_fc = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)
         


         fcDrive = solph.Transformer(
                label=self.labelDrive,
                inputs={self.sector_out : v_fcToMotor},
                outputs={self.sector_drive : s_fc},
                conversion_factors={self.sector_drive : 1/self.consumption})
         
         return fcDrive
    
    
    
class PetrolVehicle():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        P_drive = 110
        costsIn = 0
        costsOut=0
        Eta_charge = 1
        Eta_drive = 0.35
        Eta_sb = 1
        SOC_max = 0.05
        SOC_start = 0.05
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        timesteps=8760
        position='Consumer_1'
        vehicle_consumption=0.000044
        chargeLimit = 1
        driveLimit = 1
        #Heating Value Diesel in kWh/m³
        H_diesel = 10400
        
        #Default Values for VW Golf 2 TDI DSG Style
        
        self.eta_charge = kwargs.get("eta_charge", Eta_charge)
        self.eta_drive = kwargs.get("eta_drive", Eta_drive)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.Q_charge = kwargs.get("Q_charge", Q_charge)
        self.P_drive = kwargs.get("P_drive", P_drive)
        self.consumption = kwargs.get("consumption", vehicle_consumption)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.sector_drive = kwargs.get("sector_drive")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'PetrolVehicle_' + self.position
        self.label = kwargs.get("label", label)
        labelDrive = 'PetrolVehicleDrive_' + self.position
        self.labelDrive = kwargs.get("labelDrive", labelDrive)
        self.labelSink = 'PetrolVehicleSink_' + self.position
        self.labelSankey = kwargs.get('label_sankey', 'Petrol\nVehicle_' + self.position)
        self.balanced = kwargs.get("balanced", False)
        self.charge_limit=kwargs.get("charge_limit", chargeLimit)
        self.drive_limit=kwargs.get("drive_limit", driveLimit)
        self.rangeMax = self.soc_max/self.consumption
        self.H_diesel = kwargs.get("H_diesel", H_diesel)
        self.costsSink=0
        color='darkkhaki'
        self.color = kwargs.get("color", color)
        
        
        
    def component(self):
        #Energy Flows
        v_petrolvCharge = solph.Flow(min=0, max=self.charge_limit, nominal_value=self.Q_charge*self.deltaT)
        v_petrolvDischarge = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
            
        petrolVehicle = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_petrolvCharge},
                outputs={self.sector_out: v_petrolvDischarge},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_charge, outflow_conversion_factor=self.eta_drive,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return petrolVehicle
    
    def sink(self):
        q_sink = solph.Flow(min=0, max=1, nominal_value=self.P_drive, variable_costs=self.costsSink)
            
        vehicle_sink = solph.Sink(label=self.labelSink, inputs={self.sector_out: q_sink})
        
        return vehicle_sink
    
    def drive(self):
        v_petvToMotor = solph.Flow(min=0, max=self.drive_limit, nominal_value=self.P_drive*self.deltaT/self.H_diesel)
        s_petv = solph.Flow(min=0, max=1, nominal_value=self.rangeMax, binary=True)


        petVDrive = solph.Transformer(
            label=self.labelDrive,
            inputs={self.sector_out : v_petvToMotor},
            outputs={self.sector_drive : s_petv},
            conversion_factors={self.sector_drive : 1/self.consumption})
        
        return petVDrive
    
    
    
class PetrolFuel():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        
        self.Q= kwargs.get("Q", Q_charge)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ExternalPetrol_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'External\nPetrol_' + self.position)
        self.costs=0
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        q_petrol = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs)
            
        petrol = solph.Source(label=self.label, outputs={self.sector: q_petrol})
        
        return petrol
    
class PetrolFuelEmissions():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        emissions=2766.4
        maxEmissions=1000000
        
        self.Q= kwargs.get("Q", Q_charge)
        self.sector= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'ExternalPetrol_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'External\nPetrol_' + self.position)
        self.costs=0
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        q_petrol = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs)
        
        if(self.sector_emissions==0 or self.emissions==0):
  
            petrol = solph.Source(label=self.label, outputs={self.sector: q_petrol})
            
        else:
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            petrol = em.Source(label=self.label, 
                           outputs={self.sector: q_petrol, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector)
        
        return petrol
    
class PetrolPurchase():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        timesteps=8760

        
        self.Q= kwargs.get("Q_charge", Q_charge)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Petrolpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Petrol\nPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='slategrey'
        self.color = kwargs.get("color", color)

        
        costsArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsArray)
        
        
    def component(self):
        q_petrolGridpurchase = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_out)
            
        petrol = solph.Source(label=self.label, outputs={self.sector_out: q_petrolGridpurchase})
        
        return petrol     


class PetrolPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        Q_charge = 4000
        deltaT=1
        position='Consumer_1'
        emissions=2766.4
        maxEmissions=1000000
        timesteps=8760
        
        self.Q= kwargs.get("Q", Q_charge)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        label = 'Petrolpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Petrol\nPurchase_' + self.position)
        
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsArray)
        
        
    def component(self):
        q_petrol = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_out)
        
        if(self.sector_emissions==0 or self.emissions==0):
  
            petrol = solph.Source(label=self.label, outputs={self.sector_out: q_petrol})
            
        else:
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            petrol = em.Source(label=self.label, 
                           outputs={self.sector_out: q_petrol, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
            
        return petrol
        
    