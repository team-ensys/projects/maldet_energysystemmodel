# -*- coding: utf-8 -*-
"""
Created on Tue May 10 11:00:47 2022

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import lsc_components as lsc_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth
import resultprocessing_lsc as resultprocessing_lsc
import constraints_lsc as constraints_lsc
import math
import household_components as household_comp
import consumer_lsc as consumer_lsc



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'test_phase2_6consumer'

#Filename
filename = "Input_Model_Hour.xlsx"

#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 8784
timestepsTotal = 8784

#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
#number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    

# -------------- Additional parameters that are set for the simulation -------------- 

#_____________ Electricity _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzentgelte
c_grid = 0.062

#Abgabe
c_abgabe = 0.018

#Reduktion Netzentgelt Lokalbereich
red_local = 1-0.57

#Reduktion Netzentgelt Regionalbereich
red_regional = 1-0.28

#_____________ Heat _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_heat = 0.046


#_____________ Cooling _____________

#Energiepreise und Einspeisetarif in Excel File als Zeitreihe

#Netzgebühr
c_grid_cool = 0.046


#---------------------- DEFINITION OF CONSUMERS ----------------------

#Consumer 1
consumer1 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_1')
consumer1.addElectricitySector()
consumer1.addHeatSector()
consumer1.addCoolingSector()
consumer1.addWaterSector()
consumer1.addWasteSector()
#consumer1.addSector('testsector')
#TESTSTESTE = consumer1.getSector('testsector')
consumer1.sector2system(my_energysystem)

#Consumer 2
consumer2 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_2')
consumer2.addElectricitySector()
consumer2.addHeatSector()
consumer2.addCoolingSector()
consumer2.addWaterSector()
consumer2.addWasteSector()
consumer2.sector2system(my_energysystem)

#Consumer 3
consumer3 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_3')
consumer3.addElectricitySector()
consumer3.addHeatSector()
consumer3.addCoolingSector()
consumer3.addWaterSector()
consumer3.addWasteSector()
consumer3.sector2system(my_energysystem)

#Consumer 4
consumer4 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_4')
consumer4.addElectricitySector()
consumer4.addHeatSector()
consumer4.addCoolingSector()
consumer4.addWaterSector()
consumer4.addWasteSector()
consumer4.sector2system(my_energysystem)

#Consumer 5
consumer5 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_5')
consumer5.addElectricitySector()
consumer5.addHeatSector()
consumer5.addCoolingSector()
consumer5.addWaterSector()
consumer5.addWasteSector()
consumer5.sector2system(my_energysystem)

#Consumer 6
consumer6 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_6')
consumer6.addElectricitySector()
consumer6.addHeatSector()
consumer6.addCoolingSector()
consumer6.addWaterSector()
consumer6.addWasteSector()
consumer6.sector2system(my_energysystem)

#LSC
LSC = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='LSC')
LSC.addElectricitySector()
LSC.addHeatSector()
LSC.addCoolingSector()
LSC.addWaterSector()
LSC.addWaterpoolSector()
LSC.addWasteSector()
LSC.addTransportSector()
LSC.sector2system(my_energysystem)



#---------------------- Consumer 1 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility = consumer1.get_willingness_for_waterflexibility()
willingness_for_flexibility = 0.007007007007007007
waterFlexibility = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer1.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_1",
                                                       color='darkcyan', demand=consumer1.input_data['waterDemand'], wff=willingness_for_flexibility,
                                                       costs_lsc=consumer1.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer1.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr1=consumer1.get_willingness_for_recycling()
wfr1=0.3211211211211211
wasteFlexibility = lsc_comp.WasteFlexibility(sector=consumer1.getWasteSector(), timesteps=timesteps, revenues=consumer1.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_1", color='forestgreen', waste=consumer1.input_data['waste'], 
                                             wfr=wfr1, share_recycling=1)

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold1 = waste_comp.WasteStorage(sector_in=consumer1.getWastestorageSector(), sector_out=consumer1.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_1", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck = 10
s_wastetruck = 20
delay_wastetruck = math.ceil(s_wastetruck/v_wastetruck)
transmissionWaste = lsc_comp.Delay_Transport(sector_in=consumer1.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_1', color='aquamarine')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents1 = consumer_lsc.consumer_setup(  consumer=consumer1, LSC=LSC, timesteps=timesteps, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility, wasteFlexibility=wasteFlexibility, wasteStorageHousehold=wasteStorageHousehold1, transmissionWaste=transmissionWaste)

#Assign components to consumer and energy system
for component in consumercomponents1:
    my_energysystem.add(component.component())
    consumer1.addComponent(component)


#---------------------- Consumer 2 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No additional heat components

#_____________ Cooling _____________
#No additional cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility2 = consumer2.get_willingness_for_waterflexibility()
willingness_for_flexibility2 = 0.05905905905905906
waterFlexibility2 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer2.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_2",
                                                       color='darkcyan', demand=consumer2.input_data['waterDemand'], wff=willingness_for_flexibility2,
                                                       costs_lsc=consumer2.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer2.input_data['waterPipelineCosts'])


#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr2=consumer2.get_willingness_for_recycling()
wfr2=0.367967967967968
wasteFlexibility2 = lsc_comp.WasteFlexibility(sector=consumer2.getWasteSector(), timesteps=timesteps, revenues=consumer2.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_2", color='forestgreen', waste=consumer2.input_data['waste'], 
                                             wfr=wfr2, share_recycling=1)


#Waste Storage for Household
wasteStorageHousehold2 = waste_comp.WasteStorage(sector_in=consumer2.getWastestorageSector(), sector_out=consumer2.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_2", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
v_wastetruck2 = 10
s_wastetruck2 = 20
delay_wastetruck2 = math.ceil(s_wastetruck2/v_wastetruck2)
transmissionWaste2 = lsc_comp.Delay_Transport(sector_in=consumer2.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck2, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_2', color='aquamarine')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents2 = consumer_lsc.consumer_setup(  consumer=consumer2, LSC=LSC, timesteps=timesteps, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility2, wasteFlexibility=wasteFlexibility2, wasteStorageHousehold=wasteStorageHousehold2, transmissionWaste=transmissionWaste2)

#Assign components to consumer and energy system
for component in consumercomponents2:
    my_energysystem.add(component.component())
    consumer2.addComponent(component)
    

#---------------------- Consumer 3 Components ----------------------

#_____________ Electricity _____________

#Grid Purchase combined metering
#No additional electricity components


#_____________ Heat _____________
#No additional heat components

#_____________ Cooling _____________
#No additional cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility3 = consumer3.get_willingness_for_waterflexibility()
willingness_for_flexibility3 = 0.36336336336336333
waterFlexibility3 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer3.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_3",
                                                       color='darkcyan', demand=consumer3.input_data['waterDemand'], wff=willingness_for_flexibility3,
                                                       costs_lsc=consumer3.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer3.input_data['waterPipelineCosts'])


#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
#Willingness for Recycling definition
wfr3=consumer3.get_willingness_for_recycling()
wfr3=0.2838838838838839
wasteFlexibility3 = lsc_comp.WasteFlexibility(sector=consumer3.getWasteSector(), timesteps=timesteps, revenues=consumer3.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_3", color='forestgreen', waste=consumer3.input_data['waste'], 
                                             wfr=wfr3, share_recycling=1)



#Waste Storage for Household
#Disposal Periods - 1 to other consumers because of longer distance
wasteStorageHousehold3 = waste_comp.WasteStorage(sector_in=consumer3.getWastestorageSector(), sector_out=consumer3.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_3", color='mediumvioletred', balanced=False, empty_end=True, t_start=167)



#Waste Transport with Truck
v_wastetruck3 = 10
s_wastetruck3 = 30
delay_wastetruck3 = math.ceil(s_wastetruck3/v_wastetruck3)
transmissionWaste3 = lsc_comp.Delay_Transport(sector_in=consumer3.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck3, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_3', color='aquamarine')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents3 = consumer_lsc.consumer_setup(  consumer=consumer3, LSC=LSC, timesteps=timesteps, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=True, deactivate_electricitygridFeedin=True, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility3, wasteFlexibility=wasteFlexibility3, wasteStorageHousehold=wasteStorageHousehold3, transmissionWaste=transmissionWaste3)

#Assign components to consumer and energy system
for component in consumercomponents3:
    my_energysystem.add(component.component())
    consumer3.addComponent(component)
    
    

#---------------------- Consumer 4 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility4 = consumer4.get_willingness_for_waterflexibility()
willingness_for_flexibility4 = 0.007007007007007007
waterFlexibility4 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer4.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_4",
                                                       color='darkcyan', demand=consumer4.input_data['waterDemand'], wff=willingness_for_flexibility4,
                                                       costs_lsc=consumer4.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer4.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr4=consumer4.get_willingness_for_recycling()
wfr4=0.3211211211211211
wasteFlexibility4 = lsc_comp.WasteFlexibility(sector=consumer4.getWasteSector(), timesteps=timesteps, revenues=consumer4.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_4", color='forestgreen', waste=consumer4.input_data['waste'], 
                                             wfr=wfr4, share_recycling=1)

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold4 = waste_comp.WasteStorage(sector_in=consumer4.getWastestorageSector(), sector_out=consumer4.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_4", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck4 = 10
s_wastetruck4 = 20
delay_wastetruck4 = math.ceil(s_wastetruck4/v_wastetruck4)
transmissionWaste4 = lsc_comp.Delay_Transport(sector_in=consumer4.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck4, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_4', color='aquamarine')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents4 = consumer_lsc.consumer_setup(  consumer=consumer4, LSC=LSC, timesteps=timesteps, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility4, wasteFlexibility=wasteFlexibility4, wasteStorageHousehold=wasteStorageHousehold4, transmissionWaste=transmissionWaste4)

#Assign components to consumer and energy system
for component in consumercomponents4:
    my_energysystem.add(component.component())
    consumer4.addComponent(component)    
    
    
#---------------------- Consumer 5 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility5 = consumer5.get_willingness_for_waterflexibility()
willingness_for_flexibility5 = 0.007007007007007007
waterFlexibility5 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer5.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_5",
                                                       color='darkcyan', demand=consumer5.input_data['waterDemand'], wff=willingness_for_flexibility5,
                                                       costs_lsc=consumer5.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer5.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr5=consumer5.get_willingness_for_recycling()
wfr5=0.3211211211211211
wasteFlexibility5 = lsc_comp.WasteFlexibility(sector=consumer5.getWasteSector(), timesteps=timesteps, revenues=consumer5.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_5", color='forestgreen', waste=consumer5.input_data['waste'], 
                                             wfr=wfr5, share_recycling=1)

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold5 = waste_comp.WasteStorage(sector_in=consumer5.getWastestorageSector(), sector_out=consumer5.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_5", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck5 = 10
s_wastetruck5 = 20
delay_wastetruck5 = math.ceil(s_wastetruck5/v_wastetruck5)
transmissionWaste5 = lsc_comp.Delay_Transport(sector_in=consumer5.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck5, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_5', color='aquamarine')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents5 = consumer_lsc.consumer_setup(  consumer=consumer5, LSC=LSC, timesteps=timesteps, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility5, wasteFlexibility=wasteFlexibility5, wasteStorageHousehold=wasteStorageHousehold5, transmissionWaste=transmissionWaste5)

#Assign components to consumer and energy system
for component in consumercomponents5:
    my_energysystem.add(component.component())
    consumer5.addComponent(component)   
    
    
#---------------------- Consumer 6 Components ----------------------

#_____________ Electricity _____________
#No separate electricity components

#_____________ Heat _____________
#No separate heat components

#_____________ Cooling _____________
#No separate cooling components

#_____________ Water _____________

#Flexibility of water to consumption pool
willingness_for_flexibility6 = consumer6.get_willingness_for_waterflexibility()
willingness_for_flexibility6 = 0.007007007007007007
waterFlexibility6 = lsc_comp.WaterFlexibility(timesteps=timesteps, sector_in=consumer6.getWaterdemandSector(), sector_out=LSC.getWaterpoolSector(), label="Waterflexibility2pool_Consumer_6",
                                                       color='darkcyan', demand=consumer6.input_data['waterDemand'], wff=willingness_for_flexibility6,
                                                       costs_lsc=consumer6.input_data['waterPipelineCosts'], discount=0.5, revenues_consumer=consumer6.input_data['waterPipelineCosts'])

#_____________ Waste _____________

#Waste Reduction and Recycling Flexibility
wfr6=consumer6.get_willingness_for_recycling()
wfr6=0.3211211211211211
wasteFlexibility6 = lsc_comp.WasteFlexibility(sector=consumer6.getWasteSector(), timesteps=timesteps, revenues=consumer6.input_data['wasteMarket'],
                                             label="WasteFlexibility_Consumer_6", color='forestgreen', waste=consumer6.input_data['waste'], 
                                             wfr=wfr6, share_recycling=1)

#Waste Storage for Household
#Für ganzzahlige Storages, sodass am Ende 0: Teiler 144, 183
#Empty end as parameter to determine if storage is empty at the end of the simulation
wasteStorageHousehold6 = waste_comp.WasteStorage(sector_in=consumer6.getWastestorageSector(), sector_out=consumer6.getWastedisposalSector(), 
                                       volume_max=1, volume_start=0, disposal_periods=168, timesteps=timesteps,
                                       label="Wastestorage_Household_6", color='mediumvioletred', balanced=False, empty_end=True, t_start=168)


#Waste Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,7/800 €/kg
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 800kg per Household
#Distance 20km, Average Speed 10km/h (Time to load waste) --> 2h --> round up --> 1 timestep
#for all consumers same disposal days as they are assumed to be located within the same regional area 
#--> can be set differently if the consumers have different disposal days
v_wastetruck6 = 10
s_wastetruck6 = 20
delay_wastetruck6 = math.ceil(s_wastetruck6/v_wastetruck6)
transmissionWaste6 = lsc_comp.Delay_Transport(sector_in=consumer6.getWastedisposalSector(), sector_out=LSC.getWastestorageSector(), 
                                        efficiency=1, costs_transmission=0.7/800, delay=delay_wastetruck6, timesteps=timesteps, capacity_transmission=800,
                                        label='Wastetransport_Consumer_6', color='aquamarine')


#_____________ Consumer Standard Definition _____________

#Consumer definition with function
consumercomponents6 = consumer_lsc.consumer_setup(  consumer=consumer6, LSC=LSC, timesteps=timesteps, 
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=False, deactivate_heatpump=False, deactivate_elcool=False, 
                                                    deactivate_thermalStorage=False, waterFlexibility=waterFlexibility6, wasteFlexibility=wasteFlexibility6, wasteStorageHousehold=wasteStorageHousehold6, transmissionWaste=transmissionWaste6)

#Assign components to consumer and energy system
for component in consumercomponents6:
    my_energysystem.add(component.component())
    consumer6.addComponent(component)   



#---------------------- LSC Components ----------------------



#Sludge Transport with Truck
#Transmission with delay
#Assumption Transport Costs of 0,5€/m³ 
# --> for 20km about 2,5€ costs
# --> Costs for worker partly considered
# --> Transmission capacity assumed with 20m³ 
#Distance 20km, Average Speed 60km/h --> 20min --> round up --> 1 timestep
v_sludgetruck = 60
s_sludgetruck = 20
delay_sludgetruck = math.ceil(s_sludgetruck/v_sludgetruck)
sludgetruckLSC = lsc_comp.Delay_Transport(sector_in=LSC.getSludgestorageSector(), sector_out=LSC.getSludgetreatmentSectorIn(), 
                                        efficiency=1, costs_transmission=0.7, delay=delay_sludgetruck, timesteps=timesteps, capacity_transmission=20,
                                        label='Sludgetransport_LSC', color='aquamarine')


#_____________ LSC Standard Definition _____________

#Consumer definition with function
lscComponents = consumer_lsc.lsc_setup(LSC=LSC, timesteps=timesteps, sludgetruck = sludgetruckLSC)

#Assign components to consumer and energy system
for component in lscComponents:
    my_energysystem.add(component.component())
    LSC.addComponent(component)
    
    
#_____________ LSC Transport Demand _____________

lscTransportDemand = consumer_lsc.lsc_transportdemand(consumers=[consumer1, consumer2, consumer3], LSC=LSC)

#Assign components to consumer and energy system
for component in lscTransportDemand:
    my_energysystem.add(component.component())
    if('Consumer_1' in component.label):
        consumer1.addComponent(component)
    elif('Consumer_2' in component.label):
        consumer2.addComponent(component)
    elif('Consumer_3' in component.label):
        consumer3.addComponent(component)
    else:
        LSC.addComponent(component)


#_____________ LSC Vehicles _____________

lsc_vehicles = consumer_lsc.lsc_vehicles(LSC=LSC, timesteps=timesteps)

#Assign components to consumer and energy system
for component in lsc_vehicles:
    my_energysystem.add(component.component())
    my_energysystem.add(component.drive())
    LSC.addComponent(component)


#---------------------- Solve Model ----------------------
saka = solph.Model(my_energysystem) 




#---------------------- Additional constraints ----------------------

#Prevent Charging and Discharging at same time
saka = constraints.waterstorage_chargeblock(model=saka, storage=consumer_lsc.get_component_by_name(components=LSC.components, name='Waterpoolstorage'))
#saka = constraints.waterstorage_chargeblock(model=saka, storage=waterPoolStorage)

vehiclelist = []
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='Kia'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='Nissan'))
vehiclelist.append(consumer_lsc.get_component_by_name(components=LSC.components, name='Zoe'))

saka = constraints.transport_decision_EVs_flex(vehicles=vehiclelist, componentName='TransportBinaryEV', timesteps=timesteps, model=saka,
                                                      demands=[consumer1.input_data['transportDemand'], consumer2.input_data['transportDemand'], consumer3.input_data['transportDemand']])

"""
#Activation of Excess Pipeline purchase only if casual purchase is exceeded
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=consumer_lsc.get_component_by_name(components=consumer1.components, name='PipelinepurchaseLimited'), 
                                                      pipeline_excess=consumer_lsc.get_component_by_name(components=consumer1.components, name='PipelinepurchaseExcess'))
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=consumer_lsc.get_component_by_name(components=consumer2.components, name='PipelinepurchaseLimited'), 
                                                      pipeline_excess=consumer_lsc.get_component_by_name(components=consumer2.components, name='PipelinepurchaseExcess'))
saka = constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=consumer_lsc.get_component_by_name(components=consumer3.components, name='PipelinepurchaseLimited'), 
                                                      pipeline_excess=consumer_lsc.get_component_by_name(components=consumer3.components, name='PipelinepurchaseExcess'))
"""


#--------------------------------------------------------------------

"""
if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    saka.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, saka, scenario)


results = meth.restore_results(scenario)

"""

if timesteps < 5:
        file2write = scenario + '/saka.lp'
        saka.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
 
    
saka.solve(solver=solver, solve_kwargs={'tee': True})
results = solph.processing.results(saka)

print("----------------optimisation solved----------------")



#__________________________ Result Total Costs of the Previous Run ________________________________________
#Just a comment - no code
#Total Costs ohne NB mit Big M = 5206.62
#Total Costs Run 1 mit Big M = 4932.75


#__________________________ ToDo ________________________________________
#Label Drive muss noch eingebunden werden