# -*- coding: utf-8 -*-
"""
Created on Thu May 12 06:47:35 2022

@author: Matthias Maldet
"""

import pandas as pd
import os as os
#import shutil as shutil
#import sys as sys
import matplotlib.pyplot as plt
import matplotlib.sankey as msank
import numpy as np
import oemof.solph as solph
import pandas as pd
from pathlib import Path

def consumer_to_excel(*args, **kwargs):
    
    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    filename = kwargs.get("filename", "lsc_results.xlsx")
    activate_replacement = kwargs.get("replacement", True)
    model = kwargs.get("model", 0)
    objective_write = kwargs.get("objective", 'TotalCosts')
    
    #if os.path.exists(filename):
        #os.remove(filename)
    
    #open(filename, 'w').close()

    if consumers:
        consumerCounter=0
        for consumer in consumers:
            
            counter=0
            
            if(consumerCounter==0):
                mode = 'w'
            else:
                mode='a'
            consumerCounter+=1
            
            for component in consumer.components:
                if(counter==0):
                    df = solph.views.node(results, component.label).get("sequences")
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df, consumer, component)
                        df = df.set_axis(replacecolumns, axis=1, inplace=False)
                    counter=1
                else:
                    df_to_join = solph.views.node(results, component.label).get("sequences")
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                        df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                    df = df.join(df_to_join)
                    
                    if(hasattr(component, 'labelDrive')):
                        df_to_join = solph.views.node(results, component.labelDrive).get("sequences")
                        if(activate_replacement):
                            replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                            df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                        df = df.join(df_to_join)
            
            writer = pd.ExcelWriter(filename, mode=mode, engine="openpyxl")  
            df.to_excel(writer, sheet_name=consumer.label)
            #writer.save()
            writer.close()
            
    """
    #Logic for objective function
    objective = model.objective()
    # initialize list elements
    data = [objective]
 
    # Create the pandas DataFrame with column name is provided explicitly
    dfObjective = pd.DataFrame(data, columns=['Objective_'+objective_write])
    writer = pd.ExcelWriter(filename, mode='a', engine="openpyxl")  
    dfObjective.to_excel(writer, sheet_name='Objective')
    #writer.save()
    
    writer.close()
    """
                
def consumer_to_csv(*args, **kwargs):
    
    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    filename = kwargs.get("filename", "lsc_results.xlsx")
    scenarioname = kwargs.get("scenarioname", "testscenario")
    activate_replacement = kwargs.get("replacement", True)
    model = kwargs.get("model", 0)
    objective_write = kwargs.get("objective", 'TotalCosts')
    file2plot = 'TEST'
    
    #create the plot directory
    
    #scenarioname = os.path.join('results', scenarioname)
    
    if not os.path.exists(scenarioname):
        os.makedirs(scenarioname)
    
    plotDirectory = os.path.join(scenarioname, 'results_technology')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
    
    filenameSave = filename
    
    #if os.path.exists(filename):
        #os.remove(filename)
    
    #open(filename, 'w').close()

    if consumers:
        consumerCounter=0
        for consumer in consumers:
            
            counter=0
            
            if(consumerCounter==0):
                mode = 'w'
            else:
                mode='a'
            consumerCounter+=1
            
            for component in consumer.components:
                if(counter==0):
                    df = solph.views.node(results, component.label).get("sequences")
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df, consumer, component)
                        df = df.set_axis(replacecolumns, axis=1, inplace=False)
                    counter=1
                else:
                    df_to_join = solph.views.node(results, component.label).get("sequences")
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                        df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                    df = df.join(df_to_join)
                    
                    if(hasattr(component, 'labelDrive')):
                        df_to_join = solph.views.node(results, component.labelDrive).get("sequences")
                        if(activate_replacement):
                            replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                            df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                        df = df.join(df_to_join)
                        
            
            
            filename = plotDirectory + '/' + consumer.label + '_' + filenameSave
            #writer = pd.ExcelWriter(filename, mode=mode, engine="openpyxl")  
            df.to_csv(filename)
            
            #writer.save()
            #writer.close()            
    

def rename_dataframe_columns(df, consumer, component):
    replacecolumns = []
    
    for column in df.columns:
        if('flow' in str(column[1]).lower()):
            string = consumer_string_replacement_logic_flow(column, consumer.label, component)
        elif('storage_content' in str(column[1]).lower()):
            string = consumer_string_replacement_logic_storage(column, consumer.label, component)
        else:
            string = 'UNDEFINED'
         
        replacecolumns.append(string)
        
    return replacecolumns



def consumer_string_replacement_logic_flow(data, label, component):
    data0 = data[0][0].split('_')
    data1 = data[0][1].split('_')

    
    uid = 'UNDEFINED'
    inout = 'UNDEFINED'
    sector = 'UNDEFINED'
    consumer = 'UNDEFINED'
    
    
    if( data0[0].lower() in label.lower() or (data1[1].lower() in label.lower()) and not(data1[1].lower().isnumeric())):
        uid = data1[0]
        
        inout = 'IN'
        
        if('lsc' in data0[0].lower()):
            sector=data0[1]
        else:
            sector=data0[2]
        
        
        if('lsc' in label.lower() or 'lsc' in data0[0].lower()):
            consumer = 'LSC_1'
        elif('household' in data0[0].lower()):
            consumer = 'Household_' + data0[1]
        else:
            consumer = data0[0] + '_' + data0[1]
            
            
    elif((data0[1].lower() in label.lower() or data1[0].lower() in label.lower())):
        uid = data0[0]
        inout='OUT'
        
        if('lsc' in data1[0].lower()):
            sector=data1[1]
        else:
            sector=data1[2]
        
        if('lsc' in label.lower() or 'lsc' in data1[0].lower()):
            consumer = 'LSC_1'
        elif('household' in data1[0].lower()):
            consumer = 'Household_' + data1[1]
        else:
            consumer = data1[0] + '_' + data1[1]
            

    string_to_print = uid + '_' + inout + '_' + sector + '_' + consumer + '_' + component.color
    
    return string_to_print


def consumer_string_replacement_logic_storage(data, label, component):
    data0 = data[0][0].split('_')
    
    uid = data0[0]
    inout = 'SOC'
    sector = 'Storage'
    
    if('lsc' in label.lower()):
        consumer = 'LSC_1'
    elif('household' in data0[1].lower()):
            consumer = 'Household_' + data0[2]
    else:
        consumer = data0[1] + '_' + data0[2]
            
    string_to_print = uid + '_' + inout + '_' + sector + '_' + consumer + '_' + component.color
    
    return string_to_print
        
 
def costs_to_excel(*args, **kwargs):
    totalCosts=0
    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    filename = kwargs.get("filename", "lsc_costs.xlsx")
    activate_replacement = kwargs.get("replacement", True)
    model = kwargs.get("model", 0)
    timeseries = kwargs.get("timeseries", True)
    
    df_to_plot=pd.DataFrame()
    
    
    #if os.path.exists(filename):
        #os.remove(filename)
    
    #open(filename, 'w').close()

    #Components for consumer flows
    consumerFlows=[]
    for number, item in model.flows.items():
        consumerFlows.append(item) 

    if consumers:
        consumerCounter=0
        for consumer in consumers:
            
            #Power Costs for Peakpower Sector
            c_power = 0
            
            df_to_plot=pd.DataFrame()
            
            counter=0
            
            #Consumer counter for writing in the excel
            if(consumerCounter==0):
                mode = 'w'
            else:
                mode='a'
            consumerCounter+=1
                
            for component in consumer.components:
                
                #Peakpower Cost setting
                if(hasattr(component, 'costs_power')):
                    c_power=component.costs_power
                
                
                if(counter==0):
                    df = solph.views.node(results, component.label).get("sequences")
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df, consumer, component)
                        df = df.set_axis(replacecolumns, axis=1, inplace=False)
                    counter=1
                else:
                    df_to_join = solph.views.node(results, component.label).get("sequences")
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                        df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                    df = df.join(df_to_join)
                    
                    if(hasattr(component, 'labelDrive')):
                        df_to_join = solph.views.node(results, component.labelDrive).get("sequences")
                        if(activate_replacement):
                            replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                            df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                        df = df.join(df_to_join)
                        
                
                cols = list(df.columns)
                
                #Alle Columns des Dataframes durchgehen
                for column in cols:
                    variable_costs=0
                    #Split of the columns
                    colsplit = column.split('_')
                    
                    #Alle gefundenen Flows für einen Dataframe durchgehen
                    for flow in consumerFlows:
                        
                        #Find the current component
                            
                        #If Flow is an input --> check the input costs for a component
                        if('lsc' in colsplit[3].lower()):
                            if('lsc' in consumer.label.lower()):
                                plotlabelIn = colsplit[3] + '_' +colsplit[2]
                                plotlabelOut = colsplit[0] + '_' +colsplit[3]
                            else:
                                plotlabelIn = colsplit[3] + '_' + colsplit[2]
                                plotlabelOut = colsplit[0] + '_' + consumer.label
                                
                        else:
                            plotlabelIn = colsplit[3] + '_' +colsplit[4] + '_' +colsplit[2]
                            plotlabelOut = colsplit[0] + '_' +colsplit[3] + '_' +colsplit[4]
                                
                        if('in' in colsplit[1].lower()): 
                                if(plotlabelIn.lower() in str(flow.input).lower() and plotlabelOut.lower() in str(flow.output).lower()):
                                    variable_costs=flow.variable_costs
                                    
                        if('out' in colsplit[1].lower()): 
                                if(plotlabelIn.lower() in str(flow.output).lower() and plotlabelOut.lower() in str(flow.input).lower()):
                                    variable_costs=flow.variable_costs

                                    
                    flowData = list(df[column].values)
                    costs = np.multiply(flowData, variable_costs)
                    
                    #Get the peakpower costs for electricity
                    if('electricitypeakpowercombined' in colsplit[0].lower() and 'in' in colsplit[1].lower() and 'lsc' in colsplit[3].lower()):
                        costs[0] = c_power*max(flowData)
                    
                    
                    if not timeseries:
                        costs=[np.sum(costs)]
                        
                    
                    df_to_plot[column] = costs
        

            writer = pd.ExcelWriter(filename, mode=mode, engine="openpyxl")
            df_to_plot.to_excel(writer, sheet_name=consumer.label)
            #writer.save()
            writer.close()


            
            
def costs_to_csv(*args, **kwargs):
    totalCosts=0
    consumers = kwargs.get("consumers", 0)
    results = kwargs.get("results", 0)
    filename = kwargs.get("filename", "lsc_costs.xlsx")
    scenarioname = kwargs.get("scenarioname", "testscenario")
    activate_replacement = kwargs.get("replacement", True)
    model = kwargs.get("model", 0)
    timeseries = kwargs.get("timeseries", True)
    
    df_to_plot=pd.DataFrame()
    
    filenameSave = filename
    
    #Create Resultdirectories
    #scenarioname = os.path.join('results', scenarioname)
    
    if not os.path.exists(scenarioname):
        os.makedirs(scenarioname)
    
    plotDirectory = os.path.join(scenarioname, 'results_costs')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
    
    #if os.path.exists(filename):
        #os.remove(filename)
    
    #open(filename, 'w').close()

    #Components for consumer flows
    consumerFlows=[]
    for number, item in model.flows.items():
        consumerFlows.append(item) 

    if consumers:
        consumerCounter=0
        for consumer in consumers:
            
            #Power Costs for Peakpower Sector
            c_power = 0
            
            df_to_plot=pd.DataFrame()
            
            counter=0
            
            #Consumer counter for writing in the excel
            if(consumerCounter==0):
                mode = 'w'
            else:
                mode='a'
            consumerCounter+=1
                
            for component in consumer.components:
                
                
                #Peakpower Cost setting
                if(hasattr(component, 'costs_power')):
                    c_power=component.costs_power
                
                
                if(counter==0):
                    df = solph.views.node(results, component.label).get("sequences")
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df, consumer, component)
                        df = df.set_axis(replacecolumns, axis=1, inplace=False)
                    counter=1
                else:
                    
                    df_to_join = solph.views.node(results,component.label).get("sequences")
                    
                    if(activate_replacement):
                        replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                        df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                    df = df.join(df_to_join)
                    
                    if(hasattr(component, 'labelDrive')):
                        df_to_join = solph.views.node(results, component.labelDrive).get("sequences")
                        if(activate_replacement):
                            replacecolumns = rename_dataframe_columns(df_to_join, consumer, component)
                            df_to_join = df_to_join.set_axis(replacecolumns, axis=1, inplace=False)
                        df = df.join(df_to_join)
                        
                
                cols = list(df.columns)
                
                #Alle Columns des Dataframes durchgehen
                for column in cols:
                    variable_costs=0
                    #Split of the columns
                    colsplit = column.split('_')
                    
                    #Alle gefundenen Flows für einen Dataframe durchgehen
                    for flow in consumerFlows:
                        
                        #Find the current component
                            
                        #If Flow is an input --> check the input costs for a component
                        if('lsc' in colsplit[3].lower()):
                            
                            if('lsc' in consumer.label.lower()):
                                plotlabelIn = colsplit[3] + '_' +colsplit[2]
                                plotlabelOut = colsplit[0] + '_' +colsplit[3]
                            else:
                                plotlabelIn = colsplit[3] + '_' + colsplit[2]
                                plotlabelOut = colsplit[0] + '_' + consumer.label
                                
                        else:
                            plotlabelIn = colsplit[3] + '_' +colsplit[4] + '_' +colsplit[2]
                            plotlabelOut = colsplit[0] + '_' +colsplit[3] + '_' +colsplit[4]
                        
                        
                        if('in' in colsplit[1].lower()):
                            
                            
                                
                                
                                if(plotlabelIn.lower() in str(flow.input).lower() and plotlabelOut.lower() in str(flow.output).lower()):
                                    
                                    variable_costs=flow.variable_costs
                                    
                                    if('LSC2heat' in colsplit[0] or 'LSC2cool' in colsplit[0]):
                                        variable_costs = np.subtract(variable_costs, 0.001)
                                        
                                    
                                    
                        if('out' in colsplit[1].lower()): 
                                
                                if(plotlabelIn.lower() in str(flow.output).lower() and plotlabelOut.lower() in str(flow.input).lower()):
                                    
                                    variable_costs=flow.variable_costs
                                    
                                    if('LSC2heat' in colsplit[0] or 'LSC2cool' in colsplit[0]):
                                        variable_costs = np.subtract(variable_costs, 0.001)
                                        

                                    
                    flowData = list(df[column].values)
                    costs = np.multiply(flowData, variable_costs)
                    
                    #Get the peakpower costs for electricity
                    if('electricitypeakpowercombined' in colsplit[0].lower() and 'in' in colsplit[1].lower() and 'lsc' in colsplit[3].lower()):
                        costs[0] = c_power*max(flowData)
                    
                    
                    if not timeseries:
                        costs=[np.sum(costs)]
                        
                    
                    df_to_plot[column] = costs
        

            filename = plotDirectory + '/' + consumer.label + '_' + filenameSave
            #writer = pd.ExcelWriter(filename, mode=mode, engine="openpyxl")  
            df_to_plot.to_csv(filename)
                    
                    
                
                    
                                    