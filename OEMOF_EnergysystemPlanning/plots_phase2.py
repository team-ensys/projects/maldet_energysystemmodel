# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 08:37:27 2022

@author: Matthias Maldet
"""

import pandas as pd
import os as os
#import shutil as shutil
#import sys as sys
import matplotlib.pyplot as plt
import matplotlib.sankey as msank
import numpy as np
import oemof.solph as solph
import pandas as pd
import plotly.graph_objects as go
from plotly.offline import init_notebook_mode, iplot

import math
from matplotlib import pyplot as plt
import matplotlib.sankey as msank

import seaborn as sns

from sklearn.cluster import KMeans


def get_unit(sector):
    sectors = ['Electricitysector', 'Heatsector', 'HotWater', 'Coolingsector', 'Potablewatersector', 'Waterpoolsector', 'Waterdemandsector', 'Sewagesector', 
           'Sludgesector', 'Waterrecoverysector', 'Sludgestoragesector', 'SludgetreatmentsectorIN', 'SludgetreatmentsectorOUT', 'Wastesector', 'Wastestoragesector', 
           'Wastedisposalsector', 'Mobilitysector', 'Waterwashingsector', 'Electricvehicle1sector', 'Electricvehicle2sector', 'Electricvehicle3sector', 'Electricvehicle4sector',
           'Electricvehicle5sector', 'Electricvehicle6sector', 'Electricvehicle7sector', 'Electricvehicle8sector', 'Electricvehicle9sector', 'Electricvehicle10sector',
           'Electricvehicle11sector', 'Electricvehicle12sector']
    
    if('electricitysector' in sector.lower()):
        unit='kWh'
    elif('heatsector' in sector.lower()):
        unit='kWh'
    elif('hotWater' in sector.lower()):
        unit='kWh'
    elif('coolingsector' in sector.lower()):
        unit='kWh'
    elif('potablewatersector' in sector.lower()):
        unit='kWh'
    elif('waterpoolsector' in sector.lower()):
        unit='m³'
    elif('waterdemandsector' in sector.lower()):
        unit='m³'
    elif('sewagesector' in sector.lower()):
        unit='m³'
    elif('sludgesector' in sector.lower()):
        unit='m³'
    elif('waterrecoverysector' in sector.lower()):
        unit='m³'
    elif('sludgestoragesector' in sector.lower()):
        unit='m³'
    elif('sludgetreatmentsectorIN' in sector.lower()):
        unit='m³'
    elif('sludgetreatmentsectorOUT' in sector.lower()):
        unit='m³'
    elif('wastesector' in sector.lower()):
        unit='kg'
    elif('wastestoragesector' in sector.lower()):
        unit='kg'
    elif('wastedisposalsector' in sector.lower()):
        unit='kg'
    elif('mobilitysector' in sector.lower()):
        unit='km'
    elif('waterwashingsector' in sector.lower()):
        unit='m³'
    elif('electricvehicle1sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle2sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle3sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle4sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle5sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle6sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle7sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle8sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle9sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle10sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle11sector' in sector.lower()):
        unit='kWh'
    elif('electricvehicle12sector' in sector.lower()):
        unit='kWh'
    elif('emissionsector' in sector.lower()):
        unit='kg'
    elif('emissionswastetrucksector' in sector.lower()):
        unit='kg'
    elif('emissionssludgetrucksector' in sector.lower()):
        unit='m³'
    else:
        unit='kWh'
        
    return unit

def get_color_unit(sector):
    sectors = ['Electricitysector', 'Heatsector', 'HotWater', 'Coolingsector', 'Potablewatersector', 'Waterpoolsector', 'Waterdemandsector', 'Sewagesector', 
           'Sludgesector', 'Waterrecoverysector', 'Sludgestoragesector', 'SludgetreatmentsectorIN', 'SludgetreatmentsectorOUT', 'Wastesector', 'Wastestoragesector', 
           'Wastedisposalsector', 'Mobilitysector', 'Waterwashingsector', 'Electricvehicle1sector', 'Electricvehicle2sector', 'Electricvehicle3sector', 'Electricvehicle4sector',
           'Electricvehicle5sector', 'Electricvehicle6sector', 'Electricvehicle7sector', 'Electricvehicle8sector', 'Electricvehicle9sector', 'Electricvehicle10sector',
           'Electricvehicle11sector', 'Electricvehicle12sector']
    
    if('electricity' in sector.lower()):
        unit='blue'
    elif('peakpower' in sector.lower()):
        unit='cornflowerblue'
    elif('heatsector' in sector.lower()):
        unit='red'
    elif('hotWater' in sector.lower()):
        unit='firebrick'
    elif('coolingsector' in sector.lower()):
        unit='aqua'
    elif('potablewatersector' in sector.lower()):
        unit='green'
    elif('waterpoolsector' in sector.lower()):
        unit='lime'
    elif('waterdemandsector' in sector.lower()):
        unit='olivedrab'
    elif('sewagesector' in sector.lower()):
        unit='goldenrod'
    elif('sludgesector' in sector.lower()):
        unit='brown'
    elif('waterrecoverysector' in sector.lower()):
        unit='deepskyblue'
    elif('sludgestoragesector' in sector.lower()):
        unit='darkorange'
    elif('sludgetreatmentsectorIN' in sector.lower()):
        unit='mediumslateblue'
    elif('sludgetreatmentsectorOUT' in sector.lower()):
        unit='mediumpurple'
    elif('wastesector' in sector.lower()):
        unit='grey'
    elif('wastestoragesector' in sector.lower()):
        unit='lightgrey'
    elif('wastedisposalsector' in sector.lower()):
        unit='black'
    elif('mobilitysector' in sector.lower()):
        unit='darkviolet'
    elif('waterwashingsector' in sector.lower()):
        unit='navy'
    elif('electricvehicle1sector' in sector.lower()):
        unit='palegreen'
    elif('electricvehicle2sector' in sector.lower()):
        unit='magenta'
    elif('electricvehicle3sector' in sector.lower()):
        unit='wheat'
    elif('electricvehicle4sector' in sector.lower()):
        unit='sienna'
    elif('electricvehicle5sector' in sector.lower()):
        unit='teal'
    elif('electricvehicle6sector' in sector.lower()):
        unit='mediumslateblue'
    elif('electricvehicle7sector' in sector.lower()):
        unit='floralwhite'
    elif('electricvehicle8sector' in sector.lower()):
        unit='palegreen'
    elif('electricvehicle9sector' in sector.lower()):
        unit='beige'
    elif('electricvehicle10sector' in sector.lower()):
        unit='whitesmoke'
    elif('electricvehicle11sector' in sector.lower()):
        unit='lavender'
    elif('electricvehicle12sector' in sector.lower()):
        unit='navajowhite'
    elif('emissionsector' in sector.lower()):
        unit='black'
    elif('emissionswastetrucksector' in sector.lower()):
        unit='sienna'
    elif('emissionssludgetrucksector' in sector.lower()):
        unit='peachpuff'
    else:
        unit='midnightblue'
        
    return unit

def get_color(consumer):

    colorPV=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru', 'grey']
    if('LSC' in consumer):
        color='deepskyblue'
    elif('10' in consumer):
        color='darkcyan'
    elif('11' in consumer):
        color='crimson'
    elif('12' in consumer):
        color='darkolivegreen'
    elif('13' in consumer):
        color='peru'
    elif('LSC' in consumer):
        color='grey'
    elif('1' in consumer):
        color='red'
    elif('2' in consumer):
        color='green'
    elif('3' in consumer):
        color='blue'
    elif('4' in consumer):
        color='orange'
    elif('5' in consumer):
        color='purple'
    elif('6' in consumer):
        color='brown'
    elif('7' in consumer):
        color='steelblue'
    elif('8' in consumer):
        color='gold'
    elif('9' in consumer):
        color='seagreen'
    else:
        color='black'
        
    return color

    

#create the required files
def create_folders(scenario, sectorlist, consumerlist):
    #plotting the results
    #create the output folders
    direc = scenario
    
    for sector in sectorlist:
        createOutSectorFolders(direc, sector)
        
        for consumer in consumerlist:
            createOutSectorFoldersConsumers(direc, sector, consumer)

#Create Folder
def createOutSectorFolders(resultdirectory, foldername):
    plotDirectory = os.path.join(resultdirectory, foldername)
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, '_sankey')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_bar_lscshare')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)    
        
    plotDirectory = os.path.join(resultdirectory, '_bar_lscshare\sector')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_bar_lscshare\consumer')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_violin_lsc')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)    
        
    plotDirectory = os.path.join(resultdirectory, '_violin_lsc/casual')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)  
        
    plotDirectory = os.path.join(resultdirectory, '_violin_lsc/nozeros')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)  
        
    plotDirectory = os.path.join(resultdirectory, '_heatmap_lsc')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)   
        
    plotDirectory = os.path.join(resultdirectory, foldername, '_loadcurve')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_storage')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_resource_energyrecovery')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_transport')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_transport/number_rides')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_transport/timeseries_rides')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_transport/vehicle_distribution')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_transport/vehicle_share')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_peakpower')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_flexibility')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_own_consumption')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_delay')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_waterpool')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, '_scatter')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, foldername, '_scatter_sorted')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_storage')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_emissions')
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
        
def createOutSectorFoldersConsumers(resultdirectory, foldername, consumer):
    plotDirectory = os.path.join(resultdirectory, foldername, '_loadcurve', consumer)
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_storage', consumer)
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)
        
    plotDirectory = os.path.join(resultdirectory, '_costs', consumer)
    if not os.path.exists(plotDirectory):
        os.makedirs(plotDirectory)

#labellogic for certain identifiers
def labellogic(label):
    if('Grid2ElecElectricitygridpurchase' in label):
        label = 'Electricitygridpurchase'
        
    return label

def plot_sankey(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    source=[]
    target=[]
    value=[]
    colors=[]
    color_node=[]
    
    
    #Vehicle Objects
    sectorname = kwargs.get("sectorname", 0)
    df = kwargs.get("dataframe", 0)
    consumer=kwargs.get("consumer", 0)
    unit=kwargs.get("unit", 'kWh')
    directory = kwargs.get("directory", 0)
    
    if(sectorname==0):
        return 0
    
    #List with all column names
    columns = list(df.columns)
    
    #select all columns with a sectorname
    for column in columns:
        if(sectorname in column and consumer in column):
            collist.append(column)
    
    #Sourcelabel for sector
    label = ['']
    
    for column in collist:
        colsplit = column.split('_')
        if('in' in colsplit[1].lower()):
            source.append(0)
            target.append(number)
            label.append(labellogic(colsplit[0]) + ': \n' + str(round(np.sum(list(df[column].values)), 1)) + unit)
            value.append(np.sum(list(df[column].values)))
            if('waterdemandsector' in sectorname.lower() and 'waterflexibility2pool' in column.lower()):
                value[-1] = value[-1]*(-1)
                target[-1] = 0
                source[-1]=number
                label[-1] = (labellogic(colsplit[0]) + ': \n' + str(round(-np.sum(list(df[column].values)), 1)) + unit)
            colors.append(colsplit[5])
            number+=1
        elif('out' in colsplit[1].lower()):
            source.append(number)
            target.append(0)
            label.append(labellogic(colsplit[0]) + ': \n' + str(round(np.sum(list(df[column].values)), 1)) + unit)
            value.append(np.sum(list(df[column].values)))
            if('waterdemandsector' in sectorname.lower() and 'waterflexibility2pool' in column.lower()):
                value[-1] = value[-1]*(-1)
                target[-1] = number
                source[-1]=0
                label[-1] = (labellogic(colsplit[0]) + ': \n' + str(round(-np.sum(list(df[column].values)), 1)) + unit)
            colors.append(colsplit[5])
            number+=1
     
    for sources in source:
        color_node.append("black")        
     
    # data to dict, dict to sankey
    link = dict(source=source, target=target, value=value, color=colors)
    node = dict(label=label, pad=50, thickness=5, color=color_node)
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title=sectorname + '\n' + consumer,
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory + '/' + sectorname + '/' + '_sankey/' + sectorname + '_' + consumer + ".png")
    
    
def plot_sankeyLSC(*args, **kwargs):
    
    
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    source=[]
    target=[]
    value=[]
    colors=[]
    color_node=[]
    
    
    #Vehicle Objects
    sectorname = kwargs.get("sectorname", 0)
    dfs = kwargs.get("dataframes", 0)
    df = kwargs.get("dataframe", 0)
    consumer=kwargs.get("consumer", 0)
    unit=kwargs.get("unit", 'kWh')
    directory = kwargs.get("directory", 0)
    
    
    if(sectorname==0):
        return 0
    
    #Sourcelabel for sector
    label = ['']
    valueIn = 0
    colorIn=''
    labelIn=''
    valueOut=0
    colorOut=''
    for dfx in dfs:
        collist=[]
    
        #List with all column names
        columns = list(dfx.columns)
    
        #select all columns with a sectorname
        for column in columns:
            if(sectorname in column and consumer in column.split('_')[3]):
                collist.append(column)
        
        
        for column in collist:
            colsplit = column.split('_')
            if('in' in colsplit[1].lower()):
                if('emissionsector' in sectorname.lower()):
                    labelIn='ConsumerEmissions'
                else:
                    labelIn = (labellogic(colsplit[0]))
                valueIn+=(np.sum(list(dfx[column].values)))
                colorIn = (colsplit[5])
            elif('out' in colsplit[1].lower()):
                if('emissionsector' in sectorname.lower()):
                    labelOut='ConsumerEmissions'
                else:
                    labelOut = (labellogic(colsplit[0]))
                
                valueOut+= (np.sum(list(dfx[column].values)))
                colorOut = (colsplit[5]) 
    if(valueIn!=0):
        source.append(0)
        target.append(number)
        number+=1
        label.append(labelIn + ': \n' + str(round(valueIn, 1)) + unit)
        value.append(valueIn)
        colors.append(colorIn)
            
    if(valueOut!=0):
        source.append(number)
        target.append(0)
        number+=1
        label.append(labelOut+ ': \n' + str(round(valueOut, 1)) + unit)
        value.append(valueOut)
        colors.append(colorOut)
        
    #List with all column names
    columns = list(df.columns)
    collist=[]
    #select all columns with a sectorname
    for column in columns:
        if(sectorname in column and consumer in column.split('_')[3]):
            collist.append(column)

    #Sourcelabel for sector
    for column in collist:
            
        colsplit = column.split('_')
        if('in' in colsplit[1].lower()):
            source.append(0)
            target.append(number)
            label.append(labellogic(colsplit[0]) + ': \n' + str(round(np.sum(list(df[column].values)), 1)) + unit)
            value.append(np.sum(list(df[column].values)))
            colors.append(colsplit[5])
            number+=1
        elif('out' in colsplit[1].lower()):
            source.append(number)
            target.append(0)
            label.append(labellogic(colsplit[0]) + ': \n' + str(round(np.sum(list(df[column].values)), 1)) + unit)
            value.append(np.sum(list(df[column].values)))
            colors.append(colsplit[5])
            number+=1
    
        
    for sources in source:
        color_node.append("black")        
     
    # data to dict, dict to sankey
    link = dict(source=source, target=target, value=value, color=colors)
    node = dict(label=label, pad=50, thickness=5, color=color_node)
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title=sectorname + '\n' + consumer,
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory + '/' + sectorname + '/' + '_sankey/' + sectorname + '_' + consumer + ".png")
    
    
def plot_lscshareSector_bar(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    colors=[]
    color_node=[]
    
    inshares=[]
    outshares=[]
    
    
    #Vehicle Objects
    sectors = kwargs.get("sectors", 0)
    df = kwargs.get("dataframe", 0)
    consumer=kwargs.get("consumer", 0)
    directory = kwargs.get("directory", 0)
    
    if(sectors==0):
        return 0
    
    #List with all column names
    columns = list(df.columns)
    
    for sectorname in sectors:
        collist=[]
        #select all columns with a sectorname
        for column in columns:
            if(sectorname in column and consumer in column):
                collist.append(column)
    
        #Sourcelabel for sector
        labelIn = []
        valueIn=[]
        labelOut=[]
        valueOut=[]
    
        #get all inputs and outputs for each sector
        for column in collist:
            colsplit = column.split('_')
            if('waterdemandsector' in sectorname.lower()):
                data_locIn = 'Waterpoolpurchase_OUT_Potablewatersector_' + consumer + '_gold'
                data_locTotalIn = 'WaterdemandFlexible_IN_Potablewatersector_' + consumer + '_beige'
                
                labelIn.append('WaterpoolpurchaseLSC')
                valueIn.append(np.sum(list(df[data_locIn].values)))
                colors.append('gold')
                totalInLSC = np.sum(list(df[data_locTotalIn].values))
                
                data_locOut = 'Waterflexibility2pool_OUT_Waterpoolsector_LSC_1_darkcyan'
                data_locTotalOut = 'WaterdemandPredefine_IN_Waterdemandsector_' + consumer + '_yellow'
                labelOut.append('Water2poolLSC')
                valueOut.append(np.sum(list(df[data_locOut].values)))
                colors.append('beige')
                totalOutLSC = np.sum(list(df[data_locTotalOut].values))
                
            elif('out' in colsplit[1].lower()):
                labelIn.append(labellogic(colsplit[0]))
                valueIn.append(np.sum(list(df[column].values)))
                colors.append(colsplit[5])
            elif('in' in colsplit[1].lower()):
                
                labelOut.append(labellogic(colsplit[0]))
                valueOut.append(np.sum(list(df[column].values)))
                
                colors.append(colsplit[5])
           
        #calculate the total sector input and output
        if('waterdemandsector' in sectorname.lower()):
            totalIn=totalInLSC
            totalOut=totalOutLSC
        else:
            totalIn = np.sum(valueIn)
            totalOut = np.sum(valueOut)
            
        
        #Find the LSC components for input and output
        indexIn=0
        iterindex=0
        foundIn = False
        for label in labelIn:
            if('lsc' in label.lower()):
                indexIn=iterindex
                foundIn=True
            iterindex+=1
            
        indexOut=0
        iterindex=0
        foundOut=False
        for label in labelOut:
            if('lsc' in label.lower()):
                indexOut=iterindex
                foundOut=True
            iterindex+=1
            
        
        inLSC = valueIn[indexIn]
        outLSC = valueOut[indexOut]
        
        #Add the found value for each sector to a list to be plotted
        if(foundIn):
            inshares.append(inLSC/totalIn*100)
        else:
            inshares.append(0)
            
        if(foundOut):
            outshares.append(outLSC/totalOut*100)
        else:
            outshares.append(0)
            
    # Figure Size
    fig = plt.figure(figsize =(20, 14))
    # Horizontal Bar Plot
    fig.suptitle(consumer + '_Inputs', fontsize=16)
    
    indexPlus=0
    sectors_plus=[]
    for sector in sectors:
        sectors_plus.append(sectors[indexPlus] + '\n' + str(round(inshares[indexPlus], 2)) + '%')
        indexPlus+=1
    
    plt.bar(sectors_plus, inshares, color=['dodgerblue', 'red', 'green', 'aqua'])
    plt.ylabel("LSC Input Shares in % of total input")
    
    filenameIn = directory + '/' + '_bar_lscshare/sector/' + 'lscSectorShareIn_' + consumer + ".png"
    
    plt.savefig(filenameIn)
    plt.close(fig)
    
     # Figure Size
    fig = plt.figure(figsize =(20, 14))
    fig.suptitle(consumer + '_Outputs', fontsize=16)
    
    indexPlus=0
    sectors_plus=[]
    for sector in sectors:
        sectors_plus.append(sectors[indexPlus] + '\n' + str(round(outshares[indexPlus], 2)) + '%')
        indexPlus+=1
    
    # Horizontal Bar Plot
    plt.bar(sectors_plus, outshares, color=['dodgerblue', 'red', 'green', 'aqua'])
    plt.ylabel("LSC Output Shares in % of total output")
    # Show Plot
    
    filenameIn = directory + '/' + '_bar_lscshare/sector/' + 'lscSectorShareOut_' + consumer + ".png"
    
    plt.savefig(filenameIn)
    plt.close(fig)
        
def plot_lscshareConsumer_bar(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    colors=[]
    color_node=[]
    
    inshares=[]
    outshares=[]
    
    
    #Vehicle Objects
    consumers = kwargs.get("consumers", 0)
    dfs = kwargs.get("dataframes", 0)
    sectorname=kwargs.get("sectorname", 0)
    directory = kwargs.get("directory", 0)
    
    if(consumers==0):
        return 0
    
    
    
    for consumer in consumers:
        df=dfs.parse(consumer)
        #List with all column names
        columns = list(df.columns)
        collist=[]
        #select all columns with a sectorname
        for column in columns:
            if(sectorname in column and consumer in column):
                collist.append(column)
    
        #Sourcelabel for sector
        labelIn = []
        valueIn=[]
        labelOut=[]
        valueOut=[]
    
        #get all inputs and outputs for each sector
        for column in collist:
            colsplit = column.split('_')
            if('waterdemandsector' in sectorname.lower()):
                data_locIn = 'Waterpoolpurchase_OUT_Potablewatersector_' + consumer + '_gold'
                data_locTotalIn = 'WaterdemandFlexible_IN_Potablewatersector_' + consumer + '_beige'
                
                labelIn.append('WaterpoolpurchaseLSC')
                valueIn.append(np.sum(list(df[data_locIn].values)))
                colors.append('gold')
                totalInLSC = np.sum(list(df[data_locTotalIn].values))
                
                data_locOut = 'Waterflexibility2pool_OUT_Waterpoolsector_LSC_1_darkcyan'
                data_locTotalOut = 'WaterdemandPredefine_IN_Waterdemandsector_' + consumer + '_yellow'
                labelOut.append('Water2poolLSC')
                valueOut.append(np.sum(list(df[data_locOut].values)))
                colors.append('beige')
                totalOutLSC = np.sum(list(df[data_locTotalOut].values))
            elif('out' in colsplit[1].lower()):
                labelIn.append(labellogic(colsplit[0]))
                valueIn.append(np.sum(list(df[column].values)))
                if('waterdemandsector' in sectorname.lower() and 'waterflexibility2pool' in column.lower()):
                    valueIn[-1] = valueIn[-1]*(-1)
                colors.append(colsplit[5])
            elif('in' in colsplit[1].lower()):
                labelOut.append(labellogic(colsplit[0]))
                valueOut.append(np.sum(list(df[column].values)))
                if('waterdemandsector' in sectorname.lower() and 'waterflexibility2pool' in column.lower()):
                    valueOut[-1] = valueOut[-1]*(-1)
                colors.append(colsplit[5])
           
        if('waterdemandsector' in sectorname.lower()):
            totalIn=totalInLSC
            totalOut=totalOutLSC
        else:
            totalIn = np.sum(valueIn)
            totalOut = np.sum(valueOut)
        
        #Find the LSC components for input and output
        indexIn=0
        iterindex=0
        foundIn = False
        for label in labelIn:
            if('lsc' in label.lower()):
                indexIn=iterindex
                foundIn=True
            iterindex+=1
            
        indexOut=0
        iterindex=0
        foundOut=False
        for label in labelOut:
            if('lsc' in label.lower()):
                indexOut=iterindex
                foundOut=True
            iterindex+=1
            
        
        inLSC = valueIn[indexIn]
        outLSC = valueOut[indexOut]
        
        #Add the found value for each sector to a list to be plotted
        if(foundIn):
            inshares.append(inLSC/totalIn*100)
        else:
            inshares.append(0)
            
        if(foundOut):
            outshares.append(outLSC/totalOut*100)
        else:
            outshares.append(0)
            
    # Figure Size
    fig = plt.figure(figsize =(20, 14))
    # Horizontal Bar Plot
    fig.suptitle(sectorname + '_Inputs', fontsize=16)
    
    indexPlus=0
    consumer_plus=[]
    for consumer in consumers:
        consumer_plus.append(consumers[indexPlus] + '\n' + str(round(inshares[indexPlus], 2)) + '%')
        indexPlus+=1
    
    plt.bar(consumer_plus, inshares, color=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.ylabel("LSC Input Shares in % of total input")
    
    filenameIn = directory + '/' + '_bar_lscshare/consumer/' + 'lscConsumerShareIn_' + sectorname + ".png"
    
    plt.savefig(filenameIn)
    plt.close(fig)
    
     # Figure Size
    fig = plt.figure(figsize =(20, 14))
    fig.suptitle(sectorname + '_Outputs', fontsize=16)
    
    indexPlus=0
    consumer_plus=[]
    for consumer in consumers:
        consumer_plus.append(consumers[indexPlus] + '\n' + str(round(outshares[indexPlus], 2)) + '%')
        indexPlus+=1
    
    # Horizontal Bar Plot
    plt.bar(consumer_plus, outshares, color=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.ylabel("LSC Output Shares in % of total output")
    # Show Plot
    
    filenameIn = directory + '/' + '_bar_lscshare/consumer/' + 'lscConsumerShareOut_' + sectorname + ".png"
    
    plt.savefig(filenameIn)
    plt.close(fig)
                
    
def get_dataframe_heatmap_in(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot

    
    #Sources and target lists for sankey
    colors=[]

    
    inshares=[]

    
    
    #Vehicle Objects
    sectors = kwargs.get("sectors", 0)
    df = kwargs.get("dataframe", 0)
    consumer=kwargs.get("consumer", 0)
    dfIn=kwargs.get("dataframe_in", 0)
    
    totalInLSC=0

    
    if(sectors==0):
        return 0
    
    #List with all column names
    columns = list(df.columns)
    
    for sectorname in sectors:
        collist=[]
        #select all columns with a sectorname
        for column in columns:
            if(sectorname in column and consumer in column):
                collist.append(column)
    
        #Sourcelabel for sector
        labelIn = []
        valueIn=[]

    
        #get all inputs and outputs for each sector
        for column in collist:
            colsplit = column.split('_')
            if('waterdemandsector' in sectorname.lower()):
                data_locIn = 'Waterpoolpurchase_OUT_Potablewatersector_' + consumer + '_gold'
                data_locTotalIn = 'WaterdemandFlexible_IN_Potablewatersector_' + consumer + '_beige'
                
                labelIn.append('WaterpoolpurchaseLSC')
                valueIn.append(np.sum(list(df[data_locIn].values)))
                colors.append('gold')
                totalInLSC = np.sum(list(df[data_locTotalIn].values))
                
            elif('out' in colsplit[1].lower()):
                labelIn.append(labellogic(colsplit[0]))
                valueIn.append(np.sum(list(df[column].values)))
                if('waterdemandsector' in sectorname.lower() and 'waterflexibility2pool' in column.lower()):
                    valueIn[-1] = valueIn[-1]*(-1)
                colors.append(colsplit[5])

           
        #calculate the total sector input and output
        if('waterdemandsector' in sectorname.lower()):
            totalIn=totalInLSC
        else:
            totalIn = np.sum(valueIn)

        
        #Find the LSC components for input and output
        indexIn=0
        iterindex=0
        foundIn = False
        for label in labelIn:
            if('lsc' in label.lower()):
                indexIn=iterindex
                foundIn=True
            iterindex+=1       

        inLSC = valueIn[indexIn]

        
        #Add the found value for each sector to a list to be plotted
        if(foundIn):
            inshares.append(inLSC/totalIn*100)
        else:
            inshares.append(0)
            
    dfIn.loc[len(dfIn)+1] = inshares
    return dfIn        


def get_dataframe_heatmap_out(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    colors=[]
    color_node=[]
    
    outshares=[]
    
    
    #Vehicle Objects
    sectors = kwargs.get("sectors", 0)
    df = kwargs.get("dataframe", 0)
    consumer=kwargs.get("consumer", 0)
    dfOut=kwargs.get("dataframe_out", 0)
    
    if(sectors==0):
        return 0
    
    #List with all column names
    columns = list(df.columns)
    
    for sectorname in sectors:
        collist=[]
        #select all columns with a sectorname
        for column in columns:
            if(sectorname in column and consumer in column):
                collist.append(column)
    
        #Sourcelabel for sector
        labelIn = []
        valueIn=[]
        labelOut=[]
        valueOut=[]
    
        #get all inputs and outputs for each sector
        for column in collist:
            colsplit = column.split('_')
            if('waterdemandsector' in sectorname.lower()):               
                data_locOut = 'Waterflexibility2pool_OUT_Waterpoolsector_LSC_1_darkcyan'
                data_locTotalOut = 'WaterdemandPredefine_IN_Waterdemandsector_' + consumer + '_yellow'
                labelOut.append('Water2poolLSC')
                valueOut.append(np.sum(list(df[data_locOut].values)))
                colors.append('beige')
                totalOutLSC = np.sum(list(df[data_locTotalOut].values))
            elif('in' in colsplit[1].lower()):
                labelOut.append(labellogic(colsplit[0]))
                valueOut.append(np.sum(list(df[column].values)))
                if('waterdemandsector' in sectorname.lower() and 'waterflexibility2pool' in column.lower()):
                    valueOut[-1] = valueOut[-1]*(-1)
                colors.append(colsplit[5])
           
        #calculate the total sector input and output
        if('waterdemandsector' in sectorname.lower()):
            totalOut=totalOutLSC
        else:
            totalOut = np.sum(valueOut)
        
            
        indexOut=0
        iterindex=0
        foundOut=False
        for label in labelOut:
            if('lsc' in label.lower()):
                indexOut=iterindex
                foundOut=True
            iterindex+=1
            
        

        outLSC = valueOut[indexOut]
        
            
        if(foundOut):
            outshares.append(outLSC/totalOut*100)
        else:
            outshares.append(0)
            
    dfOut.loc[len(dfOut)+1] = outshares
    return dfOut


def plot_heatmap(*args, **kwargs):
    title=kwargs.get("title", 'Heatmap')
    df = kwargs.get("dataframe", 0)
    yLabel = kwargs.get("ylabel", "Consumer")
    column_names = kwargs.get("columns", 0)
    inout = kwargs.get("in_out", 'in')
    directory = kwargs.get("directory", 0)
    
        
    scale_label = 'Contribution in %'
        
    if('in' in inout.lower()):
        scale_label='LSC to Sector in %'
    elif('out' in inout.lower()):
        scale_label='Sector to LSC in %'
        
    plt.figure(figsize = (30,21))
    plt.title(title, fontsize=24)
    sns.heatmap(df[column_names], annot=True, cmap="YlGnBu", cbar_kws={'label': scale_label}, vmin=0, vmax=100, fmt='.3g')
    #plt.xlabel('x(m)')
    plt.ylabel(yLabel, fontsize=14)
    #plt.savefig('heatmap.png')
    
    filenameIn = directory + '/' + '_heatmap_lsc/' + 'lscConsumerShareOut_' + inout + ".png"
    plt.savefig(filenameIn)
    
    
def plot_loadcurve(*args, **kwargs):
    dataframe=kwargs.get("dataframe", 0)
    directory = kwargs.get("directory", 0)
    
    columnlist = list(dataframe.columns.values)
    
    lenTimesteps = len(list(dataframe[columnlist[0]].values))
    
    xValues = list(range(1,lenTimesteps+1))
    
    columnlist.remove(columnlist[0])
    
    for column in columnlist:
        cols = column.split('_')
        
        #No load curve plot for storages
        if('storage' in cols[2].lower() or 'electricitygridsector' in cols[2].lower() or 'peakpowersector' in cols[2].lower()):
            continue
        
        
        col_to_plot_frame = dataframe[column]
        
        col_to_plot_frame = col_to_plot_frame.sort_values(ascending=False)
        
        load = list(col_to_plot_frame.values)
        
        unit = get_unit(cols[2])
        
        fig = plt.figure(figsize = (10,7))
        plt.plot(xValues, load, color=cols[5])
        plt.xlabel("Time in h")
        plt.ylabel(cols[0] + '_' + cols[1] + ' in ' + unit)
        plt.xlim(0, lenTimesteps)
        
        if('lsc' in cols[3].lower()):
            filename = directory + '/' + cols[2] + '/' + '_loadcurve/' + cols[3] + "/" + cols[0] + '_' + cols[1] + '.png'
        else:
            filename = directory + '/' + cols[2] + '/' + '_loadcurve/' + cols[3] + '_' + cols[4] + "/" + cols[0] + '_' + cols[1] + '.png'
        plt.savefig(filename)
        plt.close(fig)
        
    
    
            
def plot_violinSector(*args, **kwargs):
    

    #Vehicle Objects
    sectorname = kwargs.get("sectorname", 0)
    dfs = kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    df_out = kwargs.get("dataframeOut", 0)
    df_in = kwargs.get("dataframeIn", 0)
    directory = kwargs.get("directory", 0)
    
    if(consumers==0):
        return 0
    
    
    for consumer in consumers:
        df=dfs.parse(consumer)
        
        #List with all column names
        columns = list(df.columns)
        collistIn=[]
        collistOut=[]
        
        #select all columns with a sectorname
        waterdemandNotAdded=True
        for column in columns:
            colsplit=column.split('_')
            if('waterdemandsector' in sectorname.lower() and waterdemandNotAdded): 
                collistIn.append('Waterpoolpurchase_OUT_Potablewatersector_' + consumer + '_gold')
                collistOut.append('Waterflexibility2pool_OUT_Waterpoolsector_LSC_1_darkcyan')
                waterdemandNotAdded=False
            elif(sectorname in column and 'lsc' in colsplit[3].lower() and ('2lsc' in column.lower() or 'coolinglsc' in column.lower()) and 'out' in colsplit[1].lower()):
                collistOut.append(column)
            elif(sectorname in column and 'lsc' in colsplit[3].lower() and ('lsc2' in column.lower() ) and 'in' in colsplit[1].lower()):
                collistIn.append(column)
                
      
        valuesOutNew=[]
        valuesOut = df[collistOut].values
        for value in valuesOut:
            valuesOutNew.append(value[0])
            
        valuesInNew=[]
        valuesIn = df[collistIn].values
        for value in valuesIn:
            valuesInNew.append(value[0])
        
        df_out[consumer.split('_')[1]] = valuesOutNew
        df_in[consumer.split('_')[1]] = valuesInNew
    
    

    plt.figure(figsize = (10,7))
    plt.title('Consumer to LSC ' + sectorname, fontsize=20)
    sns.violinplot(data=df_out, palette=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.xlabel('Consumer', fontsize=14)
    unit = get_unit(sectorname)
    plt.ylabel('Shared commodity in ' + unit, fontsize=14)
    #plt.savefig('heatmap.png')
    
    filenameOut = directory + '/' + '_violin_lsc/casual/' + 'lscConsumerShare_' + sectorname + '_OUT' + ".png"
    plt.savefig(filenameOut)
    
    df_out_new = df_out.replace(0, np.nan)
    plt.figure(figsize = (10,7))
    plt.title('Consumer to LSC no Zeros ' + sectorname, fontsize=20)
    sns.violinplot(data=df_out_new, palette=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.xlabel('Consumer', fontsize=14)
    unit = get_unit(sectorname)
    plt.ylabel('Shared commodity in ' + unit, fontsize=14)
    #plt.savefig('heatmap.png')
    
    filenameOut = directory + '/' + '_violin_lsc/nozeros/' + 'lscConsumerShareNOzero_' + sectorname + '_OUT' + ".png"
    plt.savefig(filenameOut)

    
    plt.figure(figsize = (10,7))
    plt.title('LSC to Consumer ' + sectorname, fontsize=20)
    sns.violinplot(data=df_in, palette=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.xlabel('Consumer')
    unit = get_unit(sectorname)
    plt.ylabel('Shared commodity in ' + unit, fontsize=14)
    #plt.savefig('heatmap.png')
    
    filenameIn = directory + '/' + '_violin_lsc/casual/' + 'lscConsumerShare_' + sectorname + '_IN' + ".png"
    plt.savefig(filenameIn)
    
    df_in_new = df_in.replace(0, np.nan)
    plt.figure(figsize = (10,7))
    plt.title('LSC to Consumer no Zeros ' + sectorname, fontsize=20)
    sns.violinplot(data=df_in_new, palette=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.xlabel('Consumer')
    unit = get_unit(sectorname)
    plt.ylabel('Shared commodity in ' + unit, fontsize=14)
    #plt.savefig('heatmap.png')
    
    filenameIn = directory + '/' + '_violin_lsc/nozeros/' + 'lscConsumerShareNOzero_' + sectorname + '_IN' + ".png"
    plt.savefig(filenameIn)
                
    
        
            
     
def plot_storages(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    colors=[]
    color_node=[]
    
    inshares=[]
    outshares=[]
    
    
    
    
    #Vehicle Objects
    sectors = kwargs.get("sectors", 0)
    dfs = kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    timesteps = kwargs.get("timesteps", 8784)
    frequency = kwargs.get("frequency", '24H')
    
    if(len(frequency)==2):
        divisor = int(frequency[0])
    elif(len(frequency)==3):
        divisor = int(frequency[0])*10+int(frequency[1])
    
    listTimeseries=[]
    
    for i in range(0, timesteps):
        if(i%int(divisor)==0):
            listTimeseries.append(i)
    
    if(sectors==0 or consumers==0):
        return 0
    
    for consumer in consumers:
        df=dfs.parse(consumer)
        
        #List with all column names
        columns = list(df.columns)
        df = df.resample(frequency, on=columns[0]).mean()
        columns.remove(columns[0])
        collist=[]
        #select all columns with a sectorname
        
        
        for column in columns:
            
            colsplit = column.split('_')
            if('soc' in colsplit[1].lower()):
                collist.append(column)
                
        for coll in collist:
            inseries=[]
            outseries=[]
            socseries=[]
            sector=''
            
            col2plot = coll.split('_')
            
            for column in columns:
                colsplit = column.split('_')
                if(col2plot[0] in colsplit[0] and 'in' in colsplit[1].lower()):
                    inseries=list(df[column].values)
                    sector=colsplit[2]
                elif(col2plot[0] in colsplit[0] and 'out' in colsplit[1].lower()):
                    outseries=list(df[column].values)
                elif(col2plot[0] in colsplit[0] and 'soc' in colsplit[1].lower()):
                    socseries=list(df[column].values)
            
                    
            
                   
            unit=get_unit(sector)
                    
            fig, axs = plt.subplots(3, 1, sharex=True, figsize=(20,14))
            fig.suptitle(consumer + '_' + col2plot[0], fontsize=36)
            axs[0].set_xlim([0, timesteps])
            axs[0].plot(listTimeseries, inseries, color='darkgreen')
            axs[0].set_ylabel('Input in ' + unit, fontsize=21)
            axs[1].plot(listTimeseries, outseries, color='red')
            axs[1].set_ylabel('Output in ' + unit, fontsize=21)
            axs[2].plot(listTimeseries, socseries)
            axs[2].set_xlabel('Time in h', fontsize=21)
            axs[2].set_ylabel('SOC in ' + unit, fontsize=21)
            
            filenameIn = directory + '/' + '_storage/' + consumer + '/' + col2plot[0] + '.png'
            fig.savefig(filenameIn, bbox_inches="tight")
            fig.clf()
            plt.close(fig)
            
            
def plot_waste_sludge_sankey(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    colors=[]
    color_node=[]
    
    inshares=[]
    outshares=[]
    
    dfs = kwargs.get("dataframes", 0)
    directory = kwargs.get("directory", 0)
    
    df = dfs.parse('LSC')
    
    columns = list(df.columns)
    
    wastein=0
    wastetreat=0
    wastedisposal=0
    wasteheat=0
    wasteelec=0
    
    sludgein=0
    sludgetreat=0
    sludgedisposal=0
    sludgeheat=0
    sludgeelec=0
    
    elecfixwashing=0
    waterwashing=0
    elecheatwashing=0
    thermheatwashing=0
    heatrecoverywashing=0
    
    
    for column in columns:
        colsplit = column.split('_')
        
        #Logic to get the components
        if('wastestoragedisposal' in colsplit[0].lower() and 'out' in colsplit[1].lower()):
            wastein = np.sum(list(df[column].values))
            
        if('wastedisposal' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            wastedisposal = np.sum(list(df[column].values))
            
        if('wastecombustion' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            wastetreat = np.sum(list(df[column].values))

            
        if('wastecombustion' in colsplit[0].lower() and 'out' in colsplit[1].lower() and 'electricity' in colsplit[2].lower()):
            wasteelec = np.sum(list(df[column].values))

            
        if('wastecombustion' in colsplit[0].lower() and 'out' in colsplit[1].lower() and 'heat' in colsplit[2].lower()):
            wasteheat = np.sum(list(df[column].values))
            
            
        #Logic to get the components
        if('sludgestoragetreatment' in colsplit[0].lower() and 'out' in colsplit[1].lower()):
            sludgein = np.sum(list(df[column].values))*1000
            
        if('sludgedisposal' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            sludgedisposal = np.sum(list(df[column].values))*1000
            
        if('sludgecombustion' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            sludgetreat = np.sum(list(df[column].values))*1000

            
        if('sludgecombustion' in colsplit[0].lower() and 'out' in colsplit[1].lower() and 'electricity' in colsplit[2].lower()):
            sludgeelec = np.sum(list(df[column].values))

            
        if('sludgecombustion' in colsplit[0].lower() and 'out' in colsplit[1].lower() and 'heat' in colsplit[2].lower()):
            sludgeheat = np.sum(list(df[column].values))

    #Logic to get the components
        if('elecfixwashingmaschine' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            elecfixwashing = np.sum(list(df[column].values))
            
        if('waterwashingmaschine' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            waterwashing = np.sum(list(df[column].values))
            
        if('elecheatwashingmaschine' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            elecheatwashing = np.sum(list(df[column].values))

            
        if('thermheatwashingmaschine' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
            thermheatwashing = np.sum(list(df[column].values))

            
        if('heatrecoverywashingmaschine' in colsplit[0].lower() and 'out' in colsplit[1].lower()):
            heatrecoverywashing = np.sum(list(df[column].values))

    
    #------------ Waste Combustion ---------------
    source=[0, 0, 1, 1]
    target=[1, 2, 3, 4]
    colors=['peru', 'lightgrey', 'deepskyblue', 'orangered']
    
    for sources in source:
        color_node.append("black")  
    
    value=[wastetreat, wastedisposal, wasteelec, wasteheat]
    label=['Wasteinput ' + str(round(wastein, 1)) + ' kg', 'Wastecombustion ' + str(round(wastetreat, 1)) + ' kg', 'Wastedisposal ' + str(round(wastedisposal, 1)) + ' kg', 'Electricity ' + str(round(wasteelec, 1)) + ' kWh', 'Heat ' + str(round(wasteheat, 1)) + ' kWh']
    link = dict(source=source, target=target, value=value, color=colors)
    node = dict(label=label, pad=50, thickness=5, color=color_node)
    
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title='Waste Treatment',
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory +  '/' + '_resource_energyrecovery/'  + "wasteRecovery.png")
    
    #------------ Sludge Combustion ---------------
    
    source=[0, 0, 1, 1]
    target=[1, 2, 3, 4]
    colors=['peru', 'lightgrey', 'deepskyblue', 'orangered']
    
    for sources in source:
        color_node.append("black")  
    
    value=[sludgetreat, sludgedisposal, sludgeelec, sludgeheat]
    label=['Sludgeinput ' + str(round(sludgein, 1)) + ' l', 'Sludgecombustion ' + str(round(sludgetreat, 1)) + ' l', 'Sludgedisposal ' + str(round(sludgedisposal, 1)) + ' l', 'Electricity ' + str(round(sludgeelec, 1)) + ' kWh', 'Heat ' + str(round(sludgeheat, 1)) + ' kWh']
    link = dict(source=source, target=target, value=value, color=colors)
    node = dict(label=label, pad=50, thickness=5, color=color_node)
    
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title='Sludge Treatment',
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory +  '/' + '_resource_energyrecovery/'  + "sludgeRecovery.png")
    
    
    #------------ Washing Maschine ---------------
    source=[1, 2, 3, 4, 0]
    target=[0, 0, 0, 0, 5]
    colors=['peru', 'lightgrey', 'deepskyblue', 'orangered', 'lightcoral']
    
    for sources in source:
        color_node.append("black")  
    
    value=[elecfixwashing, waterwashing, elecheatwashing, thermheatwashing, heatrecoverywashing]
    label=['Washingmaschine', 'ElecFix ' + str(round(elecfixwashing, 1)) + ' kWh', 'Water ' + str(round(waterwashing, 1)) + ' m³', 
           'ElecHeat ' + str(round(elecheatwashing, 1)) + ' kWh', 'ThermHeat ' + str(round(thermheatwashing, 1)) + ' kWh', 'Heatrecovery ' + str(round(heatrecoverywashing, 1)) + ' kWh']
    link = dict(source=source, target=target, value=value, color=colors)
    node = dict(label=label, pad=50, thickness=5, color=color_node)
    
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title='Washing Maschine',
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory +  '/' + '_resource_energyrecovery/'  + "washingRecovery.png")
    
    
def plot_transport(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    colors=[]
    color_node=[]
    
    inshares=[]
    outshares=[]
    listTimeseries=[]
    
    dfs = kwargs.get("dataframes", 0)
    consumers = kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    timesteps = kwargs.get("timesteps", 8784)
    
    for i in range(0, timesteps):
        listTimeseries.append(i)
    
    dfLSC = dfs.parse('LSC')
    
    carColumns = list(dfLSC.columns)
    carColumns.remove(carColumns[0])
    
    #Dataframe where all vehicles in the model are stored
    vehicle_dataframes=[]
    
    #Search all assigned vehicles
    for column in carColumns:
        colsplit = column.split('_')
        
        if('out' in colsplit[1].lower() and 'mobilitysector' in colsplit[2].lower()):
            vehicle_dataframes.append(dfLSC[column])
            
            
    for consumer in consumers:
        transport=pd.DataFrame()
        dfConsumer = dfs.parse(consumer)
        columns = list(dfConsumer.columns)
        columns.remove(columns[0])
        
        for column in columns:
            colsplit = column.split('_')
            if('in' in colsplit[1].lower() and 'mobilitysector' in colsplit[2].lower()):
                transport = (dfConsumer[column])
                
        if transport.empty:
            continue
        transportvalues = list(transport.values)
        
        #List of timeseries for vehicle-transport association
        vehicleCoverageBinary=pd.DataFrame()
        vehicleCoverageDistance=pd.DataFrame()
        for vehicle in vehicle_dataframes:
            vehiclevalues = list(vehicle.values)
            index=0
            #List for Transport coverage
            #if vehicle used for transport at timestep: element is 1, else 0
            list_vehicletransport_binary=[]
            list_vehicletransport_distance=[]
            for values in vehiclevalues:
                if(vehiclevalues[index]>0 and vehiclevalues[index] == transportvalues[index]):
                    list_vehicletransport_binary.append(1)
                    list_vehicletransport_distance.append(vehiclevalues[index])
                    
                else:
                    list_vehicletransport_binary.append(0)
                    list_vehicletransport_distance.append(0)
                    
                index+=1
                    
            #Anm: Always -2 for the Index (0=1, first row is the heading)
            vehicleCoverageBinary[vehicle.name.split('_')[0]] = list_vehicletransport_binary
            vehicleCoverageDistance[vehicle.name.split('_')[0]] = list_vehicletransport_distance
            
            
        vehicleCoverageDistanceXZero = vehicleCoverageDistance.replace(0, np.nan)
        
        
        #Plot binary timeseries
        fig, axs = plt.subplots(12, 1, sharex=True, figsize=(30,21))
        fig.suptitle('Vehicle for Transport Demand ' + consumer, fontsize=36)
        axs[0].set_xlim([0, timesteps])
        axs[0].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[0]].values), color='darkgreen')
        axs[0].set_ylabel(vehicleCoverageBinary.columns[0], fontsize=7)
        axs[1].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[1]].values), color='red')
        axs[1].set_ylabel(vehicleCoverageBinary.columns[1], fontsize=7)
        axs[2].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[2]].values), color='blue')
        axs[2].set_ylabel(vehicleCoverageBinary.columns[2], fontsize=7)
        axs[3].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[3]].values), color='cadetblue')
        axs[3].set_ylabel(vehicleCoverageBinary.columns[3], fontsize=7)
        axs[4].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[4]].values), color='brown')
        axs[4].set_ylabel(vehicleCoverageBinary.columns[4], fontsize=7)
        axs[5].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[5]].values), color='orange')
        axs[5].set_ylabel(vehicleCoverageBinary.columns[5], fontsize=7)
        axs[6].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[6]].values), color='crimson')
        axs[6].set_ylabel(vehicleCoverageBinary.columns[6], fontsize=7)
        axs[7].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[7]].values), color='grey')
        axs[7].set_ylabel(vehicleCoverageBinary.columns[7], fontsize=7)
        axs[8].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[8]].values), color='navy')
        axs[8].set_ylabel(vehicleCoverageBinary.columns[8], fontsize=7)
        axs[9].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[9]].values), color='olive')
        axs[9].set_ylabel(vehicleCoverageBinary.columns[9], fontsize=7)
        axs[10].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[10]].values), color='peru')
        axs[10].set_ylabel(vehicleCoverageBinary.columns[10], fontsize=7)
        axs[11].plot(listTimeseries, list(vehicleCoverageBinary[vehicleCoverageBinary.columns[11]].values), color='lightcoral')
        axs[11].set_ylabel(vehicleCoverageBinary.columns[11], fontsize=7)
        axs[11].set_xlabel('Time in h', fontsize=21)    
        
        filenameIn = directory + '/' + '_transport/timeseries_rides/' + consumer + '_binarytransport.png'
        fig.savefig(filenameIn, bbox_inches="tight")
        fig.clf()
        plt.close(fig)
        
        
        #Plot Binary Bar Charts

        fig = plt.figure(figsize =(40, 28))
        fig.suptitle('Number of Rides ' + consumer, fontsize=24)
        
        barlist = []
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[0]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[1]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[2]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[3]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[4]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[5]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[6]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[7]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[8]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[9]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[10]].values)))
        barlist.append(np.sum(list(vehicleCoverageBinary[vehicleCoverageBinary.columns[11]].values)))
        
    
        # Horizontal Bar Plot
        plt.bar(vehicleCoverageBinary.columns, barlist, color=['darkred', 'red', 'blue', 'cadetblue', 'brown', 'orange', 'crimson', 'grey', 'navy', 'olive', 'peru', 'lightcoral'])
        plt.ylabel("Number of rides per year")
        # Show Plot
    
        filenameIn = directory + '/' + '_transport/number_rides/' + consumer + '_binarynumbers.png'
    
        plt.savefig(filenameIn)
        plt.close(fig)
        
        
        
        #Plot transport pie chart
        plt.figure(figsize = (30,21))
        pielist = []
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[0]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[1]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[2]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[3]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[4]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[5]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[6]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[7]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[8]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[9]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[10]].values)))
        pielist.append(np.sum(list(vehicleCoverageDistance[vehicleCoverageDistance.columns[11]].values)))
        
        index=0
        pieLabel = []
        for column in vehicleCoverageDistance.columns:
            pieLabel.append(column + ': ' + str(round(pielist[index], 1)) + 'km')
            index+=1
        
        patches, texts = plt.pie(pielist, colors=['darkred', 'red', 'blue', 'cadetblue', 'brown', 'orange', 'crimson', 'grey', 'navy', 'olive', 'peru', 'lightcoral'])
        plt.legend(patches, pieLabel, bbox_to_anchor=(1.1, 1.1))
        
        figtitle = 'Transport demand coverage ' + consumer
        
        plt.suptitle(figtitle)
    
    
        filenameIn = directory + '/' + '_transport/vehicle_share/' + consumer + '_vehicleshare.png'
        plt.savefig(filenameIn)
        
        #Plot Violins without Zero
                   
        plt.figure(figsize = (30,21))
        plt.title('Distance distribution ' + consumer, fontsize=20)
        sns.violinplot(data=vehicleCoverageDistanceXZero, palette=['red', 'green', 'blue'])
        plt.xlabel('Vehicle')
        plt.ylabel('Distance in km', fontsize=14)
        #plt.savefig('heatmap.png')
    
        filenameIn = directory + '/' + '_transport/vehicle_distribution/' + consumer + '_vehicledistribution.png'
        plt.savefig(filenameIn)
        
        
        
        
        
            
def plot_peakpowerloadcurve(*args, **kwargs):
    dataframes=kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    
    for consumer in consumers:
        
        dataframe = dataframes.parse(consumer)
        columnlist = list(dataframe.columns.values)
    
        lenTimesteps = len(list(dataframe[columnlist[0]].values))
    
        xValues = list(range(1,lenTimesteps+1))
    
        columnlist.remove(columnlist[0])
    
        for column in columnlist:
            cols = column.split('_')
        
            #No load curve plot for storages
            if('peakpowersector' in cols[2].lower()):
        
                col_to_plot_frame = dataframe[column]
        
                col_to_plot_frame = col_to_plot_frame.sort_values(ascending=False)
        
                load = list(col_to_plot_frame.values)

        
                fig = plt.figure(figsize = (10,7))
                plt.plot(xValues, load, color=cols[5])
                plt.title("Peakpower " + consumer)
                plt.xlabel("Time in h")
                plt.ylabel('Peakload Electricitygrid in kW')
                plt.xlim(0, lenTimesteps)
        
           
                filename = directory + '/' + '_peakpower/' + consumer + '_peakpower.png'
                plt.savefig(filename)
                plt.close(fig)
                    
        
            
                
def plot_flexibilitybar(*args, **kwargs):
    dataframes=kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    
    waterflexLabel=[]
    waterflex=[]
    wasteflexLabel=[]
    wasteflex=[]
    
    for consumer in consumers:
        
        
        dataframe = dataframes.parse(consumer)
        columnlist = list(dataframe.columns.values)
    
    
        columnlist.remove(columnlist[0])
    
        for column in columnlist:
            cols = column.split('_')
        
            #No load curve plot for storages
            if('waterflexibility2pool' in cols[0].lower() and 'out' in cols[1].lower()):
                waterflex.append(np.sum(list(dataframe[column].values)))
                waterflexLabel.append(consumer)
                
            if('wasteflexibility' in cols[0].lower() and 'in' in cols[1].lower()):
                wasteflex.append(np.sum(list(dataframe[column].values)))
                wasteflexLabel.append(consumer)
                
                
    #Plot Binary Bar Charts

    fig = plt.figure(figsize =(20, 14))
    fig.suptitle('Water Flexibility', fontsize=16)
    # Horizontal Bar Plot
    plt.bar(waterflexLabel, waterflex, color=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.ylabel("Water Flexibility in m³")
    # Show Plot
    
    filenameIn = directory + '/' + '_flexibility/' + 'waterflexibility.png'
    
    plt.savefig(filenameIn)
    plt.close(fig)
    
    fig = plt.figure(figsize =(20, 14))
    fig.suptitle('Waste Flexibility', fontsize=16)
    # Horizontal Bar Plot
    plt.bar(wasteflexLabel, wasteflex, color=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru'])
    plt.ylabel("Waste Flexibility in kg")
    # Show Plot
    
    filenameIn = directory + '/' + '_flexibility/' + 'wasteflexibility.png'
    
    plt.savefig(filenameIn)
    plt.close(fig)
    
    
def plot_pvconsumption(*args, **kwargs):
    dataframes=kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    pv=[]
    consumer_pv=[]

    pv_recovery=[]
    consumer_recovery=[]
    
    colorPV=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru', 'grey']
    colorRecovery=['red', 'green', 'blue', 'orange', 'purple', 'brown', 'steelblue', 'gold', 'seagreen', 'darkcyan', 'crimson', 'darkolivegreen', 'peru', 'grey']
    index=0
    index1=0
    for consumer in consumers:
        pv_generation=0
        pv_feedin=0
        wasteRecovery=0
        sludgeRecovery=0
        
        dataframe = dataframes.parse(consumer)
        columnlist = list(dataframe.columns.values)
    
    
        columnlist.remove(columnlist[0])
    
        for column in columnlist:
            cols = column.split('_')
            
            #No load curve plot for storages
            if('pv' in cols[0].lower() and 'out' in cols[1].lower()):
                pv_generation = (np.sum(list(dataframe[column].values)))

            if('electricityfeedin' in cols[0].lower() and 'in' in cols[1].lower()):
                pv_feedin = (np.sum(list(dataframe[column].values)))
                
            if('sludgecombustion' in cols[0].lower() and 'out' in cols[1].lower() and 'electricity' in cols[2].lower()):
                sludgeRecovery = (np.sum(list(dataframe[column].values)))
                
            if('wastecombustion' in cols[0].lower() and 'out' in cols[1].lower() and 'electricity' in cols[2].lower()):
                wasteRecovery = (np.sum(list(dataframe[column].values)))
                
        if(pv_generation!=0):
            consumer_pv.append(consumer)
            pv.append((pv_generation-pv_feedin)/(pv_generation)*100)
        else:
            colorPV.remove(colorPV[index])
            index-=1
                
        if(pv_generation !=0 or wasteRecovery!=0 or sludgeRecovery!=0):
            consumer_recovery.append(consumer)
            pv_recovery.append((pv_generation+wasteRecovery+sludgeRecovery-pv_feedin)/(pv_generation+wasteRecovery+sludgeRecovery)*100)
        else:
            colorRecovery.remove(colorRecovery[index1])
            index1-=1
                
        index+=1
        index1+=1

                
                
    #Plot Binary Bar Charts

    fig = plt.figure(figsize =(20, 14))
    fig.suptitle('PV Own Consumption', fontsize=16)
    # Horizontal Bar Plot
    plt.bar(consumer_pv, pv, color=colorPV)
    plt.ylabel("PV own consumption in %")
    # Show Plot
    
    filenameIn = directory + '/' + '_own_consumption/' + 'pvconsumption.png'
    
    plt.savefig(filenameIn)
    plt.close(fig)
    
    fig = plt.figure(figsize =(20, 14))
    fig.suptitle('PV and energy recovery own consumption', fontsize=16)
    # Horizontal Bar Plot
    plt.bar(consumer_recovery, pv_recovery, color=colorRecovery)
    plt.ylabel("Generation own consumption in %")
    # Show Plot
    
    filenameIn = directory + '/' + '_own_consumption/' + 'energyrecoveryconsumption.png'
    
    plt.savefig(filenameIn)
    plt.close(fig)
                    
                      
    
def plot_transportdelay(*args, **kwargs):
    dataframes=kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    
    wasteDelayTot = []
    sludgeDelayTot = []
    
    colorsWaste=[]
    colorsSludge=[]
    
    consumerWaste=[]
    consumerSludge=[]

    for consumer in consumers:
        totalWasteDelay=0
        totalSludgeDelay=0
        wastein = []
        wasteout=[]
        sludgein=[]
        sludgeout=[]
        
        
        dataframe = dataframes.parse(consumer)
        columnlist = list(dataframe.columns.values)
    
    
        columnlist.remove(columnlist[0])
    
        for column in columnlist:
            cols = column.split('_')
            
            if('wastetransport' in cols[0].lower() and 'in' in cols[1].lower() and 'emissions' not in cols[0].lower()):
                wastein = list(dataframe[column].values)
            elif('wastetransport' in cols[0].lower() and 'out' in cols[1].lower()and 'emissions' not in cols[0].lower()):
                wasteout = list(dataframe[column].values)
            elif('sludgetransport' in cols[0].lower() and 'in' in cols[1].lower()and 'emissions' not in cols[0].lower()):
                sludgein = list(dataframe[column].values)
            elif('sludgetransport' in cols[0].lower() and 'out' in cols[1].lower()and 'emissions' not in cols[0].lower()):
                sludgeout = list(dataframe[column].values)
        
        
        
        if(len(wastein) !=0 and len(wasteout)!=0):
            indexWaste=0
             
            totalWasteDelayCount=False
            for waste in wastein:
                if(wastein[indexWaste]!=0):
                    totalWasteDelayCount=True
                        
                if(wasteout[indexWaste]==0):
                    
                    if(totalWasteDelayCount):
                        totalWasteDelay+=1
                else:
                    totalWasteDelayCount=False
                        
                indexWaste+=1
                
        
        if(totalWasteDelay!=0):
            wasteDelayTot.append(totalWasteDelay)
            consumerWaste.append(consumer)
            colorsWaste.append(get_color(consumer))
            
                
                
        if(len(sludgein) !=0 and len(sludgeout)!=0):
            indexSludge=0
            
            totalSludgeDelayCount=False
            for sludge in sludgein:
                if(sludgein[indexSludge]!=0):
                    totalSludgeDelayCount=True
                        
                if(sludgeout[indexSludge]==0):
                    if(totalSludgeDelayCount):
                        totalSludgeDelay+=1
                else:
                    totalSludgeDelayCount=False
                        
                indexSludge+=1
                
        if(totalSludgeDelay!=0):
            sludgeDelayTot.append(totalSludgeDelay)
            consumerSludge.append(consumer)
            colorsSludge.append(get_color(consumer))

    if(len(wasteDelayTot)>0):
        fig = plt.figure(figsize =(20, 14))
        fig.suptitle('Waste Transport Delay', fontsize=16)
        # Horizontal Bar Plot
        plt.bar(consumerWaste, wasteDelayTot, color=colorsWaste)
        plt.ylabel("Waste delay in h")
        # Show Plot
    
        filenameIn = directory + '/' + '_delay/' + 'wastedelay.png'
    
        plt.savefig(filenameIn)
        plt.close(fig)
        
    if(len(sludgeDelayTot)>0):
        fig = plt.figure(figsize =(20, 14))
        fig.suptitle('Sludge Transport Delay', fontsize=16)
        # Horizontal Bar Plot
        plt.bar(consumerSludge, sludgeDelayTot, color=colorsSludge)
        plt.ylabel("Sludge delay in h")
        # Show Plot
    
        filenameIn = directory + '/' + '_delay/' + 'sludgedelay.png'
    
        plt.savefig(filenameIn)
        plt.close(fig)
            
            
def plot_pool_sankey(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    
    color_node=[]
    

    
    source=[]
    target=[]
    values=[]
    labels=['Waterpool']
    number=1
    colors=[]
    
    sourceLSC=[]
    targetLSC=[]
    valuesLSC=[]
    labelsLSC=['Waterpool']
    numberLSC=1
    colorsLSC=[]
    
    dfs = kwargs.get("dataframes", 0)
    directory = kwargs.get("directory", 0)
    consumers=kwargs.get("consumers", 0)
    
    for consumer in consumers:
        df = dfs.parse(consumer)
    
        columns = list(df.columns)
    
        poolin=0
        poolout=0
    
        for column in columns:
            colsplit = column.split('_')
        
            
            #Logic to get the components
            if('waterflexibility2pool' in colsplit[0].lower() and 'out' in colsplit[1].lower()):
                poolout = round(np.sum(list(df[column].values)), 2)
                values.append(poolout)
                source.append(number)
                target.append(0)
                number+=1
                colors.append(get_color(consumer))
                labels.append(consumer + ': ' + str(poolout) + ' m³')
            
            if('waterpoolpurchase' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
                poolin = round(np.sum(list(df[column].values)), 2)
                values.append(poolin)
                source.append(0)
                target.append(number)
                number+=1
                colors.append(get_color(consumer))
                labels.append(consumer+ ': ' + str(poolin) + ' m³')
                
            if('poolwaterwasted' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
                poolout = round(np.sum(list(df[column].values)), 2)
                values.append(poolout)
                source.append(0)
                target.append(number)
                number+=1
                colors.append(get_color(consumer))
                labels.append('Wasted' + ': ' + str(poolout) + ' m³')
                
                valuesLSC.append(poolout)
                sourceLSC.append(0)
                targetLSC.append(numberLSC)
                numberLSC+=1
                colorsLSC.append('steelblue')
                labelsLSC.append('Wasted' + ': ' + str(poolout) + ' m³')
                
            if('waterpoolstorage' in colsplit[0].lower() and 'in' in colsplit[1].lower()):
                poolin = round(np.sum(list(df[column].values)), 2)

                valuesLSC.append(poolin)
                sourceLSC.append(numberLSC)
                targetLSC.append(0)
                numberLSC+=1
                colorsLSC.append('lightskyblue')
                labelsLSC.append('Pool_in' + ': ' + str(poolin) + ' m³')
                
            if('waterpoolstorage' in colsplit[0].lower() and 'out' in colsplit[1].lower()):
                poolout = round(np.sum(list(df[column].values)), 2)

                valuesLSC.append(poolout)
                sourceLSC.append(0)
                targetLSC.append(numberLSC)
                numberLSC+=1
                colorsLSC.append('deepskyblue')
                labelsLSC.append('Pool_out' + ': ' + str(poolout) + ' m³')
                
                
    for sources in source:
        color_node.append("black")        
     
    # Sankey for consumers
    link = dict(source=source, target=target, value=values, color=colors)
    node = dict(label=labels, pad=50, thickness=5, color=color_node)
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title='Waterpool Consumers',
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory + '/_waterpool/' + 'waterpool_consumer.png')
    
    
    # Total Sankey for LSC
    link = dict(source=sourceLSC, target=targetLSC, value=valuesLSC, color=colorsLSC)
    node = dict(label=labelsLSC, pad=50, thickness=5, color=color_node)
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title='Waterpool Total',
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory + '/_waterpool/' + 'waterpool_total.png')
            
            

def plot_scatter(*args, **kwargs):
    

    #Vehicle Objects
    sectorname = kwargs.get("sectorname", 0)
    dfs = kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    timesteps = kwargs.get("timesteps", 8784)
    listTimeseries=[]
    
    for i in range(0, timesteps):
        listTimeseries.append(i)
    
    if(consumers==0):
        return 0
    
    
    for consumer in consumers:
        df=dfs.parse(consumer)
        
        #List with all column names
        columns = list(df.columns)
        collistIn=[]
        collistOut=[]
        
        #select all columns with a sectorname
        waterdemandNotAdded=True
        for column in columns:
            colsplit=column.split('_')
            if('waterdemandsector' in sectorname.lower() and waterdemandNotAdded): 
                collistIn.append('Waterpoolpurchase_OUT_Potablewatersector_' + consumer + '_gold')
                collistOut.append('Waterflexibility2pool_OUT_Waterpoolsector_LSC_1_darkcyan')
                waterdemandNotAdded=False
            elif(sectorname in column and 'lsc' in colsplit[3].lower() and ('2lsc' in column.lower() or 'coolinglsc' in column.lower()) and 'out' in colsplit[1].lower()):
                collistOut.append(column)
            elif(sectorname in column and 'lsc' in colsplit[3].lower() and ('lsc2' in column.lower() ) and 'in' in colsplit[1].lower()):
                collistIn.append(column)
                
      
        valuesOutNew=[]
        valuesOut = df[collistOut].values
        for value in valuesOut:
            if(value[0]<0):
                valuesOutNew.append(0)
            else:
                valuesOutNew.append(round(value[0], 5))
            
        valuesInNew=[]
        valuesIn = df[collistIn].values
        for value in valuesIn:
            if(value[0]<0):
                valuesInNew.append(0)
            else:
                valuesInNew.append(round(value[0], 5))
            
        
        unit=get_unit(sectorname)
                    
        fig, axs = plt.subplots(2, 1, sharex=True, figsize=(20,14))
        fig.suptitle(consumer + '_Scatter_' + sectorname, fontsize=36)
        axs[0].set_xlim([0, timesteps])
        axs[0].scatter(listTimeseries, valuesInNew, color='darkgreen')
        axs[0].set_ylabel('LSC Purchase in ' + unit, fontsize=21)
        axs[1].scatter(listTimeseries, valuesOutNew, color='red')
        axs[1].set_ylabel('LSC Feedin in ' + unit, fontsize=21)
        axs[1].set_xlabel('Time in h', fontsize=21)
        
        filenameIn = directory + '/' + sectorname + '/' + '_scatter/' + sectorname + '_' + consumer + ".png"
        fig.savefig(filenameIn, bbox_inches="tight")
        fig.clf()
        plt.close(fig)
        
def plot_scatter_sorted(*args, **kwargs):
    

    #Vehicle Objects
    sectorname = kwargs.get("sectorname", 0)
    dfs = kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    timesteps = kwargs.get("timesteps", 8784)
    listTimeseries=[]
    
    for i in range(0, timesteps):
        listTimeseries.append(i)
    
    if(consumers==0):
        return 0
    
    
    for consumer in consumers:
        df=dfs.parse(consumer)
        
        #List with all column names
        columns = list(df.columns)
        collistIn=[]
        collistOut=[]
        
        #select all columns with a sectorname
        waterdemandNotAdded=True
        for column in columns:
            colsplit=column.split('_')
            if('waterdemandsector' in sectorname.lower() and waterdemandNotAdded): 
                collistIn.append('Waterpoolpurchase_OUT_Potablewatersector_' + consumer + '_gold')
                collistOut.append('Waterflexibility2pool_OUT_Waterpoolsector_LSC_1_darkcyan')
                waterdemandNotAdded=False
            elif(sectorname in column and 'lsc' in colsplit[3].lower() and ('2lsc' in column.lower() or 'coolinglsc' in column.lower()) and 'out' in colsplit[1].lower()):
                collistOut.append(column)
            elif(sectorname in column and 'lsc' in colsplit[3].lower() and ('lsc2' in column.lower() ) and 'in' in colsplit[1].lower()):
                collistIn.append(column)
                
      
        valuesOutNew=[]
        valuesOut = df[collistOut].values
        for value in valuesOut:
            if(value[0]<0):
                valuesOutNew.append(0)
            else:
                valuesOutNew.append(round(value[0], 5))
            
        valuesInNew=[]
        valuesIn = df[collistIn].values
        for value in valuesIn:
            if(value[0]<0):
                valuesInNew.append(0)
            else:
                valuesInNew.append(round(value[0], 5))
            
        valuesInNew.sort()
        valuesOutNew.sort()
        unit=get_unit(sectorname)
                    
        fig, axs = plt.subplots(2, 1, sharex=True, figsize=(20,14))
        fig.suptitle(consumer + '_Scatter_' + sectorname, fontsize=36)
        axs[0].set_xlim([0, timesteps])
        axs[0].scatter(listTimeseries, valuesInNew, color='darkgreen')
        axs[0].set_ylabel('LSC Purchase in ' + unit, fontsize=21)
        axs[1].scatter(listTimeseries, valuesOutNew, color='red')
        axs[1].set_ylabel('LSC Feedin in ' + unit, fontsize=21)
        axs[1].set_xlabel('Datapoint Number', fontsize=21)
        
        filenameIn = directory + '/' + sectorname + '/' + '_scatter_sorted/' + sectorname + '_' + consumer + ".png"
        fig.savefig(filenameIn, bbox_inches="tight")
        fig.clf()
        plt.close(fig)
 
        
 
def plot_sankey_emissions(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    source=[]
    target=[]
    value=[]
    colors=[]
    color_node=[]
    
    
    #Vehicle Objects
    sectorname = "Emissionsector"
    df = kwargs.get("dataframe", 0)
    consumer=kwargs.get("consumer", 0)
    unit='kg'
    directory = kwargs.get("directory", 0)
    
    if(sectorname==0):
        return 0
    
    #List with all column names
    columns = list(df.columns)
    
    #select all columns with a sectorname
    for column in columns:
        if(sectorname in column):
            collist.append(column)
    
    #Sourcelabel for sector
    label = ['']
    
    
    for column in collist:
        colsplit = column.split('_')
        source.append(number)
        target.append(0)
        label.append(labellogic(colsplit[0]) + ': \n' + str(round(np.sum(list(df[column].values)), 1)) + unit)
        value.append(np.sum(list(df[column].values)))
        colors.append(colsplit[5])
        number+=1
        

    totalValue=np.sum(value)
    label.append('Total ' + consumer + ' \n' + str(round(totalValue, 1)) + unit)
    source.append(0)
    target.append(number)
    value.append(totalValue)
     
    for sources in source:
        color_node.append("black")        
     
    # data to dict, dict to sankey
    link = dict(source=source, target=target, value=value, color=colors)
    node = dict(label=label, pad=50, thickness=5, color=color_node)
    data = go.Sankey(link=link, node=node)
    # plot
    fig = go.Figure(data=data)
    
    fig.update_layout(
    hovermode = 'x',
    title=sectorname + '\n' + consumer,
    font=dict(size = 12, color = 'black'),
    plot_bgcolor='white',
    paper_bgcolor='white'
    )
    
    fig.show()
    fig.write_image(directory + '/' + sectorname + '/' + '_sankey/' + sectorname + '_' + consumer + ".png")
    
def bar_emissions(*args, **kwargs):

    #Vehicle Objects
    sectorname = "Emissionsector"
    dfs = kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)
    
    consumerlabels=[]
    consumervalues=[]
    consumercolors=[]
    
    for consumer in consumers:
        collist=[]
        df=dfs.parse(consumer)
        
    
        #List with all column names
        columns = list(df.columns)
    
        #select all columns with a sectorname
        for column in columns:
            if(sectorname in column):
                collist.append(column)
    
        #Sourcelabel for sector
        value=[]
    
        
        for column in collist:
            colsplit = column.split('_')
            if('out' in colsplit[1].lower()):
                val2add=np.sum(list(df[column].values))
                value.append(val2add)
        
        
        totalVal = np.sum(value)
        
        consumerlabels.append(consumer)
        consumervalues.append(totalVal)
        consumercolors.append(get_color(consumer))
        
    #Bar Plot
    fig = plt.figure(figsize =(30, 21))
    fig.suptitle('Consumer Emissions: ' + str(round(np.sum(consumervalues), 2)) + 'kg', fontsize=24)
    
    # Horizontal Bar Plot
    plt.bar(consumerlabels, consumervalues, color=consumercolors)
    plt.ylabel("Emissions in kg CO2")
    # Show Plot
    
    filenameIn = directory + '/' + '_emissions/' + 'consumer_emissions.png'
    
    plt.savefig(filenameIn)
    plt.close(fig)
    
        
        
def plot_costs(*args, **kwargs):
    
    collist=[]
    
    #Startvalue for Sankey Plot
    number=1
    
    #Sources and target lists for sankey
    colors=[]
    color_node=[]
    
    inshares=[]
    outshares=[]
    
    
    #Vehicle Objects
    sectors = kwargs.get("sectors", 0)
    dfs = kwargs.get("dataframes", 0)
    consumers=kwargs.get("consumers", 0)
    directory = kwargs.get("directory", 0)

    
    
    if(sectors==0 or consumers==0):
        return 0
    
    
    consumerdict={}
    consumerdictLabel={}
    
    for consumer in consumers:        
        consumerdict[consumer]=[]
        consumerdictLabel[consumer]=[]
    
    for sector in sectors:
        lscSectorCosts=[]
        lscSectorCostsLabel=[]
        lscColorSector=[]
        
        if('electricity' in sector.lower()):
            sector='Electricity'
        
    
        for consumer in consumers:
        
            sectorCosts=[]
            sectorCostsLabel=[]
            colorSector=[]

            if('LSC' in consumer):
                sectorCosts=lscSectorCosts
                sectorCostsLabel=lscSectorCostsLabel
                colorSector=lscColorSector
        
        
            df=dfs.parse(consumer)
        
            #List with all column names
            columns = list(df.columns)
            columns.remove(columns[0])
            
            
            for column in columns:
                
                colsplit=column.split('_')
                
                if(sector.lower() in colsplit[2].lower()):
                    if('2lsc' in colsplit[0].lower() and 'lsc' in colsplit[3].lower() or 'lsc2' in colsplit[0].lower() and 'lsc' in colsplit[3].lower()):
                        if(round(np.sum(list(df[column].values)), 2)!=0):
                            lscSectorCosts.append(np.sum(list(df[column].values)))
                            lscSectorCostsLabel.append('C_' + consumer.split('_')[1])
                            lscColorSector.append(colsplit[5])
                    else:
                        if(round(np.sum(list(df[column].values)), 2)!=0):
                            sectorCosts.append(np.sum(list(df[column].values)))
                            if('gridsourceelectricity' in colsplit[0].lower()):
                                colsplit[0] = 'Grid'
                                sectorCostsLabel.append(colsplit[0])
                            elif('sludgecomb' in colsplit[0].lower()):
                                colsplit[0] = 'Sludgecomb.'
                                sectorCostsLabel.append(colsplit[0])
                            elif('wastecomb' in colsplit[0].lower()):
                                colsplit[0] = 'Wastecomb.'
                                sectorCostsLabel.append(colsplit[0])
                            elif('feedin' in colsplit[0].lower()):
                                colsplit[0] = 'Feedin'
                                sectorCostsLabel.append(colsplit[0])
                            elif('heatstorage' in colsplit[0].lower()):
                                colsplit[0] = 'Heatstore'
                                sectorCostsLabel.append(colsplit[0])
                            else:
                                sectorCostsLabel.append(colsplit[0] + '_' + colsplit[1])
                            
                            colorSector.append(colsplit[5])
                
            #Append the total sector costs
            sumSector = np.sum(sectorCosts)
            
            if(round(sumSector, 2)!=0):
                sectorCosts.append(sumSector)
                sectorCostsLabel.append('Total')
                consumerdict[consumer].append(sumSector)
                consumerdictLabel[consumer].append(sector)
                colorSector.append('hotpink')


                fig = plt.figure(figsize =(30, 21))
                # Horizontal Bar Plot
                fig.suptitle('Costs_' + consumer + '_' + sector + ': ' + str(round(sumSector, 2)) + '€', fontsize=24)
            
                plt.bar(sectorCostsLabel, sectorCosts, color=colorSector)
                plt.axhline(y=0, color='black')
                plt.xlabel('Technology', fontsize=16)
                plt.ylabel('Costs in €', fontsize=16)
    
                filenameIn = directory + '/' + '_costs/' + consumer + '/' + sector + ".png"
    
                plt.savefig(filenameIn)
                plt.close(fig)
     
    #Get the total costs for each consumer
    totalCosts=[]
    totalCostsLabel=[]
    totalColors=[]
    for consumer in consumers:
        plotlabels = consumerdictLabel[consumer]
        data = consumerdict[consumer]
        
        colorSector=[]
        for label in plotlabels:
            colorSector.append(get_color_unit(label))
            
        colorSector.append('hotpink')
        
        index=0
        lblPiePos=[]
        lblPieNeg=[]
        dataPiePos=[]
        dataPieNeg=[]
        colorPos=[]
        colorNeg=[]
        
        for lbl in plotlabels:
            unit = '€'
            if((round(data[index], 2))>=0):
                lblPiePos.append(plotlabels[index] + ' ' + str(round(data[index], 2)) + unit)
                dataPiePos.append(data[index])
                colorPos.append(colorSector[index])
            else:
                lblPieNeg.append(plotlabels[index] + ' ' + str(round(-data[index], 2)) + unit)
                dataPieNeg.append(-data[index])
                colorNeg.append(colorSector[index])
                
            index+=1
            
            
        #Pie Plot Positive
        fig = plt.figure(figsize =(14, 14))
        patches, texts = plt.pie(dataPiePos, colors=colorPos)
        plt.legend(patches, lblPiePos, bbox_to_anchor=(1.1, 1.1))
        figtitle = 'Costs_' + consumer + '_Total'
        plt.suptitle(figtitle, fontsize=24)
        filenameIn = directory + '/' + '_costs/' + consumer + '/' + 'totalcosts_pie' + ".png"
        plt.savefig(filenameIn)
        
        #Pie Plot Positive
        fig = plt.figure(figsize =(14, 14))
        patches, texts = plt.pie(dataPieNeg, colors=colorNeg)
        plt.legend(patches, lblPieNeg, bbox_to_anchor=(1.1, 1.1))
        figtitle = 'Revenues_' + consumer + '_Total'
        plt.suptitle(figtitle, fontsize=24)
        filenameIn = directory + '/' + '_costs/' + consumer + '/' + 'totalrevenues_pie' + ".png"
        plt.savefig(filenameIn)
        
        
        plotlabels.append('Total')
        sumData = np.sum(data)
        data.append(sumData)
        
        totalCosts.append(sumData)
        totalCostsLabel.append(consumer)
        totalColors.append(get_color(consumer))
        #Plot Consumer Plots
        
        fig = plt.figure(figsize =(30, 21))
        # Horizontal Bar Plot
        fig.suptitle('Costs_' + consumer + '_Total' + ': ' + str(round(sumData, 2)) + '€', fontsize=24)
            
        plt.bar(plotlabels, data, color=colorSector)
        plt.axhline(y=0, color='black')
        plt.xlabel('Sector', fontsize=16)
        plt.ylabel('Costs in €', fontsize=16)
    
        filenameIn = directory + '/' + '_costs/' + consumer + '/' + 'total' + ".png"
    
        plt.savefig(filenameIn)
        plt.close(fig)
        
    
    index=0
    lblPiePos=[]
    lblPieNeg=[]
    dataPiePos=[]
    dataPieNeg=[]
    colorPos=[]
    colorNeg=[]
    
    totalColors.append('hotpink')
        
    for lbl in totalCostsLabel:
        unit = '€'
        if((round(totalCosts[index], 2))>=0):
            lblPiePos.append(totalCostsLabel[index] + ' ' + str(round(totalCosts[index], 2)) + unit)
            dataPiePos.append(totalCosts[index])
            colorPos.append(totalColors[index])
        else:
            lblPieNeg.append(totalCostsLabel[index] + ' ' + str(round(-totalCosts[index], 2)) + unit)
            dataPieNeg.append(-totalCosts[index])
            colorNeg.append(totalColors[index])
                
        index+=1
        
    #Pie Plot
    fig = plt.figure(figsize =(14, 14))
    patches, texts = plt.pie(dataPiePos, colors=colorPos)
    plt.legend(patches, lblPiePos, bbox_to_anchor=(1.1, 1.1))
    figtitle = 'Costs_Total'
    plt.suptitle(figtitle, fontsize=24)
    filenameIn = directory + '/' + '_costs/' + 'totalcosts_pie' + ".png"
    plt.savefig(filenameIn)
    
    fig = plt.figure(figsize =(14, 14))
    patches, texts = plt.pie(dataPieNeg, colors=colorNeg)
    plt.legend(patches, lblPieNeg, bbox_to_anchor=(1.1, 1.1))
    figtitle = 'Revenues_Total'
    plt.suptitle(figtitle, fontsize=24)
    filenameIn = directory + '/' + '_costs/' + 'totalrevenues_pie' + ".png"
    plt.savefig(filenameIn)
        
    sumTotal = np.sum(totalCosts)
    totalCosts.append(sumTotal)
    totalCostsLabel.append('Total')
    
    
    
    fig = plt.figure(figsize =(30, 21))
    # Horizontal Bar Plot
    fig.suptitle('Costs_Total' + ': ' + str(round(sumTotal, 2)) + '€', fontsize=24)
    plt.rcParams.update({'font.size':15})        
    plt.bar(totalCostsLabel, totalCosts, color=totalColors)
    plt.axhline(y=0, color='black')
    plt.xlabel('Consumer', fontsize=24)
    plt.ylabel('Costs in €', fontsize=24)
    
    filenameIn = directory + '/' + '_costs/' + 'total' + ".png"
    
    plt.savefig(filenameIn)
    plt.close(fig)
    
    #Plots for Total
    
    

