# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 13:50:12 2021

@author: Matthias Maldet
"""

import pandas as pd

#import pyomo.core as pyomo
#from datetime import datetime
import oemof.solph as solph
import numpy as np
import pyomo.environ as po
import pyomo.core as pyomo
import time
import electricity_components as el_comp
import heat_components as heat_comp
import gas_components as gas_comp
import cooling_components as cool_comp
import waste_components as waste_comp
import water_components as water_comp
import hydrogen_components as hydro_comp
import transport_components as trans_comp
import emission_components as em_comp
import Consumer
import constraints_add_binary as constraints
import transformerLosses as tl
import plots as plots
import os as os
import copy
import methods as meth



# -------------- INPUT PARAMETERS TO BE SET -------------- 

#ScenarioName for Data
scenario = 'valid_noGas'

#Filename
filename = "Input_Model_Hour.xlsx"

#Timestep for Optimisation
deltaT = 1

#Timesteps Considered
timesteps = 1
timestepsTotal = 8784

#Number Consumers Sewage to aggregate Sewage Sludge for Treatment - default value is 1
number_consumers_sewage = 1

#Solver for optimisation
solver = 'gurobi'

#Change to False if not the first run
firstrun = True 

# -------------- INPUT PARAMETERS TO BE SET -------------- 


#scenario = 'results/' + scenario
scenario = os.path.join('results', scenario)

if not os.path.exists(scenario):
    os.makedirs(scenario)


#global variables for testing
consumers_number = 1


yearWeightFactor=timesteps/timestepsTotal



#Recycling Parameters
H_wasteRecycled = 3.4
H_wasteNotRecycled = 3.2




#Get Frequency
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'

#Begin Oemof Optimisation
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
    #Define Energy System
my_energysystem = solph.EnergySystem(timeindex=my_index)

    
#adding busses to the energy system which represent the energy sectors
#Some are represented as buses for modelling purposes, even if they are not own energy sectors



consumer1 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx')
consumer1.addAllSectors()
consumer1.addSector('testsector')
TESTSTESTE = consumer1.getSector('testsector')
consumer1.sector2system(my_energysystem)





#___________ ELECTRICITY ____________________

#Grid     
#elgrid = el_comp.Grid(sector=consumer1.getElectricitySector())        
#my_energysystem.add(elgrid.component())
#consumer1.addComponent(elgrid)

elgrid = el_comp.GridEmissions(sector=consumer1.getElectricitySector(), sector_emissions=consumer1.getEmissionsSector())        
my_energysystem.add(elgrid.component())
consumer1.addComponent(elgrid)

#Grid Purchase
#elgrid = el_comp.GridPurchase(sector=consumer1.getElectricitySector(), costs_energy=consumer1.input_data['electricityCosts'], timesteps=timesteps)        
#my_energysystem.add(elgrid.component())
#consumer1.addComponent(elgrid)

#elgrid = el_comp.GridPurchaseEmissions(sector=consumer1.getElectricitySector(), sector_emissions=consumer1.getEmissionsSector(), costs_energy=consumer1.input_data['electricityCosts'], timesteps=timesteps)        
#my_energysystem.add(elgrid.component())
#consumer1.addComponent(elgrid)


#Grid Sink
#elgridSink = el_comp.GridSink(sector=consumer1.getElectricitySector())        
#my_energysystem.add(elgridSink.component())
#consumer1.addComponent(elgridSink)

#Grid Feedin
#elgridFeedin = el_comp.GridFeedin(sector=consumer1.getElectricitySector(), revenues=consumer1.input_data['electricityFeedin'], timesteps=timesteps)        
#my_energysystem.add(elgridFeedin.component())
#consumer1.addComponent(elgridFeedin)

#Heatpump
heatpump = el_comp.Heatpump(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector(), 
                            conversion_timeseries=consumer1.input_data['copHP'], timesteps=timesteps, P_in=0) 
my_energysystem.add(heatpump.component())
consumer1.addComponent(heatpump)


#Power to Heat
p2h = el_comp.Power2Heat(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector())                      
my_energysystem.add(p2h.component())
consumer1.addComponent(p2h)

#Battery
battery = el_comp.Battery(sector_in=consumer1.getElectricitySector())
my_energysystem.add(battery.component())
consumer1.addComponent(battery)

#Groundwaterwell
groundwaterwell = el_comp.Groundwaterwell(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getPotablewaterSector())
my_energysystem.add(groundwaterwell.component())
consumer1.addComponent(groundwaterwell)


#Electrolysis
electrolysis = el_comp.Electrolysis(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getHydrogenSector(), sector_loss=consumer1.getPotablewaterSector())
my_energysystem.add(electrolysis.component())
consumer1.addComponent(electrolysis)

#Electric Cooling
elcool = el_comp.ElectricCooling(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getCoolingSector(), 
                                 timesteps=timesteps, conversion_timeseries=consumer1.input_data['copCool'])
my_energysystem.add(elcool.component())
consumer1.addComponent(elcool)


#Photovoltaic
pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], timesteps=timesteps)
my_energysystem.add(pv.component())
consumer1.addComponent(pv)

#Demand
elecdemand = el_comp.Demand(sector=consumer1.getElectricitySector(), demand_timeseries=consumer1.input_data['electricityDemand'])
my_energysystem.add(elecdemand.component())
consumer1.addComponent(elecdemand)



#___________ HEAT ____________________

#Thermal Cooling
thermalCooling = heat_comp.ThermalCooling(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getCoolingSector())
my_energysystem.add(thermalCooling.component())
consumer1.addComponent(thermalCooling)

#Heat Storage
thermalStorage = heat_comp.ThermalStorage(sector_in=consumer1.getHeatSector())
my_energysystem.add(thermalStorage.component())
consumer1.addComponent(thermalStorage)

#Hot Water Boiler
hotwaterBoiler = heat_comp.HotwaterBoiler(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getHotwaterSector(), sector_loss=consumer1.getPotablewaterSector())
my_energysystem.add(hotwaterBoiler.component())
consumer1.addComponent(hotwaterBoiler)


#District Heat Grid
#dhgrid = heat_comp.Grid(sector=consumer1.getHeatSector())        
#my_energysystem.add(dhgrid.component())
#consumer1.addComponent(dhgrid)

dhgrid = heat_comp.GridEmissions(sector=consumer1.getHeatSector(), sector_emissions=consumer1.getEmissionsSector())        
my_energysystem.add(dhgrid.component())
consumer1.addComponent(dhgrid)


#District Heat Grid Sink
dhgridSink = heat_comp.GridSink(sector=consumer1.getHeatSector())        
my_energysystem.add(dhgridSink.component())
consumer1.addComponent(dhgridSink)

#District Heat Grid Purchase
#dhgrid = heat_comp.GridPurchase(sector=consumer1.getHeatSector(), costs_energy=consumer1.input_data['heatCosts'], timesteps=timesteps)        
#my_energysystem.add(dhgrid.component())
#consumer1.addComponent(dhgrid)

#dhgrid = heat_comp.GridPurchaseEmissions(sector=consumer1.getHeatSector(), sector_emissions=consumer1.getEmissionsSector(), costs_energy=consumer1.input_data['heatCosts'], timesteps=timesteps)        
#my_energysystem.add(dhgrid.component())
#consumer1.addComponent(dhgrid)


#District Heat Grid Feedin
#dhgridFeedin = el_comp.GridFeedin(sector=consumer1.getHeatSector(), revenues=consumer1.input_data['heatFeedin'], timesteps=timesteps)        
#my_energysystem.add(dhgridFeedin.component())
#consumer1.addComponent(dhgridFeedin)

#Demand Heat
heatdemand = heat_comp.Demand(sector=consumer1.getHeatSector(), demand_timeseries=consumer1.input_data['heatDemand'])
my_energysystem.add(heatdemand.component())
consumer1.addComponent(heatdemand)

#Demand Hotwater
hotwaterdemand = heat_comp.Hotwaterdemand(sector=consumer1.getHotwaterSector(), demand_timeseries=consumer1.input_data['hotwaterDemand'])
my_energysystem.add(hotwaterdemand.component())
consumer1.addComponent(hotwaterdemand)

"""
#___________ Gas ____________________

#Gas Grid
gasgrid = gas_comp.Grid(sector=consumer1.getGasSector())
my_energysystem.add(gasgrid.component())
consumer1.addComponent(gasgrid)

#Gas Grid Purchase
#gasgridPurchase = gas_comp.GridPurchase(sector=consumer1.getGasSector(), costs_energy=consumer1.input_data['gasCosts'], timesteps=timesteps)        
#my_energysystem.add(gasgridPurchase.component())
#consumer1.addComponent(gasgridPurchase)

#Demand
gasdemand = gas_comp.Demand(sector=consumer1.getGasSector(), demand_timeseries=consumer1.input_data['gasDemand'])
my_energysystem.add(gasdemand.component())
consumer1.addComponent(gasdemand)

#Gasboiler
gasboiler = gas_comp.Gasboiler(sector_in=consumer1.getGasSector(), sector_out=consumer1.getHeatSector(), sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(gasboiler.component())
consumer1.addComponent(gasboiler)

#Blockheat
blockheat = gas_comp.Blockheat(sector_in=consumer1.getGasSector(), sector_out=consumer1.getElectricitySector(), sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(blockheat.component())
consumer1.addComponent(blockheat)

#Gas CHP
gasCHP = gas_comp.CHP(sector_in=consumer1.getGasSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()], sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(gasCHP.component())
consumer1.addComponent(gasCHP)
"""

#___________ Waste ____________________

#Accruing
wasteAccruing = waste_comp.Accruing(sector=consumer1.getWasteSector(), waste_timeseries=consumer1.input_data['waste'])
my_energysystem.add(wasteAccruing.component())
consumer1.addComponent(wasteAccruing)

#Waste Storage
wasteStorage = waste_comp.WasteStorage(sector_in=consumer1.getWasteSector(), sector_out=consumer1.getWastedisposalSector(), disposal_periods=2, timesteps=timesteps)
my_energysystem.add(wasteStorage.component())
consumer1.addComponent(wasteStorage)

#Wastestorage Sink
#wastestorageSink = waste_comp.WastestorageSink(sector=consumer1.getWastestorageSector())        
#my_energysystem.add(wastestorageSink.component())
#consumer1.addComponent(wastestorageSink)


#Wastecombustion
wasteCombustion = waste_comp.Wastecombustion(sector_in=consumer1.getWastedisposalSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()], sector_emissions = consumer1.getEmissionsSector())
my_energysystem.add(wasteCombustion.component())
consumer1.addComponent(wasteCombustion)

"""
#Waste to Biogas
wasteBiogas = waste_comp.WasteBiogas(sector_in=consumer1.getWastedisposalSector(), sector_out=consumer1.getGasSector())
my_energysystem.add(wasteBiogas.component())
consumer1.addComponent(wasteBiogas)
"""

#Waste to Hydrogen AD
wasteADH2 = waste_comp.WasteHydrogenAD(sector_in=consumer1.getWastedisposalSector(), sector_out=consumer1.getHydrogenSector())
my_energysystem.add(wasteADH2.component())
consumer1.addComponent(wasteADH2)


#Waste Fermentation
wasteFermentation = waste_comp.Fermentation(sector_in=consumer1.getWastedisposalSector(), sector_out=consumer1.getHydrogenSector())
my_energysystem.add(wasteFermentation.component())
consumer1.addComponent(wasteFermentation)


#Waste Biofuel
wasteBiofuel = waste_comp.WasteBiofuel(sector_in=consumer1.getWastedisposalSector(), sector_out=[consumer1.getBiofuelSector(), consumer1.getSewageSector()])
my_energysystem.add(wasteBiofuel.component())
consumer1.addComponent(wasteBiofuel)

#Biofuel Sink
#biofuelSink = waste_comp.BiofuelSink(sector=consumer1.getBiofuelSector())        
#my_energysystem.add(biofuelSink.component())
#consumer1.addComponent(biofuelSink)


#Waste Disposal
#Emissions bei Disposal --> nur einrechnen wenn minimaler Kostenbetrieb geplant ist, und die Kosten nicht an einen "unbedeutenden" Drittanbieter verrrechnet werden
#wasteDisposal = waste_comp.Wastedisposal(sector=consumer1.getWastedisposalSector(), costs=consumer1.input_data['wasteCosts'])
wasteDisposal = waste_comp.Wastedisposal(sector=consumer1.getWastedisposalSector(), costs=consumer1.input_data['wasteCosts'], sector_emissions = consumer1.getEmissionsSector())        
my_energysystem.add(wasteDisposal.component())
consumer1.addComponent(wasteDisposal)

#Waste Market
#wasteMarket = waste_comp.Wastemarket(sector=consumer1.getWastedisposalSector(), revenues=consumer1.input_data['wasteMarket'])        
#my_energysystem.add(wasteMarket.component())
#consumer1.addComponent(wasteMarket)

#___________ Water ____________________

#Pipeline
#waterpipeline=water_comp.Pipeline(sector=consumer1.getPotablewaterSector())
#my_energysystem.add(waterpipeline.component())
#consumer1.addComponent(waterpipeline)


#Pipeline with Electricity Demand
waterpipelinepump=el_comp.Pipelinepump(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getPotablewaterSector())
my_energysystem.add(waterpipelinepump.component())
consumer1.addComponent(waterpipelinepump)

#Water Pipeline Purchase
#waterpipelinePurchase = water_comp.PipelinePurchase(sector=consumer1.getPotablewaterSector(), costs_water=consumer1.input_data['waterPipelineCosts'], timesteps=timesteps)        
#my_energysystem.add(waterpipelinePurchase.component())
#consumer1.addComponent(waterpipelinePurchase)

#Water Demand
#waterDemand = water_comp.Waterdemand(sector_in=consumer1.getPotablewaterSector(), sector_out=consumer1.getSewageSector(), demand=consumer1.input_data['waterDemand'], timesteps=timesteps)
#my_energysystem.add(waterDemand.component())
#consumer1.addComponent(waterDemand)

#Water Demand with Grey Water --> Standard Block for Flexibility
waterDemand = water_comp.WaterdemandReuse(sector_in=consumer1.getPotablewaterSector(), sector_out=consumer1.getSewageSector(), sector_greywater=consumer1.getGreywaterSector(), 
                                          sector_waterdemand=consumer1.getWaterdemandSector(), demand=consumer1.input_data['waterDemand'], timesteps=timesteps, share_greywater=0.75)
my_energysystem.add(waterDemand.component())
consumer1.addComponent(waterDemand)

#Water Demand total with greywater and blackwater --> required if share_greywater!=0
waterDemandTotal = water_comp.WaterDemandPotableGreySink(sector=consumer1.getWaterdemandSector(), demand=consumer1.input_data['waterDemand'], timesteps=timesteps)
my_energysystem.add(waterDemandTotal.component())
consumer1.addComponent(waterDemandTotal)

"""
#Sinks only required when no other flexibilities
#Sewage Sink
sewagesink = water_comp.SewageSink(sector=consumer1.getSewageSector())
my_energysystem.add(sewagesink.component())
consumer1.addComponent(sewagesink)

#Sludge Sink
sludgesink = water_comp.SludgeSink(sector=consumer1.getSludgestorageSector())
my_energysystem.add(sludgesink.component())
consumer1.addComponent(sludgesink)
"""

#Sewage Treatment Plant
sewageTreatmentPlant = water_comp.SewageTreatment(sector_in=consumer1.getSewageSector(), sector_loss=consumer1.getElectricitySector(), 
                                                  sector_out=[consumer1.getPotablewaterSector(), consumer1.getSludgeSector(), consumer1.getHeatSector()], 
                                                  sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(sewageTreatmentPlant.component())
consumer1.addComponent(sewageTreatmentPlant)

#Sewage H2
sewageH2 = water_comp.SewageHydrogen(sector_in=consumer1.getSewageSector(), sector_out=consumer1.getHydrogenSector(), timesteps=timesteps)
my_energysystem.add(sewageH2.component())
consumer1.addComponent(sewageH2)


#Water Storage
waterstorage = water_comp.WaterStorage(sector_in=consumer1.getPotablewaterSector())
my_energysystem.add(waterstorage.component())
consumer1.addComponent(waterstorage)

#Sludge Storage
sludgeStorage = water_comp.SludgeStorage(sector_in=consumer1.getSludgeSector(), sector_out=consumer1.getSludgestorageSector(), timesteps=timesteps)
my_energysystem.add(sludgeStorage.component())
consumer1.addComponent(sludgeStorage)


#Sludge Combustion
sludgecombustion = water_comp.Sludgecombustion(sector_in=consumer1.getSludgestorageSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()], sector_emissions=consumer1.getEmissionsSector())
my_energysystem.add(sludgecombustion.component())
consumer1.addComponent(sludgecombustion)

#MFC
sludgeMFC = water_comp.MFC(sector_in=consumer1.getSludgestorageSector(), sector_out=consumer1.getElectricitySector())
my_energysystem.add(sludgeMFC.component())
consumer1.addComponent(sludgeMFC)

"""
#Sludge Anaerobic Digestion
sludgeAD = water_comp.SludgeAD(sector_in=consumer1.getSludgestorageSector(), sector_out=consumer1.getGasSector())
my_energysystem.add(sludgeAD.component())
consumer1.addComponent(sludgeAD)
"""

#Sludge Anaerobic Digestion to Hydrogen
sludgeADH2 = water_comp.SludgeADH2(sector_in=consumer1.getSludgestorageSector(), sector_out=consumer1.getHydrogenSector())
my_energysystem.add(sludgeADH2.component())
consumer1.addComponent(sludgeADH2)


#Sewage Disposal
sewageDisposal = water_comp.Sewagedisposal(sector=consumer1.getSewageSector(), costs=consumer1.input_data['sewageCosts'], sector_emissions = consumer1.getEmissionsSector())        
my_energysystem.add(sewageDisposal.component())
consumer1.addComponent(sewageDisposal)

#Sludge Disposal
sludgeDisposal = water_comp.Sludgedisposal(sector=consumer1.getSludgestorageSector(), costs=consumer1.input_data['sludgeCosts'], sector_emissions = consumer1.getEmissionsSector())        
my_energysystem.add(sludgeDisposal.component())
consumer1.addComponent(sludgeDisposal)


#-------- Greywater Components -----------
#Only if greywater share != 0

#Grey Water Demand
greywaterDemand = water_comp.Greywaterdemand(sector_in=consumer1.getGreywaterSector(), 
                                          sector_out=consumer1.getWaterdemandSector(), 
                                          demand=consumer1.input_data['waterDemand'], timesteps=timesteps, min_greywater=0)
my_energysystem.add(greywaterDemand.component())
consumer1.addComponent(greywaterDemand)

#Grey Water to Sewage Disposal
greywaterSewage = water_comp.Greywatersewage(sector_in=consumer1.getGreywaterSector(), 
                                          sector_out=consumer1.getSewageSector(), 
                                          demand=consumer1.input_data['waterDemand'], timesteps=timesteps)
my_energysystem.add(greywaterSewage.component())
consumer1.addComponent(greywaterSewage)


#Greywater Storage
greywaterStorage = water_comp.GreywaterStorage(sector_in=consumer1.getGreywaterSector(), disposal_periods=24, timesteps=timesteps)
my_energysystem.add(greywaterStorage.component())
consumer1.addComponent(greywaterStorage)


#-------- Rainwater Components -----------
#Rainwater Collection
rainwater = water_comp.Rainwatercollect(sector=consumer1.getRainwaterSector(), generation_timeseries=consumer1.input_data['rainfall'], timesteps=timesteps)
my_energysystem.add(rainwater.component())
consumer1.addComponent(rainwater)

#Rainwater Tank
rainwatertank = water_comp.Rainwatertank(sector_in=consumer1.getRainwaterSector(), sector_out=consumer1.getRainwaterstorageSector())
my_energysystem.add(rainwatertank.component())
consumer1.addComponent(rainwatertank)

#Rainwater Disposal
rainwaterDisposal = water_comp.Rainwaterdisposal(sector=consumer1.getRainwaterstorageSector(), timesteps=timesteps)        
my_energysystem.add(rainwaterDisposal.component())
consumer1.addComponent(rainwaterDisposal)

#Rainwater Pump
rainwaterpump = water_comp.Rainwaterpump(sector_in=consumer1.getRainwaterstorageSector(), sector_out=consumer1.getGreywaterSector(), sector_loss=consumer1.getElectricitySector())
my_energysystem.add(rainwaterpump.component())
consumer1.addComponent(rainwaterpump)


#-------- Seawater Components -----------
#Seawater Source
seawater=water_comp.Seawatersource(sector=consumer1.getSeawaterSector())
my_energysystem.add(seawater.component())
consumer1.addComponent(seawater)

#Desalination
#Thermal Desalination
#desalination = water_comp.Desalination(sector_in=consumer1.getSeawaterSector(), sector_out=consumer1.getPotablewaterSector(), sector_loss=consumer1.getHeatSector(), energy=10)
#Electric Desalination
desalination = water_comp.Desalination(sector_in=consumer1.getSeawaterSector(), sector_out=consumer1.getPotablewaterSector(), sector_loss=consumer1.getElectricitySector())
my_energysystem.add(desalination.component())
consumer1.addComponent(desalination)

#___________ Cooling ____________________

#Demand
coolingdemand = cool_comp.Demand(sector=consumer1.getCoolingSector(), demand_timeseries=consumer1.input_data['coolingDemand'])
my_energysystem.add(coolingdemand.component())
consumer1.addComponent(coolingdemand)

#Grid
#coolgrid = cool_comp.Grid(sector=consumer1.getCoolingSector())        
#my_energysystem.add(coolgrid.component())
#consumer1.addComponent(coolgrid)

coolgrid = cool_comp.GridEmissions(sector=consumer1.getCoolingSector(), sector_emissions=consumer1.getEmissionsSector())        
my_energysystem.add(coolgrid.component())
consumer1.addComponent(coolgrid)

#Gridpurchase
#coolgrid = cool_comp.GridPurchase(sector=consumer1.getCoolingSector(), costs_energy=consumer1.input_data['coolingCosts'], timesteps=timesteps)        
#my_energysystem.add(coolgrid.component())
#consumer1.addComponent(coolgrid)

#coolgrid = cool_comp.GridPurchaseEmissions(sector=consumer1.getCoolingSector(), sector_emissions=consumer1.getEmissionsSector(), costs_energy=consumer1.input_data['coolingCosts'], timesteps=timesteps)        
#my_energysystem.add(coolgrid.component())
#consumer1.addComponent(coolgrid)


#___________ Hydrogen ____________________

#Demand
hydrogendemand = hydro_comp.Demand(sector=consumer1.getHydrogenSector(), demand_timeseries=consumer1.input_data['hydrogenDemand'])
my_energysystem.add(hydrogendemand.component())
consumer1.addComponent(hydrogendemand)

#Hydrogenboiler
hydrogenboiler = hydro_comp.Hydrogenboiler(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getHeatSector())
my_energysystem.add(hydrogenboiler.component())
consumer1.addComponent(hydrogenboiler)

#Methanation
methanation = hydro_comp.Methanation(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getGasSector())
my_energysystem.add(methanation.component())
consumer1.addComponent(methanation)


#Hydrogen Storage
hydrogenStorage = hydro_comp.Hydrogenstorage(sector_in=consumer1.getHydrogenSector())
my_energysystem.add(hydrogenStorage.component())
consumer1.addComponent(hydrogenStorage)

#Hydrogen CHP
hydrogenCHP = hydro_comp.CHP(sector_in=consumer1.getHydrogenSector(), sector_out=[consumer1.getElectricitySector(), consumer1.getHeatSector()])
my_energysystem.add(hydrogenCHP.component())
consumer1.addComponent(hydrogenCHP)

#Fuelcell
fuelcell = hydro_comp.Fuelcell(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getElectricitySector())
my_energysystem.add(fuelcell.component())
consumer1.addComponent(fuelcell)

"""
#Hydrogen to Gas Grid
hydrogenGasgrid = hydro_comp.HydrogenGasgrid(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getHydrogen2GasgridSector(), timesteps=timesteps)
my_energysystem.add(hydrogenGasgrid.component())
consumer1.addComponent(hydrogenGasgrid)
"""
#___________ Transport ____________________

#Mobility Demand
mobilitydemand = trans_comp.Demand(sector=consumer1.getTransportSector(), demand_timeseries=consumer1.input_data['transportDemand'])
my_energysystem.add(mobilitydemand.component())
consumer1.addComponent(mobilitydemand)

#Electric Vehicle
electricVehicle=trans_comp.ElectricVehicle(sector_in=consumer1.getElectricitySector(), sector_out=consumer1.getElectricVehicleSector(), sector_drive=consumer1.getTransportSector(), timesteps=timesteps)
my_energysystem.add(electricVehicle.component())
#my_energysystem.add(electricVehicle.sink())
my_energysystem.add(electricVehicle.drive())
consumer1.addComponent(electricVehicle)


#Biofuel Vehicle
biofuelVehicle=trans_comp.BiofuelVehicle(sector_in=consumer1.getBiofuelSector(), sector_out=consumer1.getBiofuelVehicleSector(), sector_drive=consumer1.getTransportSector(), timesteps=timesteps)
my_energysystem.add(biofuelVehicle.component())
#my_energysystem.add(biofuelVehicle.sink())
my_energysystem.add(biofuelVehicle.drive())
consumer1.addComponent(biofuelVehicle)


#Fuelcell Vehicle
fuelcellVehicle=trans_comp.FuelcellVehicle(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getFuelcellVehicleSector(), sector_drive=consumer1.getTransportSector(), timesteps=timesteps)
my_energysystem.add(fuelcellVehicle.component())
#my_energysystem.add(fuelcellVehicle.sink())
my_energysystem.add(fuelcellVehicle.drive())
consumer1.addComponent(fuelcellVehicle)


#Petrol Vehicle
petrolVehicle=trans_comp.PetrolVehicle(sector_in=consumer1.getPetrolSector(), sector_out=consumer1.getPetrolVehicleSector(), sector_drive=consumer1.getTransportSector(), timesteps=timesteps)
my_energysystem.add(petrolVehicle.component())
#my_energysystem.add(petrolVehicle.sink())
my_energysystem.add(petrolVehicle.drive())
consumer1.addComponent(petrolVehicle)


#External Petrol     
#externalPetrol = trans_comp.PetrolFuel(sector=consumer1.getPetrolSector())        
#my_energysystem.add(externalPetrol.component())
#consumer1.addComponent(externalPetrol)

#externalPetrol = trans_comp.PetrolFuelEmissions(sector=consumer1.getPetrolSector(), sector_emissions=consumer1.getEmissionsSector())        
#my_energysystem.add(externalPetrol.component())
#consumer1.addComponent(externalPetrol)

#Petrol Purchase
#petrolPurchase = trans_comp.PetrolPurchase(sector=consumer1.getPetrolSector(), costs=consumer1.input_data['petrolCosts'], timesteps=timesteps)        
#my_energysystem.add(petrolPurchase.component())
#consumer1.addComponent(petrolPurchase)

petrolPurchase = trans_comp.PetrolPurchaseEmissions(sector=consumer1.getPetrolSector(), sector_emissions=consumer1.getEmissionsSector(), costs=consumer1.input_data['petrolCosts'], timesteps=timesteps)        
my_energysystem.add(petrolPurchase.component())
consumer1.addComponent(petrolPurchase)



#___________ Emissions ____________________
emissions = em_comp.Emissions(sector=consumer1.getEmissionsSector())
my_energysystem.add(emissions.component())
consumer1.addComponent(emissions)


#___________ Additional Consumer ____________________



#Consumer 2
consumer2 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx', label='Consumer_2')
consumer2.addAllSectors()
consumer2.sector2system(my_energysystem)

#Grid     
elgrid2 = el_comp.Grid(sector=consumer2.getElectricitySector(), position=consumer2.label)        
my_energysystem.add(elgrid2.component())
consumer2.addComponent(elgrid2)

#Demand
elecdemand2 = el_comp.Demand(sector=consumer2.getElectricitySector(), demand_timeseries=consumer2.input_data['electricityDemand'], position='Consumer_2')
my_energysystem.add(elecdemand2.component())
consumer2.addComponent(elecdemand2)


print("----------------model set up----------------")

#___________ Solve Model ____________________

messi = solph.Model(my_energysystem) 


messi = constraints.transport_decision(electric_vehicle=electricVehicle, biofuel_vehicle=biofuelVehicle, 
                                       fuelcell_vehicle=fuelcellVehicle, petrol_vehicle=petrolVehicle, timesteps=timesteps, model=messi, demand_timeseries=consumer1.input_data['transportDemand'])

#get Objective Value
#messi.objective.value()

if(firstrun):

    if timesteps < 5:
        file2write = scenario + '/messi.lp'
        messi.write(file2write, io_options={'symbolic_solver_labels': True})
        print("----------------model written----------------")
    
    messi.solve(solver=solver, solve_kwargs={'tee': True})
    
    meth.store_results(my_energysystem, messi, scenario)


results = meth.restore_results(scenario)

print("----------------optimisation solved----------------")
    
#messi.solve(solver=solver, solve_kwargs={'tee': True})

#my_energysystem.results = solph.processing.results(messi)
    
#results = my_energysystem.results
resultcheck = solph.views.node(results, heatpump.label).get("sequences")

consumer1.sortComponents()



#___________ Plots ____________________
plots.create_folders(scenario, [consumer1, consumer2])



#Electricity Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity', color='dodgerblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='electricity', sectorname='Electricitysector', label='Electricity')


#Heat Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat', color='red')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='heat', sectorname='Heatsector', label='Heat')


#Gas Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas', color='lightgrey')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='gas', sectorname='Gassector', label='Gas')


#Waste Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', unit='kg')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste', color='saddlebrown')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='waste', sectorname='Wastedisposalsector', label='Waste')


#Water Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water', color='aqua')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Potablewatersector', label='Water')


plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage', color='lime')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sewagesector', label='Sewage')


plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge', color='slategrey')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='water', sectorname='Sludgestoragesector', label='Sludge')


plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Greywatersector', label='Greywater', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Greywatersector', label='Greywater', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Greywatersector', label='Greywater', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Greywatersector', label='Greywater', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Greywatersector', label='Greywater', color='lightblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Greywatersector', label='Water')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Greywatersector', label='Water')

plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwaterstoragesector', label='Rainwaterstorage', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwaterstoragesector', label='Rainwaterstorage', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwaterstoragesector', label='Rainwaterstorage', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwaterstoragesector', label='Rainwaterstorage', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwaterstoragesector', label='Rainwaterstorage', color='darkcyan')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwaterstoragesector', label='Water')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwaterstoragesector', label='Water')

plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Rainwater', color='deepskyblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Water')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='greywater', sectorname='Rainwatersector', label='Water')

plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='TotalWaterdemand', color='aliceblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='Water')
#plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='waterdemandTotal', sectorname='Waterdemandsector', label='Water')


#Cooling Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='cooling', sectorname='Coolingsector', label='Cooling', unit='kWh')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='cooling', sectorname='Coolingsector', label='Cooling', unit='kWh')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='cooling', sectorname='Coolingsector', label='Cooling', unit='kWh')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='cooling', sectorname='Coolingsector', label='Cooling', unit='kWh')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='cooling', sectorname='Coolingsector', label='Cooling', color='deepskyblue')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='cooling', sectorname='Coolingsector', label='Cooling')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='cooling', sectorname='Coolingsector', label='Cooling')



#Hydrogen Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', unit='m³')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen', color='limegreen')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='hydrogen', sectorname='Hydrogensector', label='Hydrogen')



#Transport Plots
plots.pie_transportShare(consumers=[consumer1], results=results, scenario=scenario)
plots.bar_transportShare(consumers=[consumer1], results=results, scenario=scenario)
plots.sankeyTransport(consumers=[consumer1], results=results, scenario=scenario)
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='transport', sectorname='Mobilitysector', label='Transport')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='transport', sectorname='Mobilitysector', label='Transport')

#CO2 Emission Plots
plots.pie_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='kg')
plots.pie_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='kg')
plots.bar_OutShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='kg')
plots.bar_InShare(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', unit='kg')
plots.sankey(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions', color='slategrey')
plots.anualLoadCurve(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions')
plots.timerows(consumers=[consumer1], results=results, scenario=scenario, folder='emissions', sectorname='Emissionsector', label='Emissions')

print("----------------plots done----------------")



