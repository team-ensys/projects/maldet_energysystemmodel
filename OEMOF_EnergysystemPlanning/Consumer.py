# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 07:56:02 2021

@author: Matthias Maldet
"""

import oemof.solph as solph
import pandas as pd
import numpy as np
import math
import methods as meth

class Consumer():
    
    sectors=[]
    components = []
    
    #Components
    
    componentList = []
    
    electricityComponents=[]
    heatComponents=[]
    gasComponents=[]
    wasteComponents=[]
    waterComponents=[]
    coolingComponents=[]
    hydrogenComponents=[]
    transportComponents=[]
    emissionComponents=[]
    emissionOutputComponents=[]
    
    #Service Components
    
    serviceComponentList = []
    
    electricityServiceComponents=[]
    heatServiceComponents=[]
    gasServiceComponents=[]
    wasteServiceComponents=[]
    waterServiceComponents=[]
    coolingServiceComponents=[]
    hydrogenServiceComponents=[]
    transportServiceComponents=[]

    
    componentsDict={}
    serviceComponentsDict={}
    
    #------------------------------------------------------------
    #Additional attributes - only relevant for LSC investigations 
    #------------------------------------------------------------
    
    #Limit for LSC water purchase - default 0.5
    waterLSCLimit = 0.5
    
    #Limit for pipeline water purchase - default 0.5
    waterPipelineLimit = 0.5
    
    def __init__(self, *args, **kwargs):
        
        consumerlabel = 'Consumer_1'
        timesteps = 8760
        filename_default = 'data.xlsx'
        self.components=[]
        self.sectors=[]
        self.electricityComponents=[]
        self.heatComponents=[]
        self.gasComponents=[]
        self.wasteComponents=[]
        self.waterComponents=[]
        self.coolingComponents=[]
        self.hydrogenComponents=[]
        self.transportComponents=[]
        self.label=kwargs.get("label", consumerlabel)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.filename = kwargs.get("filename", "none")
        self.dataframe_input=kwargs.get("data", 0)
        self.fixed_costs = 0
        self.willingness_for_waterflexibility=0
        self.willingness_for_waterflexibility_set=False
        self.willingness_for_recycling = 0
        self.willingness_for_recycling_set=False
        
        #Clustering extension
        self.input_data_clustered=0
        
        
        
        #Read the input File
        
        #Hier Änderung für Rollierende Optimierung
        if('none' in self.filename):
            demand=self.dataframe_input.parse(self.label, nrows=self.timesteps)
        else:
            demand = pd.read_excel(self.filename, sheet_name=self.label, index_col=0, nrows=self.timesteps)
        
        
        #Electricity Demand
        eldemand_cols = [col for col in demand.columns if 'Elec_Demand' in col]
        if(eldemand_cols):
            eldemand =[]
            eldemandOld=(demand[eldemand_cols].values)

            for i in eldemandOld:
                eldemand.append(i[0])
                
        else:
            eldemand = np.zeros(self.timesteps)
            
            
        #Heat Demand
        heatdemand_cols = [col for col in demand.columns if 'Heat_Demand' in col]
        if(heatdemand_cols):
            heatdemand =[]
            heatdemandOld=(demand[heatdemand_cols].values)

            for i in heatdemandOld:
                heatdemand.append(i[0])
                
        else:
            heatdemand = np.zeros(self.timesteps)
            
        #Hotwater Demand
        hotwaterdemand_cols = [col for col in demand.columns if 'Hotwater_Demand' in col]
        if(hotwaterdemand_cols):
            hotwaterdemand =[]
            hotwaterdemandOld=(demand[hotwaterdemand_cols].values)

            for i in hotwaterdemandOld:
                hotwaterdemand.append(i[0])
                
        else:
            hotwaterdemand = np.zeros(self.timesteps)
            
            
        #Gas Demand
        gasdemand_cols = [col for col in demand.columns if 'Gas_Demand' in col]
        if(gasdemand_cols):
            gasdemand =[]
            gasdemandOld=(demand[gasdemand_cols].values)

            for i in gasdemandOld:
                gasdemand.append(i[0])
        
        else:
            gasdemand = np.zeros(self.timesteps)
                
                
        #Waste Demand
        wastedemand_cols = [col for col in demand.columns if 'Waste' in col]
        if(wastedemand_cols):
            wastedemand =[]
            wastedemandOld=(demand[wastedemand_cols].values)

            for i in wastedemandOld:
                wastedemand.append(i[0])
                
        else:
            wastedemand = np.zeros(self.timesteps)
                
                
        #Water Demand
        waterdemand_cols = [col for col in demand.columns if 'Water_Demand' in col]
        if(waterdemand_cols):
            waterdemand =[]
            waterdemandOld=(demand[waterdemand_cols].values)

            for i in waterdemandOld:
                waterdemand.append(i[0]) 
                
        else:
            waterdemand = np.zeros(self.timesteps)
            
            
        #Cooling Demand
        coolingdemand_cols = [col for col in demand.columns if 'Cooling_Demand' in col]
        if(coolingdemand_cols):
            coolingdemand =[]
            coolingdemandOld=(demand[coolingdemand_cols].values)

            for i in coolingdemandOld:
                coolingdemand.append(i[0]) 
                
        else:
            coolingdemand = np.zeros(self.timesteps)
            
            
        #Hydrogen Demand
        hydrogendemand_cols = [col for col in demand.columns if 'Hydrogen_Demand' in col]
        if(hydrogendemand_cols):
            hydrogendemand =[]
            hydrogendemandOld=(demand[hydrogendemand_cols].values)

            for i in hydrogendemandOld:
                hydrogendemand.append(i[0]) 
                
        else:
            hydrogendemand = np.zeros(self.timesteps)
            
            
        #Transport Demand
        transportdemand_cols = [col for col in demand.columns if 'Transport_Demand' in col]
        if(transportdemand_cols):
            transportdemand =[]
            transportdemandOld=(demand[transportdemand_cols].values)

            for i in transportdemandOld:
                transportdemand.append(i[0]) 
                
        else:
            transportdemand = np.zeros(self.timesteps)
        
        #COP Wärmepumpe
        copHP_cols = [col for col in demand.columns if 'COP_HP' in col]
        if(copHP_cols):
            copHP =[]
            copHPold=(demand[copHP_cols].values)

            for i in copHPold:
                copHP.append(i[0])
                
        else:
            copHP = np.zeros(self.timesteps)



        #Electricity Generation
        elGen_cols = [col for col in demand.columns if 'EL_Gen' in col]
        if(elGen_cols):
            elGen=[]
            elGenold = (demand[elGen_cols].values)
            
            for i in elGenold:
                elGen.append(i[0])

        else:
            elGen = np.zeros(self.timesteps)
    
        #COP Kühlung
        copCool_cols = [col for col in demand.columns if 'COP_Cool' in col]
        if(copCool_cols):
            copCool = []
            copCoolold = (demand[copCool_cols].values)
            
            for i in copCoolold:
                copCool.append(i[0])

        else:
            copCool = np.zeros(self.timesteps)


        #Thermal Cooling
        thCool_cols = [col for col in demand.columns if 'Heat_Cool' in col]
        if(thCool_cols):
            thCool = []
            thCoolold = (demand[thCool_cols].values)
            
            for i in thCoolold:
                thCool.append(i[0])

        else:
            thCool = np.zeros(self.timesteps)
            
            
        #Rainfall Input
        rainfall_cols = [col for col in demand.columns if 'Rainfall' in col]
        if(rainfall_cols):
            rainfall = []
            rainfallOld = (demand[rainfall_cols].values)
            
            for i in rainfallOld:
                rainfall.append(i[0])

        else:
            rainfall = np.zeros(self.timesteps)
            
        #Costs electricity
        electricity_costs_col= [col for col in demand.columns if 'C_elEnergy' in col]
        if(electricity_costs_col):
            electricity_costs = []
            electricity_costs_old = (demand[electricity_costs_col].values)
            
            for i in electricity_costs_old:
                electricity_costs.append(i[0])

        else:
            electricity_costs = np.zeros(self.timesteps)
            
        #Feedin electricity
        electricity_feedin_cols= [col for col in demand.columns if 'P_elFeedIn' in col]
        if(electricity_feedin_cols):
            electricity_feedin = []
            electricity_feedin_old = (demand[electricity_feedin_cols].values)
            
            for i in electricity_feedin_old:
                electricity_feedin.append(i[0])

        else:
            electricity_feedin = np.zeros(self.timesteps)
            
            
        #Costs District Heat
        heat_costs_cols= [col for col in demand.columns if 'C_heatEnergy' in col]
        if(heat_costs_cols):
            heat_costs=[]
            heat_costs_old = (demand[heat_costs_cols].values)
            for i in heat_costs_old:
                heat_costs.append(i[0])

        else:
            heat_costs = np.zeros(self.timesteps)
            
        #Feedin Heat
        heat_feedin_cols= [col for col in demand.columns if 'P_heatFeedIn' in col]
        if(heat_feedin_cols):
            heat_feedin = []
            heat_feedin_old = (demand[heat_feedin_cols].values)
            
            for i in heat_feedin_old:
                heat_feedin.append(i[0])

        else:
            heat_feedin = np.zeros(self.timesteps)
            
        #Gas Costs
        gas_costs_cols= [col for col in demand.columns if 'C_gasEnergy' in col]
        if(gas_costs_cols):
            gas_costs=[]
            gas_costs_old = (demand[gas_costs_cols].values)
            
            for i in gas_costs_old:
                gas_costs.append(i[0])

        else:
            gas_costs = np.zeros(self.timesteps)
            
        #Waste Disposal Costs
        waste_costs_cols= [col for col in demand.columns if 'C_wastedisposal' in col]
        if(waste_costs_cols):
            waste_costs=[]
            waste_costs_old = (demand[waste_costs_cols].values)
            
            for i in waste_costs_old:
                waste_costs.append(i[0])

        else:
            waste_costs = np.zeros(self.timesteps)
            
        #Waste Market Price
        waste_market_cols= [col for col in demand.columns if 'P_wastemarket' in col]
        if(waste_market_cols):
            waste_market=[]
            waste_market_old = (demand[waste_market_cols].values)
            
            for i in waste_market_old:
                waste_market.append(i[0])

        else:
            waste_market = np.zeros(self.timesteps)
            
        #Water Pipeline Costs
        waterPipeline_costs_cols= [col for col in demand.columns if 'C_waterPipeline' in col]
        if(waterPipeline_costs_cols):
            waterPipeline_costs = []
            waterPipeline_costs_old = (demand[waterPipeline_costs_cols].values)
            
            for i in waterPipeline_costs_old:
                waterPipeline_costs.append(i[0])

        else:
            waterPipeline_costs = np.zeros(self.timesteps)
            
        #Water Sewage Costs
        waterSewage_costs_cols= [col for col in demand.columns if 'C_waterSewage' in col]
        if(waterSewage_costs_cols):
            waterSewage_costs=[]
            waterSewage_costs_old = (demand[waterSewage_costs_cols].values)
            for i in waterSewage_costs_old:
                waterSewage_costs.append(i[0])

        else:
            waterSewage_costs = np.zeros(self.timesteps)
            
        #Cooling Energy Costs
        cooling_costs_cols= [col for col in demand.columns if 'C_coolEnergy' in col]
        if(cooling_costs_cols):
            cooling_costs=[]
            cooling_costs_old = (demand[cooling_costs_cols].values)
            
            for i in cooling_costs_old:
                cooling_costs.append(i[0])

        else:
            cooling_costs = np.zeros(self.timesteps)
            
            
        #Cooling Feedin
        cooling_feedin_cols= [col for col in demand.columns if 'P_coolFeedIn' in col]
        if(cooling_feedin_cols):
            cooling_feedin=[]
            cooling_feedin_old = (demand[cooling_feedin_cols].values)
            
            for i in cooling_feedin_old:
                cooling_feedin.append(i[0])

        else:
            cooling_feedin = np.zeros(self.timesteps)
            
        
        #Revenues Hydrogen Market
        hydrogen_market_cols= [col for col in demand.columns if 'P_hydrogenmarket' in col]
        if(hydrogen_market_cols):
            hydrogen_market=[]
            hydrogen_market_old = (demand[hydrogen_market_cols].values)
            
            for i in hydrogen_market_old:
                hydrogen_market.append(i[0])

        else:
            hydrogen_market = np.zeros(self.timesteps)
            
            
        #Sludge Disposal
        sludge_costs_cols= [col for col in demand.columns if 'C_sludgeDisposal' in col]
        if(sludge_costs_cols):
            sludge_costs=[]
            sludge_costs_old = (demand[sludge_costs_cols].values)
            
            for i in sludge_costs_old:
                sludge_costs.append(i[0])

        else:
            sludge_costs = np.zeros(self.timesteps)
            
        #Petrol Costs
        petrol_costs_cols= [col for col in demand.columns if 'C_petrol' in col]
        if(petrol_costs_cols):
            petrol_costs=[]
            petrol_costs_old = (demand[petrol_costs_cols].values)
            
            for i in petrol_costs_old:
                petrol_costs.append(i[0])

        else:
            petrol_costs = np.zeros(self.timesteps)
            
            
        #Scarcity Factor
        scarcity_factor_cols= [col for col in demand.columns if 'F_scarcity' in col]
        if(scarcity_factor_cols):
            scarcity_factor=[]
            scarcity_factor_old = (demand[scarcity_factor_cols].values)
            
            for i in scarcity_factor_old:
                scarcity_factor.append(i[0])

        else:
            scarcity_factor = np.zeros(self.timesteps)
            
        
        #Washing Temperature
        washtemp_cols= [col for col in demand.columns if 'T_wash' in col]
        if(washtemp_cols):
            washtemp=[]
            washtemp_old = (demand[washtemp_cols].values)
            
            for i in washtemp_old:
                washtemp.append(i[0])

        else:
            washtemp = np.zeros(self.timesteps)
            
            
        #Costs for wastetransmission
        wastetransmission_cols= [col for col in demand.columns if 'C_wastetransmission' in col]
        if(wastetransmission_cols):
            wastetransmission=[]
            wastetransmission_old = (demand[wastetransmission_cols].values)
            
            for i in wastetransmission_old:
                wastetransmission.append(i[0])

        else:
            wastetransmission = np.zeros(self.timesteps)
            




        self.input_data = {'copHP' : copHP, 
                  'elGen' : elGen, 
                  'copCool' : copCool, 
                  'thCool' : thCool,
                  'rainfall' : rainfall,
                  'electricityCosts' : electricity_costs,
                  'electricityFeedin' : electricity_feedin,
                  'heatCosts' : heat_costs,
                  'heatFeedin' : heat_feedin,
                  'gasCosts' : gas_costs,
                  'wasteCosts' : waste_costs,
                  'wasteMarket' : waste_market,
                  'waterPipelineCosts' : waterPipeline_costs,
                  'sewageCosts' : waterSewage_costs,
                  'sludgeCosts' : sludge_costs,
                  'coolingCosts' : cooling_costs,
                  'coolingFeedin' : cooling_feedin,
                  'wastetransmissioncosts' : wastetransmission,
                  'hydrogenMarket' : hydrogen_market,
                  'petrolCosts' : petrol_costs,
                  'waterScarcity' : scarcity_factor,
                  'washTemperature' : washtemp,
                  'electricityDemand' : eldemand,
                  'heatDemand' : heatdemand,
                  'hotwaterDemand' : hotwaterdemand,
                  'gasDemand': gasdemand,
                  'waste' : wastedemand,
                  'waterDemand' : waterdemand,
                  'coolingDemand' : coolingdemand,
                  'hydrogenDemand': hydrogendemand,
                  'transportDemand' : transportdemand}
    
    def addSector(self, *args, **kwargs):
        self.sectorname=kwargs.get("sectorname", 'nosector')
        self.balanced=kwargs.get("balanced", True)
        sector = solph.Bus(label= self.label + "_" + self.sectorname, balanced=self.balanced)
        self.sectors.append(sector)
        
    def getSector(self, *args, **kwargs):
        self.sectorname=kwargs.get("sectorname", 'nosector')
        value = []
        
        for i in self.sectors:
            if self.sectorname in i.label:
                value.append(i)
                
        return value[0]
        
    
    def addAllSectors(self):
        electricity_sector = solph.Bus(label= self.label + "_Electricitysector", balanced=True)
        self.sectors.append(electricity_sector)
        heat_sector = solph.Bus(label=self.label + "_Heatsector", balanced=True)
        self.sectors.append(heat_sector)
        hotwater_sector = solph.Bus(label= self.label + "_HotWater", balanced=True)
        self.sectors.append(hotwater_sector)
        districtheating_sector = solph.Bus(label=self.label + "_Districtheatingsector", balanced=True)
        self.sectors.append(districtheating_sector)
        gas_sector = solph.Bus(label= self.label + "_Gassector", balanced=True)
        self.sectors.append(gas_sector)
        wasteaccruing_sector = solph.Bus(label= self.label + "_Wasteaccruingsector", balanced=True)
        self.sectors.append(wasteaccruing_sector)
        waste_sector = solph.Bus(label= self.label + "_Wastesector", balanced=True)
        self.sectors.append(waste_sector)
        wastestorage_sector = solph.Bus(label=self.label + "_Wastestoragesector", balanced=True)
        self.sectors.append(wastestorage_sector)
        wastedisposal_sector = solph.Bus(label=self.label + "_Wastedisposalsector", balanced=True)
        self.sectors.append(wastedisposal_sector)
        wastebio_sector = solph.Bus(label=self.label + "_Wastebiosector", balanced=True)
        self.sectors.append(wastebio_sector)
        emissionwastetruck_sector = solph.Bus(label=self.label + "_EmissionsWastetrucksector", balanced=True)
        self.sectors.append(emissionwastetruck_sector)
        biofuel_sector = solph.Bus(label=self.label + "_Biofuelsector", balanced=True)
        self.sectors.append(biofuel_sector)
        water_sector = solph.Bus(label=self.label + "_Potablewatersector", balanced=True)
        self.sectors.append(water_sector)
        greywater_sector = solph.Bus(label=self.label + "_Greywatersector", balanced=True)
        self.sectors.append(greywater_sector)
        rainwater_sector = solph.Bus(label=self.label + "_Rainwatersector", balanced=True)
        self.sectors.append(rainwater_sector)
        rainwaterstorage_sector = solph.Bus(label=self.label + "_Rainwaterstoragesector", balanced=True)
        self.sectors.append(rainwaterstorage_sector)
        seawater_sector = solph.Bus(label=self.label + "_Seawatersector", balanced=True)
        self.sectors.append(seawater_sector)
        waterdemand_sector = solph.Bus(label=self.label + "_Waterdemandsector", balanced=True)
        self.sectors.append(waterdemand_sector)
        sewage_sector = solph.Bus(label=self.label + "_Sewagesector", balanced=True)
        self.sectors.append(sewage_sector)
        sludge_sector = solph.Bus(label=self.label + "_Sludgesector", balanced=True)
        self.sectors.append(sludge_sector)
        sludgestorage_sector = solph.Bus(label=self.label + "_Sludgestoragesector", balanced=True)
        self.sectors.append(sludgestorage_sector)
        sludgetreatment_sectorIN = solph.Bus(label=self.label + "_SludgetreatmentsectorIN", balanced=True)
        self.sectors.append(sludgetreatment_sectorIN)
        sludgetreatment_sectorOUT = solph.Bus(label=self.label + "_SludgetreatmentsectorOUT", balanced=True)
        self.sectors.append(sludgetreatment_sectorOUT)
        emissionsludgetruck_sector = solph.Bus(label=self.label + "_EmissionsSludgetrucksector", balanced=True)
        self.sectors.append(emissionsludgetruck_sector)
        waterpool_sector = solph.Bus(label=self.label + "_Waterpoolsector", balanced=True)
        self.sectors.append(waterpool_sector)
        waterrecovery_sector = solph.Bus(label=self.label + "_Waterrecoverysector", balanced=True)
        self.sectors.append(waterrecovery_sector)
        waterwashing_sector = solph.Bus(label=self.label + "_Waterwashingsector", balanced=True)
        self.sectors.append(waterwashing_sector)
        cooling_sector = solph.Bus(label=self.label + "_Coolingsector", balanced=True)
        self.sectors.append(cooling_sector)
        hydrogen_sector = solph.Bus(label=self.label + "_Hydrogensector", balanced=True)
        self.sectors.append(hydrogen_sector)
        hydrogen2gasgrid_sector = solph.Bus(label=self.label + "_H2FeedInsector", balanced=False)
        self.sectors.append(hydrogen2gasgrid_sector)
        mobility_sector = solph.Bus(label=self.label + "_Mobilitysector", balanced=True)
        self.sectors.append(mobility_sector)
        electricvehicle_sector = solph.Bus(label=self.label + "_ElectricVehiclesector", balanced=True)
        self.sectors.append(electricvehicle_sector)
        biofuelvehicle_sector = solph.Bus(label=self.label + "_BiofuelVehiclesector", balanced=True)
        self.sectors.append(biofuelvehicle_sector)
        fuelcellvehicle_sector = solph.Bus(label=self.label + "_FuelcellVehiclesector", balanced=True)
        self.sectors.append(fuelcellvehicle_sector)
        petrolvehicle_sector = solph.Bus(label=self.label + "_PetrolVehiclesector", balanced=True)
        self.sectors.append(petrolvehicle_sector)
        petroleum_sector = solph.Bus(label=self.label + "_Petrolsector", balanced=True)
        self.sectors.append(petroleum_sector)
        peakpower_sector = solph.Bus(label=self.label + "_Peakpowersector", balanced=True)
        self.sectors.append(peakpower_sector)
        emission_sector = solph.Bus(label=self.label + "_Emissionsector", balanced=True)
        self.sectors.append(emission_sector)
        electricitygrid_sector = solph.Bus(label=self.label + "_Electricitygridsector", balanced=True)
        self.sectors.append(electricitygrid_sector)
        
    def addElectricitySector(self):
        electricity_sector = solph.Bus(label= self.label + "_Electricitysector", balanced=True)
        self.sectors.append(electricity_sector)
        peakpower_sector = solph.Bus(label=self.label + "_Peakpowersector", balanced=True)
        self.sectors.append(peakpower_sector)
        electricitygrid_sector = solph.Bus(label=self.label + "_Electricitygridsector", balanced=True)
        self.sectors.append(electricitygrid_sector)
        
        
    def addHeatSector(self):
        heat_sector = solph.Bus(label=self.label + "_Heatsector", balanced=True)
        self.sectors.append(heat_sector)
        hotwater_sector = solph.Bus(label= self.label + "_HotWater", balanced=True)
        self.sectors.append(hotwater_sector)
        
    def addDistrictheatingSector(self):
        districtheating_sector = solph.Bus(label=self.label + "_Districtheatingsector", balanced=True)
        self.sectors.append(districtheating_sector)
        
        
    def addGasSector(self):
        gas_sector = solph.Bus(label= self.label + "_Gassector", balanced=True)
        self.sectors.append(gas_sector)
        
    def addWasteSector(self):
        wasteaccruing_sector = solph.Bus(label= self.label + "_Wasteaccruingsector", balanced=True)
        self.sectors.append(wasteaccruing_sector)
        waste_sector = solph.Bus(label= self.label + "_Wastesector", balanced=True)
        self.sectors.append(waste_sector)
        wastestorage_sector = solph.Bus(label=self.label + "_Wastestoragesector", balanced=True)
        self.sectors.append(wastestorage_sector)
        wastedisposal_sector = solph.Bus(label=self.label + "_Wastedisposalsector", balanced=True)
        self.sectors.append(wastedisposal_sector)
        biofuel_sector = solph.Bus(label=self.label + "_Biofuelsector", balanced=True)
        self.sectors.append(biofuel_sector)
        emissionwastetruck_sector = solph.Bus(label=self.label + "_EmissionsWastetrucksector", balanced=True)
        self.sectors.append(emissionwastetruck_sector)
        
    def addWasteBioSector(self):
        wastebio_sector = solph.Bus(label=self.label + "_Wastebiosector", balanced=True)
        self.sectors.append(wastebio_sector)
        
    def addWaterSector(self):
        water_sector = solph.Bus(label=self.label + "_Potablewatersector", balanced=True)
        self.sectors.append(water_sector)
        greywater_sector = solph.Bus(label=self.label + "_Greywatersector", balanced=True)
        self.sectors.append(greywater_sector)
        rainwater_sector = solph.Bus(label=self.label + "_Rainwatersector", balanced=True)
        self.sectors.append(rainwater_sector)
        rainwaterstorage_sector = solph.Bus(label=self.label + "_Rainwaterstoragesector", balanced=True)
        self.sectors.append(rainwaterstorage_sector)
        seawater_sector = solph.Bus(label=self.label + "_Seawatersector", balanced=True)
        self.sectors.append(seawater_sector)
        waterdemand_sector = solph.Bus(label=self.label + "_Waterdemandsector", balanced=True)
        self.sectors.append(waterdemand_sector)
        sewage_sector = solph.Bus(label=self.label + "_Sewagesector", balanced=True)
        self.sectors.append(sewage_sector)
        sludge_sector = solph.Bus(label=self.label + "_Sludgesector", balanced=True)
        self.sectors.append(sludge_sector)
        sludgestorage_sector = solph.Bus(label=self.label + "_Sludgestoragesector", balanced=True)
        self.sectors.append(sludgestorage_sector)
        sludgetreatment_sectorIN = solph.Bus(label=self.label + "_SludgetreatmentsectorIN", balanced=True)
        self.sectors.append(sludgetreatment_sectorIN)
        sludgetreatment_sectorOUT = solph.Bus(label=self.label + "_SludgetreatmentsectorOUT", balanced=True)
        self.sectors.append(sludgetreatment_sectorOUT)
        emissionsludgetruck_sector = solph.Bus(label=self.label + "_EmissionsSludgetrucksector", balanced=True)
        self.sectors.append(emissionsludgetruck_sector)
        waterrecovery_sector = solph.Bus(label=self.label + "_Waterrecoverysector", balanced=True)
        self.sectors.append(waterrecovery_sector)
        waterwashing_sector = solph.Bus(label=self.label + "_Waterwashingsector", balanced=True)
        self.sectors.append(waterwashing_sector)
        
        
    def addWaterpoolSector(self):
        waterpool_sector = solph.Bus(label=self.label + "_Waterpoolsector", balanced=True)
        self.sectors.append(waterpool_sector)
    
    def addCoolingSector(self):
        cooling_sector = solph.Bus(label=self.label + "_Coolingsector", balanced=True)
        self.sectors.append(cooling_sector)
        
        
    def addHydrogenSector(self):
        hydrogen_sector = solph.Bus(label=self.label + "_Hydrogensector", balanced=True)
        self.sectors.append(hydrogen_sector)
        hydrogen2gasgrid_sector = solph.Bus(label=self.label + "H2FeedInsector", balanced=False)
        self.sectors.append(hydrogen2gasgrid_sector)
        
    def addTransportSector(self):
        mobility_sector = solph.Bus(label=self.label + "_Mobilitysector", balanced=True)
        self.sectors.append(mobility_sector)
        electricvehicle_sector = solph.Bus(label=self.label + "_ElectricVehiclesector", balanced=True)
        self.sectors.append(electricvehicle_sector)
        biofuelvehicle_sector = solph.Bus(label=self.label + "_BiofuelVehiclesector", balanced=True)
        self.sectors.append(biofuelvehicle_sector)
        fuelcellvehicle_sector = solph.Bus(label=self.label + "_FuelcellVehiclesector", balanced=True)
        self.sectors.append(fuelcellvehicle_sector)
        petrolvehicle_sector = solph.Bus(label=self.label + "_PetrolVehiclesector", balanced=True)
        self.sectors.append(petrolvehicle_sector)
        petroleum_sector = solph.Bus(label=self.label + "_Petrolsector", balanced=True)
        self.sectors.append(petroleum_sector)
        
    def addEmissionSector(self):
        emission_sector = solph.Bus(label=self.label + "_Emissionsector", balanced=True)
        self.sectors.append(emission_sector)
        
    def sector2system(self, energysystem):
        for i in self.sectors:
            energysystem.add(i)
            
            
    def addComponent(self, component):
        self.components.append(component)
        if hasattr(component, 'costs_fixed'):
            self.fixed_costs += component.costs_fixed
            
    def getElectricitySector(self):
        value = []
        
        for i in self.sectors:
            if 'Electricitysector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getElectricityGridSector(self):
        value = []
        
        for i in self.sectors:
            if 'Electricitygridsector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getPeakpowerSector(self):
        value = []
        
        for i in self.sectors:
            if 'Peakpower' in i.label:
                value.append(i)
                
        return value[0]
    
    def getHeatSector(self):
        value = []
        
        for i in self.sectors:
            if 'Heatsector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getDistrictheatingSector(self):
        value = []
        
        for i in self.sectors:
            if 'Districtheatingsector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getHotwaterSector(self):
        value = []
        
        for i in self.sectors:
            if 'HotWater' in i.label:
                value.append(i)
                
        return value[0]
    
    def getGasSector(self):
        value = []
        
        for i in self.sectors:
            if 'Gas' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWasteaccruingSector(self):
        value = []
        
        for i in self.sectors:
            if 'Wasteaccruingsector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWasteSector(self):
        value = []
        
        for i in self.sectors:
            if 'Wastesector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWasteBioSector(self):
        value = []
        
        for i in self.sectors:
            if 'Wastebiosector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWastestorageSector(self):
        value = []
        
        for i in self.sectors:
            if 'Wastestorage' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWastedisposalSector(self):
        value = []
        
        for i in self.sectors:
            if 'Wastedisposal' in i.label:
                value.append(i)
                
        return value[0]
    
    def getEmissionsWastetruckSector(self):
        value = []
        
        for i in self.sectors:
            if 'EmissionsWastetruck' in i.label:
                value.append(i)
                
        return value[0]
    
    def getBiofuelSector(self):
        value = []
        
        for i in self.sectors:
            if 'Biofuel' in i.label:
                value.append(i)
                
        return value[0]
    
    def getPotablewaterSector(self):
        value = []
        
        for i in self.sectors:
            if 'Potablewatersector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getGreywaterSector(self):
        value = []
        
        for i in self.sectors:
            if 'Greywatersector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getRainwaterSector(self):
        value = []
        
        for i in self.sectors:
            if 'Rainwatersector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getRainwaterstorageSector(self):
        value = []
        
        for i in self.sectors:
            if 'Rainwaterstoragesector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getSeawaterSector(self):
        value = []
        
        for i in self.sectors:
            if 'Seawatersector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWaterdemandSector(self):
        value = []
        
        for i in self.sectors:
            if 'Waterdemandsector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWaterpoolSector(self):
        value = []
        
        for i in self.sectors:
            if 'Waterpoolsector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getSewageSector(self):
        value = []
        
        for i in self.sectors:
            if 'Sewage' in i.label:
                value.append(i)
                
        return value[0]
    
    def getSludgeSector(self):
        value = []
        
        for i in self.sectors:
            if 'Sludgesector' in i.label:
                value.append(i)
                
                
        return value[0]
    
    def getSludgestorageSector(self):
        value = []
        
        for i in self.sectors:
            if 'Sludgestorage' in i.label:
                value.append(i)
                
        return value[0]
    
    
    def getSludgetreatmentSectorIn(self):
        value = []
        
        for i in self.sectors:
            if 'SludgetreatmentsectorIN' in i.label:
                value.append(i)
                
        return value[0]
    
    def getSludgetreatmentSectorOut(self):
        value = []
        
        for i in self.sectors:
            if 'SludgetreatmentsectorOUT' in i.label:
                value.append(i)
                
        return value[0]
    
    def getEmissionsSludgetruckSector(self):
        value = []
        
        for i in self.sectors:
            if 'EmissionsSludgetruck' in i.label:
                value.append(i)
                
        return value[0]
    
    def getWaterrecoverysector(self):
        value = []
        
        for i in self.sectors:
            if 'Waterrecoverysector' in i.label:
                value.append(i)
                
        return value[0]
    
    
    def getWaterwashingSector(self):
        value = []
        
        for i in self.sectors:
            if 'Waterwashingsector' in i.label:
                value.append(i)
                
        return value[0]
    
    
    def getCoolingSector(self):
        value = []
        
        for i in self.sectors:
            if 'Cooling' in i.label:
                value.append(i)
                
        return value[0]
    
    def getHydrogenSector(self):
        value = []
        
        for i in self.sectors:
            if 'Hydrogensector' in i.label:
                value.append(i)
                
        return value[0]
    
    def getHydrogen2GasgridSector(self):
        value = []
        
        for i in self.sectors:
            if 'H2FeedIn' in i.label:
                value.append(i)
                
        return value[0]
    
    def getTransportSector(self):
        value = []
        
        for i in self.sectors:
            if 'Mobility' in i.label:
                value.append(i)
                
        return value[0]
    
    def getElectricVehicleSector(self):
        value = []
        
        for i in self.sectors:
            if 'ElectricVehicle' in i.label:
                value.append(i)
                
        return value[0]
    
    def getBiofuelVehicleSector(self):
        value = []
        
        for i in self.sectors:
            if 'BiofuelVehicle' in i.label:
                value.append(i)
                
        return value[0]
    
    def getFuelcellVehicleSector(self):
        value = []
        
        for i in self.sectors:
            if 'FuelcellVehicle' in i.label:
                value.append(i)
                
        return value[0]
    
    def getPetrolVehicleSector(self):
        value = []
        
        for i in self.sectors:
            if 'PetrolVehicle' in i.label:
                value.append(i)
                
        return value[0]
    
    
    def getPetrolSector(self):
        value = []
        
        for i in self.sectors:
            if 'Petrolsector' in i.label:
                value.append(i)
                
        return value[0]
    
    
    def getEmissionsSector(self):
        value = []
        
        for i in self.sectors:
            if 'Emissionsector' in i.label:
                value.append(i)
                
        return value[0]
    
    
    def sortComponents(self):
        for i in self.components:
            

            if(hasattr(i, 'sector_emissions')):
                self.emissionOutputComponents.append(i)
            
            if hasattr(i, 'sector_in'):
                if('Electricity' in str(i.sector_in)):
                    self.electricityComponents.append(i)
                elif('ElectricVehicle' in str(i.sector_in)):
                    self.electricityComponents.append(i)
                elif('Potablewatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Heat' in str(i.sector_in)):
                    self.heatComponents.append(i)
                elif('Hot' in str(i.sector_in)):
                    self.heatComponents.append(i)
                elif('Gas' in str(i.sector_in)):
                    self.gasComponents.append(i)
                elif('Waste' in str(i.sector_in)):
                    self.wasteComponents.append(i)
                elif('Biofuelsector' in str(i.sector_in)):
                    self.wasteComponents.append(i)
                elif('Greywatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Rainwatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Seawatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Waterdemandsector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Sewage' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Sludge' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Cooling' in str(i.sector_in)):
                    self.coolingComponents.append(i)
                elif('Hydrogen' in str(i.sector_in)):
                    self.hydrogenComponents.append(i)
                elif('Transport' in str(i.sector_in)):
                    self.transportComponents.append(i)
                elif('Petrolsector' in str(i.sector_in)):
                    self.transportComponents.append(i)
                elif('Emissionsector' in str(i.sector_in)):
                    self.emissionComponents.append(i)
                
            elif hasattr(i, 'sector'):
                if('Electricity' in str(i.sector)):
                    self.electricityComponents.append(i)
                elif('ElectricVehicle' in str(i.sector)):
                    self.electricityComponents.append(i)
                elif('Potablewatersector' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Heat' in str(i.sector)):
                    self.heatComponents.append(i)
                elif('Hot' in str(i.sector)):
                    self.heatComponents.append(i)
                elif('Gas' in str(i.sector)):
                    self.gasComponents.append(i)
                elif('Waste' in str(i.sector)):
                    self.wasteComponents.append(i)
                elif('Biofuelsector' in str(i.sector)):
                    self.wasteComponents.append(i)
                elif('Greywatersector' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Rainwatersector' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Seawatersector' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Waterdemandsector' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Sewage' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Sludge' in str(i.sector)):
                    self.waterComponents.append(i)
                elif('Cooling' in str(i.sector)):
                    self.coolingComponents.append(i)
                elif('Hydrogen' in str(i.sector)):
                    self.hydrogenComponents.append(i)
                elif('Transport' in str(i.sector)):
                    self.transportComponents.append(i)
                elif('Petrolsector' in str(i.sector)):
                    self.transportComponents.append(i)
                elif('Emissionsector' in str(i.sector)):
                    self.emissionComponents.append(i)
                    
                    
            elif (not(hasattr(i, 'sector_out')) and hasattr(i , 'sector_in')):
                if('Electricity' in str(i.sector_in)):
                    self.electricityComponents.append(i)
                elif('ElectricVehicle' in str(i.sector_in)):
                    self.electricityComponents.append(i)
                elif('Potablewatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Heat' in str(i.sector_in)):
                    self.heatComponents.append(i)
                elif('Hot' in str(i.sector_in)):
                    self.heatComponents.append(i)
                elif('Gas' in str(i.sector_in)):
                    self.gasComponents.append(i)
                elif('Waste' in str(i.sector_in)):
                    self.wasteComponents.append(i)
                elif('Biofuelsector' in str(i.sector_in)):
                    self.wasteComponents.append(i)
                elif('Greywatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Rainwatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Seawatersector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Waterdemandsector' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Sewage' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Sludge' in str(i.sector_in)):
                    self.waterComponents.append(i)
                elif('Cooling' in str(i.sector_in)):
                    self.coolingComponents.append(i)
                elif('Hydrogen' in str(i.sector_in)):
                    self.hydrogenComponents.append(i)
                elif('Transport' in str(i.sector_in)):
                    self.transportComponents.append(i)
                elif('Petrolsector' in str(i.sector_in)):
                    self.transportComponents.append(i)
                    
            elif (not(hasattr(i, 'sector_in')) and hasattr(i , 'sector_out')):
                if('Electricity' in str(i.sector_out)):
                    self.electricityComponents.append(i)
                elif('ElectricVehicle' in str(i.sector_out)):
                    self.electricityComponents.append(i)
                elif('Potablewatersector' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Heat' in str(i.sector_out)):
                    self.heatComponents.append(i)
                elif('Hot' in str(i.sector_out)):
                    self.heatComponents.append(i)
                elif('Gas' in str(i.sector_out)):
                    self.gasComponents.append(i)
                elif('Waste' in str(i.sector_out)):
                    self.wasteComponents.append(i)
                elif('Biofuelsector' in str(i.sector_out)):
                    self.wasteComponents.append(i)
                elif('Greywatersector' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Rainwatersector' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Seawatersector' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Waterdemandsector' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Sewage' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Sludge' in str(i.sector_out)):
                    self.waterComponents.append(i)
                elif('Cooling' in str(i.sector_out)):
                    self.coolingComponents.append(i)
                elif('Hydrogen' in str(i.sector_out)):
                    self.hydrogenComponents.append(i)
                elif('Transport' in str(i.sector_out)):
                    self.transportComponents.append(i)
                elif('Petrolsector' in str(i.sector_out)):
                    self.transportComponents.append(i)
                    
                    
        
                    
        self.componentsDict={'Electricity' : self.electricityComponents,
                             'Heat' : self.heatComponents,
                             'Gas' : self.gasComponents,
                             'Waste' : self.wasteComponents,
                             'Water' : self.waterComponents,
                             'Cooling' : self.coolingComponents,
                             'Hydrogen' : self.hydrogenComponents,
                             'Transport' : self.transportComponents,
                             'Emission' : self.emissionComponents,
                             'EmissionOutput' : self.emissionOutputComponents}
        
        
    def sortServiceComponents(self):
        for i in self.components:
            if hasattr(i, 'sector_out'):
                if('Electricity' in str(i.sector_out)):
                    self.electricityServiceComponents.append(i)
                elif('ElectricVehicle' in str(i.sector_out)):
                    self.electricityServiceComponents.append(i)
                elif('Potablewatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Heat' in str(i.sector_out)):
                    self.heatServiceComponents.append(i)
                elif('Hot' in str(i.sector_out)):
                    self.heatServiceComponents.append(i)
                elif('Gas' in str(i.sector_out)):
                    self.gasServiceComponents.append(i)
                elif('Waste' in str(i.sector_out)):
                    self.wasteServiceComponents.append(i)
                elif('Biofuelsector' in str(i.sector_out)):
                    self.wasteServiceComponents.append(i)
                elif('Greywatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Rainwatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Seawatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Waterdemandsector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Sewage' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Sludge' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Cooling' in str(i.sector_out)):
                    self.coolingServiceComponents.append(i)
                elif('Hydrogen' in str(i.sector_out)):
                    self.hydrogenServiceComponents.append(i)
                elif('Transport' in str(i.sector_out)):
                    self.transportServiceComponents.append(i)
                elif('Petrolsector' in str(i.sector_out)):
                    self.transportServiceComponents.append(i)

                
            elif hasattr(i, 'sector'):
                if('Electricity' in str(i.sector)):
                    self.electricityServiceComponents.append(i)
                elif('ElectricVehicle' in str(i.sector)):
                    self.electricityServiceComponents.append(i)
                elif('Potablewatersector' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Heat' in str(i.sector)):
                    self.heatServiceComponents.append(i)
                elif('Hot' in str(i.sector)):
                    self.heatServiceComponents.append(i)
                elif('Gas' in str(i.sector)):
                    self.gasServiceComponents.append(i)
                elif('Waste' in str(i.sector)):
                    self.wasteServiceComponents.append(i)
                elif('Biofuelsector' in str(i.sector)):
                    self.wasteServiceComponents.append(i)
                elif('Greywatersector' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Rainwatersector' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Seawatersector' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Waterdemandsector' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Sewage' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Sludge' in str(i.sector)):
                    self.waterServiceComponents.append(i)
                elif('Cooling' in str(i.sector)):
                    self.coolingServiceComponents.append(i)
                elif('Hydrogen' in str(i.sector)):
                    self.hydrogenServiceComponents.append(i)
                elif('Transport' in str(i.sector)):
                    self.transportServiceComponents.append(i)
                elif('Petrolsector' in str(i.sector)):
                    self.transportServiceComponents.append(i)
                    
                    
            elif (not(hasattr(i, 'sector_out')) and hasattr(i , 'sector_in')):
                if('Electricity' in str(i.sector_in)):
                    self.electricityServiceComponents.append(i)
                elif('ElectricVehicle' in str(i.sector_in)):
                    self.electricityServiceComponents.append(i)
                elif('Potablewatersector' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Heat' in str(i.sector_in)):
                    self.heatServiceComponents.append(i)
                elif('Hot' in str(i.sector_in)):
                    self.heatServiceComponents.append(i)
                elif('Gas' in str(i.sector_in)):
                    self.gasServiceComponents.append(i)
                elif('Waste' in str(i.sector_in)):
                    self.wasteServiceComponents.append(i)
                elif('Biofuelsector' in str(i.sector_in)):
                    self.wasteServiceComponents.append(i)
                elif('Greywatersector' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Rainwatersector' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Seawatersector' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Waterdemandsector' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Sewage' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Sludge' in str(i.sector_in)):
                    self.waterServiceComponents.append(i)
                elif('Cooling' in str(i.sector_in)):
                    self.coolingServiceComponents.append(i)
                elif('Hydrogen' in str(i.sector_in)):
                    self.hydrogenServiceComponents.append(i)
                elif('Transport' in str(i.sector_in)):
                    self.transportServiceComponents.append(i)
                elif('Petrolsector' in str(i.sector_in)):
                    self.transportServiceComponents.append(i)
                    
            elif (not(hasattr(i, 'sector_in')) and hasattr(i , 'sector_out')):
                if('Electricity' in str(i.sector_out)):
                    self.electricityServiceComponents.append(i)
                elif('ElectricVehicle' in str(i.sector_out)):
                    self.electricityServiceComponents.append(i)
                elif('Potablewatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Heat' in str(i.sector_out)):
                    self.heatServiceComponents.append(i)
                elif('Hot' in str(i.sector_out)):
                    self.heatServiceComponents.append(i)
                elif('Gas' in str(i.sector_out)):
                    self.gasServiceComponents.append(i)
                elif('Waste' in str(i.sector_out)):
                    self.wasteServiceComponents.append(i)
                elif('Biofuelsector' in str(i.sector_out)):
                    self.wasteServiceComponents.append(i)
                elif('Greywatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Rainwatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Rainwaterstoragesector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Seawatersector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Waterdemandsector' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Sewage' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Sludge' in str(i.sector_out)):
                    self.waterServiceComponents.append(i)
                elif('Cooling' in str(i.sector_out)):
                    self.coolingServiceComponents.append(i)
                elif('Hydrogen' in str(i.sector_out)):
                    self.hydrogenServiceComponents.append(i)
                elif('Transport' in str(i.sector_out)):
                    self.transportServiceComponents.append(i)
                elif('Petrolsector' in str(i.sector_out)):
                    self.transportServiceComponents.append(i)

                    
        self.serviceComponentsDict={'Electricity' : self.electricityServiceComponents,
                             'Heat' : self.heatServiceComponents,
                             'Gas' : self.gasServiceComponents,
                             'Waste' : self.wasteServiceComponents,
                             'Water' : self.waterServiceComponents,
                             'Cooling' : self.coolingServiceComponents,
                             'Hydrogen' : self.hydrogenServiceComponents,
                             'Transport' : self.transportServiceComponents}
        
    
    def add2componentList(self):
        
        #Components
        if self.electricityComponents != 0:
            self.componentList.append(self.electricityComponents)
            
        if self.heatComponents != 0:
            self.componentList.append(self.heatComponents)
            
        if self.gasComponents != 0:
            self.componentList.append(self.gasComponents)
            
        if self.wasteComponents != 0:
            self.componentList.append(self.wasteComponents)
            
        if self.waterComponents != 0:
            self.componentList.append(self.waterComponents)
            
        if self.coolingComponents != 0:
            self.componentList.append(self.coolingComponents)
            
        if self.hydrogenComponents != 0:
            self.componentList.append(self.hydrogenComponents)
            
        if self.transportComponents != 0:
            self.componentList.append(self.transportComponents)
            
        if self.emissionComponents != 0:
            self.componentList.append(self.emissionComponents)
            
            
        #Service Components
        if self.electricityServiceComponents != 0:
            self.serviceComponentList.append(self.electricityServiceComponents)
            
        if self.heatServiceComponents != 0:
            self.serviceComponentList.append(self.heatServiceComponents)
            
        if self.gasServiceComponents != 0:
            self.serviceComponentList.append(self.gasServiceComponents)
            
        if self.wasteServiceComponents != 0:
            self.serviceComponentList.append(self.wasteServiceComponents)
            
        if self.waterServiceComponents != 0:
            self.serviceComponentList.append(self.waterServiceComponents)
            
        if self.coolingServiceComponents != 0:
            self.serviceComponentList.append(self.coolingServiceComponents)
            
        if self.hydrogenServiceComponents != 0:
            self.serviceComponentList.append(self.hydrogenServiceComponents)
            
        if self.transportServiceComponents != 0:
            self.serviceComponentList.append(self.transportServiceComponents)
            
            
    
    #Add a willingness for flexibility to the consumer based on probability distributions
    def get_willingness_for_waterflexibility(self, *args, **kwargs):
        
        if(self.willingness_for_waterflexibility_set==False):
            steprange = kwargs.get("steprange", 1000)
        
            saving = np.linspace(0, 0.5, steprange)
            probability = 39.62*np.exp(-6.208*saving)

            sumProbab = np.sum(probability)

            probability = np.multiply(probability, 1/sumProbab)

            self.willingness_for_waterflexibility = np.random.choice(saving, p=probability)
            
            #SET TO TRUE FOR STOCHASTICAL DETERMINATION FIXING
            self.willingness_for_waterflexibility_set=True
                    
            
        return self.willingness_for_waterflexibility
    
    def get_willingness_for_waterflexibility_costdependent(self, *args, **kwargs):
        
        if(self.willingness_for_waterflexibility_set==False):
            costs = kwargs.get("costs", 1.5)
            self.willingness_for_waterflexibility = -0.2095*math.pow(costs, 4) + 0.7762*math.pow(costs, 3) - 0.6119*math.pow(costs, 2) + 0.0452*costs - 3*math.pow(10, -11)
            self.willingness_for_waterflexibility_set=True
                    
            
        return self.willingness_for_waterflexibility
    
    
    #Add a willingness for flexibility to the consumer based on probability distributions
    def get_willingness_for_recycling(self, *args, **kwargs):
        
        if(self.willingness_for_recycling_set==False):
            steprange = kwargs.get("steprange", 1000)
        
            saving = np.linspace(0, 0.4, steprange)
            probability = -1083.3*np.power(saving, 3) + 816.67*np.power(saving, 2) + 40.833*saving + 5

            sumProbab = np.sum(probability)

            probability = np.multiply(probability, 1/sumProbab)

            self.willingness_for_recycling = np.random.choice(saving, p=probability)
            
            #SET TO TRUE FOR STOCHASTICAL DETERMINATION FIXING
            self.willingness_for_recycling_set=False
                    
            
        return self.willingness_for_recycling
    
    
    def get_willingness_for_recycling_costdependent(self, *args, **kwargs):
        
        if(self.willingness_for_recycling_set==False):
            costs = kwargs.get("costs", 0.000875)
        
            self.willingness_for_recycling = 6*math.pow(10,8)*math.pow(costs,3) - 576707 * math.pow(costs, 2) + 276.92*costs - math.pow(10, -13)
            self.willingness_for_recycling_set=True
                    
            
        return self.willingness_for_recycling
                    
    
    #get/set for LSC and pipeline purchase limit
    def setWaterLSCLimit(self, limit):
        self.waterLSCLimit=limit
        
    def getWaterLSCLimit(self):
        return self.waterLSCLimit
    
    
    def setWaterPipelineLimit(self, limit):
        self.waterPipelineLimit=limit
        
    def getWaterPipelineLimit(self):
        return self.waterPipelineLimit
    
    
    #clustering approach
    def clustering(self, *args, **kwargs):
        
        demand = kwargs.get("data", 0)
        

        #Electricity Demand
        eldemand_cols = [col for col in demand.columns if 'Elec_Demand' in col]
        if(eldemand_cols):
            eldemand =[]
            eldemandOld=(demand[eldemand_cols].values)

            for i in eldemandOld:
                eldemand.append(i[0])
                
        else:
            eldemand = np.zeros(self.timesteps)
            
            
        #Heat Demand
        heatdemand_cols = [col for col in demand.columns if 'Heat_Demand' in col]
        if(heatdemand_cols):
            heatdemand =[]
            heatdemandOld=(demand[heatdemand_cols].values)

            for i in heatdemandOld:
                heatdemand.append(i[0])
                
        else:
            heatdemand = np.zeros(self.timesteps)
            
        #Hotwater Demand
        hotwaterdemand_cols = [col for col in demand.columns if 'Hotwater_Demand' in col]
        if(hotwaterdemand_cols):
            hotwaterdemand =[]
            hotwaterdemandOld=(demand[hotwaterdemand_cols].values)

            for i in hotwaterdemandOld:
                hotwaterdemand.append(i[0])
                
        else:
            hotwaterdemand = np.zeros(self.timesteps)
            
            
        #Gas Demand
        gasdemand_cols = [col for col in demand.columns if 'Gas_Demand' in col]
        if(gasdemand_cols):
            gasdemand =[]
            gasdemandOld=(demand[gasdemand_cols].values)

            for i in gasdemandOld:
                gasdemand.append(i[0])
        
        else:
            gasdemand = np.zeros(self.timesteps)
                
                
        #Waste Demand
        wastedemand_cols = [col for col in demand.columns if 'Waste' in col]
        if(wastedemand_cols):
            wastedemand =[]
            wastedemandOld=(demand[wastedemand_cols].values)

            for i in wastedemandOld:
                wastedemand.append(i[0])
                
        else:
            wastedemand = np.zeros(self.timesteps)
                
                
        #Water Demand
        waterdemand_cols = [col for col in demand.columns if 'Water_Demand' in col]
        if(waterdemand_cols):
            waterdemand =[]
            waterdemandOld=(demand[waterdemand_cols].values)

            for i in waterdemandOld:
                waterdemand.append(i[0]) 
                
        else:
            waterdemand = np.zeros(self.timesteps)
            
            
        #Cooling Demand
        coolingdemand_cols = [col for col in demand.columns if 'Cooling_Demand' in col]
        if(coolingdemand_cols):
            coolingdemand =[]
            coolingdemandOld=(demand[coolingdemand_cols].values)

            for i in coolingdemandOld:
                coolingdemand.append(i[0]) 
                
        else:
            coolingdemand = np.zeros(self.timesteps)
            
            
        #Hydrogen Demand
        hydrogendemand_cols = [col for col in demand.columns if 'Hydrogen_Demand' in col]
        if(hydrogendemand_cols):
            hydrogendemand =[]
            hydrogendemandOld=(demand[hydrogendemand_cols].values)

            for i in hydrogendemandOld:
                hydrogendemand.append(i[0]) 
                
        else:
            hydrogendemand = np.zeros(self.timesteps)
            
            
        #Transport Demand
        transportdemand_cols = [col for col in demand.columns if 'Transport_Demand' in col]
        if(transportdemand_cols):
            transportdemand =[]
            transportdemandOld=(demand[transportdemand_cols].values)

            for i in transportdemandOld:
                transportdemand.append(i[0]) 
                
        else:
            transportdemand = np.zeros(self.timesteps)
        
        #COP Wärmepumpe
        copHP_cols = [col for col in demand.columns if 'COP_HP' in col]
        if(copHP_cols):
            copHP =[]
            copHPold=(demand[copHP_cols].values)

            for i in copHPold:
                copHP.append(i[0])
                
        else:
            copHP = np.zeros(self.timesteps)



        #Electricity Generation
        elGen_cols = [col for col in demand.columns if 'EL_Gen' in col]
        if(elGen_cols):
            elGen=[]
            elGenold = (demand[elGen_cols].values)
            
            for i in elGenold:
                elGen.append(i[0])

        else:
            elGen = np.zeros(self.timesteps)
    
        #COP Kühlung
        copCool_cols = [col for col in demand.columns if 'COP_Cool' in col]
        if(copCool_cols):
            copCool = []
            copCoolold = (demand[copCool_cols].values)
            
            for i in copCoolold:
                copCool.append(i[0])

        else:
            copCool = np.zeros(self.timesteps)


        #Thermal Cooling
        thCool_cols = [col for col in demand.columns if 'Heat_Cool' in col]
        if(thCool_cols):
            thCool = []
            thCoolold = (demand[thCool_cols].values)
            
            for i in thCoolold:
                thCool.append(i[0])

        else:
            thCool = np.zeros(self.timesteps)
            
            
        #Rainfall Input
        rainfall_cols = [col for col in demand.columns if 'Rainfall' in col]
        if(rainfall_cols):
            rainfall = []
            rainfallOld = (demand[rainfall_cols].values)
            
            for i in rainfallOld:
                rainfall.append(i[0])

        else:
            rainfall = np.zeros(self.timesteps)
            
        #Costs electricity
        electricity_costs_col= [col for col in demand.columns if 'C_elEnergy' in col]
        if(electricity_costs_col):
            electricity_costs = []
            electricity_costs_old = (demand[electricity_costs_col].values)
            
            for i in electricity_costs_old:
                electricity_costs.append(i[0])

        else:
            electricity_costs = np.zeros(self.timesteps)
            
        #Feedin electricity
        electricity_feedin_cols= [col for col in demand.columns if 'P_elFeedIn' in col]
        if(electricity_feedin_cols):
            electricity_feedin = []
            electricity_feedin_old = (demand[electricity_feedin_cols].values)
            
            for i in electricity_feedin_old:
                electricity_feedin.append(i[0])

        else:
            electricity_feedin = np.zeros(self.timesteps)
            
            
        #Costs District Heat
        heat_costs_cols= [col for col in demand.columns if 'C_heatEnergy' in col]
        if(heat_costs_cols):
            heat_costs=[]
            heat_costs_old = (demand[heat_costs_cols].values)
            for i in heat_costs_old:
                heat_costs.append(i[0])

        else:
            heat_costs = np.zeros(self.timesteps)
            
        #Feedin Heat
        heat_feedin_cols= [col for col in demand.columns if 'P_heatFeedIn' in col]
        if(heat_feedin_cols):
            heat_feedin = []
            heat_feedin_old = (demand[heat_feedin_cols].values)
            
            for i in heat_feedin_old:
                heat_feedin.append(i[0])

        else:
            heat_feedin = np.zeros(self.timesteps)
            
        #Gas Costs
        gas_costs_cols= [col for col in demand.columns if 'C_gasEnergy' in col]
        if(gas_costs_cols):
            gas_costs=[]
            gas_costs_old = (demand[gas_costs_cols].values)
            
            for i in gas_costs_old:
                gas_costs.append(i[0])

        else:
            gas_costs = np.zeros(self.timesteps)
            
        #Waste Disposal Costs
        waste_costs_cols= [col for col in demand.columns if 'C_wastedisposal' in col]
        if(waste_costs_cols):
            waste_costs=[]
            waste_costs_old = (demand[waste_costs_cols].values)
            
            for i in waste_costs_old:
                waste_costs.append(i[0])

        else:
            waste_costs = np.zeros(self.timesteps)
            
        #Waste Market Price
        waste_market_cols= [col for col in demand.columns if 'P_wastemarket' in col]
        if(waste_market_cols):
            waste_market=[]
            waste_market_old = (demand[waste_market_cols].values)
            
            for i in waste_market_old:
                waste_market.append(i[0])

        else:
            waste_market = np.zeros(self.timesteps)
            
        #Water Pipeline Costs
        waterPipeline_costs_cols= [col for col in demand.columns if 'C_waterPipeline' in col]
        if(waterPipeline_costs_cols):
            waterPipeline_costs = []
            waterPipeline_costs_old = (demand[waterPipeline_costs_cols].values)
            
            for i in waterPipeline_costs_old:
                waterPipeline_costs.append(i[0])

        else:
            waterPipeline_costs = np.zeros(self.timesteps)
            
        #Water Sewage Costs
        waterSewage_costs_cols= [col for col in demand.columns if 'C_waterSewage' in col]
        if(waterSewage_costs_cols):
            waterSewage_costs=[]
            waterSewage_costs_old = (demand[waterSewage_costs_cols].values)
            for i in waterSewage_costs_old:
                waterSewage_costs.append(i[0])

        else:
            waterSewage_costs = np.zeros(self.timesteps)
            
        #Cooling Energy Costs
        cooling_costs_cols= [col for col in demand.columns if 'C_coolEnergy' in col]
        if(cooling_costs_cols):
            cooling_costs=[]
            cooling_costs_old = (demand[cooling_costs_cols].values)
            
            for i in cooling_costs_old:
                cooling_costs.append(i[0])

        else:
            cooling_costs = np.zeros(self.timesteps)
            
            
        #Cooling Feedin
        cooling_feedin_cols= [col for col in demand.columns if 'P_coolFeedIn' in col]
        if(cooling_feedin_cols):
            cooling_feedin=[]
            cooling_feedin_old = (demand[cooling_feedin_cols].values)
            
            for i in cooling_feedin_old:
                cooling_feedin.append(i[0])

        else:
            cooling_feedin = np.zeros(self.timesteps)
            
        
        #Revenues Hydrogen Market
        hydrogen_market_cols= [col for col in demand.columns if 'P_hydrogenmarket' in col]
        if(hydrogen_market_cols):
            hydrogen_market=[]
            hydrogen_market_old = (demand[hydrogen_market_cols].values)
            
            for i in hydrogen_market_old:
                hydrogen_market.append(i[0])

        else:
            hydrogen_market = np.zeros(self.timesteps)
            
            
        #Sludge Disposal
        sludge_costs_cols= [col for col in demand.columns if 'C_sludgeDisposal' in col]
        if(sludge_costs_cols):
            sludge_costs=[]
            sludge_costs_old = (demand[sludge_costs_cols].values)
            
            for i in sludge_costs_old:
                sludge_costs.append(i[0])

        else:
            sludge_costs = np.zeros(self.timesteps)
            
        #Petrol Costs
        petrol_costs_cols= [col for col in demand.columns if 'C_petrol' in col]
        if(petrol_costs_cols):
            petrol_costs=[]
            petrol_costs_old = (demand[petrol_costs_cols].values)
            
            for i in petrol_costs_old:
                petrol_costs.append(i[0])

        else:
            petrol_costs = np.zeros(self.timesteps)
            
            
        #Scarcity Factor
        scarcity_factor_cols= [col for col in demand.columns if 'F_scarcity' in col]
        if(scarcity_factor_cols):
            scarcity_factor=[]
            scarcity_factor_old = (demand[scarcity_factor_cols].values)
            
            for i in scarcity_factor_old:
                scarcity_factor.append(i[0])

        else:
            scarcity_factor = np.zeros(self.timesteps)
            
        
        #Washing Temperature
        washtemp_cols= [col for col in demand.columns if 'T_wash' in col]
        if(washtemp_cols):
            washtemp=[]
            washtemp_old = (demand[washtemp_cols].values)
            
            for i in washtemp_old:
                washtemp.append(i[0])

        else:
            washtemp = np.zeros(self.timesteps)
            
            
        #Costs for wastetransmission
        wastetransmission_cols= [col for col in demand.columns if 'C_wastetransmission' in col]
        if(wastetransmission_cols):
            wastetransmission=[]
            wastetransmission_old = (demand[wastetransmission_cols].values)
            
            for i in wastetransmission_old:
                wastetransmission.append(i[0])

        else:
            wastetransmission = np.zeros(self.timesteps)
            




        self.input_data_clustered = {'copHP' : copHP, 
                  'elGen' : elGen, 
                  'copCool' : copCool, 
                  'thCool' : thCool,
                  'rainfall' : rainfall,
                  'electricityCosts' : electricity_costs,
                  'electricityFeedin' : electricity_feedin,
                  'heatCosts' : heat_costs,
                  'heatFeedin' : heat_feedin,
                  'gasCosts' : gas_costs,
                  'wasteCosts' : waste_costs,
                  'wasteMarket' : waste_market,
                  'waterPipelineCosts' : waterPipeline_costs,
                  'sewageCosts' : waterSewage_costs,
                  'sludgeCosts' : sludge_costs,
                  'coolingCosts' : cooling_costs,
                  'coolingFeedin' : cooling_feedin,
                  'wastetransmissioncosts' : wastetransmission,
                  'hydrogenMarket' : hydrogen_market,
                  'petrolCosts' : petrol_costs,
                  'waterScarcity' : scarcity_factor,
                  'washTemperature' : washtemp,
                  'electricityDemand' : eldemand,
                  'heatDemand' : heatdemand,
                  'hotwaterDemand' : hotwaterdemand,
                  'gasDemand': gasdemand,
                  'waste' : wastedemand,
                  'waterDemand' : waterdemand,
                  'coolingDemand' : coolingdemand,
                  'hydrogenDemand': hydrogendemand,
                  'transportDemand' : transportdemand}
        
        
    def modify_inputdata(self, *args, **kwargs):
        data = self.input_data
        steps = kwargs.get("steps", 4)
        
        data["coolingCosts"] = meth.list_meanvalue(data["coolingCosts"], steps)
        data["coolingDemand"] = meth.list_sumvalue(data["coolingDemand"], steps)
        data["coolingFeedin"] = meth.list_meanvalue(data["coolingFeedin"], steps)
        data["copCool"] = meth.list_meanvalue(data["copCool"], steps)
        data["copHP"] = meth.list_meanvalue(data["copHP"], steps)
        data["electricityCosts"] = meth.list_meanvalue(data["electricityCosts"], steps)
        data["electricityDemand"] = meth.list_sumvalue(data["electricityDemand"], steps)
        data["electricityFeedin"] = meth.list_meanvalue(data["electricityFeedin"], steps)
        data["elGen"] = meth.list_sumvalue(data["elGen"], steps)
        data["gasCosts"] = meth.list_meanvalue(data["gasCosts"], steps)
        data["gasDemand"] = meth.list_sumvalue(data["gasDemand"], steps)
        data["heatCosts"] = meth.list_meanvalue(data["heatCosts"], steps)
        data["heatDemand"] = meth.list_sumvalue(data["heatDemand"], steps)
        data["heatFeedin"] = meth.list_meanvalue(data["heatFeedin"], steps)
        data["hotwaterDemand"] = meth.list_sumvalue(data["hotwaterDemand"], steps)
        data["hydrogenDemand"] = meth.list_sumvalue(data["hydrogenDemand"], steps)
        data["hydrogenMarket"] = meth.list_meanvalue(data["hydrogenMarket"], steps)
        data["petrolCosts"] = meth.list_meanvalue(data["petrolCosts"], steps)
        data["rainfall"] = meth.list_sumvalue(data["rainfall"], steps)
        data["sewageCosts"] = meth.list_meanvalue(data["sewageCosts"], steps)
        data["sludgeCosts"] = meth.list_meanvalue(data["sludgeCosts"], steps)
        data["thCool"] = meth.list_meanvalue(data["thCool"], steps)
        data["transportDemand"] = meth.list_sumvalue(data["transportDemand"], steps)
        data["washTemperature"] = meth.list_meanvalue(data["washTemperature"], steps)
        data["waste"] = meth.list_sumvalue(data["waste"], steps)
        data["wasteCosts"] = meth.list_meanvalue(data["wasteCosts"], steps)
        data["wasteMarket"] = meth.list_meanvalue(data["wasteMarket"], steps)
        data["wastetransmissioncosts"] = meth.list_meanvalue(data["wastetransmissioncosts"], steps)
        data["waterDemand"] = meth.list_sumvalue(data["waterDemand"], steps)
        data["waterPipelineCosts"] = meth.list_meanvalue(data["waterPipelineCosts"], steps)
        data["waterScarcity"] = meth.list_meanvalue(data["waterScarcity"], steps)
        
        self.input_data = data
    
    
    
    