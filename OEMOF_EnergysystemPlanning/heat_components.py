# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 06:57:48 2021

@author: Matthias Maldet
"""


from oemof.network import network as on
from oemof.solph import blocks
import oemof.solph as solph
from oemof.solph.plumbing import sequence
import transformerLosses as tl
import transformerLossesMultipleOut as tlmo
import numpy as np
import source_emissions as em

class ThermalCooling():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.7
            costsIn=0
            costsOut=0.002
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'ThermalCooling_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Thermal\nCooling_' + self.position)
            color='lime'
            self.color = kwargs.get("color", color)
            

        def component(self):
            q_thCoolTh = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_thCoolCool = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            thCool = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_thCoolTh},
                outputs={self.sector_out : q_thCoolCool},
                conversion_factors={self.sector_out : self.conversion_factor})
            return thCool
        
        
class ThermalStorage():
    
    def __init__(self, *args, **kwargs):
        P_inMax = 7
        costsIn = 0.0005
        costsOut=0.0005
        Eta_in = 0.8
        Eta_out = 0.8
        Eta_sb = 0.999
        V_max = 0.3
        V_start = 0
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        tempdif=45
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_in = kwargs.get("P_in", P_inMax)
        self.P_out = kwargs.get("P_out", self.P_in)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        label = 'ThermalStorage_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Thermal\nStorage_' + self.position)
        self.balanced = kwargs.get("balanced", True)
        color='red'
        self.color = kwargs.get("color", color)
        self.tempdif = kwargs.get("tempdif", tempdif)
            
        self.thCapacityWater = 4.190/3600
        self.rhoWater = 1000
        
        self.soc_max = self.thCapacityWater*self.rhoWater*self.tempdif*self.v_max
        
    def component(self):
        #Energy Flows
        q_thStoreIn = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
        q_thStoreOut = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
        if(self.v_max==0):
            soc_divisor=1
        else:
            soc_divisor=self.soc_max
            
        thStore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_thStoreIn},
                outputs={self.sector_out: q_thStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.v_start/soc_divisor, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return thStore
        
class HotwaterBoiler():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.95
            costsIn=0
            costsOut=0.0005
            PowerInLimit = 21
            PowerOutLimit = 21
            WaterBoiler=0.017
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.WaterFactor = kwargs.get("water_demand", WaterBoiler)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_loss = kwargs.get("sector_loss", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'HotwaterBoiler_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Hotwater\nBoiler_' + self.position)
            color='blue'
            self.color = kwargs.get("color", color)
            

        def component(self):
            q_hwBoilHeat = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_hwBoilHW = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            if(self.sector_loss==0):
                hwBoiler = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_hwBoilHeat},
                    outputs={self.sector_out : q_hwBoilHW},
                    conversion_factors={self.sector_out : self.conversion_factor})
            else:
            
                v_water2boiler = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT*self.WaterFactor)
            
                hwBoiler = tlmo.TransformerLossesMultipleOut(
                    label=self.label,
                    inputs={self.sector_in : q_hwBoilHeat, self.sector_loss : v_water2boiler},
                    outputs={self.sector_out : q_hwBoilHW},
                    conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                    conversion_factor=[self.conversion_factor], losses_factor=self.WaterFactor)
            return hwBoiler        
        
class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Districtheatgrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'District\nHeat\nGrid_' + self.position)
        color='royalblue'
        self.color = kwargs.get("color", color)
        self.costs_model=kwargs.get("costs_model", costs)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
            
        dhGrid = solph.Source(label=self.label, outputs={self.sector_out: q_dhGridfeedin})
        
        return dhGrid
    
class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0.14
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Districtheatgrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'District\nHeat\nGrid_' + self.position)
        self.costs_model=kwargs.get("costs_model", costs)
        color = 'royalblue'
        self.color = kwargs.get("color", color)
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            dhGrid = solph.Source(label=self.label, outputs={self.sector_out: q_dhGridfeedin})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            dhGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_dhGridfeedin, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return dhGrid
    
class GridSink():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'DistrictheatgridSink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'District\nHeat\nGrid Sink_' + self.position)
        self.costs_model=kwargs.get("costs_model", 0)
        self.costs_in=0
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
            
        dhGrid = solph.Sink(label=self.label, inputs={self.sector_in: q_dhGridfeedin})
        
        return dhGrid
    
    
class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_in = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'Heatdemand_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heat\nDemand_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)
            
            color='yellow'
            self.color = kwargs.get("color", color)

        def component(self):
            
            q_heatDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            heatDemand = solph.Sink(label=self.label, inputs={self.sector_in: q_heatDemand})
            
            return heatDemand    
        
        
class Hotwaterdemand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector_in = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            label = 'DemandHotwater_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Hotwater\nDemand_' + self.position)
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)
            color='blue'
            self.color = kwargs.get("color", color)

        def component(self):
            
            q_hotwaterDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            hotwaterDemand = solph.Sink(label=self.label, inputs={self.sector_in: q_hotwaterDemand})
            
            return hotwaterDemand  
    
class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        existingGrid = 0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Districtheatgridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'DistrictHeat\nGridPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        q_dhGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))
            
        dhGrid = solph.Source(label=self.label, outputs={self.sector_out: q_dhGridpurchase})
        
        return dhGrid 
    
class GridPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        existingGrid=0
        
        self.mini= kwargs.get("minimum", 0)
        self.maxi= kwargs.get("maximum", 1)
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Districtheatgridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'DistrictHeat\nGridPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
        
    def component(self):
        q_dhGridpurchase = solph.Flow(min=self.mini, max=self.maxi, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))

        
        if(self.emissions==0 or self.sector_emissions==0):
            
            dhGrid = solph.Source(label=self.label, outputs={self.sector_out: q_dhGridpurchase})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            dhGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_dhGridpurchase, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return dhGrid
    
class GridPurchaseEmissionsFix():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        
        self.mini= kwargs.get("minimum", 0)
        self.maxi= kwargs.get("maximum", 1)
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Districtheatgridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'DistrictHeat\nGridPurchase_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
        
    def component(self):
        q_dhGridpurchase = solph.Flow(min=self.mini, max=self.maxi, variable_costs=self.costs_out,  nominal_value=self.P*self.deltaT)

        
        if(self.emissions==0 or self.sector_emissions==0):
            
            dhGrid = solph.Source(label=self.label, outputs={self.sector_out: q_dhGridpurchase})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            dhGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_dhGridpurchase, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return dhGrid
        
    
class GridFeedin():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'DistrictheatgridFeedin_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'DistrictHeat\nGridFeedin' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        revenuesEnergyArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("revenues", revenuesEnergyArray)
        self.revenues=np.multiply(self.revenues, -1)
        color='slategrey'
        self.color = kwargs.get("color", color)
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.revenues)
            
        dhGridFeedIn = solph.Sink(label=self.label, inputs={self.sector_in: q_dhGridfeedin})
        
        return dhGridFeedIn
        
    
class flexibilityComponent():
    
    def __init__(self, *args, **kwargs):
        P = 11
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        minimum=-1
        maximum=1
        
        self.P= kwargs.get("P", P)
        self.sector_out= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'HeatFlexibility_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = kwargs.get('label_sankey', 'Heat\nFlexibility_' + self.position)
        self.timesteps = kwargs.get("timesteps", timesteps)
        color='lightsalmon'
        self.color = kwargs.get("color", color)
        

        
        self.minimum = kwargs.get("minimum", minimum)
        self.maximum = kwargs.get("maximum", maximum)
        
        
    def component(self):
        q_flexibility = solph.Flow(min=self.minimum, max=self.maximum, nominal_value=self.P*self.deltaT, variable_costs=0)
            
        flexibility = solph.Source(label=self.label, outputs={self.sector_out: q_flexibility})
        
        return flexibility
    
#District heat
class DistrictHeat():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            PowerInLimit = 10
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.efficiency = kwargs.get("efficiency", 1)

            self.P = kwargs.get("P", PowerInLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'DistrictHeatInvest_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = kwargs.get('label_sankey', 'Heat2LSC_' + self.position)
            color = 'olivedrab'
            self.color = kwargs.get("color", color)
            
            
            self.timesteps = kwargs.get("timesteps", 8760)
            self.timestepsTotal = kwargs.get("timestepsTotal", self.timesteps)
            



        def component(self):
            
            
            q_dh_in = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=0)              
            q_dh_out = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=0)
            
            districtheat = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_dh_in},
                outputs={self.sector_out : q_dh_out},
                conversion_factors={self.sector_out : self.efficiency})
            
            return districtheat
        
        