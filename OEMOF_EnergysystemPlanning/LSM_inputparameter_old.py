# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 10:15:34 2022

@author: Matthias Maldet
"""

def LSM_data():
    
    lsm_struct ={
        
        #_________________________________________ LSM Hub 1 _______________________________________________________
        
        "LSM_1" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1540,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 0,
            "battery_soc_start": 0,
            "battery_P_in": 0,
            "battery_P_out": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 0,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": 1,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 0, "LSM_2": 2, "LSM_3": 2, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 2, "LSM_7": 1.4},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_wastetruck": {"LSM_1": 0*0.125/3200, "LSM_2": 2*0.125/3200, "LSM_3": 2*0.125/3200, "LSM_4": 1.2*0.125/3200, "LSM_5": 2.4*0.125/3200, "LSM_6": 2*0.125/3200, "LSM_7": 1.4*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            #----------------- Sewage ------------------------------
            
           
            #Sewage treatment
            "sewagetreat_eta_water": 1,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 0, "LSM_2": 2, "LSM_3": 2, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 2, "LSM_7": 1.4},
            "emissions_sludgetruck":{"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_sludgetruck": {"LSM_1": 0*0.125/4, "LSM_2": 2*0.125/4, "LSM_3": 2*0.125/4, "LSM_4": 1.2*0.125/4, "LSM_5": 2.4*0.125/4, "LSM_6": 2*0.125/4, "LSM_7": 1.4*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
        },
        
        #_________________________________________ LSM Hub 2 _______________________________________________________
        
        "LSM_2" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1562,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 0,
            "battery_soc_start": 0,
            "battery_P_in": 0,
            "battery_P_out": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 0,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": 1,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 2, "LSM_2": 0, "LSM_3": 1, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 1, "LSM_7": 1.4},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_wastetruck": {"LSM_1": 2*0.125/3200, "LSM_2": 0*0.125/3200, "LSM_3": 1*0.125/3200, "LSM_4": 1.2*0.125/3200, "LSM_5": 2.4*0.125/3200, "LSM_6": 1*0.125/3200, "LSM_7": 1.4*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 1,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 2, "LSM_2": 0, "LSM_3": 1, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 1, "LSM_7": 1.4},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_sludgetruck": {"LSM_1": 2*0.125/4, "LSM_2": 0*0.125/4, "LSM_3": 1*0.125/4, "LSM_4": 1.2*0.125/4, "LSM_5": 2.4*0.125/4, "LSM_6": 1*0.125/4, "LSM_7": 1.4*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
        },
        
        #_________________________________________ LSM Hub 3 _______________________________________________________
        
        "LSM_3" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 2090,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 0,
            "battery_soc_start": 0,
            "battery_P_in": 0,
            "battery_P_out": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 0,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": 1,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 2, "LSM_2": 1, "LSM_3": 0, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 1, "LSM_7": 1.4},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_wastetruck": {"LSM_1": 2*0.125/3200, "LSM_2": 1*0.125/3200, "LSM_3": 0*0.125/3200, "LSM_4": 1.2*0.125/3200, "LSM_5": 2.4*0.125/3200, "LSM_6": 1*0.125/3200, "LSM_7": 1.4*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 1,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 2, "LSM_2": 1, "LSM_3": 0, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 1, "LSM_7": 1.4},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_sludgetruck": {"LSM_1": 2*0.125/4, "LSM_2": 1*0.125/4, "LSM_3": 0*0.125/4, "LSM_4": 1.2*0.125/4, "LSM_5": 2.4*0.125/4, "LSM_6": 1*0.125/4, "LSM_7": 1.4*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
        },
        
        #_________________________________________ LSM Hub 4 _______________________________________________________
        
        "LSM_4" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1331,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 0,
            "battery_soc_start": 0,
            "battery_P_in": 0,
            "battery_P_out": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 0,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": 1,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 1.2, "LSM_2": 1.2, "LSM_3": 1.2, "LSM_4": 0, "LSM_5": 1.6, "LSM_6": 1.2, "LSM_7": 0.6},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_wastetruck": {"LSM_1": 1.2*0.125/3200, "LSM_2": 1.2*0.125/3200, "LSM_3": 1.2*0.125/3200, "LSM_4": 0*0.125/3200, "LSM_5": 1.6*0.125/3200, "LSM_6": 1.2*0.125/3200, "LSM_7": 0.6*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 1,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 1.2, "LSM_2": 1.2, "LSM_3": 1.2, "LSM_4": 0, "LSM_5": 1.6, "LSM_6": 1.2, "LSM_7": 0.6},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_sludgetruck": {"LSM_1": 1.2*0.125/4, "LSM_2": 1.2*0.125/4, "LSM_3": 1.2*0.125/4, "LSM_4": 0*0.125/4, "LSM_5": 1.6*0.125/4, "LSM_6": 1.2*0.125/4, "LSM_7": 0.6*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
        },
        
        #_________________________________________ LSM Hub 5 _______________________________________________________
        
        "LSM_5" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 1507,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 0,
            "battery_soc_start": 0,
            "battery_P_in": 0,
            "battery_P_out": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 0,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": 1,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 2.4, "LSM_2": 2.4, "LSM_3": 2.4, "LSM_4": 1.6, "LSM_5": 0, "LSM_6": 2.4, "LSM_7": 1.8},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_wastetruck": {"LSM_1": 2.4*0.125/3200, "LSM_2": 2.4*0.125/3200, "LSM_3": 2.4*0.125/3200, "LSM_4": 1.6*0.125/3200, "LSM_5": 0*0.125/3200, "LSM_6": 2.4*0.125/3200, "LSM_7": 1.8*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 1,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 2.4, "LSM_2": 2.4, "LSM_3": 2.4, "LSM_4": 1.6, "LSM_5": 0, "LSM_6": 2.4, "LSM_7": 1.8},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_sludgetruck": {"LSM_1": 2.4*0.125/4, "LSM_2": 2.4*0.125/4, "LSM_3": 2.4*0.125/4, "LSM_4": 1.6*0.125/4, "LSM_5": 0*0.125/4, "LSM_6": 2.4*0.125/4, "LSM_7": 1.8*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
        },
        
        #_________________________________________ LSM Hub 6 _______________________________________________________
        
        "LSM_6" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 500,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 0,
            "battery_soc_start": 0,
            "battery_P_in": 0,
            "battery_P_out": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 0,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": 1,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 2, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 0, "LSM_7": 1.4},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_wastetruck": {"LSM_1": 2*0.125/3200, "LSM_2": 1*0.125/3200, "LSM_3": 1*0.125/3200, "LSM_4": 1.2*0.125/3200, "LSM_5": 2.4*0.125/3200, "LSM_6": 0*0.125/3200, "LSM_7": 1.4*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 1,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 2, "LSM_2": 1, "LSM_3": 1, "LSM_4": 1.2, "LSM_5": 2.4, "LSM_6": 0, "LSM_7": 1.4},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_sludgetruck": {"LSM_1": 2*0.125/4, "LSM_2": 1*0.125/4, "LSM_3": 1*0.125/4, "LSM_4": 1.2*0.125/4, "LSM_5": 2.4*0.125/4, "LSM_6": 0*0.125/4, "LSM_7": 1.4*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
        },
        
        #_________________________________________ LSM Hub 7 _______________________________________________________
        
        "LSM_7" :{
            
            #----------------- Electricity ------------------------------
            
            #Electricity grid
            "c_grid_elec": 0.062,
            "c_abgabe_elec": 0.018,
            "c_grid_power": 0,
            "P_elgrid": 500,
            "em_elgrid": 0.209,
            
            #PV (first not possible - potentially with public area extension)
            "area_efficiency": 1,
            "area_roof": 0,
            "area_factor": 10,
            
            #Battery
            "battery_soc_max": 0,
            "battery_soc_start": 0,
            "battery_P_in": 0,
            "battery_P_out": 0,
            
            
            #----------------- Heat ------------------------------
            
            #Heatpump (first not possible - potentially in additional scenarios)
            "P_heatpump_in": 0,
            "P_heatpump_out": 0,
            
            #Thermal storage
            "P_heatstore": 0,
            "v_heatstore": 0,
            
            #Exhaustheat
            "P_exhaustheat": 1000000,
            
            #District heat source
            "P_districtheatsource": 0,
            "c_districtheatsource": 0.03,
            "em_districtheatsource": 0.14,
            
            
            #----------------- Waste ------------------------------
            
            #Waste combustion
            "wastecomb_P_in": 5000*2,
            "wastecomb_P_out": 5000*2,
            "wastecomb_usable_energy": 1,
            
            #Waste storage
            "wastestore_v_max": 70625*2,
            "wastestore_v_start": 0,
            "wastestore_disposal_periods": 0,
            "wastestore_balanced": True,
            
            #Waste transmission
            "transmissioncapacity_wastetruck": 3200*2,
            "distance_wastetruck": {"LSM_1": 1.4, "LSM_2": 1.4, "LSM_3": 1.4, "LSM_4": 0.6, "LSM_5": 1.8, "LSM_6": 1.4, "LSM_7": 0},
            "emissions_wastetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_wastetruck": {"LSM_1": 1.4*0.125/3200, "LSM_2": 1.4*0.125/3200, "LSM_3": 1.4*0.125/3200, "LSM_4": 0.6*0.125/3200, "LSM_5": 1.8*0.125/3200, "LSM_6": 1.4*0.125/3200, "LSM_7": 0*0.125/3200},
            "delay_wastetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            
            #----------------- Water ------------------------------
            "wasted_water_costs": 0,
            
            
            #----------------- Sewage ------------------------------
            
            #Sewage treatment
            "sewagetreat_eta_water": 1,
            "sewagetreat_emissions": 0.3,
            "sewagetreat_tempdif": 0,
            
            
            #----------------- Sludge ------------------------------
            #Sludge combustion
            "sludgecomb_P_in": 5000,
            "sludgecomb_P_out": 5000,
            "sludgecomb_usable_energy": 1,
            
            #Sludge storage
            "sludgestore_v_max": 100,
            "sludgestore_v_start": 0,
            "sludgestore_disposal_periods": 0,
            "sludgestore_balanced": True,
            
            #Sludge transmission
            "transmissioncapacity_sludgetruck": 4,
            "distance_sludgetruck": {"LSM_1": 1.4, "LSM_2": 1.4, "LSM_3": 1.4, "LSM_4": 0.6, "LSM_5": 1.8, "LSM_6": 1.4, "LSM_7": 0},
            "emissions_sludgetruck": {"LSM_1": 0.125, "LSM_2": 0.125, "LSM_3": 0.125, "LSM_4": 0.125, "LSM_5": 0.125, "LSM_6": 0.125, "LSM_7": 0.125},
            "costs_sludgetruck": {"LSM_1": 1.4*0.125/4, "LSM_2": 1.4*0.125/4, "LSM_3": 1.4*0.125/4, "LSM_4": 0.6*0.125/4, "LSM_5": 1.8*0.125/4, "LSM_6": 1.4*0.125/4, "LSM_7": 0*0.125/4},
            "delay_sludgetruck": {"LSM_1": 0, "LSM_2": 0, "LSM_3": 0, "LSM_4": 0, "LSM_5": 0, "LSM_6": 0, "LSM_7": 0},
            
            #----------------- Emissions ------------------------------
            
            #CO2 price
            "co2price": 0
            
        }
        
    }
    return lsm_struct
            
            