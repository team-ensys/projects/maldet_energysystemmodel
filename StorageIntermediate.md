# OEMOF Block StorageIntermediate

## Introduction

Often it is required to guarantee an empty storage at certain timeperiods in a total timeperiod. Therefore, constraints for the storage level are time dependent.
Therefore, the OEMOF Block
[GenericStorage](https://oemof-solph.readthedocs.io/en/latest/_modules/oemof/solph/components/generic_storage.html#GenericInvestmentStorageBlock)
 was is extended by time-dependent storage content constraints.

 ## Class Variable Extension

 The block is implemented in such way, that the storage level is set to zero in euqal timeintervalls `disposal_periods`. To get the considered timesteps, the parameter `timesteps` must be set.

```
self.timesteps= kwargs.get("timesteps", 8760)
self.disposalPeriods= kwargs.get("disposal_periods", self.timesteps)
```
<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Timesteps after which the Storage must be empty | `disposal_periods` | Set Equal to total timesteps|
| Total Timesteps | `timesteps` | 8760 |
| Other Storage Parameters | Definition Like in [GenericStorage](https://oemof-solph.readthedocs.io/en/latest/_modules/oemof/solph/components/generic_storage.html#GenericInvestmentStorageBlock) | / |



</details>

## Constraint Extension

The constraint extension is done in the contraint-group block. Thereby certain constraints are added, and the storage balance rule is changed.

### Storage Balance Rule

In the disposal periods, the sum of the previous storage level, the inflow and the outflow must be zero:

**soc(t-1) + inflow(t) - outflow(t) = 0 --> for t=disposal_periods**

This is only done for `disposal_periods` that are greater than one, as it would otherwise not be possible to store content.

In all other timesteps, the storage equation is applied.

**soc(t-1) + inflow(t) - outflow(t) = soc(t) --> for t!=disposal_periods**

<details>
<summary>Full Code</summary>

```
def _storage_balance_rule(block, n, t):
            """
            Rule definition for the storage balance of every storage n and
            every timestep but the first (t > 0).
            """
            expr = 0

            if(n.disposalPeriods==1):
 
                expr += block.storage_content[n, t]
                expr += (
                    -block.storage_content[n, t - 1]
                    * (1 - n.loss_rate[t]) ** m.timeincrement[t]
                    )
                expr += (
                    n.fixed_losses_relative[t]
                    * n.nominal_storage_capacity
                    * m.timeincrement[t]
                    )
                expr += n.fixed_losses_absolute[t] * m.timeincrement[t]
                expr += (
                    -m.flow[i[n], n, t] * n.inflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                expr += (
                    m.flow[n, o[n], t] / n.outflow_conversion_factor[t]
                    ) * m.timeincrement[t]
            
            elif((t+1)%n.disposalPeriods==0):

                #expr += block.storage_content[n, t]
                
                expr += (
                    -block.storage_content[n, t - 1]
                    * (1 - n.loss_rate[t]) ** m.timeincrement[t]
                    )
                expr += (
                    n.fixed_losses_relative[t]
                    * n.nominal_storage_capacity
                    * m.timeincrement[t]
                    )
                expr += n.fixed_losses_absolute[t] * m.timeincrement[t]
                expr += (
                    -m.flow[i[n], n, t] * n.inflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                expr += (
                    m.flow[n, o[n], t] / n.outflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                
                
            else:

                expr += block.storage_content[n, t]
                expr += (
                    -block.storage_content[n, t - 1]
                    * (1 - n.loss_rate[t]) ** m.timeincrement[t]
                    )
                expr += (
                    n.fixed_losses_relative[t]
                    * n.nominal_storage_capacity
                    * m.timeincrement[t]
                    )
                expr += n.fixed_losses_absolute[t] * m.timeincrement[t]
                expr += (
                    -m.flow[i[n], n, t] * n.inflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                expr += (
                    m.flow[n, o[n], t] / n.outflow_conversion_factor[t]
                    ) * m.timeincrement[t]

            return expr == 0

        self.balance = Constraint(
            self.STORAGES, reduced_timesteps, rule=_storage_balance_rule
        )
```
</details>

### Storage empty

In addition to the storage balance rule, the soc in the `disposal_periods` must be explizitily set to zero.

**soc(t) = 0 for t=disposal_periods**

<details>
<summary>Full Code</summary>

```
def _storage_leveldisposal_rule(block, n, t):
            if((t+1)%n.disposalPeriods==0):
                expr=0
                expr += block.storage_content[n, t]
                return expr==0
            else:
                return Constraint.Skip
            
        self.disposalperiod = Constraint(
            self.STORAGES, reduced_timesteps, rule=_storage_leveldisposal_rule
            )
```
</details>

### Outflow Rule

The outflows can only be greater than zero in disposal_periods timesteps. In all other timesteps they are zero, which is set with a constraint.

<details>
<summary>Full Code</summary>

```
def _outflow_rule(block, n, t):
            
            if((t+1)%n.disposalPeriods!=0 and t!= m.TIMESTEPS[-1]):
                expr=0
                expr += m.flow[n, o[n], t]
                return expr==0
            
            
            else:
                return Constraint.Skip
            
        self.outflowset = Constraint(
            self.STORAGES, reduced_timesteps, rule=_outflow_rule
            )
```
</details>

## Definition of a StorageIntermediate

In the following example, a definition of a StorageIntermediate is presented:

```
m_waste2storeWasteStoreIn = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_in)
        m_waste2storeWasteStoreOut = solph.Flow(min=0, max=1, nominal_value=self.v_max*self.rho_municipalwaste, variable_costs=self.costs_out)
        
        
            wastestore = storage_int.StorageIntermediate(
                label=self.label,
                inputs={self.sector_in: m_waste2storeWasteStoreIn},
                outputs={self.sector_out: m_waste2storeWasteStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max*self.rho_municipalwaste,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max,
                disposal_periods=self.disposal_periods, timesteps=self.timesteps)
        
            return wastestore
```

## Total Code

See following for the code

<details>
<summary>Full Code</summary>

```
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 07:23:23 2021

@author: Matthias Maldet
"""

from oemof.network import network
from pyomo.core.base.block import SimpleBlock
from pyomo.environ import Binary
from pyomo.environ import Constraint
from pyomo.environ import Expression
from pyomo.environ import NonNegativeReals
from pyomo.environ import Set
from pyomo.environ import Var

from oemof.solph import network as solph_network
from oemof.solph.options import Investment
from oemof.solph.plumbing import sequence as solph_sequence


class StorageIntermediate(network.Node):
    

    def __init__(
        self, *args, max_storage_level=1, min_storage_level=0, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.nominal_storage_capacity = kwargs.get("nominal_storage_capacity")
        self.initial_storage_level = kwargs.get("initial_storage_level")
        self.balanced = kwargs.get("balanced", True)
        self.loss_rate = solph_sequence(kwargs.get("loss_rate", 0))
        self.fixed_losses_relative = solph_sequence(
            kwargs.get("fixed_losses_relative", 0)
        )
        self.fixed_losses_absolute = solph_sequence(
            kwargs.get("fixed_losses_absolute", 0)
        )
        self.inflow_conversion_factor = solph_sequence(
            kwargs.get("inflow_conversion_factor", 1)
        )
        self.outflow_conversion_factor = solph_sequence(
            kwargs.get("outflow_conversion_factor", 1)
        )
        self.max_storage_level = solph_sequence(max_storage_level)
        self.min_storage_level = solph_sequence(min_storage_level)
        self.investment = kwargs.get("investment")
        self.invest_relation_input_output = kwargs.get(
            "invest_relation_input_output"
        )
        self.invest_relation_input_capacity = kwargs.get(
            "invest_relation_input_capacity"
        )
        self.invest_relation_output_capacity = kwargs.get(
            "invest_relation_output_capacity"
        )
        self._invest_group = isinstance(self.investment, Investment)
        
        
        #----------------- Addition Maldet ---------------
        
        self.timesteps= kwargs.get("timesteps", 8760)
        self.disposalPeriods= kwargs.get("disposal_periods", self.timesteps)
        
        #-------------------------------------------------
        

        # Check number of flows.
        self._check_number_of_flows()

        # Check attributes for the investment mode.
        if self._invest_group is True:
            self._check_invest_attributes()

       
        renamed_parameters = [
            ("nominal_capacity", "nominal_storage_capacity"),
            ("initial_capacity", "initial_storage_level"),
            ("capacity_loss", "loss_rate"),
            ("capacity_min", "min_storage_level"),
            ("capacity_max", "max_storage_level"),
        ]
        messages = [
            "`{0}` to `{1}`".format(old_name, new_name)
            for old_name, new_name in renamed_parameters
            if old_name in kwargs
        ]
        if messages:
            message = (
                "The following attributes have been renamed from v0.2 to v0.3:"
                "\n\n  {}\n\n"
                "You are using the old names as parameters, thus setting "
                "deprecated\n"
                "attributes, which is not what you might have intended.\n"
                "Use the new names, or, if you know what you're doing, set "
                "these\n"
                "attributes explicitly after construction instead."
            )
            raise AttributeError(message.format("\n  ".join(messages)))

    def _set_flows(self):
        for flow in self.inputs.values():
            if (
                self.invest_relation_input_capacity is not None
                and not isinstance(flow.investment, Investment)
            ):
                flow.investment = Investment()
        for flow in self.outputs.values():
            if (
                self.invest_relation_output_capacity is not None
                and not isinstance(flow.investment, Investment)
            ):
                flow.investment = Investment()

    def _check_invest_attributes(self):
        if self.investment and self.nominal_storage_capacity is not None:
            e1 = (
                "If an investment object is defined the invest variable "
                "replaces the nominal_storage_capacity.\n Therefore the "
                "nominal_storage_capacity should be 'None'.\n"
            )
            raise AttributeError(e1)
        if (
            self.invest_relation_input_output is not None
            and self.invest_relation_output_capacity is not None
            and self.invest_relation_input_capacity is not None
        ):
            e2 = (
                "Overdetermined. Three investment object will be coupled"
                "with three constraints. Set one invest relation to 'None'."
            )
            raise AttributeError(e2)
        if (
            self.investment
            and sum(solph_sequence(self.fixed_losses_absolute)) != 0
            and self.investment.existing == 0
            and self.investment.minimum == 0
        ):
            e3 = (
                "With fixed_losses_absolute > 0, either investment.existing "
                "or investment.minimum has to be non-zero."
            )
            raise AttributeError(e3)

        self._set_flows()

    def _check_number_of_flows(self):
        msg = "Only one {0} flow allowed in the GenericStorage {1}."
        solph_network.check_node_object_for_missing_attribute(self, "inputs")
        solph_network.check_node_object_for_missing_attribute(self, "outputs")
        if len(self.inputs) > 1:
            raise AttributeError(msg.format("input", self.label))
        if len(self.outputs) > 1:
            raise AttributeError(msg.format("output", self.label))

    def constraint_group(self):
        if self._invest_group is True:
            return StorageIntermediateInvestmentBlock
        else:
            return StorageIntermediateBlock



class StorageIntermediateBlock(SimpleBlock):
    

    CONSTRAINT_GROUP = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _create(self, group=None):
        """
        Parameters
        ----------
        group : list
            List containing storage objects.
            e.g. groups=[storage1, storage2,..]
        """
        m = self.parent_block()

        if group is None:
            return None

        i = {n: [i for i in n.inputs][0] for n in group}
        o = {n: [o for o in n.outputs][0] for n in group}

        #  ************* SETS *********************************

        self.STORAGES = Set(initialize=[n for n in group])

        self.STORAGES_BALANCED = Set(
            initialize=[n for n in group if n.balanced is True]
        )

        self.STORAGES_WITH_INVEST_FLOW_REL = Set(
            initialize=[
                n for n in group if n.invest_relation_input_output is not None
            ]
        )

        #  ************* VARIABLES *****************************

        def _storage_content_bound_rule(block, n, t):
            """
            Rule definition for bounds of storage_content variable of
            storage n in timestep t.
            """
            bounds = (
                n.nominal_storage_capacity * n.min_storage_level[t],
                n.nominal_storage_capacity * n.max_storage_level[t],
            )
            return bounds

        self.storage_content = Var(
            self.STORAGES, m.TIMESTEPS, bounds=_storage_content_bound_rule
        )

        def _storage_init_content_bound_rule(block, n):
            return 0, n.nominal_storage_capacity

        self.init_content = Var(
            self.STORAGES,
            within=NonNegativeReals,
            bounds=_storage_init_content_bound_rule,
        )

        # set the initial storage content
        for n in group:
            if n.initial_storage_level is not None:
                self.init_content[n] = (
                    n.initial_storage_level * n.nominal_storage_capacity
                )
                self.init_content[n].fix()

        #  ************* Constraints ***************************

        reduced_timesteps = [x for x in m.TIMESTEPS if x > 0]

        # storage balance constraint (first time step)
        def _storage_balance_first_rule(block, n):
            """
            Rule definition for the storage balance of every storage n for
            the first timestep.
            """
            expr = 0
            expr += block.storage_content[n, 0]
            expr += (
                -block.init_content[n]
                * (1 - n.loss_rate[0]) ** m.timeincrement[0]
            )
            expr += (
                n.fixed_losses_relative[0]
                * n.nominal_storage_capacity
                * m.timeincrement[0]
            )
            expr += n.fixed_losses_absolute[0] * m.timeincrement[0]
            expr += (
                -m.flow[i[n], n, 0] * n.inflow_conversion_factor[0]
            ) * m.timeincrement[0]
            expr += (
                m.flow[n, o[n], 0] / n.outflow_conversion_factor[0]
            ) * m.timeincrement[0]
            return expr == 0

        self.balance_first = Constraint(
            self.STORAGES, rule=_storage_balance_first_rule
        )
        
        
        #----------------- Addition Maldet ---------------

        # storage balance constraint (every time step but the first)
        def _storage_balance_rule(block, n, t):
            """
            Rule definition for the storage balance of every storage n and
            every timestep but the first (t > 0).
            """
            expr = 0

            if(n.disposalPeriods==1):
 
                expr += block.storage_content[n, t]
                expr += (
                    -block.storage_content[n, t - 1]
                    * (1 - n.loss_rate[t]) ** m.timeincrement[t]
                    )
                expr += (
                    n.fixed_losses_relative[t]
                    * n.nominal_storage_capacity
                    * m.timeincrement[t]
                    )
                expr += n.fixed_losses_absolute[t] * m.timeincrement[t]
                expr += (
                    -m.flow[i[n], n, t] * n.inflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                expr += (
                    m.flow[n, o[n], t] / n.outflow_conversion_factor[t]
                    ) * m.timeincrement[t]
            
            elif((t+1)%n.disposalPeriods==0):

                #expr += block.storage_content[n, t]
                
                expr += (
                    -block.storage_content[n, t - 1]
                    * (1 - n.loss_rate[t]) ** m.timeincrement[t]
                    )
                expr += (
                    n.fixed_losses_relative[t]
                    * n.nominal_storage_capacity
                    * m.timeincrement[t]
                    )
                expr += n.fixed_losses_absolute[t] * m.timeincrement[t]
                expr += (
                    -m.flow[i[n], n, t] * n.inflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                expr += (
                    m.flow[n, o[n], t] / n.outflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                
                
            else:

                expr += block.storage_content[n, t]
                expr += (
                    -block.storage_content[n, t - 1]
                    * (1 - n.loss_rate[t]) ** m.timeincrement[t]
                    )
                expr += (
                    n.fixed_losses_relative[t]
                    * n.nominal_storage_capacity
                    * m.timeincrement[t]
                    )
                expr += n.fixed_losses_absolute[t] * m.timeincrement[t]
                expr += (
                    -m.flow[i[n], n, t] * n.inflow_conversion_factor[t]
                    ) * m.timeincrement[t]
                expr += (
                    m.flow[n, o[n], t] / n.outflow_conversion_factor[t]
                    ) * m.timeincrement[t]

            return expr == 0

        self.balance = Constraint(
            self.STORAGES, reduced_timesteps, rule=_storage_balance_rule
        )
        
        
        
        # storage empty at certain timesteps
        def _storage_leveldisposal_rule(block, n, t):
            if((t+1)%n.disposalPeriods==0):
                expr=0
                expr += block.storage_content[n, t]
                return expr==0
            else:
                return Constraint.Skip
            
        self.disposalperiod = Constraint(
            self.STORAGES, reduced_timesteps, rule=_storage_leveldisposal_rule
            )
        
        # storage empty at certain timesteps
        def _outflow_rule(block, n, t):
            
            if((t+1)%n.disposalPeriods!=0 and t!= m.TIMESTEPS[-1]):
                expr=0
                expr += m.flow[n, o[n], t]
                return expr==0
            
            
            else:
                return Constraint.Skip
            
        self.outflowset = Constraint(
            self.STORAGES, reduced_timesteps, rule=_outflow_rule
            )
        
        
        #-------------------------------------------------
        

        def _balanced_storage_rule(block, n):
            """
            Storage content of last time step == initial storage content
            if balanced.
            """
            return (
                block.storage_content[n, m.TIMESTEPS[-1]]
                == block.init_content[n]
            )

        self.balanced_cstr = Constraint(
            self.STORAGES_BALANCED, rule=_balanced_storage_rule
        )

        def _power_coupled(block, n):
            """
            Rule definition for constraint to connect the input power
            and output power
            """
            expr = (
                m.InvestmentFlow.invest[n, o[n]]
                + m.flows[n, o[n]].investment.existing
            ) * n.invest_relation_input_output == (
                m.InvestmentFlow.invest[i[n], n]
                + m.flows[i[n], n].investment.existing
            )
            return expr

        self.power_coupled = Constraint(
            self.STORAGES_WITH_INVEST_FLOW_REL, rule=_power_coupled
        )

    def _objective_expression(self):
        r"""
        Objective expression for storages with no investment.
        Note: This adds nothing as variable costs are already
        added in the Block :class:`Flow`.
        """
        if not hasattr(self, "STORAGES"):
            return 0

        return 0



class StorageIntermediateInvestmentBlock(SimpleBlock):
    
    CONSTRAINT_GROUP = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _create(self, group=None):
        """ """
        m = self.parent_block()
        if group is None:
            return None

        # ########################## SETS #####################################

        self.INVESTSTORAGES = Set(initialize=[n for n in group])

        self.CONVEX_INVESTSTORAGES = Set(
            initialize=[n for n in group if n.investment.nonconvex is False]
        )

        self.NON_CONVEX_INVESTSTORAGES = Set(
            initialize=[n for n in group if n.investment.nonconvex is True]
        )

        self.INVESTSTORAGES_BALANCED = Set(
            initialize=[n for n in group if n.balanced is True]
        )

        self.INVESTSTORAGES_NO_INIT_CONTENT = Set(
            initialize=[n for n in group if n.initial_storage_level is None]
        )

        self.INVESTSTORAGES_INIT_CONTENT = Set(
            initialize=[
                n for n in group if n.initial_storage_level is not None
            ]
        )

        self.INVEST_REL_CAP_IN = Set(
            initialize=[
                n
                for n in group
                if n.invest_relation_input_capacity is not None
            ]
        )

        self.INVEST_REL_CAP_OUT = Set(
            initialize=[
                n
                for n in group
                if n.invest_relation_output_capacity is not None
            ]
        )

        self.INVEST_REL_IN_OUT = Set(
            initialize=[
                n for n in group if n.invest_relation_input_output is not None
            ]
        )

        # The storage content is a non-negative variable, therefore it makes no
        # sense to create an additional constraint if the lower bound is zero
        # for all time steps.
        self.MIN_INVESTSTORAGES = Set(
            initialize=[
                n
                for n in group
                if sum([n.min_storage_level[t] for t in m.TIMESTEPS]) > 0
            ]
        )

        # ######################### Variables  ################################
        self.storage_content = Var(
            self.INVESTSTORAGES, m.TIMESTEPS, within=NonNegativeReals
        )

        def _storage_investvar_bound_rule(block, n):
            """
            Rule definition to bound the invested storage capacity `invest`.
            """
            if n in self.CONVEX_INVESTSTORAGES:
                return n.investment.minimum, n.investment.maximum
            elif n in self.NON_CONVEX_INVESTSTORAGES:
                return 0, n.investment.maximum

        self.invest = Var(
            self.INVESTSTORAGES,
            within=NonNegativeReals,
            bounds=_storage_investvar_bound_rule,
        )

        self.init_content = Var(self.INVESTSTORAGES, within=NonNegativeReals)

        # create status variable for a non-convex investment storage
        self.invest_status = Var(self.NON_CONVEX_INVESTSTORAGES, within=Binary)

        # ######################### CONSTRAINTS ###############################
        i = {n: [i for i in n.inputs][0] for n in group}
        o = {n: [o for o in n.outputs][0] for n in group}

        reduced_timesteps = [x for x in m.TIMESTEPS if x > 0]

        def _inv_storage_init_content_max_rule(block, n):
            """Constraint for a variable initial storage capacity."""
            return (
                block.init_content[n]
                <= n.investment.existing + block.invest[n]
            )

        self.init_content_limit = Constraint(
            self.INVESTSTORAGES_NO_INIT_CONTENT,
            rule=_inv_storage_init_content_max_rule,
        )

        def _inv_storage_init_content_fix_rule(block, n):
            """Constraint for a fixed initial storage capacity."""
            return block.init_content[n] == n.initial_storage_level * (
                n.investment.existing + block.invest[n]
            )

        self.init_content_fix = Constraint(
            self.INVESTSTORAGES_INIT_CONTENT,
            rule=_inv_storage_init_content_fix_rule,
        )

        def _storage_balance_first_rule(block, n):
            """
            Rule definition for the storage balance of every storage n for the
            first time step.
            """
            expr = 0
            expr += block.storage_content[n, 0]
            expr += (
                -block.init_content[n]
                * (1 - n.loss_rate[0]) ** m.timeincrement[0]
            )
            expr += (
                n.fixed_losses_relative[0]
                * (n.investment.existing + self.invest[n])
                * m.timeincrement[0]
            )
            expr += n.fixed_losses_absolute[0] * m.timeincrement[0]
            expr += (
                -m.flow[i[n], n, 0] * n.inflow_conversion_factor[0]
            ) * m.timeincrement[0]
            expr += (
                m.flow[n, o[n], 0] / n.outflow_conversion_factor[0]
            ) * m.timeincrement[0]
            return expr == 0

        self.balance_first = Constraint(
            self.INVESTSTORAGES, rule=_storage_balance_first_rule
        )

        def _storage_balance_rule(block, n, t):
            """
            Rule definition for the storage balance of every storage n for the
            every time step but the first.
            """
            expr = 0
            expr += block.storage_content[n, t]
            expr += (
                -block.storage_content[n, t - 1]
                * (1 - n.loss_rate[t]) ** m.timeincrement[t]
            )
            expr += (
                n.fixed_losses_relative[t]
                * (n.investment.existing + self.invest[n])
                * m.timeincrement[t]
            )
            expr += n.fixed_losses_absolute[t] * m.timeincrement[t]
            expr += (
                -m.flow[i[n], n, t] * n.inflow_conversion_factor[t]
            ) * m.timeincrement[t]
            expr += (
                m.flow[n, o[n], t] / n.outflow_conversion_factor[t]
            ) * m.timeincrement[t]
            return expr == 0

        self.balance = Constraint(
            self.INVESTSTORAGES, reduced_timesteps, rule=_storage_balance_rule
        )

        def _balanced_storage_rule(block, n):
            return (
                block.storage_content[n, m.TIMESTEPS[-1]]
                == block.init_content[n]
            )

        self.balanced_cstr = Constraint(
            self.INVESTSTORAGES_BALANCED, rule=_balanced_storage_rule
        )

        def _power_coupled(block, n):
            """
            Rule definition for constraint to connect the input power
            and output power
            """
            expr = (
                m.InvestmentFlow.invest[n, o[n]]
                + m.flows[n, o[n]].investment.existing
            ) * n.invest_relation_input_output == (
                m.InvestmentFlow.invest[i[n], n]
                + m.flows[i[n], n].investment.existing
            )
            return expr

        self.power_coupled = Constraint(
            self.INVEST_REL_IN_OUT, rule=_power_coupled
        )

        def _storage_capacity_inflow_invest_rule(block, n):
            """
            Rule definition of constraint connecting the inflow
            `InvestmentFlow.invest of storage with invested capacity `invest`
            by nominal_storage_capacity__inflow_ratio
            """
            expr = (
                m.InvestmentFlow.invest[i[n], n]
                + m.flows[i[n], n].investment.existing
            ) == (
                n.investment.existing + self.invest[n]
            ) * n.invest_relation_input_capacity
            return expr

        self.storage_capacity_inflow = Constraint(
            self.INVEST_REL_CAP_IN, rule=_storage_capacity_inflow_invest_rule
        )

        def _storage_capacity_outflow_invest_rule(block, n):
            """
            Rule definition of constraint connecting outflow
            `InvestmentFlow.invest` of storage and invested capacity `invest`
            by nominal_storage_capacity__outflow_ratio
            """
            expr = (
                m.InvestmentFlow.invest[n, o[n]]
                + m.flows[n, o[n]].investment.existing
            ) == (
                n.investment.existing + self.invest[n]
            ) * n.invest_relation_output_capacity
            return expr

        self.storage_capacity_outflow = Constraint(
            self.INVEST_REL_CAP_OUT, rule=_storage_capacity_outflow_invest_rule
        )

        def _max_storage_content_invest_rule(block, n, t):
            """
            Rule definition for upper bound constraint for the
            storage content.
            """
            expr = (
                self.storage_content[n, t]
                <= (n.investment.existing + self.invest[n])
                * n.max_storage_level[t]
            )
            return expr

        self.max_storage_content = Constraint(
            self.INVESTSTORAGES,
            m.TIMESTEPS,
            rule=_max_storage_content_invest_rule,
        )

        def _min_storage_content_invest_rule(block, n, t):
            """
            Rule definition of lower bound constraint for the
            storage content.
            """
            expr = (
                self.storage_content[n, t]
                >= (n.investment.existing + self.invest[n])
                * n.min_storage_level[t]
            )
            return expr

        # Set the lower bound of the storage content if the attribute exists
        self.min_storage_content = Constraint(
            self.MIN_INVESTSTORAGES,
            m.TIMESTEPS,
            rule=_min_storage_content_invest_rule,
        )

        def maximum_invest_limit(block, n):
            """
            Constraint for the maximal investment in non convex investment
            storage.
            """
            return (
                n.investment.maximum * self.invest_status[n] - self.invest[n]
            ) >= 0

        self.limit_max = Constraint(
            self.NON_CONVEX_INVESTSTORAGES, rule=maximum_invest_limit
        )

        def smallest_invest(block, n):
            """
            Constraint for the minimal investment in non convex investment
            storage if the invest is greater than 0. So the invest variable
            can be either 0 or greater than the minimum.
            """
            return (
                self.invest[n] - (n.investment.minimum * self.invest_status[n])
                >= 0
            )

        self.limit_min = Constraint(
            self.NON_CONVEX_INVESTSTORAGES, rule=smallest_invest
        )

    def _objective_expression(self):
        """Objective expression with fixed and investement costs."""
        if not hasattr(self, "INVESTSTORAGES"):
            return 0

        investment_costs = 0

        for n in self.CONVEX_INVESTSTORAGES:
            investment_costs += self.invest[n] * n.investment.ep_costs
        for n in self.NON_CONVEX_INVESTSTORAGES:
            investment_costs += (
                self.invest[n] * n.investment.ep_costs
                + self.invest_status[n] * n.investment.offset
            )
        self.investment_costs = Expression(expr=investment_costs)

        return investment_costs

        
"""        
if(t%n.disposalPeriods==0):
                
    
                
    expr+=block.storage_content[n, t]
    
"""
```
</details>
