# Cooling Components

File with all Components, allocated to the cooling sector. Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import cooling_components as cool_comp`

Call of the components:

```
componentObject = cool_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`coolingdemand = cool_comp.Demand(sector=consumer1.getCoolingSector(), demand_timeseries=consumer1.input_data['coolingDemand'])`

## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'Coolingdemand_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_coolingDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            coolingDemand = solph.Sink(label=self.label, inputs={self.sector: q_coolingDemand})
            
            return coolingDemand
```

## Demand

Cooling Demand given as timeseries, which can be given in the input data.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Coolingdemand_ + position |
| Heat Demand | `demand_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'Coolingdemand_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_coolingDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            coolingDemand = solph.Sink(label=self.label, inputs={self.sector: q_coolingDemand})
            
            return coolingDemand


```
</details>

## Grid

Input component to cover the remaining required cooling energy.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Coolinggrid_ + position |

</details>

<details>
<summary>Code</summary>

```

class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Coolinggrid' + self.position
        self.costs=0
        
        
    def component(self):
        q_coolingGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs)
            
        coolGrid = solph.Source(label=self.label, outputs={self.sector: q_coolingGrid})
        
        return coolGrid  

```
</details>

## Grid Emissions

Additionally emissions as output

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Sector | `sector` | / |
| Sector Emissions | `sector_emissions` | 0 |
| Emissions | `emissions` | 0.28 |
| Maximum emissions | `max_emissions` | 1000000 |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Coolinggrid_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0.28
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Coolinggrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Cooling\nGrid_' + self.position
        self.costs_model=kwargs.get("costs_model", costs)
        self.color = 'slategrey'
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_coolingGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            coolGrid = solph.Source(label=self.label, outputs={self.sector_out: q_coolingGrid})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            coolGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_coolingGrid, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return coolGrid

```
</details>

## Grid Purchase

Purchase from the district cooling grid with power and energy costs. The power costs are evaluated with investment flows, assuming no power has been installed yet. If only a certain share of power costs should be considered, the parameter `'existing'` can be set. The energy costs are given to the model as timeseries.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Power Costs | `costs_power` | 31 |
| Energy Costs | `costs_energy` | Numpay Array with 0 |
| Sector | `sector` | / |
| Sector Emissions | `sector_emissions` | 0 |
| Emissions | `emissions` | 0.28 |
| Maximum emissions | `max_emissions` | 1000000 |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Coolinggridpurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        existingGrid = 0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Coolinggridpurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_energy = kwargs.get("costs_energy", costsEnergyArray)
        
        
    def component(self):
        q_coolGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_energy,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))
            
        coolGrid = solph.Source(label=self.label, outputs={self.sector: q_coolGridpurchase})
        
        return coolGrid   

```
</details>

## Grid Purchase Emissions

Additional emissions are considered.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Power Costs | `costs_power` | 31 |
| Energy Costs | `costs_energy` | Numpay Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Coolinggridpurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        existingGrid=0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Coolinggridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Cooling\nGrid\nPurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        self.color='slategrey'
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
        
    def component(self):
        q_coolGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))

        
        if(self.emissions==0 or self.sector_emissions==0):
            
            coolGrid = solph.Source(label=self.label, outputs={self.sector_out: q_coolGridpurchase})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            coolGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_coolGridpurchase, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return coolGrid
        

```
</details>
