# Maldet Energysystemmodel

Optimization model of an energysystem derived from the open modelling framework OEMOF. The model is component based. All the energy system components that shall be used must be included into the model. In the end, an optimisation that minimized variable parameters (e.g. costs) is carried out.

## Open Modelling Framework OEMOF
The model is derived from this framework. It is open source and can be further developed. Mainly, the code is based on the OEMOF solph package.

For the documentation, see [OEMOF](https://oemof.readthedocs.io/en/latest/).

For the source code in Github, see [OEMOF-Github](https://github.com/oemof).

For OEMOF solph package, see [OEMOF-Solph](https://github.com/oemof/oemof-solph).

For examples, see [OEMOF-Examples](https://github.com/oemof/oemof-examples).

## Getting started

### First, the required modules must be included:


```
import pandas as pd

import numpy as np

import oemof.solph as solph
```


### Second, all the required classes (components) must be included.

`import class_file_name`

It is also recommended to define variables for the considered timesteps and for the total timesteps of the considered period:

```
timesteps=...
totalTimesteps=...
```


The frequency must be defined for the energysystem. This can be done as follows:

```
if(deltaT==1):
    frequency = 'H'
elif(deltaT==0.25):
    frequency = '0.25H'
else:
    frequency= 'H'
```


After that, an energysystem instance must be created:


```
my_index = pd.date_range('1/1/2020', periods=timesteps, freq=frequency)
    
my_energysystem = solph.EnergySystem(timeindex=my_index)
```


Components can now be added to the model (energysystem).

## Add Consumers

For detailed info, see Consumer [Sourcecode](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/Consumer.py).

A consumer is represented by the class Consumer. This represents a location in the energysystem. Each consumer has its own energysystems for different sectors, that can be interconnected with other consumers. It can be defined, which sectors are considered for the specific consumer.


**A consumer is added to the model as follows:**

```
consumer1 = Consumer.Consumer(timesteps=timesteps, filename='input_data.xlsx')
consumer1.addAllSectors()
consumer1.sector2system(my_energysystem)
```

New consumer sectors can also be defined apart from pre-defined ones.

```
consumer1.getWaterdemandSector()
consumer1.getSector('testsector')
```

Multiple consumers can be added to the system. All sectors that are available for the consumers must be appended to the consumers. 
Consumer data are provided via input Excel files see [File](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/input_data.xlsx).

Consumer data can be accessed as follows:

```
consumer1.input_data['identifyer']
```

To get the consumer sector, the sector functions must be called.

```
consumer1.getWaterdemandSector()
```

## Add Components to the energysystem

When a consumer and an energy system are defined, the components can be added to both.
Multiple components are available in the RUTIS model. To add a component to a consumer, the following workflow is required.

Define component:
```
pv = el_comp.Photovoltaic(sector=consumer1.getElectricitySector(), generation_timeseries=consumer1.input_data['elGen'], 
                          label='PV_' + consumer1.label, timesteps=timesteps, area_efficiency=0.5, area_roof=20, area_factor=10) 

```

Add component to energy system:
```
my_energysystem.add(pv.component())
```

Add component to consumer:
```
consumer1.addComponent(pv)
```

### Component logic
Components are defined in classes. When calling a component, certain parameter defined in the components can be set. Otherwise, default values are assumed. An example is provided in the following code.

```
self.max_emissions= kwargs.get("max_emissions", 0)
```
If the parameter max_emissions is set when calling a component:

```
emissionSink = emission_components.Sink(max_emissions=1000)
```

the parameter is set to this value. Otherwise it is set to the default value (zero in the example). This is done in such way for all available components.

### Available Components
Components are frequently added to the RUTIS model. Components are separated dependent on their sector. The available Components can be seen in the source code. click the following links to see the source code of the sectors.

[Electricity Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/electricity_components.py)

[Cooling Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/cooling_components.py)

[Emission Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/emission_components.py)

[Gas Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/gas_components.py)

[Heat Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/heat_components.py)

[Household Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/household_components.py)

[Hydrogen Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/hydrogen_components.py)

[LSC Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/lsc_components.py)

[Transport Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/transport_components.py)

[Water Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/water_components.py)

[Waste Components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/waste_components.py)

It is important to note that the LSC components include LSC trading components. They are applied as follows.

```
elec2lsc = lsc_comp.Electricity2LSC(sector_in=consumer1.getElectricitySector(), sector_out=LSC.getElectricitySector(),
                                    P=11, label="Elec2LSC_" + consumer1.label, rev_consumer=sell_tariff_lsc_elec, c_lsc=sell_tariff_lsc_elec)            
        
lsc2elec = lsc_comp.LSC2electricity(sector_in=LSC.getElectricitySector(), sector_out=consumer1.getElectricitySector(),
                                    P=11, label="LSC2elec_" + consumer1.label, rev_lsc=sell_tariff_lsc_elec, c_consumer=buy_tariff_lsc_elec)            

```

## Investment components
RUTIS included the option for investment decision analyses. This is implemented by OEMOF investment flow. Similar to operational components, pre-defined investment components are provided for simple implementation (see code).

```
wasteCombustion_invest = investment_comp.Wastecombustion_Investment(sector_in=lsm.getWasteSector(), 
                                                                        sector_out=[lsm.getElectricitySector(), lsm.getHeatSector()], sector_emissions=lsm.getEmissionsSector(),
                                             c_in=0, c_out=[0.04, 0.04], conversion_factor=[0.35, 0.4], P_in=wastecomb_P_in, P_out=wastecomb_P_out, 
                                             usable_energy=wastecomb_usable_energy, label='Wastecombustion_' + lsm.label, color='lightsteelblue', timestepsTotal=timestepsTotal, timesteps=timesteps)
```

[Investment components](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/investment_components.py)

## Non-standard Components
OEMOF provides various components and functionalities. The majority of RUTIS components is directly derived from OEMOF components. However, not all functionalities can be included. Some OEMOF components have been extended and newly developed. They are directly used in the sector components in RUTIS. Source code for the new components can be found in the following list.

[Storage with disposal periods](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/StorageIntermediate.py)

[Source with emissions](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/source_emissions.py)

[Transformer with multiple inputs](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/transformerLosses.py)

[Transformer with multiple inputs and outputs](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/transformerLossesMultipleOut.py)

[Delay Component](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/transportDelay.py)



## Solve the Model
After adding all the components to the energy system, the model can be set up.

```
saka = solph.Model(my_energysystem) 
```

It is possible to save a file with constraints.

```
saka.write(file2write, io_options={'symbolic_solver_labels': True})
```

When all additional constraints are added (see next section), the model can be solved.

```
saka.solve(solver=solver, solve_kwargs={'tee': True})
```

Results can then be processed.
```
my_energysystem.results = solph.processing.results(saka)
```

The results can be saved in a file and also restored. See [Source](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/methods.py)
```
meth.store_results(my_energysystem, saka, scenario)
results = meth.restore_results(scenario)
```


## Additional Constraints 

Additional constraints can be added to the model after setting up the energy system.
Multiple additional constraints can be added. They are called similar to model blocks by setting certain parameters. These must be set so that the method works.

An example of adding an additional constraint is provided as follows:

```
constraints_lsc.flow_limitation_excesspurchase(model=saka, pipeline_limited=consumer_lsc.get_component_by_name(components=consumer1.components, name='PipelinepurchaseLimited'), 
                                                      pipeline_excess=consumer_lsc.get_component_by_name(components=consumer1.components, name='PipelinepurchaseExcess'))
```

The available constraints can be seen in the source codes in the following list:

[Flexibility summation](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/constraints.py)

[Binary transport balance and chargeblock](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/constraints_add_binary.py)

[LSC waterlimit and trading limit](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/constraints_lsc.py)

[General waterlimits](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/constraints_waterlimit.py)

[LSM constraints](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/consumer_lsm.py)


## Methods
Several supporting methods are developed that can be used within the framework. These include mainly writing of results to structs, xlsx and csv.

An overview can be found in [methods](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/methods.py)

## Result processing
Methods to get the flows out of a model for all consumers are available for a solved model. The results are saved in csv files in target folders.

Cost evaluation:
```
resultprocessing_lsc.costs_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                             consumer10, consumer11, consumer12, LSC], 
                                  results=results, filename='costs.csv', scenarioname=scenario, model=saka, timeseries=False)
```

Flow evaluation:
```
resultprocessing_lsc.consumer_to_csv(consumers=[consumer1, consumer2, consumer3, consumer4, consumer5, consumer6, consumer7, consumer8, consumer9, 
                                                consumer10, consumer11, consumer12, LSC], 
                                     results=results, filename='technology.csv', scenarioname=scenario)
```

Saving as excel is also possible, but not recommended as is takes much longer.
For the available methods, see [Source](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/resultprocessing_lsc.py)

## Plots
Multiple plot functions are available. Their use is described in the Example [Code](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/run_plots_phase2_csv.py)

The plots are defined in [Source](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/plots_phase2_csv.py)

## LSC Consumer Method
A method for a standard LSC consumer is available. Standard components are defined. Parameters and components can be changed by specific calls. The following example provides the use of the method.

```
consumer_lsc.consumer_setup(  consumer=consumer7, LSC=LSC, timesteps=timesteps, red_local_elec=0, c_grid_heat=0.001, c_grid_cool=0.001,
                                                    deactivate_electricitygridpurchase=True, deactivate_electricitygridpurchase_combined=False, deactivate_pv=False, deactivate_electricitygridFeedin=False, deactivate_battery=True, deactivate_heatpump=True, deactivate_elcool=True, 
                                                    deactivate_thermalStorage=True, waterFlexibility=waterFlexibility7, wasteFlexibility=wasteFlexibility7, wasteStorageHousehold=wasteStorageHousehold7, transmissionWaste=transmissionWaste7, pv=pv, transmissionWasteEmissions=transmissionWasteEmissions7)
```

The detailed information can be found in the [Code](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/consumer_lsc.py)

## LSM Consumer Method
Similar to LSC components, an LSM consumer extension is provided. A consumer can be implemented as follows.

```
LSM_list = [lsm1, lsm2, lsm3, lsm4, lsm5]
IDlsc = lsc1.label
lsccomponents1 = consumer_lsm_cluster.lsc_setup(LSC=lsc1, LSM=lsm1, LSM_list=LSM_list, timesteps=timesteps, timestepsTotal=timestepsTotal, 
                                        c_grid_elec=lscData[IDlsc]["c_grid_elec"], c_abgabe_elec=lscData[IDlsc]["c_abgabe_elec"], 
                                        c_grid_power=lscData[IDlsc]["c_grid_power"], P_elgrid=lscData[IDlsc]["P_elgrid"], em_elgrid=lscData[IDlsc]["em_elgrid"], 
                                        area_efficiency=lscData[IDlsc]["area_efficiency"], area_roof=lscData[IDlsc]["area_roof"], area_factor=lscData[IDlsc]["area_factor"],
                                        battery_soc_max=lscData[IDlsc]["battery_soc_max"], battery_soc_start=lscData[IDlsc]["battery_soc_start"], 
                                        battery_P_in=lscData[IDlsc]["battery_P_in"], battery_P_out=lscData[IDlsc]["battery_P_out"],
                                        sell_tariff_lsc_elec=lscData[IDlsc]["sell_tariff_lsc_elec"], efficiency_lsc_elec=lscData[IDlsc]["efficiency_lsc_elec"], P_elec2lsm=lscData[IDlsc]["P_elec2lsm"],
                                        rev_tariff_lsm_elec=lscData[IDlsc]["rev_tariff_lsm_elec"], buy_tariff_lsm_elec=lscData[IDlsc]["buy_tariff_lsm_elec"], 
                                        efficiency_lsm_elec=lscData[IDlsc]["efficiency_lsm_elec"], P_lsm2elec=lscData[IDlsc]["P_lsm2elec"],
                                        P_heatpump_in=lscData[IDlsc]["P_heatpump_in"], P_heatpump_out=lscData[IDlsc]["P_heatpump_out"],
                                        P_heatstore=lscData[IDlsc]["P_heatstore"], v_heatstore=lscData[IDlsc]["v_heatstore"], 
                                        P_districtheat=lscData[IDlsc]["P_districtheat"], efficiency_lsc_heat=lscData[IDlsc]["efficiency_lsc_heat"],
                                        rev_tariff_lsm_heat=lscData[IDlsc]["rev_tariff_lsm_heat"], buy_tariff_lsm_heat=lscData[IDlsc]["buy_tariff_lsm_heat"], efficiency_lsm_heat=lscData[IDlsc]["efficiency_lsm_heat"],
                                        v_max_wastestore=lscData[IDlsc]["v_max_wastestore"], v_start_wastestore=lscData[IDlsc]["v_start_wastestore"], 
                                        waste_disposal_periods=lscData[IDlsc]["waste_disposal_periods"], wastestore_balanced=lscData[IDlsc]["wastestore_balanced"], 
                                        waterpurchase_limit=lscData[IDlsc]["waterpurchase_limit"], waterpurchase_discount=lscData[IDlsc]["waterpurchase_discount"],
                                        lsmwaterpurchase_limit=lscData[IDlsc]["lsmwaterpurchase_limit"], lsmwaterpurchase_discount=lscData[IDlsc]["lsmwaterpurchase_discount"], 
                                        rev_tariff_lsm_water=lscData[IDlsc]["rev_tariff_lsm_water"], buy_tariff_lsm_water=lscData[IDlsc]["buy_tariff_lsm_water"], efficiency_lsm_water=lscData[IDlsc]["efficiency_lsm_water"],
                                        wff=lscData[IDlsc]["wff"], revenues_waterreduction=lscData[IDlsc]["revenues_waterreduction"], co2price=lscData[IDlsc]["co2price"], wastestore_empty_end=lscData[IDlsc]["wastestore_empty_end"]
                                        
                                        )
```

LSC and LSM input data can be provided in form of structs, as presented in the code: [LSC struct](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/LSC_inputparameter.py), [LSM struct](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/LSM_inputparameter.py)


## Clustering

Optimization in the RUTIS framework does not necessarily have to be performed over a year. Data clustering methods with K-means clustering are provided. Monthly clustering and total clustering is implemented

The method to data clustering can be applied in consumer definition.

```
data_clustered=clustering.cluster_data_month(timestepsTotal=timestepsTotal, data=data, list_consumers=list_consumers, sorting=True, clusterPerMonth=clusterPerMonth, max_begin=False)
consumer.clustering(data=data_clustered[consumer.label])
```

It is implemented in the [Consumer](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/Consumer.py) class.


For detailed information, see [clustering](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/clustering.py)



## Application
For the application of the code, test scenarios are provided.

[Test scenario Sector Coupling](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/test_versuche.py)

[Test scenario LSC](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/test_phase2.py)


## Clustering-based bi-level optimization

Approach with two steps. First step portfolio optimization with clustered data.
Second steps, operational optimization with capacity inputs from first step.

[LSM clustering](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/OEMOF_EnergysystemPlanning/LSM_clustering.py)


## Input data

The input data are provided as xlsx timeseries. This section provides the assumptions for input data that should be seen as potential recommendations for users of the model. However, in the end it is up to the user which model input timeseries are used.

###  Electricity demand

For the electricity demand, standard load profiles are assumed.

### Heating, hot water and cooling demand

The hot water demand timeseries are generated with hotmaps: 

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu

For the heating demand, similar profiles from hotmaps are used: 

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu

The same counts for cooling: 

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu

### Water demand

The water demand is based on similar standard load profiles as for electricity. Alternatively, a constant water demand is assumed.


### Waste 

Accruing waste is assumed with a constant value over the year as due to the possibility of storing waste, the timeseries have less impact than in other sectors.


### Gas and hydrogen demand

Gas demand and hydrogen demand are assumed based on synthetic load profiles.


### Transport demand

Transport demands are generated with emobpy: 

Gaete-Morales, C., Kramer, H., Schill, WP. et al. An open tool for creating battery-electric vehicle time series from empirical data, emobpy. Sci Data 8, 152 (2021). https://doi.org/10.1038/s41597-021-00932-9


### PV generation timeseries

PV generation timeseries are generated with renewables ninja: 

Stefan Pfenninger and Iain Staffell (2016). Long-term patterns of European PV output using 30 years of validated hourly reanalysis and satellite data. Energy 114, pp. 1251-1265. doi: 10.1016/j.energy.2016.08.060


### Heat pump generation

Heat pump generation is based on regression and similarity analyses. The initial regression equation was defined in: 

Carlo Corinaldesi, Daniel Schwabeneder, Georg Lettner, Hans Auer, A rolling horizon approach for real-time trading and portfolio optimization of end-user flexibilities, Sustainable Energy, Grids and Networks, Volume 24, 2020, 100392, ISSN 2352-4677, https://doi.org/10.1016/j.segan.2020.100392.


### Rainfall

Rainfall input is assumed based on wether events.


### Costs

Cost input timeseries are assumed based on current market data. As these are steadily changing, concrete implementation inputs do not make sense.

