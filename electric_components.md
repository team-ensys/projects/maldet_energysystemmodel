# Electric Components

File with all Components, allocated to the electricity sector. Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import electric_components as elec_comp`

Call of the components:

```
componentObject = el_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`p2h = el_comp.Power2Heat(sector_in = consumer1.getElectricitySector(), sector_out=consumer1.getHeatSector()) `

## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
def component(self):
            q_p2hel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_p2hth = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            p2h = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_p2hel},
                outputs={self.sector_out : q_p2hth},
                conversion_factors={self.sector_out : self.conversion_factor})
            return p2h
```


## Power to Heat

Power to Heat Component --> Simple Transformer

**Input: Electricity**

**Output: Heat**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.0005 |
| Output costs | `c_out` | 0 |
| Conversion factor | `conversion_factor` | 0.95 |
| Input Power Limit | `P_in` | 360 |
| Output Power Limit | `P_out` | 360 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | P2H_ + position |

</details>

<details>
<summary>Code</summary>

```

    class Power2Heat():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.95
            costsIn=0.0005
            costsOut=0
            PowerInLimit = 360
            PowerOutLimit = 360
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'P2H_' + self.position

        def component(self):
            q_p2hel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_p2hth = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            p2h = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_p2hel},
                outputs={self.sector_out : q_p2hth},
                conversion_factors={self.sector_out : self.conversion_factor})
            return p2h


```
</details>


## Battery

Battery Component --> Generic Storage Block

**Input: Electricity**

**Output: Electricity**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.003 |
| Output costs | `c_out` | 0.003 |
| Input Efficiency | `eta_in` | 0.95 |
| Output Efficiency | `eta_out` | 0.95 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Maximum SOC | `soc_max` | 10 |
| Start SOC | `soc_start` | 10 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Input Power Limit | `P_in` | 10 |
| Output Power Limit | `P_out` | 10 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Battery_ + position |
| Battery Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class Battery():
    
    def __init__(self, *args, **kwargs):
        P_inMax = 10
        P_outMax = 10
        costsIn = 0.003
        costsOut=0.003
        Eta_in = 0.95
        Eta_out = 0.95
        Eta_sb = 0.999
        SOC_max = 10
        SOC_start = 10
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_in = kwargs.get("P_in", P_inMax)
        self.P_out = kwargs.get("P_Out", P_outMax)
        self.soc_start = kwargs.get("SOC_start", SOC_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.soc_max = kwargs.get("soc_max", SOC_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'Battery_' + self.position
        self.balanced = kwargs.get("balanced", True)

    def component(self):
        #Energy Flows
        q_elec2battery = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
        q_battery2elec = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)

            
        battery = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_elec2battery},
                outputs={self.sector_out: q_battery2elec},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return battery

```
</details>


## Heatpump

Heatpump Component --> Simple Transformer

**Input: Electricity**

**Output: Heat**

Conversion factors must be given as timeseries. If they are read from the input file, this is done automatically.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.0015 |
| Conversion factor | `conversion_factor` | Numpy Array with Zeros |
| Input Power Limit | `P_in` | 7 |
| Output Power Limit | `P_out` | 7 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Heatpump_ + position |

</details>

<details>
<summary>Code</summary>

```

    class Heatpump():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsIn=0
            costsOut=0.0015
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            

            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'Heatpump_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("conversion_timeseries", generationArray)

        def component(self):
            q_HPel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_HPth = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            heatpump = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_HPel},
                outputs={self.sector_out : q_HPth},
                conversion_factors={self.sector_out : self.conversion_timeseries})
            return heatpump


```
</details>


## Grid

Input component to cover the remaining required electric energy. Additionally a parameter grid is set to true, to mark the component as a grid that can have advantages for plot and costs functions.
For grids it is differed between model costs, used for the optimization, and costs that are used for the result presentation.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 11 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Electricitygrid_ + position |
| Label | `costs` | 1000 |
| Label | `costs_model` | costs |

</details>

<details>
<summary>Code</summary>

```

class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Electricitygrid_' + self.position
        self.costs=0
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs)
            
        elGrid = solph.Source(label=self.label, outputs={self.sector: q_electricityGridfeedin})
        
        return elGrid


```
</details>

## Grid Emissione

Emissions can be set as additional outputs for the grid, based on emission factors in the electricity mix.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 11 |
| Sector | `sector` | / |
| Sector for Emissions | `sector_emissions` | 0 |
| Emissions | `emisisons` | 0 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Electricitygrid_ + position |
| Label | `costs` | 1000 |
| Label | `costs_model` | costs |

</details>

<details>
<summary>Code</summary>

```

class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Electricitygrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Electricity\nGrid_' + self.position
        self.costs_model=kwargs.get("costs_model", costs)
        self.color = 'slategrey'
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            elGrid = solph.Source(label=self.label, outputs={self.sector_out: q_electricityGridfeedin})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            elGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_electricityGridfeedin, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return elGrid


```
</details>



## Grid Purchase

Purchase from the electricity grid with power and energy costs. The power costs are evaluated with investment flows, assuming no power has been installed yet. If only a certain share of power costs should be considered, the parameter `'existing'` can be set. The energy costs are given to the model as timeseries.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 11 |
| Power Costs | `costs_power` | 35 |
| Energy Costs | `costs_energy` | Numpay Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Electricitygridpurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        C_power = 35
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        existingGrid = 0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Electricitygridpurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_energy = kwargs.get("costs_energy", costsEnergyArray)
        
        
    def component(self):
        q_electricityGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_energy,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))
            
        elGrid = solph.Source(label=self.label, outputs={self.sector: q_electricityGridpurchase})
        
        return elGrid     


```
</details>

## Grid Purchase Emissions

Additional Emissions Considered

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 11 |
| Power Costs | `costs_power` | 35 |
| Energy Costs | `costs_energy` | Numpay Array with 0 |
| Sector | `sector` | / |
| Sector for Emissions | `sector_emissions` | 0 |
| Emissions | `emisisons` | 0 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Electricitygridpurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        C_power = 35
        deltaT=1
        position='Consumer_1'
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        existingGrid=0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Electricitygridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Electricity\nGrid\nPurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        self.color='slategrey'
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
        
    def component(self):
        q_electricityGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))

        
        if(self.emissions==0 or self.sector_emissions==0):
            
            elGrid = solph.Source(label=self.label, outputs={self.sector_out: q_electricityGridpurchase})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            elGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_electricityGridpurchase, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return elGrid    


```
</details>

## Grid Sink

Sink for the excess electricity.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 11 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | ElectricitygridSink_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridSink():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'ElectricitygridSink_' + self.position
        self.costs=0
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs)
            
        elGrid = solph.Sink(label=self.label, inputs={self.sector: q_electricityGridfeedin})
        
        return elGrid


```
</details>

## Grid Feedin

Feedin excess electricity with corresponding feed-in tariffs, that are given as timeseries.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 11 |
| Energy Costs | `revenues` | Numpay Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | ElectricitygridFeedin_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridFeedin():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'ElectricitygridFeedin_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        revenuesEnergyArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("revenues", revenuesEnergyArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.revenues)
            
        elGridFeedIn = solph.Sink(label=self.label, inputs={self.sector: q_electricityGridfeedin})
        
        return elGridFeedIn


```
</details>


## Demand

Electricity Demand given as timeseries, which can be given in the input data.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Electricitydemand_ + position |
| Electricity Demand | `demand_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class GridFeedin():
    
    def __init__(self, *args, **kwargs):
        P_connection = 11
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'ElectricitygridFeedin_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        revenuesEnergyArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("revenues", revenuesEnergyArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        q_electricityGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.revenues)
            
        elGridFeedIn = solph.Sink(label=self.label, inputs={self.sector: q_electricityGridfeedin})
        
        return elGridFeedIn


```
</details>

## Groundwaterwell

Groundwaterwell Component --> Simple Transformer

**Input: Electricity**

**Output: Water**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0 |
| Height/Depth | `h_max` | 10 |
| Input Power Limit | `P_max` | 1.02 |
| Output Flow Limit | `Q_max` | 3 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Groundwaterwell_ + position |

</details>

<details>
<summary>Code</summary>

```

class Groundwaterwell():
    
    def __init__(self, *args, **kwargs):
        P = 0.1635
        Q = 3
        eta = 0.5
        h = 10
        deltaT=1
        costsIn=0
        costsOut=0
        position='Consumer_1'
        rho_H2O = 1000
        g = 9.81
        
        self.P= kwargs.get("P_max", P)
        self.Q= kwargs.get("Q_max", Q)
        self.h= kwargs.get("h_max", h)
        self.eta= kwargs.get("eta", eta)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Groundwaterwell_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Groundwaterwell_' + self.position
        self.costs_in=kwargs.get("c_in", costsIn)
        self.costs_out=kwargs.get("c_out", costsOut)
        self.conversionFactor = rho_H2O*g*self.h/self.eta
        self.color='darkseagreen'
        
    def component(self):
        q_GroundwaterwellEl = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_in)
        v_GroundwaterwellWater = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs=self.costs_out)
            
        groundwaterwell = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_GroundwaterwellEl},
                outputs={self.sector_out : v_GroundwaterwellWater},
                conversion_factors={self.sector_out : 3600000/self.conversionFactor})


```
</details>

## Pipelinepump

Pipeline Pump Component --> Simple Transformer

**Input: Electricity**

**Output: Water**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.58 |
| Conversion Factor | `conversion_factor` | 1/0.037 |
| Input Power Limit | `P_max` | 18.2 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Pipelinepump_ + position |

</details>

<details>
<summary>Code</summary>

```

class Pipelinepump():
    
    def __init__(self, *args, **kwargs):
        P = 18.2
        conversion_factor = 1/0.337
        deltaT=1
        costsIn=0
        costsOut=0.58
        position='Consumer_1'

        
        self.P= kwargs.get("P_max", P)
        self.sector_in= kwargs.get("sector_in")
        self.sector_out= kwargs.get("sector_out")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Pipelinepump_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Pipelinepump_' + self.position
        self.costs_in=kwargs.get("c_in", costsIn)
        self.costs_out=kwargs.get("c_out", costsOut)
        self.conversion_factor = kwargs.get("conversion_factor", conversion_factor)
        self.color='dodgerblue'
        
    def component(self):
        q_PipelinepumpEl = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_in)
        v_PipelinepumpWater = solph.Flow(min=0, max=1, nominal_value=self.P/self.conversion_factor*self.deltaT, variable_costs=self.costs_out)
            
        pipelinepump = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_PipelinepumpEl},
                outputs={self.sector_out : v_PipelinepumpWater},
                conversion_factors={self.sector_out : self.conversion_factor})
        
        return pipelinepump


```
</details>

## Electric Cooling

Electric Cooling Component, where the conversion factors are given as timeseries (given in the input_data). Additionally, an efficiency is assumed.

**Input: Electricity**

**Output: Cooling**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.002 |
| Conversion timeseries | `conversion_timeseries` | Numpy Array with 0 |
| Conversion efficiency | `eta` | 0.95 |
| Input Power Limit | `P_in` | 7 |
| Output Power Limit | `P_out` | 7 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | ElectricCooling_ + position |

</details>

<details>
<summary>Code</summary>

```

    class ElectricCooling():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            costsIn=0
            costsOut=0.002
            efficiency=0.95
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps = 8760
            

            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.eta = kwargs.get("eta", efficiency)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'ElectricCooling_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            generationArray = np.zeros(self.timesteps)
            
            self.conversion_timeseries = kwargs.get("conversion_timeseries", generationArray)
            self.conversion = np.multiply(self.conversion_timeseries, self.eta)

        def component(self):
            q_elCoolel = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_elCoolcool = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            elcool = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_elCoolel},
                outputs={self.sector_out : q_elCoolcool},
                conversion_factors={self.sector_out : self.conversion})
            return elcool


```
</details>


## PV Generation

PV Generation Block, with given timeseries for generation (input_data). The limit of the PV is given with the rooftop area and the m² per kWp. It is assumed that the entire roof is covered in PV.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0 |
| Generation timeseries | `generation_timeseries` | Numpy Array with 0 |
| m² per kWp | `area_factor` | 10 |
| Rooftop Area | `area_roof` | 100 |
| Sector | `sector` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Photovoltaic_ + position |

</details>

<details>
<summary>Code</summary>

```

    def component(self):
            
            Q_pvGenerationValue = solph.Flow(fix = self.conversion_timeseries, nominal_value=self.Area_Roof/self.Area_Factor)
            
            pvGeneration = solph.Source(label=self.label, outputs={self.sector: Q_pvGenerationValue})
            
            return pvGeneration

```
</details>

## Electrolysis

Electrolysis is a special case, as two input sectors are needed. The second input flow is dependent on the first input flow. A transformer loss block [TransformerLosses](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/TransformerLosses.md) is included in OEMOF. Additionally to the input sector and the output sector, the second input sector (losses sector) must be given. Conversion and loss factor must be given as separate parameters.

**Input: Electricity**

**Output: Hydrogen**

**Losses: Water**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.068 |
| Conversion factor | `conversion_factor` | 0.283 |
| Loss factor water| `water_demand` | 1000 |
| Power Limit | `P` | 1000 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Losses Sector | `sector_loss` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Electrolysis_ + position |

</details>

<details>
<summary>Code</summary>

```

class Electrolysis():
    
    def __init__(self, *args, **kwargs):
        P_electrolysis = 1000
        K_electrolysis=0.283
        C_elecrolysis_in = 0
        C_elecrolysisout = 0.068
        Water_electrolysis = 1000
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.P = kwargs.get("P", P_electrolysis)
        self.f = kwargs.get("conversion_factor", K_electrolysis)
        self.Q = self.P*self.f
        self.Water = kwargs.get("water_demand", Water_electrolysis)
        self.costs_in = kwargs.get("c_in", C_elecrolysis_in)
        self.costs_out = kwargs.get("c_out", C_elecrolysisout)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out")
        self.sector_loss = kwargs.get("sector_loss")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'Electrolysis_' + self.position
        
    def component(self):
        v_hydrogenElectrolysis = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT, variable_costs = self.costs_in)
        q_hydrogenElectrolysis = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_out)
            
  
        v_water2electrolysis = solph.Flow(min=0, max=1, nominal_value=self.Q*self.deltaT/self.Water)
            
            
            
        electrolysis = tl.TransformerLosses(
            label=self.label,
            inputs={self.sector_in : q_hydrogenElectrolysis, self.sector_loss : v_water2electrolysis},
            outputs={self.sector_out : v_hydrogenElectrolysis},
            conversion_sector = self.sector_in, losses_sector=self.sector_loss,
            conversion_factor=self.f, losses_factor=self.f/self.Water)

        
        return electrolysis


```
</details>




