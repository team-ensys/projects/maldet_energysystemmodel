# Water Scarcity Constraints
Addition of waterlimits to the model. Different types of constraints can be added to the model. To import the constraints into the model, the following line must be included in the main program.

`import constraints_waterlimit as constraints_waterlimit`

The method returns a modified OEMOF model, where the required water scarcity constraints are added. To add the contraints to a model, the following line of code must be implemented **ater the model was solved**.

```
model = solph.Model(my_energysystem) 
messi = constraints_waterlimit.potablewater_limit(model=messi, electrolysis=electrolysis, waterdemand = waterDemand,
                                                  waterlimit=np.multiply(consumer1.input_data['waterDemand'], scarcityFactor))
```

## Constraint with Electrolysis

The potable water input for the demand and the electrolysis is limited by a constraint. Scarcity factors must be given as a timeseries to limit the water consumption. Scarcity factors and given waterdemand must be multiplied before adding them to the model --> limit must be given.

** v_waterElectrolysis(t) + v_waterDemand(t) <= limit(t) **
** limit(t) = f_scarcity(t) * V_waterDemandGiven(t) **


| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Electrolysis Component | `electrolysis` | 0|
| Waterdemand Component | `waterdemand` | 0|
| Water Limit | `waterlimit` | 0|
| Model where constraints are added | `model` | 0 |

If no model or water components are set as parameters, no constraints are added.

```

def potablewater_limit(*args, **kwargs):
    
    #Vehicle Objects
    electrolysis = kwargs.get("electrolysis", 0)
    waterdemand = kwargs.get("waterdemand", 0)
    
    limit = kwargs.get("waterlimit", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlockWaterlimit', myBlock)
        
        def waterlimitRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if ((i.label==str(electrolysis.sector_loss) and o.label==electrolysis.label)
                                                                               or (i.label==waterdemand.label and o.label==str(waterdemand.sector_waterdemand)))))
                                                                                <= limit[t]) 
        
    if electrolysis and waterdemand:
        
        myBlock.waterlimit = pyomo.Constraint(
            myBlock.t,
            rule=waterlimitRule,
            doc='WaterdemandLimit')
                
        
           
            
        
    return modelIn  

```

## Constraint without Electrolysis

Same constraints as above, without electrolysis. For model uses where water scarcity is implemented, but no hydrogen sector with electrolysis

** v_waterDemand(t) <= limit(t) **
** limit(t) = f_scarcity(t) * V_waterDemandGiven(t) **


| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Waterdemand Component | `waterdemand` | 0|
| Water Limit | `waterlimit` | 0|
| Model where constraints are added | `model` | 0 |

If no model or water components are set as parameters, no constraints are added.

```


def potablewater_limitXElectrolysis(*args, **kwargs):
    
    #Vehicle Objects
    waterdemand = kwargs.get("waterdemand", 0)
    
    limit = kwargs.get("waterlimit", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
    
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
        
        
        modelIn.add_component('MyBlockWaterlimit', myBlock)
        
        def waterlimitRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if ((i.label==waterdemand.label and o.label==str(waterdemand.sector_waterdemand)))))
                                                                                <= limit[t]) 
        
    if waterdemand:
        
        myBlock.waterlimit = pyomo.Constraint(
            myBlock.t,
            rule=waterlimitRule,
            doc='WaterdemandLimit')
                
        
           
            
        
    return modelIn  

```

