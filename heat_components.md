# Heat Components

File with all Components, allocated to the heat sector. Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import heat_components as heat_comp`

Call of the components:

```
componentObject = heat_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`thermalCooling = heat_comp.ThermalCooling(sector_in=consumer1.getHeatSector(), sector_out=consumer1.getCoolingSector()) `

## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
def component(self):
            q_thCoolTh = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_thCoolCool = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            thCool = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_thCoolTh},
                outputs={self.sector_out : q_thCoolCool},
                conversion_factors={self.sector_out : self.conversion_factor})
            return thCool
```

## Thermal Cooling

Thermal Cooling Component --> Simple Transformer

**Input: Heat**

**Output: Cooling**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.02 |
| Conversion factor | `conversion_factor` | 0.7 |
| Input Power Limit | `P_in` | 7 |
| Output Power Limit | `P_out` | 7 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | ThermalCooling_ + position |

</details>

<details>
<summary>Code</summary>

```

    class ThermalCooling():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.7
            costsIn=0
            costsOut=0.002
            PowerInLimit = 7
            PowerOutLimit = 7
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'ThermalCooling_' + self.position

        def component(self):
            q_thCoolTh = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_thCoolCool = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            thCool = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_thCoolTh},
                outputs={self.sector_out : q_thCoolCool},
                conversion_factors={self.sector_out : self.conversion_factor})
            return thCool
        


```
</details>

## Thermal Storage

Thermal Storage Component --> Generic Storage Block

**Input: Heat**

**Output: Heat**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.0005 |
| Output costs | `c_out` | 0.0005 |
| Input Efficiency | `eta_in` | 0.8 |
| Output Efficiency | `eta_out` | 0.8 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Maximum Storage Volume | `volume_max` | 0.3 |
| Start Storage Volume | `volume_start` | 0 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Input Power Limit | `P_in` | 7 |
| Output Power Limit | `P_out` | 7 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | ThermalStorage_ + position |
| Storage Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class ThermalStorage():
    
    def __init__(self, *args, **kwargs):
        P_inMax = 7
        P_outMax = 7
        costsIn = 0.0005
        costsOut=0.0005
        Eta_in = 0.8
        Eta_out = 0.8
        Eta_sb = 0.999
        V_max = 0.3
        V_start = 0
        Storagelevelmin = 0
        Storagelevelmax=1
        emissions=0
        deltaT=1
        position='Consumer_1'
        
        self.eta_in = kwargs.get("eta_in", Eta_in)
        self.eta_out = kwargs.get("eta_out", Eta_out)
        self.eta_sb = kwargs.get("eta_sb", Eta_sb)
        self.costs_in = kwargs.get("c_in", costsIn)
        self.costs_out = kwargs.get("c_out", costsOut)
        self.P_in = kwargs.get("P_in", P_inMax)
        self.P_out = kwargs.get("P_Out", P_outMax)
        self.v_start = kwargs.get("volume_start", V_start)
        self.level_min = kwargs.get("level_min", Storagelevelmin)
        self.level_max = kwargs.get("level_max", Storagelevelmax)
        self.v_max = kwargs.get("volume_max", V_max)
        self.sector_in = kwargs.get("sector_in")
        self.sector_out = kwargs.get("sector_out", self.sector_in)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.emissions=kwargs.get("emissions", emissions)
        self.position = kwargs.get("position", position)
        self.label = 'ThermalStorage_' + self.position
        self.balanced = kwargs.get("balanced", True)
        
        
    def component(self):
        #Energy Flows
        q_thStoreIn = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
        q_thStoreOut = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
        thStore = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: q_thStoreIn},
                outputs={self.sector_out: q_thStoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.v_max,
                initial_storage_level=self.v_start/self.v_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in, outflow_conversion_factor=self.eta_out,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
        
        return thStore

```
</details>

## Hotwater Boiler

Hotwater Boiler is a special case, as two input sectors are needed. The second input flow is dependent on the first input flow. A transformer loss block [TransformerLosses](https://gitlab.com/team-ensys/projects/maldet_energysystemmodel/-/blob/main/TransformerLosses.md) is included in OEMOF. Additionally to the input sector and the output sector, the second input sector (losses sector) must be given. Conversion and loss factor must be given as separate parameters.

**Input: Heat**

**Output: Hotwater**

**Losses: Water**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.0005 |
| Conversion factor | `conversion_factor` | 0.95 |
| Losses Water Demand | `water_demand` | 0.017 |
| Input Power Limit | `P_in` | 21 |
| Output Power Limit | `P_out` | 21 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Loss Sector | `sector_loss` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | HotwaterBoiler_ + position |

</details>

<details>
<summary>Code</summary>

```

    class HotwaterBoiler():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            conversionFactor=0.95
            costsIn=0
            costsOut=0.0005
            PowerInLimit = 21
            PowerOutLimit = 21
            WaterBoiler=0.017
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", conversionFactor)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.WaterFactor = kwargs.get("water_demand", WaterBoiler)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_loss = kwargs.get("sector_loss")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'HotwaterBoiler_' + self.position
            

        def component(self):
            q_hwBoilHeat = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_hwBoilHW = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            v_water2boiler = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT*self.WaterFactor)
            
            hwBoiler = tl.TransformerLosses(
                label=self.label,
                inputs={self.sector_in : q_hwBoilHeat, self.sector_loss : v_water2boiler},
                outputs={self.sector_out : q_hwBoilHW},
                conversion_sector = self.sector_in, losses_sector=self.sector_loss,
                conversion_factor=self.conversion_factor, losses_factor=self.WaterFactor)
            return hwBoiler        
        


```
</details>


## Demand

Heat Demand given as timeseries, which can be given in the input data.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Heatdemand_ + position |
| Heat Demand | `demand_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'Heatdemand_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_heatDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            heatDemand = solph.Sink(label=self.label, inputs={self.sector: q_heatDemand})
            
            return heatDemand    


```
</details>

## Hotwater Demand

Hotwater Demand given as timeseries, which can be given in the input data.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Hotwaterdemand_ + position |
| Heat Demand | `demand_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class Hotwaterdemand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'Hotwaterdemand_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_hotwaterDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            hotwaterDemand = solph.Sink(label=self.label, inputs={self.sector: q_hotwaterDemand})
            
            return hotwaterDemand  
```
</details>


## Grid

Input component to cover the remaining required heat energy. Like for electricity, it is differed between model costs and costs for results.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Costs | `costs` | 1000 |
| Costs Model | `costs_model` | costs |
| Label | `label` | Districtheatgrid_ + position |

</details>

<details>
<summary>Code</summary>

```

class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Districtheatgrid_' + self.position
        self.costs=0
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs)
            
        dhGrid = solph.Source(label=self.label, outputs={self.sector: q_dhGridfeedin})
        
        return dhGrid

```
</details>

## Grid Emissions

Emissions as additional output.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Sector | `sector` | / |
| Sector Emissions | `sector_emissions` | 0 |
| Emissions | `emisisons` | 0 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Costs | `costs` | 1000 |
| Costs Model | `costs_model` | costs |
| Label | `label` | Districtheatgrid_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0.14
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Districtheatgrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'District\nHeat\nGrid_' + self.position
        self.costs_model=kwargs.get("costs_model", costs)
        self.color = 'royalblue'
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            dhGrid = solph.Source(label=self.label, outputs={self.sector_out: q_dhGridfeedin})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            dhGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_dhGridfeedin, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return dhGrid

```
</details>

## Grid Sink

Sink for the excess heat.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | DistrictheatgridSink_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridSink():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'DistrictheatgridSink_' + self.position
        self.costs=0
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs)
            
        dhGrid = solph.Sink(label=self.label, inputs={self.sector: q_dhGridfeedin})
        
        return dhGrid


```
</details>

## Grid Purchase

Purchase from the districtheat grid with power and energy costs. The power costs are evaluated with investment flows, assuming no power has been installed yet. If only a certain share of power costs should be considered, the parameter `'existing'` can be set. The energy costs are given to the model as timeseries.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Power Costs | `costs_power` | 31 |
| Energy Costs | `costs_energy` | Numpay Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Districtheatgridpurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        existingGrid = 0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Districtheatgridpurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_energy = kwargs.get("costs_energy", costsEnergyArray)
        
        
    def component(self):
        q_dhGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_energy,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))
            
        dhGrid = solph.Source(label=self.label, outputs={self.sector: q_dhGridpurchase})
        
        return dhGrid   

```
</details>

## Grid Purchase Emissions

Emissions as additional Output.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Power Costs | `costs_power` | 31 |
| Energy Costs | `costs_energy` | Numpay Array with 0 |
| Sector | `sector` | / |
| Sector Emissions | `sector_emissions` | 0 |
| Emissions | `emisisons` | 0 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Districtheatgridpurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridPurchaseEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_power = 31
        deltaT=1
        position='Consumer_1'
        emissions=0.209
        maxEmissions=1000000
        timesteps=8760
        existingGrid=0
        
        self.P= kwargs.get("P", P_connection)
        self.costs_power = kwargs.get("costs_power", C_power)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions = kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Districtheatgridpurchase_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'DistrictHeat\nGridPurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        self.existingGrid = kwargs.get("existing", existingGrid)
        self.color='slategrey'
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs_energy", costsEnergyArray)
        
        
        
    def component(self):
        q_dhGridpurchase = solph.Flow(min=0, max=1, variable_costs=self.costs_out,  investment=solph.Investment(minimum=0, maximum=self.P*self.deltaT, existing=self.existingGrid, ep_costs=self.costs_power/self.deltaT))

        
        if(self.emissions==0 or self.sector_emissions==0):
            
            dhGrid = solph.Source(label=self.label, outputs={self.sector_out: q_dhGridpurchase})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            dhGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_dhGridpurchase, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return dhGrid 

```
</details>


## Grid Feedin

Feedin excess heat with corresponding feed-in tariffs, that are given as timeseries.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Energy Costs | `revenues` | Numpay Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | DistrictheatgridFeedin_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridFeedin():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'DistrictheatgridFeedin_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        revenuesEnergyArray = np.zeros(self.timesteps)
            
        self.revenues = kwargs.get("revenues", revenuesEnergyArray)
        self.revenues=np.multiply(self.revenues, -1)
        
        
    def component(self):
        q_dhGridfeedin = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.revenues)
            
        dhGridFeedIn = solph.Sink(label=self.label, inputs={self.sector: q_dhGridfeedin})
        
        return dhGridFeedIn

```
</details>


