# Battery Charging
Battery Charging can be blocked to be in the same time as battery discharging by binary variables. A file 'constraints_add_binary.py' includes a method `battery_chargeblock()`. To use the method, it must be imported in the main programme:

`import constraints_add_binary as constraints`

The method returns a modified OEMOF model, where the required battery binary constraints are added. To add the contraints to a model, the following line of code must be implemented **ater the model was solved**.

```
model = solph.Model(my_energysystem) 
messi = constraints.battery_chargeblock(model=messi, battery=battery)
```

The method has different input parameters. 

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Battery Component | `battery` | 0|
| Model where constraints are added | `model` | 0 |

If no model or battery are set as parameters, no constraints are added.

</details>

## Timestep Set
Pyomo set with all considered timesteps of the model. To include sets into OEMOF, a block must be defined before.

```
myBlock = po.Block()
    
#Set with timesteps of OEMOF Model
myBlock.t = pyomo.Set(
    initialize=modelIn.TIMESTEPS,
    ordered=True,
    doc='Set of timesteps')
```

## Binary Battery Set
For the battery (not zero --> given as parameter to the method), binary variables for each timestep are created.

```
if battery:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_battery = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Charge and Discharge Block'
            )
```

## Add Block to Model
Before creating the constraints, the defined block must be added to the model.

`modelIn.add_component('MyBlockBattery', myBlock)`

## Binary sum rule
This rule guarantess that each binary variable can only be unequal to zero if all the others are equal to zero.

**q_charge(t) <= bin_Charge(t)*P_maxCharge(t)*deltaT**
**q_discharge(t) <= (1-bin_Charge(t))*P_maxDischarge(t)*deltaT**

The rule is only applied if a battery is given as a parameter.

```
PmaxCharge_Battery = battery.P_in
        def chargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==battery.label)) 
                                                            <= (model.bin_battery[t]) * PmaxCharge_Battery)
        
        PmaxDischarge_Battery = battery.P_out
        def dischargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==battery.label)) 
                                                            <= (1-model.bin_battery[t]) * PmaxDischarge_Battery)
    
```


## Full Code

<details>
<summary>Full Code</summary>

```
def battery_chargeblock(*args, **kwargs):
    
    #Vehicle Objects
    battery = kwargs.get("battery", 0)
    
    
    #Model
    modelIn = kwargs.get("model", 0)
            

    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if battery:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_battery = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Charge and Discharge Block'
            )
            
      
    
        modelIn.add_component('MyBlockBattery', myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        
        PmaxCharge_Battery = battery.P_in
        def chargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==battery.label)) 
                                                            <= (model.bin_battery[t]) * PmaxCharge_Battery)
        
        PmaxDischarge_Battery = battery.P_out
        def dischargePowerBatteryRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==battery.label)) 
                                                            <= (1-model.bin_battery[t]) * PmaxDischarge_Battery)
    

        
        
        
        if battery:
                
                myBlock.batteryCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBatteryRule,
                    doc='Battery Charging')
                
                myBlock.batteryDischarging = pyomo.Constraint(
                    myBlock.t,
                    rule=dischargePowerBatteryRule,
                    doc='Battery Discharging')
        
           
            
        
        return modelIn  
        


```
</details>
