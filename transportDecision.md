# Transport Decision
The transport decision is a method that is implemented after the model is created to include additional constraints in the model. A file 'constraints_add_binary.py' includes a method `transport_decision()`. To use the method, it must be imported in the main programme:

`import constraints_add_binary as constraints`

The method returns a modified OEMOF model, where the required transport decision and corresponding charging limit constraints are added. To add the contraints to a model, the following line of code must be implemented **ater the model was solved**.

```
model = solph.Model(my_energysystem) 
model = constraints.transport_decision(electric_vehicle=electricVehicle, biofuel_vehicle=biofuelVehicle, 
                                       fuelcell_vehicle=fuelcellVehicle, petrol_vehicle=petrolVehicle,
                                       timesteps=timesteps, model=model, demand_timeseries=consumer1.input_data['transportDemand'])
```

The method has different input parameters. 

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Electric Vehicle | `electric_vehicle` | 0|
| Biofuel Vehicle | `biofuel_vehicle` | 0 |
| Fuelcell Vehicle | `fuelcell_vehicle` | 0 |
| Petrol Vehicle | `petrol_vehicle` | 0 |
| Model where constraints are added | `model` | / |
| Total Timesteps | `timesteps` | 8760 |
| Timeseries with mobility demand | `demand_timeseries` | Numpy array with 0 |

If no model or none of the 4 vehicles are set as parameters, no constraints are added.

</details>

## Timestep Set
Pyomo set with all considered timesteps of the model. To include sets into OEMOF, a block must be defined before.

```
myBlock = po.Block()
    
#Set with timesteps of OEMOF Model
myBlock.t = pyomo.Set(
    initialize=modelIn.TIMESTEPS,
    ordered=True,
    doc='Set of timesteps')
```

## Binary Vehicle Variables Set
For each vehicle that is considered for covering the transport balance (not zero --> given as parameter to the method), binary variables for each timestep are created.

```
if electricVehicle:
        #Set with Vehicles
            
        #Binary optimization variables
        #Electric Vehicle Binary
        myBlock.bin_EV = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Electric Vehicle Drive'
        )
            
    if biofuelVehicle:
      
        # Biofuel Vehicle Binary
        myBlock.bin_BioV = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Biofuel Vehicle Drive')
    
    if fuelcellVehicle:
            
        # Fuelcell Vehicle Binary
        myBlock.bin_FC = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Fuelcell Vehicle Drive')
    
    if petrolVehicle:

        # Petrol Vehicle Binary
        myBlock.bin_PET = pyomo.Var(
            myBlock.t, 
            within=pyomo.Binary,
            doc='Binary for Petrol Vehicle Drive')
```

## Add Block to Model
Before creating the constraints, the defined block must be added to the model.

modelIn.add_component('MyBlock', myBlock)

## Binary sum rule
This rule guarantess that each binary variable can only be unequal to zero if all the others are equal to zero.

**bin_EV(t) + bin_BioV(t) + bin_Fuelcell(t) + bin_Petrol(t) <= 1**

The rule is only applied if at leat one vehicle is given as a parameter.

```
def binarySumRule(model, t):
            return (model.bin_EV[t] + model.bin_BioV[t] + model.bin_FC[t] + model.bin_PET[t] <= 1)

myBlock.binarySum = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule,
                doc='Sum of Tranfport Binaries')
```

## Transport Demand Coverage Rule
A binary balance rule is implemented, where all the given vehicles are used to cover the required transport demand. With the multiplication with the defined binary variables it is guaranteed that only one vehicle at each time is used to cover the demand.

**bin_EV(t)*s_EV(t) + bin_BioV(t)*s_BioV(t) + bin_Fuelcell(t)*s_Fuelcell(t) + bin_Petrol(t)*s_Petrol(t) = s_Demand(t)**

The rule is only applied if at leat one vehicle is given as a parameter.

```
def binaryTransportConstraintRule(model, t):
            return ((model.bin_EV[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle.labelDrive)) 
                                                            + (model.bin_BioV[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==biofuelVehicle.labelDrive)) 
                                                            + (model.bin_FC[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==fuelcellVehicle.labelDrive))
                                                            + (model.bin_PET[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==petrolVehicle.labelDrive)) 
                                                            == demand[t])

myBlock.binaryTransportBalance = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule,
                doc='Transport Balance')
```

## Electric Vehicle Charging Limit
The electric vehicle can only be charged when the car is not driving, respectively when

**bin_EV(t) = 0**

The charging power is thereby set to zero with the binary driving variable:

**P_chargeEV(t) <= P_chargeMaxEv * (1-bin_EV(t))**

```
PmaxCharge_EV = electricVehicle.P_charge
        def chargePowerEVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle.label)) 
                                                            <= (1-model.bin_EV[t]) * PmaxCharge_EV)

if electricVehicle:
                myBlock.evCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule,
                    doc='EV Charging')
```

## Biofuel Vehicle Charging Limit
The biofuel vehicle can only be charged when the car is not driving, respectively when

**bin_BioV(t) = 0**

The charging flow is thereby set to zero with the binary driving variable:

**Q_chargeBioV(t) <= Q_chargeMaxBioV * (1-bin_BioV(t))**

```
Flow_BioV = biofuelVehicle.Q_charge
        def chargePowerBioVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==biofuelVehicle.label)) 
                                                            <= (1-model.bin_BioV[t]) * Flow_BioV)
    
if biofuelVehicle:
                myBlock.biovCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBioVRule,
                    doc='Biofuel Vehicle Charging')
```

## Fuelcell Vehicle Charging Limit
The fuelcell vehicle can only be charged when the car is not driving, respectively when

**bin_Fuelcell(t) = 0**

The charging flow is thereby set to zero with the binary driving variable:

**Q_chargeFuelcell(t) <= Q_chargeMaxFuelcell * (1-bin_Fuelcell(t))**

```
Flow_fc = fuelcellVehicle.Q_charge
        def chargePowerFCVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==fuelcellVehicle.label)) 
                                                            <= (1-model.bin_FC[t]) * Flow_fc)
    
    
if fuelcellVehicle:
                myBlock.fuelcellvCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerFCVRule,
                    doc='Fuelcell Vehicle Charging')
```

## Petrol Vehicle Charging Limit
The petrol vehicle can only be charged when the car is not driving, respectively when

**bin_Petrol(t) = 0**

The charging flow is thereby set to zero with the binary driving variable:

**Q_chargePetrol(t) <= Q_chargeMaxPetrol * (1-bin_Petrol(t))**

```
Flow_petV = petrolVehicle.Q_charge
        def chargePowerPetVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==petrolVehicle.label)) 
                                                            <= (1-model.bin_PET[t]) * Flow_petV)
    
    
if petrolVehicle:
                myBlock.petvCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerPetVRule,
                    doc='Petrol Vehicle Charging')
```

## Full Code

<details>
<summary>Full Code</summary>

```
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 10:28:54 2021

@author: Matthias Maldet
"""

import pyomo.environ as po
import pyomo.core as pyomo
import numpy as np
#import pyomo_rules

def transport_decision(*args, **kwargs):
    
    #Vehicle Objects
    electricVehicle = kwargs.get("electric_vehicle", 0)
    biofuelVehicle = kwargs.get("biofuel_vehicle", 0)
    fuelcellVehicle = kwargs.get("fuelcell_vehicle", 0)
    petrolVehicle = kwargs.get("petrol_vehicle", 0)
    
    #Model
    modelIn = kwargs.get("model", 0)
    
    
    
    #Transport Demand
    timesteps = kwargs.get("timesteps", 8760)
    demandArray = np.zeros(timesteps)
            
    demand = kwargs.get("demand_timeseries", demandArray)
    
    if(modelIn==0):
        print('Model not found!')
        return 0
        
    else:
        #Add the additional constraints to the model
    #Block for Pyomo Environment
        myBlock = po.Block()
    
        #Set with timesteps of OEMOF Model
        myBlock.t = pyomo.Set(
            initialize=modelIn.TIMESTEPS,
            ordered=True,
            doc='Set of timesteps')
    
        if electricVehicle:
            #Set with Vehicles
            
            #Binary optimization variables
            #Electric Vehicle Binary
            myBlock.bin_EV = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Electric Vehicle Drive'
            )
            
        if biofuelVehicle:
    
            
            # Biofuel Vehicle Binary
            myBlock.bin_BioV = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Biofuel Vehicle Drive')
    
        if fuelcellVehicle:
            
            # Fuelcell Vehicle Binary
            myBlock.bin_FC = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Fuelcell Vehicle Drive')
    
        if petrolVehicle:

            # Petrol Vehicle Binary
            myBlock.bin_PET = pyomo.Var(
                myBlock.t, 
                within=pyomo.Binary,
                doc='Binary for Petrol Vehicle Drive')
    
    
        modelIn.add_component('MyBlock', myBlock)
    
    
    
        #Binary Balance Rule
        #myBlock.binaryBalance = pyomo.Constraint(
            #myBlock.t, 
            #rule=pyomo_rules.binSum_all_rule,
            #doc='binary sum equation')
            
        def binarySumRule(model, t):
            return (model.bin_EV[t] + model.bin_BioV[t] + model.bin_FC[t] + model.bin_PET[t] <= 1)

        def binaryTransportConstraintRule(model, t):
            return ((model.bin_EV[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==electricVehicle.labelDrive)) 
                                                            + (model.bin_BioV[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==biofuelVehicle.labelDrive)) 
                                                            + (model.bin_FC[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==fuelcellVehicle.labelDrive))
                                                            + (model.bin_PET[t]*sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if i.label==petrolVehicle.labelDrive)) 
                                                            == demand[t])
    
        PmaxCharge_EV = electricVehicle.P_charge
        def chargePowerEVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==electricVehicle.label)) 
                                                            <= (1-model.bin_EV[t]) * PmaxCharge_EV)
    
        Flow_BioV = biofuelVehicle.Q_charge
        def chargePowerBioVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==biofuelVehicle.label)) 
                                                            <= (1-model.bin_BioV[t]) * Flow_BioV)
    
        Flow_fc = fuelcellVehicle.Q_charge
        def chargePowerFCVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==fuelcellVehicle.label)) 
                                                            <= (1-model.bin_FC[t]) * Flow_fc)
    
        Flow_petV = petrolVehicle.Q_charge
        def chargePowerPetVRule(model, t):
            return ((sum(modelIn.flow[i, o, t] for (i, o) in modelIn.FLOWS if o.label==petrolVehicle.label)) 
                                                            <= (1-model.bin_PET[t]) * Flow_petV)
    
    
                                                            #<=100)
    
        if(electricVehicle != 0 or biofuelVehicle != 0 or fuelcellVehicle != 0 or petrolVehicle != 0):
            myBlock.binaryTransportBalance = pyomo.Constraint(
                myBlock.t,
                rule=binaryTransportConstraintRule,
                doc='Transport Balance')
            
            myBlock.binarySum = pyomo.Constraint(
                myBlock.t,
                rule= binarySumRule,
                doc='Sum of Tranfport Binaries')
        
        
            if electricVehicle:
                myBlock.evCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerEVRule,
                    doc='EV Charging')
        
            if biofuelVehicle:
                myBlock.biovCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerBioVRule,
                    doc='Biofuel Vehicle Charging')
        
            if fuelcellVehicle:
                myBlock.fuelcellvCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerFCVRule,
                    doc='Fuelcell Vehicle Charging')
        
            if petrolVehicle:
                myBlock.petvCharging = pyomo.Constraint(
                    myBlock.t,
                    rule=chargePowerPetVRule,
                    doc='Petrol Vehicle Charging')
            
        
        return modelIn
        

</details>
```
