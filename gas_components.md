# Gas Components

File with all Components, allocated to the gas sector. Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import gas_components as gas_comp`

Call of the components:

```
componentObject = gas_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`blockheat = gas_comp.Blockheat(sector_in=consumer1.getGasSector(), sector_out=consumer1.getElectricitySector())`

## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
def component(self):
            q_gasBlockHeatGas = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_gasBlockHeatEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            blockHeat = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : q_gasBlockHeatGas},
                outputs={self.sector_out : q_gasBlockHeatEl},
                conversion_factors={self.sector_out : self.conversion_factor})
            return blockHeat
```

## Gasboiler

Gasboiler Component --> Simple Transformer

**Input: Gas**

**Output: Heat**

**Optional Output: Emissions**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.001 |
| Conversion factor | `conversion_factor` | 0.95|
| Input Power Limit | `P_in` | 21 |
| Output Power Limit | `P_out` | 21 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions Sector| `sector_emissions` | 0 |
| Emissions | `emissions` | 0.201 |
| Maximum Emissions | `emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Gasboiler_ + position |

</details>

<details>
<summary>Code</summary>

```

class Gasboiler():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.95
            costsIn=0
            costsOut=0.001
            PowerInLimit = 21
            PowerOutLimit = 21
            deltaT=1
            emissions=0.201
            maxEmissions=1000000
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'Gasboiler_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Gas\nBoiler_' + self.position
            self.color='darkcyan'

        def component(self):
            q_gasBoilGas = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_gasBoilTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            if(self.emissions==0 or self.sector_emissions==0):
                gasBoiler = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBoilGas},
                    outputs={self.sector_out : q_gasBoilTh},
                    conversion_factors={self.sector_out : self.conversion_factor})
                
            else:
                m_gasBoilEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                gasBoiler = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBoilGas},
                    outputs={self.sector_out : q_gasBoilTh, self.sector_emissions : m_gasBoilEmissions},
                    conversion_factors={self.sector_out : self.conversion_factor, self.sector_emissions : self.emissions})
                
            
            return gasBoiler

```
</details>


## Blockheat Power Plant

Blockheat Component --> Simple Transformer

**Input: Gas**

**Output: Electricity**

**Optional Output: Emissions**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.003 |
| Conversion factor | `conversion_factor` | 0.44|
| Input Power Limit | `P_in` | 100 |
| Output Power Limit | `P_out` | 100 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions Sector| `sector_emissions` | 0 |
| Emissions | `emissions` | 0.201 |
| Maximum Emissions | `emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | GasBlockheat_ + position |

</details>

<details>
<summary>Code</summary>

```

class Blockheat():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.44
            costsIn=0
            costsOut=0.003
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.201
            maxEmissions=1000000
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'GasBlockheat_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Gas\nBlockheat_' + self.position
            self.color='orange'

        def component(self):
            q_gasBlockHeatGas = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_gasBlockHeatEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                blockHeat = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBlockHeatGas},
                    outputs={self.sector_out : q_gasBlockHeatEl},
                    conversion_factors={self.sector_out : self.conversion_factor})
                
            else:
                m_gasblockheatEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                blockHeat = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasBlockHeatGas},
                    outputs={self.sector_out : q_gasBlockHeatEl, self.sector_emissions : m_gasblockheatEmissions},
                    conversion_factors={self.sector_out : self.conversion_factor, self.sector_emissions : self.emissions})
                
            return blockHeat
        

```
</details>

## Gas CHP

**Input: Gas**

**Output: Electricity, Heat**

**Optional Output: Emissions**

CHP is a special case, as this technology has two outputs. The output sectors, output costs and conversion factors are given to the component as list and are called with their indices. 

`sector_out=[sector_out1, sector_out1]`

`c_out=[c_out1, c_out2]`

`conversion_factor=[conversion_factor1, conversion_factor2]`

**It is important that output sectors and conversion factors are given to the component in the same order!** 

The default values are set in the order `[Electricity, Heat]`.


<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | [0.01, 0.01] |
| Conversion factor | `conversion_factor` | [0.35, 0.44]|
| Input Power Limit | `P_in` | 100 |
| Output Power Limit | `P_out` | 100 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions Sector| `sector_emissions` | 0 |
| Emissions | `emissions` | 0.201 |
| Maximum Emissions | `emissions` | 1000000 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | GasCHP_ + position |

</details>

<details>
<summary>Code</summary>

```

class CHP():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.01, 0.01]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0.201
            maxEmissions=1000000
            position='Consumer_1'
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_out", PowerOutLimit)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.sector_emissions = kwargs.get("sector_emissions", 0)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.max_emissions=kwargs.get("max_emissions", maxEmissions)
            self.position = kwargs.get("position", position)
            label = 'GasCHP_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Gas\nCHP_' + self.position
            self.color='blue'

        def component(self):
            q_gasCHPGas = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT, variable_costs=self.costs_in)
            q_gasCHPEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[0], variable_costs=self.costs_out[0])
            q_gasCHPTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT*self.conversion_factor[1], variable_costs=self.costs_out[1])
            
            if(self.emissions==0 or self.sector_emissions==0):
            
                gasCHP = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasCHPGas},
                    outputs={self.sector_out[0] : q_gasCHPEl, self.sector_out[1] : q_gasCHPTh},
                    conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1]})
                
            else:
                m_gasCHPEmissions = solph.Flow(min=0, max=1, nominal_value=self.max_emissions)
                
                gasCHP = solph.Transformer(
                    label=self.label,
                    inputs={self.sector_in : q_gasCHPGas},
                    outputs={self.sector_out[0] : q_gasCHPEl, self.sector_out[1] : q_gasCHPTh, self.sector_emissions : m_gasCHPEmissions},
                    conversion_factors={self.sector_out[0] : self.conversion_factor[0], self.sector_out[1] : self.conversion_factor[1], self.sector_emissions : self.emissions})
            
            
            return gasCHP
        

```
</details>

## Demand

Gas Demand given as timeseries, which can be given in the input data.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Gasdemand_ + position |
| Heat Demand | `demand_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class Demand():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'Gasdemand_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            q_gasDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            gasDemand = solph.Sink(label=self.label, inputs={self.sector: q_gasDemand})
            
            return gasDemand


```
</details>


## Grid

Input component to cover the remaining required gas energy. Differed between costs and model costs.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Sector | `sector` | / |
| Costs | `costs` | 1000 |
| Model costs | `model_costs` | costs |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Gasgrid_ + position |

</details>

<details>
<summary>Code</summary>

```

class Grid():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        
        self.P= kwargs.get("P", P_connection)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Gasgrid_' + self.position
        self.costs=0
        
        
    def component(self):
        q_gasGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs)
            
        gasGrid = solph.Source(label=self.label, outputs={self.sector: q_gasGrid})
        
        return gasGrid


```
</details>

## Grid Emissions

Additionally considered pre-chain emissions

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Sector | `sector` | / |
| Costs | `costs` | 1000 |
| Model costs | `model_costs` | costs |
| Sector Emissions | `sector_emissions` | 0 |
| Emissions | `emisisons` | 0 |
| Maximum Emissions | `max_emissions` | 1000000 |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Gasgrid_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridEmissions():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        deltaT=1
        position='Consumer_1'
        costs=1000
        emissions=0
        maxEmissions=1000000
        timesteps=8760
        
        self.P= kwargs.get("P", P_connection)
        self.sector_out= kwargs.get("sector")
        self.sector_emissions= kwargs.get("sector_emissions", 0)
        self.emissions=kwargs.get("emissions", emissions)
        self.maxEmissions=kwargs.get("max_emissions", maxEmissions)
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Gasgrid_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Gas\nGrid_' + self.position
        self.costs_model=kwargs.get("costs_model", costs)
        self.color = 'burlywood'
        self.grid=True
        self.timesteps = kwargs.get("timesteps", timesteps)
        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_out = kwargs.get("costs", costsEnergyArray)
        
        
    def component(self):
        q_gasGrid = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_model)
        
        if(self.emissions==0 or self.sector_emissions==0):
            
            gasGrid = solph.Source(label=self.label, outputs={self.sector_out: q_gasGrid})
            
        else:
        
            m_co2 = solph.Flow(min=0, max=1, nominal_value=self.maxEmissions)
            
            gasGrid = em.Source(label=self.label, 
                           outputs={self.sector_out: q_gasGrid, self.sector_emissions : m_co2},
                           emissions=self.emissions,
                           emissions_sector = self.sector_emissions,
                           conversion_sector=self.sector_out)
        
        return gasGrid

```
</details>


## Grid Purchase

Purchase from the gas grid with fixed costs and energy costs. The energy costs are given to the model as timeseries. Fixed costs are added to the consumer attribute `fixed_costs` to be able to evaluate the total fixed costs for all sectors.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Grid Connection Power | `P` | 10 |
| Fix Costs | `costs_fixed` | 36 |
| Energy Costs | `costs_energy` | Numpay Array with 0 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Power decrease for costs | `existing` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Gasgridpurchase_ + position |

</details>

<details>
<summary>Code</summary>

```

class GridPurchase():
    
    def __init__(self, *args, **kwargs):
        P_connection = 10
        C_fix = 36
        deltaT=1
        position='Consumer_1'
        timesteps=8760

        
        self.P= kwargs.get("P", P_connection)
        self.costs_fixed = kwargs.get("costs_fixed", C_fix)
        self.sector= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        self.label = 'Gasgridpurchase_' + self.position
        self.timesteps = kwargs.get("timesteps", timesteps)

        
        costsEnergyArray = np.zeros(self.timesteps)
            
        self.costs_energy = kwargs.get("costs_energy", costsEnergyArray)
        
        
    def component(self):
        q_gasGridpurchase = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_energy)
            
        gasGrid = solph.Source(label=self.label, outputs={self.sector: q_gasGridpurchase})
        
        return gasGrid     

```
</details>
