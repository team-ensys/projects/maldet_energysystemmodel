# Get Costs
Methods for calculating the costs based on the component costs in the model. 

`import get_costs as get_costs`

The following methods are implemented

```
costsServiceElecSectorXgrid = get_costs.getSectorCosts(results=results, components=consumer1.electricityServiceComponents, label='Consumer_1', exclude_grid=True) 
costsEmissionsOutput = get_costs.getEmissionCosts(results=results, components=consumer1.emissionOutputComponents, sectorname='Emissions', costs=co2Price)
totalCosts = get_costs.getTotalCosts(sectorlist=sectorList, costslist=costList, consumer=consumer1)

```

<details>
<summary>Sector Costs</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Components | `components` | 0 |
| Model Results | `results` | 0 |
| Costs Label | `label` | ' ' |
| Exclusion of Grid Costs | `exclude_grid` | 0 |

If no components are added, the plots are skipped. The costs are calculated based on the sector components of the consumer. All possible costs are searched for the components (costs_in, costs_out, costs_loss, revenues, costs_power, costs_fixed). Costs for the components are summed up to the total costs. All the costs of components allocated to the sector + total costs are store in a dictionairy.

```
def getSectorCosts(*args, **kwargs):
    components = kwargs.get("components", 0)
    results = kwargs.get("results", 0)
    label = kwargs.get("label", ' ')
    excludeGrid = kwargs.get("exclude_grid", False)
    
    costs=0
    totalCosts = 0
    columnIn=0
    columnOut=0
    columnLoss=0
    columnRevenue=0

    
    costsDict = {}
    
    if components:
        for component in components:
        
            #Excluding Grid
            if(excludeGrid):
                if(hasattr(component, 'grid')):
                    if(component.grid):
                        continue
        
            elementCosts=0
            
            element = solph.views.node(results, component.label).get("sequences")
            
            
            # ----------------------------------------------------------------
            #Costs Input
            #Only Calc if the Element has the Attribute
            if(hasattr(component, 'costs_in')):
                costsIn = component.costs_in
                
                
                #More Sectors can be as Inputs
                if(isinstance(component.sector_in, list)):
                    #Go through list with input costs
                    for i in range(0, len(costsIn)):
                        for col in element.columns:
                            #get the input column
                            if(str(component.sector_in[i]) in str(col).split(',')[0]):
                                columnIn=col
                        
                        #Evaluate input flow based on input column
                        inputFlow = list(element[columnIn])
                        
                        #Multiplication costs and flow
                        costs = np.multiply(inputFlow, costsIn[i])
                        
                        elementCosts+=costs
                        
                        totalCosts+=np.sum(costs)
                                
                else:
                    for col in element.columns:
                        if(str(component.sector_in) in str(col).split(',')[0]):
                            columnIn=col
                        
                    inputFlow = list(element[columnIn])
                        
                    costs = np.multiply(inputFlow, costsIn)
                    
                    elementCosts+=costs
                        
                    totalCosts+=np.sum(costs)
                    
            
            
            # ----------------------------------------------------------------
            #Costs Output
            #Only Calc if the Element has the Attribute
            if(hasattr(component, 'costs_out')):
                costsOut = component.costs_out
                
                
                
                #More Sectors can be as Outputs
                if(isinstance(component.sector_out, list)):
                    #Go through list with outputs costs
                    for i in range(0, len(costsOut)):
                        for col in element.columns:
                            #get the output column
                            
                            if(str(component.sector_out[i]) in str(col).split(',')[1]):
                                columnOut=col
                        
                        #Evaluate input flow based on input column
                        outputFlow = list(element[columnOut])
                        
                        #Multiplication costs and flow
                        costs = np.multiply(outputFlow, costsOut[i])
                        
                        elementCosts+=costs
                        
                        totalCosts+=np.sum(costs)
                                
                else:
                    for col in element.columns:
                        if(str(component.sector_out) in str(col).split(',')[1]):
                            columnOut=col
                        
                    outputFlow = list(element[columnOut])
                        
                    costs = np.multiply(outputFlow, costsOut)
                    
                    elementCosts+=costs
                    
                    
                        
                    totalCosts+=np.sum(costs)
                    
                    
            # ----------------------------------------------------------------
            #Costs Loss
            #Only Calc if the Element has the Attribute
            if(hasattr(component, 'costs_loss')):
                costsLoss = component.costs_loss
                
                
                
                #More Sectors can be as Outputs
                if(isinstance(component.sector_loss, list)):
                    #Go through list with outputs costs
                    for i in range(0, len(costsLoss)):
                        for col in element.columns:
                            #get the output column
                            
                            if(str(component.sector_loss[i]) in str(col).split(',')[0]):
                                columnLoss=col
                        
                        #Evaluate input flow based on input column
                        lossFlow = list(element[columnLoss])
                        
                        #Multiplication costs and flow
                        costs = np.multiply(lossFlow, costsLoss[i])
                        
                        elementCosts+=costs
                        
                        totalCosts+=np.sum(costs)
                                
                else:
                    for col in element.columns:
                        if(str(component.sector_loss) in str(col).split(',')[0]):
                            columnLoss=col
                        
                    lossFlow = list(element[columnLoss])
                        
                    costs = np.multiply(lossFlow, costsLoss)
                    
                    elementCosts+=costs
                    
                    
                        
                    totalCosts+=np.sum(costs)
             
            
            
            
            
            #Add Component Costs to Dict
            costsDict[component.label] = np.sum(elementCosts)
            
            
            
            # ----------------------------------------------------------------
            #Revenues
            #Only Calc if the Element has the Attribute
            if(hasattr(component, 'revenues')):
                revenues = component.revenues
                
                
                
                #More Sectors can be as Outputs
                if(isinstance(component.revenues, list)):
                    #Go through list with outputs costs
                    for i in range(0, len(revenues)):
                        for col in element.columns:
                            #get the output column
                            
                            if(str(component.sector_in[i]) in str(col).split(',')[0]):
                                columnRevenue=col
                        
                        #Evaluate input flow based on input column
                        revenueFlow = list(element[columnRevenue])
                        
                        #Multiplication costs and flow
                        costs = np.multiply(revenueFlow, revenues[i])
                        
                        elementCosts+=costs
                        
                        totalCosts+=np.sum(costs)
                                
                else:
                    for col in element.columns:
                        if(str(component.revenues) in str(col).split(',')[0]):
                            columnRevenue=col
                        
                    revenueFlow = list(element[columnLoss])
                        
                    costs = np.multiply(revenueFlow, revenues)
                    
                    elementCosts+=costs
                    
                    
                        
                    totalCosts+=np.sum(costs)
             
            
            
            
            
            #Add Component Costs to Dict
            costsDict[component.label] = np.sum(elementCosts)
            
            
            
            # ----------------------------------------------------------------
            #Costs Power
            #Only Calc if the Element has the Attribute
            if(hasattr(component, 'costs_power')):
                costsPower = component.costs_power
                
                
                
                #More Sectors can be as Outputs
                if(isinstance(component.sector_out, list)):
                    #Go through list with outputs costs
                    for i in range(0, len(costsPower)):
                        for col in element.columns:
                            #get the output column
                            
                            if(str(component.sector_out[i]) in str(col).split(',')[1]):
                                columnPower=col
                        
                        #Evaluate input flow based on input column
                        powerFlow = list(element[columnPower])
                        
                        #Multiplication costs and flow
                        costs = np.multiply(powerFlow, costsPower[i])
                        
                        elementCosts+=costs
                        
                        totalCosts+=np.sum(costs)
                                
                else:
                    for col in element.columns:
                        if(str(component.sector_out) in str(col).split(',')[1]):
                            columnPower=col
                        
                    powerFlow = list(element[columnPower])
                        
                    costs = np.multiply(powerFlow, costsPower)
                    
                    elementCosts+=costs
                    
                    
                        
                    totalCosts+=np.sum(costs)
             
            
            
            
            
            #Add Component Costs to Dict
            costsDict[component.label] = np.sum(elementCosts) 
            
            
            
            # ----------------------------------------------------------------
            #Costs Fixed
            #Only Calc if the Element has the Attribute
            if(hasattr(component, 'costs_fixed')):
                costsFixed = component.costs_fixed
                
                elementCosts+=costsFixed
                    
                totalCosts+=np.sum(costsFixed)
                
            
            
                    
    
    totalstring = 'Total_' + label
    costsDict[totalstring] = totalCosts
    
    return costsDict


```

</details>

<details>
<summary>Emission Costs</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sectorname | `sectorname` | 'Electricity' |
| Components | `components` | 0 |
| Model Results | `results` | 0 |
| Costs | `costs` | 0 |
| Label | `label` | ' ' |
| Exclusion of Grid Costs | `exclude_grid` | 0 |

Sector costs for the emissions. 

```
def getEmissionCosts(*args, **kwargs):
    sectorname = kwargs.get("sectorname", "Electricity")
    components = kwargs.get("components", 0)
    results = kwargs.get("results", 0)
    costs = kwargs.get("costs", 0)
    consumer = kwargs.get("label", 'Consumer_1')
    excludeGrid = kwargs.get("exclude_grid", False)
    
    totalSum=0
    values=[]
    labels=[]
    
    resultDict={}
    
    for element in components:
        
        
        #Excluding Grid
        if(excludeGrid):
            if(hasattr(element, 'grid')):
                if(element.grid):
                    continue
        
        printElement = solph.views.node(results, element.label).get("sequences")
                    
        if(len(printElement.filter(regex=sectorname).columns) > 1):
            for col in printElement.columns:
                if not( sectorname in str(col[0][1])):
                    del printElement[col]
                    
        elementValue=(printElement.filter(regex=sectorname)).values.sum()
        values.append(elementValue)
        labels.append(element.label)
        
    for value in values:
        totalSum+= value*costs
    
    for i in range(0, len(values)):
        resultDict[labels[i]] = values[i]*costs
        
        
    resultDict['Total_' + consumer] = totalSum
    
    return resultDict


```

</details>

<details>
<summary>Total Costs</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| List with considered Sectors | `sectorlist` | 0 |
| Cost List | `costslist` | 0 |
| Consumer | `consumer` | 0 |


Calculation of the total costs. 

```
def getTotalCosts(*args, **kwargs): 
    sectorlist = kwargs.get("sectorlist", 0)
    costslist = kwargs.get("costslist", 0) 
    consumer = kwargs.get("consumer", 0)
    
    
    
    totalCosts=0
    
    costs = {}
    
    if(sectorlist!=0 and costslist!=0 and consumer!=0):
        
        totallable = 'Total_' + consumer.label
        
        for i in range(0, len(costslist)):
            costs[sectorlist[i]]=costslist[i][totallable]
            totalCosts+=costslist[i][totallable]
            
    costs['Total'] = totalCosts
    
    return costs


```

</details>



