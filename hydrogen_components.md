# Water Components

File with all Components, allocated to the hydrogen sector. Following packages are imported:

```
import oemof.solph as solph
import transformerLosses as tl
import numpy as np
```

Import into Optimization Model code:

`import hydrogen_components as hydro_comp`

Call of the components:

```
componentObject = hydro_comp.Class(parameters)
my_energysystem.add(componentObject.component()) 
```

## Class Constructor

Default values for the parameters are given as variables. If the parameter is not set, these values are assumed for the component. 

```
defaultValue1 = ..
defaultValue2= ..
...
```

When creating component object, the constructor is setting all the parameters set.

```
def __init__(self, *args, **kwargs):
    self.class_variable = kwargs.get("parameter_name", defaultValue)
```

The input and output sectors must be set in the component definition, based on the required sectors for the specific consumer.

```
self.sector_in = kwargs.get("sector_in")
self.sector_out = kwargs.get("sector_out")
```

This is done in the main programme like in the following example:

`hydrogenboiler = hydro_comp.Hydrogenboiler(sector_in=consumer1.getHydrogenSector(), sector_out=consumer1.getHeatSector())`


## Method component

A flow of the OEMOF.solph package is created for the set input and output sectors. The flows are contrained by the given limits. Input and output operational costs are allocated to the flows and used for the optimisation.
The component is created, based on the component name, the input and output sectors, the flows and conversion factors. An example is shown in the following code.

```
class Hydrogenboiler():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.95
            costsIn=0
            costsOut=0.001
            PowerInLimit = 21
            PowerOutLimit = 21
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'Hydrogenboiler_' + self.position

        def component(self):
            v_hydrogenBoilHydro = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in)
            q_hydrogenBoilTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            hydrogenBoiler = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_hydrogenBoilHydro},
                outputs={self.sector_out : q_hydrogenBoilTh},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_hydrogen})
            return hydrogenBoiler
```

## Demand

Hydrogen Demand given as timeseries, which can be given in the input data.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Hydrogendemand_ + position |
| Heat Demand | `demand_timeseries` | Numpy Array with 0 |

</details>

<details>
<summary>Code</summary>

```

class Demand():

        
        def __init__(self, *args, **kwargs):
            deltaT=1
            position='Consumer_1'
            timesteps = 8760
            

            self.sector = kwargs.get("sector")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.position = kwargs.get("position", position)
            self.label = 'Hydrogendemand_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            demandArray = np.zeros(self.timesteps)
            
            self.demand_timeseries = kwargs.get("demand_timeseries", demandArray)

        def component(self):
            
            v_hydrogenDemand = solph.Flow(fix=self.demand_timeseries, nominal_value=1)
            
            hydrogenDemand = solph.Sink(label=self.label, inputs={self.sector: v_hydrogenDemand})
            
            return hydrogenDemand


```
</details>

## Hydrogenboiler

Hydrogenboiler Component --> Simple Transformer

**Input: Hydrogen**

**Output: Heat**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.001 |
| Conversion factor | `conversion_factor` | 0.95|
| Input Power Limit | `P_in` | 21 |
| Output Power Limit | `P_out` | 21 |
| Heating Value Hydrogen | `H_hydrogen` | 3 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Hydrogenboiler_ + position |

</details>

<details>
<summary>Code</summary>

```

    class Hydrogenboiler():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.95
            costsIn=0
            costsOut=0.001
            PowerInLimit = 21
            PowerOutLimit = 21
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'Hydrogenboiler_' + self.position

        def component(self):
            v_hydrogenBoilHydro = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in)
            q_hydrogenBoilTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out)
            
            hydrogenBoiler = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_hydrogenBoilHydro},
                outputs={self.sector_out : q_hydrogenBoilTh},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_hydrogen})
            return hydrogenBoiler
        
```
</details>

## Methanation

Methanation Component --> Simple Transformer

**Input: Hydrogen**

**Output: Gas**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.15 |
| Conversion factor | `conversion_factor` | 0.25|
| Input Power Limit | `P_in` | 500 |
| Output Power Limit | `P_out` | 500 |
| Heating Value Gas | `H_gas` | 11.42 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Methanation_ + position |

</details>

<details>
<summary>Code</summary>

```

    class Methanation():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.25
            costsIn=0
            costsOut=0.15
            PowerInLimit = 500
            PowerOutLimit = 500
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_gas = 11.42
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.H_gas = kwargs.get("H_gas", H_gas)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'Methanation_' + self.position

        def component(self):
            v_methanationHydrogen = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/(self.H_gas*self.conversion_factor), variable_costs=self.costs_in)
            q_methanationGas = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT/(self.H_gas), variable_costs=self.costs_out/self.H_gas)
            
            methanation = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_methanationHydrogen},
                outputs={self.sector_out : q_methanationGas},
                conversion_factors={self.sector_out : self.conversion_factor*self.H_gas})
            return methanation
        
```
</details>


## Hydrogen CHP

**Input: Hydrogen**

**Output: Electricity, Heat**

CHP is a special case, as this technology has two outputs. The output sectors, output costs and conversion factors are given to the component as list and are called with their indices. 

`sector_out=[sector_out1, sector_out1]`

`c_out=[c_out1, c_out2]`

`conversion_factor=[conversion_factor1, conversion_factor2]`

**It is important that output sectors and conversion factors are given to the component in the same order!** 

The default values are set in the order `[Electricity, Heat]`.


<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | [0.03, 0.03] |
| Conversion factor | `conversion_factor` | [0.35, 0.44]|
| Input Power Limit | `P_in` | 100 |
| Output Power Limit | `P_out` | 100 |
| Heating Value Hydrogen | `H_hydrogen` | 3 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | GasCHP_ + position |

</details>

<details>
<summary>Code</summary>

```

    class CHP():

        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta = [0.35, 0.4]
            costsIn=0
            costsOut=[0.03, 0.03]
            PowerInLimit = 100
            PowerOutLimit = 100
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
            
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", PowerInLimit)
            self.P_out = kwargs.get("P_Out", PowerOutLimit)
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'HydrogenCHP_' + self.position

        def component(self):
            v_CHPhydroH2 = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in)
            q_CHPhydroEl = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out[0])
            q_CHPhydroTh = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT, variable_costs=self.costs_out[1])
            
            CHPhydro = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_CHPhydroH2},
                outputs={self.sector_out[0] : q_CHPhydroEl, self.sector_out[1] : q_CHPhydroTh},
                conversion_factors={self.sector_out[0] : self.conversion_factor[0]*self.H_hydrogen, self.sector_out[1] : self.conversion_factor[1]*self.H_hydrogen})
            
            
            return CHPhydro



```
</details>


## Hydrogen Storage

**Input: Hydrogen**

**Output: Hydrogen**

Implemented as GenericStorage

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0.016 |
| Output costs | `c_out` | 0.016 |
| Input Efficiency | `eta_in` | 0.95 |
| Output Efficiency | `eta_out` | 0.95 |
| Standby Efficiency | `eta_sb` | 0.999 |
| Heating Value Hydrogen | `H_hydrogen` | 3 |
| Maximum SOC | `soc_max` | 300 |
| Start SOC | `soc_start` | 0 |
| Maximum Power In | `P_in` | 8 |
| Maximum Power Out | `P_out` | 8 |
| Minimum Level | `level_min` | 0 |
| Maximum Level | `level_max` | 1 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | sector_in |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Hydrogenstorage_ + position |
| Battery Balance | `balanced` | True |
</details>

<details>
<summary>Code</summary>

```

class Hydrogenstorage():
    
        def __init__(self, *args, **kwargs):
            
            P_inMax = 8
            P_outMax = 8
            costsIn = 0.016
            costsOut= 0.016
            Eta_in = 0.95
            Eta_out = 0.95
            Eta_sb = 0.999
            SOC_max = 300
            SOC_start = 0
            Storagelevelmin = 0
            Storagelevelmax=1
            emissions=0
            deltaT=1
            position='Consumer_1'
        
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
        
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.balanced = kwargs.get("balanced", True)
            self.eta_in = kwargs.get("eta_in", Eta_in)
            self.eta_out = kwargs.get("eta_out", Eta_out)
            self.eta_sb = kwargs.get("eta_sb", Eta_sb)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P_in = kwargs.get("P_in", P_inMax)
            self.P_out = kwargs.get("P_Out", P_outMax)
            self.soc_start = kwargs.get("soc_start", SOC_start)
            self.level_min = kwargs.get("level_min", Storagelevelmin)
            self.level_max = kwargs.get("level_max", Storagelevelmax)
            self.soc_max = kwargs.get("soc_max", SOC_max)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out", self.sector_in)
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'Hydrogenstorage_' + self.position
            
        
        
        def component(self):
            #Energy Flows
            v_hydrostoreIn = solph.Flow(min=0, max=1, nominal_value=self.P_in*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in*self.H_hydrogen)
            v_hydrostoreOut = solph.Flow(min=0, max=1, nominal_value=self.P_out*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in*self.H_hydrogen)
            
            hydroStorage = solph.GenericStorage(
                label=self.label,
                inputs={self.sector_in: v_hydrostoreIn},
                outputs={self.sector_out: v_hydrostoreOut},
                loss_rate=1-self.eta_sb, nominal_storage_capacity=self.soc_max,
                initial_storage_level=self.soc_start/self.soc_max, balanced=self.balanced,
                inflow_conversion_factor=self.eta_in*self.H_hydrogen, outflow_conversion_factor=self.eta_out/self.H_hydrogen,
                min_storage_level=self.level_min, max_storage_level=self.level_max)
            
        
            return hydroStorage        
    
    
```
</details>

## Hydrogen to Gas Grid

Hydrogen to Gas Grid --> Simple Transformer

**Input: Hydrogen**

**Output: Gasgrid**

A certain amount of hydrogen can be fed into the gas grid. Thereby it is necessary that the total amount is limited, which is implemented with the flow parameter `summed_max`. 

`Note: The output sector must be a dummy sector that is not belanced, to not contradict the balance rule for OEMOF Busses!`


<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Conversion factor | `conversion_factor` | 0.9|
| Maximum Gas Grid Volume | `V_max` | 30000 |
| Maximum Hydrogen Share in Gas Grid | `share_max` | 0.1 |
| Heating Value Gas | `H_gas` | 11.42 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | HydrogenGasgrid_ + position |
| Feed In Revenues | `revenues` | Numpy Array with 0 |


</details>

<details>
<summary>Code</summary>

```

    class HydrogenGasgrid():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.9
            V_max = 30000
            share_gasgridMax=0.1
            deltaT=1
            emissions=0
            position='Consumer_1'
            timesteps=8760
            
            #Heating value for hydrogen in kWh/m³
            H_gas = 11.42
            
            self.conversion_factor = kwargs.get("conversion_factor", eta)
            self.V_max = kwargs.get("V_max", V_max)
            self.share_max = kwargs.get("share_max", share_gasgridMax)
            self.H_gas = kwargs.get("H_gas", H_gas)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            self.label = 'HydrogenGasgrid_' + self.position
            self.timesteps = kwargs.get("timesteps", timesteps)
            
            revenuesEnergyArray = np.zeros(self.timesteps)
            
            self.revenues = kwargs.get("revenues", revenuesEnergyArray)
            self.revenues=np.multiply(self.revenues, -1)

        def component(self):
            v_h2tohydrogenGridIn = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.share_max, summed_max=1)
            v_h2tohydrogenGridOut = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.share_max, summed_max=1)
            #v_h2tohydrogenGridSold = solph.Flow(min=0, max=1, nominal_value=self.V_max*self.share_max, summed_max=1, variable_costs=self.revenues)
            
            #Transformer for real value to feed into gas grid
            hydro2gas = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_h2tohydrogenGridIn},
                outputs={self.sector_out : v_h2tohydrogenGridOut},
                conversion_factors={self.sector_out : self.conversion_factor})
            
   
            
            #hydro2gasgrid = solph.Sink(label='hydrogen2gasgrid', inputs={hydrogen2gasgrid_sector: v_h2tohydrogenGridSold})
            return hydro2gas
        
```
</details>

## Fuel Cell

Fuel cell Component --> Simple Transformer

**Input: Hydrogen**

**Output: Electricity**

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Input costs | `c_in` | 0 |
| Output costs | `c_out` | 0.018 |
| Efficiency | `efficiency` | 0.6|
| Power Limit | `P` | 5 |
| Heating Value Hydrogen | `H_hydrogen` | 3 |
| Input Sector | `sector_in` | / |
| Output Sector | `sector_out` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Fuelcell_ + position |

</details>

<details>
<summary>Code</summary>

```

class Fuelcell():

        
        def __init__(self, *args, **kwargs):
            #Default Data for Power to Heat Class
            eta=0.6
            costsIn=0
            costsOut=0.018
            P = 5
            deltaT=1
            emissions=0
            position='Consumer_1'
            
            #Heating value for hydrogen in kWh/m³
            H_hydrogen = 3
            
            self.efficiency = kwargs.get("efficiency", eta)
            self.costs_in = kwargs.get("c_in", costsIn)
            self.costs_out = kwargs.get("c_out", costsOut)
            self.P = kwargs.get("P", P)
            self.H_hydrogen = kwargs.get("H_hydrogen", H_hydrogen)
            self.sector_in = kwargs.get("sector_in")
            self.sector_out = kwargs.get("sector_out")
            self.deltaT = kwargs.get("timerange", deltaT)
            self.emissions=kwargs.get("emissions", emissions)
            self.position = kwargs.get("position", position)
            label = 'Fuelcell_' + self.position
            self.label = kwargs.get("label", label)
            self.labelSankey = 'Fuelcell_' + self.position
            self.color='royalblue'

        def component(self):
            v_fuelcellH2 = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT/self.H_hydrogen, variable_costs=self.costs_in)
            q_fuelcellH2 = solph.Flow(min=0, max=1, nominal_value=self.P*self.deltaT, variable_costs=self.costs_out)
            
            fuelcell = solph.Transformer(
                label=self.label,
                inputs={self.sector_in : v_fuelcellH2},
                outputs={self.sector_out : q_fuelcellH2},
                conversion_factors={self.sector_out : self.efficiency*self.H_hydrogen})
            return fuelcell    
        
```
</details>

## Hydrogen Sink

Sink for excess hydrogen. Only hydrogen as an input.

<details>
<summary>Parameters</summary>

| **Parameter** | **Name** | **Default Value** |
| ------ | ------ | ------ |
| Max Volume| `V_max` | 30000 |
| Sector | `sector` | / |
| Timestep | `timerange` | 1 |
| Emissions | `emissions` | 0 |
| Component Location | `position` | Consumer_1 |
| Label | `label` | Hydrogensink_ + position |

</details>

<details>
<summary>Code</summary>

```

class HydrogenSink():
    
    def __init__(self, *args, **kwargs):
        V_max=30000
        deltaT=1
        position='Consumer_1'
        
        self.V_max= kwargs.get("V_max", V_max)
        self.sector_in= kwargs.get("sector")
        self.deltaT = kwargs.get("timerange", deltaT)
        self.position = kwargs.get("position", position)
        label = 'Hydrogensink_' + self.position
        self.label = kwargs.get("label", label)
        self.labelSankey = 'Hydrogen\nSink_' + self.position
        self.costs_in=0
        self.color='slategrey'
        
        
    def component(self):
        v_hydrogenfeedin = solph.Flow(min=0, max=1, nominal_value=self.V_max, variable_costs=self.costs_in)
            
        hydroSink = solph.Sink(label=self.label, inputs={self.sector_in: v_hydrogenfeedin})
        
        return hydroSink
          
        
```
</details>

